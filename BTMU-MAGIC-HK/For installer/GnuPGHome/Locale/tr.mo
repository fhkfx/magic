��    �        Q  �?      0U  -   1U  #   _U  )   �U  %   �U  �   �U    YV     bW  1   zW  0   �W      �W  >   �W  =   =X  �   {X  ;   AY     }Y     �Y     �Y     �Y     �Y     Z     #Z  D   @Z  .   �Z  I   �Z  8   �Z     7[     T[     n[     �[     �[     �[     �[  "   �[     \  #   4\     X\     t\     �\     �\  $   �\  &   �\  ,   ]     C]     `]     {]     �]     �]     �]     �]     �]     ^     "^     <^  
   V^     a^     v^     �^     �^     �^  %   �^     �^  
    _     _     _     <_  +   J_  #   v_     �_  -   �_  %   �_  ]   `  Z   e`  H   �`      	a     *a     Ba  /   Ua  (   �a  .   �a  3   �a  "   b  )   4b     ^b     yb     �b     �b     �b     �b      �b     �b     c     *c     Fc     _c  "   xc  )   �c     �c     �c     �c     �c     d     4d     Kd     ad     vd     �d     �d  "   �d  %   �d  &   	e  !   0e  %   Re  "   xe  #   �e  '   �e      �e     f     %f     :f     Wf     lf     �f  (   �f  $   �f     �f     �f  #   g     0g     Og     ng  #   �g     �g  &   �g  %   �g  ,   h  4   Ch     xh     �h     �h     �h     �h     �h     i  '   i     >i     Zi     xi     �i     �i     �i  "   �i     �i     j  -   j  (   Gj  0   pj  H   �j  	  �j     �k     l     l  �   ,l  �   �l  A   �m  /   �m  0   �m  \   0n  1   �n     �n  C   �n  )   o  -   Bo  0   po  .   �o  J   �o  '   p     Cp     Xp     rp     �p     �p  3   �p  4   �p  -   q  �   Dq  .   �q  .   -r     \r  	   er  	   or     yr  .   �r  :   �r     �r     s     s  !   ;s  *   ]s  1   �s     �s  #   �s  &   �s  "   t  &   At     ht     t     �t     �t  <   �t  0   �t  '   #u     Ku  0   ku  '   �u  6   �u  G   �u  @   Cv  >   �v  +   �v  =   �v     -w     Bw     Rw  "   kw  :   �w     �w     �w     �w      x  /   "x  �   Rx     #y  H   =y  -   �y  ,   �y     �y  
   �y     z  8   z  #   Qz     uz     �z  "   �z  �   �z  E   N{  �   �{  9   *|  ;   d|  �   �|     T}     r}     �}  ;   �}  $   �}     ~     !~     9~     H~  �   h~  �   	     �     �     �     %�     ;�     N�     ^�     p�  %   ��     ��  S   ��  �   	�      ��  G   ��  !   	�  3   +�  <   _�     ��  "   ��  ,   ڂ  %   �  ,   -�  %   Z�  2   ��     ��      σ  (   ��      �  
   :�  *   E�     p�     ��     ��     ��     ф  ,   �     �     *�  
   E�  �   P�  "   ߅     �     �     1�  d   Q�     ��     Ȇ  �   �  E   ��  o   ̇      <�      ]�  7   ~�  '   ��  c   ވ  /   B�  E   r�  ,   ��  )   �  #   �  -   3�  )   a�  �   ��    n�  '   u�  �   ��     0�     9�     R�     [�     t�     ��  1   ��     ȍ  (   ؍  %   �     '�  %   D�     j�     ��     ��  9   ��     �     
�     %�     8�     L�      j�  }  ��     	�     �  /   2�    b�  "   ��     ��     ��     Ȕ     ޔ  *   ��     &�     ;�  J   N�  j   ��  �   �  9   ��  O   ˖  �   �  5   �  .   �  '   N�    v�  �   ��     K�     c�     ~�  !   ��     ��  "   Қ  '   ��  �   �     �  -   1�     _�  �   r�     B�     _�  *   ~�  +   ��     ՝     �     �  %   �     E�  7   _�     ��  r   �  1   ��  =   ��  I   ��  4   D�     y�  #   ��  =   ��     ��  ?   �  D   Z�  1   ��  %   ѡ  5   ��  A   -�  E   o�  H   ��  -   ��  H   ,�     u�  <   ��  )   ͣ  C   ��  =   ;�  .   y�  F   ��  H   �  2   8�  1   k�  9   ��  ;   ץ  %   �  '   9�      a�  2   ��     ��  !   ��  h  ا  *   A�  &   l�  1   ��  #   ŭ  2   �  >   �  �   [�      �  =   .�  0   l�  M   ��  "   �  &   �  3   5�  f   i�  %   а  D  ��  ,   ;�  5   h�     ��     ��  _   ��     �  
   3�  
   >�     I�  
   V�     a�     r�     ~�  %   ��     ��  S   ݴ  5   1�  4   g�     ��     ��     ��     Ե  (   �  #   �     /�     6�  
   ?�     J�     Q�  1   q�     ��  
   ��     ��     ٶ     ��  E   �     X�     v�     ~�     ��     ��     ��     ��     ��     ˷     ٷ     �     �     �     &�     (�     >�     Y�     n�      ��     ��     ĸ  +   �  !   �  '   .�  (   V�  %   �  2   ��  '   ع      �     �     $�     9�     N�     k�  :   ��  >   ��  ?   ��  ;   <�  "   x�     ��     ��  #   ̻     �  5   �     ;�     Q�  ;   d�  <   ��     ݼ     ��     
�      �     &�  A   ;�  &   }�     ��     ��     ѽ  -   �  ;   �  %   J�  (   p�  +   ��     ž     ۾     ��  1   �     H�     _�     e�     |�     ��     ��     ��     Ϳ     ܿ     ��     �     
�     �  3   �     L�     T�     b�     }�     ��     ��  %   ��     ��     ��     �     #�      0�  3   Q�  $   ��  =   ��  %   ��     �      '�     H�     g�     ��     ��     ��  '   ��     ��     �  &   5�  &   \�     ��     ��     ��     ��     ��     ��      �  K   �  "   i�  %   ��  $   ��     ��     ��     ��     �     �     (�     8�     J�     [�     z�     ��     ��  E   ��  @   �  @   E�     ��     ��     ��  !   ��  D   ��  +   �  /   C�     s�     x�     ��     ��     ��  %   ��  %   ��  $   �     ;�     L�     Z�  .   q�  '   ��     ��     ��     ��  3   �     N�     f�     ��     ��     ��     ��  $   ��  &   ��  $   �  '   >�     f�  ,   ��  '   ��     ��     ��  M   ��  N   9�     ��  '   ��  /   ��  "   ��     �      9�      Z�     {�     ��     ��  -   ��  0   �  *   4�  '   _�  #   ��  1   ��  %   ��  .   �  ,   2�  &   _�  "   ��  0   ��  9   ��  8   �  2   M�  "   ��  %   ��  !   ��     ��  >   	�  3   H�  %   |�  (   ��  2   ��     ��  $   �     :�  ;   X�  '   ��  #   ��  $   ��  $   �  +   *�  .   V�  ;   ��     ��  $   ��  7   �  3   >�     r�     ��  $   ��  5   ��  S   �  9   Y�  ,   ��  <   ��     ��  G   �  H   ]�     ��  B   ��  "   ��     �     2�     B�     b�  0   ��  ;   ��  2   ��     !�     #�     2�     7�  	   M�     W�     r�  !   ��     ��     ��     ��     ��     �     	�  '   #�     K�     j�  .   y�     ��  (   ��  )   ��  ,   �     :�     =�     [�  
   i�     t�  $   ��     ��     ��  )   ��      �     $�  %   C�     i�  &   ��  "   ��     ��  4   ��     �     $�  !   4�     V�     t�     ��  %   ��  %   ��     ��     �     �     /�     =�      K�  #   l�     ��     ��  &   ��  )   ��     ��  <   �     L�     b�     }�  ,   ��     ��     ��     ��  ;   �  >   ?�  G   ~�     ��     ��     ��     �     "�     :�     V�     n�  "   v�  -   ��  ,   ��     ��  +   �  *   :�  8   e�  9   ��     ��  !   ��  &   �  $   A�     f�     {�  5   ��     ��     ��     ��     ��  I   ��     )�     9�     P�     c�     ~�     ��  #   ��  #   ��     ��     �  !   �     @�  %   ]�     ��     ��     ��     ��     ��     ��     ��     ��     ��      �     �     �  &   3�     Z�     y�  #   ��     ��     ��  3   ��  %   �  %   8�  =   ^�  =   ��     ��     ��     ��     �     �  %   $�  	   J�     T�     ]�  
   b�     m�  $   ��     ��     ��     ��     ��  &   ��     �  "   5�     X�     l�     u�     ��  V   ��      ��  5   �  $   O�     t�  +   ��  #   ��  4   ��  %   �  
   :�  *   E�  $   p�  0   ��     ��     ��     ��  &   ��  .   �  *   G�  �   r�  :   �  +   >�  E   j�  /   ��  %   ��  A   �  &   H�     o�     ��  ,   ��     ��  (   ��     ��      �  -   �  /   C�  ,   s�  "   ��  )   ��     ��     �      '�     H�     b�     f�     ��  #   ��  -   ��     ��  I   �      ]�     ~�     ��     ��     ��     ��     ��     ��     �     :�     S�     g�     ��     ��     ��  #   ��     ��     ��     �     (�      <�     ]�     p�     ��     ��     ��     ��     ��     ��  6   �  (   8�     a�  #   s�      ��  
   ��  7   ��     ��     �     �     �  -   <�     j�     ��     ��     ��     ��     ��     �     �     -�     0�  D   4�  F   y�  E   ��  D   �  A   K�  7   ��     ��  $   ��  F   �      N�      o�  '   ��      ��     ��  "   ��  #   �  $   7�     \�  /   |�  '   ��  7   ��  $   �     1�     M�  #   f�     ��  !   ��     ��     ��  #   ��  `  �  =   x�  ,   ��  )   ��  +   �  �   9�    ��     ��  ,   �  1   A�     s�  6   ��  ;   ��  �   ��  `   ��     T�  !   j�  "   ��  "   ��  "   ��     ��  "     5   5  !   k  @   �  +   �  "   �      "   : "   ]    �    � $   � #   � )     /   * %   Z +   � $   �    � !   � #    *   5 "   ` 2   �     � "   �    � "       /    A    W    r    �    �    �    � 
   �    �        0    O 	   d    n "   �    � 0   � '   �     5   0 (   f t   � h    I   m "   � (   �     -    +   ? 4   k 1   � "   � .   � 9   $	    ^	    f	    }	    �	    �	 #   �	    �	    �	    
     -
    N
 #   `
 -   �
    �
    �
    �
    �
    �
        7    O    i    }    � #   � (   � -   � #    )   C    m (   � )   � "   �         ! ,   B     o )   �    � %   � (   � #       > '   T 4   | *   � *   � ?       G $   ] /   � (   � 7   �        + '   A    i        �    � &   �    �        )    <    Y    m 4   � %   �    � 3   � *   ! 0   L H   } '  �    �    �     �   ( �   � >   � @    0   Q o   � B   �    5 Q   P 3   � +   � 9    -   < Y   j 1   �    �         ,   :    g :   m L   � D   � �   : >   � 6   *    a    i 	   q    { C   � I   �        0    I #   a <   � A   � 1    "   6 %   Y -    '   �    �    �    �    	 Y    6   o .   � '   � E   � =   C H   � K   � H     B   _  -   �  A   �     !    .! '   B! -   j! J   �!    �!    �!    "     4" 8   U" �   �"    F# >   a# J   �# &   �#    $    1$    >$ *   Q$    |$    �$    �$ 4   �$ �   �$ H   f% �   �% ]   Q& Q   �& �   ' '   �' (   �' +   �' M   )( 0   w(    �(    �(    �(    �( �   �( �   �)    �* )   �*    �*    �*    �*    + 5   1+ %   g+ 2   �+    �+ d   �+ �   5, &   �, H   -    J- 3   i- L   �- (   �- *   . 9   >. B   x. ;   �. B   �. 5   :/ (   p/ (   �/ &   �/ (   �/ 
   0 .   0    L0    `0    ~0 (   �0 !   �0 .   �0 )   1 !   <1 
   ^1    i1     �1 .   
2    92 .   P2 o   2    �2    3 �   )3 K   �3 �   G4 #   �4 %   �4 @   5 %   T5 u   z5 .   �5 =   6 B   ]6 ,   �6 6   �6 0   7 %   57 �   [7 �   L8 )   -9 �   W9 	   �9    �9 	   :    :    3:    M: M   c:    �: (   �: 9   �: -   0; >   ^; 2   �; 3   �;    < P   $< 2   u<     �<    �<    �< !   �< +   = k  9=    �>    �> -   �> $  
? 3   /B    cB $   ~B &   �B "   �B F   �B    4C    FC ^   WC x   �C �   /D 0   �D @   �D �   'E 8   �E &   8F 0   _F �   �F �   �G    PH    bH $   }H (   �H    �H H   �H *   ,I �   WI &   QJ /   xJ    �J   �J    �K    �K 7   �K 8   4L #   mL .   �L     �L 2   �L    M *   +M �   VM �   N ?   �N A   �N N   O H   gO -   �O #   �O M   P    PP D   iP ]   �P %   Q (   2Q 9   [Q b   �Q i   �Q E   bR '   �R 9   �R %   
S F   0S 1   wS I   �S @   �S +   4T G   `T T   �T (   �T .   &U 5   UU 6   �U $   �U #   �U (   V 7   4V �   lV +   `W �  �W .   {\ (   �\ ;   �\ &   ] >   6] <   u] �   �] *   a^ T   �^ ?   �^ M   !_     o_ *   �_ ?   �_ �   �_ (   �`   �` 2   �b )   �b    c    %c o   1c    �c    �c    �c    �c    �c 	   d 	   d    !d )   ;d '   ed ^   �d 9   �d 3   &e    Ze '   qe    �e    �e 7   �e (   f    1f    8f 
   Af    Lf '   Sf 1   {f    �f 	   �f (   �f '   �f    g ^   5g )   �g 
   �g 
   �g    �g    �g    �g    h    !h    6h -   Bh    ph    �h    �h    �h    �h "   �h    �h    �h     i #   2i A   Vi B   �i 4   �i (   j (   9j +   bj 4   �j !   �j    �j    �j    k    k    1k    Nk 4   hk >   �k @   �k 6   l E   Tl    �l     �l    �l    �l :   m    Bm     Xm X   ym O   �m     "n    Cn    cn    �n    �n M   �n +   �n "   !o     Do    eo 6   wo :   �o "   �o &   p 0   3p    dp )   vp    �p A   �p "   �p    "q    (q    Hq    cq    vq    �q    �q    �q    �q    �q    �q    �q 4   �q    +r    3r    Kr    `r    ur    �r !   �r    �r    �r    �r    s 6   s P   Js (   �s 9   �s "   �s    !t -   ?t )   mt !   �t    �t    �t !   �t 3   u "   ?u 0   bu 8   �u 8   �u    v    v (   0v    Yv    `v +   uv '   �v Q   �v 3   w @   Ow 8   �w    �w    �w    �w    x    x    &x    9x    Vx #   ix %   �x    �x    �x T   �x R   0y K   �y    �y 
   �y    �y +   �y N   (z #   wz ,   �z    �z    �z ,   �z &   {    *{ B   I{ &   �{ &   �{    �{    �{    �{ )   | &   ;| (   b| %   �|    �| ?   �| $   }    4} #   T}    x}    �}    �} "   �} '   �} %   ~ (   +~    T~ /   d~ *   �~    �~    �~ Q   �~ Q   #    u 1   � B   � !   � -   $� .   R�     �� *   �� -   ̀    �� =   � <   X� 8   �� 4   ΁ 4   � E   8� .   ~� 2   �� 1   �� /   � +   B� >   n� J   �� G   �� <   @� 5   }� @   �� 2   � &   '� R   N� A   �� ?   � F   #� K   j� %   �� /   ܆ *   � A   7� 5   y� %   �� )   Շ ,   �� >   ,� J   k� G   �� '   �� ,   &� >   S� 9   �� (   ̉ +   �� ;   !� >   ]� R   �� 8   � 8   (� U   a�    �� F   ͋ M   �    b� C   u� +   �� !   �    � /   � $   O� @   t� I   �� C   ��    C�    E�    U� (   Z�    �� '   ��     �� &   �    �    $�    D�    _�    q�    w� 1   �� !   ��    � &   �    � &   0� +   W� (   ��    �� '   �� 	   א    � '   � +   � *   >�    i� &   o� &   �� (   �� )   � )   � /   :� ,   j�    �� G   ��    �    � #   #�    G�    c� (   y� .   �� .   ѓ     �    �    '� 
   >�    I�    X� *   x�    ��    ��    �� %   ϔ    �� 7   �    G�    a� '   {� N   ��    �    � !   
� L   ,� M   y� V   ǖ     �    ?�    D�    ]� !   u� $   ��    ��    ۗ +   � 3   � 0   C�    t� '   �� .   �� 1   � 2   �    L� )   j� )   �� (   ��    �    �� ,   �    C�    E�    H�    M� `   \�    ��    Ϛ    �    ��    �    6� +   T� +   �� '   ��    ԛ 6   � 7   � 7   T�    �� !   �� *   �� *   ��    �    )�    0�    7�    K�    M�    R� (   `� /   ��    ��    ՝ &   �    �    2� 4   N� #   �� &   �� C   Ξ A   �    T�    s�    {�    ��    �� B   �� 	   ��    �    �    � !   (� 3   J� *   ~�     ��    ʠ    ۠ +   �� %   $�    J�    e�    }�    ��    �� Q   �� )   
� ?   4� !   t�    �� 3   �� #   � I   �    N�    m� ,   {� 2   �� /   ۣ    � "   � 	   0�    :� -   T� (   ��    �� D   +� /   p� J   �� 0   � *   � I   G� 1   ��    æ    ۦ 8   �    � -   2�    `�    f� 8   �� A   �� B   �� 3   >� 9   r� #   �� )   Ш /   �� .   *�    Y�    ]�    {�    �� &   �� .   � ;   � +   K�    w�    ��    �� '   �� 
   �    � !   �     -�    N�    n� "   �� $   ��    ̫    � &   ��    � %   .�    T�    o� .   ��    �� !   Ӭ    ��    �    *�    2�    O�    o� 2   �� 3   ��    �� A   � -   G�    u� ?   ��    ʮ    ֮    � .   �� 4   '�    \� %   u�    ��    ��    ׯ    �    ��     �    4�    7� N   <� K   �� O   װ =   '� D   e� M   �� (   �� *   !� B   L� "   �� .   �� >   � &    �    G� 2   g� ,   �� 4   ǳ .   �� F   +� 1   r� O   �� E   �� -   :� ,   h� )   ��    �� &   ׵ %   ��    $� .   D�        �  S              �  Z       �   a   �      B   Y      &    �  �   1   �  �  �  B  �  J    �     F  |      e      �      �  �   �  �  �  �  �  �      c   �       w      �    �  �  �  =  �   �  �   �      �  �  ^          1     �  �      
  �  X   �  *  )   1  g          `  �      �      �  n  �  �   /       �  #          h  �      |  B      �      9  �  �  �  h   �      ;      �   o   h  �       �  �      A  �  �  �  �  �  �   �         f  �   �             c  �      #  8  a  ^  �   �   9  w  �  w  �  �         �   �  �  �    =  �         �  *  3      �          
  �   �  �                D       y      +         �  	   V  l  n  �  {  �  �  &   "   0      <      V  �                  �  -       �            �  �  �   �   {  �   �  �  c      �  J   �  <      �     �  �  ;  N  T      ?  v      2   �  �   �  �      �       g  �   �  !  �   �      ,                  {   (      v  �  �          �  j              �   �           �     �  �   �  )    �      �       ?    �   �      �  �   �      �  �  E  �  �  �  �  z   i  }   �  7      �   |   �             _     �  [      �   �   �  �      L   �     �      �  �   S    �           >      �       #  �   �    �  �  �  �  K   �  �  Y      I  �  t      B  2  �  �  �  W              �   �     W    k      U  �   �      �   �  �  �  F  �  ?   <     �      T      �  U          �  �      L                  �      �  �       �  �       �       �  �       �  �       �  %  �  :  H  �    �   �      �  �         ^  �  �   �    �  �      �  �  �         �   U   �       u   �              R  f  �  A    �       I        =      �  Q  .   �  A   �  �  }  �       Q  C  z     �   >       	  �    �  �      ^  V     ;   C  �  k  $  �  K          L      g   C  �              �      X  �            �   L  �  
  �  "  �  ]  �  3          �        F   ~       5  %  \           �  O   f  �    �  �   �      �               :           �  �      G  �      q          3  p   d       U          �  9  N  �  �    �  �  �   \  �      �  �      �  :      �  �       "  �      �   u  �  �  �    �   �  2     �  K  �  H        c  8   <  �       �  }  �   J      4  �  4       x  G   �  �  �   m   x  v  �  0   �      �  N  �               �          �  l  �  i  @  �  �      _  4  @   �   S  �   b      �   r      Q  )  �  R  �   �  �    J  �   R  �  u  �   �  �   i  R   o    l   @  �  �   �      �      ]              a  �   ,    ,  �  -        s      0  �      y       b           I   �    �  +      G  �      �  �      5  O  X  W   %   �      _      �      *  i   j   D  >       �      N   E  �   �  �  C   �  :  �  e  �     �           �      9   �       '      '  �      8  6  �  Z  M      �  �  �  T       /      �  o  �  �    r  �  (  �   Y  p              �   �      O  �  3       �  �  �  D  .  z  �  ,           �   �  d  �  �  [   �          �  �      X        �  �   =       �  n   �     �  �  h      �  $  �  y      m  v       �      �        j      �   �   �      Z  �     8      �  �  �   �   @  ]   �             �            �          �  !  �  �   �     .  �  m  }  �  a  �        �      w   7       �  k  �      �  �          `  �   M  7      �  G  �   �  �                   u      )  d  �           O  �  �        s      �  �  �  �  �  �      �     #   �    �   �   �     �           j      �    S       _       �     �  P   �   H   M            s   +  \      �  �    �     �  �   �  �      �  �  Q       �  p      �      �     �      �  �  �  �  �  �          �       P        �  t   �           
       �  �  �   \  �  r   �  �  |  �  {  �  o  �  �      t  %      �  �  �  0          �  .                 �  �      6  �  F      l  �         Z    E                      �  $   ?  D      �    z  p  V   �   `  d  b  b  �               �            s        �  �          �   (  �          ~  q          *   �  �  Y   �  '      '      	  y      �     g            n    4      �  H  �  �                  `       �  ]  t          1  r  �  �  I  �  ;          �   &  �  �      �  �  �  �      �  [         �       �  �  �             /  /     �     6   �  e   �          �   P  �      �  �  �  �  q          P    (   5  �  �  �   k   >  �  �  $  �  "  �          q  �   �               �   �  �   �      �  �      	  �  m  �        6  �   !  �  K  7   �      �       �          ~          �  �   �             -  �  �   �  �   �      �           �  �  5       E      �                      �          ~  +   T      �  e      M   x    �  -     W  f   �  �  �       �      !     2  &      �          �    �  �  [  �  �      �        �       x                           A              �  �        
Enter the user ID.  End with an empty line:  
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Key fingerprint =      Subkey fingerprint:    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
    signed by %08lX at %s%s
    signed by %08lX at %s%s%s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  Primary key fingerprint:  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" is not a JPEG file
 "%s" was already locally signed by key %08lX
 "%s" was already signed by key %08lX
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d keys processed (%d validity counts cleared)
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s ...
 %s does not expire at all
 %s encrypted data
 %s encryption will be used
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s signature from: "%s"
 %s signature, digest algorithm %s
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't make lock
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  ASCII armored output forced.
 About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to appoint this key as a designated revoker? (y/N):  Are you sure you want to use it (y/N)?  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Cipher:  Command>  Comment:  Compression:  Create a revocation certificate for this key?  Create a revocation certificate for this signature? (y/N)  Create anyway?  Critical signature notation:  Critical signature policy:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Displaying %s photo ID of size %ld for key 0x%08lX (uid %d)
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Experimental algorithms should not be used!
 Expired signature from " Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid passphrase; please try again Invalid selection.
 Is this correct (y/n)?  Is this okay?  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: Elgamal primary key detected - this may take some time to import
 NOTE: This key is not protected!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Policy:  Primary key fingerprint: Pubkey:  Public key is disabled.
 Quit without saving?  Real name:  Really create the revocation certificates? (y/N)  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really revoke all selected user IDs?  Really revoke this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Really update the preferences?  Reason for revocation: %s
 Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature expired %s
 Signature expires %s
 Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Signature notation:  Signature policy:  Syntax: gpg [options] [files]
Check signatures against known trusted keys
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The use of this algorithm is only supported by GnuPG.  You will not be
able to use this key to communicate with PGP users.  This algorithm is also
very slow, and may not be as secure as the other choices.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key  This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To be revoked by:
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 Unable to open photo "%s": %s
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) Use this key anyway?  User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: appointing a key as a designated revoker cannot be undone!
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: secret key %08lX does not have a simple SK checksum
 WARNING: signature digest conflict in message
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy url (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe enclosing directory ownership on %s "%s"
 WARNING: unsafe enclosing directory permissions on %s "%s"
 WARNING: unsafe ownership on %s "%s"
 WARNING: unsafe permissions on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [expired]  [filename] [revocation] [revoked]  [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid long keyID
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption anonymous recipient; trying secret key %08lX ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %08lX due to an unknown critical bit
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet binary build_packet failed: %s
 c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't get server read FD for the agent
 can't get server write FD for the agent
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open file: %s
 can't open signed data `%s'
 can't open the keyring can't put a policy URL into v3 (PGP 2.x style) signatures
 can't put a policy URL into v3 key (PGP 2.x style) signatures
 can't put notation data into v3 (PGP 2.x style) key signatures
 can't put notation data into v3 (PGP 2.x style) signatures
 can't query password in batchmode
 can't read `%s': %s
 can't search keyserver: %s
 can't set client pid for the agent
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking at depth %d signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 checking created signature failed: %s
 checking keyring `%s'
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 cipher extension "%s" not loaded due to unsafe permissions
 communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID deleting keyblock failed: %s
 delkey delphoto delsig deluid digest algorithm `%s' is read-only in this release
 disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error finding trust record: %s
 error in trailer line
 error reading `%s': %s
 error reading keyblock: %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 error: invalid fingerprint
 error: missing colon
 error: no ownertrust value
 expire export keys export keys to a key server export the ownertrust values external program calls are disabled due to unsafe options file permissions
 failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing digest algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 help iImMqQsS import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid symkey algorithm detected (%d)
 invalid value
 key key %08lX has been created %lu second in future (time warp or clock problem)
 key %08lX has been created %lu seconds in future (time warp or clock problem)
 key %08lX incomplete
 key %08lX marked as ultimately trusted
 key %08lX occurs more than once in the trustdb
 key %08lX: "%s" %d new signatures
 key %08lX: "%s" %d new subkeys
 key %08lX: "%s" %d new user IDs
 key %08lX: "%s" 1 new signature
 key %08lX: "%s" 1 new subkey
 key %08lX: "%s" 1 new user ID
 key %08lX: "%s" not changed
 key %08lX: "%s" revocation certificate added
 key %08lX: "%s" revocation certificate imported
 key %08lX: HKP subkey corruption repaired
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted as trusted key
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: direct key signature added
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid self-signature on user id "%s"
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey binding
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: this is a PGP generated ElGamal key which is NOT secure for signatures!
 key %08lX: unexpected signature class (0x%02X) - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key `%s' not found: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key incomplete
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l line too long
 list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never      never use a MDC for encryption new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition "%s"
 no corresponding public key: %s
 no default secret keyring: %s
 no entropy gathering module detected
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for `%s'
 no secret key
 no secret subkey for public subkey %08lX - ignoring
 no signed data
 no such user id no ultimately trusted keys found
 no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again passphrase too long
 passwd please do a --check-trustdb
 please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 please use "%s%s" instead
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent - disabling agent use
 problem with the agent: agent returns 0x%lx
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key does not match secret key!
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read error: %s
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring requesting key %08lX from %s
 resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke a user ID revoke signatures revsig revuid rounded up to %u bits
 s save save and quit search for keys on a key server searching for "%s" from HKP server %s
 secret key `%s' not found: %s
 secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key locally and non-revocably sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user id "%s"
 sorry, can't do this in batch mode
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 system error while calling external program: %s
 t take the keys from this keyring textmode the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temp files when calling external programs
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to display photo ID!
 unable to execute %s "%s": %s
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown protection algorithm
 unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg 1.2.2
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-05-21 11:08+0300
Last-Translator: Nilg�n Belma Bug�ner <nilgun@superonline.com>
Language-Team: Turkish <gnu-tr-u12a@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=CP1254
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 0.9.6
 
Kullan�c� kimli�ini girin. Bo� bir sat�r i�lemi sonland�r�r: 
Bu anahtar� kar��la�t�rmal� kontrol ettim.
 
Bu anahtar� �ok dikkatli kontrol ettim.
 
Her �eyiyle bu anahtar� kontrol edemedim.
 
Rasgele �retilen baytlar yetersiz. L�tfen baz� i�lemler yaparak
daha fazla rasgele bayt toplayabilmesi i�in i�letim sistemine
yard�mc� olun! (%d bayt daha gerekiyor)
 
Foto kimli�i olarak kullan�lmak �zere bir resim se�iniz. Resim bir JPEG
dosyas� olmal�d�r. Bu resim genel anahtar�n�zda saklanaca��ndan, �ok b�y�k
bir resim kullan�rsan�z genel anahtar�n�z da �ok b�y�k olacakt�r. Resim
boyutlar�n�n 240x288 civar�nda se�ilmesi uygun olacakt�r.
 
Desteklenen algoritmalar:
 
�mza d��ar� g�nderilemez olarak imlenecek.
 
�mza y�r�rl�kten kald�r�lamaz olarak imlenecek.
 
Bu bir �z-imza olacak.
 
UYARI: imza d��ar� g�nderilemez olarak imlenmeyecek.
 
UYARI: imza y�r�rl�kten kald�r�lamaz olarak imlenmeyecek.
 
Anahtar�n�z�n size ait oldu�unu belirten bir Kullan�c�-Kimli�i olmal�;
Kullan�c�-Kimli�i, Ger�ek �sminiz, Bir �nbilgi ve e-Posta Adresiniz
alanlar�n�n bir birle�iminden olu�ur. �rne�in:
	"Fatih Sultan Mehmed (Padisah) <padisah@ottoman.gov>"

 
Gizli anahtar�n kilidini a�mak i�in bir anahtar parolas�na ihtiyac�n�z var.
Anahtar�n sahibi: "                 den "                       al�nan: %lu                    de�i�medi: %lu
     yeni yard�mc� anahtarlar: %lu
       yeni kullan�c� kimli�i: %lu
              al�namad�: %lu
          kullan�c� kimliksiz: %lu
          Bu imzan�n sahibine ait oldu�u kesin de�il.
          Bu imza SAHTE olabilir.
          Bu imzan�n sahibine ait oldu�una dair bir belirti yok.
          Bu imza sahte anlam�na gelebilir.
                 yeni imzalar: %lu
 Yard�mc� anahtar parmak izi:      gizli anahtarlar okundu: %lu
      yeni anahtarlar atland�: %lu
      Anahtar parmak izi = Yard�mc� anahtar parmak izi:    (%d) DSA (yaln�z imzalamak i�in)
    (%d) DSA ve ElGamal (�ntan�ml�)
    (%d) ElGamal (yaln�z �ifrelemek i�in)
    (%d) ElGamal (imzalamak ve �ifrelemek i�in)
    (%d) RSA (sadece �ifrelemek i�in)
    (%d) RSA (imzalamak ve �ifrelemek i�in)
    (%d) RSA (sadece imzalamak i�in)
    (0) Cevab� bilmiyorum. %s
    (1) Tamamen kontrol edildi.%s
    (2) �li�kisel denetim yapt�m.%s
    (3) �ok dikkatli bir denetim yapt�m.%s
       yeni anahtar iptalleri: %lu
    %08lX taraf�ndan %s de y�r�rl�kten kald�r�lm��
    %08lX ile %s%s de imzalanm��
    %08lX ile %s%s%s de imzalanm��
   �mzalanam�yor.
   gizli anahtarlar indirildi: %lu
  %d = bilmiyorum
  %d = g�vence vermem
  %d = Tamamen g�veniyorum
  %d = ��yle b�yle g�veniyorum
  %d = Son derece g�veniyorum
  (�ntan�ml�)  (as�l anahtar kimli�i %08lX)  (d��arda ge�ersiz)  (duyarl�) Birincil anahtar parmak izi: [son kullanma tarihi: %s]  b = Daha fazla bilgi gerekli
  m = ana men�ye d�n
  k = ��k
  a = bu anahtar� atla
   gizli anahtarlar de�i�medi: %lu
  g�vencesi: %c/%c "
%08lX anahtar�n�zla %s de yerel olarak imzal�
 "
 %08lX anahtar�n�zla %s de imzaland�
 "%s" bir JPEG dosyas� de�il
 "%s" zaten %08lX anahtar�yla yerel olarak imzalanm��
 "%s" zaten %08lX anahtar�yla imzalanm��
 # Atanan g�vencede�erlerinin listesi %s olu�turuldu
# (Eski haline getirmek i�in "gpg --import-ownertrust" kullan�n
 %08lX: Bu anahtar�n ger�ekten sahibine ait olup olmad���ndan emin
olunamad� fakat yine de kabul edildi.
 %08lX: Bu anahtar�n ger�ekten sahibine ait oldu�una dair bir belirti yok
 %08lX: Bu anahtara g�ven-mi-yoruz
 %08lX: anahtar�n kullan�m s�resi dolmu�
 %d k�t� imza
 %d anahtar i�lendi (%d do�rulama temizlendi)
 %d imza hatalardan dolay� kontrol edilmedi
 %d imza kay�p bir anahtar y�z�nden kontrol edilmedi
 %d �z-imzas� ge�ersiz kullan�c� kimli�i saptand�
 %lu anahtar denetlendi (%lu imza)
 �imdiye dek %lu anahtar denetlendi (%lu imza)
 �u ana kadar her�ey yolunda giderek %lu anahtar i�lenmi�
 %s ...
 %s hep ge�erli olacak
 %s �ifreli veri
 %s �ifrelemesi kullan�lmayacak
 %s %s sonra ge�ersiz olacak
 %s ge�erli bir karakter seti de�il
 %s yenilerden
 %s de�i�meyenlerden
 %s, %s ile etkisiz olur!
 %s ile %s birlikte kullan�lmaz!
 %s imza: "%s"den
 %s imzas�, %s �z�mleme algoritmas�
 %s%c %4u%c/%08lX  �retildi: %s zamana��m�: %s %s.
 %s/%s "%s" i�in �ifrelendi
 %s: UYARI: dosya bo�
 %s: eri�ilemedi: %s
 %s: dizin olu�turulam�yor: %s
 %s: kilit olu�turulamad�
 %s: olu�turulamad�: %s
 %s: kilitleme yap�lamad�
 %s: a��lam�yor: %s
 %s: dizin olu�turuldu
 %s: dizin yok!
 %s: serbest kayd� okuma hatas�: %s
 %s: s�r�m kayd�n�n okunmas�nda hata: %s
 %s: s�r�m kayd�n�n g�ncellenmesinde hata: %s
 %s: dizin kayd�n� yazma hatas�: %s
 %s: s�r�m kayd�n�n yaz�lmas�nda hata: %s
 %s: kay�t ekleme ba�ar�s�z: %s
 %s: nitelemeli tablo olu�turulamad�: %s
 %s: s�r�m kayd� olu�turmada ba�ar�s�z: %s %s: kay�t s�f�rlama ba�ar�s�z: %s
 %s: dosya s�r�m� %d ge�ersiz
 %s: g�vence veritaban� ge�ersiz
 %s: ge�ersiz g�vence veritaban� olu�turuldu
 %s: anahtar zinciri olu�turuldu
 %s: bir g�vence veritaban� dosyas� de�il
 %s: atland�: %s
 %s: atland�: genel anahtar zaten var
 %s: atland�: genel anahtar iptal edildi
 %s: g�vence veritaban� olu�turuldu
 %s: bilinmeyen sonek
 %s: %lu kay�t numaras� ile s�r�m kayd�
 %s:%d: "%s" se�ene�i kullan�mdan kald�r�lmak �zere.
 %s:%d ge�ersiz d��salla�t�rma se�enekleri
 %s:%d: ge�ersiz i�selle�tirme se�enekleri
 %u bitlik %s anahtar�, %08lX kimli�i ile
%s tarihinde �retilmi� (a��klama verilmedi)
 (Burada %d se�ti�iniz varsay�l�yor)
 (Bu bir duyarl� y�r�rl�kten kald�rma anahtar�)
 (anahtar� parmak izi ile belirtmedik�e)
 (bu g�rev i�in yanl�� program kullanm�� olabilirsiniz)
 --clearsign [dosyaismi] --decrypt [dosyaismi] --edit-key kullan�c�-kimli�i [komutlar] --encrypt [dosyaismi] --lsign-key kullan�c�-kimli�i --nrlsign-key KULL-KML --nrsign-key KULL-KML --output se�ene�i bu komutla �al��maz
 --sign --encrypt [dosyaismi] --sign --symmetric [DOSYA] --sign [dosyaismi] --sign-key kullan�c�-kimli�i --store [dosyaismi] --symmetric [dosyaismi] -k[v][v][v][c] [kullan�c�-kimli�i] [anahtar-zinciri] ... bu bir yaz�l�m hatas� (%s:%d:%s)
 1 k�t� imza
 1 imza kay�p bir anahtar y�z�nden kontrol edilmedi
 1 imza bir hata y�z�nden kontrol edilmedi
 1 �z-imzas� ge�ersiz kullan�c� kimli�i saptand�
 @
(T�m komut ve se�eneklerin komple listesi i�in man sayfalar�na bak�n)
 @
�rnekler:

 -se -r Ali [dosya]         kullan�c� Ali i�in imzalar ve �ifreler
 --clearsign [dosya]        a��k�a okunabilir bir imza yapar
 --detach-sign [dosya]      ba��ms�z bir imza yapar
 --list-keys [isimler]      anahtarlar� listeler
 --fingerprint [isimler]    parmak izlerini g�sterir
 @
Se�enekler:
  @Komutlar:
  ASCII z�rhl� ��kt� istendi.
 Yeni bir %s anahtar �ifti �retmek �zeresiniz.
                   en k���k anahtar uzunlu�u:  768 bit
                  �ntan�ml� anahtar uzunlu�u: 1024 bit
    �nerilebilecek en b�y�k anahtar uzunlu�u: 2048 bit
 Bu anahtarlar t�m programlar taraf�ndan desteklenmedi�i i�in ve
onlarla olu�turulan imzalar gere�inden b�y�k ve do�rulanmas� �ok yava�
oldu�undan RFC2440 standard�nda tan�ml� olmalar�na ra�men tavsiye
edilmezler. Yard�mc� anahtar� �retmek istiyorsan�z "evet" ya da "e" girin. Bu yard�mc� anahtar� silme izni vermek istiyorsan�z "evet" girin Dosyan�n �zerine yaz�lacaksa l�tfen "evet" yaz�n Bu kullan�c� kimli�ini ger�ekten silmek istiyorsan�z "evet" girin.
B�ylece b�t�n sertifikalar� kaybedeceksiniz! Kullan�c� kimliklerinin T�M�n� imzalamak istiyorsan�z "evet" girin Cevap "evet" ya da "hay�r" Bu anahtar� kendi anahtar�n�zla imzalamak istedi�inize ger�ekten
emin misiniz?: " Bu anahtar uzunlu�unu istedi�inizden emin misiniz?  Onu yine de eklemek istiyor musunuz? (e/H)  Onu yine de y�r�rl�kten kald�rmak istiyor musunuz? (e/H)  Onu yine de imzalamak istiyor musunuz? (e/H)  bir anahtar�n, y�r�rl�kten kald�ran anahtar olmas�n� istedi�inizden emin misiniz? (e/H):  Onu kullanmak istedi�inizden emin misiniz? (e/H)  K�T� imza: " CRC hatas�; %06lx - %06lx
 �mza kontrol edilemedi: %s
 Bu anahtar �zerinde d�zenleme yap�lamaz: %s
 �ptal Son derece g�venli bir anahtarla sonu�lanan sertifikalar:
 (A)d� ve Soyad�, (Y)orum, (E)posta alanlar�n� de�i�tir ya da (T)amam/��(k)?  (A)d� ve Soyad�, (Y)orum, (E)posta alanlar�n� de�i�tir ya da ��(k)?  T�m kullan�c� kimlik tercihlerini (ya da se�ilen birini) mevcut tercihler
listesine �evirir. T�m etkilenen �z-imzalar�n zaman damgalar� bir sonraki
taraf�ndan �ne al�nacakt�r.
 Bir yard�mc� anahtar i�in son kullanma tarihi de�i�tiriliyor.
 As�l anahtar i�in son kullanma tarihi de�i�tiriliyor.
 �ifre:  Komut>  �nbilgi:  S�k��t�rma:  Bu imza i�in bir y�r�rl�kten kald�rma sertifikas� olu�turulsun mu?  Bu imza i�in bir y�r�rl�kten kald�rma sertifikas� olu�turulsun mu? (e/H)  Yine de olu�turulsun mu?  Kritik imza niteleyici:  Kritik imza guvencesi:  DSA anahtar �ifti 1024 bit olacak.
 DSA anahtar�n�n uzunlu�u 512 ile 1024 bit aras�nda olabilir
 DSA, 160 bitlik bir hash algoritmas� kullan�lmas�n� gerektiriyor
 Bir dosya veya standart girdinin z�rh�n� kald�r�r Bu do�ru imza silinsin mi? (e/H/k) Bu ge�ersiz imza silinsin mi? (e/H/k) Bu anahtar, anahtar zincirinden silinsin mi?  Bu bilinmeyen imza silinsin mi? (e/H/k) %d imza silindi.
 %d imza silindi.
 Ba��ms�z imza.
 �z�mlenen:  Anahtar 0x%3$08lX (kull-kiml %4$d) i�in %2$ld uzunluktaki %1$s foto kimli�i g�steriliyor
 Se�ilen anahtarlar� ger�ekten silmek istiyor musunuz?  Bu anahtar� ger�ekten silmek istiyor musunuz?  Ger�ekten bunu yapmak istiyor musunuz?  Se�ilen anahtarlar� ger�ekten y�r�rl�kten kald�rmak istiyor musunuz?  Bu anahtar� ger�ekten y�r�rl�kten kald�rmak istiyor musunuz?  Bu anahtara ger�ekten en y�ksek g�ven derecesini vermek istiyor musunuz? Yeni imzan�z�n s�resi dolmu� biriyle de�i�tirilmesini ister misiniz? (e/H)  Bu imzan�n d��arda da ge�erli hale getirilmesini istiyor musunuz? (e/H)  Bir OpenPGP �z-imzas� haline getirilmesini istiyor musunuz? (e/H)  Onu yine de imzalamak istiyor musunuz? (e/H)  �mzan�z�n da bu kadar s�re ge�erli olmas�n� ister misiniz? (E/h)  Foto kimliklerini g�stermez E-posta adresiniz:  Bir dosya veya standart girdiyi z�rhlar Foto kimli�i i�in JPEG dosya ismini giriniz:  �ste�e ba�l� a��klamay� girebilirsiniz; Bo� bir sat�r i�lemi sonland�r�r:
 Yeni dosya ismini giriniz Anahtar parolas�n� giriniz
 Anahtar parolas�n� girin:  Anahtar tutucunun ismini giriniz Bu gizli anahtar i�in yeni anahtar parolas�n� giriniz.

 �stenen de�eri girin. ISO tarihi (YYYY-AA-GG) girmeniz m�mk�nd�r fakat
iyi bir hata cevab� alamazs�n�z -- onun yerine sistem verilen de�eri
bir zaman aral��� olarak ��z�mlemeyi dener. Anahtar uzunlu�unu giriniz Bu iletiyi g�nderece�iniz adresin kullan�c� kimli�ini giriniz. Tasarlanm�� y�r�rl�kten kald�rma anahtar�n�n kullan�c� kimli�ini giriniz:  Deneysel algoritmalar kullan�lmamal�!
 Kullan�m tarihi ge�mi� imza: " �zellikler:  "%s" dosyas� var.  �mzan�n uygulanaca�� dosyan�n ismini verin �letinizi yaz�n ...
 Kullan�c� kimli�i: " Hash:  �pucu: �mzalamak i�in bir kullan�c� kimli�i se�iniz
 Bu anahtar�n ismi yukarda yaz�l� ki�iye ait oldu�unu ne kadar dikkatli
do�rulad�n�z?  Bu sorunun cevab�n� bilmiyorsan�z "0" yaz�n.
 IDEA �ifre kullan��s�z, iyimserlikle yerine %s kullan�lmaya �al���l�yor
 �sterseniz, neden bu y�r�rl�kten kald�rma sertifikas�n�
verdi�inizi a��klayan bir metin girebilirsiniz.
L�tfen bu metin k�sa olsun. Bir bo� sat�r metni bitirir.
 Bu y�r�rl�kten kald�r�lm�� anahtar� yine de kullanmak istiyorsan�z
cevap olarak "evet" yaz�n. Bu g�vencesiz anahtar� yine de kullanmak istiyorsan�z cevap olarak
 "evet" yaz�n. Genelde imzalama ve �ifreleme i�in ayn� anahtar� kullanmak iyi bir fikir
de�ildir. Bu algoritma sadece belli alanlarda kullan�labilir.
L�tfen g�venlik uzman�n�za dan���n. �nbilgi alan�nda ge�ersiz karakter var
 Ad ve soyad�n�zda ge�ersiz karakter var
 Komut ge�ersiz ("yard�m" komutunu deneyin)
 Ge�ersiz %08lX anahtar� --allow-non-selfsigned-uid kullan�larak ge�erli oldu
 Anahtar parolas� ge�ersiz; l�tfen tekrar deneyin Se�im ge�ersiz.
 Bu do�ru mu? (e/h)?  Bu tamam m�?  Bu foto do�ru mu? (e/H/�)?  Bu anahtar�n kullan�c� kimli�inde ismi belirtilen �ahsa ait
oldu�u kesin DE��L. *Ger�ekten* ne yapt���n�z� biliyorsan�z,
sonraki soruya da evet cevab� verebilirsiniz.
 Bir de�eri buraya i�aretlemek size kalm��; bu de�er herhangi bir 3. �ahsa
g�nderilmeyecek. Bir g�vence a�� sa�lamak i�in bizim buna ihtiyac�m�z var;
bunun (a��k�a belirtilmeden olu�turulmu�) sertifikalar a��yla
hi�bir alakas� yok. Anahtar �retimi durduruldu.
 Anahtar �retimi ba�ar�s�zl��a u�rad�: %s
 Anahtar tehlikede Anahtar art�k kullan�lmayacak Anahtar korunmu�.
 Anahtar y�r�rl�kten kald�r�ld�. Anahtar�n yerine ba�kas� konulmu� ve iptal edilmi�tir Anahtar ne kadar ge�erli olacak? (0)  G�ncelleme gere�i olmad���ndan anahtar de�i�medi.
 Anahtar Zinciri Hesaplama EPEYCE UZUN zaman alaca��ndan anahtar uzunluklar�nda
2048 bitten fazlas� tavsiye edilmez.
 S iSim de�i�tirmek i�in.
B �nBilgiyi de�i�tirmek i�in.
P e-Posta adresini de�i�tirmek i�in.
D anahtar �retimine Devam etmek i�in.
K anahtar �retiminden ��Kmak i�in. B�LG�: %s normal kullan�m i�in de�il!
 B�LG�: Elgamal birincil anahtar� saptand� - al�nmas� biraz zaman alacak
 B�LG�: Bu anahtar korunmam��!
 B�LG�: %d �ifre algoritmas� tercihlerde bulunamad�
 B�LG�: v3 anahtarlar� i�in yard�mc� anahtar �retimi OpenPGP uyumlu de�ildir
 B�LG�: anahtar y�r�rl�kten kald�r�lm��t� B�LG�: "%s" �ntan�ml� se�enek dosyas� yok
 B�LG�: eski �ntan�ml� se�enekler dosyas� `%s' yoksay�ld�
 B�LG�: %08lX gizli anahtar�n�n %s tarihinde kullan�m s�resi doldu
 B�LG�: g�nderen "yaln�z-g�zleriniz-i�in" ricas�nda bulundu
 B�LG�: %08lX imza anahtar�n�n kullan�m s�resi %s sular�nda dolmu�
 B�LG�: basit S2K kipi (0) kesinlikle tavsiye edilmez
 B�LG�: g�vence veritaban�na yaz�lam�yor
 Ad ve soyad�n�z bir rakamla ba�lamamal�
 Ad ve soyad�n�z en az 5 harfli olmal�
 Bunu yapmak i�in gizli anahtar gerekli.
 AaYyEeTtKk Gizli anahtar demetinde uygun/benzer imza yok
 yard�m mevcut de�il "%s" i�in yard�m mevcut de�il Belirtilmi� bir neden yok %d endeksine sahip yard�mc� anahtar yok
 B�yle bir kullan�c� kimli�i yok.
 G�ven derecesi belirtilmemi�:
%4u%c/%08lX %s " %d endeksine sahip kullan�c� kimli�i yok
 ge�erli bir E-posta adresi de�il
 Niteleme:  Bu anahtar �ifreleme i�in kullan�lamaz. �ifreleme i�in ikinci bir anahtar�
"--edit-key" se�ene�ini kullanarak �retebilirsiniz.
 Bilgi: Bu anahtar iptal edildi.
 Bilgi: Bu anahtar�n kullan�m s�resi dolmu�tu!
 Hi�bir �ey silinmedi.
 %08lX anahtar� ile imzalanacak hi�bir �ey yok
 Tamam, ama sald�r�lara �ok duyarl� olan monit�r ve klavye ���n�mlar�ndan kendinizi uzak tutun! (ne demekse...)
 �zerine yaz�ls�n m�? (e/H)?  L�tfen �nce hatay� d�zeltin
 Di�er kullan�c�lar� anahtarlar�n do�rulayacak
bu kullan�c�n�n g�ven derecesine l�tfen karar verin.
(pasportuna m� bakars�n�z yoksa farkl� kaynaklardan
parmakizlerini mi kontrol edersiniz...)? karar�n�z� verin
 L�tfen E-posta adresinizi Ad� ve Soyad� veya A��klama alan� i�ine koymay�n
 L�tfen yeni dosya ismini girin. Dosya ismini yazmadan RETURN tu�larsan�z
parantez i�inde g�sterilen �ntan�ml� dosya kullan�lacak. L�tfen �nbilgi girin (iste�e ba�l�) L�tfen veri dosyas�n�n ismini girin:  L�tfen bir anahtar parolas� giriniz; yazd�klar�n�z g�r�nmeyecek
 L�tfen bu g�venlik �atla��n� giderin
 G�sterilen anahtar�n, uygulamay� yeniden ba�lat�ncaya kadar, gerekli
do�rulukta olmayaca��n� l�tfen g�z�n�ne al�n�z.
 L�tfen gizli anahtarlardan se�ilenleri silin.
 L�tfen son parolay� tekrarlayarak ne yazd���n�zdan emin olun. Yaz�l�m hatalar�n� l�tfen <gnupg-bugs@gnu.org> adresine bildirin.
 L�tfen en fazla bir yard�mc� anahtar se�in.
 L�tfen sadece ve sadece bir kullan�c� kimlik se�iniz.
 L�tfen bir y�r�rl�kten kald�rma sebebi se�iniz:
 L�tfen istedi�iniz anahtar� se�iniz:
 L�tfen anahtar�n ne kadar s�reyle ge�erli olaca��n� belirtin.
         0 = anahtar s�resiz ge�erli
      <n>  = anahtar n g�n ge�erli
      <n>w = anahtar n hafta ge�erli
      <n>m = anahtar n ay ge�erli
      <n>y = anahtar n y�l ge�erli
 L�tfen imzan�nn ne kadar s�reyle ge�erli olaca��n� belirtin.
         0 = imza s�resiz ge�erli
      <n>  = imza n g�n ge�erli
      <n>w = imza n hafta ge�erli
      <n>m = imza n ay ge�erli
      <n>y = imza n y�l ge�erli
 l�tfen �nce "se�mece" komutunu kullan�n.
 L�tfen bekleyin rasgele baytlar toplan�yor. Bu i�lem s�ras�nda ba�ka
i�lere bak�n, ��nk� bu anahtar�n�z�n daha kaliteli olmas�n� sa�layacak.
 G�vence:  Birincil anahtar parmak izi: GenAnah:  Genel anahtar iptal edildi.
 Kaydetmeden ��k�ls�n m�?  Ad�n�z ve Soyad�n�z:  Bu y�r�rl�kten kald�rma sertifikalar�n� ger�ekten olu�turacak m�s�n�z? (e/H)  Ger�ekten olu�turulsun mu?  Bu �z-imza ger�ekten silinecek mi? (e/H) Se�ilen t�m kullan�c� kimlikleri ger�ekten silinecek mi?  Bu kullan�c� kimli�i ger�ekten silinecek mi?  Se�ilen t�m kullan�c� kimlikleri ger�ekten iptal edilecek mi?  Bu kullan�c� kimli�i ger�ekten iptal edilecek mi?  T�m kullan�c� kimlikleri ger�ekten imzalanacak m�?  Ger�ekten imzalayacak m�s�n�z?  Se�ilen kullan�c� kimlik i�in tercihleri ger�ekten g�ncellemek istiyor musunuz?  Tercihleri ger�ekten g�ncellemek istiyor musunuz?  Y�r�rl�kten kald�rma sebebi: %s
 Parolay� tekrar yaz�n�z
 Tekrar:  �stenen anahtar uzunlu�u: %u bit
 Y�r�rl�kten kald�rma sertifikas� �retildi.
 Y�r�rl�kten kald�rma sertifikas� �retildi.

Sertifika ba�kalar�n�n kolayca eri�ebilece�i yerlerde saklanmamal�d�r.
Aksi takdirde, y�r�rl�kten kald�rma sertifikan�z bilginiz d���nda
yay�nland���nda ge�erli olan genel anahtar�n�z�n ge�ersiz hale gelebilir.
Sertifika k�sa olaca��ndan isterseniz, bir yaz�c� ��kt�s� olarak al�p
bir kasada da muhafaza edebilirsiniz.
 De�i�iklikler kaydedilecek mi?  Gizli anahtar mevcut.
 As�l anahtar�n gizli par�alar� kullan�lamaz.
 Kullan�lacak algoritmay� se�iniz.

DSA (DSS olarak da bilinir) sadece imzalar i�in kullan�lan bir say�sal
imza algoritmas�d�r. Bu algoritma ElGamal algoritmas�ndan �ok daha h�zl�
do�ruland��� i�in �nerilmektedir.

ElGamal imzalar ve �ifreleme i�in kullan�lan bir algoritmad�r.
OpenPGP bu algoritman�n bu iki kullan�m�n� birbirinden ay�r�r:
sadece �ifreleme ve imza+�ifreleme; esas olarak ayn� gibi g�r�nmekle beraber
imzalar i�in kullan�lacak anahtar� olu�turacak baz� �zel parametrelerin
se�ilmesini gerektirir: bu program bunu yapar ama di�er OpenPGP
ger�eklemelerinin imza+�ifreleme olay�n� anlamas� gerekmiyorsa kullanmak
anlaml� olur.

�lk (as�l) anahtar imzalama yetene�ine sahip bir anahtar olmal�d�r;
bu durum, sadece �ifreleme yapabilen ElGamal anahtarlar�n�n neden men�de
bulunmad���n� a��klar. Komut sat�r�n� foto kimliklerini g�stermeye ayarlar Foto kimliklerini g�sterir Bu imzan�n ge�erlili�i %s de bitti.
 Bu imzan�n ge�erlili�i %s de bitecek.
 �mza ne kadar ge�erli olacak? (0)  %.*s imzas�, %s anahtar� ve
     %08lX kullan�c� kimli�i ile yap�lm��
 imza niteleyici:  imza guvencesi:  Kullan�m�: gpg [se�enekler] [dosyalar]
Bilinen g�venli anahtarlara g�re imzalar� kontrol eder
 Yaz�l���: gpg [se�enekler] [dosyalar]
imzalama, kontrol, �ifreleme veya ��zme
�ntan�ml� i�lem girilen veriye ba��ml�d�r
 Rasgele say� �reteci kendi halinde �al��an
bir kukla - g�venilir bir RS� de�il!

BU PROGRAMLA �RET�LM�� H��B�R VER�Y� KULLANMAYIN!!

 "%s" �zerindeki �z-imza
bir PGP 2.x tarz� imza.
 �mza ge�ersiz. Onu anahtar zincirinizden kald�rmak uygun olacak. Bu algoritman�n kullan�m� sadece GnuPG taraf�ndan desteklenmektedir.
Bu anahtar� PGP kullan�c�lar� ile haberle�mek i�in kullanamayacaks�n�z
Bu algoritma ayr�ca �ok yava� ve di�er se�imler kadar g�venli olmayabilir.
 Bir PGP 2.x tarz� kullan�c� kimli�ine uygun tercih yok.
 %s kipindeyken bu komut kullan�lamaz.
 Bu bir gizli anahtar! - ger�ekten silinecek mi?  Bu imza kullan�c� kimli�ini anahtara ba�lar. �z-imzay� silmek hi� iyi
bir fikir de�il. GnuPG bu anahtar� bir daha hi� kullanamayabilir.
Bunu sadece, e�er bu �z-imza baz� durumlarda ge�erli de�ilse ya da
kullan�labilir bir ikincisi var ise yap�n. Bu, anahtar �zerinde ge�erli bir imzad�r; anahtara ya da bu anahtarla
sertifikalanm�� bir di�er anahtara bir g�vence ba�lant�s� sa�lamakta
�nemli olabilece�inden normalde bu imzay� silmek istemezsiniz. Bu anahtar bizim
 Bu anahtar iptal edilmi�ti Bu anahtar�n kullan�m s�resi dolmu�! Bu anahtar�n ge�erlili�i %s de bitiyor.
 Bu anahtar korunmam��.
 Bu anahtar %s taraf�ndan �u anahtarla y�r�rl�kten kald�r�lm�� olabilir:  Bu anahtar�n sahibine ait oldu�u umuluyor
 Bu imza, anahtar�na sahip olmad���n�zdan, kontrol edilemez. Bu imzan�n
silinmesini hangi anahtar�n kullan�ld���n� bilene kadar
ertelemelisiniz ��nk� bu imzalama anahtar� ba�ka bir sertifikal�
anahtar vas�tas� ile bir g�vence ba�lant�s� sa�layabilir. Bu anahtar�n ge�erlili�i %s de bitti.
 Bu, anahtar� PGP 2.x i�in kullan��s�z yapacak.
 Y�r�rl�kten kald�ran:
 Web-of-Trust olu�turulabilmesi i�in GnuPG'ye hangi anahtarlar�n son derece
g�venli (bunlar gizli anahtarlar�na eri�iminiz olan anahtarlard�r) oldu�unun
bildirilmesi gerekir. "evet" yan�t� bu anahtar�n son derece g�venli
oldu�unun belirtilmesi i�in yeterlidir.
 ��lenmi� toplam miktar: %lu
 "%s" resmi a��lam�yor: %s
 Kullan�m�: gpg [se�enekler] [dosyalar] (yard�m i�in -h) Kullan�m�: gpgv [se�enekler] [dosyalar] (yard�m i�in -h) Bu anahtar yine de kullan�ls�n m�?  Kullan�c� kimli�i "%s" y�r�rl�kten kald�r�ld�. Kullan�c� kimli�i art�k ge�ersiz UYARI: %s se�ene�i kullan�mdan kald�r�lmak �zere.
 UYARI: %s %s'i a��yor
 UYARI: gizli bilgi i�eren 2 dosya mevcut.
 UYARI: Bu PGP-2 tarz� bir anahtar. Tasarlanm�� bir y�r�rl�kten kald�r�c�
       eklenmesi bu anahtar�n baz� PGP s�r�mleri taraf�ndan reddedilmesi
       ile sonu�lanabilir.
 UYARI: Bu PGP-2 tarz� bir anahtar. Bir foto kimli�i eklenmesi bu anahtar�n
       baz� PGP s�r�mleri taraf�ndan reddedilmesi ile sonu�lanabilir.
 UYARI: Bu anahtar sahibi taraf�ndan y�r�rl�kten kald�r�lm��t�!
 UYARI: Bu anahtar g�ven dereceli bir imza ile sertifikalanmam��!
 UYARI: Bu anahtar yeterli g�ven derecesine sahip imzalarla sertifikalanmam��!
 UYARI: Bu yard�mc� anahtar sahibi taraf�ndan y�r�rl�kten kald�r�lm��t�!
 UYARI: G�ven derecesiz anahtar kullan�l�yor!
 UYARI: Bu anahtara g�ven-mi-yoruz!
 UYARI: Zay�f anahtar saptand� - l�tfen anahtar parolas�n� tekrar de�i�tirin.
 UYARI: "%s" dosyas� bo�
 UYARI: bir kullan�c� kimli�i imzas� %d saniye gelecekte olu�turuldu
 UYARI: y�r�rl�kten kald�ran olarak tasarlanm�� bir anahtar ba�ka ama�la
       kullan�lamaz!
 UYARI: �ifreli ileti tahrip edilmi�!
 UYARI: ge�ersiz niteleme verisi bulundu
 UYARI: random_seed dosyas�n�n boyu hatal� - kullan�lmad�
 UYARI: anahtar %08lX y�r�rl�kten kald�r�lm�� olmal�: y�r�rl�kten kald�rma anahtar� %08lX al�n�yor
 UYARI: anahtar %08lX y�r�rl�kten kald�r�lm�� olabilir: y�r�rl�kten kald�rma anahtar� %08lX mevcut de�il.
 UYARI: ileti simetrik �ifre i�indeki zay�f bir anahtarla �ifrelendi.
 UYARI: ileti b�t�nl�k korumal� de�ildi
 UYARI: �oklu imzalar saptand�. Sadece ilki denetlenecek.
 UYARI: hi�bir �ey d��ar� aktar�lmad�
 UYARI: `%s' deki se�enekler bu �al��t�rma s�ras�nda hen�z etkin de�il
 UYARI: program bir "core" dosyas� olu�turabilir!
 UYARI: al�c�lar (-r) genel anahtar �ifrelemesi kullan�lmadan belirtilmi�
 UYARI: gizli anahtar %08lX basit bir SK sa�lamas�na sahip de�il
 UYARI: iletideki imza �z�mlemesi �eli�kili
 UYARI: %%-geni�letmesi imkans�z (�ok b�y�k).
Uzat�lmadan kullan�l�yor.
 UYARI: g�vence adresinin uzat�lmas� imkans�z (�ok b�y�k).
Uzat�lmadan kullan�l�yor.
 UYARI: %s ge�ici dizini silinemiyor: %s
 UYARI: ge�ici dosya silinemiyor (%s) `%s': %s
 UYARI: %s "%s" de g�vensiz ili�tirilen dizin iyeli�i
 UYARI: %s "%s" de g�vensiz ili�tirilen dizin izinleri
 UYARI: %s "%s" i�in g�vensiz iyelik
 UYARI: %s "%s" de g�vensiz izinler
 UYARI: kullan�lan bellek g�venli de�il!
 UYARI: kullan�lan rasgele say� �reteci g�venli de�il!!
 Bir miktar rasgele bayt �retilmesi gerekiyor. �lk �retim s�ras�nda biraz
hareket (klavyeyi kullanmak, fareyi hareket ettirmek, disklerden yararlanmak)
iyi olacakt�r; bu yeterli rasgele bayt kazanmak i�in rasgele say�
�retecine yard�mc� olur. 
 �stedi�iniz anahtar uzunlu�u nedir? (1024)  Bir anahtar� bir kullan�c� kimlikle imzalamadan �nce kullan�c� kimli�in
i�indeki ismin, anahtar�n sahibine ait olup olmad���n� kontrol etmelisiniz.

"0" bu kontrolu yapmad���n�z ve yapmay� da bilmedi�iniz anlam�ndad�r.
"1" anahtar size sahibi taraf�ndan g�nderildi ama siz bu anahtar� ba�ka
      kaynaklardan do�rulamad�n�z anlam�ndad�r. Bu ki�isel do�rulama i�in
      yeterlidir. En az�nda yar� anonim bir anahtar imzalamas� yapm��
      olursunuz.
"2" ayr�nt�l� bir inceleme yap�ld��� anlam�ndad�r. �rne�in parmakizi ve
      bir anahtar�n foto kimli�iyle kullan�c� kimli�ini kar��la�t�rmak
      gibi denetimleri yapm��s�n�zd�r.
"3" inceden inceye bir do�rulama anlat�r. �rne�in, �ah�staki anahtar�n
      sahibi ile anahtar parmak izini kar��la�t�rm��s�n�zd�r ve anahtardaki
      kullan�c� kimlikte belirtilen isme ait bir bas�l� kimlik belgesindeki
      bir foto�rafla �ahs� kar��la�t�rm��s�n�zd�r ve son olarak anahtar
      sahibinin e-posta adresini kendisinin kullanmakta oldu�unu da
      denetlemi�sinizdir.
Burada 2 ve 3 i�in verilen �rnekler *sadece* �rnektir.
Eninde sonunda bir anahtar� imzalarken "ayr�nt�l�" ve "inceden inceye" kontroller aras�ndaki ayr�ma siz karar vereceksiniz.
Bu karar� verebilecek durumda de�ilseniz "0" cevab�n� verin. Bu imzalar� y�r�rl�kten kald�rmak �zeresiniz:
 `%s' karakter k�mesini kullan�yorsunuz.
 Bir v3 anahtar�n�n son kullanma tarihini de�i�tiremezsiniz
 Son kullan�c� kimli�ini silemezsiniz!
 Bir kullan�c� kimli�i belirtmediniz. ("-r" kullanabilirsiniz)
 Bir anahtar parolas� vermediniz - bu �ok *k�t�* bir fikir!

 Bir anahtar parolas� istemediniz - bu *k�t�* bir fikir!
Nas�l isterseniz. Anahtar parolan�z� bu program� "--edit-key"
se�ene�i ile kullanarak her zaman de�i�tirebilirsiniz.

 Bu kullan�c� kimliklerini imzalam��s�n�z:
 PGP2 tarz� bir anahtara tasarlanm�� bir y�r�rl�kten kald�r�c� ekleyemeyebilirsiniz.
 PGP2 tarz� bir anahtara bir foto kimli�i ekleyemeyebilirsiniz.
 --pgp2 kipinde bir PGP 2.x anahtarlara bir OpenPGP imzas� uygulanamayabilir.
 En az bir anahtar se�melisiniz.
 En az bir kullan�c� kimli�i se�melisiniz.
 Gizli anahtar�n�z� korumak i�in bir Anahtar Parolan�z olmal�.

 "%.*s"
kullan�c�s�n�n gizli anahtar�n� a�acak bir anahtar parolas�na ihtiya� var.
%u bitlik %s anahtar�, kimlik %08lX, olu�turulan %s%s
 Se�ti�iniz KULLANICI-K�ML���:
    "%s"

 Sertifikalama i�in bir sebep belirtmelisiniz. ��eri�ine ba�l� olarak
bu listeden se�ebilirsiniz:
  "Anahtar tehlikede"
	Yetkisiz ki�ilerin gizli anahtar�n�za eri�ebildi�ine inan�yorsan�z
	bunu se�in.
  "Anahtar ge�ici"
	Mevcut anahtar� daha yeni bir anahtar ile de�i�tirmi�seniz bunu se�in.
  "Anahtar art�k kullan�lmayacak"
	Anahtar� emekliye ay�racaksan�z bunu se�in.
  "Kullan�c� kimli�i art�k ge�ersiz"
	Kullan�c� kimli�i art�k kullan�lamayacak durumdaysa bunu
	se�in; genelde Eposta adresi ge�ersiz oldu�unda kullan�l�r.
 "%s" �zerindeki imzan�z�n
kullan�m s�resi dolmu�.
 "%s" �zerindeki imzan�z
dahili bir imza.
 Karar�n�z?  Se�iminiz?  Sisteminiz 2038 y�l�ndan sonraki tarihleri g�steremiyor.
Ama emin olun ki 2106 y�l�na kadar elde edilebilecek.
 [Kullan�c� kimli�i bulunamad�] [s�resi doldu]  [dosyaismi] [y�r�rl�kten kald�rma] [y�r�rl�kten kald�r�ld�]  [�z-imza] [��pheli] `%s' zaten s�k��t�r�lm��
 `%s' d�zenli bir dosya de�il - g�r�lmedi
 `%s' ge�erli bir anahtar kimli�i de�il
 bir niteleme ismi sadece harfler, rakamlar ve alt�izgiler i�erebilir ve sonuna bir '=' gelir.
 bir niteleme de�erinde kontrol karakterleri kullan�lamaz
 bir kullan�c� niteleme ismi '@' karakteri i�ermeli
 bir foto kimli�i ekler bir y�r�rl�kten kald�rma anahtar� ekler bir yard�mc� anahtar ekler bir kullan�c� kimli�i ekler bu anahtar zincirini anahtar zincirleri listesine ekler bu gizli anahtar zincirini listeye ekler addkey addphoto addrevoker adduid �ifreleme i�in daima bir MDC kullan�l�r anonim al�c�: %08lX gizli anahtar� deneniyor ...
 z�rh ba�l���:  z�rh: %s
 sorular�n �o�unda cevap hay�r farzedilir sorular�n �o�unda cevap evet farzedilir %s �ifreli veri varsay�l�yor
 hatal� imzan�n bilinmeyen bir kritik bitten dolay� %08lX anahtar�ndan kaynakland��� san�l�yor
 "%s" i�indeki veri imzal� kabul ediliyor
 MPI hatal� URI hatal� sertifika hatal� anahtar hatal� anahtar parolas� hatal� genel anahtar hatal� gizli anahtar hatal� imza hatal� �nceden belirlenmi� i�lemler kipi: hi� sormaz daha az detayl� ikili build_packet ba�ar�s�z: %s
 k "%s" kapat�lam�yor: %s
 "%s" sunucusuna ba�lan�lamad�: %s
 %s olu�turulam�yor: %s
 "%s" olu�turulam�yor: %s
 `%s' dizini olu�turulam�yor: %s
 "core" olu�umu iptal edilemedi: %s
 bu �nceden belirlenmi� i�lemler kipinde (in batchmode) yap�lamaz
 �nceden belirlenmi� i�lemler kipinde "--yes" olmaks�z�n yap�lamaz
 anahtar sunucusunun %s adresinden anahtar al�namad�
 sunucu okuma dosya tan�t�c�s� al�namad�
 sunucu yazma dosya tan�t�c�s� al�namad�
 %d genel anahtar algoritmas� kullan�lamad�
 %d karakterden daha uzun metin sat�rlar� okunam�yor
 bu �oklu imzalar elde edilemiyor
 %s a��lamad�: %s
 `%s' a��lamad�
 `%s' a��lam�yor: %s
 dosya a��lamad�: %s
 imzal� veri '%s'  a��lamad�
 anahtar zinciri a��lamad� poli�e URL'si v3 (PGP 2.x tarz�) imzalara konulamaz
 poli�e URL'si v3 (PGP 2.x tarz�) anahtar imzalar�na konulamaz
 niteleme verisi v3 (PGP 2.x tarz�) anahtar imzalar�na konulamaz
 niteleme verisi v3 (PGP 2.x tarz�) imzalara konulamaz
 �nceden tan�mlanm�� i�lemler kipinde (batchmode) parola sorgulanamaz
 "%s" okunam�yor: %s
 anahtar sunucusu aranam�yor: %s
 istemci pid'i belirlenemiyor
 `%s' durumlanam�yor: %s
 S2K kipi sayesinde bir simetrik ESK paketi kullan�lam�yor
 "%s" yaz�lam�yor: %s
 kullan�c� taraf�ndan durduruldu
 bir PGP 2.x tarz� anahtar bir tasarlanm�� y�r�rl�kten kald�rma anahtar� olarak atanamaz
 simetrik �ifre i�in zay�f anahtar�n �nlenmesi m�mk�n olamad�: %d kere denendi!
 son kullan�m tarihini de�i�tirir sahibining�vencesini de�i�tirir anahtar parolas�n� de�i�tirir check anahtar imzalar�n� kontrol eder denetim %d derinlikte yap�l�yor: signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 olu�turulan imzan�n denetimi ba�ar�s�z: %s
 `%s' anahtar zinciri denetleniyor
 g�vence veritaban� denetleniyor
 "checksum" hatas� �ifre algoritmas� %d%s bilinmiyor ya da iptal edilmi�
 �ifre uzant�s� "%s" g�vensiz izinlerden dolay� y�klenmedi
 gpg-agent ile haberle�me problemi
 "completes-needed" 0 dan b�y�k olmal�
 s�k��t�rma algoritmas� %d..%d aral���nda olmal�
 �eli�en komutlar
 anahtar sunucusunun adresi ��z�mlenemedi
 ascii z�rhl� ��kt� olu�turur veri kaydedilmedi; kaydetmek i�in "--output" se�ene�ini kullan�n
 z�rh�n kald�r�lmas� ba�ar�s�z: %s
 debug veri �ifresini a�ar (�ntan�ml�) �ifre ��zme ba�ar�s�z: %s
 �ifre ��zme tamam
 bir yard�mc� anahtar siler imzalar� siler kullan�c� kimli�ini siler anahtar blo�u silinemedi: %s
 delkey delphoto delsig deluid bu da��t�mda �z�mleme algoritmas� %s salt-okunurdur
 disable bir anahtar� iptal eder v3 imzalara zorlamaz v4 imzalara zorlamaz hi�bir de�i�iklik yapmaz terminali hi� kullanma RFC1991 de a��klanan kipi uygular enable bir anahtar� kullan�ma sokar z�rhlama ba�ar�s�z: %s
 veriyi �ifreler %s anahtar� ve %08lX kullan�c� kimli�i ile �ifrelendi
 %u bitlik %s anahtar� ve kullan�c� kimli�i
%08lX ile �ifrelendi, %s olu�turuldu
 bilinmeyen algoritma %d ile �ifrelenmi�
 --pgp2 kipinde ileti �ifrelemesi IDEA �ifresi gerektirir
 sadece simetrik �ifre ile �ifreler `%s' olu�turulurken hata: %s
 `%s' anahtar zinciri olu�turulurken hata: %s
 anahtar parolas� olu�turulurken hata: %s
 g�vence kayd�n� ararken hata: %s
 kuyruk sat�r�nda hata
 "%s" okunurken hata: %s
 anahtar blo�u okunurken hata: %s
 gizli anahtar blo�u `%s' okunurken hata olu�tu: %s
 "%s" adresine g�nderme hatas�: %s
 "%s" anahtar zincirine yazarken hata olu�tu: %s
 `%s' genel anahtar zincirine yaz�l�rken hata olu�tu: %s
 `%s' gixli anahtar zincirine yaz�l�rken hata olu�tu: %s
 hata: parmakizi ge�ersiz
 hata: ":" eksik
 hata: hi� sahibining�vencesi de�eri yok
 expire anahtarlar� g�nderir anahtarlar� bir anahtar sunucusuna g�nderir sahibining�vencesi de�erlerini g�nderir g�vensiz options dosyas� yetkilerinden dolay� d�� program �a�r�lar� iptal
edildi
 "%s" adresine g�nderme i�lemi ba�ar�s�z (durum=%u)
 "TrustDB" g�vence veritaban� ba�lang�� a�amas�nda ba�ar�s�z: %s
 anahtar zinciri belle�i yeniden olu�turulurken hata: %s
 dosya kapama hatas� dosya olu�turma hatas� dosya silme hatas� dosya mevcut dosya a�ma hatas� dosya okuma hatas� dosya isim de�i�tirme hatas� dosya yazma hatas� bozulan g�vence veritaban�n� onar�r kullan�c� kimli�ini as�l olarak imler v3 imzalar�na zorlar v4 imzalara zorlar al�c�n�n tercihleriyle �eli�en %s (%d) s�k��t�rma algoritmas� kullan�lmak isteniyor
 al�c�n�n tercihleriyle �eli�en %s (%d) �z�mleme algoritmas� kullan�lmak isteniyor
 al�c�n�n tercihleriyle �eli�en %s (%d) simetrik �ifre kullan�m� zorlan�yor
 fpr genel hata yeni bir anahtar �ifti �retir bir y�r�rl�kten kald�rma sertifikas� �retir gizli anahtar�n g�venli�i i�in eski tarz 16 bitlik sa�lama toplam� �retiliyor
 gpg-agent bu oturumda kullan�lamaz
 gpg-agent protokol� s�r�m %d desteklenmiyor
 help bBmMaAkK anahtarlar� bir anahtar sunucusundan indirir sahibining�vencesi de�erlerini indirir anahtarlar� indirir/kat��t�r�r girdi sat�r� %u ya �ok uzun ya da sonunda sat�rsonu karakteri yok
 girdi sat�r� %d karakterden daha uzun
 S2K kipi ge�ersiz; 0, 1 veya 3 olmal�
 ge�ersiz arg�man ge�ersiz z�rh z�rh ba�l��� ge�ersiz:  ge�ersiz z�rh: sat�r %d karakterden uzun
 tercih dizgesindeki karakter ge�ersiz
 a��k�a okunabilen imza ba�l��� ge�ersiz
 ara�izgisi escape'l� sat�r ge�ersiz:  �ntan�ml� tercihler ge�ersiz
 �ntan�ml� denetim seviyesi ge�ersiz; 0, 1, 2, ya da 3 olabilir
 d��salla�t�rma se�enekleri ge�ersiz
 `%s' hash algoritmas� ge�ersiz
 i�selle�tirme se�enekleri ge�ersiz
 anahtar zinciri ge�ersiz ge�ersiz paket anahtar parolas� ge�ersiz ki�isel �ifre tercihleri ge�ersiz
 ki�isel s�k��t�rma tercihleri ge�ersiz
 ki�isel �z�mleme tercihleri ge�ersiz
 ge�ersiz radix64 karakteri %02x atland�
 yan�t ge�ersiz
 proc_tree() i�inde ge�ersiz k�k paket saptand�
 ge�ersiz symkey algoritmas� saptand� (%d)
 de�er hatal�
 key %08lX anahtar� %lu saniye gelecekte �retilmi� (zaman sapmas� veya saat problemi)
 %08lX anahtar� %lu saniye gelecekte �retilmi� (zaman sapmas� veya saat problemi)
 anahtar %08lX: i�i bo�
 anahtar %08lX son derece g�venli olarak imlendi.
 %08lX anahtar� g�vence veritaban�nda birden fazla kay�tta bulundu
 anahtar %08lX: "%s" %d yeni imza
 anahtar %08lX: "%s" %d yeni yard�mc� anahtar
 anahtar %08lX: "%s" %d yeni kullan�c� kimli�i
 anahtar %08lX: "%s" 1 yeni imza
 anahtar %08lX: %s 1 yeni yard�mc� anahtar
 anahtar %08lX: "%s" 1 yeni kullan�c� kimli�i
 anahtar %08lX: "%s" de�i�medi
 anahtar %08lX: "%s" y�r�rl�kten kald�rma sertifikas� eklendi
 anahtar %08lX: "%s" y�r�rl�kten kald�rma sertifikas� al�nd�
 anahtar %08lX: HKP yard�mc� anahtar bozulmas� giderildi
 %08lX anahtar�: PGP 2.x tarz� bir anahtar - atland�
 %08lX anahtar�: g�venli anahtar olarak kabul edildi
 anahtar %08lX: �z-imzal� olmayan kullan�c� kimli�i '%s' kabul edildi
 anahtar %08lX: zaten gizli anahtar zincirinde
 anahtar %08lX: �zg�n anahtar bloku bulunamad�: %s
 anahtar %08lX: �zg�n anahtar bloku okunamad�: %s
 anahtar %08lX: do�rudan anahtar imzas� eklendi
 anahtar %08lX: bizim kopyam�zla e�le�miyor
 anahtar %08lX: �ift kullan�c� kimli�i saptand� - kat��t�r�ld�
 anahtar %08lX: y�r�rl�kten kald�rma sertifikas� ge�ersiz: %s - reddedildi
 anahtar %08lX: y�r�rl�kten kald�rma sertifikas� ge�ersiz: %s - atland�
 anahtar %08lX: kullan�c� kimli�i "%s" i�in �z-imza ge�ersiz
 anahtar %08lX: yard�mc� anahtar garantileme ge�ersiz
 anahtar %08lX: yard�mc� anahtar y�r�rl�kten kald�rmas� ge�ersiz
 anahtar %08lX: anahtar y�r�rl�kten kald�r�lm��t�!
 anahtar %08lX: yeni anahtar - atland�
 anahtar %08lX: genel anahtar de�il - y�r�rl�kten kald�rma sertifikas� uygulanamaz
 %08lX anahtar�: g�venli anahtar i�in genel anahtar yok - atland�
 anahtar %08lX: anahtar� garantilemek i�in yard�mc� anahtar yok
 anahtar %08lX: anahtar� y�r�rl�kten kald�r�lacak yard�mc� anahtar yok
 anahtar %08lX: anahtar� y�r�rl�kten kald�racak yard�mc� anahtar paketi yok
 anahtar %08lX: kullan�c� kimli�i yok
 anahtar %08lX: imza i�in kullan�c� kimli�i yok
 anahtar %08lX: kullan�c� kimli�i ge�ersiz
 anahtar %08lX: imza g�nderilebilir de�il (%02x s�n�f�) - atland�
 %08lX anahtar�: bir RFC2440 anahtar� de�il - atland�
 %08lX anahtar�: korunmam�� - atland�
 anahtar %08lX: genel anahtar "%s" al�nd�
 anahtar %08lX: genel anahtar bulunamad�: %s
 anahtar %08lX: �ok say�da yard�mc� anahtar ba�lant�s� silindi
 anahtar %08lX: �ok say�da yard�mc� anahtar y�r�rl�kten kald�rmas� silindi
 anahtar %08lX: y�r�rl�kten kald�rma sertifikas� yanl�� yerde - atland�
 anahtar %08lX: gizli anahtar indirildi
 anahtar %08lX: gizli anahtar bulunamad�: %s
 %08lX anahtar�: ge�ersiz �ifreli (%d) gizli anahtar - atland�
 %08lX anahtar�: genel anahtars�z gizli anahtar - atland�
 anahtar %08lX: yard�mc� anahtar atland�
 anahtar %08lX: kullan�c� kimli�i atland�: ' anahtar %08lX: yard�mc� anahtar y�r�rl�kten kald�r�lm��t�!
 anahtar %08lX: yard�mc� anahtar imzas� yanl�� yerde - atland�
 anahtar %08lX: Bu, imzalar i�in g�venli olmayan PGP �retimi bir ElGamal anahtar�!
 anahtar %08lX: umulmayan imza s�n�f� (0x%02X) - atland�
 anahtar %08lX: genel anahtar algoritmas� desteklenmiyor
 anahtar %08lX: genel anahtar algoritmas�, kullan�c� kimli�i "%s" i�in desteklenmiyor
 anahtar `%s' yok: %s
 anahtar %lu saniye sonra �retilmi� (zaman sapmas� veya saat problemi)
 anahtar bundan %lu saniye sonra �retilmi� (zaman sapmas� veya saat problemi)
 anahtar�n i�i bo�
 anahtar g�venli olarak imlenmemi� - onu sahte RS� ile kullanmay�n!
 anahtar son derece g�venli olarak imlendi.
 `%s' anahtar zinciri olu�turuldu
 anahtar sunucusu hatas� anahtar uzunlu�u ge�ersiz; %u bit kullan�l�yor
 anahtar uzunlu�u %u bite yuvarland�
 anahtar uzunlu�u �ok b�y�k; izin verilen en b�y�k de�er: %d bit
 anahtar uzunlu�u �ok k���k; RSA anahtar� i�in en k���k uzunluk: 1024 bit
 anahtar uzunlu�u �ok k���k; en k���k anahtar uzunlu�u 768 bit'tir.
 l sat�r �ok uzun
 list anahtar� ve kullan�c� kimli�ini g�sterir anahtarlar� listeler anahtarlar� ve parmak izlerini listeler anahtarlar� ve imzalar� listeler sadece paketlerin silsilesini listeler tercihleri listeler (uzman) tercihleri listeler (ayr�nt�l�) gizli anahtarlar� listeler imzalar� listeler lsign ba��ms�z bir imza yapar zaman damgas� �eli�kilerini uyar� olarak bildirir make_keysig_packet ba�ar�s�z: %s
 CRC bozulmu�
 GPG_AGENT_INFO �evre de�i�keni hatal�
 kullan�c� kimli�i bozuk "marginals-needed" 1 den b�y�k olmal�
 "max-cert-depth" 1 ile 255 aras�nda olmal�
 bir anahtar imzas� do�ru yere ta��n�yor
 hH a��k�a okunabilen imzalar dahil edildi
 a� hatas� asla     �ifreleme i�in asla bir MDC kullan�lmaz yeni yap�land�rma dosyas� `%s' olu�turuldu
 sonraki g�vence veritaban� denetimi %s de
 hay�r grup tan�m� "%s" i�inde = i�areti yok
 kar��l��� olan genel anahtar yok: `%s
 �ntan�ml� gizli anahtar zinciri yok: %s
 rasgele bayt elde etme mod�l� bulunamad�
 bir g�vence veritaban� denetimi gereksiz
 uzaktan uygulama �al��t�r�lmas� desteklenmiyor
 `%s' i�in y�r�rl�kten kald�rma anahtar� yok
 gizli anahtar yok
 yard�mc� genel anahtar %08lX i�in bir gizli anahtar yok - yoksay�l�yor
 imzal� veri yok
 b�yle bir kullan�c� kimli�i yok son derece g�venli bir anahtar yok
 ge�erli OpenPGP verisi yok
 ge�erli adresler yok
 yaz�labilir bir anahtar zinciri yok: %s
 yaz�labilir bir genel anahtar zinciri yok: %s
 yaz�labilir bir gizli anahtar zinciri yok: %s
 bir ba��ms�z imza de�il
 �ifrelenemedi insan okuyabilir de�il i�lenemedi desteklenmiyor not: "random_seed" dosyas� bo�
 bilgi: "random_seed" dosyas� g�ncel de�il
 nrlsign nrsign tamam, biz anonim al�c�y�z.
 DEK'in eski kodlamas� desteklenmiyor
 eski stil (PGP 2.x) imza
 g�venli bellek haz�rlanmadan i�lem yapmak m�mk�n de�il
 se�enek dosyas� "%s": %s
 �zg�n dosya ad� = '%.*s'
 sahibinin g�vencesi bilgisi temizlendi
 ikinci kez yazd���n�z anahtar parolas� ilkiyle ayn� de�il; i�lem tekrarlanacak Parola �ok uzun
 passwd l�tfen bir --check-trustdb yap�n
 l�tfen bir E-posta adresi girin (iste�e ba�l� ancak kuvvetle tavsiye edilir) Daha geni� bilgi edinmek i�in http://www.gnupg.org/faq.html adresine bak�n�z
 Daha fazla bilgi i�in l�tfen http://www.gnupg.org/why-not-idea.html adresine
bak�n�z.
 l�tfen yerine "%s%s" kullan�n�z
 pref %c%lu tercihi yinelendi
 %c%lu tercihi ge�ersiz
 dosya sonu belirsiz (CRC i�inde)
 dosya sonu belirsiz (kuyruk i�inde)
 dosya sonu belirsiz (CRC yok)
 primary �ifreli paketin elde edilmesinde sorun var
 vekil ile problem - vekil kullan�m� iptal ediliyor
 vekil ile sorun var: vekil 0x%lx ile sonu�land�
 �zerine yazmadan �nce sorar koruma algoritmas� %d%s desteklenmiyor
 genel ve gizli anahtar �retildi ve imzaland�.
 genel anahtar %08lX imzadan %lu saniye daha yeni
 genel anahtar %08lX imzadan %lu saniye daha yeni.
 %08lX genel anahtar� yok: %s
 genel anahtar �ifre ��z�m� ba�ar�s�z: %s
 genel anahtar gizli anahtarla uyu�muyor!
 genel anahtarla �ifreli veri: do�ru DEK
 genel anahtar: %08lX
 genel anahtar bulunamad� son derece g�venli %08lX genel anahtar� yok
 k �� quit bu men�den ��k z�rh i�inde uluslararas� karakterler - b�y�k olas�l�kla hatal� bir e-posta sunucusu kullan�lm��
 okuma hatas�: %s
 se�enekleri dosyadan okur `%s'den okunuyor
 "%s"den se�enekler okunuyor
 standart girdiden okuyor ...
 y�r�rl�kten kald�rma sebebi:  anahtarlar� genel anahtar zincirinden siler anahtarlar� gizli anahtar zincirinden siler %08lX anahtar� %s adresinden isteniyor
 i� kaynak s�n�r� y�rkal! yard�mc� anahtar y�r�rl�l�kten kald�r�ld�: %s
 y�rkal- sahte y�r�rl�kten kald�rma sertifikas� bulundu
 y�rkal? Y�r�rl�kten kald�rma denetlenirken problem: %s
 revkey y�r�rl�kten kald�rma a��klamas�:  bir yard�mc� anahtar� y�r�rl�kten kald�r�r bir kullan�c� kimli�i y�r�rl�kten kald�r�r imzalar� y�r�rl�kten kald�r�r revsig revuid %u bite yuvarland�
 i save kaydet ve ��k bir anahtar sunucusunda anahtarlar� arar HKP sunucusunun %2$s adresinde "%1$s" aran�yor
 gizli anahtar `%s' yok: %s
 gizli anahtar kullan��s�z gizli anahtar par�alar� kullan�m d���
 N yard�mc� anahtar�n� se�er N kullan�c� kimli�ini se�er se�ilen sertifikalama �z�mleme algoritmas� ge�ersiz
 se�ilen �ifre algoritmas� ge�ersiz
 se�ilen �z�mleme algoritmas� ge�ersiz
 t�m paket, �ifre ve �z�mleme se�eneklerini OpenPGP tarz�nda ayarlar t�m paket, �ifre ve �z�mleme se�eneklerini PGP 2.x'e g�re ayarlar tercih listesi olu�turmak i�in setpref parmak izini g�sterir foto kimli�ini g�sterir bunu g�sterir  listedeki bir anahtar�n hangi anahtar zincirinde oldu�unu g�sterir showphoto showpref sign bir anahtar� imzalar bir anahtar� yerel olarak imzalar bir anahtar� yerel ve iptal edilemez olarak imzalar bir anahtar� iptal edilemez olarak imzalar bir anahtar� d�zenler ve imzalar anahtar� imzalar anahtar� yerel olarak imzala y�r�rl�kten kald�r�lamayan yerel imza yapar y�r�rl�kten kald�r�lamayan imza yapar imza do�rulama engellendi
 imzalama ba�ar�s�z: %s
 imzalan�yor: "%s" atland�: %s
 `%s' atland�: tekrarlanm��
 `%s' atland�:
Bu, imzalar i�in g�venli olmayan PGP �retimi bir ElGamal anahtar�!
 atland�: genel anahtar zaten belirtilmi�
 atland�: genel anahtar zaten �ntan�ml� al�c� olarak ayarlanm��
 atland�: gizli anahtar zaten var
 %d. t�r blok atland�
 kullan�c� kimli�i "%s" i�in v3 �z-imzas� atlan�yor
 pardon, bu betik kipinde yap�lamaz
 tek ba��na y�r�rl�kten kald�rma - uygulamak i�in "gpg --import" kullan�n
 0x%02x s�n�f� tek ba��na imza
 sadece saklar %d tipi alt paket kritik bit k�mesine sahip
 "%s" adresine g�nderme i�lemi ba�ar�l� (durum=%u)
 d�� uygulama �al��t�r�l�rken sistem hatas�: %s
 b anahtarlar bu anahtarl�ktan al�n�r metinkipi IDEA �ifre eklentisi yok
 belirtilen sertifika g�vence adresi ge�ersiz
 belirtilen imza g�vence adresi ge�ersiz
 imza do�rulanamad�.
�mza dosyas�n�n (.sig veya .asc) komut sat�r�nda verilecek
ilk dosya olmas� gerekti�ini l�tfen hat�rlay�n.
 g�vence veritaban� bozulmu�; l�tfen "gpg --fix-trustdb" �al��t�r�n.
 genel anahtar "%s" i�in bir gizli anahtar var!
 bu �ifre algoritmas� standart d���; l�tfen daha standart birini kullan�n!
 bu kay�p bir �z-imza y�z�nden meydana gelebilir
 bu ileti %s taraf�ndan kullan�lamayabilir
 bu platformda, d�� uygulamalar �al��t�r�l�rken ge�ici dosyalar gerekiyor
 �ifreli paketlerin anahtar-kimlik alanlar�n� atar zaman damgas� �eli�kili toggle genel ve gizli anahtar listeleri aras�nda yer de�i�tirir `%c' tercih �ok fazla
 pk belle�inde �ok fazla girdi - iptal edildi
 trust g�vence veritaban� hatas� g�vence veritaban�n�n %lu. kayd� %d istek t�r�nde de�il
 g�vence veritaban� kayd� %lu, istek tipi %d: okuma ba�ar�s�z: %s
 g�vence veritaban�n�n %lu. kayd�, %d t�r�nde: yazma ba�ar�s�z: %s
 g�vence veritaban� %lu kayd�: eri�im ba�ar�s�z: %s
 g�vence veritaban� %lu kayd�: yazma ba�ar�s�z (n=%d): %s
 g�vence veritaban� i�lemi �ok uzun
 g�vence veritaban�: eri�im ba�ar�s�z: %s
 g�vence veritaban�: okuma ba�ar�s�z (n=%d): %s
 g�vence veritaban�: e�zamanlama ba�ar�s�z: %s
 uid foto kimli�i g�sterilemiyor!
  %s "%s" �al��t�r�lam�yor: %s
 d�� uygulama �al��t�r�lam�yor
 d�� uygulaman�n yan�t� okunam�yor: %s
 �al��t�r�labilirlerin patikas� %s yap�lam�yor
 t�m anahtarlar� �ifrelemek i�in IDEA �ifresi kullan�lamaz.
 bak�ms�z g�vence veritaban�n�n g�ncellemesi beklenmeyen z�rh:  beklenmeyen veri tamamlanmam�� �ifre algoritmas� tamamlanmam�� genel anahtar algoritmas� bilinmeyen bilinmeyen �ifre algoritmas� bilinmeyen s�k��t�rma algoritmas� �ntan�ml� al�c� `%s' bilinmiyor
 bilinmeyen �z�mleme algoritmas� bilinmeyen paket tipi bilinmeyen s�k��t�rma algoritmas�
 bilinmeyen genel anahtar algoritmas� bilinmeyen imza s�n�f� bilinmeyen s�r�m D�� uygulamamn�n do�al olmayan ��k���
 desteklenmeyen URI genel anahtar algoritmas� kullan��s�z genel anahtar kullan�md��� gizli anahtar kullan�md��� anahtarlar� bir anahtar sunucusundan g�nceller g�ncelleme ba�ar�s�z: %s
 gizliyi g�ncelleme ba�ar�s�z: %s
 g�vence veritaban�n� g�nceller g�ncelenmi� tercihler updpref kullan�m�: gpg [se�enekler]  ��kt� dosyas� olarak kullan�l�r kurall� metin kipini kullan�r onu �nce "--delete-secret-keys" ile silmelisiniz.
 �ntan�ml� al�c� olarak �ntan�ml� anahtar kullan�l�r gpg-agent kullan imzalamak ya da �ifre ��zmek i�in bu kullan�c� kimli�i kullan�l�r kullan�c� kimli�i "%s" zaten iptal edilmi�ti
 Kullan�c� kimli�i: " yard�mc� anahtar %08lX, as�l anahtar %08lX yerine kullan�l�yor
 �ok detayl� bir imzay� do�rular anahtar zay�f zay�f anahtar olu�turuldu - yeniden deneniyor
 bir �ifreli oturum anahtar� (%d) i�in tuhaf uzunluk
 do�rudan imza yaz�l�yor
 anahtar� garantileyen imzay� yaz�yor
 genel anahtar� `%s'e yaz�yor
 gizli anahtar� `%s'e yaz�yor
 �z-imza yaz�l�yor
 "%s"e yaz�yor
 standart ��kt�ya yaz�yor
 yanl�� gizli anahtar kullan�lm�� eE evet --pgp2 kipinde sadece PGP 2.x tarz� anahtarlarla a��k imzalama yapabilirsiniz
 --pgp2 kipinde sadece PGP 2.x tarz� anahtarlarla ayr�k imza yapabilirsiniz
 --pgp2 kipinde sadece 2048 bitlik RSA anahtarlar� ile �ifreleme yapabilirsiniz
 --pgp2 kipinde sadece ayr�k veya sade imzalar yapabilirsiniz
 --pgp2 kipinde ayn� anda hem imzalama hem de �ifreleme yapamazs�n�z
 bir anahtar� kendisini y�r�rl�kten kald�racak anahtar olarak kullanamazs�n�z
 bir yaz�l�m hatas� buldunuz ... (%s:%d)
 %2$s kipindeyken %1$s kullan�lamayabilir.
 --pgp2 ile �al���rken veri yolu yerine dosyalar� kullanmal�s�n�z.
 |FD|durum bilgisini bu FD'ye yazar |DOSYA|geni�letme mod�l� olarak DOSYA y�klenir |MAK�NA|anahtarlar� aramak i�in bu anahtar sunucusu kullan�l�r |ANHK�ML|bu anahtar son derece g�venli |�S�M|�S�M i�in �ifreleme yapar |�S�M|terminal karakter setini �S�M olarak ayarlar |�S�M|�ntan�ml� al�c� olarak �S�M kullan�l�r |�S�M|�ntan�ml� gizli anahtar olarak �S�M kullan�l�r |�S�M|�ifre algoritmas� olarak �S�M kullan�l�r |�S�M|anahtar parolalar� i�in �ifre algoritmas� olarak �S�M kullan�l�r |�S�M|�z�mleme algoritmas� olarak �S�M kullan�l�r |�S�M|anahtar parolalar� i�in ileti �z�mleme algoritmas� olarak �S�M kullan�l�r |N|s�k��t�rma seviyesi N olarak ayarlan�r (0 ise s�k��t�rma yap�lmaz) |N|s�k��t�rma algoritmas� olarak N kullan�l�r |N|anahtar parolas� kipi olarak N kullan�l�r |[dosya]|a��k�a okunabilen bir imza yapar |[dosya]|bir imza yapar |[DOSYA]|durum bilgisini DOSYAya yazar |[dosyalar]|dosyalar�n �ifresi a��l�r |[dosyalar]|dosyalar �ifrelenir |algo [dosyalar]|ileti �z�mlemelerini g�sterir 