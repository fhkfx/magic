��         |   e  �@      pV  -   qV  #   �V  )   �V  %   �V  �   W    �W     �X  1   �X  0   �X      Y  >   >Y  =   }Y  �   �Y  ;   �Z     �Z     �Z     �Z     [     )[     F[     c[  D   �[  .   �[  I   �[  8   >\     w\     �\     �\     �\     �\     �\     ]  "   1]     T]  #   t]     �]     �]     �]     �]  $   
^  &   /^  ,   V^     �^     �^     �^     �^     �^     _     %_     7_     M_     b_     |_  
   �_     �_     �_     �_     �_     �_  %   �_     $`  
   @`     K`     _`     |`  +   �`  #   �`     �`  -   �`  %   !a  ]   Ga  Z   �a  H    b      Ib     jb     �b  /   �b  (   �b  .   �b  3   c  "   Qc  )   tc     �c     �c     �c     �c     �c     d      d     >d     Qd     jd     �d     �d  "   �d  )   �d     e     
e     %e     >e     Te     te     �e     �e     �e     �e     �e  "    f  %   #f  &   If  !   pf  %   �f  "   �f  #   �f  '   �f      'g     Hg     eg     zg     �g     �g     �g  (   �g  $   �g     #h     8h  #   Lh     ph     �h     �h  #   �h     �h  &   	i  %   0i  ,   Vi  4   �i     �i     �i     �i     j     j     +j     Aj  '   Vj     ~j     �j     �j     �j     �j     �j  "   k     *k     Hk  -   Yk  (   �k  0   �k  H   �k  	  *l     4m     Am     Nm  �   lm  �   n  A   �n  /   o  0   ?o  \   po  1   �o     �o  C   p  )   Xp  -   �p  0   �p  .   �p  J   q  '   [q     �q     �q     �q     �q     �q  3   �q  4   !r  -   Vr  �   �r  .   >s  .   ms     �s  	   �s  	   �s     �s  .   �s  :   �s     1t     At     _t  !   {t  *   �t  1   �t     �t  #   u  &   7u  "   ^u  &   �u     �u     �u     �u     �u  <   �u  0   2v  '   cv     �v  0   �v  '   �v  6   w  G   ;w  @   �w  >   �w  +   x  =   /x     mx     �x     �x  "   �x  :   �x     	y     y     .y      Ay  /   by  �   �y     cz  H   }z  -   �z  ,   �z     !{  
   :{     E{  8   X{  #   �{     �{     �{  "   �{  �   �{  E   �|  �   �|  9   j}  ;   �}  �   �}     �~     �~     �~  ;   �~  $   (     M     a     y     �  �   �  �   I�     �     *�     D�     _�     x�     ��     ��     ��     Á  %   ځ      �  S   �  �   \�      �  G   �  !   \�  3   ~�  <   ��     �  "   
�  ,   -�  %   Z�  ,   ��  %   ��  2   ӄ     �      "�  (   C�      l�  
   ��  *   ��     Å     Յ     ��     �     $�  ,   6�     c�     }�  
   ��  �   ��  "   2�     U�     r�     ��  d   ��     	�     �  �   ;�  E   و  o   �      ��      ��  7   щ  '   	�  c   1�  /   ��  E   Ŋ  ,   �  )   8�  #   b�  -   ��  )   ��  �   ދ    ��  '   ȍ  �   ��     ��     ��     ��     ��     ǎ     ݎ  1   �     �  (   +�  %   T�     z�  %   ��     ��     ڏ     ��  9   �     =�     ]�     x�     ��     ��      ��  }  ސ     \�     k�  /   ��    ��  "   ӕ     ��     �     �     1�  *   N�     y�     ��  J   ��  j   �  �   W�  9   �  O   �  �   n�  5   <�  .   r�  '   ��    ə  �   ښ     ��     ��     ћ  !   �     	�  "   %�  '   H�  �   p�     e�  -   ��     ��  �   ŝ     ��     ��  *   ў  +   ��     (�     >�      W�     x�     ��  %   ��     ҟ  7   �     $�  r   ��  1   �  =   I�  I   ��  4   ѡ     �  #   %�  =   I�     ��  ?   ��  D   �  D   ,�  1   q�  %   ��  5   ɣ  A   ��  E   A�  H   ��  -   Ф  H   ��  �   G�     ѥ  <   �  )   )�  C   S�  =   ��  .   զ  F   �  H   K�  2   ��  1   ǧ  9   ��  ;   3�  %   o�  '   ��      ��  2   ި     �  !   �  h  4�  *   ��  &   ȯ  1   �  #   !�  2   E�  >   x�  �   ��      i�  =   ��  0   ȱ  M   ��  "   G�  &   j�  3   ��  f   Ų  %   ,�  D  R�  ,   ��  5   ĵ     ��     
�  _   �     {�  
   ��  
   ��     ��  
   ��     ��     ζ     ڶ  %   �     �  S   9�  5   ��  4   ÷     ��     �     �     0�  (   >�  #   g�     ��     ��  
   ��     ��     ��  1   ͸     ��  
   �     �     5�     R�  E   n�     ��     ҹ     ڹ     �     �     ��     	�     �     '�     5�     K�     b�     i�     ��     ��     ��     ��     ʺ      �     �      �  +   <�  !   h�  '   ��  (   ��  %   ۻ  2   �  '   4�     \�     o�     ��     ��     ��     Ǽ  :   ޼  >   �  ?   X�  ;   ��  "   Խ     ��     �  #   (�     L�  5   a�     ��     ��  ;   ��  <   ��     9�     P�     f�     |�     ��  A   ��  &   ٿ      �     �     -�  -   <�  ;   j�  %   ��  (   ��  +   ��     !�     7�     V�  1   r�     ��     ��     ��     ��     ��      �     �     )�     8�     V�     ]�     f�     m�  3   t�     ��     ��     ��     ��     ��     �  %   .�     T�     [�     h�     �      ��  3   ��  $   ��  =   �  %   D�     j�      ��     ��     ��     ��     ��     �  '   .�     V�     q�  &   ��  &   ��     ��     ��     �     -�     4�     @�     \�  K   y�  "   ��  %   ��  $   �     3�     D�     V�     h�     t�     ��     ��     ��     ��     ��     ��     �  E   �  @   `�  @   ��     ��     ��     ��  !   �  D   .�  +   s�  /   ��     ��     ��     ��     ��     �  %   &�  %   L�  $   r�     ��     ��     ��  .   ��  '   ��     $�     =�     Y�  3   v�     ��     ��     ��     ��     �     �  $   )�  &   N�  $   u�  '   ��     ��  ,   ��  '   �     4�     C�  M   G�  N   ��     ��  '   ��  /   "�  "   R�     u�      ��      ��     ��     ��     �  -   1�  0   _�  *   ��  '   ��  #   ��  1   �  %   9�  .   _�  ,   ��  &   ��  "   ��  0   �  9   6�  8   p�  2   ��  "   ��  %   ��  !   %�     G�  >   e�  3   ��  %   ��  (   ��  2   '�  2   Z�     ��  $   ��     ��  ;   ��  '   #�  #   K�  $   o�  $   ��  +   ��  .   ��  ;   �     P�  $   p�  7   ��  3   ��     �     �  $   9�  5   ^�  S   ��  9   ��  ,   "�  <   O�     ��     ��  G   ��  H   �     L�  B   \�  "   ��     ��     ��     ��     �     $�     A�     \�     |�  0   ��  ;   ��  2   �     ;�     =�     L�     Q�  	   g�     q�     ��  !   ��     ��     ��     ��     �     �     #�  '   =�     e�     ��  .   ��     ��  (   ��  )   ��  ,   '�     T�     W�     u�  
   ��     ��  $   ��     ��     ��  )   ��      �     >�  %   ]�     ��  &   ��  "   ��     ��  4   ��     .�     >�  !   N�     p�     ��     ��  %   ��  %   ��     �     (�     6�     I�     W�      e�  #   ��     ��     ��  &   ��  )   ��     
�  <   )�     f�     |�     ��  ,   ��     ��     ��      �  ;   �  >   Y�  G   ��     ��     ��      �     �     <�     T�     p�     ��  "   ��  -   ��  ,   ��     �  +   (�  *   T�  8   �  9   ��     ��  !   �  &   4�  $   [�     ��     ��  5   ��     ��     ��     ��     ��  I   ��     C�     S�     j�     }�     ��     ��  #   ��  #   ��     �     )�  !   8�     Z�  %   w�     ��     ��     ��     ��     ��     ��     ��     �     �     �     �     -�  &   M�     t�     ��  #   ��     ��     ��  3   ��  %   ,�  %   R�  =   x�  =   ��     ��     �     �     !�     /�  %   >�  	   d�     n�     w�  
   |�     ��  $   ��     ��     ��     ��     ��  &   �     4�  "   O�     r�     ��     ��     ��  V   ��      �  5   3�  $   i�     ��  +   ��  #   ��  4   ��  %   .�  
   T�  *   _�  $   ��  0   ��     ��     ��     �  &   �  .   2�  *   a�  �   ��  :   �  +   X�  E   ��  .   ��  /   ��  %   )�  A   O�  &   ��     ��     ��  ,   ��     ��  (   �     C�     I�  -   ^�  /   ��  ,   ��  "   ��  )   �     6�     U�      p�     ��     ��     ��     ��  #   ��  -   �     =�  I   \�      ��     ��     ��     ��     �     '�     /�     H�     c�     ��     ��     ��     ��     ��     ��  #   �     3�     C�     ]�     q�      ��     ��     ��     ��     ��     �     	�     �     2�  6   J�  (   ��     ��  #   ��      ��  
   �  7   �     D�     L�     _�     h�  -   ��     ��     ��     ��     �     $�     <�     M�     `�     v�     y�  D   }�  F   ��  E   	�  D   O�  A   ��  7   ��     �  $   +�  7   P�  <   ��  7   ��  F   ��      D�      e�  '   ��      ��     ��  "   ��  #   	�  $   -�     R�  /   r�  '   ��  7   ��  $   �     '�     C�  #   \�     ��  !   ��     ��     ��  #   ��    �  9   ,  5   f  5   �  7   �  �   
   �    � =   � ?   0 $   p J   � L   �   - B   1    t !   � "   � "   � "   � "    "   B N   e 6   � J   � ;   6 "   r    � "   � "   �    �    	 $   ! =   F <   � E   � #   	 @   +	 $   l	 &   �	    �	    �	    �	 "    
    #
    B
    `
    �
 "   �
    �
    �
    �
    �
        4    @    R 	   n    x    � "   �    �    �    � "       ) .   : %   i    � /   � &   � g    I   l <   �    � %       9 ;   T -   � ;   � H   �    C (   ^ (   �    �    �    �    �    
 *        K    ` #   w    �     �    � +   �         !   =    _ $   z    �    �    �    �        . #   I #   m .   � '   � "   � +    3   7 3   k +   �     � #   � -       > #   Z    ~ 0   � 2   �    �        / %   N (   t *   � 3   �    � %   
 %   0 8   V B   �    �    � (   �         1    O    n +   �    �    �    �    �        % -   8 %   f    � 5   � .   � C    H   K G  � 
   �    � #   � �    �   � A   � 0   
 :   ; }   v C   �    8 I   S ,   � !   � &   � $    D   8 %   } "   �    �     � $        *  8   1  _   j  B   �  �   !     �! &   �!    �!    
"    "    "" 5   ." 6   d"    �"    �"    �" 5   �" ;   (# G   d# )   �# $   �# '   �#    #$ $   @$    e$    z$    �$    �$ B   �$ +   �$ &   (%    O% /   o% *   �% @   �% 8   & H   D& 0   �& '   �& K   �&    2'    H' !   f' )   �' 9   �'    �'    �'    �'    ( D   '( �   l(    .)    F) -   f) .   �) &   �)    �)    �) ,   *     ?*    `* 	   �* >   �* q   �* 6   ;+ �   r+ N   , |   T, �   �,    �- (   �- &   �- L   . ,   Z.    �.    �.    �. %   �. �   �. �   �/    �0 .   �0 &   �0    1     1    ;1    R1    f1    ~1 @   �1    �1 e   �1 �   O2 )   �2 O   3 %   i3 5   �3 I   �3     4 *   04 ;   [4 /   �4 A   �4 <   	5 4   F5 )   {5 1   �5 9   �5 6   6 
   H6 9   S6    �6    �6    �6    �6 )   �6 ,   7 .   =7 1   l7    �7 s   �7 +   8 +   J8    v8 (   �8 �   �8    L9 "   ]9 �   �9 S   M: G   �: &   �:    ; 4   %; 1   Z; n   �; 3   �; =   /< 6   m< $   �< 9   �< #   =    '= '  E= ,  m> (   �? �   �?    z@    �@    �@ $   �@    �@    �@ 4    A    5A 3   MA B   �A 3   �A F   �A 7   ?B =   wB    �B L   �B %   C    @C    YC    kC #   {C +   �C    �C    �E    �E .   �E )  (F *   RI /   }I    �I    �I    �I -   J    1J    EJ P   YJ �   �J �   CK H   BL ?   �L �   �L -   �M ,   �M /   N 4  8N �   mO    4P #   MP #   qP    �P    �P .   �P .   Q 5  2Q     hR /   �R    �R �   �R "   �S '   �S 0   T 1   ET    wT B   �T 6   �T 5   U /   DU -   tU %   �U 9   �U �   V {   �V >   %W @   dW I   �W A   �W 9   1X &   kX L   �X $   �X N   Y H   SY |   �Y 7   Z *   QZ H   |Z y   �Z d   ?[ L   �[ @   �[ F   2\ �   y\ -   X] 9   �] 8   �] E   �] E   ?^ 4   �^ r   �^ |   -_ B   �_ D   �_ a   2` `   �` 9   �` 7   /a 6   ga U   �a ?  �a *   4c   _c +   rh    �h 3   �h 9   �h C   (i %   li }   �i -   j C   >j /   �j G   �j '   �j ;   "k H   ^k �   �k >   ,l �  kl /   Gn 8   wn    �n    �n j   �n !   8o    Zo    lo    zo    �o    �o 
   �o    �o 4   �o 0   p d   Cp 5   �p -   �p %   q #   2q    Vq 3   hq "   �q *   �q    �q    �q    �q    �q !   r )   &r    Pr    fr -   vr -   �r 5   �r U   s ,   ^s    �s    �s    �s    �s    �s    �s    �s    t    )t    Et    Xt 4   `t    �t    �t $   �t    �t    �t '   u '   <u    du /   �u &   �u .   �u -   v 9   6v 7   pv /   �v    �v    �v    w    -w ,   Kw &   xw L   �w G   �w L   4x E   �x -   �x    �x !   y 4   6y    ky I   �y    �y    �y 9   z e   Jz    �z    �z    �z    �z    { M   { 3   l{ !   �{    �{    �{ =   �{ P   0| (   �| /   �| 5   �|    }     %} !   F} G   h} .   �} 	   �} !   �}    ~    %~    =~    Q~ -   d~ ,   �~    �~    �~    �~    �~ :   �~         /   / /   _    �    � "   �    �    � (    �    )� )   <� ?   f� 0   �� A   ׀ &   �    @� &   Y� !   �� 2   ��    Ձ    �    � +   %� "   Q� #   t� 9   �� 5   ҂     �    )� )   ?�    i�    n�     ��    �� L   �� 1   � /   @� *   p�    ��    ��    τ    �    ��    �    /�    M� !   e� 3   �� +   �� +   � E   � B   Y� B   ��    ߆    �    � *   � F   6� (   }� 0   ��    ׇ    ݇    �    �    !� .   :�    i� 3   ��    ��    ʈ !   � -   � $   2� 3   W� '   ��     �� F   ԉ "   � !   >� #   `�    ��    ��    ��    Ɗ ,   �    � =   3�    q� 3   �� 3   ��    �    � x   � z   ��    � A   !� 9   c� '   �� (   ō :   � "   )� $   L� 5   q�    �� 4   Ǝ A   �� @   =� '   ~� .   �� C   Տ 2   � 0   L� >   }� '   �� ,   � :   � B   L� C   �� 8   ӑ .   � 0   ;� (   l� $   �� d   �� F   � *   f� ;   �� 3   ͓ ;   � -   =� 8   k� 9   �� >   ޔ 6   � ,   T� 7   �� )   �� 7   � :   � b   V� ,   �� %   � =   � 8   J�     �� 2   �� +   ח D   � �   H� <   ˘ 3   � =   <� (   z� #   �� r   Ǚ q   :�    �� `   Ś <   &� %   c�    �� ,   �� 2   ʛ 0   �� /   .� 2   ^� '   �� 2   �� K   � B   8�    {�    }�    �� +   ��    ��    Ν    �    �    �    8�    S�    k�    z� *   �� -   �� :   ۞    � 1   (� '   Z� /   �� ?   �� ,   �    � (   "� 
   K� 
   V� %   a� +   �� *   ��    ޠ *   � ,   � )   :� .   d� #   �� 1   �� (   �    � E   '�    m� (   �� !   �� 6   Ѣ    � %   #� 1   I� -   {�    ��    ɣ    ڣ    ��    � #   !� .   E� 	   t�    ~� )   �� =   ��    � :   �    J�    a� >   � ;   ��    ��    � 1   � >   E� B   �� E   Ǧ $   �    2�    8� "   W� "   z� .   �� $   ̧    � +   �� &   $�    K�     j� +   �� B   �� <   �� >   7� *   v� ,   �� 1   Ω =    �    >�    U� B   m�    ��    ��    ��    �� s   Ѫ    E�    W�    o�    ~� &   ��    �� -   ҫ ,    �    -�    K� %   `� &   �� /   ��    ݬ    �     � (   �    A�    W�    ]�    c�    |�    ~�    ��    �� %   �� 1   ܭ    � #   %�    I� "   [� 6   ~� -   �� 5   � <   � 4   V�    ��    ��    �� !   ��    � 2   ��    '�    ,�    2�    9� %   T� F   z� ;   �� !   ��    � 1   :� E   l� :   �� )   �� &   �    >�    F�    \� �   x� ,   �� 8   )� +   b�    �� >   �� *   �� G   �     `�    �� -   �� -   ϴ 7   ��    5�    7�    W� $   `� 5   �� 0   �� �   � G   m� 7   �� J   �� /   8� 9   h� 4   �� N   ׷ 1   &�    X�    l� ;   r�    �� ;   ȸ    �    � 6   #� @   Z� ?   �� >   ۹ :   � %   U� ,   {� 2   �� 2   ۺ    �    �     0� *   Q� 8   |� 8   �� I   � (   8�    a�    {� -   �� C   ��    �    
�    '� !   C�    e�    ~�    ��    ��    ͽ    � 5   ��    *� "   C�    f�    �� '   ��     Ǿ 4   �    �    9�    V�    ]�    t�    �� @   �� '   ߿ (   � -   0� 9   ^�    �� :   ��    ��    
�    � 7   *� 6   b�    ��    ��     ��    ��    �    4� #   C� '   g�    ��    �� E   �� O   �� G   ,� N   t� >   �� 8   � +   ;� !   g� +   �� /   �� +   �� 4   � )   F� *   p� -   �� 2   �� %   �� *   "� ,   M� 9   z� .   �� 4   �� )   � *   B�    m� #   ��    �� '   ��    �� $   �    5�    U�    q�    �  S      �  )  �  g   �  �  x   �  F  
              v    �  -  5       �   �  -  k   �      n          �  �                 �  /       �  �  3          �              Z        3  �      �       K          J       �       %  i      +  <      �  N  �  �  �      �       �  l  �              �                  p          ]   �  �  �  �        �   �            �  *  �  �      �  �  �          r          �      .       #  �   �  �  �          E          �   �  �   �  �      [  �  6  �  �  A     E   �  �      U   �      �  �  �       �           �  *          �      �  �         �  �       8  �  �  2  3  �  !  v         �             �         �      
  B  �  �  �  X   �   �  �  �                 1  �  �  �  �      �   �   i                               F  �       Z   �  �  �   �       �  �  U    j  �   L        T  �            �  �       N    �  �  c  '      �          f  �      L  �  �    �  �  I                        O  >  �      �  �  	          K  Y  $  �  �  �     �              `  �  �   $      N  �  ,   �  �  �  �  ~  :   |  z          �   �     �  �          v      �  _  �   �      �      �  q  �  �  �  u       F  �  '  T  y  0      j  �  y   �  �  �  �  %      r      �  �    �        �  �  �   "  �  �        +  �          X  �  �   �  r  [  �  �               �   ^       �  &  
      '  �     t      9  ?      �  �  o  +      �  �   �  �   �  �  �   �  �  M  w  �       �          Z      9      =  e      �  p  W   \           �  �  �  �  /  �     �   �          W      '   �  �       �  �  �  >   �      �  �  n  `      �       �  @  a  �    U      �  �      0   �  �  �  v  x  s  #      �   6      )  �   Y  ?  q              �  �  �          y  �      	   P  ]  G         �  �   8      �  x  �  e    =   �   �          �        �  �  �  �    b  �           �      �       �  O      �    �      �  E      �  �  M   �      �   �   :  �  7      5  �  �  S  �      �  @  Y   �    u  �          �  �   �  3  �  	  �      �   �  �   ]  (  �  �      d      l  $   4  o      |               �  P   V      S  �      m  �                  �  Q  �       l           #             >        5  �       I  ~      �    }      �      �  �      H  �  �  #  P                 �  �   �      �  �   �  �      �                  �  X      b      �  �   [         L      d      �  (      2   �   �        �  �  k          R  �         a   A  �    �   �  �        g  L   {  m   �       �   �  w  �  O  {      �   ~   /  6      �   �  &    �             �  �          �  g  s  
   8   <   �      1  J    	  �      �  �    *      �  �  s   �    �      �  f   �   M  �  �   �              �    �      0      �  �  ,  R  �   G  ;    4  z      �        �      H      �  �  -   e  �  �  �   :  <           _  �   o  �  �  �  �              H  �         R   b   �  �  a        �  {   ,       }  �       �    B  �  �   j       N         �        T           p   �  �  �      �         �  �   �  �  �   @  �      �  �      _      ;  "      w       i          �  �  p  �  �   �           q     8      �  �             �  �  C  �   A  `  �  W  �  �                 G   �      �  �     �  \  &  �        k      �        <  7      S   �   c  �       
    t                      |      �   �  m  �     �  �  �  �  4   j      �      �    �  �  �       t    �  1  ,      �   �  ^    �  �  	      �  �  i  ?      )   �  F       $  �       �  �   h      �        �       "   K             .        �          ]          �  �    �  �       �      +   %   n  f  Z  �  �      z      �    �  }   �  s  x        �          �   ?   �  5  �  �      Q   �  �                �   l  �          d   �   }  �      /  C       T  �  "          t   �    �    �  D  �  (   �   d  �  �  :              �    )  ^      �  �   Y  n   h   �  V  �       P  �  �  �  J    4      �  7      f  �      �  2      �  C      �  b  M  �  G  �  D  �       �  �  �     *   B  �      �     �  {  k      �   �   �  �  h  @   q  �     �  �  ^  �  �  !       1       `   �     =  >  �   �              a      �  �    �  u      �  �   \        _       �   A  �  H             Q  �   �  �   �        �         I  !      X          �  6   �       R  E              �    �   �  .    K  �  (  �  9  |      J  �    �      �  �                   u  �  �       �  �      �  �  �   �   7  �      h  V  �       �  �      ;   c  ~  �  D   =          �    �  �  �   w  ;  �  �  �     D  &                 �   �     Q  �  �   �  �  e   �  9       �   �   C  �      B   V   r                   �  0      [      �  �   �   �      �  .      �  �  g  �  2      �   W  �   �     �  O        m  o   �  �  y      �  z      �   !              I  c   \  �          -  U  %  �    
Enter the user ID.  End with an empty line:  
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Key fingerprint =      Subkey fingerprint:    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
    signed by %08lX at %s%s
    signed by %08lX at %s%s%s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  Primary key fingerprint:  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" is not a JPEG file
 "%s" was already locally signed by key %08lX
 "%s" was already signed by key %08lX
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d keys processed (%d validity counts cleared)
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s ...
 %s does not expire at all
 %s encrypted data
 %s encryption will be used
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s signature from: "%s"
 %s signature, digest algorithm %s
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't make lock
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  ASCII armored output forced.
 About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to appoint this key as a designated revoker? (y/N):  Are you sure you want to use it (y/N)?  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Cipher:  Command>  Comment:  Compression:  Create a revocation certificate for this key?  Create a revocation certificate for this signature? (y/N)  Create anyway?  Critical signature notation:  Critical signature policy:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Displaying %s photo ID of size %ld for key 0x%08lX (uid %d)
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Experimental algorithms should not be used!
 Expired signature from " Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid passphrase; please try again Invalid selection.
 Is this correct (y/n)?  Is this okay?  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key available at:  Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: Elgamal primary key detected - this may take some time to import
 NOTE: This key is not protected!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Policy:  Primary key fingerprint: Pubkey:  Public key is disabled.
 Quit without saving?  Real name:  Really create the revocation certificates? (y/N)  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really revoke all selected user IDs?  Really revoke this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Really update the preferences?  Reason for revocation: %s
 Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature expired %s
 Signature expires %s
 Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Signature notation:  Signature policy:  Syntax: gpg [options] [files]
Check signatures against known trusted keys
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The use of this algorithm is only supported by GnuPG.  You will not be
able to use this key to communicate with PGP users.  This algorithm is also
very slow, and may not be as secure as the other choices.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key  This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To be revoked by:
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 Unable to open photo "%s": %s
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) Use this key anyway?  User ID "%s" is expired. User ID "%s" is not self-signed. User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: appointing a key as a designated revoker cannot be undone!
 WARNING: digest `%s' is not part of OpenPGP.  Use at your own risk!
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: no user ID has been marked as primary.  This command may
              cause a different user ID to become the assumed primary.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: secret key %08lX does not have a simple SK checksum
 WARNING: signature digest conflict in message
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy url (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe enclosing directory ownership on %s "%s"
 WARNING: unsafe enclosing directory permissions on %s "%s"
 WARNING: unsafe ownership on %s "%s"
 WARNING: unsafe permissions on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [expired]  [filename] [revocation] [revoked]  [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid long keyID
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption anonymous recipient; trying secret key %08lX ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %08lX due to an unknown critical bit
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet binary build_packet failed: %s
 c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't get server read FD for the agent
 can't get server write FD for the agent
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open file: %s
 can't open signed data `%s'
 can't open the keyring can't put a policy URL into v3 (PGP 2.x style) signatures
 can't put a policy URL into v3 key (PGP 2.x style) signatures
 can't put notation data into v3 (PGP 2.x style) key signatures
 can't put notation data into v3 (PGP 2.x style) signatures
 can't query password in batchmode
 can't read `%s': %s
 can't search keyserver: %s
 can't set client pid for the agent
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking at depth %d signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 checking created signature failed: %s
 checking keyring `%s'
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 cipher extension "%s" not loaded due to unsafe permissions
 communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID deleting keyblock failed: %s
 delkey delphoto delsig deluid digest algorithm `%s' is read-only in this release
 disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error finding trust record: %s
 error in trailer line
 error reading `%s': %s
 error reading keyblock: %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 error: invalid fingerprint
 error: missing colon
 error: no ownertrust value
 expire export keys export keys to a key server export the ownertrust values external program calls are disabled due to unsafe options file permissions
 failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing digest algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 help iImMqQsS import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid symkey algorithm detected (%d)
 invalid value
 key key %08lX has been created %lu second in future (time warp or clock problem)
 key %08lX has been created %lu seconds in future (time warp or clock problem)
 key %08lX incomplete
 key %08lX marked as ultimately trusted
 key %08lX occurs more than once in the trustdb
 key %08lX: "%s" %d new signatures
 key %08lX: "%s" %d new subkeys
 key %08lX: "%s" %d new user IDs
 key %08lX: "%s" 1 new signature
 key %08lX: "%s" 1 new subkey
 key %08lX: "%s" 1 new user ID
 key %08lX: "%s" not changed
 key %08lX: "%s" revocation certificate added
 key %08lX: "%s" revocation certificate imported
 key %08lX: HKP subkey corruption repaired
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted as trusted key
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: direct key signature added
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid self-signature on user id "%s"
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey binding signature
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey binding
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: this is a PGP generated ElGamal key which is NOT secure for signatures!
 key %08lX: unexpected signature class (0x%02X) - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key `%s' not found: %s
 key export failed: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key incomplete
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keyserver receive failed: %s
 keyserver refresh failed: %s
 keyserver search failed: %s
 keyserver send failed: %s
 keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l line too long
 list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never      never use a MDC for encryption new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition "%s"
 no corresponding public key: %s
 no default secret keyring: %s
 no entropy gathering module detected
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for `%s'
 no secret key
 no secret subkey for public subkey %08lX - ignoring
 no signed data
 no such user id no ultimately trusted keys found
 no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again passphrase too long
 passwd please do a --check-trustdb
 please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 please use "%s%s" instead
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent - disabling agent use
 problem with the agent: agent returns 0x%lx
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key does not match secret key!
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read error: %s
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring requesting key %08lX from %s
 resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke a user ID revoke signatures revsig revuid rounded up to %u bits
 s save save and quit search for keys on a key server searching for "%s" from HKP server %s
 secret key `%s' not found: %s
 secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key locally and non-revocably sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user id "%s"
 sorry, can't do this in batch mode
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 system error while calling external program: %s
 t take the keys from this keyring textmode the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this keyserver does not support --search-keys
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temp files when calling external programs
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to display photo ID!
 unable to execute %s "%s": %s
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown protection algorithm
 unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you may not use cipher algorithm "%s" while in %s mode
 you may not use compression algorithm "%s" while in %s mode
 you may not use digest algorithm "%s" while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg-1.2.2
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-06-19 18:58+0200
Last-Translator: Janusz A. Urbanowicz <alex@bofh.net.pl>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-2
Content-Transfer-Encoding: 8bit
Xgettext-Options: --default-domain=gnupg --directory=.. --add-comments --keyword=_ --keyword=N_ --files-from=./POTFILES.in
Files: util/secmem.c util/argparse.c cipher/random.c cipher/rand-dummy.c cipher/rand-unix.c cipher/rand-w32.c g10/g10.c g10/pkclist.c g10/keygen.c g10/decrypt.c g10/encode.c g10/import.c g10/keyedit.c g10/keylist.c g10/mainproc.c g10/passphrase.c g10/plaintext.c g10/pref.c g10/seckey-cert.c g10/sig-check.c g10/sign.c g10/trustdb.c g10/verify.c g10/status.c g10/pubkey-enc.c
 
Identyfikator u�ytkownika (pusta linia oznacza koniec):  
To�samo�� u�ytkownika zosta�a sprawdzona pobie�nie.
 
To�samo�� u�ytkownika zosta�a dok�adnie sprawdzona.
 
To�samo�� u�ytkownika nie zosta�a w og�le sprawdzona.
 
Brakuje mo�liwo�ci wygenerowania odpowiedniej liczby losowych bajt�w.
Prosz� kontynuowa� inne dzia�ania aby system m�g� zebra� odpowiedni�
ilo�� entropii do ich wygenerowania (brakuje %d bajt�w).
 
Wybierz zdj�cie kt�re chcesz do��czy� do swojego klucza jako identyfikator.
Musi to by� plik w formacie JPEG. Zostanie on zapisany w Twoim kluczu
publicznym. Je�li b�dzie du�y, powi�kszy to tak�e rozmiar Twojego klucza!
Dobry rozmiar to oko�o 240 na 288 pikseli.
 
Obs�ugiwane algorytmy:
 
Podpis zostanie oznaczony jako prywatny (nieeksportowalny).
 
Podpis zostanie oznaczony jako nie podlegaj�cy uniewa�nieniu.
 
To b�dzie podpis klucza nim samym.
 
OSTRZE�ENIE: podpis zostanie oznaczony jako prywatny (nieeksportowalny).
 
OSTRZE�ENIE: podpis zostanie oznaczony jako nie podlegaj�cy uniewa�nieniu.
 
Musisz poda� identyfikator u�ytkownika aby mo�na by�o rozpozna� tw�j klucz;
program z�o�y go z twojego imienia i nazwiska, komentarza i adresu poczty
elektronicznej. B�dzie on mia�, na przyk�ad, tak� posta�:
    "Tadeusz �ele�ski (Boy) <tzb@ziemianska.pl>"

 
Musisz poda� has�o aby odbezpieczy� klucz prywatny u�ytkownika:
"                         alias "          do��czono do zbioru: %lu                    bez zmian: %lu
             nowych podkluczy: %lu
       nowych identyfikator�w: %lu
       nie w��czono do zbioru: %lu
           bez identyfikatora: %lu
              Nie ma pewno�ci co do to�samo�ci osoby kt�ra z�o�y�a ten podpis.
              Ten podpis prawdopodobnie jest FA�SZYWY.
              Nie ma pewno�ci co do to�samo�ci osoby kt�ra z�o�y�a podpis.
              To mo�e oznacza� �e podpis jest fa�szerstwem.
              nowych podpis�w: %lu
        Odcisk podklucza:    tajnych kluczy wczytanych: %lu
    pomini�tych nowych kluczy: %lu
      Odcisk klucza =       Odcisk podklucza:    (%d) DSA (tylko do podpisywania)
    (%d) Para kluczy dla algorytm�w DSA i ElGamala (domy�lne)
    (%d) Klucz dla algorytmu ElGamala (tylko do szyfrowania)
    (%d) Klucz dla algorytmu ElGamala (do szyfrowania i podpisywania)
    (%d) RSA (tylko do szyfrowania)
    (%d) Klucz dla algorytmu RSA (do szyfrowania i podpisywania)
    (%d) RSA (tylko do podpisywania)
    (0) Nie odpowiem na to pytanie. %s
    (1) W og�le nie.%s
    (2) Pobie�nie.%s
    (3) Bardzo dok�adnie.%s
    nowych uniewa�nie� kluczy: %lu
 uniewa�niony przez %08lX w %s
 podpisany przez %08lX w %s%s
 podpisany przez %08lX w %s%s%s
  Nie da si� z�o�y� podpisu.
      tajnych kluczy dodanych: %lu
  %d = nie wiem
  %d = NIE ufam mu
  %d = mam pe�ne zaufanie
  %d = mam ograniczone zaufanie
  %d = ufam absolutnie
 (domy�lnie)  (podklucz %08lX)  (podpis nieeksportowalny)   (poufne)  Odcisk klucza g��wnego:  [wygasa :%s]  i = potrzebuj� wi�cej informacji
  m = powr�t do g��wnego menu
  w = wyj�cie
  p = pomini�cie klucza
     tajnych kluczy bez zmian: %lu
  zaufanie: %c/%c "
lokalnie podpisano Twoim kluczem %08lX w %s
 "
podpisano Twoim kluczem %08lX w %s
 ,,%s'' nie jest plikiem JPEG
 "%s" jest ju� lokalnie podpisany kluczem %08lX
 "%s" jest ju� podpisany kluczem %08lX
 # Lista przypisanych warto�ci zaufania, stworzona %s
# (u�yj "gpg --import-ownertrust" aby j� wczyta�)
 %08lX: Nie ma pewne, do do kogo nale�y ten klucz, ale jest akceptowalny.
 %08lX: Nie ma pewno�ci co do to�samo�ci w�a�ciciela klucza.
 %08lX: NIE UFAMY temu kluczowi
 %08lX: data wa�no�ci klucza up�yn�a
 %d niepoprawnych podpis�w
 przetworzono %d kluczy (rozwi�zano %d przelicze� zaufania)
 %d podpis�w nie sprawdzonych z powodu b��d�w
 %d podpis�w nie zosta�o sprawdzonych z powodu braku kluczy
 wykryto %d identyfikator�w u�ytkownika niepodpisanych tym samym kluczem
 %lu kluczy (%lu podpis�w)
 %lu kluczy do tej chwili (%lu podpis�w)
 %lu kluczy przetworzonych do tej chwili
 %s ...
 %s nie ma daty wa�no�ci
 dane zaszyfrowano za pomoc� %s
 zostanie u�yty szyfr %s
 %s traci wa�no��: %s
 %s nie jest poprawn� nazw� zestawu znak�w
 %s zosta� utworzony
 %s pozosta� bez zmian
 %s nie ma sensu w po��czeniu z %s!
 Nie wolno u�ywa� %s z %s!
 %s podpis z�o�ony przez: ,,%s''
 podpis %s, skr�t %s
 %s%c %4u%c/%08lX  utworzony: %s, wygasa: %s %s.
 %s/%s zaszyfrowany dla: ,,%s''
 %s: OSTRZE�ENIE: plik jest pusty
 %s: dost�p niemo�liwy: %s
 %s: nie mo�na utworzy� katalogu: %s
 %s: nie mo�na utworzy� blokady
 %s: nie mo�na utworzy�: %s
 %s: nie mo�na utworzy� blokady
 %s: nie mo�na otworzy�: %s
 %s: katalog utworzony
 %s: katalog nie istnieje!
 %s: b��d odczytu pustego wpisu: %s
 %s: b��d odczytu numeru wersji: %s
 %s: b��d przy uaktualnianiu numeru wersji: %s
 %s: b��d zapisu wpisu katalogowego: %s
 %s: b��d zapisu numeru wersji: %s
 %s: dopisanie rekordu nie powiod�o si�: %s
 %s: tworzenie tablicy skr�t�w nie powiod�o si�: %s
 %s: stworzenie zapisu o wersji nie powiod�o si�: %s %s: zerowanie rekordu nie powiod�o si�: %s
 %s: niew�a�ciwa wersja pliku %d
 %s: niepoprawny plik bazy zaufania
 %s: stworzony niepoprawny plik bazy zaufania
 %s: zbi�r kluczy utworzony
 %s: to nie jest plik bazy zaufania
 %s: pomini�ty: %s
 %s: pomini�ty: zosta� ju� wybrany w innej opcji
 %s: pomini�ty: klucz publiczny wy��czony z u�ytku
 %s: baza zaufania utworzona
 %s: nieznana ko�c�wka nazwy
 %s: wpis wersji z numerem %lu
 %s:%d jest przestarza�� opcj� ,,%s''
 %s:%d niepoprawne opcje eksportu kluczy
 %s:%d: niepoprawne opcje wczytania kluczy
 d�ugo�� %u bit�w, typ %s, numer %08lX, stworzony %s (nie podano)
 (Prawdopodobnie chcesz tu wybra� %d)
 (to jest czu�y klucz uniewazniaj�cy)
 (chyba, �e klucz zostaje wybrany przez podanie odcisku)
 (prawdopodobnie u�ywany program jest niew�a�ciwy dlatego zadania)
 --clearsign [plik]" --decrypt [plik] --edit-key nazwa u�ytkownika [polecenia] --encrypt [plik] --lsign-key nazwa u�ytkownika --nrsign-key nazwa u�ytkownika --nrsign-key nazwa u�ytkownika opcja --output nie dzia�a z tym poleceniem
 --sign --encrypt [plik] --sign --symmetric [plik] --sign [plik] --sign-key nazwa u�ytkownika --store [plik] --symmetric [plik] -k[v][v][v][c] [identyfikator] [zbi�r kluczy] ... to jest b��d programu (%s:%d:%s)
 1 niepoprawny podpis
 1 podpis nie zosta� sprawdzony z powodu braku klucza
 1 podpis nie zosta� sprawdzony z powodu b��du
 wykryto 1 identyfikator u�ytkownika niepodpisany tym samym kluczem
 @
(Pe�n� list� polece� i opcji mo�na znale�� w podr�czniku systemowym.)
 @
Przyk�ady:

 -se -r Bob [plik]          podpisa� i zaszyfrowa� kluczem Boba
 --clearsign [plik]         podpisa� z pozostawieniem czytelno�ci dokumentu
 --detach-sign [plik]       podpisa� z umieszczeniem podpisu w osobnym pliku
 --list-keys [nazwy]        pokazuje klucze
 --fingerprint [nazwy]      pokazuje odciski kluczy
 @
Opcje:
  @Polecenia:
  wymuszono opakowanie ASCII wyniku.
 Nast�pi generacja nowej pary kluczy dla algorytmu(�w) %s.
               minimalny rozmiar klucza wynosi 768 bit�w
               domy�lny rozmiar klucza wynosi 1024 bity
  najwi�kszy sugerowany rozmiar klucza wynosi 2048 bit�w
 Ten typ klucza jest zdefiniowany w RFC2440, jednak�e jest on odradzany, gdy�
nie jest obs�ugiwany przez wszystkie programy zgodne z OpenPGP, a podpisy nim
sk�adane s� du�e i ich sprawdzanie trwa d�ugo. Je�li ma zosta� wygenerowany podklucz, nale�y odpowiedzie� "tak". Aby skasowa� podklucz nale�y odpowiedzie� "tak". Je�li mo�na nadpisa� ten plik, nale�y odpowiedzie� ,,tak'' Aby skasowa� ten identyfikator u�ytkownika (co wi��e si� ze utrat�
wszystkich jego po�wiadcze�!) nale�y odpowiedzie� ,,tak''. Odpowiedz "tak", aby podpisa� WSZYSTKIE identyfikatory u�ytkownika. Odpowied� "tak" lub "nie". Czy jeste� naprawd� pewien �e chcesz podpisa� ten klucz 
swoim kluczem: " Na pewno wygenerowa� klucz takiej d�ugo�ci?  Czy dalej chcesz je doda�? (t/N)  Czy dalej chcesz go uniewa�ni�? (t/N)  Czy na pewno chcesz podpisa�? (t/N)  Czy na pewno chcesz wyznaczy� ten klucz jako uniewa�niaj�cy? (t/N):  Czy na pewno chcesz tego u�y�? (t/N)  NIEPOPRAWNY podpis z�o�ony przez " B��d sumy CRC; %06lx - %06lx
 Nie mo�na sprawdzi� podpisu: %s
 Tego klucza nie mo�na edytowa�: %s.
 Anuluj Certyfikaty prowadz�ce do ostatecznie zaufanego klucza:
 Zmieni� (I)mi�/nazwisko, (K)omentarz, adres (E)mail, przej�� (D)alej,
czy (W)yj�� z programu ?  Zmieni� (I)mi�/nazwisko, (K)omentarz, adres (E)mail, czy (W)yj��?  Przestawienie wszystkich (lub tylko wybranych) identyfikator�w na aktualne
ustawienia. Data na odpowiednich podpisach zostane przesuni�ta do przodu o
jedn� sekund�. 
 Zmiana daty wa�no�ci podklucza.
 Zmiana daty wa�no�ci g��wnego klucza.
 Symetryczne:  Polecenie>  Komentarz:  Kompresji:  Stworzy� certyfikat uniewa�nienia tego klucza? (t/N)  Stworzy� certyfikat uniewa�nienia tego podpisu? (t/N)  Stworzy� klucz pomimo to?  Krytyczne adnotacje podpisu:  Krytyczny regulamin podpisu:  Para kluczy dla DSA b�dzie mia�a 1024 bity d�ugo�ci.
 Klucz dla DSA musi mie� d�ugo�� pomi�dzy 512 i 1024 bitow.
 Algorytm DSA wymaga u�ycia algorytmu skr�tu daj�cego 160-bitowy wynik.
 zdj�cie opakowania ASCII pliku lub potoku Usun�� ten poprawny podpis? (t/N/w)  Usun�� ten niepoprawny podpis? (t/N/w)  Usun�� ten klucz ze zbioru?  Usun�� ten nieznany podpis? (t/N/w)  %d podpis usuni�ty.
 %d podpis�w usuni�tych.
 Podpis oddzielony od danych.
 Skr�t:  Zdj�cie w formacie %s, rozmiar %ld bajt�w, klucz 0x%08lX (id %d).
 Czy na pewno chcesz usun�� wybrane klucze?  Czy na pewno chcesz usun�� ten klucz?  Czy na pewno chcesz to zrobi�?  Czy na pewno chcesz uniewa�ni� wybrane klucze?  Czy na pewno chcesz uniewa�ni� ten klucz?  Czy na pewno chcesz przypisa� absolutne zaufanie temu kluczowi?  Czy chcesz zast�pi� przeterminowany podpis nowym? (t/N)  Czy chcesz zamieni� go na pe�ny, publiczny, eksportowalny podpis? (t/N)  Czy chcesz zamieni� go na podpis OpenPGP? (t/N)  Czy na pewno chcesz to podpisa�? (t/N)  Czy chcesz �eby wa�no�� Twojego podpisu wygasa�a w tej samej chwili? (T/n)  bez pokazywania zdj�� Adres poczty elektronicznej:  opakowanie ASCII pliku lub potoku Nazwa pliku ze zdj�ciem w formacie JPEG:  Wprowad� opis (nieobowi�zkowy) i zako�cz go pust� lini�:
 Nazwa pliku Has�o
 Podaj has�o:  Nazwa w�a�ciciela klucza. Wprowad� nowe d�ugie, skomplikowane has�o dla tego klucza tajnego.

 Wprowad� ��dan� warto�� (jak w znaku zach�ty).  
Mo�na tu poda� dat� w formacie ISO (RRRR-MM-DD) ale nie da to
w�a�ciwej obs�ugi b��d�w - system pr�buje interpretowa� podan� warto��
jako okres. Wprowad� rozmiar klucza Podaj adresat�w tej wiadomo�ci. Podaj identyfikator klucza uniewa�niaj�cego:  Nie nale�y u�ywa� algorytm�w do�wiadczalnych!
 Przeterminowany podpis z�o�ony przez " Ustawienia:  Plik ,,%s'' ju� istnieje.  Podaj nazw� pliku kt�rego dotyczy ten podpis Wpisz tutaj swoj� wiadomo�� ...
 Poprawny podpis z�o�ony przez " Skr�t�w:  Podpowied�: wybierz identyfikatory u�ytkownika do podpisania.
 Jak dok�adnie zosta�a przez Ciebie sprawdzona to�samo�� tej osoby?
Je�li nie wiesz co odpowiedzie�, podaj ,,0''.
 szyfr IDEA nie jest dost�pny, pr�ba u�ycia %s zamiast
 Je�li chcesz, mo�esz poda� opis powodu wystawienia certyfikatu
uniewa�nienia. Opis powinien byc zwi�z�y. 
Pusta linia ko�czy wprowadzanie tekstu.
 Je�li mimo wszystko chcesz u�y� tego uniewa�nionego klucza, odpowiedz ,,tak''. Je�li mimo wszystko chcesz u�y� tego klucza, klucza, co do kt�rego nie ma
�adnej pewno�ci do kogo nale�y, odpowiedz ,,tak''. U�ywanie tego samego klucza do podpisywania i szyfrowania nie jest dobrym
pomys�em. Mo�na tak post�powa� tylko w niekt�rych zastosowaniach. Prosz� si�
najpierw skonsultowa� z ekspertem od bezpiecze�stwa.  Niew�a�ciwy znak w komentarzu
 Niew�a�ciwy znak w imieniu lub nazwisku
 Niepoprawna komenda  (spr�buj "help")
 Opcja --allow-non-selfsigned-uid wymusi�a uznanie za poprawny klucza %08lX.
 Niepoprawne has�o; prosz� spr�bowa� ponownie Niew�a�ciwy wyb�r.
 Czy wszystko si� zgadza (t/n)?  Informacje poprawne?  Czy zdj�cie jest w porz�dku? (t/N/w)  NIE MA pewno�ci, czy klucz nale�y do osoby wymienionej w identyfikatorze.
Je�li nie masz co do tego �adnych w�tpliwo�ci i *naprawd�* wiesz co robisz,
mo�esz odpowiedzie� ,,tak'' na nast�pne pytanie.

 Te wartosci u�ytkownik przydziela wg swojego uznania; nie b�d� nigdy
eksportowane poza ten system. Potrzebne s� one do zbudowania sieci
zaufania, i nie ma to nic wsp�lnego z tworzon� automatycznie sieci�
certyfikat�w. Klucz dost�pny w:  Procedura generacji klucza zosta�a anulowana.
 Generacja klucza nie powiod�a si�: %s
 klucz zosta� skompromitowany klucz nie jest ju� u�ywany Klucz jest chroniony.
 Klucz uniewa�niony. klucz zosta� zast�piony Okres wa�no�ci klucza ? (0)  Klucz nie zosta� zmieniony wi�c zapis zmian nie jest konieczny.
 Zbi�r kluczy Klucze d�u�sze ni� 2048 bit�w s� odradzane, poniewa� potrzebne
obliczenia trwaj� wtedy BARDZO d�ugo!
 N aby zmieni� nazw� (nazwisko).
C aby zmieni� komentarz.<
E aby zmieni� adres e-mail.
O aby kontynuowa� tworzenie klucza.
Q aby zrezygnowa� z tworzenia klucza. UWAGA: %s nie jest do normalnego u�ytku!
 UWAGA: Wykryto klucz g��wny algorytmu ElGamala. Jego import potrwa jaki� czas.
 UWAGA: Ten klucz nie jest chroniony!
 UWAGA: brak algorytmu szyfruj�cego %d w ustawieniach
 UWAGA: tworzenie podkluczy dla kluczy wersji 3 jest niezgodne z OpenPGP.
 UWAGA: klucz zosta� uniewa�niony UWAGA: brak domy�lnego pliku opcji ,,%s''
 UWAGA: stary domy�lny plik opcji ,,%s'' zosta� zignorowany
 UWAGA: wa�no�� klucza tajnego %08lX wygas�a %s
 UWAGA: nadawca zaznaczy� �e wiadomo�� nie powinna by� zapisywana
 UWAGA: klucz podpisuj�cy %08lX przekroczy� dat� wa�no�ci %s
 UWAGA: prosty tryb S2K (0) jest stanowczo odradzany
 UWAGA: nie mo�na zapisywa� bazy zaufania
 Imi� lub nazwisko nie mo�e zaczyna� si� od cyfry
 Imi� i nazwisko musz� mie� conajmniej 5 znak�w d�ugo�ci.
 Do wykonania tej operacji potrzebny jest klucz tajny.
 IiKkEeDdWw Brak odpowiadaj�cego podpisu w zbiorze kluczy prywatnych
 Pomoc niedost�pna Brak pomocy o ,,%s'' nie podano przyczyny Brak podklucza o numerze %d
 Brak takiego identyfikatora u�ytkownika.
 Brak warto�ci zaufania dla:
%4u%c/%08lX %s " Brak identyfikatora u�ytkownika o numerze %d.
 To nie jest poprawny adres poczty elektronicznej
 Adnotacja:  Ten klucz nie mo�e by� wykorzystany do szyfrowania. Komend� "--edit-key"
mo�na doda� do niego podklucz szyfruj�cy.
 Uwaga: Ten klucz zosta� wy��czony z u�ytku
 Uwaga: Data wa�no�ci tego klucza up�yn�a!
 Nic nie zosta�o usuni�te.
 Nie ma nic do podpisania kluczem %08lX.
 Nale�y tak�e pami�ta� o tym, �e informacje mog� by� te� wykradzione z
komputera przez pods�uch emisji elektromagnetycznej klawiatury i monitora!
 Nadpisa� (t/N)?  Najpierw trzeba poprawi� ten b��d
 Zastan�w si� jak bardzo ufasz temu u�ytkownikowi w kwestii sprawdzania
to�samo�ci innych u�ytkownik�w (czy sprawdzi on odciski kluczy pobrane
z r�nych �r�de�, dokumenty potwierdzaj�ce to�samo��, itd.).

 Nie nalezy umieszcza� adresu poczty elektronicznej w polu nazwiska czy
komentarza.
 Nazwa pliku. Naci�ni�cie ENTER potwierdzi nazw� domy�ln� (w nawiasach). Prosz� wprowadzi� opcjonalny komentarz Nazwa pliku danych:  Podaj d�ugie, skomplikowane has�o, np. ca�e zdanie.
 Prosz� usun�� to naruszenie zasad bezpiecze�stwa
 Pokazana warto�� wiarygodno�ci klucza mo�e by� niepoprawna,
dop�ki program nie zostanie uruchomiony ponownie.
 Prosz� usun�� znacznik wyboru z kluczy prywatnych.
 Prosz� powr�trzy� has�o, aby upewni� si� �e nie by�o pomy�ki. B��dy prosimy zg�asza� na adres <gnupg-bugs@gnu.org>.
 Prosz� wybra� tylko jeden podklucz.
 Prosz� wybra� dok�adnie jeden identyfikator u�ytkownika.
 Prosz� wybra� pow�d uniewa�nienia:
 Prosz� wybra� rodzaj klucza:
 Okres wa�no��i klucza.
         0 = klucz nie ma okre�lonego terminu wa�no�ci
      <n>  = termin wa�no�ci klucza up�ywa za n dni
      <n>w = termin wa�no�ci klucza up�ywa za n tygodni
      <n>m = termin wa�no�ci klucza up�ywa za n miesi�cy
      <n>y = termin wa�no�ci klucza up�ywa za n lat
 Okres wa�no��i podpisu.
         0 = klucz nie ma okre�lonego terminu wa�no�ci
      <n>  = termin wa�no�ci podpisu up�ywa za n dni
      <n>w = termin wa�no�ci podpisu up�ywa za n tygodni
      <n>m = termin wa�no�ci podpisu up�ywa za n miesi�cy
      <n>y = termin wa�no�ci podpisu up�ywa za n lat
 Najpierw trzeba u�y� polecenia "prze�".
 Prosz� czeka�, prowadzona jest zbi�rka losowo�ci. �eby si� nie nudzi�, mo�esz
popracowa� w systemie, przy okazji dostarczy to systemowi wi�cej entropii do
tworzenia liczb losowych.

 Regulamin:  Odcisk klucza g��wnego: Asymetryczne:  Klucz publiczny wy��czony z u�ycia.
 Wyj�� bez zapisania zmian?  Imi� i nazwisko:  Na pewno utworzy� certyfikaty uniewa�nienia ? (t/N)  Czy na pewno utworzy�?  Na pewno usun�� ten podpis klucza nim samym? (t/N)  Czy na pewno usun�� wszystkie wybrane identyfikatory u�ytkownika?  Czy na pewno usun�� ten identyfikator u�ytkownika?  Czy na pewno uniewa�ni� wszystkie wybrane identyfikatory u�ytkownika?  Czy na pewno uniewa�ni� ten identyfikator u�ytkownika?  Podpisa� wszystkie identyfikatory u�ytkownika na tym kluczu?  Czy na pewno podpisa�?  Czy na pewno zaktualizowa� ustawienia klucza dla wybranych identyfikator�w?  Czy na pewno usaktualni� ustawienia?  Pow�d uniewa�nienia: %s
 Powt�rzone has�o
 Powt�rz has�o:  ��dana d�ugo�� klucza to %u bit�w.
 Certyfikat uniewa�nienia zosta� utworzony.
 Certyfikat uniewa�nienia zosta� utworzony.

Nale�y przenie�� go na no�nik kt�ry mo�na bezpiecznie ukry�; je�li �li ludzie
dostan� ten certyfikat w swoje r�ce, mog� u�y� go do uczynienia klucza
nieu�ytecznym.

Niez�ym pomys�em jest wydrukowanie certyfikatu uniewa�nienia i schowanie
wydruku w bezpiecznym miejscu, na wypadek gdyby no�nik z certyfikatem sta� si�
nieczytelny. Ale nale�y zachowa� ostro�no��, systemy drukowania r�nych
komputer�w mog� zachowa� tre�� wydruku i udost�pni� j� osobom nieupowa�nionym.
 Zapisa� zmiany?  Dost�pny jest klucz tajny.
 Cz�� tajna g��wnego klucza jest niedost�pna.
 Wyb�r algorytmu.

DSA (zwany te� DSS) to algorytm podpisu cyfrowego i tylko do sk�adania
podpis�w mo�e by� u�ywany. Jest to algorytm preferowany, gdy�
sk�adane nim podpisy sprawdza si� du�o szybciej ni� te sk�adane
algorytmem ElGamala.

Algorytm ElGamala mo�e by� u�ywany zar�wno do podpis�w jak i do szyfrowania.
Standard OpenPGP rozr�nia dwa typy tego algorytmu - tylko do szyfrowania,
oraz do szyfrowania i podpisywania. Algorytm pozostaje bez zmian ale pewne
parametry musz� by� odpowiednio dobrane aby stworzy� klucz kt�rym mo�na
sk�ada� bezpieczne podpisy. Ten program obs�uguje oba typy ale inne
implementacje nnie musz� rozumie� kluczy do podpis�w i szyfrowania

G��wny klucz musi by� kluczem podpisuj�cym, jest to powodem dla
kt�rego w tym menu nie ma mo�no�ci wyboru klucza ElGamala do
szyfrowania. polecenie wywo�uj�ce przegl�dark� do zdj�� okazywanie zdj�cia - identyfikatora u�ytkownika Wa�no�� podpisu wygas�a %s.
 Wa�no�� podpisu wygasa %s.
 Okres wa�no�ci podpisu? (0)  Podpisano w %.*s kluczem %s o numerze %08lX.
 Adnotacje podpisu:  Regulamin podpisu:  Sk�adnia: gpgm [opcje] [pliki]
Sprawdzanie podpis�w ze znanych zaufanych kluczy
 Sk�adnia: gpg [opcje] [pliki]
podpisywanie, sprawdzanie podpis�w, szyfrowanie, deszyfrowanie
domy�lnie wykonywana operacja zale�y od danych wej�ciowych
 U�ywany generator liczb losowych jest atrap� wprowadzon� dla umo�liwienia
normalnej kompilacji - nie jest kryptograficznie bezpieczny!

JAKIEKOLWIEK DANE GENEROWANE PRZEZ TEN PROGRAM NIE NADAJ� SI� DO 
NORMALNEGO U�YTKU I NIE ZAPEWNIAJ� BEZPIECZE�STWA!!
 Podpis klucza nim samym na ,,%s''
jest podpisem z�o�onym przez PGP 2.x.
 Ten podpis jest niepoprawny. Mo�na usuni�� go ze zbioru kluczy. Ten algorytm szyfruj�cy jest u�ywany tylko przez GnuPG. Tego klucza nie b�dzie
mo�na u�y� do ��czno�ci z u�ytkownikami PGP. Wybrany algorytm jest tak�e
bardzo powolny, oraz mo�e nie by� tak bezpieczny jak pozosta�e dost�pne.
 Klucze PGP 2.x nie zawieraj� opisu ustawie�.
 To polecenie nie jest dost�pne w trybie %s.
 To jest klucz tajny! - czy na pewno go usun��?  To jest podpis wi���cy identyfikator u�ytkownika z kluczem. Nie nale�y
go usuwa� - GnuPG mo�e nie m�c pos�ugiwa� si� dalej kluczem bez
takiego podpisu. Bezpiecznie mo�na go usun�� tylko je�li ten podpis
klucza nim samym z jakich� przyczyn nie jest poprawny, i klucz jest
drugi raz podpisany w ten sam spos�b. To jest poprawny podpis na tym kluczu; normalnie nie nale�y go usuwa�
poniewa� mo�e by� wa�ny dla zestawienia po�aczenia zaufania do klucza
kt�rym go z�o�ono lub do innego klucza nim po�wiadczonego. Ten klucz nale�y do nas
 Ten klucz zosta� wy��czony z u�ytku Data wa�no�ci tego klucza up�yn�a! Wa�no�� tego klucza wygasa %s.
 Ten klucz nie jest chroniony.
 Klucz mo�e zosta� uniewa�niony przez klucz %s  Ten klucz prawdopodobnie nale�y do tej osoby.
 Ten podpis nie mo�e zosta� potwierdzony poniewa� nie ma
odpowiadaj�cego mu klucza publicznego. Nale�y od�o�y� usuni�cie tego
podpisu do czasu, kiedy oka�e si� kt�ry klucz zosta� u�yty, poniewa�
w momencie uzyskania tego klucza mo�e pojawi� si� �cie�ka zaufania
pomi�dzy tym a innym, ju� po�wiadczonym kluczem. Wa�no�� tego klucza wygas�a %s.
 To uczyni ten klucz nieuzytecznym dla PGP 2.x.
 Zostanie uniewa�niony przez:
 Aby zbudowa� Sie� Zaufania, GnuPG potrzebuje zna� klucze do kt�rych
masz absolutne zaufanie. Zwykle s� to klucze do kt�rych masz klucze
tajne. Odpowiedz ,,tak'', je�li chcesz okre�li� ten klucz jako klucz
do kt�rego masz absolutne zaufanie.
 Og�em przetworzonych kluczy: %lu
 Nie mo�na otworzy� zdj�cia ,,%s'': %s.
 Wywo�anie: gpg [opcje] [pliki] (-h podaje pomoc) Wywo�anie: gpgv [opcje] [pliki] (-h podaje pomoc) U�y� tego klucza pomimo to?  Identyfikator u�ytkownika ,,%s'' przekroczy� sw�j termin wa�no�ci. Identyfikator ,,%s'' nie jest podpisany swoim kluczem. Identyfikator u�ytkownika ,,%s'' zosta� uniewa�niony. identyfikator u�ytkownika przesta� by� poprawny OSTRZE�ENIE: ,,%s'' jest przestarza�� opcj�.
 OSTRZE�ENIE: %s powoduje obej�cie %s
 OSTRZE�ENIE: Istniej� dwa pliki z poufnymi informacjami.
 OSTRZE�ENIE: To jest klucz PGP wersji 2.x. Wyznaczenie mu klucza
             uniewa�niaj�cego, spowoduje, �e niekt�re wersje PGP przestan�
             go rozumie�.
 OSTRZE�ENIE: To jest klucz PGP wersji 2. Dodanie zdj�cia spowoduje, �e
             niekt�re wersje przestan� go rozumie�.
 OSTRZE�ENIE: Ten klucz zosta� uniewa�niony przez w�a�ciciela!
 OSTRZE�ENIE: Ten klucz nie jest po�wiadczony zaufanym podpisem!
 OSTRZE�ENIE: Tego klucza nie po�wiadczaj� wystarczaj�co zaufane podpisy!
 OSTRZE�ENIE: Ten podklucz zosta� uniewa�niony przez w�a�ciciela!
 OSTRZE�ENIE: u�ywany jest klucz nie obdarzony zaufaniem!
 OSTRZE�ENIE: NIE UFAMY temu kluczowi!
 OSTRZE�ENIE: Wykryto klucz s�aby algorytmu - nale�y ponownie zmieni� has�o.
 OSTRZE�ENIE: plik ,,%s'' jest pusty
 OSTRZE�ENIE: identyfikator u�ytkownika podpisany za %d sekund (w przysz�o�ci)
 OSTRZE�ENIE: nie mo�na cofn�� wyznaczenia klucza jako uniewa�niaj�cego!
 OSTRZE�ENIE: algorytm skr�tu ,,%s'' nie jest cz�ci� standardu OpenPGP.
             U�ywasz go na w�asn� odpowiedzialno��!
 OSTRZE�ENIE: zaszyfrowana wiadomo�� by�a manipulowana!
 OSTRZE�ENIE: niepoprawne dane w adnotacji
 OSTRZE�ENIE: niew�a�ciwy rozmiar pliku random_seed - nie zostanie u�yty
 OSTRZE�ENIE: klucz %08lX m�g� zosta� uniewazniony:
             zapytanie o uniewa�niaj�cy klucz %08lX w serwerze kluczy
 OSTRZE�ENIE: klucz %08lX m�g� zosta� uniewa�niony:
             brak uniewa�niaj�cego klucza %08lX.
 OSTRZE�ENIE: wiadomo�� by�a szyfrowana kluczem s�abym szyfru symetrycznego.
 OSTRZE�ENIE: wiadomo�� nie by�a zabezpieczona przed manipulacj�
 OSTRZE�ENIE: wielokrotne podpisy. Tylko pierwszy zostanie sprawdzony.
 OSTRZE�ENIE: �aden identyfikator u�ytkownika nie zosta� oznaczony explicite
             jako g��wny. Wykonanie tego polecenie mo�e wi�c spowodowa�
             wy�wietlanie innego identyfikatora jako domy�lnego g��wnego.
 OSTRZE�ENIE: nic nie zosta�o wyeksportowane!
 OSTRZE�ENIE: opcje w ,,%s'' nie s� jeszcze uwzgl�dnione.
 OSTRZE�ENIE: program mo�e stworzy� plik zrzutu pami�ci!
 OSTRZE�ENIE: podano adresat�w (-r) w dzia�aniu kt�re ich nie dotyczy
 OSTRZE�ENIE: klucz prywatny %08lX nie ma prostej sumy kontrolnej SK.
 OSTRZE�ENIE: konflikt skr�t�w podpis�w w wiadomo�ci
 OSTRZE�ENIE: nie mo�na rozwin�� %% w URL adnotacji (jest zbyt d�ugi).
             U�yty zostanie nie rozwini�ty.
 OSTRZE�ENIE: nie mo�na rozwin�� znacznik�w %% w URL regulaminu
           (jest zbyt d�ugi). U�yty zostanie nie rozwini�ty.
 OSTRZE�ENIE: nie mo�na skasowa� tymczasowego katalogu ,,%s'': %s.
 OSTRZE�ENIE: nie mo�na skasowa� pliku tymczasowego (%s) ,,%s'': %s.
 OSTRZE�ENIE: niebezpieczne prawa w�asno�ci do katalogu
                  zawieraj�cego %s ,,%s''
 OSTRZE�ENIE: niebezpieczne prawa dost�pu do katalogu 
                  zawieraj�cego %s ,,%s''
 OSTRZE�ENIE: niebezpieczne prawa w�asno�ci do %s ,,%s''.
 OSTRZE�ENIE: niebezpieczne prawa dost�pu do %s ,,%s''.
 OSTRZE�ENIE: nie mo�na zabezpieczy� u�ywanej pami�ci!
 OSTRZE�ENIE: u�ywany generator liczb losowych
nie jest kryptograficznie bezpieczny!!
 Musimy wygenerowa� du�o losowych bajt�w. Dobrym pomys�em aby pom�c komputerowi
podczas generowania liczb pierszych jest wykonywanie w tym czasie innych
dzia�a� (pisanie na klawiaturze, poruszanie myszk�, odwo�anie si� do dysk�w);
dzi�ki temu generator liczb losowych ma mo�liwo�� zebrania odpowiedniej ilo�ci
entropii.
 Jakiej d�ugo�ci klucz wygenerowa�? (1024)  Przy podpisywaniu identyfikatora u�ytkownika na kluczu nale�y sprawdzi�, 
czy to�samo�� u�ytkownika odpowiada temu, co jest wpisane w identyfikatorze.
Innym u�ytkownikom przyda si� informacja, jak dog��bnie zosta�o to przez
Ciebie sprawdzone.

"0" oznacza, �e nie podajesz �adnych informacji na temat tego jak dog��bnie
    to�samo�� u�ytkownika zosta�a przez Ciebie potwierdzona.

"1" oznacza, �e masz przekonanie, �e to�samo�� u�ytkownka odpowiada
    identyfikatorowi klucza, ale nie by�o mo�liwo�ci sprawdzenia tego.
    Taka sytuacja wyst�puje te� kiedy podpisujesz identyfikator b�d�cy
    pseudonimem.

"2" oznacza, �e to�samo�� u�ytkownika zosta�� przez Ciebie potwierdzona
    pobie�nie - sprawdzili�cie odcisk klucza, sprawdzi�a�/e� to�samo��
    na okazanym dokumencie ze zdj�ciem.

"3" to dog��bna weryfikacja to�samo�ci. Na przyk�ad sprawdzenie odcisku 
    klucza, sprawdzenie to�samo�ci z okazanego oficjalnego dokumentu ze
    zdj�ciem (np paszportu) i weryfikacja poprawno�ci adresu poczty
    elektronicznej przez wymian� poczty z tym adresem.

Zauwa�, �e podane powy�ej przyk�ady dla poziom�w "2" i "3" to *tylko*
przyk�ady. Do Ciebie nale�y decyzja co oznacza "pobie�ny" i "dog��bny" w
kontek�cie po�wiadczania i podpisywania kluczy.

Je�li nie wiesz co odpowiedzie�, podaj "0". Czy na pewno chcesz uniewa�ni� te podpisy:
 U�ywasz zestawu znak�w %s.
 Nie mo�na zmieni� daty wa�no�ci klucza w wersji 3.
 Nie mo�esz usun�� ostatniego identyfikatora u�ytkownika!
 Nie zosta� podany identyfikatora u�ytkownika (np za pomoc� ,,-r'')
 Nie chcesz has�a - to *z�y* pomys�!

 Nie chcesz poda� has�a - to *z�y* pomys�!
W ka�dej chwili mo�esz ustawi� has�o u�ywaj�c tego programu i opcji
"--edit-key".

 Te identyfikatory s� podpisane przez Ciebie:
 Do klucza dla PGP 2.x nie mo�na wyznaczy� klucza uniewa�niaj�cego.
 Do klucza dla PGP 2.x nie mo�na doda� zdj�cia.
 W trybie --pgp2 nie mo�na podpisywa� kluczy PGP 2.x podpisami OpenPGP.
 Musisz wybra� co najmniej jeden klucz.
 Musisz wybra� co najmniej jeden identyfikator u�ytkownika.
 Musisz poda� d�ugie, skomplikowane has�o aby ochroni� sw�j klucz tajny.
 Musisz poda� has�o aby odbezpieczy� klucz tajny u�ytkownika:
"%.*s".
Klucz o d�ugo�ci %u bit�w, typ %s, numer %08lX, stworzony %s%s
 Tw�j identyfikator u�ytkownika b�dzie wygl�da� tak:
    "%s"

 Nalezy poda� pow�d uniewa�nienia klucza. W zale�no�ci od kontekstu mo�na
go wybra� z listy:
  "Klucz zosta� skompromitowany"
      Masz powody uwa�a� �e tw�j klucz tajny dosta� si� w niepowo�ane r�ce.
  "Klucz zosta� zast�piony"
      Klucz zosta� zast�piony nowym.
  "Klucz nie jest ju� u�ywany"
      Klucz zosta� wycofany z u�ycia.
  "Identyfikator u�ytkownika przesta� by� poprawny"
      Identyfikator u�ytkownika (najcz�ciej adres e-mail przesta� by� 
      poprawny.
 Tw�j podpis na "%s"
przekroczy� dat� wa�no�ci.
 Tw�j podpis na "%s"
jest podpisem prywatnym (lokalnym).
 Twoja decyzja?  Tw�j wyb�r?  Tw�j system nie potrafi pokaza� daty po roku 2038.
Niemniej daty do roku 2106 b�d� poprawnie obs�ugiwane.
 [brak identyfikatora u�ytkownika] [przeterminowany] [nazwa pliku] [uniewa�nienie] [uniewa�niony] [podpis klucza nim samym] [niepewne] ,,%s'' ju� jest skompresowany
 ,,%s'' nie jest zwyk�ym plikiem - zostaje pomini�ty
 ,,%s'' nie jest poprawnym d�ugim numerem klucza
 nazwa adnotacji mo�e zawiera� tylko litery, cyfry, kropki i podkre�lenia, 
i musi ko�czy� si� ,,=''
 tre�� adnotacji nie mo�e zawiera� znak�w steruj�cych
 adnotacja u�ytkownika musi zawiera� znak '@'
 dodanie zdj�cia u�ytkownika do klucza wyznaczenie klucza uniewa�niaj�cego dodanie podklucza dodanie nowego identyfikatora u�ytkownika do klucza dodanie zbioru kluczy do u�ywanych dodanie zbioru kluczy tajnych do u�ywanych dodkl dodfoto dodun dodid do szyfrowania b�dzie u�ywany MDC adresat anonimowy; sprawdzanie %08lX ...
 nag��wek opakowania:  opakowanie: %s
 automatyczna odpowied� nie na wi�kszo�� pyta� automatyczna odpowied� tak na wi�kszo�� pyta� przyjmuj�c �e dane zosta�y zaszyfrowane za pomoc� %s
 podpis z�o�ony kluczem %08lX uznany za niewa�ny z powodu nieznanego bitu krytycznego
 przyj�to obecno�� podpisanych danych w '%s'
 b��d MPI niepoprawny URI niepoprawny certyfikat niepoprawny klucz niepoprawne has�o niepoprawny klucz publiczny niepoprawny klucz prywatny niepoprawny podpis tryb wsadowy: �adnych pyta� mniej komunikat�ww binarny wywo�anie funkcji build_packet nie powiod�o si�: %s
 l nie mo�na zamkn�� ,,%s'': %s
 nie mo�na si� po��czy� z ,,%s'': %s
 nie mo�na utworzy� %s: %s
 nie mo�na utworzy� ,,%s'': %s
 nie mo�na utworzy� katalogu ,,%s'': %s
 nie mo�na wy��czy� zrzut�w pami�ci: %s
 nie dzia�a w trybie wsadowym
 bez opcji "--yes" nie dzia�a w trybie wsadowym
 nie mo�na pobra� klucza z serwera: %s
 serwer nie chce czyta� deskryptora dla agenta
 serwer nie chce pisa� deskryptora dla agenta
 nie mo�na obs�u�y� tego algorytmu klucza publicznego: %d
 nie mo�na obs�u�y� linii tekstu d�u�szej ni� %d znak�w
 nie mo�na obs�uzy� tych wielokrotnych podpis�w
 nie mo�na otworzy� %s: %s
 nie mo�na otworzy� ,,%s''
 nie mo�na otworzy� ,,%s'': %s
 nie mo�na otworzy� pliku: %s
 nie mo�na otworzy� podpisanego pliku ,,%s''
 otwarcie zbioru kluczy jest niemo�liwe nie mo�na umie�ci� URL-a regulaminu w podpisach sk�adanych kluczami PGP 2.x
 w podpisach dla PGP 2.x nie mo�na umie�ci� URL-a do regulaminu podpisu
 nie mo�na umie�ci� adnotacji w podpisach kluczy sk�adanych kluczami PGP 2.x
 nie mo�na umie�ci� adnotacji w podpisach sk�adanych kluczami PGP 2.x
 pytanie o has�o nie dzia�a w trybie wsadowym
 nie mo�na odczyta� ,,%s'': %s
 nie mo�na przeszuka� serwera: %s
 nie mo�na ustawi� numeru procesu klienckiego agenta
 nie mo�na sprawdzi� ,,%s'': %s
 ustawiony tryb S2K nie pozwala u�y� pakietu ESK dla szyfru symetrycznego
 nie mo�na zapisa� ,,%s'': %s
 anulowano przez u�ytkownika
 klucza PGP 2.x nie mo�na wyznaczy� jako uniewa�niaj�cego
 brak mo�liwo�ci generacji dobrego klucza dla szyfru symetrycznego;
operacja by�a powtarzana %d razy!
 zmiana daty wa�no�ci klucza zmiana zaufania w�a�ciciela zmiana has�a klucza lista sprawdzenie podpis�w kluczy sprawdzanie na g��boko�ci %d podpis�w =%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d)
 sprawdzenie z�o�onego podpisu nie powiod�o si�: %s
 sprawdzanie zbioru kluczy ,,%s''
 sprawdzanie bazy zaufania
 b��d sumy kontrolnej algorytm szyfruj�cy %d%s jest nieznany, lub zosta� wy��czony
 modu� szyfru ,,%s'' nie zosta� za�adowany z powodu niebezpiecznych praw dost�pu
 problem z porozumiewaniem si� z agentem
 warto�� completes-needed musi by� wi�ksza od 0
 ustawienie kompresji musi pochodzi� z zakresu %d..%d
 sprzeczne polecenia
 niezrozuma�y URI serwera kluczy
 opakowanie ASCII pliku wynikowego dane nie zosta�y zapisane; aby to zrobi�, nale�y u�y� opcji "--output"
 zdj�cie opakowania ASCII nie powiod�o si�: %s
 �ledzenia odszyfrowywanie danych (domy�lne) b��d odszyfrowywania: %s
 odszyfrowanie poprawne
 usuni�cie podklucza usuni�cie podpis�w usuni�cie identyfikatora u�ytkownika z klucza usuni�cie bloku klucza nie powiod�o si�: %s
 uskl usfoto uspod usid w tej wersji algorytm skr�tu ,,%s'' jest tylko do odczytu
 wy�kl wy��czy� klucz z u�ycia bez wymuszania trzeciej wersji formatu podpis�w bez wymuszania czwartej wersji formatu podpis�w pozostawienie bez zmian bez odwo�a� do terminala emulacja trybu opisanego w RFC1991 w�kl w��czy� klucz do u�ycia opakowywanie ASCII nie powiod�o si�: %s
 szyfrowanie danych zaszyfrowano kluczem %s, o numerze %08lX
 zaszyfrowano %u-bitowym kluczem %s, numer %08lX, stworzonym %s
 dane zaszyfrowano nieznanym algorytmem numer %d
 szyfrowanie wiadomo�ci w trybie --pgp2 wymaga modu�u szyfru IDEA
 szyfrowanie tylko szyfrem symetrycznym b��d tworzenia `%s': %s
 b��d tworzenia zbioru kluczy `%s': %s
 b��d podczas tworzenia has�a: %s
 b��d podczas odczytu zapisu warto�ci zaufania: %s
 b��d w linii ko�cz�cej
 b��d odczytu ,,%s'': %s
 b��d odczytu bloku kluczy: %s
 b��d odczytu bloku klucza tajnego '%s': %s
 b��d przy wysy�aniu do ,,%s'': %s
 b��d zapisu zbioru kluczy '%s': %s
 b��d podczas zapisu zbioru kluczy publicznych ,,%s'': %s
 b��d podczas zapisu zbioru kluczy tajnych ,,%s'': %s
 b��d: niew�a�ciwy odcisk klucza
 b��d: brak dwukropka
 b��d: brak warto��i zaufania w�a�ciciela
 data eksport kluczy do pliku eksport kluczy do serwera kluczy eksport warto�ci zaufania nieszczelne uprawnienia ustawie� - wo�anie zewn�trznych program�w wy��czone
 wysy�anie do ,,%s'' nie powiod�o si� (status=%u)
 inicjowanie Bazy Zaufania nie powiod�o si�: %s
 nie powiod�a si� odbudowa bufora bazy: %s
 b��d przy zamykaniu pliku b��d przy tworzeniu pliku b��d przy usuwaniu pliku plik ju� istnieje b��d przy otwieraniu pliku. b��d przy odczycie pliku b��d przy zmianie nazwy pliku b��d przy zapisie pliku naprawa uszkodzonej bazy zaufania oznaczenie identyfikatora u�ytkownika jako g��wnego wymuszenie trzeciej wersji formatu podpis�w wymuszenie czwartej wersji formatu podpis�w wymuszone u�ycie kompresji %s (%d) k��ci si� z ustawieniami adresata
 wymuszone u�ycie skr�tu %s (%d) k��ci si� z ustawieniami adresata
 wymuszone u�ycie szyfru %s (%d) k��ci si� z ustawieniami adresata
 odc b��d og�lny generacja nowej pary kluczy tworzenie certyfikatu uniewa�nienia klucza tworzenie przestarza�ej 16-bitowej sumy kontrolnej dla ochrony klucza
 gpg-agent nie jest dost�pny w tej sesji
 wersja %d protoko�u agenta nie jest obs�ugiwana
 pomoc iImMwWpP import kluczy z serwera kluczy wczytanie warto��i zaufania import/do��czenie kluczy linia wej�cia %u zbyt d�uga lub brak znaku LF
 linia d�u�sza ni� %d znak�w
 niepoprawny tryb S2K; musi mie� warto�� 0, 1 lub 3
 b��dny argument b��d w opakowaniu ASCII niepoprawny nag��wek opakowania:  b��d opakowania: linia d�u�sza ni� %d znak�w
 niew�a�ciwy znak w tek�cie ustawie�
 niew�a�ciwy nag��wek dokumentu z podpisem na ko�cu
 niepoprawne oznaczenie linii minusami:  niew�a�ciwe domy�lne ustawienia
 niew�a�ciwy domy�lny poziom sprawdzania; musi mie� warto�� 0, 1 lub 3
 niepoprawne opcje eksportu kluczy
 niew�a�ciwy algorytm skr�tu ,%s'
 niepoprawne opcje wczytania kluczy
 b��d w zbiorze kluczy niepoprawny pakiet niepoprawne d�ugie has�o niew�a�ciwe ustawienia szyfr�w
 niew�a�ciwe ustawienia algorytm�w kompresji
 niew�a�ciwe ustawienia skr�t�w
 niew�a�ciwy znak formatu radix64: ,,%02x'', zosta� pomini�ty
 b��dna odpowied� agenta
 wykryto niepoprawny pakiet pierwotny w proc_tree()
 odnaleziono odwo�anie do niepoprawnego szyfru (%d)
 niepoprawna warto��
 klucz klucz %08lX zosta� stworzony %lu sekund� w przysz�o�ci (zaburzenia
czasoprzestrzeni, lub �le ustawiony zegar systemowy)
 klucz %08lX zosta� stworzony %lu sekund(y) w przysz�o�ci (zaburzenia
czasoprzestrzeni, lub �le ustawiony zegar systemowy)
 klucz %08lX nie jest kompletny
 klucz %08lX zosta� oznaczony jako obdarzony absolutnym zaufaniem
 klucz %08lX jest wpisany wi�cej ni� raz w bazie zaufania
 klucz %08lX: ,,%s'' %d nowych podpis�w
 klucz %08lX: ,,%s'' %d nowych podkluczy
 klucz %08lX: ,,%s'' %d nowych identyfikator�w u�ytkownika
 klucz %08lX: ,,%s'' 1 nowy podpis
 klucz %08lX: ,,%s'' 1 nowy podklucz
 klucz %08lX: ,,%s'' 1 nowy identyfikator u�ytkownika
 klucz %08lX: ,,%s'' bez zmian
 klucz %08lX: ,,%s'' dodany certyfikat uniewa�nienia
 klucz %08lX: ,,%s'' certyfikat uniewa�nienia zosta� ju� wczytany
 klucz %08lX: podklucz uszkodzony przez serwer zosta� naprawiony
 klucz %08lX: klucz PGP 2.x - pomini�ty
 klucz %08lX: zaakceptowany jako klucz zaufany
 klucz %08lX: przyj�to identyfikator nie podpisany nim samym ,,%s''
 Klucz %08lX: ten klucz ju� znajduje si� w zbiorze
 klucz %08lX: brak oryginalnego bloku klucza; %s
 klucz %08lX: nie mo�na odczyta� oryginalnego bloku klucza; %s
 klucz %08lX: dodano bezpo�redni podpis
 klucz %08lX: nie zgadza si� z lokaln� kopi�
 key %08lX: do��czono powt�rzony identyfikator u�ytkownika
 klucz %08lX: niepoprawny certyfikat uniewa�nienia: %s - odrzucony
 klucz %08lX: pomini�to -  niepoprawny certyfikat uniewa�nienia: %s
 klucz %08lX: niepoprawny podpis na identyfikatorze "%s"
 klucz %08lX: niepoprawne dowi�zanie podklucza
 klucz %08lX: nieoprawne uniewa�nienie podklucza
 klucz %08lX: klucz zosta� uniewa�niony!
 klucz %08lX: nowy klucz - pomini�ty
 klucz %08lX: brak klucza publicznego kt�rego dotyczy wczytany certyfikat
              uniwa�nienia
 klucz %08lX: brak klucza publicznego dla zaufanego klucza - pomini�ty
 klucz %08lX: brak podklucza do dowi�zania
 klucz %08lX: brak podklucza, kt�rego dotyczy uniewa�nienie
 klucz %08lX: brak podklucza dowi�zywanego podpisem
 klucz %08lX: brak podklucza, kt�rego dotyczy uniewa�nienie
 klucz %08lX: brak identyfikatora u�ytkownika
 klucz %08lX: brak identyfikatora u�ytkownika do podpisu
 klucz %08lX: brak poprawnych identyfikator�w u�ytkownika
 klucz %08lX: podpis nieeksportowalny (klasy %02x) - pomini�ty
 klucz %08lX: nie jest w formacie RFC 2440 - pomini�ty
 klucz %08lX: nie jest chroniony - pomini�ty
 klucz %08lX: klucz publiczny ,,%s'' wczytano do zbioru
 klucz %08lX: brak klucza publicznego: %s
 klucz %08lX: usuni�to wielokrotne dowi�zanie podklucza
 klucz %08lX: usuni�to wielokrotne uniewa�nienie podklucza
 klucz %08lX: pomini�to certyfikat uniewa�nienia umieszczony 
              w niew�a�ciwym miejscu
 Klucz %08lX: klucz tajny wczytany do zbioru
 klucz %08lX: brak klucza tajnego: %s
 klucz %08lX: klucz tajny z ustawionym szyfrem %d - pomini�ty
 klucz %08lX: klucz tajny bez klucza jawnego - pomini�ty
 klucz %08lX: podklucz pomini�ty
 klucz %08lX: pomini�to identyfikator u�ytkownika ' klucz %08lX: podklucz zosta� uniewa�niony!
 klucz %08lX: pomini�to - podpis na podkluczu w niew�a�ciwym miejscu
 klucz %08lX: Klucz algorytmu ElGamala wygenerowany przez PGP 
              - podpisy nim sk�adane nie zapewniaj� bezpiecze�stwa!
 klucz %08lX: pomini�to - nieoczekiwana klasa podpisu (%02x)
 klucz %08lX: nie obs�ugiwany algorytm asymetryczny
 klucz %08lX: algorytm asymetryczny "%s" nie jest obs�ugiwany
 klucz ,,%s'' nie zosta� odnaleziony: %s
 eksport kluczy nie powi�d� si�: %s
 klucz zosta� stworzony %lu sekund� w przysz�o�ci (zaburzenia
czasoprzestrzeni, lub �le ustawiony zegar systemowy)
 klucz zosta� stworzony %lu sekund w przysz�o�ci (zaburzenia
czasoprzestrzeni, lub �le ustawiony zegar systemowy)
 klucz jest niekompletny
 klucz nie jest oznaczony jako niepewny - nie mo�na go u�y� z atrap� 
generatora liczb losowych!
 klucz zosta� oznaczony jako obdarzony absolutnym zaufaniem.
 zbi�r kluczy ,,%s'' zosta� utworzony
 b��d serwera kluczy odbi�r z serwera kluczy nie powi�d� si�: %s
 od�wie�enie kluczy z serwera nie powiod�o si�: %s
 szukanie w serwerze kluczy nie powiod�o si�: %s
 wysy�ka do serwera kluczy nie powiod�a si�: %s
 niew�a�ciwa d�ugo�� klucza; wykorzystano %u bit�w
 rozmair klucza zaokr�glony do %u bit�w
 zbyt du�y rozmiar klucza, ograniczenie wynosi %d.
 D�ugo�� klucza zbyt ma�a; minimalna dopuszczalna dla RSA wynosi 1024 bity.
 D�ugo�� klucza zbyt ma�a; minimalna dopuszczona wynosi 768 bit�w.
 l linia zbyt d�uga
 lista lista kluczy i identyfikator�w u�ytkownik�w lista kluczy lista kluczy i ich odcisk�w lista kluczy i podpis�w wypisane sekwencji pakiet�w ustawienia (zaawansowane) rozbudowana lista ustawie� lista kluczy prywatnych lista podpis�w lpodpis z�o�enie podpisu oddzielonego od dokumentu nie traktowa� konfliktu datownik�w jako b��du wywo�anie funkcji make_keysig_packet nie powiod�o si�: %s
 b��d formatu CRC
 z�y format zmiennej �rodowiskowej GPG_AGENT_INFO
 b��d formatu identyfikatora u�ytkownika warto�� marginals-needed musi by� wi�ksza od 1
 warto�� max-cert-depth musi mie�ci� si� w zakresie od 1 do 255
 przenosz� podpis klucza na w�a�ciwe miejsce
 nN zagnie�d�one podpisy na ko�cu dokumentu
 b��d sieci nigdy      do szyfrowania nie zostanie u�yty MDC nowy plik ustawie� ,,%s'' zosta� utworzony
 nast�pne sprawdzanie bazy odb�dzie si� %s
 nie w definicji grupy ,,%s'' brak znaku ,,=''
 brak odpowiadaj�cego klucza publicznego: %s
 brak domy�lego zbioru kluczy tajnych: %s
 modu� gromadzenia entropii nie zosta� wykryty
 sprawdzanie bazy jest niepotrzebne
 odwo�ania do zewn�trznych program�w s� wy��czone
 brak kluczy uniewa�niaj�cych dla ,,%s''
 brak klucza tajnego
 brak prywatnego odpowiednika podklucza publicznego %08lX - pomini�ty
 brak podpisanych danych
 brak takiego identyfikatora u�ytkownika. brak absolutnie zaufanych kluczy
 nie odnaleziono poprawnych danych w formacie OpenPGP.
 brak poprawnych adresat�w
 brak zapisywalnego zbioru kluczy: %s
 brak zapisywalnego zbioru kluczy publicznych: %s
 brak zapisywalnego zbioru kluczy tajnych: %s
 nie jest oddzielonym podpisem.
 nie zaszyfrowany nieczytelne dla cz�owieka nie zosta� przetworzony nie jest obs�ugiwany uwaga: plik random_seed jest pusty
 uwaga: plik random_seed nie jest uaktualniony
 nulpodpis nupodpis OK, to my jeste�my adresatem anonimowym.
 stary, nie obs�ugiwany algorytm szyfrowania klucza sesyjnego
 podpis starego typu (PGP 2.x).
 bez zabezpieczenia pami�ci nie mo�na wykona� tej operacji
 plik opcji ,,%s'': %s
 pierwotna nazwa pliku='%.*s'
 informacja o zaufaniu dla w�a�ciciela klucza zosta�a wymazana
 has�o nie zosta�o poprawnie powt�rzone; jeszcze jedna pr�ba has�o zbyt d�ugie
 has�o nale�y uruchomi� gpg z opcj� ,,--check-trustdb''
 prosz� wprowadzi� opcjonalny ale wysoce doradzany adres e-mail obja�nienie mo�na przeczyta� tutaj: http://www.gnupg.org/faq.html
 wi�cej informacji jest tutaj: http://www.gnupg.org/why-not-idea.html
 w jej miejsce nale�y u�y� ,,%s%s''"
 opcje ustawienie %c%lu powtarza si�
 ustawienie %c%lu jest niepoprawne
 przedwczesny koniec pliku (w CRC)
 przedwczesny koniec pliku (w linii ko�cz�cej)
 przewczesny koniec pliku (brak CRC)
 g��wny problem podczas obr�bki pakietu szyfrowego
 problem z agentem - zostaje wy��czony
 problem agenta: zwr�ci� 0x%lx
 pytanie przed nadpisaniem plik�w algorytm ochrony %d%s nie jest obs�ugiwany
 klucz publiczny i prywatny (tajny) zosta�y utworzone i podpisane.
 klucz publiczny %08lX jest o %lu sekund� m�odszy od podpisu
 klucz publiczny %08lX jest o %lu sekund(y) m�odszy od podpisu
 klucz publiczny %08lX nie odnaleziony: %s
 b��d odszyfrowywania kluczem publicznym: %s
 klucz publiczny nie pasuje do klucza prywatngeo!
 dane zaszyfrowane kluczem publicznym: poprawny klucz sesyjny
 klucz publiczny %08lX
 brak klucza publicznego klucz publiczny absolutnie zaufanego klucza %08lX nie odnaleziony
 w wW wyj�cie wyj�cie z tego menu znak kodowania quoted-printable w opakowaniu ASCII - prawdopodobnie
przek�amanie wprowadzone przez serwer pocztowy
 b��d odczytu: %s
 wczytanie opcji z pliku odczyt z '%s'
 odczyt opcji z ,,%s''
 czytam strumie� standardowego wej�cia
 pow�d uniewa�nienia:  usuni�cie klucza ze zbioru kluczy publicznych usuni�cie klucza ze zbioru kluczy prywatnych zapytanie o klucz %08lX w %s
 ograniczenie zasob�w un! podklucz zosta� uniewa�niony: %s
 un- fa�szywy certyfikat uniewa�nienia
 un? problem przy sprawdzaniu uniewa�nienia: %s
 unpkl komentarz do uniewa�nienia:  uniewa�nienie podklucza uniewa�nienie identyfikatora u�ytkownika uniewa�nienie podpisu unpod unpod zaokr�glono do %u bit�w
 p zapis zapis zmian i wyj�cie szukanie kluczy na serwerze zapytanie o ,,%s'' w serwerze HKP %s
 klucz prywatny ,,%s'' nie zosta� odnaleziony: %s
 brak klucza prywatnego tajne cz�ci klucza s� niedost�pne
 wyb�r podklucza N wyb�r identyfikatora u�ytkownika N wybrany algorytm skr�t�w po�wiadcze� jest niepoprawny
 wybrany algorytm szyfruj�cy jest niepoprawny
 wybrany algorytm skr�t�w wiadomo�ci jest niepoprawny
 tryb zgodno�ci formatu pakiet�w, szyfr�w i skr�t�w z OpenPGP tryb zgodno�ci format�w, szyfr�w i skr�t�w z PGP 2.x ustawienie opcji klucza ustaw okazanie odcisku klucza okazanie identyfikatora - zdj�cia ten tekst pomocy okazanie, w kt�rym zbiorze znajduje si� dany klucz foto opcje podpis z�o�enie podpisu na kluczu z�o�enie prywatnego podpisu na kluczu z�o�enie na kluczu podpisu prywatnego,
nie podlegaj�cego uniewa�nieniu z�o�enie na kluczu podpisu nie podlegaj�cego 
uniewa�nieniu podpisanie lub modyfikacja klucza z�o�enie podpisu na kluczu z�o�enie prywatnego (lokalnego) podpisu na kluczu z�o�enie na kluczu prywatnego podpisu nie podlegaj�cego uniewa�nieniu z�o�enie na kluczu podpisu nie podlegaj�cego uniewa�nieniu wymuszono pomini�cie sprawdzenia podpisu
 z�o�enie podpisu nie powiod�o si�: %s
 podpis: pomini�ty ,,%s'': %s
 pomini�ty ,,%s'': duplikat
 pomini�ty ,,%s'': wygenerowany przez PGP klucz dla algorytmu ElGamala,
podpisy sk�adane tym kluczem nie zapewniaj� bezpiecze�stwa!
 pomini�ty: zosta� ju� wybrany w innej opcji
 pomini�ty: klucz publiczny ju� jest domy�lnym adresatem
 pomini�ty: klucz prywatny jest ju� wpisany
 blok typu %d zostaje pomini�ty
 podpis w wersji 3 na identyfikatorze ,,%s'' zostaje pomini�ty
 niestety, to nie dzia�a w trybie wsadowym
 osobny certyfikat uniewa�nienia - u�yj ,,gpg --import'' aby go wczyta�
 oddzielony podpis klasy 0x%02x.
 zapis danych w formacie OpenPGP podpakiet typu %d ma ustawiony krytyczny bit
 wysy�anie do ,,%s'' powiod�o si� (status=%u)
 b��d systemu podczas wo�ania programu zewn�trznego: %s
 p pobieranie kluczy z tego zbioru tekstowy modu� szyfru IDEA nie jest dost�pny
 podany URL regulaminu po�wiadczania jest niepoprawny
 podany URL regulaminu podpis�w jest niepoprawny
 nie mo�na sprawdzi� podpisu.
Nale�y pami�ta� o podawaniu pliku podpisu (.sig lub .asc) jako pierwszego
argumentu linii polece�.
 Baza zaufania jest uszkodzona; prosz� uruchomi� ,,gpg --fix-trustdb''.
 dla klucza publicznego ,,%s'' istnieje klucz prywatny!
 u�ywanie tego szyfru jest odradzane; nale�y u�ywa� standardowych szyfr�w!
 ten serwer kluczy nie umo�liwia przeszukiwania
 to mo�e by� spowodowane brakiem podpisu klucza nim samym
 ta wiadomo�� mo�e nie da� si� odczyta� za pomoc� %s
 platforma wymaga u�ycia plik�w tymczasowych do wo�ania zewn�trznych program�w
 usuni�cie numer�w kluczy adresat�w z szyfrogram�w konflikt datownik�w prze� prze��czenie pomi�dzy list� kluczy publicznych i prywatnych zbyt wiele `%c' ustawie�
 zbyt wiele wpis�w w buforze kluczy publicznych - wy��czony
 zaufanie b��d w bazie zaufania wpis zaufania %lu jest typu innego ni� poszukiwany %d
 wpis zaufania %lu, typ zapytania %d: odczyt nie powi�d� si�: %s
 wpis zaufania %lu, typ zapytania %d: zapis nie powi�d� si�: %s
 baza zaufania, wpis %lu: funkcja lseek() nie powiod�a si�: %s
 baza zaufania, wpis %lu: zapis nie powi�d� si� (n=%d): %s
 zbyt du�e zlecenie dla bazy zaufania
 baza zaufania: funkcja lseek() zawiod�a: %s
 baza zaufania: funkcja read() (n=%d) zawiod�a: %s
 baza zaufania: synchronizacja nie powiod�a si� %s
 id nie mo�na wy�wietli� zdj�cia!
 nie mo�na wykona� %s ,,%s'': %s
 nie mo�na uruchomi� zewn�trznego programu
 nie mo�na odczyta� odpowiedzi programu zewn�trznego: %s
 nie mo�na ustawi� �cie�ki program�w wykonywalnych na %s
 nie mo�na u�y� szyfru IDEA z wszystkimi kluczami dla kt�rych szyfrujesz.
 bezobs�ugowe uaktualnienie bazy zaufania nieoczekiwane opakowanie: nieoczekiowane dane algorytm szyfruj�cy nie jest zaimplementowany algorytm szyfrowania z kluczem publicznym nie jest zaimplementowany nieznany nieznany algorytm szyfruj�cy nieznany algorytm kompresji nieznany domy�lny adresat ,,%s''
 nieznany algorytm skr�tu nieznany typ pakietu nieznany algorytm ochrony
 nieznany algorytm asymetryczny nieznana klasa podpisu nieznana wersja nienaturalne zako�czenie pracy zewn�trznego programu
 URI nie jest obs�ugiwany nieu�yteczny algorytm asymetryczny bezu�yteczny klucz publiczny bezu�yteczny klucz prywatny od�wie�enie wszystkich kluczy z serwera zapis zmian nie powi�d� si�: %s
 zapis zmian na kluczu prywatnym nie powi�d� si�: %s
 uaktualnienie bazy zaufania aktualizacja ustawie� klucza aktopc wywo�anie: gpg [opcje] plik wyj�ciowy kanoniczny format tekstowy aby go usun�� nalezy najpierw u�y� opcji "--delete-secret-key".
 domy�lny klucz jest domy�lnym adresatem wykorzystanie agenta zarz�dzania has�ami identyfikator do podpisania lub odszyfrowania identyfikator u�ytkownika ,,%s'' zosta� ju� uniewa�niony
 Identyfikator u�ytkownika:  u�ywany jest podklucz %08lX zamiast klucza g��wnego %08lX
 z dodatkowymi informacjami sprawdzenie podpisu klucz s�aby wygenerowano s�aby klucz - operacja zostaje powt�rzona
 %d - dziwny rozmiar jak na zaszyfrowany klucz sesyjny
 zapis podpisu bezpo�redniego
 zapis podpisu wi���cego klucz
 zapisuj� klucz publiczny w '%s'
 zapisuj� klucz tajny w '%s'
 zapis podpisu klucza nim samym
 zapis do '%s'
 zapisywanie na wyj�cie standardowe
 zosta� u�yty niew�a�ciwy klucz prywatny tT tak w trybie --pgp2 mo�na podpisywa� tylko za pomoc� kluczy z wersji 2.x
 kluczami PGP 2 w trybie --pgp2 mo�na podpisywa� tylko do oddzielonych podpis�w
 w trybie --pgp2 mo�na szyfrowa� dla kluczy RSA kr�tszych od 2048 bit�w
 w trybie --pgp2 mo�na sk�ada� tylko podpisy oddzielne lub do��czone do tekstu
 w trybie --pgp2 nie mo�na jednocze�nie szyfrowa� i podpisywa�
 nie mo�na wyznaczu� klucza do uniewa�niania jego samego
 znalaz�e�(a�) b��d w programie ... (%s:%d)
 %s nie jest dost�pne w trybie %s
 szyfr ,,%s'' nie jest dost�pny w trybie %s
 kompresja ,,%s'' nie jest dost�pna w trybie %s
 skr�t ,,%s'' nie jest dost�pny w trybie %s
 w trybie --pgp2 trzeba u�ywa� plik�w a nie potok�w.
 |FD|pisanie opisu stanu do deskryptora FD |PLIK|�adowanie modu�u rozszerzenia z PLIK |HOST|serwer kluczy w kt�rym b�d� poszukiwane |KLUCZ|ustawienie klucza jako ca�kowicie zaufanego |NAZWA|szyfrowanie dla odbiorcy NAZWA |NAZWA| strona kodowa wy�wietlanego tekstu |NAZWA|u�ycie NAZWA jako domy�lnego adresata |NAZWA|ustawienie NAZWA jako domy�lnego klucza prywatnego |NAZWA|wymuszenie algorytmu szyfruj�cego NAZWA |ALG|wymuszenie algorytmu szyfruj�cego ALG dla has�a |NAZWA|wymuszenie algorytmu skr�tu  NAZWA |ALG|wymuszenie algorytmu skr�tu has�a ALG |N|poziom kompresji N (0 - bez) |N|wymuszenie algorytmu kompresji N |N|N-ty tryb obliczania has�a |[plik]|z�o�enie podpisu pod dokumentem |[plik]|z�o�enie podpisu |[plik]|pisanie opisu stanu do pliku |[pliki]|odszyfrowywanie plik�w |[pliki]|szyfrowanie plik�w |algo [pliki]|skr�ty wiadomo�ci 