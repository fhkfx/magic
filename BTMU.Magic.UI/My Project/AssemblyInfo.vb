﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("BTMU-MAGIC-HK")> 
<Assembly: AssemblyDescription("Intelligent Data Conversion Tool")> 
<Assembly: AssemblyCompany("Bank of Tokyo-Mitsubishi UFJ (Hong Kong)")> 
<Assembly: AssemblyProduct("BTMU-MAGIC (Hong Kong)")> 
<Assembly: AssemblyCopyright("Copyright © 2007-2019 MUFG Bank, Ltd. All rights reserved")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("14ffa2a3-5fbc-4c42-912b-6ec07013fa2e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.5.0.2")> 
<Assembly: AssemblyFileVersion("2.5.2")> 
