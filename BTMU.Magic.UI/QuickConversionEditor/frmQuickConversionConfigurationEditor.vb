Imports System.IO
Imports System.Xml.Serialization
Imports BTMU.Magic.Common
''' <summary>
''' Form for Configuring Quick Conversion Settings
''' </summary>
''' <remarks></remarks>
Public Class frmQuickConversionConfigurationEditor
    Private _applicationFolder As String
    Private _configurationMaster As ConfigurationMaster
    Private Shared _msgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)


    Private Sub frmQuickConversionConfigurationEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _applicationFolder = AppDomain.CurrentDomain.BaseDirectory
        txtFileName.Text = _applicationFolder + "magic.xml"
    End Sub

    Private Sub btnCreateNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateNew.Click
        txtFileName.Text = _applicationFolder + "magic.xml"
        If File.Exists(txtFileName.Text) Then
            Dim dlgResult As DialogResult
            dlgResult = MessageBox.Show(String.Format("{0} has exist. Do you want to overwrite the file?", txtFileName.Text), "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If dlgResult = Windows.Forms.DialogResult.Yes Then
                _configurationMaster = New ConfigurationMaster
                WriteConfigurationToFile(_configurationMaster)
            End If
        Else
            _configurationMaster = New ConfigurationMaster
            WriteConfigurationToFile(_configurationMaster)
        End If
    End Sub



    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtConfiguratiorEditor.Text.Trim.Length > 0 Then
                Dim mstream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(txtConfiguratiorEditor.Text.Trim()))
                Dim serializer As New XmlSerializer(GetType(BTMU.Magic.Common.ConfigurationMaster))

                _configurationMaster = serializer.Deserialize(mstream)
                _configurationMaster.Setup1.SetEmptyString()
                _configurationMaster.Setup2.SetEmptyString()
                _configurationMaster.Setup3.SetEmptyString()
                _configurationMaster.Setup4.SetEmptyString()
                _configurationMaster.Setup5.SetEmptyString()
                _configurationMaster.Setup6.SetEmptyString()

                WriteConfigurationToFile(_configurationMaster)

                'Re read the configuration master
                GBLConfigurationMaster = ConfigurationMaster.ReadConfigurationFromFile(AppDomain.CurrentDomain.BaseDirectory + "\magic.xml")

                MessageBox.Show("Data is saved")
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                                    String.Format(_msgReader.GetString("E00000050"), _
                                    "XML Data"), String.Empty)
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
            String.Format(ex.Message, _
            txtFileName.Text), String.Empty)

        End Try
    End Sub

    Private Sub txtFileName_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFileName.Validated
        Try
            ReadConfigurationFromFile()
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
            String.Format(ex.Message, _
            txtFileName.Text), String.Empty)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    Private Sub ReadConfigurationFromFile()
        Try
            If File.Exists(txtFileName.Text) Then
                Dim serializer As New XmlSerializer(GetType(BTMU.Magic.Common.ConfigurationMaster))
                Dim xmlData As String
                Dim cipherText As String = File.ReadAllText(txtFileName.Text)

                If cipherText.Trim.Length > 0 Then
                    Try
                        xmlData = BTMU.Magic.Common.HelperModule.AESDecrypt(cipherText)
                    Catch ex As Exception
                        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                            String.Format(_msgReader.GetString("E00000005"), _
                            txtFileName.Text), String.Empty)
                        Exit Sub
                    End Try
                    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(xmlData))
                    _configurationMaster = serializer.Deserialize(stream)
                    _configurationMaster.Setup1.SetEmptyString()
                    _configurationMaster.Setup2.SetEmptyString()
                    _configurationMaster.Setup3.SetEmptyString()
                    _configurationMaster.Setup4.SetEmptyString()
                    _configurationMaster.Setup5.SetEmptyString()
                    _configurationMaster.Setup6.SetEmptyString()
                    stream.Close()
                    txtConfiguratiorEditor.Text = xmlData
                Else
                    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                    String.Format(_msgReader.GetString("E00000004"), _
                    txtFileName.Text), String.Empty)
                End If
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                String.Format(_msgReader.GetString("E00000003"), _
                txtFileName.Text), String.Empty)
                Exit Sub
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
            String.Format(ex.Message, _
            txtFileName.Text), String.Empty)
        End Try
    End Sub

    Private Sub WriteConfigurationToFile(ByVal configObject As ConfigurationMaster)
        Dim serializer As New XmlSerializer(GetType(BTMU.Magic.Common.ConfigurationMaster))
        Dim memStream As New MemoryStream()
        Dim xmlData As String
        Dim cipherText As String

        serializer.Serialize(memStream, configObject)
        xmlData = System.Text.Encoding.UTF8.GetString(memStream.ToArray())
        memStream.Close()

        cipherText = BTMU.Magic.Common.HelperModule.AESEncrypt(xmlData)
        File.WriteAllText(txtFileName.Text, cipherText)
        txtConfiguratiorEditor.Text = xmlData
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        OpenFileDialog1.InitialDirectory = _applicationFolder
        OpenFileDialog1.FileName = ""
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtFileName.Text = OpenFileDialog1.FileName

            Try
                ReadConfigurationFromFile()
            Catch ex As Exception
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                String.Format(ex.Message, _
                txtFileName.Text), String.Empty)
            End Try
        End If

    End Sub

    Private Sub txtFileName_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtFileName.DragDrop
        'transfer the filenames to a string array
        Dim files As String() = e.Data.GetData(DataFormats.FileDrop)
        Dim file As String

        If files.Length > 0 Then
            For Each file In files
                Dim fileInfo As New System.IO.FileInfo(file)
                If fileInfo.Extension.ToLower = ".xml" Then
                    txtFileName.Text = file

                    Try
                        ReadConfigurationFromFile()
                    Catch ex As Exception
                        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
                        String.Format(ex.Message, _
                        txtFileName.Text), String.Empty)
                    End Try
                    Return
                End If
            Next
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError( _
            String.Format("No valid file found"), String.Empty)

        End If

    End Sub

    Private Sub txtFileName_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtFileName.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop, False)) Then
            ' allow them to continue
            ' (without this, the cursor stays a "NO" symbol
            e.Effect = DragDropEffects.All
        End If
				
    End Sub
End Class