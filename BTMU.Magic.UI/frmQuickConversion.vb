Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.MAGIC.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

''' <summary>
''' This Form displays on application startup and allows user to perform conversion in one mouse click
''' </summary>
''' <remarks></remarks>
Public Class frmQuickConversion

#Region " Private Variables "
    Private _objPreview As BaseFileFormat
    Private _masterTemplateList As MasterTemplateList
    Private _dataModel As ICommonTemplate
    Private setup As New QuickConfigurationItem
    Private _frmConversion As Form
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
#End Region

#Region " Form Events "
    Private Sub frmQuickConversion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Step 1 :   Read the magic.xml for Quick Configuration Setup
            If GBLConfigurationMaster Is Nothing Then
                GBLConfigurationMaster = New ConfigurationMaster
                GBLConfigurationMaster = ConfigurationMaster.ReadConfigurationFromFile(AppDomain.CurrentDomain.BaseDirectory + "\magic.xml")
            End If

            'Step 2 :   Enable/Disable the Setup buttons and set the text based on the Quick Configuration Setup
            SetButtonText(GBLConfigurationMaster.Setup1, btnSetup1)
            SetButtonText(GBLConfigurationMaster.Setup2, btnSetup2)
            SetButtonText(GBLConfigurationMaster.Setup3, btnSetup3)
            SetButtonText(GBLConfigurationMaster.Setup4, btnSetup4)
            SetButtonText(GBLConfigurationMaster.Setup5, btnSetup5)
            SetButtonText(GBLConfigurationMaster.Setup6, btnSetup6)

            'Step 3 :   Set the version as the text for the VersionLabel
            Me.MagciVersionLabel.Text = "Version " & Application.ProductVersion
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on loading Quick Conversion screen : ", ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " Event Handlers "
    ''' <summary>
    ''' This is the event handler for the Setup buttons
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ReadConfigurationSetup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetup1.Click, btnSetup2.Click, btnSetup3.Click, btnSetup4.Click, btnSetup5.Click, btnSetup6.Click
        Try
            Select Case DirectCast(sender, Button).Name
                Case "btnSetup1"
                    setup = GBLConfigurationMaster.Setup1
                Case "btnSetup2"
                    setup = GBLConfigurationMaster.Setup2
                Case "btnSetup3"
                    setup = GBLConfigurationMaster.Setup3
                Case "btnSetup4"
                    setup = GBLConfigurationMaster.Setup4
                Case "btnSetup5"
                    setup = GBLConfigurationMaster.Setup5
                Case "btnSetup6"
                    setup = GBLConfigurationMaster.Setup6
            End Select
            If Not setup Is Nothing Then ConvertSource()
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on converting source file : ", ex.Message.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' This is the event handler for the Manual Conversion buttons
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ShowManualConversionScreen(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnText.Click, btnExcel.Click, btnSWIFT.Click
        Try
            Select Case DirectCast(sender, Button).Name
                Case "btnText"
                    _frmConversion = frmConvertCommonTransactionText
                Case "btnExcel"
                    _frmConversion = frmConvertCommonTransactionExcel
                Case "btnSWIFT"
                    _frmConversion = frmConvertCommonTransactionSWIFT
            End Select
            If _frmConversion Is Nothing Then Exit Sub
            _frmConversion.MdiParent = frmMain
            _frmConversion.Visible = True
            _frmConversion.StartPosition = FormStartPosition.CenterScreen
            _frmConversion.Show()
            _frmConversion.Left = Convert.ToInt32((frmMain.Width - 716) / 2)
            _frmConversion.Top = Convert.ToInt32((frmMain.Height - 340) / 3)
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on loading Manual Conversion screen : ", ex.Message.ToString)
        End Try
        
    End Sub
#End Region

#Region " Private Methods "
    ''' <summary>
    ''' This function is used to set the text for each of the Setup buttons.
    ''' </summary>
    ''' <param name="setup">The Setup whose Template Name is to be taken as text for the button</param>
    ''' <param name="btnSetup">The button whose text is to be set</param>
    ''' <remarks></remarks>
    Private Sub SetButtonText(ByVal setup As QuickConfigurationItem, ByVal btnSetup As Button)
        If Not IsNothingOrEmptyString(setup.TemplateName) Then
            btnSetup.Enabled = True
            btnSetup.Text = TrimmedString(setup.TemplateName)
        Else
            btnSetup.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Assigns Values from the Configuration Settings to the Preview Object
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AssignValues()
        Dim convertedSrcFilename As String = String.Empty
        _objPreview.SourceFormat = TrimmedString(setup.SourceFormat)
        _objPreview.CommonTemplateName = TrimmedString(setup.CommonTemplateName)
        _objPreview.MasterTemplateName = _dataModel.OutputTemplateName
        _objPreview.SourceFileName = TrimmedString(setup.SourceFileName)
        _objPreview.UseValueDate = setup.ValueDateCheck
        If (setup.ValueDateCheck) Then _objPreview.ValueDate = GetValueDate(setup.ValueDate)
        _objPreview.OutputFileName = IIf(IsNothingOrEmptyString(setup.OutputFileName), "", TrimmedString(setup.OutputFileName))
        _objPreview.RemoveRowsFromEnd = IIf(IsNothingOrEmptyString(setup.RemoveRowsFromEnd), 0, setup.RemoveRowsFromEnd)
        _objPreview.EliminatedCharacter = IIf(IsNothingOrEmptyString(setup.EliminateChar), "", TrimmedString(setup.EliminateChar))
        _objPreview.WorksheetName = IIf(IsNothingOrEmptyString(setup.SourceWorkSheet), "", TrimmedString(setup.SourceWorkSheet))
        _objPreview.TransactionEndRow = IIf(IsNothingOrEmptyString(setup.TransactionEndsRow), 0, setup.TransactionEndsRow)
        _objPreview.SaveLocation = IIf(IsNothingOrEmptyString(setup.SaveLocation), GetOutputFileFolder(setup.SourceFormat), TrimmedString(setup.SaveLocation))
        _objPreview.DefinitionFile = IIf(IsNothingOrEmptyString(setup.DefinitionFile), "", TrimmedString(setup.DefinitionFile))
        If _dataModel.DefinitionFile <> String.Empty Then
            Dim objUnstructFileCnv As New UnstructuredFileConverter.UnstructuredFileConverter()
            objUnstructFileCnv.DefinitionFile = _objPreview.DefinitionFile

            convertedSrcFilename = My.Settings.IntermediateFile
            If objUnstructFileCnv.UnstructuredFileConverter(frmLogin.GsUserName, _objPreview.DefinitionFile, _objPreview.SourceFileName, convertedSrcFilename, "D", ";", "{""}") Then
                _objPreview.IntermediaryFileName = convertedSrcFilename
            End If
        End If
    End Sub

    ''' <summary>
    ''' This function is used to convert the source into the required file format
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConvertSource()
        If Not isValidateSetupInfo(setup) Then Exit Sub
        Select Case setup.SourceFormat.Trim.ToUpper
            Case "TXT"
                _dataModel = New CommonTemplateTextDetail
                _dataModel = CommonTemplateModule.LoadTextCommonTemplate(setup.CommonTemplateName)
            Case "EXCEL"
                _dataModel = New CommonTemplateExcelDetail
                _dataModel = CommonTemplateModule.LoadExcelCommonTemplate(setup.CommonTemplateName)
            Case "SWIFT"
                _dataModel = New SwiftTemplate
                _dataModel = CommonTemplateModule.LoadSWIFTCommonTemplate(setup.CommonTemplateName)
        End Select
        _masterTemplateList = CommonTemplateModule.GetMasterTemplateList(_dataModel.OutputTemplateName)
        _objPreview = BaseFileFormat.CreateFileFormat(_masterTemplateList.OutputFormat)
        AssignValues()
        If Not ShowHash() Then Exit Sub
        frmPreview.ShowPreview(_dataModel, _masterTemplateList, True, setup, _objPreview)
        If _objPreview.PreviewDataTable Is Nothing Then Exit Sub
        If _objPreview.PreviewDataTable.Rows.Count = 0 Then Exit Sub
        CheckForValidationErrors()
    End Sub

    ''' <summary>
    ''' Probes for Validation errors, if any
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CheckForValidationErrors()

        ' Step 1 :If there exists error at Mandatory Field then do not proceed
        If _objPreview.IsErrorWithMandatoryFields Then Exit Sub

        If _masterTemplateList.OutputFormat <> OMAKASEFormat.OutputFormatString Then
            ' Step 1 : Proceed to Consolidate Data 
            If setup.ConsolidateData Then
                ' Call the ConsolidateData function
                Dim isError As Boolean = False
                ConsolidateData(setup, _masterTemplateList, isError)
                If isError Then Exit Sub
            End If
        End If

        ' Step 2 :If there exists error at Mandatory Field then do not convert the file
        If _objPreview.IsErrorWithMandatoryFields Then Exit Sub

        ' Step 3 : Refresh the Preview Grid with Consolidated Data
        frmPreview.UcCommonTemplatePreview1.PrepareGridForDisplay()

        ' Step 4 : If there exists error at Mandatory Field then do not convert the file
        If _objPreview.IsErrorWithMandatoryFields Then Exit Sub


        ' Step 5 : For GCMS Format, do not allow to convert the file with more than 1000 records
        If _masterTemplateList.OutputFormat = GCMSFormat.OutputFormatString Then
            If _objPreview.PreviewDataTable.Rows.Count > 1000 Then
                MessageBox.Show(MsgReader.GetString("E09030210"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'MessageBox.Show("The file cannot be converted as the total number of records exceeds 1000!", "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If

        ' Step 6 : If there exists any Validation Error other than Mandatory Field error, proceed to trim the data
        If _objPreview.ValidationErrors.Count > 0 Then
            If setup.TrimData Then
                _objPreview.TrimData()
            Else
                ' Prompt the message
                Dim strmessage As String = "There are still Customer Data Greater than Max Length in Master Template." _
                            & vbNewLine & "Do you want the system to truncate the data on your behalf or, " _
                            & vbNewLine & "you want to modify the data manually* ?" _
                            & vbNewLine & vbNewLine & vbTab & "'Yes', Data will be truncated by the system." _
                            & vbNewLine & vbTab & "'No',  Modify data manually*." _
                            & vbNewLine & vbNewLine & "* If the particular field is set to 'editable'."
                If MessageBox.Show(strmessage, "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    _objPreview.TrimData()
                Else
                    Exit Sub
                End If
            End If
        End If

        ' Step 7 : Proceed to Convert the Data

        frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = False

        _objPreview.DoConvert(_dataModel, _masterTemplateList)

        frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = True
        frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.ResetBindings(False)


        If _objPreview.ValidationErrors.Count > 0 Then
            frmPreview.UcCommonTemplatePreview1.PrepareGridForDisplay()
        Else
            Dim isSuccess As Boolean = False
            isSuccess = ConversionModule.GenerateFile(_dataModel, _masterTemplateList, True, _objPreview)
            frmPreview.Close()
        End If

    End Sub

    ''' <summary>
    ''' Consolidates the Transaction data
    ''' </summary>
    ''' <param name="setup">Configuration details</param>
    ''' <param name="_masterTemplateList">Master Template</param>
    ''' <param name="isError">Determines whether there is an error</param>
    ''' <remarks></remarks>
    Private Sub ConsolidateData(ByVal setup As QuickConfigurationItem, ByVal _masterTemplateList As MasterTemplateList, Optional ByRef isError As Boolean = False)

        Dim groupByFields As New List(Of String)
        Dim referenceFields As New List(Of String)
        Dim appendFields As New List(Of String)

        Dim _paymentOption As String = String.Empty

        isError = False
        Try
            '#1. Group By Fields
            If IsNothingOrEmptyString(setup.ConsolidateGroupFields) Then
                BTMUExceptionManager.LogAndShowError("Error in Quick Configuration Setting", String.Format(MsgReader.GetString("E03030030"), "Consolidation Group By Fields"))
                isError = True
                Exit Sub
            End If

            Dim fields() As String = setup.ConsolidateGroupFields.Split(",")
            If Not (fields.Length = 1 AndAlso fields(0).Trim() = String.Empty) Then
                For Each field As String In fields
                    groupByFields.Add(field.Trim())
                Next

            End If

            '#2. Reference Fields
            fields = setup.ConsolidateRefField.Split(",")
            If Not (fields.Length = 1 AndAlso fields(0).Trim() = String.Empty) Then
                For Each field As String In fields
                    referenceFields.Add(field.Trim())
                Next
            End If

            '#3. "Append To Field" Values
            fields = setup.ConsolidateRefAppend.Split(",")
            If Not (fields.Length = 1 AndAlso fields(0).Trim() = String.Empty) Then
                For Each field As String In fields
                    appendFields.Add(field.Trim())
                Next
            End If


            '#4. Reference Fields
            If appendFields.Count > referenceFields.Count Then
                BTMUExceptionManager.LogAndShowError("Error in Quick Configuration Setting", String.Format(MsgReader.GetString("E03030030"), "Consolidation Reference and Append Fields"))
                isError = True
                Exit Sub
            End If

            '2. Fixed quick conversion field name
            For Each fld As String In groupByFields
                If fld.Trim.Length <> 0 Then
                    For Each temp As String In _objPreview.TblBankFields.Keys
                        If temp.ToUpper = fld.ToUpper Then
                            fld = temp
                        End If
                    Next
                End If
            Next
            For Each fld As String In referenceFields
                If fld.Trim.Length <> 0 Then
                    For Each temp As String In _objPreview.TblBankFields.Keys
                        If temp.ToUpper = fld.ToUpper Then
                            fld = temp
                        End If
                    Next
                End If
            Next


            '#3.    Read Payment Option
            _objPreview.IsNegativePaymentOption = IIf(setup.ConsolidationPaymentMode = "-", True, False)

            '#4.    Read Consolidation Amount
            _objPreview.IsCheckedSumAmount = setup.ConsolidationAmount

            _objPreview.IsConsolidated = True
            _objPreview.PaymentOption = _paymentOption
            _objPreview.IsNegativePaymentOption = IIf(setup.ConsolidationPaymentMode = "-", True, False)

            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = False

            _objPreview.GenerateConsolidatedData(groupByFields, referenceFields, appendFields)

            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = True
            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.ResetBindings(False)

        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error while consolidating records", ex.Message)
            isError = True
        End Try

    End Sub

    ''' <summary>
    ''' This function is to get the default output file folder
    ''' </summary>
    ''' <param name="sourceFormat"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetOutputFileFolder(ByVal sourceFormat As String) As String
        Select Case sourceFormat.Trim.ToUpper
            Case "TXT", "EXCEL"
                Return My.Settings.ConvertCommonFolder
            Case "SWIFT"
                Return My.Settings.ConvertSWIFTFolder
        End Select
        Return My.Settings.ConvertCommonFolder
    End Function

    ''' <summary>
    ''' This function is to show the hash value of the Source File
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ShowHash() As Boolean
        If _masterTemplateList.OutputFormat = GCMSFormat.OutputFormatString AndAlso My.Settings.GCMSHashing = "True" Then
            Dim objHashing As New frmGCMSHashingView
            objHashing.GetHash(setup.SourceFileName)
            If objHashing.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' Validates Configuration Setup information
    ''' </summary>
    ''' <param name="setup">Configuration Setup information</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function isValidateSetupInfo(ByVal setup As QuickConfigurationItem) As Boolean
        Dim isCommonTemplateFound As Boolean = False
        Try
            'Step 1 :   Check if the specified common template name exists
            If IsNothingOrEmptyString(setup.SourceFormat) Then
                MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Source Format"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

            'Step 2 :   Check if Common Template Name is specified
            If IsNothingOrEmptyString(setup.CommonTemplateName) Or _
                       setup.CommonTemplateName.Trim.ToUpper = "No Value".ToUpper Then
                MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Common Template Name"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

            'Step 3 :   Check if Source File Name is specified
            If IsNothingOrEmptyString(setup.SourceFileName) Or _
                       setup.SourceFileName.Trim.ToUpper = "No Value".ToUpper Then
                MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Source File Name"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

            'Step 4 :   Check if the Save Location specified exists
            If IsNothingOrEmptyString(setup.SaveLocation) Or _
                               setup.SaveLocation.Trim.ToUpper = "No Value".ToUpper Then
                'MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Save Location"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Return False
            Else
                If Not System.IO.Directory.Exists(setup.SaveLocation) Then
                    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "save location does not exist"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
            End If

            'Step 5 :   Check Value Date
            If setup.ValueDateCheck Then
                If IsNothingOrEmptyString(setup.ValueDate) Then
                    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Value Date"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
                If Not IsNumeric(setup.ValueDate) Then
                    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Value Date"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
            End If

            'Step 6 :   Check Output File Name
            If IsNothingOrEmptyString(setup.OutputFileName) Then setup.OutputFileName = ""

            If Not IsNothingOrEmptyString(setup.OutputFileName) Or _
                                       setup.OutputFileName.Trim.ToUpper <> "No Value".ToUpper Then
                If Not IsValidFileName(setup.OutputFileName) Then
                    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Output File Name is invalid"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Conversion process > error in Setup configuration value - Output File Name is invalid.", "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
            End If

            Select Case setup.SourceFormat.ToUpper
                Case "TXT"
                    ' Step 1 : Common Template should exist and enabled
                    For Each item As CommonTemplate.TextTemplateList In CommonTemplateModule.ListAllTextCommonTemplates
                        If item.CommonTemplate.Trim.ToUpper = setup.CommonTemplateName.Trim.ToUpper Then
                            isCommonTemplateFound = True
                            Exit For
                        End If
                    Next
                    If Not isCommonTemplateFound Then
                        MessageBox.Show(String.Format(MsgReader.GetString("E03030040"), "Common Template does not exist or is disabled"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'MessageBox.Show("Conversion process --> Common Template does not exist or is disabled.", "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                    'Step 2 :   Check Remove Rows from End
                    If Not IsNumeric(setup.RemoveRowsFromEnd) Then
                        MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Remove Rows From End"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                    'Step 3 :   Check Eliminate Character
                    If IsNothingOrEmptyString(setup.EliminateChar) Then
                        setup.EliminateChar = ""
                        '_objPreview.EliminatedCharacter = setup.EliminateChar
                    End If
                    'Step 4 : Validate mdt file
                    If Not IsNothingOrEmptyString(setup.DefinitionFile) Then
                        If Not System.IO.File.Exists(setup.DefinitionFile) Then
                            MessageBox.Show(String.Format(MsgReader.GetString("E03030040"), "Definition File does not exist"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'MessageBox.Show("Definition File does not exist.", "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Return False
                        End If
                    End If

                Case "EXCEL"
                    ' Step 1 : Common Template should exist and enabled
                    For Each item As CommonTemplate.CommonTemplateExcelList In CommonTemplateModule.ListAllExcelCommonTemplates
                        If item.CommonTemplate.Trim.ToUpper = setup.CommonTemplateName.Trim.ToUpper Then
                            isCommonTemplateFound = True
                            Exit For
                        End If
                    Next
                    If Not isCommonTemplateFound Then
                        MessageBox.Show(String.Format(MsgReader.GetString("E03030040"), "Common Template does not exist or is disabled"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'MessageBox.Show("Conversion process --> Common Template does not exist or is disabled.", "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                    'Step 2 :   Check Transaction End Row
                    'If Not IsNumeric(setup.TransactionEndsRow) Then
                    '    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Transaction Ends at Row"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    '    Return False
                    'End If
                    'Step 3 :   Check Worksheet name
                    If IsNothingOrEmptyString(setup.SourceWorkSheet) Then
                        MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Worksheet Name"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                    'Step 4 :   Worksheet Name should exist in the Source File
                    Dim worksheetdatatable As DataTable
                    Dim isWorksheetFound As Boolean = False
                    If Not IsNothingOrEmptyString(setup.SourceFileName) Then
                        worksheetdatatable = ExcelHelperModule.GetWorksheetNames(setup.SourceFileName)
                        For Each row As DataRow In worksheetdatatable.Rows
                            If IsNothingOrEmptyString(setup.SourceWorkSheet) Then Exit For
                            If row("WorksheetName").ToString.ToUpper = setup.SourceWorkSheet.ToString.ToUpper Then
                                isWorksheetFound = True
                                Exit For
                            End If
                        Next
                        If Not isWorksheetFound Then
                            MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Worksheet Name"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Return False
                        End If
                    End If

                Case "SWIFT"
                    ' Step 1 : Common Template should exist and enabled
                    For Each item As CommonTemplate.SwiftTemplateList In CommonTemplateModule.ListAllSWIFTCommonTemplates
                        If item.SwiftTemplate.Trim.ToUpper = setup.CommonTemplateName.Trim.ToUpper Then
                            isCommonTemplateFound = True
                            Exit For
                        End If
                    Next
                    If Not isCommonTemplateFound Then
                        MessageBox.Show(String.Format(MsgReader.GetString("E03030040"), "Common Template does not exist or is disabled"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'MessageBox.Show("Conversion process --> Common Template does not exist or is disabled.", "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                Case Else
                    MessageBox.Show(String.Format(MsgReader.GetString("E03030020"), "Source Format"), "Quick Conversion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
            End Select
            Return True
        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error on Quick Conversion", ex.Message)
            Return False
        End Try
    End Function

#End Region
    
End Class