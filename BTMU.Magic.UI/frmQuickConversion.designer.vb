<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuickConversion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQuickConversion))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSetup6 = New System.Windows.Forms.Button
        Me.btnSetup3 = New System.Windows.Forms.Button
        Me.btnSetup5 = New System.Windows.Forms.Button
        Me.btnSetup2 = New System.Windows.Forms.Button
        Me.btnSetup4 = New System.Windows.Forms.Button
        Me.btnSetup1 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnSWIFT = New System.Windows.Forms.Button
        Me.btnExcel = New System.Windows.Forms.Button
        Me.btnText = New System.Windows.Forms.Button
        Me.MagciVersionLabel = New System.Windows.Forms.Label
        Me.pnlFill = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.pnlFill.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = Global.BTMU.Magic.UI.My.Resources.Resources.Magic_About
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(590, 188)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnSetup6)
        Me.GroupBox1.Controls.Add(Me.btnSetup3)
        Me.GroupBox1.Controls.Add(Me.btnSetup5)
        Me.GroupBox1.Controls.Add(Me.btnSetup2)
        Me.GroupBox1.Controls.Add(Me.btnSetup4)
        Me.GroupBox1.Controls.Add(Me.btnSetup1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(525, 199)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Quick Conversion"
        '
        'btnSetup6
        '
        Me.btnSetup6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup6.Location = New System.Drawing.Point(359, 117)
        Me.btnSetup6.Name = "btnSetup6"
        Me.btnSetup6.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup6.TabIndex = 5
        Me.btnSetup6.Tag = ""
        Me.btnSetup6.Text = "Setup 6"
        Me.btnSetup6.UseVisualStyleBackColor = True
        '
        'btnSetup3
        '
        Me.btnSetup3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup3.Location = New System.Drawing.Point(359, 32)
        Me.btnSetup3.Name = "btnSetup3"
        Me.btnSetup3.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup3.TabIndex = 4
        Me.btnSetup3.Tag = ""
        Me.btnSetup3.Text = "Setup 3"
        Me.btnSetup3.UseVisualStyleBackColor = True
        '
        'btnSetup5
        '
        Me.btnSetup5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup5.Location = New System.Drawing.Point(190, 117)
        Me.btnSetup5.Name = "btnSetup5"
        Me.btnSetup5.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup5.TabIndex = 3
        Me.btnSetup5.Tag = ""
        Me.btnSetup5.Text = "Setup 5"
        Me.btnSetup5.UseVisualStyleBackColor = True
        '
        'btnSetup2
        '
        Me.btnSetup2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup2.Location = New System.Drawing.Point(190, 32)
        Me.btnSetup2.Name = "btnSetup2"
        Me.btnSetup2.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup2.TabIndex = 2
        Me.btnSetup2.Tag = ""
        Me.btnSetup2.Text = "Setup 2"
        Me.btnSetup2.UseVisualStyleBackColor = True
        '
        'btnSetup4
        '
        Me.btnSetup4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup4.Location = New System.Drawing.Point(19, 117)
        Me.btnSetup4.Name = "btnSetup4"
        Me.btnSetup4.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup4.TabIndex = 1
        Me.btnSetup4.Tag = ""
        Me.btnSetup4.Text = "Setup 4"
        Me.btnSetup4.UseVisualStyleBackColor = True
        '
        'btnSetup1
        '
        Me.btnSetup1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetup1.Location = New System.Drawing.Point(19, 32)
        Me.btnSetup1.Name = "btnSetup1"
        Me.btnSetup1.Size = New System.Drawing.Size(147, 56)
        Me.btnSetup1.TabIndex = 0
        Me.btnSetup1.Tag = ""
        Me.btnSetup1.Text = "Setup 1"
        Me.btnSetup1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnSWIFT)
        Me.GroupBox2.Controls.Add(Me.btnExcel)
        Me.GroupBox2.Controls.Add(Me.btnText)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 248)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(525, 95)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Manual Conversion"
        '
        'btnSWIFT
        '
        Me.btnSWIFT.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSWIFT.Location = New System.Drawing.Point(359, 20)
        Me.btnSWIFT.Name = "btnSWIFT"
        Me.btnSWIFT.Size = New System.Drawing.Size(147, 56)
        Me.btnSWIFT.TabIndex = 4
        Me.btnSWIFT.Text = "SWIFT"
        Me.btnSWIFT.UseVisualStyleBackColor = True
        '
        'btnExcel
        '
        Me.btnExcel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExcel.Location = New System.Drawing.Point(190, 20)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(147, 56)
        Me.btnExcel.TabIndex = 3
        Me.btnExcel.Text = "Excel"
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'btnText
        '
        Me.btnText.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnText.Location = New System.Drawing.Point(19, 20)
        Me.btnText.Name = "btnText"
        Me.btnText.Size = New System.Drawing.Size(147, 56)
        Me.btnText.TabIndex = 2
        Me.btnText.Text = "Text"
        Me.btnText.UseVisualStyleBackColor = True
        '
        'MagciVersionLabel
        '
        Me.MagciVersionLabel.AutoSize = True
        Me.MagciVersionLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MagciVersionLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MagciVersionLabel.Location = New System.Drawing.Point(500, 10)
        Me.MagciVersionLabel.Name = "MagciVersionLabel"
        Me.MagciVersionLabel.Size = New System.Drawing.Size(86, 14)
        Me.MagciVersionLabel.TabIndex = 3
        Me.MagciVersionLabel.Text = "Magic Version"
        Me.MagciVersionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'pnlFill
        '
        Me.pnlFill.Controls.Add(Me.Label1)
        Me.pnlFill.Controls.Add(Me.MagciVersionLabel)
        Me.pnlFill.Controls.Add(Me.GroupBox2)
        Me.pnlFill.Controls.Add(Me.GroupBox1)
        Me.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFill.Location = New System.Drawing.Point(0, 188)
        Me.pnlFill.Name = "pnlFill"
        Me.pnlFill.Size = New System.Drawing.Size(590, 387)
        Me.pnlFill.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(239, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 22)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "East Asia"
        '
        'frmQuickConversion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 575)
        Me.Controls.Add(Me.pnlFill)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQuickConversion"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Conversion Menu"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.pnlFill.ResumeLayout(False)
        Me.pnlFill.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSetup6 As System.Windows.Forms.Button
    Friend WithEvents btnSetup3 As System.Windows.Forms.Button
    Friend WithEvents btnSetup5 As System.Windows.Forms.Button
    Friend WithEvents btnSetup2 As System.Windows.Forms.Button
    Friend WithEvents btnSetup4 As System.Windows.Forms.Button
    Friend WithEvents btnSetup1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSWIFT As System.Windows.Forms.Button
    Friend WithEvents btnExcel As System.Windows.Forms.Button
    Friend WithEvents btnText As System.Windows.Forms.Button
    Friend WithEvents MagciVersionLabel As System.Windows.Forms.Label
    Friend WithEvents pnlFill As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
