Imports BTMU.Magic.Common
Public Class frmMasterTemplateNonFixedBase


    Private Sub ucMTNonFixedView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ucMTNonFixedView1.Load
        ShowView()
    End Sub

    Private Sub frmMasterTemplateNonFixedBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ucMTNonFixedView1.ContainerForm = Me
        ucMTNonFixedDetail1.ContainerForm = Me
    End Sub

    Public Sub ShowDetail()
        ucMTNonFixedView1.Visible = False
        ucMTNonFixedDetail1.Visible = True
    End Sub

    Public Sub ShowView()
        ucMTNonFixedView1.Visible = True
        ucMTNonFixedDetail1.Visible = False
    End Sub

    Private Sub frmMasterTemplateNonFixedBase_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If ucMTNonFixedDetail1.Visible And ucMTNonFixedDetail1.RecordState <> "VIEW" Then
            Dim dlgResult As DialogResult = ucMTNonFixedDetail1.ProcessDataDirtyConfirmation()

            If dlgResult = DialogResult.Yes Then
                'Do nothing
            ElseIf dlgResult = DialogResult.No Then
                'Do nothing
            Else
                e.Cancel = True
            End If

        End If
    End Sub


    Private Sub ucMTNonFixedDetail1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ucMTNonFixedDetail1.Load

    End Sub
End Class
