<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMasterTemplateFixedView
    Inherits ucMasterTemplateFixedBase ' System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tpList = New System.Windows.Forms.TabPage
        Me.btnFilter = New System.Windows.Forms.Button
        Me.btnView = New System.Windows.Forms.Button
        Me.MasterTemplateListDataGridView = New System.Windows.Forms.DataGridView
        Me.MasterTemplateFixedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnDuplicate = New System.Windows.Forms.Button
        Me.FilterByLabel = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.TemplateNameLabel = New System.Windows.Forms.Label
        Me.btnAdd = New System.Windows.Forms.Button
        Me.FilterTextBox = New System.Windows.Forms.TextBox
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.btnFilterDraft = New System.Windows.Forms.Button
        Me.btnViewDraft = New System.Windows.Forms.Button
        Me.btnCloseDraft = New System.Windows.Forms.Button
        Me.btnAddDraft = New System.Windows.Forms.Button
        Me.MasterTemplateFixedDraftDataGridView = New System.Windows.Forms.DataGridView
        Me.MasterTemplateFixedDraftBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnDuplicateDraft = New System.Windows.Forms.Button
        Me.btnEditDraft = New System.Windows.Forms.Button
        Me.FilterByDraftLabel = New System.Windows.Forms.Label
        Me.TemplateNameDraftLabel = New System.Windows.Forms.Label
        Me.FilterDraftTextBox = New System.Windows.Forms.TextBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.TemplateNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TemplateStatusDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.OutputFormatDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.TabControl1.SuspendLayout()
        Me.tpList.SuspendLayout()
        CType(Me.MasterTemplateListDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateFixedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.MasterTemplateFixedDraftDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateFixedDraftBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpList)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1161, 630)
        Me.TabControl1.TabIndex = 0
        '
        'tpList
        '
        Me.tpList.AutoScroll = True
        Me.tpList.Controls.Add(Me.btnFilter)
        Me.tpList.Controls.Add(Me.btnView)
        Me.tpList.Controls.Add(Me.MasterTemplateListDataGridView)
        Me.tpList.Controls.Add(Me.btnDuplicate)
        Me.tpList.Controls.Add(Me.FilterByLabel)
        Me.tpList.Controls.Add(Me.btnClose)
        Me.tpList.Controls.Add(Me.btnEdit)
        Me.tpList.Controls.Add(Me.TemplateNameLabel)
        Me.tpList.Controls.Add(Me.btnAdd)
        Me.tpList.Controls.Add(Me.FilterTextBox)
        Me.tpList.Location = New System.Drawing.Point(4, 23)
        Me.tpList.Name = "tpList"
        Me.tpList.Padding = New System.Windows.Forms.Padding(3)
        Me.tpList.Size = New System.Drawing.Size(1153, 603)
        Me.tpList.TabIndex = 0
        Me.tpList.Text = "View"
        Me.tpList.UseVisualStyleBackColor = True
        '
        'btnFilter
        '
        Me.btnFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilter.Location = New System.Drawing.Point(900, 36)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(87, 29)
        Me.btnFilter.TabIndex = 1
        Me.btnFilter.Text = "Filter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'btnView
        '
        Me.btnView.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnView.Location = New System.Drawing.Point(97, 555)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(87, 40)
        Me.btnView.TabIndex = 3
        Me.btnView.Text = "&View"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'MasterTemplateListDataGridView
        '
        Me.MasterTemplateListDataGridView.AllowUserToAddRows = False
        Me.MasterTemplateListDataGridView.AllowUserToDeleteRows = False
        Me.MasterTemplateListDataGridView.AllowUserToResizeRows = False
        Me.MasterTemplateListDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MasterTemplateListDataGridView.AutoGenerateColumns = False
        Me.MasterTemplateListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.MasterTemplateListDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn3, Me.DataGridViewCheckBoxColumn2})
        Me.MasterTemplateListDataGridView.DataSource = Me.MasterTemplateFixedBindingSource
        Me.MasterTemplateListDataGridView.Location = New System.Drawing.Point(3, 86)
        Me.MasterTemplateListDataGridView.Name = "MasterTemplateListDataGridView"
        Me.MasterTemplateListDataGridView.ReadOnly = True
        Me.MasterTemplateListDataGridView.RowHeadersVisible = False
        Me.MasterTemplateListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MasterTemplateListDataGridView.Size = New System.Drawing.Size(1144, 462)
        Me.MasterTemplateListDataGridView.TabIndex = 1
        '
        'MasterTemplateFixedBindingSource
        '
        Me.MasterTemplateFixedBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateNonFixed)
        '
        'btnDuplicate
        '
        Me.btnDuplicate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDuplicate.Location = New System.Drawing.Point(283, 555)
        Me.btnDuplicate.Name = "btnDuplicate"
        Me.btnDuplicate.Size = New System.Drawing.Size(87, 40)
        Me.btnDuplicate.TabIndex = 5
        Me.btnDuplicate.Text = "Dupli&cate"
        Me.btnDuplicate.UseVisualStyleBackColor = True
        '
        'FilterByLabel
        '
        Me.FilterByLabel.AutoSize = True
        Me.FilterByLabel.Location = New System.Drawing.Point(15, 17)
        Me.FilterByLabel.Name = "FilterByLabel"
        Me.FilterByLabel.Size = New System.Drawing.Size(46, 14)
        Me.FilterByLabel.TabIndex = 7
        Me.FilterByLabel.Text = "Filter By"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(376, 555)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 40)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.Location = New System.Drawing.Point(190, 555)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(87, 40)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "E&dit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'TemplateNameLabel
        '
        Me.TemplateNameLabel.AutoSize = True
        Me.TemplateNameLabel.Location = New System.Drawing.Point(15, 43)
        Me.TemplateNameLabel.Name = "TemplateNameLabel"
        Me.TemplateNameLabel.Size = New System.Drawing.Size(83, 14)
        Me.TemplateNameLabel.TabIndex = 8
        Me.TemplateNameLabel.Text = "Template Name:"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(4, 555)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(87, 40)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "&New "
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'FilterTextBox
        '
        Me.FilterTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterTextBox.Location = New System.Drawing.Point(153, 39)
        Me.FilterTextBox.MaxLength = 100
        Me.FilterTextBox.Name = "FilterTextBox"
        Me.FilterTextBox.Size = New System.Drawing.Size(741, 20)
        Me.FilterTextBox.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.AutoScroll = True
        Me.TabPage1.Controls.Add(Me.btnFilterDraft)
        Me.TabPage1.Controls.Add(Me.btnViewDraft)
        Me.TabPage1.Controls.Add(Me.btnCloseDraft)
        Me.TabPage1.Controls.Add(Me.btnAddDraft)
        Me.TabPage1.Controls.Add(Me.MasterTemplateFixedDraftDataGridView)
        Me.TabPage1.Controls.Add(Me.btnDuplicateDraft)
        Me.TabPage1.Controls.Add(Me.btnEditDraft)
        Me.TabPage1.Controls.Add(Me.FilterByDraftLabel)
        Me.TabPage1.Controls.Add(Me.TemplateNameDraftLabel)
        Me.TabPage1.Controls.Add(Me.FilterDraftTextBox)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(1153, 603)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "View Draft"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnFilterDraft
        '
        Me.btnFilterDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterDraft.Location = New System.Drawing.Point(900, 36)
        Me.btnFilterDraft.Name = "btnFilterDraft"
        Me.btnFilterDraft.Size = New System.Drawing.Size(87, 29)
        Me.btnFilterDraft.TabIndex = 1
        Me.btnFilterDraft.Text = "Filter"
        Me.btnFilterDraft.UseVisualStyleBackColor = True
        '
        'btnViewDraft
        '
        Me.btnViewDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnViewDraft.Location = New System.Drawing.Point(97, 554)
        Me.btnViewDraft.Name = "btnViewDraft"
        Me.btnViewDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnViewDraft.TabIndex = 3
        Me.btnViewDraft.Text = "&View"
        Me.btnViewDraft.UseVisualStyleBackColor = True
        '
        'btnCloseDraft
        '
        Me.btnCloseDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCloseDraft.Location = New System.Drawing.Point(376, 554)
        Me.btnCloseDraft.Name = "btnCloseDraft"
        Me.btnCloseDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnCloseDraft.TabIndex = 6
        Me.btnCloseDraft.Text = "Close"
        Me.btnCloseDraft.UseVisualStyleBackColor = True
        '
        'btnAddDraft
        '
        Me.btnAddDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddDraft.Location = New System.Drawing.Point(4, 554)
        Me.btnAddDraft.Name = "btnAddDraft"
        Me.btnAddDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnAddDraft.TabIndex = 2
        Me.btnAddDraft.Text = "&New "
        Me.btnAddDraft.UseVisualStyleBackColor = True
        '
        'MasterTemplateFixedDraftDataGridView
        '
        Me.MasterTemplateFixedDraftDataGridView.AllowUserToAddRows = False
        Me.MasterTemplateFixedDraftDataGridView.AllowUserToDeleteRows = False
        Me.MasterTemplateFixedDraftDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MasterTemplateFixedDraftDataGridView.AutoGenerateColumns = False
        Me.MasterTemplateFixedDraftDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.MasterTemplateFixedDraftDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TemplateNameDataGridViewTextBoxColumn, Me.DescriptionDataGridViewTextBoxColumn, Me.TemplateStatusDataGridViewCheckBoxColumn, Me.OutputFormatDataGridViewTextBoxColumn, Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn})
        Me.MasterTemplateFixedDraftDataGridView.DataSource = Me.MasterTemplateFixedDraftBindingSource
        Me.MasterTemplateFixedDraftDataGridView.Location = New System.Drawing.Point(3, 86)
        Me.MasterTemplateFixedDraftDataGridView.Name = "MasterTemplateFixedDraftDataGridView"
        Me.MasterTemplateFixedDraftDataGridView.ReadOnly = True
        Me.MasterTemplateFixedDraftDataGridView.RowHeadersVisible = False
        Me.MasterTemplateFixedDraftDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MasterTemplateFixedDraftDataGridView.Size = New System.Drawing.Size(1144, 461)
        Me.MasterTemplateFixedDraftDataGridView.TabIndex = 1
        '
        'MasterTemplateFixedDraftBindingSource
        '
        Me.MasterTemplateFixedDraftBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateList)
        '
        'btnDuplicateDraft
        '
        Me.btnDuplicateDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDuplicateDraft.Location = New System.Drawing.Point(283, 554)
        Me.btnDuplicateDraft.Name = "btnDuplicateDraft"
        Me.btnDuplicateDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnDuplicateDraft.TabIndex = 5
        Me.btnDuplicateDraft.Text = "Dupli&cate"
        Me.btnDuplicateDraft.UseVisualStyleBackColor = True
        '
        'btnEditDraft
        '
        Me.btnEditDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditDraft.Location = New System.Drawing.Point(190, 554)
        Me.btnEditDraft.Name = "btnEditDraft"
        Me.btnEditDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnEditDraft.TabIndex = 4
        Me.btnEditDraft.Text = "E&dit"
        Me.btnEditDraft.UseVisualStyleBackColor = True
        '
        'FilterByDraftLabel
        '
        Me.FilterByDraftLabel.AutoSize = True
        Me.FilterByDraftLabel.Location = New System.Drawing.Point(15, 17)
        Me.FilterByDraftLabel.Name = "FilterByDraftLabel"
        Me.FilterByDraftLabel.Size = New System.Drawing.Size(46, 14)
        Me.FilterByDraftLabel.TabIndex = 7
        Me.FilterByDraftLabel.Text = "Filter By"
        '
        'TemplateNameDraftLabel
        '
        Me.TemplateNameDraftLabel.AutoSize = True
        Me.TemplateNameDraftLabel.Location = New System.Drawing.Point(15, 43)
        Me.TemplateNameDraftLabel.Name = "TemplateNameDraftLabel"
        Me.TemplateNameDraftLabel.Size = New System.Drawing.Size(83, 14)
        Me.TemplateNameDraftLabel.TabIndex = 8
        Me.TemplateNameDraftLabel.Text = "Template Name:"
        '
        'FilterDraftTextBox
        '
        Me.FilterDraftTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterDraftTextBox.Location = New System.Drawing.Point(153, 39)
        Me.FilterDraftTextBox.MaxLength = 100
        Me.FilterDraftTextBox.Name = "FilterDraftTextBox"
        Me.FilterDraftTextBox.Size = New System.Drawing.Size(741, 20)
        Me.FilterDraftTextBox.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "TemplateName"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Template Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "TemplateStatus"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Enabled"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "OutputFormat"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Output Format"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "EncryptCustomerOutputFile"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "Encryption Status"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.ReadOnly = True
        Me.DataGridViewCheckBoxColumn2.Width = 120
        '
        'TemplateNameDataGridViewTextBoxColumn
        '
        Me.TemplateNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TemplateNameDataGridViewTextBoxColumn.DataPropertyName = "TemplateName"
        Me.TemplateNameDataGridViewTextBoxColumn.HeaderText = "Template Name"
        Me.TemplateNameDataGridViewTextBoxColumn.Name = "TemplateNameDataGridViewTextBoxColumn"
        Me.TemplateNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DescriptionDataGridViewTextBoxColumn
        '
        Me.DescriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.HeaderText = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.Name = "DescriptionDataGridViewTextBoxColumn"
        Me.DescriptionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TemplateStatusDataGridViewCheckBoxColumn
        '
        Me.TemplateStatusDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TemplateStatusDataGridViewCheckBoxColumn.DataPropertyName = "TemplateStatus"
        Me.TemplateStatusDataGridViewCheckBoxColumn.HeaderText = "Enabled"
        Me.TemplateStatusDataGridViewCheckBoxColumn.Name = "TemplateStatusDataGridViewCheckBoxColumn"
        Me.TemplateStatusDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'OutputFormatDataGridViewTextBoxColumn
        '
        Me.OutputFormatDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OutputFormatDataGridViewTextBoxColumn.DataPropertyName = "OutputFormat"
        Me.OutputFormatDataGridViewTextBoxColumn.HeaderText = "Output Format"
        Me.OutputFormatDataGridViewTextBoxColumn.Name = "OutputFormatDataGridViewTextBoxColumn"
        Me.OutputFormatDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EncryptCustomerOutputFileDataGridViewCheckBoxColumn
        '
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn.DataPropertyName = "EncryptCustomerOutputFile"
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn.HeaderText = "Encryption Status"
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn.Name = "EncryptCustomerOutputFileDataGridViewCheckBoxColumn"
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn.ReadOnly = True
        Me.EncryptCustomerOutputFileDataGridViewCheckBoxColumn.Width = 120
        '
        'ucMasterTemplateFixedView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucMasterTemplateFixedView"
        Me.Size = New System.Drawing.Size(1161, 630)
        Me.TabControl1.ResumeLayout(False)
        Me.tpList.ResumeLayout(False)
        Me.tpList.PerformLayout()
        CType(Me.MasterTemplateListDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateFixedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.MasterTemplateFixedDraftDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateFixedDraftBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpList As System.Windows.Forms.TabPage
    Friend WithEvents FilterByLabel As System.Windows.Forms.Label
    Friend WithEvents btnDuplicate As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents TemplateNameLabel As System.Windows.Forms.Label
    Friend WithEvents FilterTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnDuplicateDraft As System.Windows.Forms.Button
    Friend WithEvents btnEditDraft As System.Windows.Forms.Button
    Friend WithEvents FilterByDraftLabel As System.Windows.Forms.Label
    Friend WithEvents TemplateNameDraftLabel As System.Windows.Forms.Label
    Friend WithEvents FilterDraftTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents MasterTemplateListDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MasterTemplateFixedDraftDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MasterTemplateFixedDraftBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents DraftDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnViewDraft As System.Windows.Forms.Button
    Friend WithEvents btnCloseDraft As System.Windows.Forms.Button
    Friend WithEvents btnAddDraft As System.Windows.Forms.Button
    Friend WithEvents MasterTemplateFixedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents btnFilterDraft As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents TemplateNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TemplateStatusDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents OutputFormatDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EncryptCustomerOutputFileDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class
