<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterTemplateNonFixedBase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterTemplateNonFixedBase))
        Me.ucMTNonFixedView1 = New BTMU.Magic.UI.ucMasterTemplateNonFixedView
        Me.ucMTNonFixedDetail1 = New BTMU.Magic.UI.ucMasterTemplateNonFixedDetail
        Me.SuspendLayout()
        '
        'ucMTNonFixedView1
        '
        Me.ucMTNonFixedView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucMTNonFixedView1.ContainerForm = Nothing
        Me.ucMTNonFixedView1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucMTNonFixedView1.IsNew = False
        Me.ucMTNonFixedView1.Location = New System.Drawing.Point(-2, 0)
        Me.ucMTNonFixedView1.Name = "ucMTNonFixedView1"
        Me.ucMTNonFixedView1.Size = New System.Drawing.Size(1031, 609)
        Me.ucMTNonFixedView1.TabIndex = 0
        '
        'ucMTNonFixedDetail1
        '
        Me.ucMTNonFixedDetail1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucMTNonFixedDetail1.ContainerForm = Nothing
        Me.ucMTNonFixedDetail1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucMTNonFixedDetail1.IsDraft = False
        Me.ucMTNonFixedDetail1.IsNew = False
        Me.ucMTNonFixedDetail1.Location = New System.Drawing.Point(-2, 0)
        Me.ucMTNonFixedDetail1.Name = "ucMTNonFixedDetail1"
        Me.ucMTNonFixedDetail1.RecordState = Nothing
        Me.ucMTNonFixedDetail1.Size = New System.Drawing.Size(1030, 606)
        Me.ucMTNonFixedDetail1.TabIndex = 1
        '
        'frmMasterTemplateNonFixedBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 608)
        Me.Controls.Add(Me.ucMTNonFixedDetail1)
        Me.Controls.Add(Me.ucMTNonFixedView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMasterTemplateNonFixedBase"
        Me.ShowIcon = False
        Me.Text = "Generate Master Template for Non-Fixed Width Format (MA1000)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ucMTNonFixedView1 As BTMU.Magic.UI.ucMasterTemplateNonFixedView
    Friend WithEvents ucMTNonFixedDetail1 As BTMU.Magic.UI.ucMasterTemplateNonFixedDetail

End Class
