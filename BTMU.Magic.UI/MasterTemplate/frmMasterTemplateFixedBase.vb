Imports BTMU.Magic.Common

Public Class frmMasterTemplateFixedBase
    'public Const 

    Private Sub ucMTNonFixedView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ucMTFixedView1.Load
        ShowView()
    End Sub

    Private Sub frmMasterTemplateFixedBase_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If ucMTFixedDetail1.Visible And ucMTFixedDetail1.RecordState <> "VIEW" Then
            Dim dlgResult As DialogResult = ucMTFixedDetail1.ProcessDataDirtyConfirmation()

            If dlgResult = DialogResult.Yes Then
                'Do nothing
            ElseIf dlgResult = DialogResult.No Then
                'Do nothing
            Else
                e.Cancel = True
            End If

        End If
    End Sub

    Private Sub frmMasterTemplateFixedBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ucMTFixedView1.ContainerForm = Me
        ucMTFixedDetail1.ContainerForm = Me
    End Sub

    Public Sub ShowDetail()
        ucMTFixedView1.Visible = False
        ucMTFixedDetail1.Visible = True
    End Sub

    Public Sub ShowView()
        ucMTFixedView1.Visible = True
        ucMTFixedDetail1.Visible = False
    End Sub
End Class
