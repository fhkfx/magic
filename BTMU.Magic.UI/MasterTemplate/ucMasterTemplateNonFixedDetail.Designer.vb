Imports BTMU.Magic.FileFormat

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMasterTemplateNonFixedDetail
    Inherits ucMasterTemplateNonFixedBase 'System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim OutputFormatLabel As System.Windows.Forms.Label
        Dim EnclosureCharacterLabel As System.Windows.Forms.Label
        Dim CustomerOutputExtensionLabel As System.Windows.Forms.Label
        Dim TemplateNameLabel As System.Windows.Forms.Label
        Dim DescriptionLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.grpOutput = New System.Windows.Forms.GroupBox
        Me.OutputFormatCombo = New System.Windows.Forms.ComboBox
        Me.MasterTemplateNonFixedCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EncryptCustomerOutputFileCheckBox = New System.Windows.Forms.CheckBox
        Me.EnclosureCharacterTextBox = New System.Windows.Forms.TextBox
        Me.CustomerOutputExtensionTextBox = New System.Windows.Forms.TextBox
        Me.grpTemplate = New System.Windows.Forms.GroupBox
        Me.TemplateNameTextBox = New System.Windows.Forms.TextBox
        Me.DescriptionTextBox = New System.Windows.Forms.TextBox
        Me.TemplateStatusCheckBox = New System.Windows.Forms.CheckBox
        Me.MasterTemplateFixedCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnUp = New System.Windows.Forms.Button
        Me.btnDown = New System.Windows.Forms.Button
        Me.ErrorProvider1 = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.MasterTemplateNonFixedDetailCollectionDataGridView = New System.Windows.Forms.DataGridView
        Me.dgvbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.PositionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FieldNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DateFormatDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataLengthDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MandatoryDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DefaultValueDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataSampleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MapSeparatorDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CarriageReturn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.HeaderDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DetailDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.TrailerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.CreatedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CreatedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ModifiedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ModifiedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OriginalModifiedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OriginalModifiedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SequenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MasterTemplateNonFixedDetailCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MasterTemplateListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnModify = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDraft = New System.Windows.Forms.Button
        OutputFormatLabel = New System.Windows.Forms.Label
        EnclosureCharacterLabel = New System.Windows.Forms.Label
        CustomerOutputExtensionLabel = New System.Windows.Forms.Label
        TemplateNameLabel = New System.Windows.Forms.Label
        DescriptionLabel = New System.Windows.Forms.Label
        Me.grpOutput.SuspendLayout()
        CType(Me.MasterTemplateNonFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpTemplate.SuspendLayout()
        CType(Me.MasterTemplateFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateNonFixedDetailCollectionDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateNonFixedDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTop.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'OutputFormatLabel
        '
        OutputFormatLabel.AutoSize = True
        OutputFormatLabel.Location = New System.Drawing.Point(6, 27)
        OutputFormatLabel.Name = "OutputFormatLabel"
        OutputFormatLabel.Size = New System.Drawing.Size(78, 14)
        OutputFormatLabel.TabIndex = 4
        OutputFormatLabel.Text = "Output Format:"
        '
        'EnclosureCharacterLabel
        '
        EnclosureCharacterLabel.AutoSize = True
        EnclosureCharacterLabel.Location = New System.Drawing.Point(6, 81)
        EnclosureCharacterLabel.Name = "EnclosureCharacterLabel"
        EnclosureCharacterLabel.Size = New System.Drawing.Size(109, 14)
        EnclosureCharacterLabel.TabIndex = 6
        EnclosureCharacterLabel.Text = "Enclosure Character:"
        '
        'CustomerOutputExtensionLabel
        '
        CustomerOutputExtensionLabel.AutoSize = True
        CustomerOutputExtensionLabel.Location = New System.Drawing.Point(6, 54)
        CustomerOutputExtensionLabel.Name = "CustomerOutputExtensionLabel"
        CustomerOutputExtensionLabel.Size = New System.Drawing.Size(92, 14)
        CustomerOutputExtensionLabel.TabIndex = 5
        CustomerOutputExtensionLabel.Text = "Output Extension:"
        '
        'TemplateNameLabel
        '
        TemplateNameLabel.AutoSize = True
        TemplateNameLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TemplateNameLabel.Location = New System.Drawing.Point(16, 27)
        TemplateNameLabel.Name = "TemplateNameLabel"
        TemplateNameLabel.Size = New System.Drawing.Size(83, 14)
        TemplateNameLabel.TabIndex = 3
        TemplateNameLabel.Text = "Template Name:"
        '
        'DescriptionLabel
        '
        DescriptionLabel.AutoSize = True
        DescriptionLabel.Location = New System.Drawing.Point(16, 54)
        DescriptionLabel.Name = "DescriptionLabel"
        DescriptionLabel.Size = New System.Drawing.Size(64, 14)
        DescriptionLabel.TabIndex = 4
        DescriptionLabel.Text = "Description:"
        '
        'grpOutput
        '
        Me.grpOutput.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpOutput.Controls.Add(Me.OutputFormatCombo)
        Me.grpOutput.Controls.Add(OutputFormatLabel)
        Me.grpOutput.Controls.Add(Me.EncryptCustomerOutputFileCheckBox)
        Me.grpOutput.Controls.Add(EnclosureCharacterLabel)
        Me.grpOutput.Controls.Add(Me.EnclosureCharacterTextBox)
        Me.grpOutput.Controls.Add(CustomerOutputExtensionLabel)
        Me.grpOutput.Controls.Add(Me.CustomerOutputExtensionTextBox)
        Me.grpOutput.Location = New System.Drawing.Point(662, 3)
        Me.grpOutput.Name = "grpOutput"
        Me.grpOutput.Size = New System.Drawing.Size(512, 129)
        Me.grpOutput.TabIndex = 1
        Me.grpOutput.TabStop = False
        Me.grpOutput.Text = "Output Setting"
        '
        'OutputFormatCombo
        '
        Me.OutputFormatCombo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.MasterTemplateNonFixedCollectionBindingSource, "OutputFormat", True))
        Me.OutputFormatCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.OutputFormatCombo.FormattingEnabled = True
        Me.OutputFormatCombo.Items.AddRange(New Object() {"", "BULKVN", "CWS", "GCMS", "GCMSCreditData", "iFTS", "iFTS-2 Generic", "iFTS-2 SingleLine", "iFTS-2 MultiLine", "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM", "OiRTMSCI", "OiRTMSGC", "OiRTMSGD", "OiRTMSRM", "Mr.Omakase India"})
        Me.OutputFormatCombo.Location = New System.Drawing.Point(171, 20)
        Me.OutputFormatCombo.Name = "OutputFormatCombo"
        Me.OutputFormatCombo.Size = New System.Drawing.Size(117, 22)
        Me.OutputFormatCombo.TabIndex = 0
        '
        'MasterTemplateNonFixedCollectionBindingSource
        '
        Me.MasterTemplateNonFixedCollectionBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateNonFixed)
        '
        'EncryptCustomerOutputFileCheckBox
        '
        Me.EncryptCustomerOutputFileCheckBox.Checked = True
        Me.EncryptCustomerOutputFileCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.EncryptCustomerOutputFileCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.MasterTemplateNonFixedCollectionBindingSource, "EncryptCustomerOutputFile", True))
        Me.EncryptCustomerOutputFileCheckBox.Location = New System.Drawing.Point(171, 104)
        Me.EncryptCustomerOutputFileCheckBox.Name = "EncryptCustomerOutputFileCheckBox"
        Me.EncryptCustomerOutputFileCheckBox.Size = New System.Drawing.Size(170, 20)
        Me.EncryptCustomerOutputFileCheckBox.TabIndex = 3
        Me.EncryptCustomerOutputFileCheckBox.Text = "Encrypt Output File"
        '
        'EnclosureCharacterTextBox
        '
        Me.EnclosureCharacterTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateNonFixedCollectionBindingSource, "EnclosureCharacter", True))
        Me.EnclosureCharacterTextBox.Location = New System.Drawing.Point(171, 75)
        Me.EnclosureCharacterTextBox.MaxLength = 1
        Me.EnclosureCharacterTextBox.Name = "EnclosureCharacterTextBox"
        Me.EnclosureCharacterTextBox.Size = New System.Drawing.Size(52, 20)
        Me.EnclosureCharacterTextBox.TabIndex = 2
        '
        'CustomerOutputExtensionTextBox
        '
        Me.CustomerOutputExtensionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateNonFixedCollectionBindingSource, "CustomerOutputExtension", True))
        Me.CustomerOutputExtensionTextBox.Location = New System.Drawing.Point(171, 48)
        Me.CustomerOutputExtensionTextBox.MaxLength = 3
        Me.CustomerOutputExtensionTextBox.Name = "CustomerOutputExtensionTextBox"
        Me.CustomerOutputExtensionTextBox.Size = New System.Drawing.Size(53, 20)
        Me.CustomerOutputExtensionTextBox.TabIndex = 1
        '
        'grpTemplate
        '
        Me.grpTemplate.Controls.Add(Me.TemplateNameTextBox)
        Me.grpTemplate.Controls.Add(TemplateNameLabel)
        Me.grpTemplate.Controls.Add(DescriptionLabel)
        Me.grpTemplate.Controls.Add(Me.DescriptionTextBox)
        Me.grpTemplate.Controls.Add(Me.TemplateStatusCheckBox)
        Me.grpTemplate.Location = New System.Drawing.Point(3, 3)
        Me.grpTemplate.Name = "grpTemplate"
        Me.grpTemplate.Size = New System.Drawing.Size(653, 129)
        Me.grpTemplate.TabIndex = 0
        Me.grpTemplate.TabStop = False
        Me.grpTemplate.Text = "Template Setting"
        '
        'TemplateNameTextBox
        '
        Me.TemplateNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateNonFixedCollectionBindingSource, "TemplateName", True))
        Me.TemplateNameTextBox.Location = New System.Drawing.Point(106, 20)
        Me.TemplateNameTextBox.MaxLength = 100
        Me.TemplateNameTextBox.Name = "TemplateNameTextBox"
        Me.TemplateNameTextBox.Size = New System.Drawing.Size(201, 20)
        Me.TemplateNameTextBox.TabIndex = 0
        '
        'DescriptionTextBox
        '
        Me.DescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateNonFixedCollectionBindingSource, "Description", True))
        Me.DescriptionTextBox.Location = New System.Drawing.Point(106, 48)
        Me.DescriptionTextBox.MaxLength = 200
        Me.DescriptionTextBox.Name = "DescriptionTextBox"
        Me.DescriptionTextBox.Size = New System.Drawing.Size(517, 20)
        Me.DescriptionTextBox.TabIndex = 1
        '
        'TemplateStatusCheckBox
        '
        Me.TemplateStatusCheckBox.Checked = True
        Me.TemplateStatusCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TemplateStatusCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.MasterTemplateNonFixedCollectionBindingSource, "TemplateStatus", True))
        Me.TemplateStatusCheckBox.Location = New System.Drawing.Point(106, 74)
        Me.TemplateStatusCheckBox.Name = "TemplateStatusCheckBox"
        Me.TemplateStatusCheckBox.Size = New System.Drawing.Size(201, 30)
        Me.TemplateStatusCheckBox.TabIndex = 2
        Me.TemplateStatusCheckBox.Text = "Enabled"
        '
        'MasterTemplateFixedCollectionBindingSource
        '
        Me.MasterTemplateFixedCollectionBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateFixed)
        '
        'btnUp
        '
        Me.btnUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.Location = New System.Drawing.Point(5, 3)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(25, 27)
        Me.btnUp.TabIndex = 1
        Me.btnUp.Text = "▲"
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDown.Location = New System.Drawing.Point(32, 3)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(25, 27)
        Me.btnDown.TabIndex = 2
        Me.btnDown.Text = "▼"
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'MasterTemplateNonFixedDetailCollectionDataGridView
        '
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToDeleteRows = False
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.AutoGenerateColumns = False
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvbtnDelete, Me.PositionDataGridViewTextBoxColumn, Me.FieldNameDataGridViewTextBoxColumn, Me.DataTypeDataGridViewTextBoxColumn, Me.DateFormatDataGridViewTextBoxColumn, Me.DataLengthDataGridViewTextBoxColumn, Me.MandatoryDataGridViewCheckBoxColumn, Me.DefaultValueDataGridViewTextBoxColumn, Me.DataSampleDataGridViewTextBoxColumn, Me.MapSeparatorDataGridViewTextBoxColumn, Me.CarriageReturn, Me.HeaderDataGridViewTextBoxColumn, Me.DetailDataGridViewTextBoxColumn, Me.TrailerDataGridViewTextBoxColumn, Me.CreatedByDataGridViewTextBoxColumn, Me.CreatedDateDataGridViewTextBoxColumn, Me.ModifiedByDataGridViewTextBoxColumn, Me.ModifiedDateDataGridViewTextBoxColumn, Me.OriginalModifiedByDataGridViewTextBoxColumn, Me.OriginalModifiedDateDataGridViewTextBoxColumn, Me.SequenceDataGridViewTextBoxColumn})
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.DataSource = Me.MasterTemplateNonFixedDetailCollectionBindingSource
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.Location = New System.Drawing.Point(5, 33)
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.MultiSelect = False
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.Name = "MasterTemplateNonFixedDetailCollectionDataGridView"
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.RowHeadersVisible = False
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.RowHeadersWidth = 30
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.Size = New System.Drawing.Size(1169, 146)
        Me.MasterTemplateNonFixedDetailCollectionDataGridView.TabIndex = 0
        '
        'dgvbtnDelete
        '
        Me.dgvbtnDelete.HeaderText = "X"
        Me.dgvbtnDelete.Name = "dgvbtnDelete"
        Me.dgvbtnDelete.Text = "X"
        Me.dgvbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvbtnDelete.Width = 30
        '
        'PositionDataGridViewTextBoxColumn
        '
        Me.PositionDataGridViewTextBoxColumn.DataPropertyName = "Sequence"
        Me.PositionDataGridViewTextBoxColumn.HeaderText = "Position"
        Me.PositionDataGridViewTextBoxColumn.Name = "PositionDataGridViewTextBoxColumn"
        Me.PositionDataGridViewTextBoxColumn.ReadOnly = True
        Me.PositionDataGridViewTextBoxColumn.Width = 50
        '
        'FieldNameDataGridViewTextBoxColumn
        '
        Me.FieldNameDataGridViewTextBoxColumn.DataPropertyName = "FieldName"
        Me.FieldNameDataGridViewTextBoxColumn.HeaderText = "Field Name"
        Me.FieldNameDataGridViewTextBoxColumn.MaxInputLength = 50
        Me.FieldNameDataGridViewTextBoxColumn.Name = "FieldNameDataGridViewTextBoxColumn"
        Me.FieldNameDataGridViewTextBoxColumn.Width = 200
        '
        'DataTypeDataGridViewTextBoxColumn
        '
        Me.DataTypeDataGridViewTextBoxColumn.DataPropertyName = "DataType"
        Me.DataTypeDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DataTypeDataGridViewTextBoxColumn.HeaderText = "Data Type"
        Me.DataTypeDataGridViewTextBoxColumn.Items.AddRange(New Object() {"", "DateTime", "Number", "Text"})
        Me.DataTypeDataGridViewTextBoxColumn.Name = "DataTypeDataGridViewTextBoxColumn"
        Me.DataTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataTypeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DateFormatDataGridViewTextBoxColumn
        '
        Me.DateFormatDataGridViewTextBoxColumn.DataPropertyName = "DateFormat"
        Me.DateFormatDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DateFormatDataGridViewTextBoxColumn.HeaderText = "Date Format"
        Me.DateFormatDataGridViewTextBoxColumn.Items.AddRange(New Object() {"", "DDMMYY", "DDMMMYY", "DDMMYYYY", "DD-MM-YYYY", "DDMMMYYYY", "MMDDYY", "MMMDDYY", "MMDDYYYY", "MMMDDYYYY", "YYMMDD", "YYYYDDMM", "YYYYMMDD", "YYYY/MM/DD", "DD/MM/YYYY", "D/M/YYYY", "MM/DD/YYYY", "YYYY/M/D"})
        Me.DateFormatDataGridViewTextBoxColumn.Name = "DateFormatDataGridViewTextBoxColumn"
        Me.DateFormatDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DateFormatDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataLengthDataGridViewTextBoxColumn
        '
        Me.DataLengthDataGridViewTextBoxColumn.DataPropertyName = "DataLength"
        Me.DataLengthDataGridViewTextBoxColumn.HeaderText = "Data Length"
        Me.DataLengthDataGridViewTextBoxColumn.MaxInputLength = 4
        Me.DataLengthDataGridViewTextBoxColumn.Name = "DataLengthDataGridViewTextBoxColumn"
        '
        'MandatoryDataGridViewCheckBoxColumn
        '
        Me.MandatoryDataGridViewCheckBoxColumn.DataPropertyName = "Mandatory"
        Me.MandatoryDataGridViewCheckBoxColumn.HeaderText = "Mandatory"
        Me.MandatoryDataGridViewCheckBoxColumn.Name = "MandatoryDataGridViewCheckBoxColumn"
        '
        'DefaultValueDataGridViewTextBoxColumn
        '
        Me.DefaultValueDataGridViewTextBoxColumn.DataPropertyName = "DefaultValue"
        Me.DefaultValueDataGridViewTextBoxColumn.HeaderText = "Default Value"
        Me.DefaultValueDataGridViewTextBoxColumn.Name = "DefaultValueDataGridViewTextBoxColumn"
        Me.DefaultValueDataGridViewTextBoxColumn.Width = 175
        '
        'DataSampleDataGridViewTextBoxColumn
        '
        Me.DataSampleDataGridViewTextBoxColumn.DataPropertyName = "DataSample"
        Me.DataSampleDataGridViewTextBoxColumn.HeaderText = "Data Sample"
        Me.DataSampleDataGridViewTextBoxColumn.Name = "DataSampleDataGridViewTextBoxColumn"
        Me.DataSampleDataGridViewTextBoxColumn.Width = 175
        '
        'MapSeparatorDataGridViewTextBoxColumn
        '
        Me.MapSeparatorDataGridViewTextBoxColumn.DataPropertyName = "MapSeparator"
        Me.MapSeparatorDataGridViewTextBoxColumn.HeaderText = "Map Separator"
        Me.MapSeparatorDataGridViewTextBoxColumn.MaxInputLength = 1
        Me.MapSeparatorDataGridViewTextBoxColumn.Name = "MapSeparatorDataGridViewTextBoxColumn"
        Me.MapSeparatorDataGridViewTextBoxColumn.Visible = False
        '
        'CarriageReturn
        '
        Me.CarriageReturn.DataPropertyName = "CarriageReturn"
        Me.CarriageReturn.HeaderText = "Carriage Return As Map Separator"
        Me.CarriageReturn.Name = "CarriageReturn"
        Me.CarriageReturn.Visible = False
        Me.CarriageReturn.Width = 200
        '
        'HeaderDataGridViewTextBoxColumn
        '
        Me.HeaderDataGridViewTextBoxColumn.DataPropertyName = "Header"
        Me.HeaderDataGridViewTextBoxColumn.HeaderText = "Header"
        Me.HeaderDataGridViewTextBoxColumn.Name = "HeaderDataGridViewTextBoxColumn"
        Me.HeaderDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.HeaderDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DetailDataGridViewTextBoxColumn
        '
        Me.DetailDataGridViewTextBoxColumn.DataPropertyName = "Detail"
        Me.DetailDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DetailDataGridViewTextBoxColumn.HeaderText = "Detail"
        Me.DetailDataGridViewTextBoxColumn.Items.AddRange(New Object() {"", "Yes", "No", "Transaction", "Advice", "Payment", "WHT", "Invoice"})
        Me.DetailDataGridViewTextBoxColumn.Name = "DetailDataGridViewTextBoxColumn"
        Me.DetailDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DetailDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'TrailerDataGridViewTextBoxColumn
        '
        Me.TrailerDataGridViewTextBoxColumn.DataPropertyName = "Trailer"
        Me.TrailerDataGridViewTextBoxColumn.HeaderText = "Trailer"
        Me.TrailerDataGridViewTextBoxColumn.Name = "TrailerDataGridViewTextBoxColumn"
        Me.TrailerDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TrailerDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'CreatedByDataGridViewTextBoxColumn
        '
        Me.CreatedByDataGridViewTextBoxColumn.DataPropertyName = "CreatedBy"
        Me.CreatedByDataGridViewTextBoxColumn.HeaderText = "CreatedBy"
        Me.CreatedByDataGridViewTextBoxColumn.Name = "CreatedByDataGridViewTextBoxColumn"
        Me.CreatedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.CreatedByDataGridViewTextBoxColumn.Visible = False
        '
        'CreatedDateDataGridViewTextBoxColumn
        '
        Me.CreatedDateDataGridViewTextBoxColumn.DataPropertyName = "CreatedDate"
        Me.CreatedDateDataGridViewTextBoxColumn.HeaderText = "CreatedDate"
        Me.CreatedDateDataGridViewTextBoxColumn.Name = "CreatedDateDataGridViewTextBoxColumn"
        Me.CreatedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.CreatedDateDataGridViewTextBoxColumn.Visible = False
        '
        'ModifiedByDataGridViewTextBoxColumn
        '
        Me.ModifiedByDataGridViewTextBoxColumn.DataPropertyName = "ModifiedBy"
        Me.ModifiedByDataGridViewTextBoxColumn.HeaderText = "ModifiedBy"
        Me.ModifiedByDataGridViewTextBoxColumn.Name = "ModifiedByDataGridViewTextBoxColumn"
        Me.ModifiedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModifiedByDataGridViewTextBoxColumn.Visible = False
        '
        'ModifiedDateDataGridViewTextBoxColumn
        '
        Me.ModifiedDateDataGridViewTextBoxColumn.DataPropertyName = "ModifiedDate"
        Me.ModifiedDateDataGridViewTextBoxColumn.HeaderText = "ModifiedDate"
        Me.ModifiedDateDataGridViewTextBoxColumn.Name = "ModifiedDateDataGridViewTextBoxColumn"
        Me.ModifiedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModifiedDateDataGridViewTextBoxColumn.Visible = False
        '
        'OriginalModifiedByDataGridViewTextBoxColumn
        '
        Me.OriginalModifiedByDataGridViewTextBoxColumn.DataPropertyName = "OriginalModifiedBy"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.HeaderText = "OriginalModifiedBy"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.Name = "OriginalModifiedByDataGridViewTextBoxColumn"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.OriginalModifiedByDataGridViewTextBoxColumn.Visible = False
        '
        'OriginalModifiedDateDataGridViewTextBoxColumn
        '
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.DataPropertyName = "OriginalModifiedDate"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.HeaderText = "OriginalModifiedDate"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.Name = "OriginalModifiedDateDataGridViewTextBoxColumn"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.Visible = False
        '
        'SequenceDataGridViewTextBoxColumn
        '
        Me.SequenceDataGridViewTextBoxColumn.DataPropertyName = "Sequence"
        Me.SequenceDataGridViewTextBoxColumn.HeaderText = "Sequence"
        Me.SequenceDataGridViewTextBoxColumn.Name = "SequenceDataGridViewTextBoxColumn"
        Me.SequenceDataGridViewTextBoxColumn.ReadOnly = True
        Me.SequenceDataGridViewTextBoxColumn.Visible = False
        '
        'MasterTemplateNonFixedDetailCollectionBindingSource
        '
        Me.MasterTemplateNonFixedDetailCollectionBindingSource.DataMember = "MasterTemplateNonFixedDetailCollection"
        Me.MasterTemplateNonFixedDetailCollectionBindingSource.DataSource = Me.MasterTemplateNonFixedCollectionBindingSource
        '
        'MasterTemplateListBindingSource
        '
        Me.MasterTemplateListBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateList)
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.grpTemplate)
        Me.pnlTop.Controls.Add(Me.grpOutput)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(1177, 141)
        Me.pnlTop.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnDown)
        Me.Panel1.Controls.Add(Me.btnUp)
        Me.Panel1.Controls.Add(Me.MasterTemplateNonFixedDetailCollectionDataGridView)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 141)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1177, 183)
        Me.Panel1.TabIndex = 1
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.btnModify)
        Me.pnlBottom.Controls.Add(Me.btnSave)
        Me.pnlBottom.Controls.Add(Me.btnCancel)
        Me.pnlBottom.Controls.Add(Me.btnDraft)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 324)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(1177, 48)
        Me.pnlBottom.TabIndex = 2
        '
        'btnModify
        '
        Me.btnModify.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnModify.Location = New System.Drawing.Point(5, 3)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(87, 40)
        Me.btnModify.TabIndex = 0
        Me.btnModify.Text = "&Edit"
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(98, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 40)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(284, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 40)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnDraft
        '
        Me.btnDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDraft.Location = New System.Drawing.Point(191, 3)
        Me.btnDraft.Name = "btnDraft"
        Me.btnDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnDraft.TabIndex = 2
        Me.btnDraft.Text = "Save as &Draft"
        Me.btnDraft.UseVisualStyleBackColor = True
        '
        'ucMasterTemplateNonFixedDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlTop)
        Me.Controls.Add(Me.pnlBottom)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucMasterTemplateNonFixedDetail"
        Me.Size = New System.Drawing.Size(1177, 372)
        Me.grpOutput.ResumeLayout(False)
        Me.grpOutput.PerformLayout()
        CType(Me.MasterTemplateNonFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpTemplate.ResumeLayout(False)
        Me.grpTemplate.PerformLayout()
        CType(Me.MasterTemplateFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateNonFixedDetailCollectionDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateNonFixedDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTop.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pnlBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpOutput As System.Windows.Forms.GroupBox
    Friend WithEvents OutputFormatCombo As System.Windows.Forms.ComboBox
    Friend WithEvents EncryptCustomerOutputFileCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents EnclosureCharacterTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerOutputExtensionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents grpTemplate As System.Windows.Forms.GroupBox
    Friend WithEvents TemplateNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescriptionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TemplateStatusCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As BTMU.Magic.Common.ErrorProviderFixed
    Friend WithEvents MasterTemplateNonFixedDetailCollectionDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MasterTemplateNonFixedCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MasterTemplateNonFixedDetailCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MasterTemplateListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MasterTemplateFixedCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents btnModify As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDraft As System.Windows.Forms.Button
    Friend WithEvents dgvbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents PositionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FieldNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DateFormatDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataLengthDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MandatoryDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DefaultValueDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataSampleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MapSeparatorDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CarriageReturn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents HeaderDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DetailDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents TrailerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CreatedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CreatedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModifiedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModifiedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OriginalModifiedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OriginalModifiedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SequenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn





End Class
