Imports btmu.Magic.MasterTemplate
Imports System.IO
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports BTMU.Magic.Common

Public Class ucMasterTemplateFixedView

#Region " Private Fields "
    Private lstMaster As New MasterTemplateListCollection
    Private lstMasterDraft As New MasterTemplateListCollection
    Private bsViewDraft As New BindingSource
    Private bsView As New BindingSource
#End Region

#Region " Control Events "
    Private Sub ucMasterTemplateFixedView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateViewGrid()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click, btnAddDraft.Click
        ContainerForm.ShowDetail()
        ContainerForm.ucMTFixedDetail1.AddNew()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lstMaster.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateListDataGridView.SelectedRows

            ContainerForm.ucMTFixedDetail1.IsDraft = False
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.EditExisting(MasterTemplateViewFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click, btnCloseDraft.Click
        ContainerForm.Close()
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If lstMaster.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateListDataGridView.SelectedRows
            ContainerForm.ucMTFixedDetail1.IsDraft = False
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.ViewExisting(MasterTemplateViewFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If
    End Sub

    Private Sub btnDuplicate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click
        If lstMaster.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateListDataGridView.SelectedRows
            ContainerForm.ucMTFixedDetail1.IsDraft = False
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.DuplicateExisting(MasterTemplateViewFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If
    End Sub

    Private Sub btnEditDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDraft.Click
        If lstMasterDraft.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateFixedDraftDataGridView.SelectedRows

            ContainerForm.ucMTFixedDetail1.IsDraft = True
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.EditExisting(MasterTemplateViewDraftFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If
    End Sub

    Private Sub btnDuplicateDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicateDraft.Click
        If lstMasterDraft.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateFixedDraftDataGridView.SelectedRows
            ContainerForm.ucMTFixedDetail1.IsDraft = True
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.DuplicateExisting(MasterTemplateViewDraftFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If

    End Sub

    Private Sub btnViewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDraft.Click
        If lstMasterDraft.Count = 0 Then
            E01000000()
        Else
            Dim CurrentRow As DataGridViewSelectedRowCollection
            CurrentRow = MasterTemplateFixedDraftDataGridView.SelectedRows
            ContainerForm.ucMTFixedDetail1.IsDraft = True
            ContainerForm.ShowDetail()
            ContainerForm.ucMTFixedDetail1.ViewExisting(MasterTemplateViewDraftFolder & CurrentRow(0).Cells(0).Value & MasterTemplateFileExtension)
        End If
    End Sub

    Private Sub FilterButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Dim filterString As String = FilterTextBox.Text.Trim
        If String.IsNullOrEmpty(filterString) Then
            Me.MasterTemplateFixedBindingSource.RemoveFilter()

        Else
            Me.MasterTemplateFixedBindingSource.Filter = "TemplateName = '" & filterString & "%'"
        End If

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterDraft.Click
        Dim filterString As String = FilterDraftTextBox.Text.Trim

        If String.IsNullOrEmpty(filterString) Then
            Me.MasterTemplateFixedDraftBindingSource.RemoveFilter()

        Else
            Me.MasterTemplateFixedDraftBindingSource.Filter = "TemplateName = '" & filterString & "%'"
        End If

    End Sub
#End Region

#Region " Sub and Function "
    ''' <summary>
    ''' To retrieve all Template Name based on files on working directory
    ''' </summary>
    Public Sub PopulateViewGrid()

        Dim filePath As String
        Dim content As String
        Dim serializerMasterTemplateList As New XmlSerializer(GetType(MasterTemplateList))
        Dim firstError As New System.Text.StringBuilder
        Dim detailError As New System.Text.StringBuilder

        MasterTemplateListDataGridView.DataSource = MasterTemplateFixedBindingSource
        MasterTemplateFixedDraftDataGridView.DataSource = MasterTemplateFixedDraftBindingSource

        lstMaster.Clear()
        lstMasterDraft.Clear()

        'BEGIN CHECK EXISTING DIRECTORY
        Dim di As DirectoryInfo = New DirectoryInfo(MasterTemplateViewFolder)
        If Not di.Exists Then
            di.Create()
        End If
        di = New DirectoryInfo(MasterTemplateViewDraftFolder)
        If Not di.Exists Then
            di.Create()
        End If

        'MasterTemplateFixedBindingSource.RaiseListChangedEvents = False
        For Each filePath In System.IO.Directory.GetFiles(MasterTemplateViewFolder, "*" & MasterTemplateFileExtension)
            Try
                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New MasterTemplateList
                Iterator.MoveNext()
                content = Iterator.Current.Value

                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerMasterTemplateList.Deserialize(stream), MasterTemplateList)

                If item.IsFixed Then
                    If Not item.IsDraft Then
                        lstMaster.Add(item)
                    End If
                End If
            Catch ex As Exception
                If firstError.Length > 0 Then
                    detailError.AppendLine(String.Format("There is an error in XML document; File Name: {0}", filePath))
                Else
                    firstError.AppendLine(String.Format("There is an error in XML document; File Name: {0}", filePath))
                End If

            End Try

        Next

        MasterTemplateListDataGridView.DataSource = MasterTemplateFixedBindingSource
        MasterTemplateFixedBindingSource.DataSource = lstMaster.DefaultView
        MasterTemplateFixedBindingSource.RaiseListChangedEvents = True
        MasterTemplateFixedBindingSource.ResetBindings(False)

        For Each filePath In System.IO.Directory.GetFiles(MasterTemplateViewDraftFolder, "*" & MasterTemplateFileExtension)
            Try
                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New MasterTemplateList
                Iterator.MoveNext()
                content = Iterator.Current.Value

                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerMasterTemplateList.Deserialize(stream), MasterTemplateList)

                If item.IsFixed Then
                    If item.IsDraft Then
                        lstMasterDraft.Add(item)
                    End If
                End If
            Catch ex As Exception
                If firstError.Length > 0 Then
                    detailError.AppendLine(String.Format("There is an error in XML document; File Name: {0}", filePath))
                Else
                    firstError.AppendLine(String.Format("There is an error in XML document; File Name: {0}", filePath))
                End If

            End Try

        Next

        MasterTemplateFixedDraftDataGridView.DataSource = MasterTemplateFixedDraftBindingSource
        MasterTemplateFixedDraftBindingSource.DataSource = lstMasterDraft.DefaultView
        MasterTemplateFixedDraftBindingSource.RaiseListChangedEvents = True
        MasterTemplateFixedDraftBindingSource.ResetBindings(False)

        If firstError.Length > 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(firstError.ToString, detailError.ToString)
            Exit Sub
        End If

        'ButtonState()
    End Sub

    ''' <summary>
    ''' To check if grid do not have any template name
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub E01000000()
        Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E01000000"), "")
    End Sub

#End Region

    
End Class
