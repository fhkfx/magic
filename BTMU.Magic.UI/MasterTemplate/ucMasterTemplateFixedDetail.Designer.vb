Imports BTMU.Magic.FileFormat

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMasterTemplateFixedDetail
    Inherits ucMasterTemplateFixedBase 'System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim OutputFormatLabel As System.Windows.Forms.Label
        Dim FixWidthTypeLabel As System.Windows.Forms.Label
        Dim CustomerOutputExtensionLabel As System.Windows.Forms.Label
        Dim TemplateNameLabel As System.Windows.Forms.Label
        Dim DescriptionLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.grpTemplate = New System.Windows.Forms.GroupBox
        Me.TemplateNameTextBox = New System.Windows.Forms.TextBox
        Me.MasterTemplateFixedCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DescriptionTextBox = New System.Windows.Forms.TextBox
        Me.TemplateStatusCheckBox = New System.Windows.Forms.CheckBox
        Me.grpOutput = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.FixWidthTypeCheckDelimiter = New System.Windows.Forms.RadioButton
        Me.FixWidthTypeCheckNew = New System.Windows.Forms.RadioButton
        Me.OutputFormatCombo = New System.Windows.Forms.ComboBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.EncryptCustomerOutputFileCheckBox = New System.Windows.Forms.CheckBox
        Me.FixWidthTypeTextBox = New System.Windows.Forms.TextBox
        Me.CustomerOutputExtensionTextBox = New System.Windows.Forms.TextBox
        Me.MasterTemplateFixedDetailCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ErrorProvider1 = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.MasterTemplateListBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnModify = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDraft = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.pnlFill = New System.Windows.Forms.Panel
        Me.btnDown = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.MasterTemplateFixedDetailCollectionDataGridView = New System.Windows.Forms.DataGridView
        Me.dgvbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IncludeDecimal = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DecimalSeparator = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        OutputFormatLabel = New System.Windows.Forms.Label
        FixWidthTypeLabel = New System.Windows.Forms.Label
        CustomerOutputExtensionLabel = New System.Windows.Forms.Label
        TemplateNameLabel = New System.Windows.Forms.Label
        DescriptionLabel = New System.Windows.Forms.Label
        Me.pnlTop.SuspendLayout()
        Me.grpTemplate.SuspendLayout()
        CType(Me.MasterTemplateFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpOutput.SuspendLayout()
        CType(Me.MasterTemplateFixedDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterTemplateListBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBottom.SuspendLayout()
        Me.pnlFill.SuspendLayout()
        CType(Me.MasterTemplateFixedDetailCollectionDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OutputFormatLabel
        '
        OutputFormatLabel.AutoSize = True
        OutputFormatLabel.Location = New System.Drawing.Point(6, 27)
        OutputFormatLabel.Name = "OutputFormatLabel"
        OutputFormatLabel.Size = New System.Drawing.Size(78, 14)
        OutputFormatLabel.TabIndex = 6
        OutputFormatLabel.Text = "Output Format:"
        '
        'FixWidthTypeLabel
        '
        FixWidthTypeLabel.AutoSize = True
        FixWidthTypeLabel.Location = New System.Drawing.Point(6, 81)
        FixWidthTypeLabel.Name = "FixWidthTypeLabel"
        FixWidthTypeLabel.Size = New System.Drawing.Size(81, 14)
        FixWidthTypeLabel.TabIndex = 8
        FixWidthTypeLabel.Text = "Fix Width Type:"
        '
        'CustomerOutputExtensionLabel
        '
        CustomerOutputExtensionLabel.AutoSize = True
        CustomerOutputExtensionLabel.Location = New System.Drawing.Point(6, 54)
        CustomerOutputExtensionLabel.Name = "CustomerOutputExtensionLabel"
        CustomerOutputExtensionLabel.Size = New System.Drawing.Size(92, 14)
        CustomerOutputExtensionLabel.TabIndex = 7
        CustomerOutputExtensionLabel.Text = "Output Extension:"
        '
        'TemplateNameLabel
        '
        TemplateNameLabel.AutoSize = True
        TemplateNameLabel.Location = New System.Drawing.Point(16, 27)
        TemplateNameLabel.Name = "TemplateNameLabel"
        TemplateNameLabel.Size = New System.Drawing.Size(83, 14)
        TemplateNameLabel.TabIndex = 3
        TemplateNameLabel.Text = "Template Name:"
        '
        'DescriptionLabel
        '
        DescriptionLabel.AutoSize = True
        DescriptionLabel.Location = New System.Drawing.Point(16, 54)
        DescriptionLabel.Name = "DescriptionLabel"
        DescriptionLabel.Size = New System.Drawing.Size(64, 14)
        DescriptionLabel.TabIndex = 4
        DescriptionLabel.Text = "Description:"
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.grpTemplate)
        Me.pnlTop.Controls.Add(Me.grpOutput)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(1015, 138)
        Me.pnlTop.TabIndex = 0
        '
        'grpTemplate
        '
        Me.grpTemplate.Controls.Add(Me.TemplateNameTextBox)
        Me.grpTemplate.Controls.Add(TemplateNameLabel)
        Me.grpTemplate.Controls.Add(DescriptionLabel)
        Me.grpTemplate.Controls.Add(Me.DescriptionTextBox)
        Me.grpTemplate.Controls.Add(Me.TemplateStatusCheckBox)
        Me.grpTemplate.Location = New System.Drawing.Point(3, 3)
        Me.grpTemplate.Name = "grpTemplate"
        Me.grpTemplate.Size = New System.Drawing.Size(637, 129)
        Me.grpTemplate.TabIndex = 0
        Me.grpTemplate.TabStop = False
        Me.grpTemplate.Text = "Template Setting"
        '
        'TemplateNameTextBox
        '
        Me.TemplateNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateFixedCollectionBindingSource, "TemplateName", True))
        Me.TemplateNameTextBox.Location = New System.Drawing.Point(106, 23)
        Me.TemplateNameTextBox.MaxLength = 100
        Me.TemplateNameTextBox.Name = "TemplateNameTextBox"
        Me.TemplateNameTextBox.Size = New System.Drawing.Size(201, 20)
        Me.TemplateNameTextBox.TabIndex = 0
        '
        'MasterTemplateFixedCollectionBindingSource
        '
        Me.MasterTemplateFixedCollectionBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateFixed)
        '
        'DescriptionTextBox
        '
        Me.DescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateFixedCollectionBindingSource, "Description", True))
        Me.DescriptionTextBox.Location = New System.Drawing.Point(106, 48)
        Me.DescriptionTextBox.MaxLength = 200
        Me.DescriptionTextBox.Name = "DescriptionTextBox"
        Me.DescriptionTextBox.Size = New System.Drawing.Size(517, 20)
        Me.DescriptionTextBox.TabIndex = 1
        '
        'TemplateStatusCheckBox
        '
        Me.TemplateStatusCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.MasterTemplateFixedCollectionBindingSource, "TemplateStatus", True))
        Me.TemplateStatusCheckBox.Location = New System.Drawing.Point(107, 73)
        Me.TemplateStatusCheckBox.Name = "TemplateStatusCheckBox"
        Me.TemplateStatusCheckBox.Size = New System.Drawing.Size(201, 30)
        Me.TemplateStatusCheckBox.TabIndex = 2
        Me.TemplateStatusCheckBox.Text = "Enabled"
        '
        'grpOutput
        '
        Me.grpOutput.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpOutput.Controls.Add(Me.TextBox2)
        Me.grpOutput.Controls.Add(Me.FixWidthTypeCheckDelimiter)
        Me.grpOutput.Controls.Add(Me.FixWidthTypeCheckNew)
        Me.grpOutput.Controls.Add(Me.OutputFormatCombo)
        Me.grpOutput.Controls.Add(Me.TextBox1)
        Me.grpOutput.Controls.Add(OutputFormatLabel)
        Me.grpOutput.Controls.Add(Me.EncryptCustomerOutputFileCheckBox)
        Me.grpOutput.Controls.Add(FixWidthTypeLabel)
        Me.grpOutput.Controls.Add(Me.FixWidthTypeTextBox)
        Me.grpOutput.Controls.Add(CustomerOutputExtensionLabel)
        Me.grpOutput.Controls.Add(Me.CustomerOutputExtensionTextBox)
        Me.grpOutput.Location = New System.Drawing.Point(646, 3)
        Me.grpOutput.Name = "grpOutput"
        Me.grpOutput.Size = New System.Drawing.Size(366, 129)
        Me.grpOutput.TabIndex = 1
        Me.grpOutput.TabStop = False
        Me.grpOutput.Text = "Output Setting"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(-536, 28)
        Me.TextBox2.MaxLength = 100
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(201, 20)
        Me.TextBox2.TabIndex = 0
        '
        'FixWidthTypeCheckDelimiter
        '
        Me.FixWidthTypeCheckDelimiter.AutoSize = True
        Me.FixWidthTypeCheckDelimiter.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.MasterTemplateFixedCollectionBindingSource, "Delimiter", True))
        Me.FixWidthTypeCheckDelimiter.Location = New System.Drawing.Point(254, 76)
        Me.FixWidthTypeCheckDelimiter.Name = "FixWidthTypeCheckDelimiter"
        Me.FixWidthTypeCheckDelimiter.Size = New System.Drawing.Size(65, 18)
        Me.FixWidthTypeCheckDelimiter.TabIndex = 3
        Me.FixWidthTypeCheckDelimiter.TabStop = True
        Me.FixWidthTypeCheckDelimiter.Text = "Delimiter"
        Me.FixWidthTypeCheckDelimiter.UseVisualStyleBackColor = True
        '
        'FixWidthTypeCheckNew
        '
        Me.FixWidthTypeCheckNew.AutoSize = True
        Me.FixWidthTypeCheckNew.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.MasterTemplateFixedCollectionBindingSource, "NewLine", True))
        Me.FixWidthTypeCheckNew.Location = New System.Drawing.Point(171, 76)
        Me.FixWidthTypeCheckNew.Name = "FixWidthTypeCheckNew"
        Me.FixWidthTypeCheckNew.Size = New System.Drawing.Size(71, 18)
        Me.FixWidthTypeCheckNew.TabIndex = 2
        Me.FixWidthTypeCheckNew.TabStop = True
        Me.FixWidthTypeCheckNew.Text = "New Line"
        Me.FixWidthTypeCheckNew.UseVisualStyleBackColor = True
        '
        'OutputFormatCombo
        '
        Me.OutputFormatCombo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.MasterTemplateFixedCollectionBindingSource, "OutputFormat", True))
        Me.OutputFormatCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.OutputFormatCombo.FormattingEnabled = True
        Me.OutputFormatCombo.Items.AddRange(New Object() {"", "EPS", "VPS"})
        Me.OutputFormatCombo.Location = New System.Drawing.Point(171, 23)
        Me.OutputFormatCombo.Name = "OutputFormatCombo"
        Me.OutputFormatCombo.Size = New System.Drawing.Size(117, 22)
        Me.OutputFormatCombo.TabIndex = 0
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(-536, 54)
        Me.TextBox1.MaxLength = 200
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(517, 20)
        Me.TextBox1.TabIndex = 1
        '
        'EncryptCustomerOutputFileCheckBox
        '
        Me.EncryptCustomerOutputFileCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.MasterTemplateFixedCollectionBindingSource, "EncryptCustomerOutputFile", True))
        Me.EncryptCustomerOutputFileCheckBox.Location = New System.Drawing.Point(171, 103)
        Me.EncryptCustomerOutputFileCheckBox.Name = "EncryptCustomerOutputFileCheckBox"
        Me.EncryptCustomerOutputFileCheckBox.Size = New System.Drawing.Size(170, 20)
        Me.EncryptCustomerOutputFileCheckBox.TabIndex = 5
        Me.EncryptCustomerOutputFileCheckBox.Text = "Encrypt Output File"
        '
        'FixWidthTypeTextBox
        '
        Me.FixWidthTypeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateFixedCollectionBindingSource, "DelimiterCharacter", True))
        Me.FixWidthTypeTextBox.Location = New System.Drawing.Point(331, 75)
        Me.FixWidthTypeTextBox.MaxLength = 1
        Me.FixWidthTypeTextBox.Name = "FixWidthTypeTextBox"
        Me.FixWidthTypeTextBox.Size = New System.Drawing.Size(15, 20)
        Me.FixWidthTypeTextBox.TabIndex = 4
        '
        'CustomerOutputExtensionTextBox
        '
        Me.CustomerOutputExtensionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MasterTemplateFixedCollectionBindingSource, "CustomerOutputExtension", True))
        Me.CustomerOutputExtensionTextBox.Location = New System.Drawing.Point(171, 48)
        Me.CustomerOutputExtensionTextBox.MaxLength = 3
        Me.CustomerOutputExtensionTextBox.Name = "CustomerOutputExtensionTextBox"
        Me.CustomerOutputExtensionTextBox.Size = New System.Drawing.Size(53, 20)
        Me.CustomerOutputExtensionTextBox.TabIndex = 1
        '
        'MasterTemplateFixedDetailCollectionBindingSource
        '
        Me.MasterTemplateFixedDetailCollectionBindingSource.DataMember = "MasterTemplateFixedDetailCollection"
        Me.MasterTemplateFixedDetailCollectionBindingSource.DataSource = Me.MasterTemplateFixedCollectionBindingSource
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'MasterTemplateListBindingSource
        '
        Me.MasterTemplateListBindingSource.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateList)
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.btnModify)
        Me.pnlBottom.Controls.Add(Me.btnSave)
        Me.pnlBottom.Controls.Add(Me.btnDraft)
        Me.pnlBottom.Controls.Add(Me.btnCancel)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 424)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(1015, 48)
        Me.pnlBottom.TabIndex = 2
        '
        'btnModify
        '
        Me.btnModify.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnModify.Location = New System.Drawing.Point(3, 5)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(87, 40)
        Me.btnModify.TabIndex = 0
        Me.btnModify.Text = "&Edit"
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(96, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 40)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDraft
        '
        Me.btnDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDraft.Location = New System.Drawing.Point(189, 5)
        Me.btnDraft.Name = "btnDraft"
        Me.btnDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnDraft.TabIndex = 2
        Me.btnDraft.Text = "Save as &Draft"
        Me.btnDraft.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(282, 5)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 40)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'pnlFill
        '
        Me.pnlFill.Controls.Add(Me.btnDown)
        Me.pnlFill.Controls.Add(Me.btnUp)
        Me.pnlFill.Controls.Add(Me.MasterTemplateFixedDetailCollectionDataGridView)
        Me.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFill.Location = New System.Drawing.Point(0, 138)
        Me.pnlFill.Name = "pnlFill"
        Me.pnlFill.Size = New System.Drawing.Size(1015, 286)
        Me.pnlFill.TabIndex = 1
        '
        'btnDown
        '
        Me.btnDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDown.Location = New System.Drawing.Point(30, 3)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(25, 27)
        Me.btnDown.TabIndex = 2
        Me.btnDown.Text = "▼"
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.Location = New System.Drawing.Point(3, 3)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(25, 27)
        Me.btnUp.TabIndex = 1
        Me.btnUp.Text = "▲"
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'MasterTemplateFixedDetailCollectionDataGridView
        '
        Me.MasterTemplateFixedDetailCollectionDataGridView.AllowUserToDeleteRows = False
        Me.MasterTemplateFixedDetailCollectionDataGridView.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.MasterTemplateFixedDetailCollectionDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MasterTemplateFixedDetailCollectionDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MasterTemplateFixedDetailCollectionDataGridView.AutoGenerateColumns = False
        Me.MasterTemplateFixedDetailCollectionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.MasterTemplateFixedDetailCollectionDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvbtnDelete, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewTextBoxColumn9, Me.IncludeDecimal, Me.DecimalSeparator, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14})
        Me.MasterTemplateFixedDetailCollectionDataGridView.DataSource = Me.MasterTemplateFixedDetailCollectionBindingSource
        Me.MasterTemplateFixedDetailCollectionDataGridView.Location = New System.Drawing.Point(3, 37)
        Me.MasterTemplateFixedDetailCollectionDataGridView.MultiSelect = False
        Me.MasterTemplateFixedDetailCollectionDataGridView.Name = "MasterTemplateFixedDetailCollectionDataGridView"
        Me.MasterTemplateFixedDetailCollectionDataGridView.RowHeadersVisible = False
        Me.MasterTemplateFixedDetailCollectionDataGridView.RowHeadersWidth = 30
        Me.MasterTemplateFixedDetailCollectionDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MasterTemplateFixedDetailCollectionDataGridView.Size = New System.Drawing.Size(1009, 247)
        Me.MasterTemplateFixedDetailCollectionDataGridView.TabIndex = 0
        '
        'dgvbtnDelete
        '
        Me.dgvbtnDelete.HeaderText = "X"
        Me.dgvbtnDelete.Name = "dgvbtnDelete"
        Me.dgvbtnDelete.Text = "X"
        Me.dgvbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvbtnDelete.Width = 30
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Sequence"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Position"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 55
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "FieldName"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Field Name"
        Me.DataGridViewTextBoxColumn5.MaxInputLength = 50
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 200
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "DataType"
        Me.DataGridViewTextBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DataGridViewTextBoxColumn6.HeaderText = "Data Type"
        Me.DataGridViewTextBoxColumn6.Items.AddRange(New Object() {"", "Currency", "DateTime", "Number", "Text"})
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewTextBoxColumn6.Width = 70
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "DateFormat"
        Me.DataGridViewTextBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DataGridViewTextBoxColumn7.HeaderText = "Date Format"
        Me.DataGridViewTextBoxColumn7.Items.AddRange(New Object() {"", "DDMMYY", "DDMMMYY", "DDMMYYYY", "DDMMMYYYY", "MMDDYY", "MMMDDYY", "MMDDYYYY", "MMMDDYYYY", "YYMMDD", "YYYYDDMM", "YYYYMMDD", "YYYY/MM/DD", "DD/MM/YYYY", "D/M/YYYY", "MM/DD/YYYY", "YYYY/M/D"})
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "DataLength"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Data Length"
        Me.DataGridViewTextBoxColumn8.MaxInputLength = 4
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.DataPropertyName = "Mandatory"
        Me.DataGridViewCheckBoxColumn3.HeaderText = "Mandatory"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        Me.DataGridViewCheckBoxColumn3.Width = 70
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "DefaultValue"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Default Value"
        Me.DataGridViewTextBoxColumn9.MaxInputLength = 200
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 90
        '
        'IncludeDecimal
        '
        Me.IncludeDecimal.DataPropertyName = "IncludeDecimal"
        Me.IncludeDecimal.HeaderText = "Include Decimal"
        Me.IncludeDecimal.Items.AddRange(New Object() {"", "Yes", "No"})
        Me.IncludeDecimal.Name = "IncludeDecimal"
        Me.IncludeDecimal.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DecimalSeparator
        '
        Me.DecimalSeparator.DataPropertyName = "DecimalSeparator"
        Me.DecimalSeparator.HeaderText = "Decimal Separator"
        Me.DecimalSeparator.MaxInputLength = 1
        Me.DecimalSeparator.Name = "DecimalSeparator"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "DataSample"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Data Sample"
        Me.DataGridViewTextBoxColumn10.MaxInputLength = 200
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 90
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "MapSeparator"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Separator"
        Me.DataGridViewTextBoxColumn11.MaxInputLength = 1
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        Me.DataGridViewTextBoxColumn11.Width = 65
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Header"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Header"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewTextBoxColumn12.Width = 55
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Detail"
        Me.DataGridViewTextBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.DataGridViewTextBoxColumn13.HeaderText = "Detail"
        Me.DataGridViewTextBoxColumn13.Items.AddRange(New Object() {"", "Yes", "No", "Transaction", "Advice"})
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewTextBoxColumn13.Width = 75
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Trailer"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Trailer"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewTextBoxColumn14.Width = 55
        '
        'ucMasterTemplateFixedDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.pnlFill)
        Me.Controls.Add(Me.pnlBottom)
        Me.Controls.Add(Me.pnlTop)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucMasterTemplateFixedDetail"
        Me.Size = New System.Drawing.Size(1015, 472)
        Me.pnlTop.ResumeLayout(False)
        Me.grpTemplate.ResumeLayout(False)
        Me.grpTemplate.PerformLayout()
        CType(Me.MasterTemplateFixedCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpOutput.ResumeLayout(False)
        Me.grpOutput.PerformLayout()
        CType(Me.MasterTemplateFixedDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterTemplateListBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBottom.ResumeLayout(False)
        Me.pnlFill.ResumeLayout(False)
        CType(Me.MasterTemplateFixedDetailCollectionDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents grpOutput As System.Windows.Forms.GroupBox
    Friend WithEvents OutputFormatCombo As System.Windows.Forms.ComboBox
    Friend WithEvents EncryptCustomerOutputFileCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FixWidthTypeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerOutputExtensionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ErrorProvider1 As BTMU.Magic.Common.ErrorProviderFixed
    Friend WithEvents MasterTemplateFixedDetailCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MasterTemplateFixedCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FixWidthTypeCheckDelimiter As System.Windows.Forms.RadioButton
    Friend WithEvents FixWidthTypeCheckNew As System.Windows.Forms.RadioButton
    Friend WithEvents MasterTemplateListBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents grpTemplate As System.Windows.Forms.GroupBox
    Friend WithEvents TemplateNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescriptionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TemplateStatusCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents pnlFill As System.Windows.Forms.Panel
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents MasterTemplateFixedDetailCollectionDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents btnModify As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDraft As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents dgvbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IncludeDecimal As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DecimalSeparator As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewCheckBoxColumn





End Class
