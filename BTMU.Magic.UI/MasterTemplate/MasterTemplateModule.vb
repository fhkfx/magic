Imports System.IO
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports BTMU.Magic.Common
Imports BTMU.Magic.MasterTemplate

Module MasterTemplateModule
    ''' <summary>
    ''' Enumeration for the state of current record
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum eRecordState
        ADD
        VIEW
        EDIT
    End Enum

    ''' <summary>
    ''' Enumeration for row's event (up and down button)
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum mode
        top = 0
        up = 1
        down = 2
        bottom = 3
    End Enum

    Public MasterTemplateViewFolder As String = My.Settings.Item("MasterTemplateViewFolder") & "\"
    Public MasterTemplateViewDraftFolder As String = My.Settings.Item("MasterTemplateViewDraftFolder") & "\"
    Public MasterTemplateFileExtension As String = GetMasterTemplateFileExtension() 'My.Settings.Item("MasterTemplateFileExtension")
    ''' <summary>
    ''' To swap record
    ''' </summary>
    ''' <param name="DataGridView">Datagrid's name</param>
    ''' <param name="range">Mode Up/Down</param>
    ''' <remarks></remarks>
    Public Sub swapRows(ByRef DataGridView As DataGridView, ByVal range As mode)
        Dim iSelectedRow As Integer = -1
        For iTmp As Integer = 0 To DataGridView.Rows.Count - 1
            If DataGridView.Rows(iTmp).Selected Then
                iSelectedRow = iTmp
                Exit For
            End If
        Next

        If iSelectedRow <> -1 Then
            Dim sTmp(DataGridView.Columns.Count - 1) As String
            For iTmp As Integer = 0 To DataGridView.Columns.Count - 1
                If IsNothingOrEmptyString(DataGridView.Rows(iSelectedRow).Cells(iTmp).Value) Then
                    sTmp(iTmp) = ""
                Else
                    sTmp(iTmp) = DataGridView.Rows(iSelectedRow).Cells(iTmp).Value.ToString()
                End If
            Next

            Dim iNewRow As Integer
            If range = mode.down Then
                iNewRow = iSelectedRow + 1
            ElseIf range = mode.up Then
                iNewRow = iSelectedRow - 1
            End If

            If (range = mode.up And iSelectedRow > 0) Or (range = mode.down And iSelectedRow + 1 < DataGridView.Rows.Count - 1) Then

                For iTmp As Integer = 0 To DataGridView.Columns.Count - 1
                    DataGridView.Rows(iSelectedRow).Cells(iTmp).Value = DataGridView.Rows(iNewRow).Cells(iTmp).Value
                    DataGridView.Rows(iNewRow).Cells(iTmp).Value = sTmp(iTmp)
                Next
                toSelect(DataGridView, iNewRow)

            ElseIf range = mode.top Or range = mode.bottom Then
                reshuffleRows(DataGridView, sTmp, iSelectedRow, range)
            End If
        End If
    End Sub
    ''' <summary>
    ''' To select Record
    ''' </summary>
    ''' <param name="DataGridView">Datagrid's name</param>
    ''' <param name="iNewRow">New Row index</param>
    ''' <remarks></remarks>
    Public Sub toSelect(ByRef DataGridView As DataGridView, ByVal iNewRow As Integer)
        DataGridView.Rows(iNewRow).Selected = True
    End Sub
    ''' <summary>
    ''' Rearrange the data after move up or down
    ''' </summary>
    ''' <param name="DataGridView">Datagrid's name</param>
    ''' <param name="sTmp">Collection of detail data</param>
    ''' <param name="iSelectedRow">Selected Row</param>
    ''' <param name="Range">Mode Up/Down</param>
    ''' <remarks></remarks>
    Public Sub reshuffleRows(ByRef DataGridView As DataGridView, ByVal sTmp() As String, ByVal iSelectedRow As Integer, ByVal Range As mode)
        If Range = mode.top Then
            Dim iFirstRow As Integer = 0
            If iSelectedRow > iFirstRow Then
                For iTmp As Integer = iSelectedRow To 1 Step -1
                    For iCol As Integer = 0 To DataGridView.Columns.Count - 1
                        DataGridView.Rows(iTmp).Cells(iCol).Value = DataGridView.Rows(iTmp - 1).Cells(iCol).Value
                    Next
                Next
                For iCol As Integer = 0 To DataGridView.Columns.Count - 1
                    DataGridView.Rows(iFirstRow).Cells(iCol).Value = sTmp(iCol).ToString
                Next
                'toSelect(DataGridView, iFirstRow)
            End If
        Else
            Dim iLastRow As Integer = DataGridView.Rows.Count - 1
            If iSelectedRow < iLastRow Then
                For iTmp As Integer = iSelectedRow To iLastRow - 1
                    For iCol As Integer = 0 To DataGridView.Columns.Count - 1
                        DataGridView.Rows(iTmp).Cells(iCol).Value = DataGridView.Rows(iTmp + 1).Cells(iCol).Value
                    Next
                Next
                For iCol As Integer = 0 To DataGridView.Columns.Count - 1
                    DataGridView.Rows(iLastRow).Cells(iCol).Value = sTmp(iCol).ToString
                Next
                'toSelect(DataGridView, iLastRow)
            End If
        End If
    End Sub


    Public Function ListAllMasterTemplateList() As MasterTemplateListCollection
        Dim filePath As String
        Dim content As String
        Dim serializerMasterTemplateList As New XmlSerializer(GetType(MasterTemplateList))
        Dim lstMaster As New MasterTemplateListCollection
        Dim mf As New MasterTemplateSharedFunction

        Try
            For Each filePath In System.IO.Directory.GetFiles(MasterTemplateViewFolder, "*" & MasterTemplateFileExtension, IO.SearchOption.AllDirectories)

                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New MasterTemplateList
                Iterator.MoveNext()
                content = Iterator.Current.Value


                'Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(content))
                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerMasterTemplateList.Deserialize(stream), MasterTemplateList)

                lstMaster.Add(item)
            Next

            Return lstMaster
        Catch ex As Exception
            Return lstMaster
        End Try

    End Function
    ''' <summary>
    ''' Loads Fixed Master Detail Template
    ''' </summary>
    ''' <param name="TemplateFile">Template Name File</param>
    ''' <returns>Fixed Master Detail Collection</returns>
    ''' <remarks></remarks>
    Public Function LoadFixedMasterTemplate(ByVal TemplateFile As String) As MasterTemplateFixedDetailCollection
        Dim lstMaster As New MasterTemplateFixedDetailCollection
        Dim item As MasterTemplateFixed

        item = (New MasterTemplateFixed).LoadFromFile2(TemplateFile)
        lstMaster = item.MasterTemplateFixedDetailCollection

        Return lstMaster
    End Function
    ''' <summary>
    ''' Loads Non-Fixed Master Detail Template
    ''' </summary>
    ''' <param name="TemplateFile">Template Name File</param>
    ''' <returns>Non-Fixed Master Detail Collection</returns>
    ''' <remarks></remarks>
    Public Function LoadNonFixedMasterTemplate(ByVal TemplateFile As String) As MasterTemplateNonFixedDetailCollection
        Dim lstMaster As New MasterTemplateNonFixedDetailCollection
        Dim item As MasterTemplateNonFixed

        item = (New MasterTemplateNonFixed).LoadFromFile2(TemplateFile)
        lstMaster = item.MasterTemplateNonFixedDetailCollection

        Return lstMaster
    End Function
    ''' <summary>
    ''' Checking phisical file for master template
    ''' </summary>
    ''' <param name="mastertemplateName"></param>
    ''' <returns>
    ''' Shall return true, if there exists, in the underlying file system, 
    ''' a master template (could either be a fixed or non fixed type) with the given master template name
    ''' or false, otherwise
    ''' </returns>
    ''' <remarks></remarks>
    Public Function MasterTemplateExists(ByVal mastertemplateName As String) As Boolean
        Try
            If System.IO.File.Exists(System.IO.Path.Combine(MasterTemplateViewFolder, mastertemplateName) & MasterTemplateFileExtension) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
End Module
