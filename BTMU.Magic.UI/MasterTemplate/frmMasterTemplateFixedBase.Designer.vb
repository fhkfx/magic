<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterTemplateFixedBase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterTemplateFixedBase))
        Me.ucMTFixedDetail1 = New BTMU.Magic.UI.ucMasterTemplateFixedDetail
        Me.ucMTFixedView1 = New BTMU.Magic.UI.ucMasterTemplateFixedView
        Me.SuspendLayout()
        '
        'ucMTFixedDetail1
        '
        Me.ucMTFixedDetail1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucMTFixedDetail1.ContainerForm = Nothing
        Me.ucMTFixedDetail1.IsDraft = False
        Me.ucMTFixedDetail1.IsNew = False
        Me.ucMTFixedDetail1.Location = New System.Drawing.Point(-2, 0)
        Me.ucMTFixedDetail1.Name = "ucMTFixedDetail1"
        Me.ucMTFixedDetail1.RecordState = Nothing
        Me.ucMTFixedDetail1.Size = New System.Drawing.Size(1064, 606)
        Me.ucMTFixedDetail1.TabIndex = 1
        '
        'ucMTFixedView1
        '
        Me.ucMTFixedView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucMTFixedView1.ContainerForm = Nothing
        Me.ucMTFixedView1.IsNew = False
        Me.ucMTFixedView1.Location = New System.Drawing.Point(-2, 0)
        Me.ucMTFixedView1.Name = "ucMTFixedView1"
        Me.ucMTFixedView1.Size = New System.Drawing.Size(1064, 609)
        Me.ucMTFixedView1.TabIndex = 0
        '
        'frmMasterTemplateFixedBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1062, 608)
        Me.Controls.Add(Me.ucMTFixedView1)
        Me.Controls.Add(Me.ucMTFixedDetail1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMasterTemplateFixedBase"
        Me.ShowIcon = False
        Me.Text = "Generate Master Template for Fixed Width Format (MA1010)"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ucMTFixedView1 As BTMU.Magic.UI.ucMasterTemplateFixedView
    Friend WithEvents ucMTFixedDetail1 As BTMU.Magic.UI.ucMasterTemplateFixedDetail

End Class
