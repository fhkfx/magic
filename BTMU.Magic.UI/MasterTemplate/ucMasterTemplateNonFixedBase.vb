Imports System.Windows.Forms

Public Class ucMasterTemplateNonFixedBase
    Inherits System.Windows.Forms.UserControl

    Protected _parent As frmMasterTemplateNonFixedBase
    Protected _isNew As Boolean

    Public Property ContainerForm() As frmMasterTemplateNonFixedBase
        Get
            Return _parent
        End Get
        Set(ByVal value As frmMasterTemplateNonFixedBase)
            _parent = value
        End Set
    End Property

    Public Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
        Set(ByVal value As Boolean)
            _isNew = value
        End Set
    End Property

End Class
