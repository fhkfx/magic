Imports System.Windows.Forms
Imports System.IO
Imports BTMU.Magic.Common
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

Public Class ucMasterTemplateNonFixedDetail

#Region " Private Fields "
    Private lstMaster As New MasterTemplateNonFixedCollection
    Private _recordState As String
    Private _isDraft As Boolean
    Private isFound As Integer
    'Private MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    'Private MsgReader As New Global.System.Resources.ResourceManager("BTMU.Magic.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    Private MsgReader As Global.System.Resources.ResourceManager

    Private _outputFormatList As New DataTable
    Private Enum eRecordState
        ADD
        EDIT
        VIEW
    End Enum

    Private Enum mode
        top = 0
        up = 1
        down = 2
        bottom = 3
    End Enum

    Public Property RecordState() As String
        Get
            Return _recordState
        End Get
        Set(ByVal value As String)
            _recordState = value
        End Set
    End Property

    Public Property IsDraft() As Boolean
        Get
            Return _isDraft
        End Get
        Set(ByVal value As Boolean)
            _isDraft = value
        End Set
    End Property

#End Region

#Region " Control Events "
    Private Sub ucMasterTemplateNonFixedDetail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _outputFormatList.Columns.Add("OutputFormat", GetType(String))
        _outputFormatList.Rows.Add("")
        _outputFormatList.Rows.Add(BULKVNFormat.OutputFormatString)
        _outputFormatList.Rows.Add(CWSFormat.OutputFormatString)
        'MAGIC HK Enhancement (RTMS formats)
        _outputFormatList.Rows.Add(FundsTransferInstructionFormat.OutputFormatString)
        '05062018 FTI II with no header (FHK Maggie)
        _outputFormatList.Rows.Add(FundsTransferInstructionFormatII.OutputFormatString)
        _outputFormatList.Rows.Add(AutoDebitFormat.OutputFormatString)
        _outputFormatList.Rows.Add(AutoChequeFormat.OutputFormatString)

        '05062018 AutoCheque ACMS with no header (FHK Maggie)
        _outputFormatList.Rows.Add(AutoChequeFormatII.OutputFormatString)

        _outputFormatList.Rows.Add(GCMSFormat.OutputFormatString)
        _outputFormatList.Rows.Add(GCMSPlusFormat.OutputFormatString)

        _outputFormatList.Rows.Add(iFTSFormat.OutputFormatString)
        '_outputFormatList.Rows.Add(iFTS2GenericFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iFTS2MultiLineFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iFTS2SingleLineFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iRTMSCIFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iRTMSGCFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iRTMSGDFormat.OutputFormatString)
        _outputFormatList.Rows.Add(iRTMSRMFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OiRTMSCIFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OiRTMSGCFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OiRTMSGDFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OiRTMSRMFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OMAKASEFormat.OutputFormatString)
        _outputFormatList.Rows.Add(OMAKASEFormatII.OutputFormatString)
        'CARROT (2013-03-05 Added by FHK)
        _outputFormatList.Rows.Add(CarrotCustomerDataFormat.OutputFormatString)
        _outputFormatList.Rows.Add(CarrotInvoiceDataFormat.OutputFormatString)
        _outputFormatList.Rows.Add(GCMSPlusCreditDataFormat.OutputFormatString)
        'CFC (FHK Maggie 2018-07-05)
        _outputFormatList.Rows.Add(CashForecastingFormat.OutputFormatString)
        'FPS (FHK Maggie 2018-07-05)
        _outputFormatList.Rows.Add(FPSFormat.OutputFormatString)

        OutputFormatCombo.DataSource = _outputFormatList
        OutputFormatCombo.DisplayMember = "OutputFormat"
        OutputFormatCombo.ValueMember = "OutputFormat"
        MasterTemplateNonFixedCollectionBindingSource.DataSource = lstMaster
        MasterTemplateNonFixedDetailCollectionDataGridView.EditMode = DataGridViewEditMode.EditOnEnter
    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click
        RecordState = eRecordState.EDIT
        ButtonState(_recordState)
    End Sub

    Private Sub btnDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDraft.Click
        Try

            Dim objE As MasterTemplateNonFixed
            Dim fileToBeDeleted As String = ""

            MsgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

            'Checking duplicate template name
            If TemplateNameTextBox.Enabled = True And _
                (System.IO.File.Exists(MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension) Or _
                System.IO.File.Exists(MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)) Then
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Template Name already exists. Please use different Template Name.", "")
                Exit Sub
            End If

            'Checking forbidden characters
            isFound = TemplateNameTextBox.Text.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(TemplateNameTextBox.Text) AndAlso isFound <> -1 Then
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(String.Format(MsgReader.GetString("E01000010"), "Template Name"), "")
                Exit Sub
            End If


            objE = MasterTemplateNonFixedCollectionBindingSource.Current
            objE.ApplyEdit()

            If IsNothingOrEmptyString(objE.TemplateName) Then
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Please fill in Template Name field.", "")
                Exit Sub
            End If

            objE.IsDraft = True
            'Save File
            If Not IsDraft Then fileToBeDeleted = MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension
            objE.SaveToFile2(MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension, fileToBeDeleted)
            'Delete File
            'Logging
            If RecordState = eRecordState.ADD Then
                Log_CreateMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
            ElseIf eRecordState.EDIT Then
                Log_ModifyMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
            End If

            MessageBox.Show("Master Template saved successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ErrorProvider1.Clear()

            ContainerForm.ucMTNonFixedView1.PopulateViewGrid()
            ContainerForm.ShowView()

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try


            Dim objE As MasterTemplateNonFixed

            'Checking duplicate template name
            If TemplateNameTextBox.Enabled = True And _
                (System.IO.File.Exists(MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension) Or _
                System.IO.File.Exists(MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)) Then
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Template Name already exists. Please use different Template Name.", "")
                Exit Sub
            End If


            objE = MasterTemplateNonFixedCollectionBindingSource.Current
            objE.ApplyEdit()

            'Checking data grid
            If objE.MasterTemplateNonFixedDetailCollection.Count = 0 Then
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Please fill in datagrid.", "")
                Exit Sub
            End If

            If SaveCleanData(objE) Then



                MessageBox.Show("Master Template saved successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

                ContainerForm.ucMTNonFixedView1.PopulateViewGrid()
                ContainerForm.ShowView()
            End If

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Handle button cancel event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Ask confirmation if there is any changes in data.</remarks>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If RecordState = eRecordState.VIEW Then
            ContainerForm.ucMTNonFixedView1.PopulateViewGrid()
            ContainerForm.ShowView()
        Else
            Dim dlgResult As DialogResult = ProcessDataDirtyConfirmation()

            If dlgResult = DialogResult.Yes Then
                ContainerForm.ucMTNonFixedView1.PopulateViewGrid()
                ContainerForm.ShowView()
            ElseIf dlgResult = DialogResult.No Then
                ContainerForm.ucMTNonFixedView1.PopulateViewGrid()
                ContainerForm.ShowView()
            Else
                'Do Nothing
            End If
        End If
        ErrorProvider1.Clear()

    End Sub

    Private Sub MasterTemplateNonFixedDetailCollectionDataGridView_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles MasterTemplateNonFixedDetailCollectionDataGridView.CellContentClick
        If e.RowIndex >= 0 And MasterTemplateNonFixedDetailCollectionDataGridView.Columns(e.ColumnIndex).HeaderText = "X" Then
            If Not _recordState = eRecordState.VIEW Then
                If Not MasterTemplateNonFixedDetailCollectionDataGridView(e.ColumnIndex, e.RowIndex).Value Is Nothing Then
                    If Not MasterTemplateNonFixedCollectionBindingSource.Current Is Nothing Then
                        Dim dlgResult As DialogResult
                        Dim currentMasterTemplateNonFixedDetail As MasterTemplateNonFixedDetail = MasterTemplateNonFixedDetailCollectionBindingSource.Current
                        dlgResult = MessageBox.Show( _
                        String.Format("Are you sure you want to remove field name: {0}?", currentMasterTemplateNonFixedDetail.FieldName), "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                        If dlgResult = Windows.Forms.DialogResult.Yes Then

                            MasterTemplateNonFixedDetailCollectionBindingSource.RaiseListChangedEvents = False
                            MasterTemplateNonFixedDetailCollectionBindingSource.RemoveCurrent()
                            MasterTemplateNonFixedDetailCollectionBindingSource.RaiseListChangedEvents = True
                            MasterTemplateNonFixedDetailCollectionBindingSource.ResetBindings(False)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub MasterTemplateNonFixedDetailCollectionDataGridView_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles MasterTemplateNonFixedDetailCollectionDataGridView.EditingControlShowing
        If e.Control.GetType().ToString() = "System.Windows.Forms.DataGridViewTextBoxEditingControl" Then
            Dim txtEdit As TextBox = CType(e.Control, TextBox)
            If MasterTemplateNonFixedDetailCollectionDataGridView.Columns(MasterTemplateNonFixedDetailCollectionDataGridView.CurrentCell.ColumnIndex).DataPropertyName = "DataLength" Then
                AddHandler txtEdit.KeyPress, AddressOf IntegerTextBox_KeyPress
                'ElseIf MasterTemplateNonFixedDetailCollectionDataGridView.Columns(MasterTemplateNonFixedDetailCollectionDataGridView.CurrentCell.ColumnIndex).DataPropertyName = "FieldName" Then
                '    AddHandler txtEdit.KeyPress, AddressOf BeginWithAlphabet_KeyPress
            Else
                RemoveHandler txtEdit.KeyPress, AddressOf IntegerTextBox_KeyPress
            End If
        End If
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        Try
            With MasterTemplateNonFixedDetailCollectionDataGridView
                .CurrentRow.Selected = True
                swapRows(MasterTemplateNonFixedDetailCollectionDataGridView, mode.up)
                If .CurrentRow.Index <> 0 Then .CurrentCell = .Item(0, .CurrentRow.Index - 1)
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        Try
            With MasterTemplateNonFixedDetailCollectionDataGridView
                .CurrentRow.Selected = True
                swapRows(MasterTemplateNonFixedDetailCollectionDataGridView, mode.down)
                If .CurrentRow.Index + 1 <> .Rows.Count - 1 Then .CurrentCell = .Item(0, .CurrentRow.Index + 1)
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region " Sub and Function "
    ''' <summary>
    ''' Handle validation process from several objects in header section
    ''' </summary>
    ''' <param name="sender">Textbox Control</param>
    ''' <param name="e">System Argument</param>
    Private Sub Control_Validated(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles _
        TemplateNameTextBox.Validated, _
        DescriptionTextBox.Validated, _
        CustomerOutputExtensionTextBox.Validated

        Dim ctl As Control = CType(sender, Control)
        Dim bnd As Binding
        Dim objE As MasterTemplateNonFixed

        objE = MasterTemplateNonFixedCollectionBindingSource.Current
        For Each bnd In ctl.DataBindings
            If bnd.IsBinding Then
                'Dim obj As System.ComponentModel.IDataErrorInfo = _
                '  CType(bnd.DataSource, System.ComponentModel.IDataErrorInfo)
                Dim obj As System.ComponentModel.IDataErrorInfo = _
                  CType(objE, System.ComponentModel.IDataErrorInfo)
                ErrorProvider1.SetError( _
                  ctl, obj.Item(bnd.BindingMemberInfo.BindingField))
            End If
        Next

    End Sub

    ''' <summary>
    ''' Trigger validation event to control and its child
    ''' </summary>
    ''' <param name="ctl"></param>
    ''' <remarks></remarks>
    Private Sub ValidateAllControl(ByVal ctl As Control)
        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                ValidateAllControl(ctlDetail)
            Else
                Control_Validated(ctlDetail, Nothing)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Limit a textbox on a userform to integer input only
    ''' </summary>
    ''' <param name="sender">Textbox Object</param>
    ''' <param name="e">Key Press event argument</param>
    Private Sub IntegerTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim temp As String
        Dim intValue As Integer
        Dim txtBox As TextBox = CType(sender, TextBox)
        temp = txtBox.Text + e.KeyChar

        If Not (Integer.TryParse(temp, intValue) Or e.KeyChar = ControlChars.Back) Then
            e.Handled = True
        End If

    End Sub

    ''' <summary>
    ''' Handle Disable/Enable objects in several Mode
    ''' </summary>
    ''' <param name="pMode">Add, View and Edit Mode</param>
    ''' <remarks></remarks>
    Private Sub ButtonState(ByVal pMode As String)
        If pMode = eRecordState.VIEW Then
            btnModify.Enabled = True
            btnSave.Enabled = False
            btnDraft.Enabled = False
            btnUp.Enabled = False
            btnDown.Enabled = False
            MasterTemplateNonFixedDetailCollectionDataGridView.ReadOnly = True
            MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToDeleteRows = False
            MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToAddRows = False
            grpTemplate.Enabled = False
            grpOutput.Enabled = False
        Else 'If pMode = eRecordState.EDIT Or pMode = eRecordState.ADD Then
            btnModify.Enabled = False
            btnSave.Enabled = True
            btnDraft.Enabled = True
            btnUp.Enabled = True
            btnDown.Enabled = True
            MasterTemplateNonFixedDetailCollectionDataGridView.ReadOnly = False
            MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToDeleteRows = True
            MasterTemplateNonFixedDetailCollectionDataGridView.AllowUserToAddRows = True
            grpTemplate.Enabled = True
            grpOutput.Enabled = True
        End If

        If pMode = eRecordState.ADD Then
            TemplateNameTextBox.Enabled = True
            TemplateNameTextBox.Focus()
            TemplateStatusCheckBox.Checked = True
        Else
            TemplateNameTextBox.Enabled = False
            DescriptionTextBox.Focus()
        End If

        If pMode = eRecordState.EDIT Then
            TemplateStatusCheckBox.Enabled = True
        Else
            TemplateStatusCheckBox.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' To duplicate existing template
    ''' </summary>
    ''' <param name="fileName">Template File Name</param>
    Public Sub DuplicateExisting(ByVal fileName As String)
        Dim item As MasterTemplateNonFixed

        Try
            RecordState = eRecordState.ADD

            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = False
            lstMaster.Clear()

            item = (New MasterTemplateNonFixed).LoadFromFile2(fileName)
            lstMaster.Add(item)


            item.TemplateName = item.TemplateName & " Copy"
            TemplateNameTextBox.Text = item.TemplateName
            item.Validate()

            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = True
            MasterTemplateNonFixedCollectionBindingSource.ResetBindings(False)

            ButtonState(_recordState)

        Catch ex As System.Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("", "")
        End Try

    End Sub

    ''' <summary>
    ''' To edit existing template
    ''' </summary>
    ''' <param name="fileName">Template File Name</param>
    Public Sub EditExisting(ByVal fileName As String)
        Dim item As MasterTemplateNonFixed

        Try
            RecordState = eRecordState.EDIT

            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = False
            MasterTemplateNonFixedDetailCollectionBindingSource.RaiseListChangedEvents = False
            lstMaster.Clear()

            item = (New MasterTemplateNonFixed).LoadFromFile2(fileName)
            lstMaster.Add(item)

            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = True
            MasterTemplateNonFixedCollectionBindingSource.ResetBindings(False)

            MasterTemplateNonFixedDetailCollectionBindingSource.RaiseListChangedEvents = True
            MasterTemplateNonFixedDetailCollectionBindingSource.ResetBindings(False)

            item.Validate()

            ButtonState(_recordState)

        Catch ex As System.Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("", "")
        End Try

    End Sub

    ''' <summary>
    ''' To display existing template
    ''' </summary>
    ''' <param name="fileName">Template File Name</param>
    Public Sub ViewExisting(ByVal fileName As String)
        Dim item As MasterTemplateNonFixed

        Try
            RecordState = eRecordState.VIEW
            ButtonState(_recordState)
            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = False
            lstMaster.Clear()

            item = (New MasterTemplateNonFixed).LoadFromFile2(fileName)
            lstMaster.Add(item)

            item.Validate()

            MasterTemplateNonFixedCollectionBindingSource.RaiseListChangedEvents = True
            MasterTemplateNonFixedCollectionBindingSource.ResetBindings(False)

        Catch ex As System.Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("", "")
        End Try

    End Sub

    ''' <summary>
    ''' To add new template
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AddNew()
        Dim obj As New MasterTemplateNonFixed
        IsDraft = True
        RecordState = eRecordState.ADD
        lstMaster.Clear()
        lstMaster.Add(obj)
        ButtonState(_recordState)
        obj.TemplateStatus = True
    End Sub

    ''' <summary>
    ''' This validation will be performed when user click save button
    ''' </summary>
    ''' <param name="objE">Master Template Data Table</param>
    ''' <returns>True/False</returns>
    Private Function SaveCleanData(ByRef objE As MasterTemplateNonFixed) As Boolean
        Dim fileToBeDeleted As String = ""
        Dim objDetail As MasterTemplateNonFixedDetail
        Dim isContains1, isContains2, isContains3 As Boolean

        objE.Validate()

        If (objE.IsSavable) Then
            'If objE.IsValid Then

            'Field Names Settlement Account No, Currency, Remittance Amount  should exist when output format is "GCMS"
            If objE.OutputFormat = GCMSFormat.OutputFormatString Then
                For Each objDetail In objE.MasterTemplateNonFixedDetailCollection
                    If Not isContains1 Then isContains1 = IIf(objDetail.FieldName = "Settlement Account No", True, False)
                    If Not isContains2 Then isContains2 = IIf(objDetail.FieldName = "Currency", True, False)
                    If Not isContains3 Then isContains3 = IIf(objDetail.FieldName = "Remittance Amount", True, False)
                    If isContains1 AndAlso isContains2 AndAlso isContains3 Then Exit For
                Next

                If Not isContains1 Or Not isContains2 Or Not isContains3 Then
                    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Field Names Settlement Account No, Currency, Remittance Amount  should exist when output format is GCMS", "")
                    Return False
                End If
            End If


            If IsDraft Then fileToBeDeleted = MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension
            objE.IsDraft = False

            objE.SaveToFile2(MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension, fileToBeDeleted)
            If RecordState = eRecordState.ADD Then
                Log_CreateMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
            ElseIf eRecordState.EDIT Then
                Log_ModifyMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
            End If
            Return True
            'End If
        Else
            Dim errorInfo As System.ComponentModel.IDataErrorInfo
            Dim errorMessage As String
            Dim fistError As New System.Text.StringBuilder
            Dim detailError As New System.Text.StringBuilder
            Dim detailItem As MasterTemplateNonFixedDetail

            errorInfo = CType(objE, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If Not IsNothingOrEmptyString(errorMessage) Then
                Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                Dim temp As String
                For Each temp In errorList
                    If fistError.Length > 0 Then
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", temp))
                    Else
                        If (temp.Trim.Length > 0) Then fistError.AppendLine(String.Format("{0}", temp))
                    End If
                Next
            End If

            For Each detailItem In objE.MasterTemplateNonFixedDetailCollection
                errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
                errorMessage = errorInfo.Error
                If fistError.Length = 0 Then
                    If Not IsNothingOrEmptyString(errorMessage) Then
                        Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                        Dim temp As String
                        For Each temp In errorList
                            If fistError.Length > 0 Then
                                If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Template Detail (Position: {0}) - {1}", detailItem.Sequence, temp))
                            Else
                                If (temp.Trim.Length > 0) Then fistError.AppendLine(String.Format("Template Detail (Position: {0}) - {1}", detailItem.Sequence, temp))
                            End If
                        Next
                    End If
                Else
                    If Not IsNothingOrEmptyString(errorMessage) Then
                        Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                        Dim temp As String
                        For Each temp In errorList
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Template Detail (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Next
                    End If
                End If
            Next


            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(fistError.ToString, detailError.ToString)
            ValidateAllControl(Me)

            Return False
        End If
    End Function

    ''' <summary>
    ''' This validation will be performed when user click save as draft button
    ''' </summary>
    ''' <returns>True/False</returns>
    Public Function ProcessDataDirtyConfirmation() As DialogResult
        Dim objE As MasterTemplateNonFixed
        Dim dlgResult As DialogResult = DialogResult.None
        Dim fileToBeDeleted As String = ""
        objE = MasterTemplateNonFixedCollectionBindingSource.Current
        MsgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)


        If Not (objE Is Nothing) Then
            If objE.IsDirty Then
                dlgResult = MessageBox.Show("Data has been changed. Do you want to save the data?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)
                If dlgResult = DialogResult.Yes Then
                    'Checking duplicate template name
                    If TemplateNameTextBox.Enabled = True And _
                        (System.IO.File.Exists(MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension) Or _
                        System.IO.File.Exists(MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)) Then
                        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Template Name already exists. Please use different Template Name.", "")
                        Exit Function
                    End If

                    'Checking forbidden characters
                    isFound = TemplateNameTextBox.Text.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
                    If Not IsNothingOrEmptyString(TemplateNameTextBox.Text) AndAlso isFound <> -1 Then
                        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(String.Format(MsgReader.GetString("E01000010"), "Template Name"), "")
                        Exit Function
                    End If

                    If IsNothingOrEmptyString(objE.TemplateName) Then
                        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Please fill in Template Name field.", "")
                        Return DialogResult.Cancel
                    End If

                    objE.ApplyEdit()
                    'Process data save
                    If IsDraft Then
                        fileToBeDeleted = MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension
                        objE.IsDraft = True
                        objE.SaveToFile2(MasterTemplateViewDraftFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
                        If RecordState = eRecordState.ADD Then
                            Log_CreateMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
                        ElseIf eRecordState.EDIT Then
                            Log_ModifyMasterTemplate(frmLogin.GsUserName, MasterTemplateViewFolder & TemplateNameTextBox.Text & MasterTemplateFileExtension)
                        End If
                    Else
                        If Not SaveCleanData(objE) Then Return DialogResult.Cancel
                    End If

                ElseIf dlgResult = DialogResult.No Then
                    objE.CancelEdit()
                Else 'DialogResult = Cancel
                    'Do nothing
                End If
            Else 'not dirty
                dlgResult = DialogResult.No
            End If

        Else
            dlgResult = DialogResult.No
            'Throw New MagicException("System do not able to retrieve current binding object.")
        End If
        Return dlgResult
    End Function


#End Region



    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

End Class
