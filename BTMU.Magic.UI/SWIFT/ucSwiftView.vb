Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.Common

Public Class ucSwiftView

    Private fairTemplateList As New SwiftTemplateListCollection()
    Private draftTemplateList As New SwiftTemplateListCollection()

#Region " Public Property "
    ''' <summary>
    ''' This property is to get Name of Swift Template
    ''' </summary>
    ''' <value>Name of Swift Template</value>
    ''' <returns>Returns Name of Swift Template</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SwiftTemplateFile() As String
        Get

            Return System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                        , dgvView.CurrentRow.Cells("dgvCellSwiftTemplateName").Value.ToString()) _
                        & CommonTemplateModule.CommonTemplateFileExtension

        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Draft Swift Template
    ''' </summary>
    ''' <value>Draft Swift Template</value>
    ''' <returns>Returns Draft Swift Template</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SwiftTemplateFileDraft() As String
        Get
            Return System.IO.Path.Combine(CommonTemplateModule.SwiftDraftFolder _
                        , dgvDraft.CurrentRow.Cells("dgvDraftCelSwiftTemplateName").Value.ToString()) _
                         & CommonTemplateModule.CommonTemplateFileExtension
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Master Template Name
    ''' </summary>
    ''' <value>Master Template Name</value>
    ''' <returns>Returns Master Template Name</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MasterTemplateName() As String
        Get

            Return dgvView.CurrentRow.Cells("dgvCellMasterTemplateName").Value.ToString()

        End Get
    End Property

#End Region

#Region " Handlers for Action New/Edit/View/Duplicate/Close "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseWindowSwiftTemplate()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ContainerForm.ShowDetail()
        ContainerForm.UcSwiftTemplate1.AddNew()
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click

        If dgvView.CurrentRow Is Nothing Then
            E02000010()
        Else

            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcSwiftTemplate1.ViewExisting(SwiftTemplateFile)

            Else
                E02000020()
            End If

        End If

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If dgvView.CurrentRow Is Nothing Then
            E02000010()
        Else

            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcSwiftTemplate1.EditExisting(SwiftTemplateFile)

            Else
                E02000020()
            End If
        End If

    End Sub

    Private Sub btnDuplicate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click

        If dgvView.CurrentRow Is Nothing Then
            E02000010()
        Else
            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcSwiftTemplate1.DuplicateExisting(SwiftTemplateFile)

            Else
                E02000020()
            End If

        End If

    End Sub

    Private Sub btnEditDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then
            E02000010()
        Else
            ContainerForm.ShowDetail()
            ContainerForm.UcSwiftTemplate1.EditExisting(SwiftTemplateFileDraft)
        End If

    End Sub

    Private Sub btnDuplicateDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicateDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then
            E02000010()
        Else
            ContainerForm.ShowDetail()
            ContainerForm.UcSwiftTemplate1.DuplicateExisting(SwiftTemplateFileDraft, True)

        End If

    End Sub

    Private Sub btnCloseDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseDraft.Click
        CloseWindowSwiftTemplate()
    End Sub

    Private Sub CloseWindowSwiftTemplate()
        ContainerForm.Dispose()
    End Sub


#End Region

#Region " Load Fair/Draft Templates "

    Private Sub ucSwiftView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dgvView.DataSource = bindingSrcSwiftTemplateCollection
        bindingSrcSwiftTemplateCollection.DataSource = fairTemplateList.DefaultView

        dgvDraft.DataSource = bindingSrcSwiftTemplateDraftCollection
        bindingSrcSwiftTemplateDraftCollection.DataSource = draftTemplateList.DefaultView

        PopulateViewGrids()
        tabPgView.SelectedIndex = 0
        txtFilter.Focus()
    End Sub

    Friend Sub PopulateViewGrids()

        Dim filePath As String
        Dim content As String
        Dim swiftTemplateFiles As New List(Of String)
        Dim draftfiles() As String


        Dim serializerSwiftTemplateList As New XmlSerializer(GetType(SwiftTemplateList))

        bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
        fairTemplateList.Clear()
        draftTemplateList.Clear()

        If Not File.Exists(CommonTemplateModule.SwiftFolder) Then Directory.CreateDirectory(CommonTemplateModule.SwiftFolder)
        If Not File.Exists(CommonTemplateModule.SwiftDraftFolder) Then Directory.CreateDirectory(CommonTemplateModule.SwiftDraftFolder)


        Dim files() As String = System.IO.Directory.GetFiles(CommonTemplateModule.SwiftFolder _
                           , "*" & CommonTemplateModule.CommonTemplateFileExtension)

        If files.Length > 0 Then : swiftTemplateFiles.AddRange(files) : End If

        'Added the If Condition so as to prevent duplicate entries if SwiftFolder and SwiftDraftFolder are set as the same.
        'SRS/BTMU/2010/0051
        If String.Equals(CommonTemplateModule.SwiftFolder.Trim, CommonTemplateModule.SwiftDraftFolder.Trim, StringComparison.InvariantCultureIgnoreCase) Then
            draftfiles = Nothing
        Else
            draftfiles = System.IO.Directory.GetFiles(CommonTemplateModule.SwiftDraftFolder _
                                      , "*" & CommonTemplateModule.CommonTemplateFileExtension)



            If draftfiles.Length > 0 Then : swiftTemplateFiles.AddRange(draftfiles) : End If
        End If
        

        For Each filePath In swiftTemplateFiles

            Try
                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New SwiftTemplateList

                Iterator.MoveNext()
                content = Iterator.Current.Value


                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerSwiftTemplateList.Deserialize(stream), SwiftTemplateList)

                If item.IsDraft Then
                    draftTemplateList.Add(item)
                Else
                    fairTemplateList.Add(item)
                End If
                stream.Dispose()
            Catch ex As Exception
            End Try
        Next
        bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
        bindingSrcSwiftTemplateCollection.ResetBindings(False)

    End Sub

    ''' <summary>
    ''' Refreshes the Template List by reloading all the Templates from the Template Base Directory
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub reloadTemplates()
        PopulateViewGrids()
    End Sub

#End Region

#Region " Filter Templates "

    Private Sub btnFilterTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterTemplate.Click

        FilterTemplates(bindingSrcSwiftTemplateCollection, txtFilter.Text)

    End Sub


    Private Sub btnFilterDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterDraft.Click

        FilterTemplates(bindingSrcSwiftTemplateDraftCollection, txtFilterDraft.Text)

    End Sub

    Private Sub FilterTemplates(ByVal bs As BindingSource, ByVal filterString As String)

        If String.IsNullOrEmpty(filterString) Then

            bs.RemoveFilter()

        Else

            Try
                bs.Filter = "SwiftTemplate = '" & filterString.Trim() & "%'"
                bs.ResetBindings(False)
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

    End Sub

#End Region

   
    Private Sub btnNewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewDraft.Click
        btnNew_Click(sender, e)
    End Sub

    Private Sub btnViewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then
            E02000010()
        Else

            ContainerForm.ShowDetail()
            ContainerForm.UcSwiftTemplate1.ViewExisting(SwiftTemplateFileDraft)

        End If
    End Sub

    Private Sub E02000010()
        Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.UI.Resources", GetType(CommonTemplateModule).Assembly)
        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E020000101"), "")
    End Sub

    Private Sub E02000020()
        Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.UI.Resources", GetType(CommonTemplateModule).Assembly)
        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
    End Sub

End Class
