Imports BTMU.Magic.Common

''' <summary>
''' Form for Swift Template
''' </summary>
''' <remarks></remarks>
Public Class frmSwiftTemplate

    Private Sub frmSwiftTemplate_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If UcSwiftTemplate1.Visible Then
            If UcSwiftTemplate1.ConfirmBeforeCloseAndCancel() Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub SwiftTemplate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UcSwiftTemplate1.ContainerForm = Me
        UcSwiftView1.ContainerForm = Me
    End Sub
    ''' <summary>
    ''' Displays the Detail Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDetail()
        UcSwiftView1.Visible = False
        UcSwiftTemplate1.Visible = True
    End Sub
    ''' <summary>
    ''' Displays the View Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowView()
        UcSwiftView1.reloadTemplates()
        UcSwiftView1.Visible = True
        UcSwiftTemplate1.Visible = False
        UcSwiftView1.tabPgView.SelectedIndex = 0
        UcSwiftView1.txtFilter.Focus()
        UcSwiftView1.txtFilter.Text = ""
    End Sub
    ''' <summary>
    ''' Displays the View Panel for Draft Template
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDraftView()
        ShowView()
        UcSwiftView1.tabPgView.SelectedIndex = 1
        UcSwiftView1.txtFilterDraft.Focus()
        UcSwiftView1.txtFilterDraft.Text = ""
    End Sub

    Private Sub UcSwiftView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UcSwiftView1.Load
        ShowView()
    End Sub
End Class

