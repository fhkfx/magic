<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSwiftTemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UcSwiftView1 = New BTMU.Magic.UI.ucSwiftView
        Me.UcSwiftTemplate1 = New BTMU.Magic.UI.ucSwiftTemplate
        Me.SuspendLayout()
        '
        'UcSwiftView1
        '
        Me.UcSwiftView1.ContainerForm = Nothing
        Me.UcSwiftView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcSwiftView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSwiftView1.IsNew = False
        Me.UcSwiftView1.Location = New System.Drawing.Point(0, 0)
        Me.UcSwiftView1.Name = "UcSwiftView1"
        Me.UcSwiftView1.Size = New System.Drawing.Size(909, 569)
        Me.UcSwiftView1.TabIndex = 0
        '
        'UcSwiftTemplate1
        '
        Me.UcSwiftTemplate1.AutoScroll = True
        Me.UcSwiftTemplate1.ContainerForm = Nothing
        Me.UcSwiftTemplate1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcSwiftTemplate1.IsNew = False
        Me.UcSwiftTemplate1.Location = New System.Drawing.Point(0, 0)
        Me.UcSwiftTemplate1.Name = "UcSwiftTemplate1"
        Me.UcSwiftTemplate1.Size = New System.Drawing.Size(909, 569)
        Me.UcSwiftTemplate1.TabIndex = 1
        '
        'frmSwiftTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(909, 569)
        Me.Controls.Add(Me.UcSwiftTemplate1)
        Me.Controls.Add(Me.UcSwiftView1)
        Me.MinimumSize = New System.Drawing.Size(868, 603)
        Me.Name = "frmSwiftTemplate"
        Me.ShowIcon = False
        Me.Text = "BTMU-MAGIC - Generate SWIFT Transaction Template (MA2020)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcSwiftView1 As BTMU.Magic.UI.ucSwiftView
    Friend WithEvents UcSwiftTemplate1 As BTMU.Magic.UI.ucSwiftTemplate
End Class
