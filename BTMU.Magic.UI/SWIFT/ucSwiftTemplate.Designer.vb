<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSwiftTemplate
    Inherits Magic.UI.ucSwiftTemplateBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.bindingSrcSwiftTemplateCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcOutputTemplate = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcRowFilterSourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcEditableSettingsBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcTanslateBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcLookupBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcLookupSourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcCalculatedFieldsBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcOperand1SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcOperand2SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcOperand3SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnSaveAsDraft = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnModify = New System.Windows.Forms.Button
        Me.bindingSrcStringManipulationBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.DefaultBankValuesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcTranslatorSettingCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.CommonTemplateHelperBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupBox15 = New System.Windows.Forms.GroupBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton8 = New System.Windows.Forms.RadioButton
        Me.GroupBox16 = New System.Windows.Forms.GroupBox
        Me.RadioButton9 = New System.Windows.Forms.RadioButton
        Me.RadioButton10 = New System.Windows.Forms.RadioButton
        Me.GroupBox17 = New System.Windows.Forms.GroupBox
        Me.ComboBox7 = New System.Windows.Forms.ComboBox
        Me.ComboBox8 = New System.Windows.Forms.ComboBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.GroupBox18 = New System.Windows.Forms.GroupBox
        Me.RadioButton11 = New System.Windows.Forms.RadioButton
        Me.RadioButton12 = New System.Windows.Forms.RadioButton
        Me.DataGridView8 = New System.Windows.Forms.DataGridView
        Me.DataGridViewComboBoxColumn1 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewComboBoxColumn2 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.DataGridViewButtonColumn1 = New System.Windows.Forms.DataGridViewButtonColumn
        Me.GroupBox19 = New System.Windows.Forms.GroupBox
        Me.ComboBox9 = New System.Windows.Forms.ComboBox
        Me.ComboBox10 = New System.Windows.Forms.ComboBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn
        Me.ErrorProvider1 = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.bindingSrcRowFilterCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcMapSourceFieldCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcMapBankFieldCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcEditableSettingCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcLookupSettingCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcCalculatedFieldCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bindingSrcStringManipulationCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.CommonTemplateHelperBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CommonTemplateHelperBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.gbxTemplateSettings = New System.Windows.Forms.GroupBox
        Me.chkEnable = New System.Windows.Forms.CheckBox
        Me.lblSwiftTemplateName = New System.Windows.Forms.Label
        Me.txtSwiftTemplateName = New System.Windows.Forms.TextBox
        Me.lblOutputTemplate = New System.Windows.Forms.Label
        Me.cboOutputTemplate = New System.Windows.Forms.ComboBox
        Me.pnlBody = New System.Windows.Forms.Panel
        Me.tabDetail = New System.Windows.Forms.TabControl
        Me.tbPgRetrieve = New System.Windows.Forms.TabPage
        Me.btnSeparate = New System.Windows.Forms.Button
        Me.gbxDetailAdviceSettings = New System.Windows.Forms.GroupBox
        Me.txtNoOfCharsPerAdviseRecord = New BTMU.Magic.UI.NumberBox
        Me.txtAdviseRecordDelimiter = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.rdoIRTMSDelimiter = New System.Windows.Forms.RadioButton
        Me.rdoIRTMSCharPos = New System.Windows.Forms.RadioButton
        Me.rdoIRTMSEveryRow = New System.Windows.Forms.RadioButton
        Me.gbxFilterSetting = New System.Windows.Forms.GroupBox
        Me.btnFilterSettingsUp = New System.Windows.Forms.Button
        Me.btnFilterSettingsDown = New System.Windows.Forms.Button
        Me.rdoFilterSettingOR = New System.Windows.Forms.RadioButton
        Me.rdoFilterSettingAND = New System.Windows.Forms.RadioButton
        Me.dgvFilterSettings = New System.Windows.Forms.DataGridView
        Me.dgvBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvCmbFilterField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCmbFilterDataType = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCmbFilterOperator = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvTxtFilterValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblSourceFileName = New System.Windows.Forms.Label
        Me.gbxDuplicateTxnRef = New System.Windows.Forms.GroupBox
        Me.cboDuplicateTxnRefField3 = New System.Windows.Forms.ComboBox
        Me.cboDuplicateTxnRefField2 = New System.Windows.Forms.ComboBox
        Me.cboDuplicateTxnRefField1 = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.gbxRemittanceAmtSettings = New System.Windows.Forms.GroupBox
        Me.rdoCents = New System.Windows.Forms.RadioButton
        Me.rdoDollars = New System.Windows.Forms.RadioButton
        Me.gbxNumberSettings = New System.Windows.Forms.GroupBox
        Me.cboDecimalSep = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.btnBrowseSourceFile = New System.Windows.Forms.Button
        Me.txtSourceFileName = New System.Windows.Forms.TextBox
        Me.gbxDateSettings = New System.Windows.Forms.GroupBox
        Me.cboDateType = New System.Windows.Forms.ComboBox
        Me.cboDateSep = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.ChkZerosInDate = New System.Windows.Forms.CheckBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSampleRowData = New System.Windows.Forms.TextBox
        Me.tbPgMap = New System.Windows.Forms.TabPage
        Me.btnClearAll = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.dgvBank = New System.Windows.Forms.DataGridView
        Me.BankFieldExtDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MappedFieldsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MappedValuesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BankSampleValueDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvSource = New System.Windows.Forms.DataGridView
        Me.SequenceDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SourceFieldNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SourceFieldValueDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tbPgTranslate = New System.Windows.Forms.TabPage
        Me.gbxEditableSetting = New System.Windows.Forms.GroupBox
        Me.btnEditableSettingRowDown = New System.Windows.Forms.Button
        Me.btnEditableSettingRowUp = New System.Windows.Forms.Button
        Me.dgvEditable = New System.Windows.Forms.DataGridView
        Me.dgvEditableBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvTranslatorEditableSettingCellBankField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.gbxTranslatorSetting = New System.Windows.Forms.GroupBox
        Me.btnTranslatorSettingRowUp = New System.Windows.Forms.Button
        Me.btnTranslatorSettingRowDown = New System.Windows.Forms.Button
        Me.dgvTranslator = New System.Windows.Forms.DataGridView
        Me.dgvTranslatorBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvTranslatorSettingsCellCmbBankField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvTranslatorSettingsCellTxtSourceValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvTranslatorCellCboDefaultValues = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvTranslatorCellChkBankValueEmpty = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.tbPgLookup = New System.Windows.Forms.TabPage
        Me.gbxLookup = New System.Windows.Forms.GroupBox
        Me.dgvLookup = New System.Windows.Forms.DataGridView
        Me.dgvLookupBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvLookupCellCmbBankField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupCellCmbSourceField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupCellTable = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupCellCboLookupKey = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupCellCboLookupValue = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.btnLookupRowDown = New System.Windows.Forms.Button
        Me.btnLookupRowUp = New System.Windows.Forms.Button
        Me.tbPgCalculatedValues = New System.Windows.Forms.TabPage
        Me.gbxCalculatedValues = New System.Windows.Forms.GroupBox
        Me.btnCalculatedFieldsRowDown = New System.Windows.Forms.Button
        Me.btnCalculatedFieldsRowUp = New System.Windows.Forms.Button
        Me.dgvCalculated = New System.Windows.Forms.DataGridView
        Me.dgvCalculatedBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvCalculatedFieldsCellCmbBankField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalculatedFieldsOperand1 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalculatedFieldsCellOperator1 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalculatedFieldsOperand2 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalculatedFieldsCellOperator2 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalculatedFieldsOperand3 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.tbPgStringManipulation = New System.Windows.Forms.TabPage
        Me.gbxStringManipulation = New System.Windows.Forms.GroupBox
        Me.btnStringManipulationRowDown = New System.Windows.Forms.Button
        Me.btnStringManipulationRowUp = New System.Windows.Forms.Button
        Me.dgvStringManipulation = New System.Windows.Forms.DataGridView
        Me.dgvStringManipulationBtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvStringManipulationCellCmbBankField = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvStringManipulationCellStringFunction = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.StartingPositionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumberOfCharactersDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.bindingSrcSwiftTemplateCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcOutputTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcRowFilterSourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcEditableSettingsBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcTanslateBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcLookupBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcLookupSourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcCalculatedFieldsBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcOperand1SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcOperand2SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcOperand3SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBottom.SuspendLayout()
        CType(Me.bindingSrcStringManipulationBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DefaultBankValuesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcTranslatorSettingCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommonTemplateHelperBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox19.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcRowFilterCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcMapSourceFieldCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcMapBankFieldCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcEditableSettingCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcLookupSettingCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcCalculatedFieldCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcStringManipulationCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommonTemplateHelperBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommonTemplateHelperBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTop.SuspendLayout()
        Me.gbxTemplateSettings.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        Me.tabDetail.SuspendLayout()
        Me.tbPgRetrieve.SuspendLayout()
        Me.gbxDetailAdviceSettings.SuspendLayout()
        Me.gbxFilterSetting.SuspendLayout()
        CType(Me.dgvFilterSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDuplicateTxnRef.SuspendLayout()
        Me.gbxRemittanceAmtSettings.SuspendLayout()
        Me.gbxNumberSettings.SuspendLayout()
        Me.gbxDateSettings.SuspendLayout()
        Me.tbPgMap.SuspendLayout()
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPgTranslate.SuspendLayout()
        Me.gbxEditableSetting.SuspendLayout()
        CType(Me.dgvEditable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxTranslatorSetting.SuspendLayout()
        CType(Me.dgvTranslator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPgLookup.SuspendLayout()
        Me.gbxLookup.SuspendLayout()
        CType(Me.dgvLookup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPgCalculatedValues.SuspendLayout()
        Me.gbxCalculatedValues.SuspendLayout()
        CType(Me.dgvCalculated, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPgStringManipulation.SuspendLayout()
        Me.gbxStringManipulation.SuspendLayout()
        CType(Me.dgvStringManipulation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bindingSrcSwiftTemplateCollection
        '
        Me.bindingSrcSwiftTemplateCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.SwiftTemplateCollection)
        '
        'bindingSrcOutputTemplate
        '
        Me.bindingSrcOutputTemplate.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateListCollection)
        '
        'bindingSrcRowFilterSourceField
        '
        Me.bindingSrcRowFilterSourceField.DataMember = "MapSourceFields"
        Me.bindingSrcRowFilterSourceField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcTanslateBankField
        '
        Me.bindingSrcTanslateBankField.DataMember = "MapBankFields"
        Me.bindingSrcTanslateBankField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcLookupBankField
        '
        Me.bindingSrcLookupBankField.DataMember = "MapBankFields"
        Me.bindingSrcLookupBankField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcLookupSourceField
        '
        Me.bindingSrcLookupSourceField.DataMember = "MapSourceFields"
        Me.bindingSrcLookupSourceField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcCalculatedFieldsBankField
        '
        Me.bindingSrcCalculatedFieldsBankField.DataMember = "MapBankFields"
        Me.bindingSrcCalculatedFieldsBankField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcOperand1SourceField
        '
        Me.bindingSrcOperand1SourceField.DataMember = "MapSourceFields"
        Me.bindingSrcOperand1SourceField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcOperand2SourceField
        '
        Me.bindingSrcOperand2SourceField.DataMember = "MapSourceFields"
        Me.bindingSrcOperand2SourceField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcOperand3SourceField
        '
        Me.bindingSrcOperand3SourceField.DataMember = "MapSourceFields"
        Me.bindingSrcOperand3SourceField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.btnCancel)
        Me.pnlBottom.Controls.Add(Me.btnPreview)
        Me.pnlBottom.Controls.Add(Me.btnSaveAsDraft)
        Me.pnlBottom.Controls.Add(Me.btnSave)
        Me.pnlBottom.Controls.Add(Me.btnModify)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 627)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(915, 51)
        Me.pnlBottom.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(396, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 43)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Location = New System.Drawing.Point(303, 4)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 43)
        Me.btnPreview.TabIndex = 10
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnSaveAsDraft
        '
        Me.btnSaveAsDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSaveAsDraft.Location = New System.Drawing.Point(210, 4)
        Me.btnSaveAsDraft.Name = "btnSaveAsDraft"
        Me.btnSaveAsDraft.Size = New System.Drawing.Size(87, 43)
        Me.btnSaveAsDraft.TabIndex = 9
        Me.btnSaveAsDraft.Text = "Save &As Draft"
        Me.btnSaveAsDraft.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(117, 4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 43)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnModify
        '
        Me.btnModify.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnModify.Location = New System.Drawing.Point(21, 4)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(87, 43)
        Me.btnModify.TabIndex = 7
        Me.btnModify.Text = "&Edit"
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'bindingSrcStringManipulationBankField
        '
        Me.bindingSrcStringManipulationBankField.DataMember = "MapBankFields"
        Me.bindingSrcStringManipulationBankField.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'DefaultBankValuesBindingSource
        '
        Me.DefaultBankValuesBindingSource.DataMember = "DefaultBankValues"
        Me.DefaultBankValuesBindingSource.DataSource = Me.bindingSrcTranslatorSettingCollection
        '
        'bindingSrcTranslatorSettingCollection
        '
        Me.bindingSrcTranslatorSettingCollection.DataMember = "TranslatorSettings"
        Me.bindingSrcTranslatorSettingCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'CommonTemplateHelperBindingSource
        '
        Me.CommonTemplateHelperBindingSource.DataSource = GetType(BTMU.Magic.UI.CommonTemplateModule)
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.ComboBox4)
        Me.GroupBox14.Controls.Add(Me.ComboBox5)
        Me.GroupBox14.Controls.Add(Me.ComboBox6)
        Me.GroupBox14.Controls.Add(Me.Label20)
        Me.GroupBox14.Controls.Add(Me.Label21)
        Me.GroupBox14.Controls.Add(Me.Label24)
        Me.GroupBox14.Location = New System.Drawing.Point(561, 15)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(253, 94)
        Me.GroupBox14.TabIndex = 2
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Duplicate Transaction Reference"
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(70, 62)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(151, 21)
        Me.ComboBox4.TabIndex = 5
        '
        'ComboBox5
        '
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(70, 38)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(151, 21)
        Me.ComboBox5.TabIndex = 4
        '
        'ComboBox6
        '
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(70, 16)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(151, 21)
        Me.ComboBox6.TabIndex = 3
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 43)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 13)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Ref. Field2"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 70)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Ref. Field3"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(6, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(58, 13)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Ref. Field1"
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.TextBox3)
        Me.GroupBox15.Controls.Add(Me.Label25)
        Me.GroupBox15.Controls.Add(Me.TextBox4)
        Me.GroupBox15.Controls.Add(Me.Label26)
        Me.GroupBox15.Controls.Add(Me.RadioButton1)
        Me.GroupBox15.Controls.Add(Me.RadioButton2)
        Me.GroupBox15.Controls.Add(Me.RadioButton8)
        Me.GroupBox15.Location = New System.Drawing.Point(561, 115)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(253, 116)
        Me.GroupBox15.TabIndex = 8
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "For iRTMS Format"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(166, 89)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(33, 20)
        Me.TextBox3.TabIndex = 6
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(147, 69)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 13)
        Me.Label25.TabIndex = 5
        Me.Label25.Text = "character"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(108, 66)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(33, 20)
        Me.TextBox4.TabIndex = 4
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 19)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(106, 13)
        Me.Label26.TabIndex = 3
        Me.Label26.Text = "New Advice Record "
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(24, 90)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(127, 17)
        Me.RadioButton1.TabIndex = 2
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "seperated by delimiter"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(24, 67)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(78, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = " after every"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Location = New System.Drawing.Point(24, 44)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(122, 17)
        Me.RadioButton8.TabIndex = 0
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "for every row of data"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.RadioButton9)
        Me.GroupBox16.Controls.Add(Me.RadioButton10)
        Me.GroupBox16.Location = New System.Drawing.Point(409, 15)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(146, 94)
        Me.GroupBox16.TabIndex = 2
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Remittance Amount Settings"
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Location = New System.Drawing.Point(63, 63)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(52, 17)
        Me.RadioButton9.TabIndex = 1
        Me.RadioButton9.TabStop = True
        Me.RadioButton9.Text = "Cents"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Location = New System.Drawing.Point(63, 20)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(57, 17)
        Me.RadioButton10.TabIndex = 0
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "Dollars"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.ComboBox7)
        Me.GroupBox17.Controls.Add(Me.ComboBox8)
        Me.GroupBox17.Controls.Add(Me.Label27)
        Me.GroupBox17.Controls.Add(Me.Label28)
        Me.GroupBox17.Location = New System.Drawing.Point(200, 15)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(203, 94)
        Me.GroupBox17.TabIndex = 2
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Number Settings"
        '
        'ComboBox7
        '
        Me.ComboBox7.FormattingEnabled = True
        Me.ComboBox7.Location = New System.Drawing.Point(129, 59)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(62, 21)
        Me.ComboBox7.TabIndex = 6
        '
        'ComboBox8
        '
        Me.ComboBox8.FormattingEnabled = True
        Me.ComboBox8.Location = New System.Drawing.Point(129, 16)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(59, 21)
        Me.ComboBox8.TabIndex = 5
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(6, 59)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(110, 13)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Thousand Separator :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(6, 19)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(100, 13)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Decimal Separator :"
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.RadioButton11)
        Me.GroupBox18.Controls.Add(Me.RadioButton12)
        Me.GroupBox18.Controls.Add(Me.DataGridView8)
        Me.GroupBox18.Location = New System.Drawing.Point(9, 115)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(546, 116)
        Me.GroupBox18.TabIndex = 7
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "Filter Setting"
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Location = New System.Drawing.Point(441, 82)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(41, 17)
        Me.RadioButton11.TabIndex = 2
        Me.RadioButton11.TabStop = True
        Me.RadioButton11.Text = "OR"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Location = New System.Drawing.Point(440, 44)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(48, 17)
        Me.RadioButton12.TabIndex = 1
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "AND"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'DataGridView8
        '
        Me.DataGridView8.AllowUserToDeleteRows = False
        Me.DataGridView8.AllowUserToResizeRows = False
        Me.DataGridView8.BackgroundColor = System.Drawing.SystemColors.ControlDark
        Me.DataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView8.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewComboBoxColumn1, Me.DataGridViewTextBoxColumn1, Me.DataGridViewComboBoxColumn2, Me.DataGridViewButtonColumn1})
        Me.DataGridView8.Dock = System.Windows.Forms.DockStyle.Left
        Me.DataGridView8.Location = New System.Drawing.Point(3, 16)
        Me.DataGridView8.Name = "DataGridView8"
        Me.DataGridView8.Size = New System.Drawing.Size(423, 97)
        Me.DataGridView8.TabIndex = 0
        '
        'DataGridViewComboBoxColumn1
        '
        Me.DataGridViewComboBoxColumn1.HeaderText = "Filter Field"
        Me.DataGridViewComboBoxColumn1.Name = "DataGridViewComboBoxColumn1"
        Me.DataGridViewComboBoxColumn1.Width = 135
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Filter Value"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 120
        '
        'DataGridViewComboBoxColumn2
        '
        Me.DataGridViewComboBoxColumn2.HeaderText = "Filter Equality"
        Me.DataGridViewComboBoxColumn2.Items.AddRange(New Object() {"Equal", "Not Equal"})
        Me.DataGridViewComboBoxColumn2.Name = "DataGridViewComboBoxColumn2"
        '
        'DataGridViewButtonColumn1
        '
        Me.DataGridViewButtonColumn1.HeaderText = "X"
        Me.DataGridViewButtonColumn1.Name = "DataGridViewButtonColumn1"
        Me.DataGridViewButtonColumn1.Width = 25
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.ComboBox9)
        Me.GroupBox19.Controls.Add(Me.ComboBox10)
        Me.GroupBox19.Controls.Add(Me.CheckBox3)
        Me.GroupBox19.Controls.Add(Me.Label29)
        Me.GroupBox19.Controls.Add(Me.Label30)
        Me.GroupBox19.Location = New System.Drawing.Point(12, 15)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(182, 94)
        Me.GroupBox19.TabIndex = 1
        Me.GroupBox19.TabStop = False
        Me.GroupBox19.Text = "Date Settings"
        '
        'ComboBox9
        '
        Me.ComboBox9.FormattingEnabled = True
        Me.ComboBox9.Location = New System.Drawing.Point(97, 40)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(73, 21)
        Me.ComboBox9.TabIndex = 4
        '
        'ComboBox10
        '
        Me.ComboBox10.FormattingEnabled = True
        Me.ComboBox10.Location = New System.Drawing.Point(97, 13)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(72, 21)
        Me.ComboBox10.TabIndex = 3
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(6, 67)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(168, 17)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "Zeros in Date(Eg:-1900/0101)"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(6, 46)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(63, 13)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Date Type :"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(6, 16)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(85, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Date Separator :"
        '
        'DataGridViewImageColumn1
        '
        Me.DataGridViewImageColumn1.HeaderText = "X"
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        Me.DataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewImageColumn1.Width = 20
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'bindingSrcRowFilterCollection
        '
        Me.bindingSrcRowFilterCollection.DataMember = "Filters"
        Me.bindingSrcRowFilterCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcMapSourceFieldCollection
        '
        Me.bindingSrcMapSourceFieldCollection.DataMember = "MapSourceFields"
        Me.bindingSrcMapSourceFieldCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcMapBankFieldCollection
        '
        Me.bindingSrcMapBankFieldCollection.DataMember = "MapBankFields"
        Me.bindingSrcMapBankFieldCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcEditableSettingCollection
        '
        Me.bindingSrcEditableSettingCollection.DataMember = "EditableSettings"
        Me.bindingSrcEditableSettingCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcLookupSettingCollection
        '
        Me.bindingSrcLookupSettingCollection.DataMember = "LookupSettings"
        Me.bindingSrcLookupSettingCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcCalculatedFieldCollection
        '
        Me.bindingSrcCalculatedFieldCollection.DataMember = "CalculatedFields"
        Me.bindingSrcCalculatedFieldCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'bindingSrcStringManipulationCollection
        '
        Me.bindingSrcStringManipulationCollection.DataMember = "StringManipulations"
        Me.bindingSrcStringManipulationCollection.DataSource = Me.bindingSrcSwiftTemplateCollection
        '
        'CommonTemplateHelperBindingSource1
        '
        Me.CommonTemplateHelperBindingSource1.DataSource = GetType(BTMU.Magic.UI.CommonTemplateModule)
        '
        'CommonTemplateHelperBindingSource2
        '
        Me.CommonTemplateHelperBindingSource2.DataSource = GetType(BTMU.Magic.UI.CommonTemplateModule)
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.gbxTemplateSettings)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(915, 88)
        Me.pnlTop.TabIndex = 0
        '
        'gbxTemplateSettings
        '
        Me.gbxTemplateSettings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxTemplateSettings.Controls.Add(Me.chkEnable)
        Me.gbxTemplateSettings.Controls.Add(Me.lblSwiftTemplateName)
        Me.gbxTemplateSettings.Controls.Add(Me.txtSwiftTemplateName)
        Me.gbxTemplateSettings.Controls.Add(Me.lblOutputTemplate)
        Me.gbxTemplateSettings.Controls.Add(Me.cboOutputTemplate)
        Me.gbxTemplateSettings.Location = New System.Drawing.Point(3, 6)
        Me.gbxTemplateSettings.Margin = New System.Windows.Forms.Padding(5)
        Me.gbxTemplateSettings.Name = "gbxTemplateSettings"
        Me.gbxTemplateSettings.Size = New System.Drawing.Size(908, 75)
        Me.gbxTemplateSettings.TabIndex = 1
        Me.gbxTemplateSettings.TabStop = False
        Me.gbxTemplateSettings.Text = "Template Settings"
        '
        'chkEnable
        '
        Me.chkEnable.AutoSize = True
        Me.chkEnable.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.bindingSrcSwiftTemplateCollection, "IsEnabled", True))
        Me.chkEnable.Location = New System.Drawing.Point(828, 47)
        Me.chkEnable.Name = "chkEnable"
        Me.chkEnable.Size = New System.Drawing.Size(59, 19)
        Me.chkEnable.TabIndex = 2
        Me.chkEnable.Text = "Enable"
        Me.chkEnable.UseVisualStyleBackColor = True
        '
        'lblSwiftTemplateName
        '
        Me.lblSwiftTemplateName.AutoSize = True
        Me.lblSwiftTemplateName.Location = New System.Drawing.Point(6, 49)
        Me.lblSwiftTemplateName.Name = "lblSwiftTemplateName"
        Me.lblSwiftTemplateName.Size = New System.Drawing.Size(114, 15)
        Me.lblSwiftTemplateName.TabIndex = 1
        Me.lblSwiftTemplateName.Text = "Swift Template Name :"
        '
        'txtSwiftTemplateName
        '
        Me.txtSwiftTemplateName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "SwiftTemplateName", True))
        Me.txtSwiftTemplateName.Location = New System.Drawing.Point(170, 45)
        Me.txtSwiftTemplateName.MaxLength = 100
        Me.txtSwiftTemplateName.Name = "txtSwiftTemplateName"
        Me.txtSwiftTemplateName.Size = New System.Drawing.Size(646, 22)
        Me.txtSwiftTemplateName.TabIndex = 1
        '
        'lblOutputTemplate
        '
        Me.lblOutputTemplate.AutoSize = True
        Me.lblOutputTemplate.Location = New System.Drawing.Point(6, 20)
        Me.lblOutputTemplate.Name = "lblOutputTemplate"
        Me.lblOutputTemplate.Size = New System.Drawing.Size(122, 15)
        Me.lblOutputTemplate.TabIndex = 0
        Me.lblOutputTemplate.Text = "Master Template Name:"
        '
        'cboOutputTemplate
        '
        Me.cboOutputTemplate.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bindingSrcSwiftTemplateCollection, "OutputTemplateName", True))
        Me.cboOutputTemplate.DataSource = Me.bindingSrcOutputTemplate
        Me.cboOutputTemplate.DisplayMember = "TemplateName"
        Me.cboOutputTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOutputTemplate.FormattingEnabled = True
        Me.cboOutputTemplate.Location = New System.Drawing.Point(170, 16)
        Me.cboOutputTemplate.Name = "cboOutputTemplate"
        Me.cboOutputTemplate.Size = New System.Drawing.Size(646, 23)
        Me.cboOutputTemplate.TabIndex = 0
        Me.cboOutputTemplate.ValueMember = "TemplateName"
        '
        'pnlBody
        '
        Me.pnlBody.Controls.Add(Me.tabDetail)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 88)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(915, 539)
        Me.pnlBody.TabIndex = 7
        '
        'tabDetail
        '
        Me.tabDetail.Controls.Add(Me.tbPgRetrieve)
        Me.tabDetail.Controls.Add(Me.tbPgMap)
        Me.tabDetail.Controls.Add(Me.tbPgTranslate)
        Me.tabDetail.Controls.Add(Me.tbPgLookup)
        Me.tabDetail.Controls.Add(Me.tbPgCalculatedValues)
        Me.tabDetail.Controls.Add(Me.tbPgStringManipulation)
        Me.tabDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabDetail.Location = New System.Drawing.Point(0, 0)
        Me.tabDetail.Name = "tabDetail"
        Me.tabDetail.SelectedIndex = 0
        Me.tabDetail.Size = New System.Drawing.Size(915, 539)
        Me.tabDetail.TabIndex = 2
        '
        'tbPgRetrieve
        '
        Me.tbPgRetrieve.Controls.Add(Me.btnSeparate)
        Me.tbPgRetrieve.Controls.Add(Me.gbxDetailAdviceSettings)
        Me.tbPgRetrieve.Controls.Add(Me.gbxFilterSetting)
        Me.tbPgRetrieve.Controls.Add(Me.lblSourceFileName)
        Me.tbPgRetrieve.Controls.Add(Me.gbxDuplicateTxnRef)
        Me.tbPgRetrieve.Controls.Add(Me.gbxRemittanceAmtSettings)
        Me.tbPgRetrieve.Controls.Add(Me.gbxNumberSettings)
        Me.tbPgRetrieve.Controls.Add(Me.btnBrowseSourceFile)
        Me.tbPgRetrieve.Controls.Add(Me.txtSourceFileName)
        Me.tbPgRetrieve.Controls.Add(Me.gbxDateSettings)
        Me.tbPgRetrieve.Controls.Add(Me.txtSampleRowData)
        Me.tbPgRetrieve.Location = New System.Drawing.Point(4, 24)
        Me.tbPgRetrieve.Name = "tbPgRetrieve"
        Me.tbPgRetrieve.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPgRetrieve.Size = New System.Drawing.Size(907, 511)
        Me.tbPgRetrieve.TabIndex = 0
        Me.tbPgRetrieve.Text = "       Setup       "
        Me.tbPgRetrieve.UseVisualStyleBackColor = True
        '
        'btnSeparate
        '
        Me.btnSeparate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSeparate.Location = New System.Drawing.Point(805, 236)
        Me.btnSeparate.Name = "btnSeparate"
        Me.btnSeparate.Size = New System.Drawing.Size(87, 31)
        Me.btnSeparate.TabIndex = 6
        Me.btnSeparate.Text = "Separa&te"
        Me.btnSeparate.UseVisualStyleBackColor = True
        '
        'gbxDetailAdviceSettings
        '
        Me.gbxDetailAdviceSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.txtNoOfCharsPerAdviseRecord)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.txtAdviseRecordDelimiter)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.Label23)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.Label22)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.rdoIRTMSDelimiter)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.rdoIRTMSCharPos)
        Me.gbxDetailAdviceSettings.Controls.Add(Me.rdoIRTMSEveryRow)
        Me.gbxDetailAdviceSettings.Location = New System.Drawing.Point(682, 375)
        Me.gbxDetailAdviceSettings.Name = "gbxDetailAdviceSettings"
        Me.gbxDetailAdviceSettings.Size = New System.Drawing.Size(210, 116)
        Me.gbxDetailAdviceSettings.TabIndex = 9
        Me.gbxDetailAdviceSettings.TabStop = False
        Me.gbxDetailAdviceSettings.Text = "For iRTMS Format"
        '
        'txtNoOfCharsPerAdviseRecord
        '
        Me.txtNoOfCharsPerAdviseRecord.DataBindings.Add(New System.Windows.Forms.Binding("Number", Me.bindingSrcSwiftTemplateCollection, "AdviceRecordCharPos", True))
        Me.txtNoOfCharsPerAdviseRecord.Location = New System.Drawing.Point(104, 62)
        Me.txtNoOfCharsPerAdviseRecord.MaxLength = 3
        Me.txtNoOfCharsPerAdviseRecord.Name = "txtNoOfCharsPerAdviseRecord"
        Me.txtNoOfCharsPerAdviseRecord.Size = New System.Drawing.Size(37, 25)
        Me.txtNoOfCharsPerAdviseRecord.TabIndex = 2
        '
        'txtAdviseRecordDelimiter
        '
        Me.txtAdviseRecordDelimiter.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "AdviceRecordDelimiter", True))
        Me.txtAdviseRecordDelimiter.Location = New System.Drawing.Point(157, 87)
        Me.txtAdviseRecordDelimiter.MaxLength = 1
        Me.txtAdviseRecordDelimiter.Name = "txtAdviseRecordDelimiter"
        Me.txtAdviseRecordDelimiter.Size = New System.Drawing.Size(20, 22)
        Me.txtAdviseRecordDelimiter.TabIndex = 4
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(147, 66)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(54, 15)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "character"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(107, 15)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "New Advice Record "
        '
        'rdoIRTMSDelimiter
        '
        Me.rdoIRTMSDelimiter.AutoSize = True
        Me.rdoIRTMSDelimiter.Location = New System.Drawing.Point(24, 89)
        Me.rdoIRTMSDelimiter.Name = "rdoIRTMSDelimiter"
        Me.rdoIRTMSDelimiter.Size = New System.Drawing.Size(131, 19)
        Me.rdoIRTMSDelimiter.TabIndex = 3
        Me.rdoIRTMSDelimiter.TabStop = True
        Me.rdoIRTMSDelimiter.Text = "separated by delimiter"
        Me.rdoIRTMSDelimiter.UseVisualStyleBackColor = True
        '
        'rdoIRTMSCharPos
        '
        Me.rdoIRTMSCharPos.AutoSize = True
        Me.rdoIRTMSCharPos.Location = New System.Drawing.Point(24, 64)
        Me.rdoIRTMSCharPos.Name = "rdoIRTMSCharPos"
        Me.rdoIRTMSCharPos.Size = New System.Drawing.Size(81, 19)
        Me.rdoIRTMSCharPos.TabIndex = 1
        Me.rdoIRTMSCharPos.TabStop = True
        Me.rdoIRTMSCharPos.Text = " after every"
        Me.rdoIRTMSCharPos.UseVisualStyleBackColor = True
        '
        'rdoIRTMSEveryRow
        '
        Me.rdoIRTMSEveryRow.AutoSize = True
        Me.rdoIRTMSEveryRow.Location = New System.Drawing.Point(24, 42)
        Me.rdoIRTMSEveryRow.Name = "rdoIRTMSEveryRow"
        Me.rdoIRTMSEveryRow.Size = New System.Drawing.Size(126, 19)
        Me.rdoIRTMSEveryRow.TabIndex = 0
        Me.rdoIRTMSEveryRow.TabStop = True
        Me.rdoIRTMSEveryRow.Text = "for every row of data"
        Me.rdoIRTMSEveryRow.UseVisualStyleBackColor = True
        '
        'gbxFilterSetting
        '
        Me.gbxFilterSetting.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxFilterSetting.Controls.Add(Me.btnFilterSettingsUp)
        Me.gbxFilterSetting.Controls.Add(Me.btnFilterSettingsDown)
        Me.gbxFilterSetting.Controls.Add(Me.rdoFilterSettingOR)
        Me.gbxFilterSetting.Controls.Add(Me.rdoFilterSettingAND)
        Me.gbxFilterSetting.Controls.Add(Me.dgvFilterSettings)
        Me.gbxFilterSetting.Location = New System.Drawing.Point(6, 276)
        Me.gbxFilterSetting.Name = "gbxFilterSetting"
        Me.gbxFilterSetting.Size = New System.Drawing.Size(662, 215)
        Me.gbxFilterSetting.TabIndex = 8
        Me.gbxFilterSetting.TabStop = False
        Me.gbxFilterSetting.Text = "Filter Setting"
        '
        'btnFilterSettingsUp
        '
        Me.btnFilterSettingsUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilterSettingsUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFilterSettingsUp.Location = New System.Drawing.Point(6, 21)
        Me.btnFilterSettingsUp.Name = "btnFilterSettingsUp"
        Me.btnFilterSettingsUp.Size = New System.Drawing.Size(27, 29)
        Me.btnFilterSettingsUp.TabIndex = 0
        Me.btnFilterSettingsUp.Text = "▲"
        Me.btnFilterSettingsUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFilterSettingsUp.UseVisualStyleBackColor = True
        '
        'btnFilterSettingsDown
        '
        Me.btnFilterSettingsDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilterSettingsDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFilterSettingsDown.Location = New System.Drawing.Point(36, 21)
        Me.btnFilterSettingsDown.Name = "btnFilterSettingsDown"
        Me.btnFilterSettingsDown.Size = New System.Drawing.Size(27, 29)
        Me.btnFilterSettingsDown.TabIndex = 1
        Me.btnFilterSettingsDown.Text = "▼"
        Me.btnFilterSettingsDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFilterSettingsDown.UseVisualStyleBackColor = True
        '
        'rdoFilterSettingOR
        '
        Me.rdoFilterSettingOR.AutoSize = True
        Me.rdoFilterSettingOR.Location = New System.Drawing.Point(184, 28)
        Me.rdoFilterSettingOR.Name = "rdoFilterSettingOR"
        Me.rdoFilterSettingOR.Size = New System.Drawing.Size(42, 19)
        Me.rdoFilterSettingOR.TabIndex = 3
        Me.rdoFilterSettingOR.TabStop = True
        Me.rdoFilterSettingOR.Text = "OR"
        Me.rdoFilterSettingOR.UseVisualStyleBackColor = True
        '
        'rdoFilterSettingAND
        '
        Me.rdoFilterSettingAND.AutoSize = True
        Me.rdoFilterSettingAND.Location = New System.Drawing.Point(124, 28)
        Me.rdoFilterSettingAND.Name = "rdoFilterSettingAND"
        Me.rdoFilterSettingAND.Size = New System.Drawing.Size(48, 19)
        Me.rdoFilterSettingAND.TabIndex = 2
        Me.rdoFilterSettingAND.TabStop = True
        Me.rdoFilterSettingAND.Text = "AND"
        Me.rdoFilterSettingAND.UseVisualStyleBackColor = True
        '
        'dgvFilterSettings
        '
        Me.dgvFilterSettings.AllowUserToDeleteRows = False
        Me.dgvFilterSettings.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvFilterSettings.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvFilterSettings.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFilterSettings.AutoGenerateColumns = False
        Me.dgvFilterSettings.BackgroundColor = System.Drawing.SystemColors.ControlDark
        Me.dgvFilterSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFilterSettings.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvBtnDelete, Me.dgvCmbFilterField, Me.dgvCmbFilterDataType, Me.dgvCmbFilterOperator, Me.dgvTxtFilterValue})
        Me.dgvFilterSettings.DataMember = "Filters"
        Me.dgvFilterSettings.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvFilterSettings.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvFilterSettings.Location = New System.Drawing.Point(8, 56)
        Me.dgvFilterSettings.MultiSelect = False
        Me.dgvFilterSettings.Name = "dgvFilterSettings"
        Me.dgvFilterSettings.RowHeadersVisible = False
        Me.dgvFilterSettings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvFilterSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFilterSettings.Size = New System.Drawing.Size(648, 151)
        Me.dgvFilterSettings.TabIndex = 4
        '
        'dgvBtnDelete
        '
        Me.dgvBtnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvBtnDelete.HeaderText = "X"
        Me.dgvBtnDelete.Name = "dgvBtnDelete"
        Me.dgvBtnDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBtnDelete.Text = "X"
        Me.dgvBtnDelete.ToolTipText = "Delete"
        Me.dgvBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvBtnDelete.Width = 20
        '
        'dgvCmbFilterField
        '
        Me.dgvCmbFilterField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCmbFilterField.DataPropertyName = "FilterField"
        Me.dgvCmbFilterField.DataSource = Me.bindingSrcRowFilterSourceField
        Me.dgvCmbFilterField.DisplayMember = "SourceFieldName"
        Me.dgvCmbFilterField.HeaderText = "Filter Field"
        Me.dgvCmbFilterField.Name = "dgvCmbFilterField"
        Me.dgvCmbFilterField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCmbFilterField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCmbFilterDataType
        '
        Me.dgvCmbFilterDataType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvCmbFilterDataType.DataPropertyName = "FilterType"
        Me.dgvCmbFilterDataType.HeaderText = "Data Type"
        Me.dgvCmbFilterDataType.Name = "dgvCmbFilterDataType"
        '
        'dgvCmbFilterOperator
        '
        Me.dgvCmbFilterOperator.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvCmbFilterOperator.DataPropertyName = "FilterOperator"
        Me.dgvCmbFilterOperator.HeaderText = "Filter Operator"
        Me.dgvCmbFilterOperator.Name = "dgvCmbFilterOperator"
        Me.dgvCmbFilterOperator.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCmbFilterOperator.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCmbFilterOperator.Width = 120
        '
        'dgvTxtFilterValue
        '
        Me.dgvTxtFilterValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTxtFilterValue.DataPropertyName = "FilterValue"
        Me.dgvTxtFilterValue.HeaderText = "Filter Value"
        Me.dgvTxtFilterValue.Name = "dgvTxtFilterValue"
        '
        'lblSourceFileName
        '
        Me.lblSourceFileName.AutoSize = True
        Me.lblSourceFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSourceFileName.Location = New System.Drawing.Point(6, 14)
        Me.lblSourceFileName.Name = "lblSourceFileName"
        Me.lblSourceFileName.Size = New System.Drawing.Size(97, 13)
        Me.lblSourceFileName.TabIndex = 0
        Me.lblSourceFileName.Text = "SWIFT File Name :"
        '
        'gbxDuplicateTxnRef
        '
        Me.gbxDuplicateTxnRef.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.cboDuplicateTxnRefField3)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.cboDuplicateTxnRefField2)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.cboDuplicateTxnRefField1)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.Label16)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.Label15)
        Me.gbxDuplicateTxnRef.Controls.Add(Me.Label14)
        Me.gbxDuplicateTxnRef.Location = New System.Drawing.Point(682, 258)
        Me.gbxDuplicateTxnRef.Name = "gbxDuplicateTxnRef"
        Me.gbxDuplicateTxnRef.Size = New System.Drawing.Size(210, 111)
        Me.gbxDuplicateTxnRef.TabIndex = 7
        Me.gbxDuplicateTxnRef.TabStop = False
        Me.gbxDuplicateTxnRef.Text = "Duplicate Transaction Reference"
        '
        'cboDuplicateTxnRefField3
        '
        Me.cboDuplicateTxnRefField3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "DuplicateTxnRef3", True))
        Me.cboDuplicateTxnRefField3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDuplicateTxnRefField3.FormattingEnabled = True
        Me.cboDuplicateTxnRefField3.Location = New System.Drawing.Point(83, 78)
        Me.cboDuplicateTxnRefField3.Name = "cboDuplicateTxnRefField3"
        Me.cboDuplicateTxnRefField3.Size = New System.Drawing.Size(118, 23)
        Me.cboDuplicateTxnRefField3.TabIndex = 2
        '
        'cboDuplicateTxnRefField2
        '
        Me.cboDuplicateTxnRefField2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "DuplicateTxnRef2", True))
        Me.cboDuplicateTxnRefField2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDuplicateTxnRefField2.FormattingEnabled = True
        Me.cboDuplicateTxnRefField2.Location = New System.Drawing.Point(83, 49)
        Me.cboDuplicateTxnRefField2.Name = "cboDuplicateTxnRefField2"
        Me.cboDuplicateTxnRefField2.Size = New System.Drawing.Size(118, 23)
        Me.cboDuplicateTxnRefField2.TabIndex = 1
        '
        'cboDuplicateTxnRefField1
        '
        Me.cboDuplicateTxnRefField1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "DuplicateTxnRef1", True))
        Me.cboDuplicateTxnRefField1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDuplicateTxnRefField1.FormattingEnabled = True
        Me.cboDuplicateTxnRefField1.Location = New System.Drawing.Point(83, 20)
        Me.cboDuplicateTxnRefField1.Name = "cboDuplicateTxnRefField1"
        Me.cboDuplicateTxnRefField1.Size = New System.Drawing.Size(118, 23)
        Me.cboDuplicateTxnRefField1.TabIndex = 0
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(59, 15)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Ref. Field2"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 84)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(59, 15)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Ref. Field3"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 26)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Ref. Field1"
        '
        'gbxRemittanceAmtSettings
        '
        Me.gbxRemittanceAmtSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxRemittanceAmtSettings.Controls.Add(Me.rdoCents)
        Me.gbxRemittanceAmtSettings.Controls.Add(Me.rdoDollars)
        Me.gbxRemittanceAmtSettings.Location = New System.Drawing.Point(682, 170)
        Me.gbxRemittanceAmtSettings.Name = "gbxRemittanceAmtSettings"
        Me.gbxRemittanceAmtSettings.Size = New System.Drawing.Size(210, 47)
        Me.gbxRemittanceAmtSettings.TabIndex = 5
        Me.gbxRemittanceAmtSettings.TabStop = False
        Me.gbxRemittanceAmtSettings.Text = "Remittance Amount Settings"
        '
        'rdoCents
        '
        Me.rdoCents.AutoSize = True
        Me.rdoCents.Location = New System.Drawing.Point(139, 19)
        Me.rdoCents.Name = "rdoCents"
        Me.rdoCents.Size = New System.Drawing.Size(54, 19)
        Me.rdoCents.TabIndex = 1
        Me.rdoCents.TabStop = True
        Me.rdoCents.Text = "Cents"
        Me.rdoCents.UseVisualStyleBackColor = True
        '
        'rdoDollars
        '
        Me.rdoDollars.AutoSize = True
        Me.rdoDollars.Location = New System.Drawing.Point(54, 19)
        Me.rdoDollars.Name = "rdoDollars"
        Me.rdoDollars.Size = New System.Drawing.Size(59, 19)
        Me.rdoDollars.TabIndex = 0
        Me.rdoDollars.TabStop = True
        Me.rdoDollars.Text = "Dollars"
        Me.rdoDollars.UseVisualStyleBackColor = True
        '
        'gbxNumberSettings
        '
        Me.gbxNumberSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxNumberSettings.Controls.Add(Me.cboDecimalSep)
        Me.gbxNumberSettings.Controls.Add(Me.Label12)
        Me.gbxNumberSettings.Location = New System.Drawing.Point(682, 4)
        Me.gbxNumberSettings.Name = "gbxNumberSettings"
        Me.gbxNumberSettings.Size = New System.Drawing.Size(210, 55)
        Me.gbxNumberSettings.TabIndex = 2
        Me.gbxNumberSettings.TabStop = False
        Me.gbxNumberSettings.Text = "Number Settings"
        '
        'cboDecimalSep
        '
        Me.cboDecimalSep.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "DecimalSeparator", True))
        Me.cboDecimalSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDecimalSep.FormattingEnabled = True
        Me.cboDecimalSep.Location = New System.Drawing.Point(104, 20)
        Me.cboDecimalSep.Name = "cboDecimalSep"
        Me.cboDecimalSep.Size = New System.Drawing.Size(97, 23)
        Me.cboDecimalSep.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(2, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(109, 18)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Decimal Separator :"
        '
        'btnBrowseSourceFile
        '
        Me.btnBrowseSourceFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowseSourceFile.Location = New System.Drawing.Point(583, 5)
        Me.btnBrowseSourceFile.Name = "btnBrowseSourceFile"
        Me.btnBrowseSourceFile.Size = New System.Drawing.Size(87, 31)
        Me.btnBrowseSourceFile.TabIndex = 1
        Me.btnBrowseSourceFile.Text = "&Browse"
        Me.btnBrowseSourceFile.UseVisualStyleBackColor = True
        '
        'txtSourceFileName
        '
        Me.txtSourceFileName.AllowDrop = True
        Me.txtSourceFileName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSourceFileName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "SourceFileName", True))
        Me.txtSourceFileName.Location = New System.Drawing.Point(108, 10)
        Me.txtSourceFileName.Name = "txtSourceFileName"
        Me.txtSourceFileName.ReadOnly = True
        Me.txtSourceFileName.Size = New System.Drawing.Size(469, 22)
        Me.txtSourceFileName.TabIndex = 0
        '
        'gbxDateSettings
        '
        Me.gbxDateSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDateSettings.Controls.Add(Me.cboDateType)
        Me.gbxDateSettings.Controls.Add(Me.cboDateSep)
        Me.gbxDateSettings.Controls.Add(Me.Label10)
        Me.gbxDateSettings.Controls.Add(Me.ChkZerosInDate)
        Me.gbxDateSettings.Controls.Add(Me.Label11)
        Me.gbxDateSettings.Location = New System.Drawing.Point(682, 63)
        Me.gbxDateSettings.Name = "gbxDateSettings"
        Me.gbxDateSettings.Size = New System.Drawing.Size(210, 103)
        Me.gbxDateSettings.TabIndex = 4
        Me.gbxDateSettings.TabStop = False
        Me.gbxDateSettings.Text = "Date Settings"
        '
        'cboDateType
        '
        Me.cboDateType.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "DateType", True))
        Me.cboDateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDateType.FormattingEnabled = True
        Me.cboDateType.Location = New System.Drawing.Point(96, 17)
        Me.cboDateType.Name = "cboDateType"
        Me.cboDateType.Size = New System.Drawing.Size(105, 23)
        Me.cboDateType.TabIndex = 0
        '
        'cboDateSep
        '
        Me.cboDateSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDateSep.Enabled = False
        Me.cboDateSep.FormattingEnabled = True
        Me.cboDateSep.Items.AddRange(New Object() {"No Space"})
        Me.cboDateSep.Location = New System.Drawing.Point(96, 48)
        Me.cboDateSep.Name = "cboDateSep"
        Me.cboDateSep.Size = New System.Drawing.Size(105, 23)
        Me.cboDateSep.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(5, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Date Separator :"
        '
        'ChkZerosInDate
        '
        Me.ChkZerosInDate.AutoSize = True
        Me.ChkZerosInDate.Enabled = False
        Me.ChkZerosInDate.Location = New System.Drawing.Point(10, 77)
        Me.ChkZerosInDate.Name = "ChkZerosInDate"
        Me.ChkZerosInDate.Size = New System.Drawing.Size(171, 19)
        Me.ChkZerosInDate.TabIndex = 2
        Me.ChkZerosInDate.Text = "Zeros in Date(Eg:-1900/0101)"
        Me.ChkZerosInDate.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 21)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 15)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Date Type :"
        '
        'txtSampleRowData
        '
        Me.txtSampleRowData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSampleRowData.BackColor = System.Drawing.Color.White
        Me.txtSampleRowData.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcSwiftTemplateCollection, "SampleRow", True))
        Me.txtSampleRowData.Location = New System.Drawing.Point(6, 44)
        Me.txtSampleRowData.Multiline = True
        Me.txtSampleRowData.Name = "txtSampleRowData"
        Me.txtSampleRowData.ReadOnly = True
        Me.txtSampleRowData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSampleRowData.Size = New System.Drawing.Size(662, 226)
        Me.txtSampleRowData.TabIndex = 3
        '
        'tbPgMap
        '
        Me.tbPgMap.Controls.Add(Me.btnClearAll)
        Me.tbPgMap.Controls.Add(Me.btnClear)
        Me.tbPgMap.Controls.Add(Me.dgvBank)
        Me.tbPgMap.Controls.Add(Me.dgvSource)
        Me.tbPgMap.Location = New System.Drawing.Point(4, 24)
        Me.tbPgMap.Name = "tbPgMap"
        Me.tbPgMap.Size = New System.Drawing.Size(907, 511)
        Me.tbPgMap.TabIndex = 2
        Me.tbPgMap.Text = "        Map        "
        Me.tbPgMap.UseVisualStyleBackColor = True
        '
        'btnClearAll
        '
        Me.btnClearAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClearAll.Location = New System.Drawing.Point(110, 477)
        Me.btnClearAll.Name = "btnClearAll"
        Me.btnClearAll.Size = New System.Drawing.Size(87, 31)
        Me.btnClearAll.TabIndex = 3
        Me.btnClearAll.Text = "Clear &All"
        Me.btnClearAll.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClear.Location = New System.Drawing.Point(17, 477)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(87, 31)
        Me.btnClear.TabIndex = 2
        Me.btnClear.Text = "C&lear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'dgvBank
        '
        Me.dgvBank.AllowDrop = True
        Me.dgvBank.AllowUserToAddRows = False
        Me.dgvBank.AllowUserToDeleteRows = False
        Me.dgvBank.AllowUserToResizeRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvBank.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvBank.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBank.AutoGenerateColumns = False
        Me.dgvBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvBank.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BankFieldExtDataGridViewTextBoxColumn, Me.MappedFieldsDataGridViewTextBoxColumn, Me.MappedValuesDataGridViewTextBoxColumn, Me.BankSampleValueDataGridViewTextBoxColumn})
        Me.dgvBank.DataMember = "MapBankFields"
        Me.dgvBank.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvBank.Location = New System.Drawing.Point(370, 6)
        Me.dgvBank.MultiSelect = False
        Me.dgvBank.Name = "dgvBank"
        Me.dgvBank.ReadOnly = True
        Me.dgvBank.RowHeadersVisible = False
        Me.dgvBank.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvBank.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBank.Size = New System.Drawing.Size(534, 465)
        Me.dgvBank.TabIndex = 1
        '
        'BankFieldExtDataGridViewTextBoxColumn
        '
        Me.BankFieldExtDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.BankFieldExtDataGridViewTextBoxColumn.DataPropertyName = "BankFieldExt"
        Me.BankFieldExtDataGridViewTextBoxColumn.HeaderText = "Bank Field"
        Me.BankFieldExtDataGridViewTextBoxColumn.Name = "BankFieldExtDataGridViewTextBoxColumn"
        Me.BankFieldExtDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MappedFieldsDataGridViewTextBoxColumn
        '
        Me.MappedFieldsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MappedFieldsDataGridViewTextBoxColumn.DataPropertyName = "MappedFields"
        Me.MappedFieldsDataGridViewTextBoxColumn.HeaderText = "Mapping Field"
        Me.MappedFieldsDataGridViewTextBoxColumn.Name = "MappedFieldsDataGridViewTextBoxColumn"
        Me.MappedFieldsDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MappedValuesDataGridViewTextBoxColumn
        '
        Me.MappedValuesDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MappedValuesDataGridViewTextBoxColumn.DataPropertyName = "MappedValues"
        Me.MappedValuesDataGridViewTextBoxColumn.HeaderText = "Bank Value"
        Me.MappedValuesDataGridViewTextBoxColumn.Name = "MappedValuesDataGridViewTextBoxColumn"
        Me.MappedValuesDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BankSampleValueDataGridViewTextBoxColumn
        '
        Me.BankSampleValueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.BankSampleValueDataGridViewTextBoxColumn.DataPropertyName = "BankSampleValue"
        Me.BankSampleValueDataGridViewTextBoxColumn.HeaderText = "Bank Sample Value"
        Me.BankSampleValueDataGridViewTextBoxColumn.Name = "BankSampleValueDataGridViewTextBoxColumn"
        Me.BankSampleValueDataGridViewTextBoxColumn.ReadOnly = True
        '
        'dgvSource
        '
        Me.dgvSource.AllowDrop = True
        Me.dgvSource.AllowUserToAddRows = False
        Me.dgvSource.AllowUserToDeleteRows = False
        Me.dgvSource.AllowUserToResizeRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvSource.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvSource.AutoGenerateColumns = False
        Me.dgvSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvSource.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SequenceDataGridViewTextBoxColumn1, Me.SourceFieldNameDataGridViewTextBoxColumn, Me.SourceFieldValueDataGridViewTextBoxColumn})
        Me.dgvSource.DataMember = "MapSourceFields"
        Me.dgvSource.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvSource.Location = New System.Drawing.Point(3, 6)
        Me.dgvSource.MultiSelect = False
        Me.dgvSource.Name = "dgvSource"
        Me.dgvSource.ReadOnly = True
        Me.dgvSource.RowHeadersVisible = False
        Me.dgvSource.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvSource.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSource.Size = New System.Drawing.Size(365, 465)
        Me.dgvSource.TabIndex = 0
        '
        'SequenceDataGridViewTextBoxColumn1
        '
        Me.SequenceDataGridViewTextBoxColumn1.DataPropertyName = "Sequence"
        Me.SequenceDataGridViewTextBoxColumn1.HeaderText = "No."
        Me.SequenceDataGridViewTextBoxColumn1.Name = "SequenceDataGridViewTextBoxColumn1"
        Me.SequenceDataGridViewTextBoxColumn1.ReadOnly = True
        Me.SequenceDataGridViewTextBoxColumn1.Width = 50
        '
        'SourceFieldNameDataGridViewTextBoxColumn
        '
        Me.SourceFieldNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SourceFieldNameDataGridViewTextBoxColumn.DataPropertyName = "SourceFieldName"
        Me.SourceFieldNameDataGridViewTextBoxColumn.HeaderText = "Source Field Name"
        Me.SourceFieldNameDataGridViewTextBoxColumn.Name = "SourceFieldNameDataGridViewTextBoxColumn"
        Me.SourceFieldNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SourceFieldValueDataGridViewTextBoxColumn
        '
        Me.SourceFieldValueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SourceFieldValueDataGridViewTextBoxColumn.DataPropertyName = "SourceFieldValue"
        Me.SourceFieldValueDataGridViewTextBoxColumn.HeaderText = "Source Field Value"
        Me.SourceFieldValueDataGridViewTextBoxColumn.Name = "SourceFieldValueDataGridViewTextBoxColumn"
        Me.SourceFieldValueDataGridViewTextBoxColumn.ReadOnly = True
        '
        'tbPgTranslate
        '
        Me.tbPgTranslate.Controls.Add(Me.gbxEditableSetting)
        Me.tbPgTranslate.Controls.Add(Me.gbxTranslatorSetting)
        Me.tbPgTranslate.Location = New System.Drawing.Point(4, 24)
        Me.tbPgTranslate.Name = "tbPgTranslate"
        Me.tbPgTranslate.Size = New System.Drawing.Size(907, 511)
        Me.tbPgTranslate.TabIndex = 3
        Me.tbPgTranslate.Text = "    Translation    "
        Me.tbPgTranslate.UseVisualStyleBackColor = True
        '
        'gbxEditableSetting
        '
        Me.gbxEditableSetting.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxEditableSetting.BackColor = System.Drawing.Color.White
        Me.gbxEditableSetting.Controls.Add(Me.btnEditableSettingRowDown)
        Me.gbxEditableSetting.Controls.Add(Me.btnEditableSettingRowUp)
        Me.gbxEditableSetting.Controls.Add(Me.dgvEditable)
        Me.gbxEditableSetting.Location = New System.Drawing.Point(2, 283)
        Me.gbxEditableSetting.Name = "gbxEditableSetting"
        Me.gbxEditableSetting.Size = New System.Drawing.Size(899, 214)
        Me.gbxEditableSetting.TabIndex = 1
        Me.gbxEditableSetting.TabStop = False
        Me.gbxEditableSetting.Text = "Editable Settings - Select Bank Fields which would be editable in the Preview"
        '
        'btnEditableSettingRowDown
        '
        Me.btnEditableSettingRowDown.BackColor = System.Drawing.Color.Transparent
        Me.btnEditableSettingRowDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditableSettingRowDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditableSettingRowDown.Location = New System.Drawing.Point(42, 19)
        Me.btnEditableSettingRowDown.Name = "btnEditableSettingRowDown"
        Me.btnEditableSettingRowDown.Size = New System.Drawing.Size(27, 29)
        Me.btnEditableSettingRowDown.TabIndex = 1
        Me.btnEditableSettingRowDown.Text = "▼"
        Me.btnEditableSettingRowDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditableSettingRowDown.UseVisualStyleBackColor = False
        '
        'btnEditableSettingRowUp
        '
        Me.btnEditableSettingRowUp.BackColor = System.Drawing.Color.Transparent
        Me.btnEditableSettingRowUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditableSettingRowUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditableSettingRowUp.Location = New System.Drawing.Point(9, 19)
        Me.btnEditableSettingRowUp.Name = "btnEditableSettingRowUp"
        Me.btnEditableSettingRowUp.Size = New System.Drawing.Size(27, 29)
        Me.btnEditableSettingRowUp.TabIndex = 0
        Me.btnEditableSettingRowUp.Text = "▲"
        Me.btnEditableSettingRowUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditableSettingRowUp.UseVisualStyleBackColor = False
        '
        'dgvEditable
        '
        Me.dgvEditable.AllowUserToDeleteRows = False
        Me.dgvEditable.AllowUserToResizeRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvEditable.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvEditable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvEditable.AutoGenerateColumns = False
        Me.dgvEditable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEditable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvEditableBtnDelete, Me.dgvTranslatorEditableSettingCellBankField})
        Me.dgvEditable.DataMember = "EditableSettings"
        Me.dgvEditable.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvEditable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvEditable.Location = New System.Drawing.Point(6, 56)
        Me.dgvEditable.MultiSelect = False
        Me.dgvEditable.Name = "dgvEditable"
        Me.dgvEditable.RowHeadersVisible = False
        Me.dgvEditable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEditable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEditable.Size = New System.Drawing.Size(887, 152)
        Me.dgvEditable.TabIndex = 2
        '
        'dgvEditableBtnDelete
        '
        Me.dgvEditableBtnDelete.HeaderText = "X"
        Me.dgvEditableBtnDelete.Name = "dgvEditableBtnDelete"
        Me.dgvEditableBtnDelete.Text = "X"
        Me.dgvEditableBtnDelete.ToolTipText = "Delete"
        Me.dgvEditableBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvEditableBtnDelete.Width = 20
        '
        'dgvTranslatorEditableSettingCellBankField
        '
        Me.dgvTranslatorEditableSettingCellBankField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorEditableSettingCellBankField.DataPropertyName = "BankFieldName"
        Me.dgvTranslatorEditableSettingCellBankField.DataSource = Me.bindingSrcEditableSettingsBankField
        Me.dgvTranslatorEditableSettingCellBankField.HeaderText = "Bank Field"
        Me.dgvTranslatorEditableSettingCellBankField.Name = "dgvTranslatorEditableSettingCellBankField"
        Me.dgvTranslatorEditableSettingCellBankField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTranslatorEditableSettingCellBankField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'gbxTranslatorSetting
        '
        Me.gbxTranslatorSetting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxTranslatorSetting.Controls.Add(Me.btnTranslatorSettingRowUp)
        Me.gbxTranslatorSetting.Controls.Add(Me.btnTranslatorSettingRowDown)
        Me.gbxTranslatorSetting.Controls.Add(Me.dgvTranslator)
        Me.gbxTranslatorSetting.Location = New System.Drawing.Point(2, 0)
        Me.gbxTranslatorSetting.Name = "gbxTranslatorSetting"
        Me.gbxTranslatorSetting.Size = New System.Drawing.Size(899, 269)
        Me.gbxTranslatorSetting.TabIndex = 0
        Me.gbxTranslatorSetting.TabStop = False
        Me.gbxTranslatorSetting.Text = "Translator Settings"
        '
        'btnTranslatorSettingRowUp
        '
        Me.btnTranslatorSettingRowUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTranslatorSettingRowUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTranslatorSettingRowUp.Location = New System.Drawing.Point(10, 26)
        Me.btnTranslatorSettingRowUp.Name = "btnTranslatorSettingRowUp"
        Me.btnTranslatorSettingRowUp.Size = New System.Drawing.Size(27, 29)
        Me.btnTranslatorSettingRowUp.TabIndex = 0
        Me.btnTranslatorSettingRowUp.Text = "▲"
        Me.btnTranslatorSettingRowUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTranslatorSettingRowUp.UseVisualStyleBackColor = True
        '
        'btnTranslatorSettingRowDown
        '
        Me.btnTranslatorSettingRowDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTranslatorSettingRowDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTranslatorSettingRowDown.Location = New System.Drawing.Point(40, 26)
        Me.btnTranslatorSettingRowDown.Name = "btnTranslatorSettingRowDown"
        Me.btnTranslatorSettingRowDown.Size = New System.Drawing.Size(27, 29)
        Me.btnTranslatorSettingRowDown.TabIndex = 1
        Me.btnTranslatorSettingRowDown.Text = "▼"
        Me.btnTranslatorSettingRowDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTranslatorSettingRowDown.UseVisualStyleBackColor = True
        '
        'dgvTranslator
        '
        Me.dgvTranslator.AllowUserToDeleteRows = False
        Me.dgvTranslator.AllowUserToResizeRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvTranslator.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvTranslator.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTranslator.AutoGenerateColumns = False
        Me.dgvTranslator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTranslator.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvTranslatorBtnDelete, Me.dgvTranslatorSettingsCellCmbBankField, Me.dgvTranslatorSettingsCellTxtSourceValue, Me.dgvTranslatorCellCboDefaultValues, Me.dgvTranslatorCellChkBankValueEmpty})
        Me.dgvTranslator.DataMember = "TranslatorSettings"
        Me.dgvTranslator.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvTranslator.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvTranslator.Location = New System.Drawing.Point(6, 61)
        Me.dgvTranslator.MultiSelect = False
        Me.dgvTranslator.Name = "dgvTranslator"
        Me.dgvTranslator.RowHeadersVisible = False
        Me.dgvTranslator.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTranslator.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvTranslator.Size = New System.Drawing.Size(887, 199)
        Me.dgvTranslator.TabIndex = 2
        '
        'dgvTranslatorBtnDelete
        '
        Me.dgvTranslatorBtnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvTranslatorBtnDelete.HeaderText = "X"
        Me.dgvTranslatorBtnDelete.Name = "dgvTranslatorBtnDelete"
        Me.dgvTranslatorBtnDelete.Text = "X"
        Me.dgvTranslatorBtnDelete.ToolTipText = "Delete"
        Me.dgvTranslatorBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvTranslatorBtnDelete.Width = 20
        '
        'dgvTranslatorSettingsCellCmbBankField
        '
        Me.dgvTranslatorSettingsCellCmbBankField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorSettingsCellCmbBankField.DataPropertyName = "BankFieldName"
        Me.dgvTranslatorSettingsCellCmbBankField.DataSource = Me.bindingSrcTanslateBankField
        Me.dgvTranslatorSettingsCellCmbBankField.DisplayMember = "BankField"
        Me.dgvTranslatorSettingsCellCmbBankField.HeaderText = "Bank Field"
        Me.dgvTranslatorSettingsCellCmbBankField.Name = "dgvTranslatorSettingsCellCmbBankField"
        Me.dgvTranslatorSettingsCellCmbBankField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTranslatorSettingsCellCmbBankField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvTranslatorSettingsCellTxtSourceValue
        '
        Me.dgvTranslatorSettingsCellTxtSourceValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorSettingsCellTxtSourceValue.DataPropertyName = "CustomerValue"
        Me.dgvTranslatorSettingsCellTxtSourceValue.HeaderText = "Source Value"
        Me.dgvTranslatorSettingsCellTxtSourceValue.Name = "dgvTranslatorSettingsCellTxtSourceValue"
        '
        'dgvTranslatorCellCboDefaultValues
        '
        Me.dgvTranslatorCellCboDefaultValues.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorCellCboDefaultValues.DataPropertyName = "BankValue"
        Me.dgvTranslatorCellCboDefaultValues.HeaderText = "Bank Value"
        Me.dgvTranslatorCellCboDefaultValues.Name = "dgvTranslatorCellCboDefaultValues"
        Me.dgvTranslatorCellCboDefaultValues.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTranslatorCellCboDefaultValues.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvTranslatorCellChkBankValueEmpty
        '
        Me.dgvTranslatorCellChkBankValueEmpty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvTranslatorCellChkBankValueEmpty.DataPropertyName = "BankValueEmpty"
        Me.dgvTranslatorCellChkBankValueEmpty.HeaderText = "Bank Value Empty"
        Me.dgvTranslatorCellChkBankValueEmpty.Name = "dgvTranslatorCellChkBankValueEmpty"
        '
        'tbPgLookup
        '
        Me.tbPgLookup.Controls.Add(Me.gbxLookup)
        Me.tbPgLookup.Location = New System.Drawing.Point(4, 24)
        Me.tbPgLookup.Name = "tbPgLookup"
        Me.tbPgLookup.Size = New System.Drawing.Size(907, 511)
        Me.tbPgLookup.TabIndex = 4
        Me.tbPgLookup.Text = "      Lookup       "
        Me.tbPgLookup.UseVisualStyleBackColor = True
        '
        'gbxLookup
        '
        Me.gbxLookup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxLookup.Controls.Add(Me.dgvLookup)
        Me.gbxLookup.Controls.Add(Me.btnLookupRowDown)
        Me.gbxLookup.Controls.Add(Me.btnLookupRowUp)
        Me.gbxLookup.Location = New System.Drawing.Point(0, 0)
        Me.gbxLookup.Name = "gbxLookup"
        Me.gbxLookup.Size = New System.Drawing.Size(904, 500)
        Me.gbxLookup.TabIndex = 0
        Me.gbxLookup.TabStop = False
        Me.gbxLookup.Text = "Lookup"
        '
        'dgvLookup
        '
        Me.dgvLookup.AllowUserToDeleteRows = False
        Me.dgvLookup.AllowUserToResizeRows = False
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvLookup.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvLookup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLookup.AutoGenerateColumns = False
        Me.dgvLookup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLookup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvLookupBtnDelete, Me.dgvLookupCellCmbBankField, Me.dgvLookupCellCmbSourceField, Me.dgvLookupCellTable, Me.dgvLookupCellCboLookupKey, Me.dgvLookupCellCboLookupValue})
        Me.dgvLookup.DataMember = "LookupSettings"
        Me.dgvLookup.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvLookup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvLookup.Location = New System.Drawing.Point(6, 62)
        Me.dgvLookup.MultiSelect = False
        Me.dgvLookup.Name = "dgvLookup"
        Me.dgvLookup.RowHeadersVisible = False
        Me.dgvLookup.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLookup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvLookup.Size = New System.Drawing.Size(892, 432)
        Me.dgvLookup.TabIndex = 2
        '
        'dgvLookupBtnDelete
        '
        Me.dgvLookupBtnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvLookupBtnDelete.HeaderText = "X"
        Me.dgvLookupBtnDelete.Name = "dgvLookupBtnDelete"
        Me.dgvLookupBtnDelete.Text = "X"
        Me.dgvLookupBtnDelete.ToolTipText = "Delete"
        Me.dgvLookupBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvLookupBtnDelete.Width = 20
        '
        'dgvLookupCellCmbBankField
        '
        Me.dgvLookupCellCmbBankField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupCellCmbBankField.DataPropertyName = "BankFieldName"
        Me.dgvLookupCellCmbBankField.DataSource = Me.bindingSrcLookupBankField
        Me.dgvLookupCellCmbBankField.DisplayMember = "BankField"
        Me.dgvLookupCellCmbBankField.HeaderText = "Bank Field"
        Me.dgvLookupCellCmbBankField.Name = "dgvLookupCellCmbBankField"
        Me.dgvLookupCellCmbBankField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupCellCmbBankField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupCellCmbSourceField
        '
        Me.dgvLookupCellCmbSourceField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupCellCmbSourceField.DataPropertyName = "SourceFieldName"
        Me.dgvLookupCellCmbSourceField.DataSource = Me.bindingSrcLookupSourceField
        Me.dgvLookupCellCmbSourceField.DisplayMember = "SourceFieldName"
        Me.dgvLookupCellCmbSourceField.HeaderText = "Source Field"
        Me.dgvLookupCellCmbSourceField.Name = "dgvLookupCellCmbSourceField"
        '
        'dgvLookupCellTable
        '
        Me.dgvLookupCellTable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvLookupCellTable.DataPropertyName = "TableName"
        Me.dgvLookupCellTable.HeaderText = "Table"
        Me.dgvLookupCellTable.Name = "dgvLookupCellTable"
        Me.dgvLookupCellTable.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupCellTable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupCellCboLookupKey
        '
        Me.dgvLookupCellCboLookupKey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupCellCboLookupKey.DataPropertyName = "LookupKey"
        Me.dgvLookupCellCboLookupKey.HeaderText = "Lookup Key"
        Me.dgvLookupCellCboLookupKey.Name = "dgvLookupCellCboLookupKey"
        Me.dgvLookupCellCboLookupKey.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupCellCboLookupKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupCellCboLookupValue
        '
        Me.dgvLookupCellCboLookupValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupCellCboLookupValue.DataPropertyName = "LookupValue"
        Me.dgvLookupCellCboLookupValue.HeaderText = "Lookup Value"
        Me.dgvLookupCellCboLookupValue.Name = "dgvLookupCellCboLookupValue"
        Me.dgvLookupCellCboLookupValue.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupCellCboLookupValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'btnLookupRowDown
        '
        Me.btnLookupRowDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLookupRowDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLookupRowDown.Location = New System.Drawing.Point(40, 26)
        Me.btnLookupRowDown.Name = "btnLookupRowDown"
        Me.btnLookupRowDown.Size = New System.Drawing.Size(27, 29)
        Me.btnLookupRowDown.TabIndex = 1
        Me.btnLookupRowDown.Text = "▼"
        Me.btnLookupRowDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLookupRowDown.UseVisualStyleBackColor = True
        '
        'btnLookupRowUp
        '
        Me.btnLookupRowUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLookupRowUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLookupRowUp.Location = New System.Drawing.Point(10, 26)
        Me.btnLookupRowUp.Name = "btnLookupRowUp"
        Me.btnLookupRowUp.Size = New System.Drawing.Size(27, 29)
        Me.btnLookupRowUp.TabIndex = 0
        Me.btnLookupRowUp.Text = "▲"
        Me.btnLookupRowUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLookupRowUp.UseVisualStyleBackColor = True
        '
        'tbPgCalculatedValues
        '
        Me.tbPgCalculatedValues.Controls.Add(Me.gbxCalculatedValues)
        Me.tbPgCalculatedValues.Location = New System.Drawing.Point(4, 24)
        Me.tbPgCalculatedValues.Name = "tbPgCalculatedValues"
        Me.tbPgCalculatedValues.Size = New System.Drawing.Size(907, 511)
        Me.tbPgCalculatedValues.TabIndex = 5
        Me.tbPgCalculatedValues.Text = "    Calculation    "
        Me.tbPgCalculatedValues.UseVisualStyleBackColor = True
        '
        'gbxCalculatedValues
        '
        Me.gbxCalculatedValues.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxCalculatedValues.Controls.Add(Me.btnCalculatedFieldsRowDown)
        Me.gbxCalculatedValues.Controls.Add(Me.btnCalculatedFieldsRowUp)
        Me.gbxCalculatedValues.Controls.Add(Me.dgvCalculated)
        Me.gbxCalculatedValues.Location = New System.Drawing.Point(0, 0)
        Me.gbxCalculatedValues.Name = "gbxCalculatedValues"
        Me.gbxCalculatedValues.Size = New System.Drawing.Size(904, 496)
        Me.gbxCalculatedValues.TabIndex = 0
        Me.gbxCalculatedValues.TabStop = False
        Me.gbxCalculatedValues.Text = "Calculation Fields"
        '
        'btnCalculatedFieldsRowDown
        '
        Me.btnCalculatedFieldsRowDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalculatedFieldsRowDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCalculatedFieldsRowDown.Location = New System.Drawing.Point(40, 26)
        Me.btnCalculatedFieldsRowDown.Name = "btnCalculatedFieldsRowDown"
        Me.btnCalculatedFieldsRowDown.Size = New System.Drawing.Size(27, 29)
        Me.btnCalculatedFieldsRowDown.TabIndex = 1
        Me.btnCalculatedFieldsRowDown.Text = "▼"
        Me.btnCalculatedFieldsRowDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCalculatedFieldsRowDown.UseVisualStyleBackColor = True
        '
        'btnCalculatedFieldsRowUp
        '
        Me.btnCalculatedFieldsRowUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalculatedFieldsRowUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCalculatedFieldsRowUp.Location = New System.Drawing.Point(10, 26)
        Me.btnCalculatedFieldsRowUp.Name = "btnCalculatedFieldsRowUp"
        Me.btnCalculatedFieldsRowUp.Size = New System.Drawing.Size(27, 29)
        Me.btnCalculatedFieldsRowUp.TabIndex = 0
        Me.btnCalculatedFieldsRowUp.Text = "▲"
        Me.btnCalculatedFieldsRowUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCalculatedFieldsRowUp.UseVisualStyleBackColor = True
        '
        'dgvCalculated
        '
        Me.dgvCalculated.AllowUserToDeleteRows = False
        Me.dgvCalculated.AllowUserToResizeRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvCalculated.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvCalculated.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCalculated.AutoGenerateColumns = False
        Me.dgvCalculated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvCalculated.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvCalculatedBtnDelete, Me.dgvCalculatedFieldsCellCmbBankField, Me.dgvCalculatedFieldsOperand1, Me.dgvCalculatedFieldsCellOperator1, Me.dgvCalculatedFieldsOperand2, Me.dgvCalculatedFieldsCellOperator2, Me.dgvCalculatedFieldsOperand3})
        Me.dgvCalculated.DataMember = "CalculatedFields"
        Me.dgvCalculated.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvCalculated.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvCalculated.Location = New System.Drawing.Point(6, 62)
        Me.dgvCalculated.MultiSelect = False
        Me.dgvCalculated.Name = "dgvCalculated"
        Me.dgvCalculated.RowHeadersVisible = False
        Me.dgvCalculated.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvCalculated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCalculated.Size = New System.Drawing.Size(892, 426)
        Me.dgvCalculated.TabIndex = 2
        '
        'dgvCalculatedBtnDelete
        '
        Me.dgvCalculatedBtnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvCalculatedBtnDelete.HeaderText = "X"
        Me.dgvCalculatedBtnDelete.Name = "dgvCalculatedBtnDelete"
        Me.dgvCalculatedBtnDelete.Text = "X"
        Me.dgvCalculatedBtnDelete.ToolTipText = "Delete"
        Me.dgvCalculatedBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvCalculatedBtnDelete.Width = 20
        '
        'dgvCalculatedFieldsCellCmbBankField
        '
        Me.dgvCalculatedFieldsCellCmbBankField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalculatedFieldsCellCmbBankField.DataPropertyName = "BankFieldName"
        Me.dgvCalculatedFieldsCellCmbBankField.DataSource = Me.bindingSrcCalculatedFieldsBankField
        Me.dgvCalculatedFieldsCellCmbBankField.DisplayMember = "BankField"
        Me.dgvCalculatedFieldsCellCmbBankField.HeaderText = "Bank Field"
        Me.dgvCalculatedFieldsCellCmbBankField.Name = "dgvCalculatedFieldsCellCmbBankField"
        Me.dgvCalculatedFieldsCellCmbBankField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsCellCmbBankField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCalculatedFieldsOperand1
        '
        Me.dgvCalculatedFieldsOperand1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalculatedFieldsOperand1.DataPropertyName = "Operand1"
        Me.dgvCalculatedFieldsOperand1.DataSource = Me.bindingSrcOperand1SourceField
        Me.dgvCalculatedFieldsOperand1.DisplayMember = "SourceFieldName"
        Me.dgvCalculatedFieldsOperand1.HeaderText = "Operand1"
        Me.dgvCalculatedFieldsOperand1.Name = "dgvCalculatedFieldsOperand1"
        Me.dgvCalculatedFieldsOperand1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsOperand1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCalculatedFieldsCellOperator1
        '
        Me.dgvCalculatedFieldsCellOperator1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvCalculatedFieldsCellOperator1.DataPropertyName = "Operator1"
        Me.dgvCalculatedFieldsCellOperator1.HeaderText = "Operator1"
        Me.dgvCalculatedFieldsCellOperator1.Name = "dgvCalculatedFieldsCellOperator1"
        Me.dgvCalculatedFieldsCellOperator1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsCellOperator1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCalculatedFieldsOperand2
        '
        Me.dgvCalculatedFieldsOperand2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalculatedFieldsOperand2.DataPropertyName = "Operand2"
        Me.dgvCalculatedFieldsOperand2.DataSource = Me.bindingSrcOperand2SourceField
        Me.dgvCalculatedFieldsOperand2.DisplayMember = "SourceFieldName"
        Me.dgvCalculatedFieldsOperand2.HeaderText = "Operand2"
        Me.dgvCalculatedFieldsOperand2.Name = "dgvCalculatedFieldsOperand2"
        Me.dgvCalculatedFieldsOperand2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsOperand2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCalculatedFieldsCellOperator2
        '
        Me.dgvCalculatedFieldsCellOperator2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvCalculatedFieldsCellOperator2.DataPropertyName = "Operator2"
        Me.dgvCalculatedFieldsCellOperator2.HeaderText = "Operator2"
        Me.dgvCalculatedFieldsCellOperator2.Name = "dgvCalculatedFieldsCellOperator2"
        Me.dgvCalculatedFieldsCellOperator2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsCellOperator2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvCalculatedFieldsOperand3
        '
        Me.dgvCalculatedFieldsOperand3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalculatedFieldsOperand3.DataPropertyName = "Operand3"
        Me.dgvCalculatedFieldsOperand3.DataSource = Me.bindingSrcOperand3SourceField
        Me.dgvCalculatedFieldsOperand3.DisplayMember = "SourceFieldName"
        Me.dgvCalculatedFieldsOperand3.HeaderText = "Operand3"
        Me.dgvCalculatedFieldsOperand3.Name = "dgvCalculatedFieldsOperand3"
        Me.dgvCalculatedFieldsOperand3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalculatedFieldsOperand3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'tbPgStringManipulation
        '
        Me.tbPgStringManipulation.Controls.Add(Me.gbxStringManipulation)
        Me.tbPgStringManipulation.Location = New System.Drawing.Point(4, 24)
        Me.tbPgStringManipulation.Name = "tbPgStringManipulation"
        Me.tbPgStringManipulation.Size = New System.Drawing.Size(907, 511)
        Me.tbPgStringManipulation.TabIndex = 6
        Me.tbPgStringManipulation.Text = "String Manipulation"
        Me.tbPgStringManipulation.UseVisualStyleBackColor = True
        '
        'gbxStringManipulation
        '
        Me.gbxStringManipulation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxStringManipulation.Controls.Add(Me.btnStringManipulationRowDown)
        Me.gbxStringManipulation.Controls.Add(Me.btnStringManipulationRowUp)
        Me.gbxStringManipulation.Controls.Add(Me.dgvStringManipulation)
        Me.gbxStringManipulation.Location = New System.Drawing.Point(0, 0)
        Me.gbxStringManipulation.Name = "gbxStringManipulation"
        Me.gbxStringManipulation.Size = New System.Drawing.Size(904, 498)
        Me.gbxStringManipulation.TabIndex = 0
        Me.gbxStringManipulation.TabStop = False
        Me.gbxStringManipulation.Text = "String Manipulation"
        '
        'btnStringManipulationRowDown
        '
        Me.btnStringManipulationRowDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStringManipulationRowDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStringManipulationRowDown.Location = New System.Drawing.Point(40, 26)
        Me.btnStringManipulationRowDown.Name = "btnStringManipulationRowDown"
        Me.btnStringManipulationRowDown.Size = New System.Drawing.Size(27, 29)
        Me.btnStringManipulationRowDown.TabIndex = 1
        Me.btnStringManipulationRowDown.Text = "▼"
        Me.btnStringManipulationRowDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnStringManipulationRowDown.UseVisualStyleBackColor = True
        '
        'btnStringManipulationRowUp
        '
        Me.btnStringManipulationRowUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStringManipulationRowUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStringManipulationRowUp.Location = New System.Drawing.Point(10, 26)
        Me.btnStringManipulationRowUp.Name = "btnStringManipulationRowUp"
        Me.btnStringManipulationRowUp.Size = New System.Drawing.Size(27, 29)
        Me.btnStringManipulationRowUp.TabIndex = 0
        Me.btnStringManipulationRowUp.Text = "▲"
        Me.btnStringManipulationRowUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnStringManipulationRowUp.UseVisualStyleBackColor = True
        '
        'dgvStringManipulation
        '
        Me.dgvStringManipulation.AllowUserToDeleteRows = False
        Me.dgvStringManipulation.AllowUserToResizeRows = False
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvStringManipulation.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvStringManipulation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvStringManipulation.AutoGenerateColumns = False
        Me.dgvStringManipulation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvStringManipulation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvStringManipulationBtnDelete, Me.dgvStringManipulationCellCmbBankField, Me.dgvStringManipulationCellStringFunction, Me.StartingPositionDataGridViewTextBoxColumn, Me.NumberOfCharactersDataGridViewTextBoxColumn})
        Me.dgvStringManipulation.DataMember = "StringManipulations"
        Me.dgvStringManipulation.DataSource = Me.bindingSrcSwiftTemplateCollection
        Me.dgvStringManipulation.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvStringManipulation.Location = New System.Drawing.Point(6, 62)
        Me.dgvStringManipulation.MultiSelect = False
        Me.dgvStringManipulation.Name = "dgvStringManipulation"
        Me.dgvStringManipulation.RowHeadersVisible = False
        Me.dgvStringManipulation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvStringManipulation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvStringManipulation.Size = New System.Drawing.Size(892, 428)
        Me.dgvStringManipulation.TabIndex = 2
        '
        'dgvStringManipulationBtnDelete
        '
        Me.dgvStringManipulationBtnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvStringManipulationBtnDelete.HeaderText = "X"
        Me.dgvStringManipulationBtnDelete.Name = "dgvStringManipulationBtnDelete"
        Me.dgvStringManipulationBtnDelete.Text = "X"
        Me.dgvStringManipulationBtnDelete.ToolTipText = "Delete"
        Me.dgvStringManipulationBtnDelete.UseColumnTextForButtonValue = True
        Me.dgvStringManipulationBtnDelete.Width = 20
        '
        'dgvStringManipulationCellCmbBankField
        '
        Me.dgvStringManipulationCellCmbBankField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvStringManipulationCellCmbBankField.DataPropertyName = "BankFieldName"
        Me.dgvStringManipulationCellCmbBankField.DataSource = Me.bindingSrcStringManipulationBankField
        Me.dgvStringManipulationCellCmbBankField.DisplayMember = "BankField"
        Me.dgvStringManipulationCellCmbBankField.HeaderText = "Bank Field"
        Me.dgvStringManipulationCellCmbBankField.Name = "dgvStringManipulationCellCmbBankField"
        Me.dgvStringManipulationCellCmbBankField.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStringManipulationCellCmbBankField.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvStringManipulationCellStringFunction
        '
        Me.dgvStringManipulationCellStringFunction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgvStringManipulationCellStringFunction.DataPropertyName = "FunctionName"
        Me.dgvStringManipulationCellStringFunction.HeaderText = "Function Name"
        Me.dgvStringManipulationCellStringFunction.Name = "dgvStringManipulationCellStringFunction"
        Me.dgvStringManipulationCellStringFunction.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStringManipulationCellStringFunction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvStringManipulationCellStringFunction.Width = 130
        '
        'StartingPositionDataGridViewTextBoxColumn
        '
        Me.StartingPositionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.StartingPositionDataGridViewTextBoxColumn.DataPropertyName = "StartingPosition"
        Me.StartingPositionDataGridViewTextBoxColumn.HeaderText = "Start Position"
        Me.StartingPositionDataGridViewTextBoxColumn.MaxInputLength = 3
        Me.StartingPositionDataGridViewTextBoxColumn.Name = "StartingPositionDataGridViewTextBoxColumn"
        '
        'NumberOfCharactersDataGridViewTextBoxColumn
        '
        Me.NumberOfCharactersDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.NumberOfCharactersDataGridViewTextBoxColumn.DataPropertyName = "NumberOfCharacters"
        Me.NumberOfCharactersDataGridViewTextBoxColumn.HeaderText = "No. of Characters"
        Me.NumberOfCharactersDataGridViewTextBoxColumn.MaxInputLength = 3
        Me.NumberOfCharactersDataGridViewTextBoxColumn.Name = "NumberOfCharactersDataGridViewTextBoxColumn"
        Me.NumberOfCharactersDataGridViewTextBoxColumn.Width = 150
        '
        'ucSwiftTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 15.0!)
        Me.AutoScroll = True
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTop)
        Me.Controls.Add(Me.pnlBottom)
        Me.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucSwiftTemplate"
        Me.Size = New System.Drawing.Size(915, 678)
        CType(Me.bindingSrcSwiftTemplateCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcOutputTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcRowFilterSourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcEditableSettingsBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcTanslateBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcLookupBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcLookupSourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcCalculatedFieldsBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcOperand1SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcOperand2SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcOperand3SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBottom.ResumeLayout(False)
        CType(Me.bindingSrcStringManipulationBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DefaultBankValuesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcTranslatorSettingCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommonTemplateHelperBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcRowFilterCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcMapSourceFieldCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcMapBankFieldCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcEditableSettingCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcLookupSettingCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcCalculatedFieldCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcStringManipulationCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommonTemplateHelperBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommonTemplateHelperBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTop.ResumeLayout(False)
        Me.gbxTemplateSettings.ResumeLayout(False)
        Me.gbxTemplateSettings.PerformLayout()
        Me.pnlBody.ResumeLayout(False)
        Me.tabDetail.ResumeLayout(False)
        Me.tbPgRetrieve.ResumeLayout(False)
        Me.tbPgRetrieve.PerformLayout()
        Me.gbxDetailAdviceSettings.ResumeLayout(False)
        Me.gbxDetailAdviceSettings.PerformLayout()
        Me.gbxFilterSetting.ResumeLayout(False)
        Me.gbxFilterSetting.PerformLayout()
        CType(Me.dgvFilterSettings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDuplicateTxnRef.ResumeLayout(False)
        Me.gbxDuplicateTxnRef.PerformLayout()
        Me.gbxRemittanceAmtSettings.ResumeLayout(False)
        Me.gbxRemittanceAmtSettings.PerformLayout()
        Me.gbxNumberSettings.ResumeLayout(False)
        Me.gbxDateSettings.ResumeLayout(False)
        Me.gbxDateSettings.PerformLayout()
        Me.tbPgMap.ResumeLayout(False)
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPgTranslate.ResumeLayout(False)
        Me.gbxEditableSetting.ResumeLayout(False)
        CType(Me.dgvEditable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxTranslatorSetting.ResumeLayout(False)
        CType(Me.dgvTranslator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPgLookup.ResumeLayout(False)
        Me.gbxLookup.ResumeLayout(False)
        CType(Me.dgvLookup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPgCalculatedValues.ResumeLayout(False)
        Me.gbxCalculatedValues.ResumeLayout(False)
        CType(Me.dgvCalculated, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPgStringManipulation.ResumeLayout(False)
        Me.gbxStringManipulation.ResumeLayout(False)
        CType(Me.dgvStringManipulation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton10 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton11 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton12 As System.Windows.Forms.RadioButton
    Friend WithEvents DataGridView8 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewComboBoxColumn1 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewComboBoxColumn2 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewButtonColumn1 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents GroupBox19 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents bindingSrcSwiftTemplateCollection As System.Windows.Forms.BindingSource
    Friend WithEvents ErrorProvider1 As Common.ErrorProviderFixed
    Friend WithEvents bindingSrcRowFilterCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcMapSourceFieldCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcMapBankFieldCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcTranslatorSettingCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcEditableSettingCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcLookupSettingCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcCalculatedFieldCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcStringManipulationCollection As System.Windows.Forms.BindingSource
    Friend WithEvents MappingFieldDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CommonTemplateHelperBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CommonTemplateHelperBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcRowFilterSourceField As System.Windows.Forms.BindingSource
    Friend WithEvents CommonTemplateHelperBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcOutputTemplate As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcLookupSourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcTanslateBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcEditableSettingsBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcLookupBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcCalculatedFieldsBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcOperand1SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcOperand2SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcOperand3SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcStringManipulationBankField As System.Windows.Forms.BindingSource
    Friend WithEvents DefaultBankValuesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn7 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn7 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn4 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn4 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn5 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn5 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn6 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IsDraftDataGridViewCheckBoxColumn6 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents gbxTemplateSettings As System.Windows.Forms.GroupBox
    Friend WithEvents chkEnable As System.Windows.Forms.CheckBox
    Friend WithEvents lblSwiftTemplateName As System.Windows.Forms.Label
    Friend WithEvents txtSwiftTemplateName As System.Windows.Forms.TextBox
    Friend WithEvents lblOutputTemplate As System.Windows.Forms.Label
    Friend WithEvents cboOutputTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents pnlBody As System.Windows.Forms.Panel
    Friend WithEvents tabDetail As System.Windows.Forms.TabControl
    Friend WithEvents tbPgRetrieve As System.Windows.Forms.TabPage
    Friend WithEvents btnSeparate As System.Windows.Forms.Button
    Friend WithEvents gbxDetailAdviceSettings As System.Windows.Forms.GroupBox
    Friend WithEvents txtNoOfCharsPerAdviseRecord As BTMU.Magic.UI.NumberBox
    Friend WithEvents txtAdviseRecordDelimiter As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents rdoIRTMSDelimiter As System.Windows.Forms.RadioButton
    Friend WithEvents rdoIRTMSCharPos As System.Windows.Forms.RadioButton
    Friend WithEvents rdoIRTMSEveryRow As System.Windows.Forms.RadioButton
    Friend WithEvents gbxFilterSetting As System.Windows.Forms.GroupBox
    Friend WithEvents btnFilterSettingsUp As System.Windows.Forms.Button
    Friend WithEvents btnFilterSettingsDown As System.Windows.Forms.Button
    Friend WithEvents rdoFilterSettingOR As System.Windows.Forms.RadioButton
    Friend WithEvents rdoFilterSettingAND As System.Windows.Forms.RadioButton
    Friend WithEvents dgvFilterSettings As System.Windows.Forms.DataGridView
    Friend WithEvents lblSourceFileName As System.Windows.Forms.Label
    Friend WithEvents gbxDuplicateTxnRef As System.Windows.Forms.GroupBox
    Friend WithEvents cboDuplicateTxnRefField3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboDuplicateTxnRefField2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboDuplicateTxnRefField1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents gbxRemittanceAmtSettings As System.Windows.Forms.GroupBox
    Friend WithEvents rdoCents As System.Windows.Forms.RadioButton
    Friend WithEvents rdoDollars As System.Windows.Forms.RadioButton
    Friend WithEvents gbxNumberSettings As System.Windows.Forms.GroupBox
    Friend WithEvents cboDecimalSep As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseSourceFile As System.Windows.Forms.Button
    Friend WithEvents txtSourceFileName As System.Windows.Forms.TextBox
    Friend WithEvents gbxDateSettings As System.Windows.Forms.GroupBox
    Friend WithEvents cboDateType As System.Windows.Forms.ComboBox
    Friend WithEvents cboDateSep As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ChkZerosInDate As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSampleRowData As System.Windows.Forms.TextBox
    Friend WithEvents tbPgMap As System.Windows.Forms.TabPage
    Friend WithEvents btnClearAll As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents dgvBank As System.Windows.Forms.DataGridView
    Friend WithEvents BankFieldExtDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MappedFieldsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MappedValuesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BankSampleValueDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvSource As System.Windows.Forms.DataGridView
    Friend WithEvents SequenceDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceFieldNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceFieldValueDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbPgTranslate As System.Windows.Forms.TabPage
    Friend WithEvents gbxEditableSetting As System.Windows.Forms.GroupBox
    Friend WithEvents btnEditableSettingRowDown As System.Windows.Forms.Button
    Friend WithEvents btnEditableSettingRowUp As System.Windows.Forms.Button
    Friend WithEvents dgvEditable As System.Windows.Forms.DataGridView
    Friend WithEvents gbxTranslatorSetting As System.Windows.Forms.GroupBox
    Friend WithEvents btnTranslatorSettingRowUp As System.Windows.Forms.Button
    Friend WithEvents btnTranslatorSettingRowDown As System.Windows.Forms.Button
    Friend WithEvents dgvTranslator As System.Windows.Forms.DataGridView
    Friend WithEvents tbPgLookup As System.Windows.Forms.TabPage
    Friend WithEvents gbxLookup As System.Windows.Forms.GroupBox
    Friend WithEvents dgvLookup As System.Windows.Forms.DataGridView
    Friend WithEvents btnLookupRowDown As System.Windows.Forms.Button
    Friend WithEvents btnLookupRowUp As System.Windows.Forms.Button
    Friend WithEvents tbPgCalculatedValues As System.Windows.Forms.TabPage
    Friend WithEvents gbxCalculatedValues As System.Windows.Forms.GroupBox
    Friend WithEvents btnCalculatedFieldsRowDown As System.Windows.Forms.Button
    Friend WithEvents btnCalculatedFieldsRowUp As System.Windows.Forms.Button
    Friend WithEvents dgvCalculated As System.Windows.Forms.DataGridView
    Friend WithEvents tbPgStringManipulation As System.Windows.Forms.TabPage
    Friend WithEvents gbxStringManipulation As System.Windows.Forms.GroupBox
    Friend WithEvents btnStringManipulationRowDown As System.Windows.Forms.Button
    Friend WithEvents btnStringManipulationRowUp As System.Windows.Forms.Button
    Friend WithEvents dgvStringManipulation As System.Windows.Forms.DataGridView
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnSaveAsDraft As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnModify As System.Windows.Forms.Button
    Friend WithEvents dgvBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvCmbFilterField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCmbFilterDataType As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCmbFilterOperator As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvTxtFilterValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvTranslatorBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvTranslatorSettingsCellCmbBankField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvTranslatorSettingsCellTxtSourceValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvTranslatorCellCboDefaultValues As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvTranslatorCellChkBankValueEmpty As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvLookupBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvLookupCellCmbBankField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupCellCmbSourceField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupCellTable As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupCellCboLookupKey As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupCellCboLookupValue As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvCalculatedBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvCalculatedFieldsCellCmbBankField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalculatedFieldsOperand1 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalculatedFieldsCellOperator1 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalculatedFieldsOperand2 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalculatedFieldsCellOperator2 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalculatedFieldsOperand3 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvStringManipulationBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvStringManipulationCellCmbBankField As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvStringManipulationCellStringFunction As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents StartingPositionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumberOfCharactersDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvEditableBtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvTranslatorEditableSettingCellBankField As System.Windows.Forms.DataGridViewComboBoxColumn

End Class
