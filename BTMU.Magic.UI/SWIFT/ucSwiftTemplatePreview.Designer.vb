<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSwiftTemplatePreview
    Inherits Magic.UI.ucSwiftTemplateBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.btnConvert = New System.Windows.Forms.Button
        Me.btnConsolidate = New System.Windows.Forms.Button
        Me.groupBoxPreview = New System.Windows.Forms.GroupBox
        Me.dgvPreview = New System.Windows.Forms.DataGridView
        Me.labelValueTrailer = New System.Windows.Forms.Label
        Me.bindingSrcFileFormat = New System.Windows.Forms.BindingSource(Me.components)
        Me.labelTrailer = New System.Windows.Forms.Label
        Me.labelValueHeader = New System.Windows.Forms.Label
        Me.labelHeader = New System.Windows.Forms.Label
        Me.groupBoxValidationErrors = New System.Windows.Forms.GroupBox
        Me.dgvValidationErrors = New System.Windows.Forms.DataGridView
        Me.RecordNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColumnNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ValidationErrorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.labelValueConsolidatedRecords = New System.Windows.Forms.Label
        Me.labelConsolidatedRecords = New System.Windows.Forms.Label
        Me.labelValueErrors = New System.Windows.Forms.Label
        Me.lableErrors = New System.Windows.Forms.Label
        Me.labelValueTotalRecords = New System.Windows.Forms.Label
        Me.labelTotalRecords = New System.Windows.Forms.Label
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBoxPreview.SuspendLayout()
        CType(Me.dgvPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcFileFormat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBoxValidationErrors.SuspendLayout()
        CType(Me.dgvValidationErrors, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidationErrorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(742, 401)
        Me.DataGridView1.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(377, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 28)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "C&lose"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(5, 6)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 28)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "&New"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(284, 6)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 28)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&Duplicate"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(98, 6)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 28)
        Me.Button4.TabIndex = 1
        Me.Button4.Text = "&View"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(191, 6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(87, 28)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&Edit"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Filter By:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Common Template Name :"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(175, 30)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(340, 20)
        Me.TextBox1.TabIndex = 2
        '
        'btnConvert
        '
        Me.btnConvert.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnConvert.Location = New System.Drawing.Point(5, 183)
        Me.btnConvert.Name = "btnConvert"
        Me.btnConvert.Size = New System.Drawing.Size(75, 26)
        Me.btnConvert.TabIndex = 2
        Me.btnConvert.Text = "&Convert"
        Me.btnConvert.UseVisualStyleBackColor = True
        '
        'btnConsolidate
        '
        Me.btnConsolidate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnConsolidate.Location = New System.Drawing.Point(85, 183)
        Me.btnConsolidate.Name = "btnConsolidate"
        Me.btnConsolidate.Size = New System.Drawing.Size(75, 26)
        Me.btnConsolidate.TabIndex = 3
        Me.btnConsolidate.Text = "Consoli&date"
        Me.btnConsolidate.UseVisualStyleBackColor = True
        '
        'groupBoxPreview
        '
        Me.groupBoxPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxPreview.Controls.Add(Me.dgvPreview)
        Me.groupBoxPreview.Controls.Add(Me.labelValueTrailer)
        Me.groupBoxPreview.Controls.Add(Me.labelTrailer)
        Me.groupBoxPreview.Controls.Add(Me.labelValueHeader)
        Me.groupBoxPreview.Controls.Add(Me.labelHeader)
        Me.groupBoxPreview.Location = New System.Drawing.Point(3, 3)
        Me.groupBoxPreview.Name = "groupBoxPreview"
        Me.groupBoxPreview.Size = New System.Drawing.Size(648, 286)
        Me.groupBoxPreview.TabIndex = 4
        Me.groupBoxPreview.TabStop = False
        Me.groupBoxPreview.Text = "Preview"
        '
        'dgvPreview
        '
        Me.dgvPreview.AllowUserToAddRows = False
        Me.dgvPreview.AllowUserToDeleteRows = False
        Me.dgvPreview.AllowUserToOrderColumns = True
        Me.dgvPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPreview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPreview.Location = New System.Drawing.Point(9, 37)
        Me.dgvPreview.Name = "dgvPreview"
        Me.dgvPreview.RowHeadersVisible = False
        Me.dgvPreview.Size = New System.Drawing.Size(634, 220)
        Me.dgvPreview.TabIndex = 15
        '
        'labelValueTrailer
        '
        Me.labelValueTrailer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelValueTrailer.AutoSize = True
        Me.labelValueTrailer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelValueTrailer.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcFileFormat, "Trailer", True))
        Me.labelValueTrailer.Location = New System.Drawing.Point(53, 263)
        Me.labelValueTrailer.Name = "labelValueTrailer"
        Me.labelValueTrailer.Size = New System.Drawing.Size(15, 16)
        Me.labelValueTrailer.TabIndex = 14
        Me.labelValueTrailer.Text = "0"
        '
        'bindingSrcFileFormat
        '
        Me.bindingSrcFileFormat.DataSource = GetType(BTMU.Magic.FileFormat.BaseFileFormat)
        '
        'labelTrailer
        '
        Me.labelTrailer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.labelTrailer.AutoSize = True
        Me.labelTrailer.Location = New System.Drawing.Point(6, 264)
        Me.labelTrailer.Name = "labelTrailer"
        Me.labelTrailer.Size = New System.Drawing.Size(40, 14)
        Me.labelTrailer.TabIndex = 13
        Me.labelTrailer.Text = "Trailer:"
        '
        'labelValueHeader
        '
        Me.labelValueHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelValueHeader.AutoSize = True
        Me.labelValueHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelValueHeader.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcFileFormat, "Header", True))
        Me.labelValueHeader.Location = New System.Drawing.Point(53, 17)
        Me.labelValueHeader.Name = "labelValueHeader"
        Me.labelValueHeader.Size = New System.Drawing.Size(15, 16)
        Me.labelValueHeader.TabIndex = 12
        Me.labelValueHeader.Text = "0"
        '
        'labelHeader
        '
        Me.labelHeader.AutoSize = True
        Me.labelHeader.Location = New System.Drawing.Point(6, 18)
        Me.labelHeader.Name = "labelHeader"
        Me.labelHeader.Size = New System.Drawing.Size(45, 14)
        Me.labelHeader.TabIndex = 11
        Me.labelHeader.Text = "Header:"
        '
        'groupBoxValidationErrors
        '
        Me.groupBoxValidationErrors.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxValidationErrors.Controls.Add(Me.dgvValidationErrors)
        Me.groupBoxValidationErrors.Controls.Add(Me.labelValueConsolidatedRecords)
        Me.groupBoxValidationErrors.Controls.Add(Me.labelConsolidatedRecords)
        Me.groupBoxValidationErrors.Controls.Add(Me.labelValueErrors)
        Me.groupBoxValidationErrors.Controls.Add(Me.lableErrors)
        Me.groupBoxValidationErrors.Controls.Add(Me.labelValueTotalRecords)
        Me.groupBoxValidationErrors.Controls.Add(Me.labelTotalRecords)
        Me.groupBoxValidationErrors.Controls.Add(Me.btnConvert)
        Me.groupBoxValidationErrors.Controls.Add(Me.btnConsolidate)
        Me.groupBoxValidationErrors.Location = New System.Drawing.Point(3, 294)
        Me.groupBoxValidationErrors.Name = "groupBoxValidationErrors"
        Me.groupBoxValidationErrors.Size = New System.Drawing.Size(648, 215)
        Me.groupBoxValidationErrors.TabIndex = 5
        Me.groupBoxValidationErrors.TabStop = False
        Me.groupBoxValidationErrors.Text = "Validation Errors"
        '
        'dgvValidationErrors
        '
        Me.dgvValidationErrors.AllowUserToAddRows = False
        Me.dgvValidationErrors.AllowUserToDeleteRows = False
        Me.dgvValidationErrors.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvValidationErrors.AutoGenerateColumns = False
        Me.dgvValidationErrors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvValidationErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvValidationErrors.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RecordNoDataGridViewTextBoxColumn, Me.ColumnOrdinalNoDataGridViewTextBoxColumn, Me.ColumnNameDataGridViewTextBoxColumn, Me.DescriptionDataGridViewTextBoxColumn})
        Me.dgvValidationErrors.DataSource = Me.ValidationErrorsBindingSource
        Me.dgvValidationErrors.Location = New System.Drawing.Point(9, 19)
        Me.dgvValidationErrors.Name = "dgvValidationErrors"
        Me.dgvValidationErrors.RowHeadersVisible = False
        Me.dgvValidationErrors.Size = New System.Drawing.Size(634, 159)
        Me.dgvValidationErrors.TabIndex = 10
        '
        'RecordNoDataGridViewTextBoxColumn
        '
        Me.RecordNoDataGridViewTextBoxColumn.DataPropertyName = "RecordNo"
        Me.RecordNoDataGridViewTextBoxColumn.FillWeight = 50.0!
        Me.RecordNoDataGridViewTextBoxColumn.HeaderText = "Row No"
        Me.RecordNoDataGridViewTextBoxColumn.Name = "RecordNoDataGridViewTextBoxColumn"
        Me.RecordNoDataGridViewTextBoxColumn.Width = 71
        '
        'ColumnOrdinalNoDataGridViewTextBoxColumn
        '
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn.DataPropertyName = "ColumnOrdinalNo"
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn.FillWeight = 75.0!
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn.HeaderText = "Column No"
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn.Name = "ColumnOrdinalNoDataGridViewTextBoxColumn"
        Me.ColumnOrdinalNoDataGridViewTextBoxColumn.Width = 83
        '
        'ColumnNameDataGridViewTextBoxColumn
        '
        Me.ColumnNameDataGridViewTextBoxColumn.DataPropertyName = "ColumnName"
        Me.ColumnNameDataGridViewTextBoxColumn.HeaderText = "Column"
        Me.ColumnNameDataGridViewTextBoxColumn.Name = "ColumnNameDataGridViewTextBoxColumn"
        Me.ColumnNameDataGridViewTextBoxColumn.Width = 67
        '
        'DescriptionDataGridViewTextBoxColumn
        '
        Me.DescriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.HeaderText = "Error Description"
        Me.DescriptionDataGridViewTextBoxColumn.Name = "DescriptionDataGridViewTextBoxColumn"
        '
        'ValidationErrorsBindingSource
        '
        Me.ValidationErrorsBindingSource.DataMember = "ValidationErrors"
        Me.ValidationErrorsBindingSource.DataSource = Me.bindingSrcFileFormat
        '
        'labelValueConsolidatedRecords
        '
        Me.labelValueConsolidatedRecords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelValueConsolidatedRecords.AutoSize = True
        Me.labelValueConsolidatedRecords.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelValueConsolidatedRecords.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcFileFormat, "ConsolidatedRecords", True))
        Me.labelValueConsolidatedRecords.Location = New System.Drawing.Point(563, 195)
        Me.labelValueConsolidatedRecords.Name = "labelValueConsolidatedRecords"
        Me.labelValueConsolidatedRecords.Size = New System.Drawing.Size(15, 16)
        Me.labelValueConsolidatedRecords.TabIndex = 9
        Me.labelValueConsolidatedRecords.Text = "0"
        '
        'labelConsolidatedRecords
        '
        Me.labelConsolidatedRecords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelConsolidatedRecords.AutoSize = True
        Me.labelConsolidatedRecords.Location = New System.Drawing.Point(448, 196)
        Me.labelConsolidatedRecords.Name = "labelConsolidatedRecords"
        Me.labelConsolidatedRecords.Size = New System.Drawing.Size(116, 14)
        Me.labelConsolidatedRecords.TabIndex = 8
        Me.labelConsolidatedRecords.Text = "Consolidated Records:"
        '
        'labelValueErrors
        '
        Me.labelValueErrors.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelValueErrors.AutoSize = True
        Me.labelValueErrors.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelValueErrors.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcFileFormat, "TotalErrors", True))
        Me.labelValueErrors.Location = New System.Drawing.Point(380, 195)
        Me.labelValueErrors.Name = "labelValueErrors"
        Me.labelValueErrors.Size = New System.Drawing.Size(15, 16)
        Me.labelValueErrors.TabIndex = 7
        Me.labelValueErrors.Text = "0"
        '
        'lableErrors
        '
        Me.lableErrors.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lableErrors.AutoSize = True
        Me.lableErrors.Location = New System.Drawing.Point(340, 196)
        Me.lableErrors.Name = "lableErrors"
        Me.lableErrors.Size = New System.Drawing.Size(40, 14)
        Me.lableErrors.TabIndex = 6
        Me.lableErrors.Text = "Errors:"
        '
        'labelValueTotalRecords
        '
        Me.labelValueTotalRecords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelValueTotalRecords.AutoSize = True
        Me.labelValueTotalRecords.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelValueTotalRecords.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bindingSrcFileFormat, "TotalRecords", True))
        Me.labelValueTotalRecords.Location = New System.Drawing.Point(272, 195)
        Me.labelValueTotalRecords.Name = "labelValueTotalRecords"
        Me.labelValueTotalRecords.Size = New System.Drawing.Size(15, 16)
        Me.labelValueTotalRecords.TabIndex = 5
        Me.labelValueTotalRecords.Text = "0"
        '
        'labelTotalRecords
        '
        Me.labelTotalRecords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelTotalRecords.AutoSize = True
        Me.labelTotalRecords.Location = New System.Drawing.Point(195, 196)
        Me.labelTotalRecords.Name = "labelTotalRecords"
        Me.labelTotalRecords.Size = New System.Drawing.Size(77, 14)
        Me.labelTotalRecords.TabIndex = 4
        Me.labelTotalRecords.Text = "Total Records:"
        '
        'ucSwiftTemplatePreview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.groupBoxValidationErrors)
        Me.Controls.Add(Me.groupBoxPreview)
        Me.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Name = "ucSwiftTemplatePreview"
        Me.Size = New System.Drawing.Size(653, 511)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBoxPreview.ResumeLayout(False)
        Me.groupBoxPreview.PerformLayout()
        CType(Me.dgvPreview, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcFileFormat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBoxValidationErrors.ResumeLayout(False)
        Me.groupBoxValidationErrors.PerformLayout()
        CType(Me.dgvValidationErrors, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidationErrorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnConvert As System.Windows.Forms.Button
    Friend WithEvents btnConsolidate As System.Windows.Forms.Button
    Friend WithEvents groupBoxPreview As System.Windows.Forms.GroupBox
    Friend WithEvents groupBoxValidationErrors As System.Windows.Forms.GroupBox
    Friend WithEvents labelValueErrors As System.Windows.Forms.Label
    Friend WithEvents lableErrors As System.Windows.Forms.Label
    Friend WithEvents labelValueTotalRecords As System.Windows.Forms.Label
    Friend WithEvents labelTotalRecords As System.Windows.Forms.Label
    Friend WithEvents labelValueConsolidatedRecords As System.Windows.Forms.Label
    Friend WithEvents labelConsolidatedRecords As System.Windows.Forms.Label
    Friend WithEvents labelValueTrailer As System.Windows.Forms.Label
    Friend WithEvents labelTrailer As System.Windows.Forms.Label
    Friend WithEvents labelValueHeader As System.Windows.Forms.Label
    Friend WithEvents labelHeader As System.Windows.Forms.Label
    Friend WithEvents dgvPreview As System.Windows.Forms.DataGridView
    Friend WithEvents bindingSrcFileFormat As System.Windows.Forms.BindingSource
    Friend WithEvents SourceDataDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConvertedDataDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HeaderDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TrailerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValidationErrorsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalErrorsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalRecordsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConsolidatedRecordsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvValidationErrors As System.Windows.Forms.DataGridView
    Friend WithEvents ValidationErrorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RecordNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnOrdinalNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
