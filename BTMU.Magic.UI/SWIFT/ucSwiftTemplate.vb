 
Imports BTMU.MAGIC.Common
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.FileFormat
Imports System.IO
Imports System.Text.RegularExpressions

''' <summary>
''' This Class Encapsulates the GUI for Swift Template
''' </summary>
''' <remarks></remarks>
Public Class ucSwiftTemplate

    Private _dataList As New BTMU.Magic.CommonTemplate.SwiftTemplateCollection()
    Private _dataModel As Magic.CommonTemplate.SwiftTemplate
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

#Region " Members "

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
                Handles Me.Load

        bindingSrcOutputTemplate.DataSource = CommonTemplateModule.AllMasterTemplates
        cboDateType.DataSource = CommonTemplateModule.ShortDateType
        cboDecimalSep.DataSource = CommonTemplateModule.DecimalSeparator

        dgvCmbFilterOperator.DataSource = CommonTemplateModule.FilterOperator
        dgvLookupCellTable.DataSource = CommonTemplateModule.LookupTables
        dgvCalculatedFieldsCellOperator1.DataSource = CommonTemplateModule.Operators1
        dgvCalculatedFieldsCellOperator2.DataSource = CommonTemplateModule.Operators2
        dgvStringManipulationCellStringFunction.DataSource = CommonTemplateModule.StringFunctions
        dgvCmbFilterDataType.DataSource = CommonTemplateModule.FilterDataType
        bindingSrcSwiftTemplateCollection.DataSource = _dataList

    End Sub

    ''' <summary>
    ''' Prepares the GUI in Data Entry Mode to allow Adding data for Swift Template 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AddNew()

        _dataList.Clear()
        _dataModel = New BTMU.Magic.CommonTemplate.SwiftTemplate
        _dataList.Add(_dataModel)
        populateReferenceFields()
        _isNew = True

        'Init Databound/Unbound Controls
        FillEditableTable()
        InitializeNewTemplate()
        tabDetail.SelectedIndex = 0

        EnableDisableForm(True)
    End Sub

    Private Sub InitializeNewTemplate()

        _dataModel.IsEnabled = True

        _dataModel.IsAmountInDollars = True
        _dataModel.IsAdviceRecordForEveryRow = True
        _dataModel.IsFilterConditionAND = True
        cboOutputTemplate.Enabled = True
        txtSwiftTemplateName.Enabled = True
        btnModify.Enabled = False

        rdoCents.Checked = _dataModel.IsAmountInCents
        rdoDollars.Checked = _dataModel.IsAmountInDollars
        rdoFilterSettingAND.Checked = _dataModel.IsFilterConditionAND
        rdoFilterSettingOR.Checked = _dataModel.IsFilterConditionOR
        rdoIRTMSEveryRow.Checked = _dataModel.IsAdviceRecordForEveryRow
        rdoIRTMSCharPos.Checked = _dataModel.IsAdviceRecordAfterCharacter
        rdoIRTMSDelimiter.Checked = _dataModel.IsAdviceRecordDelimited

        IRTMSFormatChanged()

        cboOutputTemplate.SelectedIndex = -1
        cboDateType.SelectedIndex = -1
        cboDecimalSep.SelectedIndex = -1
        dtEditableList.Rows.Clear()


    End Sub
    ''' <summary>
    ''' Duplicates the given Swift Template and Presents the duplicated template for Editing
    ''' </summary>
    ''' <param name="swiftTemplateFile">Swift Template filename</param>
    ''' <param name="IsDraft">boolean value indicating whether the template is Draft</param>
    ''' <remarks></remarks>
    Public Sub DuplicateExisting(ByVal swiftTemplateFile As String, Optional ByVal IsDraft As Boolean = False)

        EditExisting(swiftTemplateFile)

        If _dataModel Is Nothing Then
            Exit Sub
        End If

        _dataModel.SwiftTemplateName = _dataModel.SwiftTemplateName & " Copy"
        _dataModel.IsDraft = IsDraft
        IsNew = True

        txtSwiftTemplateName.Enabled = True

    End Sub
    ''' <summary>
    ''' Prepares the GUI for Editing properties of the given Swift Template
    ''' </summary>
    ''' <param name="swiftTemplateFile"></param>
    ''' <remarks></remarks>
    Public Sub EditExisting(ByVal swiftTemplateFile As String)

        If swiftTemplateFile = String.Empty Then
            ContainerForm.ShowView()
            Exit Sub
        End If

        Try
            EnableDisableForm(True)

            _dataList.Clear()

            _dataModel = SwiftTemplate.LoadFromFile2(swiftTemplateFile)
            'format the sample row to reflect the NewLine characters 
            If Not _dataModel.SampleRow Is Nothing Then _dataModel.SampleRow = _dataModel.SampleRow.Replace(Chr(10), vbCrLf)
            _dataList.Add(_dataModel)
            populateReferenceFields()
            _dataModel.Validate()

            cboDateType.Text = _dataModel.DateType
            cboDecimalSep.Text = _dataModel.DecimalSeparator

            If _dataModel.IsAmountInCents Then
                rdoCents.Checked = True
                rdoDollars.Checked = False
            Else
                rdoCents.Checked = False
                rdoDollars.Checked = True
            End If

            If _dataModel.IsFilterConditionAND Then
                rdoFilterSettingAND.Checked = True
                rdoFilterSettingOR.Checked = False
            Else
                rdoFilterSettingAND.Checked = False
                rdoFilterSettingOR.Checked = True
            End If

            RemoveHandler rdoIRTMSEveryRow.CheckedChanged, AddressOf rdoIRTMSEveryRow_CheckedChanged
            RemoveHandler rdoIRTMSCharPos.CheckedChanged, AddressOf rdoIRTMSCharPos_CheckedChanged
            RemoveHandler rdoIRTMSDelimiter.CheckedChanged, AddressOf rdoIRTMSDelimiter_CheckedChanged

            If _dataModel.IsAdviceRecordAfterCharacter Then
                rdoIRTMSEveryRow.Checked = False
                rdoIRTMSCharPos.Checked = True
                rdoIRTMSDelimiter.Checked = False
                txtAdviseRecordDelimiter.Enabled = False
                txtNoOfCharsPerAdviseRecord.Enabled = True
            ElseIf _dataModel.IsAdviceRecordDelimited Then
                rdoIRTMSEveryRow.Checked = False
                rdoIRTMSCharPos.Checked = False
                rdoIRTMSDelimiter.Checked = True
                txtAdviseRecordDelimiter.Enabled = True
                txtNoOfCharsPerAdviseRecord.Enabled = False
            Else
                rdoIRTMSEveryRow.Checked = True
                rdoIRTMSCharPos.Checked = False
                rdoIRTMSDelimiter.Checked = False
                txtAdviseRecordDelimiter.Enabled = False
                txtNoOfCharsPerAdviseRecord.Enabled = False

            End If

            AddHandler rdoIRTMSEveryRow.CheckedChanged, AddressOf rdoIRTMSEveryRow_CheckedChanged
            AddHandler rdoIRTMSCharPos.CheckedChanged, AddressOf rdoIRTMSCharPos_CheckedChanged
            AddHandler rdoIRTMSDelimiter.CheckedChanged, AddressOf rdoIRTMSDelimiter_CheckedChanged

            FillEditableTable()

        Catch ex As System.Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error Loading " & swiftTemplateFile _
                                , ex.Message)
            ContainerForm.ShowView()
        End Try

        If Not _dataModel Is Nothing Then _dataModel.SetDirty(True) ' Added the If condition to prevent the unhandled exception SRS/BTMU/2010/0050

        tabDetail.SelectedIndex = 0

        cboOutputTemplate.Enabled = False
        txtSwiftTemplateName.Enabled = False
        EnableDisableAdviceRecordSetting()

        cboDuplicateTxnRefField1.DataBindings("Text").ReadValue()
        cboDuplicateTxnRefField2.DataBindings("Text").ReadValue()
        cboDuplicateTxnRefField3.DataBindings("Text").ReadValue()

    End Sub

    ''' <summary>
    ''' Prepares the GUI for Viewing properties of the given Swift Template
    ''' </summary>
    ''' <param name="swiftTemplateFile"></param>
    ''' <remarks></remarks>
    Public Sub ViewExisting(ByVal swiftTemplateFile As String)

        EditExisting(swiftTemplateFile)

        _dataModel.ApplyEdit()

        EnableDisableForm(False)

    End Sub

    Sub EnableDisableForm(ByVal _ctrStatus As Boolean)

        cboOutputTemplate.Enabled = _ctrStatus
        txtSwiftTemplateName.Enabled = _ctrStatus

        chkEnable.Enabled = _ctrStatus
        txtSourceFileName.Enabled = _ctrStatus
        btnBrowseSourceFile.Enabled = _ctrStatus
        gbxDateSettings.Enabled = _ctrStatus
        gbxNumberSettings.Enabled = _ctrStatus
        gbxRemittanceAmtSettings.Enabled = _ctrStatus
        gbxDuplicateTxnRef.Enabled = _ctrStatus
        gbxDetailAdviceSettings.Enabled = _ctrStatus
        gbxFilterSetting.Enabled = _ctrStatus

        btnSeparate.Enabled = _ctrStatus
        txtSampleRowData.Enabled = _ctrStatus

        dgvBank.Enabled = _ctrStatus
        dgvSource.Enabled = _ctrStatus
        btnClear.Enabled = _ctrStatus
        btnClearAll.Enabled = _ctrStatus

        gbxTranslatorSetting.Enabled = _ctrStatus
        gbxEditableSetting.Enabled = _ctrStatus

        gbxLookup.Enabled = _ctrStatus

        gbxCalculatedValues.Enabled = _ctrStatus

        gbxStringManipulation.Enabled = _ctrStatus

        btnModify.Enabled = Not (_ctrStatus)
        btnSave.Enabled = _ctrStatus
        btnSaveAsDraft.Enabled = _ctrStatus

    End Sub

    '##########################################
    '# Set Enabled=True only for iRTMS format #
    '##########################################
    Private Sub EnableDisableAdviceRecordSetting()
        Dim FileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList

        If bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub
        FileFormat = bindingSrcOutputTemplate.Current

        Select Case FileFormat.OutputFormat
            Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM", "OiRTMSCI", "OiRTMSGC", "OiRTMSGD", "OiRTMSRM"
                gbxDetailAdviceSettings.Enabled = True
            Case Else
                gbxDetailAdviceSettings.Enabled = False
        End Select

    End Sub

#Region "Event Handlers and Initializers"

    Private Sub btnBrowseSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSourceFile.Click

        Dim filename As String = ""
        Dim SrcFileDialog As New OpenFileDialog()
        'SrcFileDialog.Filter = "Text Documents (*.txt)|*.txt"
        SrcFileDialog.Filter = MsgReader.GetString("E10000032")
        SrcFileDialog.Multiselect = False
        SrcFileDialog.InitialDirectory = My.Settings.SampleSourceFolder
        SrcFileDialog.CheckFileExists = True
        SrcFileDialog.CheckPathExists = True

        If SrcFileDialog.ShowDialog(Me) = DialogResult.OK Then
            filename = SrcFileDialog.FileName
        Else
            filename = ""
        End If

        SrcFileDialog.Dispose()

        If Not ValidateNewSourceFile(filename) Then Exit Sub

        RetrieveSampleRow(filename)

    End Sub

    Private Function ValidateNewSourceFile(ByVal newSrcFilename As String) As Boolean

        If newSrcFilename = "" Then Return False

        'If there are Map Source Fields, Check if they are equal to those found in the new file
        If _dataModel.MapSourceFields.Count = 0 Then Return True

        ' If there exists a Mapping, dont wipe it out but 
        ' update it with the fields from new source file
        Try
            If _dataModel.MapSourceFields.Count > 0 Then

                Return UpdateMapSourceFields(newSrcFilename)
            End If

        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error in Source File:", ex.Message)
            Return False
        End Try


        Return False

    End Function

    Sub RetrieveSampleRow(ByVal filename As String)

        If filename = "" Or _dataModel Is Nothing Then Exit Sub

        Try

            _dataModel.RetrieveSampleRow(filename)

        Catch ex As Exception

            MessageBox.Show(ex.Message, "Generate SWIFT Transaction Template" _
                         , MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

#Region " Source Filename Drag and Drop "

    Private Sub txtSourceFileName_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSourceFileName.DragEnter

        If IsInSupportedFormat(e) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If


    End Sub

    Private Sub txtSourceFileName_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSourceFileName.DragDrop



        Dim filename As String = ""
        Dim Filenames As String()
        txtSourceFileName.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        'Just to make sure the new file does not impact the existing mapping, if any
        If Not ValidateNewSourceFile(filename) Then Exit Sub

        RetrieveSampleRow(filename)

    End Sub

    Private Function IsInSupportedFormat(ByVal e As System.Windows.Forms.DragEventArgs) As Boolean
        Dim fmt As String
        Dim supported As Boolean = False

        For Each fmt In e.Data.GetFormats()
            If fmt = System.Windows.Forms.DataFormats.FileDrop Then
                supported = True
                Exit For
            End If
        Next
        Return supported
    End Function

#End Region

    Private Sub rdoIRTMSEveryRow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSEveryRow.CheckedChanged
        If rdoIRTMSEveryRow.Checked Then
            IRTMSFormatChanged()
        End If

    End Sub

    Private Sub rdoIRTMSCharPos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSCharPos.CheckedChanged

        If rdoIRTMSCharPos.Checked Then
            IRTMSFormatChanged()
        End If
    End Sub

    Private Sub rdoIRTMSDelimiter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSDelimiter.CheckedChanged

        If rdoIRTMSDelimiter.Checked Then
            IRTMSFormatChanged()
        End If

    End Sub

    Private Sub IRTMSFormatChanged()

        If Not _dataModel Is Nothing Then

            If rdoIRTMSEveryRow.Checked Then

                _dataModel.AdviceRecordCharPos = ""
                _dataModel.AdviceRecordDelimiter = ""
                txtNoOfCharsPerAdviseRecord.Enabled = False
                txtAdviseRecordDelimiter.Enabled = False

            ElseIf rdoIRTMSCharPos.Checked Then

                txtNoOfCharsPerAdviseRecord.Enabled = True
                _dataModel.AdviceRecordDelimiter = ""
                txtAdviseRecordDelimiter.Enabled = False

            ElseIf rdoIRTMSDelimiter.Checked Then

                _dataModel.AdviceRecordDelimiter = ""
                txtAdviseRecordDelimiter.Enabled = True
                _dataModel.AdviceRecordCharPos = ""
                txtNoOfCharsPerAdviseRecord.Enabled = False

            End If

        End If

    End Sub

    Private Sub chkEnable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEnable.CheckedChanged

        If chkEnable.Checked Then
            Log_EnableCommonTemplate(frmLogin.GsUserName, _
                          System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                                      , txtSwiftTemplateName.Text) _
                          & CommonTemplateModule.CommonTemplateFileExtension)
        Else
            Log_DisableCommonTemplate(frmLogin.GsUserName, _
                                      System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                                                  , txtSwiftTemplateName.Text) _
                                      & CommonTemplateModule.CommonTemplateFileExtension)
        End If

    End Sub


#End Region

#Region "Save/Save As/Cancel/Update/Validate"

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ConfirmBeforeCloseAndCancel()
    End Sub
    Friend Function ConfirmBeforeCloseAndCancel() As Boolean

        'Cancel - Return to "View" screen			
        '1	User will be prompted with a message to save, if the data has been changed.	
        '2	On "Yes"	
        '	 	New - Template will be saved as Draft.
        '	 	If user edited the Draft template then the template will be saved as Draft.
        '	 	If user edited the Non-Draft template then the template will be saved as Non-Draft.
        '3	On "No"	
        '	 	The changes made will not be saved.
        '	 	Show the "View" screen.
        '4	On "Cancel"	
        '	 	The Entry screen will remain with the data changes.
        If _dataModel Is Nothing Then
            ContainerForm.ShowView()
            Return False
        End If

        If _dataModel.IsDirty Then
            'Data has been changed. Do you want to save the data?
            Dim _decision As DialogResult = MessageBox.Show(MsgReader.GetString("E10000033") _
                    , "Generate Swift Transaction Template", MessageBoxButtons.YesNoCancel)

            Select Case _decision

                Case DialogResult.Yes
                    If _isNew Then
                        If Not IsSavableAsDraft() Then Return True
                        btnSaveAsDraft_Click(Nothing, Nothing)

                    Else
                        If _dataModel.IsDraft Then
                            If Not IsSavableAsDraft() Then Return True
                            btnSaveAsDraft_Click(Nothing, Nothing)

                        Else
                            If Not IsDataSavable() Then Return True
                            btnSave_Click(Nothing, Nothing)

                        End If
                    End If
                Case DialogResult.No
                    ContainerForm.ShowView()
                Case DialogResult.Cancel
                    Return True
            End Select

        Else
            ContainerForm.ShowView()
        End If

        Return False

    End Function
    Private Function IsDataSavable() As Boolean

        If CanSaveCleanData() Then

            'Checking duplicate template name
            If txtSwiftTemplateName.Enabled = True And _
                 (File.Exists(Path.Combine(CommonTemplateModule.SwiftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                  File.Exists(Path.Combine(CommonTemplateModule.SwiftDraftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                'MessageBox.Show("Template Name already exists. Please use different Template Name.", "Duplicate")
                MessageBox.Show(MsgReader.GetString("E10000034"), "Duplicate")
                Return False
            Else
                Return True
            End If
        End If

        Return False

    End Function
    Private Function IsSavableAsDraft() As Boolean

        Dim errMessage As String = String.Empty


        If _dataModel.OutputTemplateName = "" Then
            'errMessage &= "Please select Master Template" & vbCrLf
            errMessage &= MsgReader.GetString("E10000035") & vbCrLf
        End If

        If _dataModel.SwiftTemplateName = "" Then
            'errMessage &= "Please fill in Swift Template Name" & vbCrLf
            errMessage &= MsgReader.GetString("E10000036") & vbCrLf
        End If

        ValidateSwiftTemplateName()

        'Checking duplicate template name
        If txtSwiftTemplateName.Enabled = True And _
            (File.Exists(Path.Combine(CommonTemplateModule.SwiftDraftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
            File.Exists(Path.Combine(CommonTemplateModule.SwiftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
            'errMessage &= "Template Name already exists. Please use different Template Name." & vbCrLf
            errMessage &= MsgReader.GetString("E10000037") & vbCrLf
        End If

        If errMessage.Trim = String.Empty Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Save as Draft : ", errMessage)
            Return False
        End If

    End Function

    Private Sub Control_Validated(ByVal sender As Object, _
                                    ByVal e As System.EventArgs) Handles txtSwiftTemplateName.Validated

        Dim ctl As Control = CType(sender, Control)
        Dim bnd As Binding

        For Each bnd In ctl.DataBindings
            If bnd.IsBinding Then
                Dim obj As System.ComponentModel.IDataErrorInfo = _
                  CType(_dataModel, System.ComponentModel.IDataErrorInfo)
                ErrorProvider1.SetError( _
                  ctl, obj.Item(bnd.BindingMemberInfo.BindingField))
            End If
        Next

    End Sub

    Private Sub ValidateAllControls(ByVal ctl As Control)
        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                ValidateAllControls(ctlDetail)
            Else
                Control_Validated(ctlDetail, Nothing)
            End If
        Next

    End Sub

    Private Sub prepareForSave()

        _dataModel.IsAmountInCents = rdoCents.Checked
        _dataModel.IsAmountInDollars = rdoDollars.Checked
        _dataModel.IsFilterConditionAND = rdoFilterSettingAND.Checked
        _dataModel.IsFilterConditionOR = rdoFilterSettingOR.Checked
        _dataModel.IsAdviceRecordForEveryRow = rdoIRTMSEveryRow.Checked
        _dataModel.IsAdviceRecordAfterCharacter = rdoIRTMSCharPos.Checked
        _dataModel.IsAdviceRecordDelimited = rdoIRTMSDelimiter.Checked

    End Sub

    Private Function ShowValidationError() As Boolean
        Dim errorInfo As System.ComponentModel.IDataErrorInfo
        Dim errorMessage As String
        Dim firstError As New System.Text.StringBuilder
        Dim detailError As New System.Text.StringBuilder

        errorInfo = CType(_dataModel, System.ComponentModel.IDataErrorInfo)
        errorMessage = errorInfo.Error
        If Not IsNothingOrEmptyString(errorMessage) Then
            Dim errorList As String() = errorMessage.Split(Environment.NewLine)
            Dim temp As String
            For Each temp In errorList
                If firstError.Length > 0 Then
                    If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", temp))
                Else
                    If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("{0}", temp))
                End If
            Next
        End If

        For Each detailItem As Magic.CommonTemplate.RowFilter In _dataModel.Filters
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If

            ''''''BEGIN - data type validation '''''''''''''''''
            If detailItem.FilterOperator Is Nothing Or detailItem.FilterType Is Nothing Or detailItem.FilterValue Is Nothing Then Continue For

            Select Case detailItem.FilterOperator.ToUpper()

                Case "> AND <", "> AND <=", ">= AND <", ">= AND <="

                    Dim values2() As String = Regex.Split(detailItem.FilterValue.Trim(), Regex.Escape("||"))

                    If values2.Length = 2 Then
                        'validate data type
                        If detailItem.FilterType.ToUpper() = "NUMERIC" Then
                            Dim x As Decimal
                            For Each numbervalue As String In values2
                                If Not HelperModule.IsDecimalDataType(numbervalue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                                    If firstError.Length > 0 Then
                                        'detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected", detailItem.Sequence, numbervalue))
                                        detailError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, numbervalue))
                                    Else
                                        firstError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, numbervalue))
                                    End If
                                End If
                            Next

                        ElseIf detailItem.FilterType.ToUpper() = "DATE" Then
                            For Each datevalue As String In values2
                                Dim x As Date
                                If Not HelperModule.IsDateDataType(datevalue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZeroInDate) Then
                                    If firstError.Length > 0 Then
                                        'Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected
                                        detailError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, datevalue))
                                    Else
                                        firstError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, datevalue))
                                    End If
                                End If
                            Next
                        End If
                    Else

                        If firstError.Length > 0 Then
                            'Filter Setting (Position: {0}) -  Filter Value is not in expected format!, Expected Format: Value1||Value2
                            detailError.AppendLine(String.Format(MsgReader.GetString("E10000040"), detailItem.Sequence))
                        Else
                            firstError.AppendLine(String.Format(MsgReader.GetString("E10000040"), detailItem.Sequence))
                        End If

                    End If

                Case Else
                    'validate data type
                    If detailItem.FilterType.ToUpper() = "NUMERIC" Then

                        Dim x As Decimal
                        If Not HelperModule.IsDecimalDataType(detailItem.FilterValue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                            If firstError.Length > 0 Then
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected"
                                detailError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, detailItem.FilterValue))
                            Else
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected"
                                firstError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    ElseIf detailItem.FilterType.ToUpper() = "DATE" Then

                        Dim x As Date
                        If Not HelperModule.IsDateDataType(detailItem.FilterValue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZeroInDate) Then
                            If firstError.Length > 0 Then
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected"
                                detailError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, detailItem.FilterValue))
                            Else
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected"
                                firstError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    End If

            End Select
            ''''''END - data type validation '''''''''''''''''

            'duplicate RowFilter?
            If _dataModel.Filters.IsDuplicateRowFilter(detailItem) Then
                If firstError.Length > 0 Then
                    'Filter Setting (Position: {0}) - Duplicate Row Filter!
                    detailError.AppendLine(String.Format(MsgReader.GetString("E10000041"), detailItem.Sequence))
                Else
                    firstError.AppendLine(String.Format(MsgReader.GetString("E10000041"), detailItem.Sequence))
                End If
            End If

            'Multiple Instances of a RowFilter Field with Different Filter Type (Data Type)?
            If _dataModel.Filters.hasDuplicateFilterType(detailItem) Then
                If firstError.Length > 0 Then
                    '"Filter Setting (Position: {0}) - Duplicate Row Filter Field '{1}' cannot have different Data Type!"
                    detailError.AppendLine(String.Format(MsgReader.GetString("E10000042"), detailItem.Sequence, detailItem.FilterField))
                Else
                    firstError.AppendLine(String.Format(MsgReader.GetString("E10000042"), detailItem.Sequence, detailItem.FilterField))
                End If
            End If

        Next


        For Each detailItem As Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.TranslatorSetting In _dataModel.TranslatorSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next


        For Each detailItem As Magic.CommonTemplate.EditableSetting In _dataModel.EditableSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.LookupSetting In _dataModel.LookupSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.CalculatedField In _dataModel.CalculatedFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.StringManipulation In _dataModel.StringManipulations
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("String Manipulation  (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        ValidateAllControls(Me)

        If (firstError.ToString() = String.Empty AndAlso detailError.ToString() = String.Empty) Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", firstError.ToString & vbCrLf & detailError.ToString)
            Return False
        End If

    End Function

    Private Function CanSaveCleanData() As Boolean

        _dataModel.Validate()

        If Not (ValidateMandatoryField()) Then Return False


        If ShowValidationError() Then

            prepareForSave()
            Dim dirtFlag As Boolean = _dataModel.IsDirty
            _dataModel.ApplyEdit()
            _dataModel.SetDirty(dirtFlag)

            If Not VerifyMapping() Then Return False

            Return True
        Else
            Return False
        End If

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If CanSaveCleanData() Then

                'Checking duplicate template name
                If txtSwiftTemplateName.Enabled = True And _
                    (File.Exists(Path.Combine(CommonTemplateModule.SwiftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                     File.Exists(Path.Combine(CommonTemplateModule.SwiftDraftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                    'Template Name already exists. Please use different Template Name.
                    MessageBox.Show(MsgReader.GetString("E10000037"), "Duplicate")
                    Exit Sub
                End If

                Dim fileToBeDeleted As String = ""
                If _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.SwiftDraftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension

                _isNew = False
                _dataModel.IsDraft = False

                _dataModel.SaveToFile2( _
                            System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                                                    , txtSwiftTemplateName.Text) _
                            & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)

                If _isNew Then
                    Log_CreateCommonTemplate(frmLogin.GsUserName, _
                                System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                                            , txtSwiftTemplateName.Text) _
                                & CommonTemplateModule.CommonTemplateFileExtension)
                Else
                    Log_ModifyCommonTemplate(frmLogin.GsUserName, _
                               System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                                           , txtSwiftTemplateName.Text) _
                               & CommonTemplateModule.CommonTemplateFileExtension)
                End If

                'SWIFT Transaction Template was saved successfully
                MessageBox.Show(MsgReader.GetString("E10000043") _
                                , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ' Refresh the CommonTemplate in the list and Show the listview
                ContainerForm.ShowView()
            End If

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        EnableDisableForm(True)

        _dataModel.SetDirty(True)
        EnableDisableAdviceRecordSetting()

        txtSwiftTemplateName.Enabled = False
        cboOutputTemplate.Enabled = False

    End Sub

    Private Sub btnSaveAsDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAsDraft.Click

        Try


            Dim fileToBeDeleted As String = ""
            If Not _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.SwiftFolder, txtSwiftTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension

            _dataModel.IsDraft = True
            _dataModel.ApplyEdit()
            _isNew = False
            prepareForSave()

            If Not IsSavableAsDraft() Then Exit Sub

            _dataModel.SaveToFile2( _
                System.IO.Path.Combine(CommonTemplateModule.SwiftDraftFolder _
                                    , txtSwiftTemplateName.Text) _
                & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)

            MessageBox.Show(MsgReader.GetString("E10000044") _
                        , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ContainerForm.ShowDraftView()

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try

    End Sub

#End Region

#Region " Up/Down/Delete and Input at Grid "

#Region "Handler for Grid Row Up/Down Buttons"

    Private Sub SelectGridRow(ByVal row As GridRow, ByVal grid As DataGridView, ByRef Xobj As Object, ByRef YObj As Object)

        Try

            If grid.CurrentRow Is Nothing Or grid.Rows.Count <= 0 _
                Or grid.CurrentRow.Index + 1 = grid.Rows.Count Then
                Exit Sub
            End If

            If row = GridRow.Up Then ' Row Up

                If grid.CurrentRow.Index = 0 Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index - 1).Selected = True
            Else ' Row Down

                If grid.CurrentRow.Index + 2 = grid.Rows.Count Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index + 1).Selected = True
            End If

            bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False

            Dim tmp As Object = Xobj

            Xobj = YObj : YObj = tmp

            bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
            bindingSrcSwiftTemplateCollection.ResetBindings(False)

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnFilterSettingsUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterSettingsUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvFilterSettings, _dataModel.Filters(dgvFilterSettings.CurrentRow.Index), _dataModel.Filters(dgvFilterSettings.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnFilterSettingsDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterSettingsDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvFilterSettings, _dataModel.Filters(dgvFilterSettings.CurrentRow.Index), _dataModel.Filters(dgvFilterSettings.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnTranslatorSettingRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTranslatorSettingRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnTranslatorSettingRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTranslatorSettingRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnEditableSettingRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditableSettingRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnEditableSettingRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditableSettingRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnLookupRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnLookupRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnCalculatedFieldsRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculatedFieldsRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvCalculated, _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnCalculatedFieldsRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculatedFieldsRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvCalculated, _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnStringManipulationRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStringManipulationRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvStringManipulation, _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index), _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnStringManipulationRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStringManipulationRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvStringManipulation, _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index), _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

#End Region

#Region "Handler for GridRow Delete Buttons"

    Private Sub dgvFilterSettings_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFilterSettings.CellContentClick

        Try

            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvFilterSettings.Rows(e.RowIndex).Cells("dgvBtnDelete").Value Is Nothing Then
                Exit Sub
            End If
            'Are you sure you want to remove the {0} at Row: {1}?
            If MessageBox.Show( _
            String.Format(MsgReader.GetString("E10000045") _
                   , "Filter Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.Filters.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvTranslator_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranslator.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvTranslator.Rows(e.RowIndex).Cells("dgvTranslatorBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                   , "Translation Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.TranslatorSettings.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvEditable_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEditable.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvEditable.Rows(e.RowIndex).Cells("dgvEditableBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Editable Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.EditableSettings.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvLookup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLookup.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvLookup.Rows(e.RowIndex).Cells("dgvLookupBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Lookup Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.LookupSettings.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvCalculated_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCalculated.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvCalculated.Rows(e.RowIndex).Cells("dgvCalculatedBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Calculation Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.CalculatedFields.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvStringManipulation_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStringManipulation.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvStringManipulation.Rows(e.RowIndex).Cells("dgvStringManipulationBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "String Manipulation Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = False
                _dataModel.StringManipulations.RemoveAt(e.RowIndex)
                bindingSrcSwiftTemplateCollection.RaiseListChangedEvents = True
                bindingSrcSwiftTemplateCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

#End Region

#End Region

#End Region

#Region "Validation Functions"

    Private Sub cboDuplicateTxnRefField2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDuplicateTxnRefField2.Enter

        If cboDuplicateTxnRefField1.Items.Count = 0 Then Exit Sub

        If cboDuplicateTxnRefField1.Text.Trim() = "" Then
            MessageBox.Show(MsgReader.GetString("E02000170") _
                    , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDuplicateTxnRefField1.Focus()

        Else 'Reference Field 2' is the same as 'Reference Field 1'. Please select another reference field.
            If cboDuplicateTxnRefField1.Text = cboDuplicateTxnRefField2.Text Then
                MessageBox.Show(MsgReader.GetString("E02000180") _
                , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()
            End If
        End If
    End Sub

    Private Sub cboDuplicateTxnRefField3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDuplicateTxnRefField3.Enter

        If cboDuplicateTxnRefField1.Items.Count = 0 Then Exit Sub

        'Reference Field 1' has not been selected yet. Please select 'Reference Field 1' first." 
        If cboDuplicateTxnRefField1.Text.Trim() = "" Then
            MessageBox.Show(MsgReader.GetString("E02000170") _
                  , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDuplicateTxnRefField1.Focus()

        Else 'Reference Field 2' has not been selected yet. Please select  'Reference Field 2' first."

            If cboDuplicateTxnRefField2.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E02000180") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()

            Else
                'Reference Field 3' is the same as 'Reference Field 1'. Please select another reference field." 
                If cboDuplicateTxnRefField1.Text = cboDuplicateTxnRefField3.Text Then
                    MessageBox.Show(MsgReader.GetString("E02000210") _
                                    , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                    MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField3.Focus()

                End If
                'Reference Field 3' is the same as 'Reference Field 2'. Please select another reference field."
                If cboDuplicateTxnRefField2.Text = cboDuplicateTxnRefField3.Text Then
                    MessageBox.Show(MsgReader.GetString("E02000220") _
                                , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField3.Focus()

                End If

            End If

        End If
    End Sub

    Private Function ValidateOutputTemplate() As Boolean

        If cboOutputTemplate.Text = String.Empty Then
            '"Master Template has not been selected yet, please select Master Template"
            MessageBox.Show(MsgReader.GetString("E10000035"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cboOutputTemplate.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateSourceFile() As Boolean
        '"There is no SWIFT file name, please enter a SWIFT file name."
        If txtSourceFileName.Text = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000028"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            btnBrowseSourceFile.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidateSwiftTemplateName() As Boolean
        '"There is no SWIFT Template name, please enter a SWIFT Template name."
        If txtSwiftTemplateName.Text = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000036"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtSwiftTemplateName.Focus()
            Return False
        End If

        Try
            Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

            Dim value As String = txtSwiftTemplateName.Text
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                Common.BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E01000010"), "Swift Template Name"))

                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

        Return True

    End Function

    Private Function ValidateNumberSetting() As Boolean

        If cboDecimalSep.SelectedIndex = -1 Then
            ''Decimal Separator' has not been selected yet, please select 'Decimal Separator'.
            MessageBox.Show(MsgReader.GetString("E10000046"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDecimalSep.Focus()
            Return False

        End If

        ' cents & decimal separator should not co-exist!
        If ((cboDecimalSep.Text = "." Or cboDecimalSep.Text = ",") _
                AndAlso rdoCents.Checked) Then
            ''Cents' setting in 'Remittance Amount Setting' and decimal separator should not co-exist. Do you want to proceed with this setting?
            If MessageBox.Show(MsgReader.GetString("E10000047") _
                    , "Generate Swift Transaction Template", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                cboDecimalSep.Focus()
                Return False
            End If
        End If

        Return True

    End Function

    Private Function ValidateDateSetting() As Boolean

        If cboDateType.SelectedIndex = -1 Then
            ''Date Type' has not been selected yet, please select 'Date Type'.
            MessageBox.Show(MsgReader.GetString("E10000048"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDateType.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateMandatoryField() As Boolean

        Return (ValidateOutputTemplate() AndAlso ValidateSwiftTemplateName() _
                AndAlso ValidateSourceFile() AndAlso ValidateNumberSetting() _
                AndAlso ValidateDateSetting() AndAlso ValidateTxnReferenceFields() _
                AndAlso ValidateCharsPerAdviseRecord() AndAlso ValidateAdviseRecordDelimiter())

    End Function



    Private Function ValidateTxnReferenceFields() As Boolean

        If cboDuplicateTxnRefField1.Items.Count = 0 Then Return True

        ' 3rd txnref is given, 2nd txnref is mandatory and should not be equal to 3
        If cboDuplicateTxnRefField3.Text.Trim() <> "" Then
            '"'Reference Field 2' has not been selected yet. Please select  'Reference Field 2' first."
            If cboDuplicateTxnRefField2.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E02000200") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()
                Return False
            Else '"'Reference Field 3' is the same as 'Reference Field 2'. Please select another reference field." 
                If cboDuplicateTxnRefField3.Text.Trim() = cboDuplicateTxnRefField2.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000220") _
                    , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField2.Focus()
                    Return False
                End If
                '"'Reference Field 3' is the same as 'Reference Field 1'. Please select another reference field."
                If cboDuplicateTxnRefField3.Text.Trim() = cboDuplicateTxnRefField1.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000210") _
                            , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField1.Focus()
                    Return False
                End If

            End If
        End If


        ' 2nd txnref is given, 1st txnref is mandatory and should not be equal to 2
        If cboDuplicateTxnRefField2.Text.Trim() <> "" Then

            If cboDuplicateTxnRefField1.Text.Trim() = "" Then
                '"'Reference Field 1' has not been selected yet. Please select  'Reference Field 1' first." 
                MessageBox.Show(MsgReader.GetString("E02000170") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField1.Focus()
                Return False
            Else
                '"'Reference Field 2' is the same as 'Reference Field 1'. Please select another reference field." 
                If cboDuplicateTxnRefField1.Text.Trim() = cboDuplicateTxnRefField2.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000180") _
                            , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField1.Focus()
                    Return False
                End If
            End If
        End If

        Return True

    End Function

    Private Function ValidateCharsPerAdviseRecord() As Boolean

        If rdoIRTMSCharPos.Checked AndAlso txtNoOfCharsPerAdviseRecord.Number = "" Then
            '"'No. of Characters per Advice Record' is empty. Please fill in no. of characters."
            MessageBox.Show(MsgReader.GetString("E02000270"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtNoOfCharsPerAdviseRecord.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateAdviseRecordDelimiter() As Boolean
        '"Delimeter for the advice record is empty. Please fill in delimiter."
        If rdoIRTMSDelimiter.Checked AndAlso txtAdviseRecordDelimiter.Text = "" Then
            MessageBox.Show(MsgReader.GetString("E02000280"), "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtAdviseRecordDelimiter.Focus()
            Return False
        End If

        Return True

    End Function


    'Not implemented yet
    Private Function ValidateTypeOfMappedFields(ByVal _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField _
                    , ByVal objCusValue As BTMU.Magic.CommonTemplate.MapSourceField) As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        Select Case _objMapBankField.DataType.ToUpper()
            Case "NUMBER", "CURRENCY"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                End If
                If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                    errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                End If
            Case "DATETIME"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                    If cboDateSep.Text <> "No Space" Then
                        If cboDateSep.Text = "<Space>" Then
                            value = value.Replace(" ", "")
                            'Else
                            '    value = value.Replace(cboDateSep.Text, "")
                        End If
                    End If
                End If
                If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZeroInDate) Then
                    errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                End If
        End Select

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function

#End Region

#Region "Separate Field Names and Values"


    Private Sub PopulateMapBankField()

        If _dataModel Is Nothing Or bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub

        Try

            Dim selMstTmp As MasterTemplateList = bindingSrcOutputTemplate.Current

            ' clear translation/editable/lookup/calculation and string mani grids
            _dataModel.MapBankFields.Clear()
            _dataModel.TranslatorSettings.Clear()
            _dataModel.EditableSettings.Clear()
            _dataModel.LookupSettings.Clear()
            _dataModel.CalculatedFields.Clear()
            _dataModel.StringManipulations.Clear()

            Dim _bankField As MapBankField

            If selMstTmp.IsFixed Then

                Dim _objTmp As New MasterTemplateFixed()

                _objTmp = _objTmp.LoadFromFile2(String.Format("{0}/{1}{2}", _
                    (New MasterTemplateSharedFunction).MasterTemplateViewFolder _
                    , cboOutputTemplate.Text _
                    , (New MasterTemplateSharedFunction).MasterTemplateFileExtension))

                For Each _masterFixedDetail As MasterTemplateFixedDetail In _objTmp.MasterTemplateFixedDetailCollection

                    _bankField = New MapBankField()
                    _bankField.Position = _masterFixedDetail.Position
                    _bankField.BankField = _masterFixedDetail.FieldName
                    _bankField.Mandatory = _masterFixedDetail.Mandatory
                    _bankField.Header = _masterFixedDetail.Header
                    _bankField.Detail = _masterFixedDetail.Detail
                    _bankField.Trailer = _masterFixedDetail.Trailer

                    If _masterFixedDetail.MapSeparator Is Nothing Then
                        _bankField.MapSeparator = ""
                    Else
                        _bankField.MapSeparator = _masterFixedDetail.MapSeparator
                    End If

                    If _masterFixedDetail.DataSample Is Nothing Then
                        _bankField.BankSampleValue = ""
                    Else
                        _bankField.BankSampleValue = _masterFixedDetail.DataSample
                    End If

                    _bankField.DataType = _masterFixedDetail.DataType
                    _bankField.DateFormat = _masterFixedDetail.DateFormat
                    _bankField.DataLength = _masterFixedDetail.DataLength
                    _bankField.DefaultValue = _masterFixedDetail.DefaultValue

                    _bankField.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                    _bankField.DecimalSeparator = _masterFixedDetail.DecimalSeparator

                    _dataModel.MapBankFields.Add(_bankField)

                Next

            Else

                Dim _objTmp As New MasterTemplateNonFixed()

                _objTmp = _objTmp.LoadFromFile2(String.Format("{0}/{1}{2}", _
                    (New MasterTemplateSharedFunction).MasterTemplateViewFolder _
                    , cboOutputTemplate.Text _
                    , (New MasterTemplateSharedFunction).MasterTemplateFileExtension))

                For Each _masterNonFixedDetail As MasterTemplateNonFixedDetail In _objTmp.MasterTemplateNonFixedDetailCollection

                    _bankField = New MapBankField()
                    _bankField.Position = _masterNonFixedDetail.Position
                    _bankField.BankField = _masterNonFixedDetail.FieldName
                    _bankField.Mandatory = _masterNonFixedDetail.Mandatory
                    _bankField.Header = _masterNonFixedDetail.Header
                    _bankField.Detail = _masterNonFixedDetail.Detail
                    _bankField.Trailer = _masterNonFixedDetail.Trailer

                    If _masterNonFixedDetail.MapSeparator Is Nothing Then
                        _bankField.MapSeparator = ""
                    Else
                        _bankField.MapSeparator = _masterNonFixedDetail.MapSeparator
                    End If

                    If _masterNonFixedDetail.DataSample Is Nothing Then
                        _bankField.BankSampleValue = ""
                    Else
                        _bankField.BankSampleValue = _masterNonFixedDetail.DataSample
                    End If

                    _bankField.DataType = _masterNonFixedDetail.DataType
                    _bankField.DateFormat = _masterNonFixedDetail.DateFormat
                    _bankField.DataLength = _masterNonFixedDetail.DataLength
                    _bankField.DefaultValue = _masterNonFixedDetail.DefaultValue

                    _dataModel.MapBankFields.Add(_bankField)

                Next

            End If

            'Add bank fields with default values to Translation
            bindingSrcTranslatorSettingCollection.RaiseListChangedEvents = False
            For Each _bankField In _dataModel.MapBankFields

                If _bankField.DefaultValue Is Nothing Or _bankField.DefaultValue = String.Empty Then Continue For

                Dim bnkfieldTranslated As New CommonTemplate.TranslatorSetting()
                bnkfieldTranslated.BankFieldName = _bankField.BankField
                'bnkfieldTranslated.DefaultValues = _bankField.DefaultValue
                _dataModel.TranslatorSettings.Add(bnkfieldTranslated)

            Next

            bindingSrcTranslatorSettingCollection.RaiseListChangedEvents = True
            bindingSrcTranslatorSettingCollection.ResetBindings(False)


            _dataModel.SetDefaultTranslation()

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Swift Transaction Template" _
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Function UpdateMapSourceFields(ByVal newSrcFilename As String) As Boolean

        If newSrcFilename = "" Then Return False

        If Not SwiftTemplate.IsValidSwiftFile(newSrcFilename) Then
            '"SWIFT file is invalid."
            MessageBox.Show(MsgReader.GetString("E10000007"), "Generate SWIFT Transaction Template" _
                         , MessageBoxButtons.OK, MessageBoxIcon.Error)
          return False 
        End If

        Dim names As New List(Of String)
        Dim namesAndValues As New MapSourceFieldCollection()

        Try
            _dataModel.GetSourceFields(newSrcFilename, names, namesAndValues)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generate SWIFT Transaction Template" _
                         , MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        'bother only mapped fields
        'if any one of the Mapped source fields is missing in the new source file, throw error

        names.Sort(System.StringComparer.InvariantCultureIgnoreCase)
        For Each _bkfield As MapBankField In _dataModel.MapBankFields
            For Each _srcField As MapSourceField In _bkfield.MappedSourceFields
                If names.IndexOf(_srcField.SourceFieldName) < 0 Then
                    BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E10000049"), newSrcFilename))
                    Return False
                End If
            Next
        Next

        'update existing fields, add additional source fields
        ' and update the reference fields
        Dim _newsrcfield As MapSourceField

        For Each _nname As MapSourceField In namesAndValues

            If _dataModel.MapSourceFields.IsFound(_nname.SourceFieldName) Then
                'update existing fields
                _dataModel.MapSourceFields.UpdateSourceValue(_nname)
            Else 'add additional fields
                _newsrcfield = New MapSourceField()
                _newsrcfield.SourceFieldName = _nname.SourceFieldName
                _newsrcfield.SourceFieldValue = _nname.SourceFieldValue
                _dataModel.MapSourceFields.Add(_newsrcfield)
                cboDuplicateTxnRefField1.Items.Add(_nname.SourceFieldName)
                cboDuplicateTxnRefField2.Items.Add(_nname.SourceFieldName)
                cboDuplicateTxnRefField3.Items.Add(_nname.SourceFieldName)
            End If
        Next

        FillEditableTable()
        Return True
    End Function
    Private Sub btnSeparate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeparate.Click
        Dim objMasterTemplate As MasterTemplateList
        objMasterTemplate = bindingSrcOutputTemplate.Current
        If objMasterTemplate.OutputFormat = iFTS2MultiLineFormat.OutputFormatString OrElse _
        objMasterTemplate.OutputFormat = OMAKASEFormat.OutputFormatString Then
            MessageBox.Show(MsgReader.GetString("E02020010"), "Generate SWIFT Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If (Not ValidateMandatoryField()) Or _dataModel Is Nothing Or bindingSrcOutputTemplate Is Nothing Then Exit Sub

        'Warn User of Loss of Data Pertaining to Settings
        If _dataModel.MapSourceFields.Count > 0 Then
            '"This action will clear all settings! Are you sure you want to proceed?"
            If MessageBox.Show(MsgReader.GetString("E10000050"), "Confirm", MessageBoxButtons.YesNo) <> DialogResult.Yes Then Exit Sub
        End If

        Me.Cursor = Cursors.AppStarting

        If Not SeparateFields() Then
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        populateReferenceFields()
        cboDuplicateTxnRefField1.SelectedIndex = 0
        cboDuplicateTxnRefField2.SelectedIndex = 0
        cboDuplicateTxnRefField3.SelectedIndex = 0

        FillEditableTable()
        Me.Cursor = Cursors.Default


    End Sub

    Sub populateReferenceFields()

        If _dataModel Is Nothing Then Exit Sub

        cboDuplicateTxnRefField1.Items.Clear()
        cboDuplicateTxnRefField2.Items.Clear()
        cboDuplicateTxnRefField3.Items.Clear()

        For Each _srcFld As MapSourceField In _dataModel.MapSourceFields

            cboDuplicateTxnRefField1.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField2.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField3.Items.Add(_srcFld.SourceFieldName)
        Next

        cboDuplicateTxnRefField1.Items.Insert(0, "")
        cboDuplicateTxnRefField2.Items.Insert(0, "")
        cboDuplicateTxnRefField3.Items.Insert(0, "")

    End Sub

    Private Function SeparateFields() As Boolean

        If Not SwiftTemplate.IsValidSwiftFile(txtSourceFileName.Text) Then

            MessageBox.Show(MsgReader.GetString("E10000007"), "Generate SWIFT Transaction Template" _
                         , MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False

        End If

        '#1. Populate Bank Field/Value
        PopulateMapBankField()

        '#2. Populate Source Field/Value
        Try

            _dataModel.PopulateSourceFields()

        Catch ex As Exception

            MessageBox.Show(ex.Message, "Generate SWIFT Transaction Template" _
                         , MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True

    End Function


#End Region


#Region " Drag n Drop for Bank Field - Source Field Mapping "

    

    ' '' ''''''''''''''''''''''Drag-N-Drop from Map Source Fields Grid To Bank Fields Grid ''''''''''''''''

    Private Sub dgvSource_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSource.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvSource.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then dgvBank.DoDragDrop(String.Format("SourceFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvBank_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("SourceFields") Then Exit Sub

        Dim _rowIndexSourceFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtBankFieldsDataGrid As Point = dgvBank.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexBankFieldsDataGrid As Integer = dgvBank.HitTest(_dropOfPointAtBankFieldsDataGrid.X _
                                                        , _dropOfPointAtBankFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvBank_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragOver
        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("SourceFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    ' '' ''''''''''''''''''''''Drag-N-Drop from Bank Fields Grid To Map Source Fields Grid ''''''''''''''''
    Private Sub dgvBank_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBank.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvBank.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then If rowIndex > -1 Then dgvSource.DoDragDrop(String.Format("BankFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvSource_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("BankFields") Then Exit Sub

        Dim _rowIndexBankFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtSourceFieldsDataGrid As Point = dgvSource.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexSourceFieldsDataGrid As Integer = dgvSource.HitTest(_dropOfPointAtSourceFieldsDataGrid.X _
                                                        , _dropOfPointAtSourceFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvSource_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragOver

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("BankFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub DoMapping(ByVal _rowIndexSourceFieldsDataGrid As Int32, ByVal _rowIndexBankFieldsDataGrid As Int32)

        If _rowIndexBankFieldsDataGrid = -1 Or _rowIndexSourceFieldsDataGrid = -1 Then Exit Sub

        Dim _objMapSourceField As BTMU.Magic.CommonTemplate.MapSourceField
        Dim _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField

        Try

            If Not _dataModel.MapBankFields Is Nothing Then
                _objMapBankField = _dataModel.MapBankFields(_rowIndexBankFieldsDataGrid)

                If Not _dataModel.MapSourceFields Is Nothing Then
                    _objMapSourceField = _dataModel.MapSourceFields(_rowIndexSourceFieldsDataGrid)
                    Dim cloneSourceField As New BTMU.Magic.CommonTemplate.MapSourceField
                    cloneSourceField.SourceFieldName = _objMapSourceField.SourceFieldName
                    cloneSourceField.SourceFieldValue = _objMapSourceField.SourceFieldValue
                    '#### Need to check if the same mapping exists
                    If _objMapBankField.MappedSourceFields.IsFound(_objMapSourceField) Then
                        '"The Field has already been mapped."
                        MessageBox.Show(MsgReader.GetString("E02000340"), "Generate Common Transaction Template" _
                                     , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ElseIf ValidateTypeOfMappedFields(_objMapBankField, cloneSourceField) Then
                        _objMapBankField.MappedSourceFields.Add(cloneSourceField)
                        _objMapBankField.MappingChanged()
                        dgvBank.Refresh()

                        For Each setting As TranslatorSetting In _dataModel.TranslatorSettings
                            If setting.BankFieldName = _objMapBankField.BankField AndAlso HelperModule.IsNothingOrEmptyString(setting.BankValue) AndAlso HelperModule.IsNothingOrEmptyString(setting.CustomerValue) AndAlso setting.BankValueEmpty = False Then
                                setting.SetDefaultCustomerValue(setting.BankFieldName)
                            End If
                        Next

                    End If

                End If

            End If

        Catch ex As Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error while Mapping : ", ex.Message)

        End Try

    End Sub


#Region " Clear Source Field - Bank Field Mapping "

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim _rowSelected As Boolean = False
        'Is there a MapSource Field selected at the data grid on the left hand side?
        'Is there a MapBank Field selected at the datagrid on the right hand side

        If Not dgvSource.CurrentRow Is Nothing Then

            If dgvSource.CurrentRow.Index <> -1 AndAlso dgvBank.CurrentRow.Index <> -1 Then
                _rowSelected = True

                'There shall exist a Mapping between the MapSourceField and MapBankField
                If _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.IsFound(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index)) Then
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.Remove(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index))
                    dgvBank.Refresh()
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappingChanged()
                Else
                    '"Fields are not mapped to each other."
                    MessageBox.Show(MsgReader.GetString("E02000420"), "Generate Swift Transaction Template" _
                                               , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If
        If Not _rowSelected Then
            MessageBox.Show(MsgReader.GetString("E10000051"), "Generate Swift Transaction Template" _
                                             , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAll.Click

        'Clears all mappedsourcefields from bankfields
        For Each objMapBankField As BTMU.Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            objMapBankField.MappedSourceFields.Clear()
            dgvBank.Refresh()
            objMapBankField.MappingChanged()
        Next

    End Sub

#End Region

#End Region


    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        If CanSaveCleanData() Then

            If bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub

            Dim selectedMasterTemplate As MasterTemplateList = bindingSrcOutputTemplate.Current

            Try
                Dim _objPreview As BaseFileFormat = BaseFileFormat.CreateFileFormat(selectedMasterTemplate.OutputFormat)
                _objPreview.CommonTemplateName = _dataModel.SwiftTemplateName
                _objPreview.MasterTemplateName = _dataModel.OutputTemplateName
                _objPreview.SourceFileName = _dataModel.SourceFileName
                _objPreview.WorksheetName = ""
                _objPreview.TransactionEndRow = 0
                _objPreview.RemoveRowsFromEnd = 0
                _objPreview.EliminatedCharacter = ""
                _objPreview.UseValueDate = False
                _objPreview.ValueDate = Date.Today

                frmPreview.ShowPreview(_dataModel, selectedMasterTemplate, False, Nothing, _objPreview)
                frmPreview.UcCommonTemplatePreview1.btnConsolidate.Visible = False
                frmPreview.UcCommonTemplatePreview1.btnConvert.Visible = False

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

    End Sub

    'Fillup the BankField of TranslationSetting with the default values of BankField
    Private Sub dgvTranslator_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTranslator.EditingControlShowing

        Dim bnd As BindingSource = dgvTranslator.DataSource
        If bnd.Position < 0 Then Exit Sub

        If ((Not (e.Control.GetType() Is GetType(DataGridViewComboEditingControl))) _
            Or (dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value Is Nothing)) Then Exit Sub


        For Each _bkfield As MapBankField In _dataModel.MapBankFields
            If _bkfield.BankField = dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value.ToString() Then

                If (_bkfield.DefaultValue <> String.Empty) Then

                    Dim freak As DataGridViewComboEditingControl = e.Control
                    freak.AddItems(_bkfield.DefaultValue.Split(","), True)  'a blank value is required 

                End If

                Exit For
            End If
        Next

    End Sub

    'Query DB and fillup the Kookup Key and Value columns
    Private Sub dgvLookup_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvLookup.EditingControlShowing

        Dim bnd As BindingSource = dgvTranslator.DataSource
        If bnd.Position < 0 Then Exit Sub

        If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupKey" _
            And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupValue" Then Exit Sub

        Try
            If dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                '"Please select the Table first."
                MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Dim freak As DataGridViewComboEditingControl = e.Control
            freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value.ToString()))


            'Just in case the First Row of Tablexx has been changed
            'combobox Item Cxx-XXX would not be valid 
            'Work Around: Select the Item based on the Column Name
            If freak.SelectedItemPersistent Is Nothing Then Exit Sub
            For Each item As String In freak.Items
                If item.ToString().StartsWith(freak.SelectedItemPersistent.ToString().Split("-")(0)) Then
                    freak.SelectedItem = item
                    Exit For
                End If
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvStringManipulation_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStringManipulation.DataError
        If e.ColumnIndex = 3 Or e.ColumnIndex = 4 Then
            '"Numeric value is required!"
            MessageBox.Show(MsgReader.GetString("E10000053"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    Private Sub tabDetail_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabDetail.SelectedIndexChanged

        'If there is no database file, then disable the look up grid
        If tabDetail.SelectedIndex = 3 Then dgvLookup.Enabled = frmExcel2DB.LookupDatabaseExists()

    End Sub

    Private Function VerifyMapping() As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        For Each objBankField As MapBankField In _dataModel.MapBankFields

            If objBankField.DataType Is Nothing Then Continue For

            Select Case objBankField.DataType.ToUpper()
                Case "NUMBER", "CURRENCY"

                    If objBankField.MappedSourceFields.Count > 1 Then If MessageBox.Show(String.Format("BankField: '{0}' is mapped to more than one source fields. Do you want to continue?", objBankField.BankField), "Confirm", MessageBoxButtons.YesNo) = DialogResult.No Then Return False

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty
                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")

                        If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                            errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                        End If

                    Next

                Case "DATETIME"

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty

                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")

                        If cboDateSep.Text <> "No Space" Then If cboDateSep.Text = "<Space>" Then value = value.Replace(" ", "")

                        If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZeroInDate) Then
                            errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                        End If

                    Next

            End Select
        Next

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function

    Private dtEditableList As DataTable

    ''' <summary>
    ''' Fills the Editable Settings Grid with the list of Bank Fields that can be edited
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditableTable()

        If dtEditableList Is Nothing Then
            dtEditableList = New DataTable
            dtEditableList.Columns.Add("Field1", GetType(String))
        End If

        dtEditableList.Rows.Clear()

        Dim currentDetail As SwiftTemplate = bindingSrcSwiftTemplateCollection.Current
        Dim currentMaster As MasterTemplateList = bindingSrcOutputTemplate.Current

        If currentDetail IsNot Nothing AndAlso currentMaster IsNot Nothing Then

            For Each temp As String In BaseFileFormat.GetEditableFields(currentMaster.OutputFormat)
                dtEditableList.Rows.Add(temp)
            Next

        End If

        bindingSrcEditableSettingsBankField.DataSource = dtEditableList
        dgvTranslatorEditableSettingCellBankField.DisplayMember = "Field1"
        dgvTranslatorEditableSettingCellBankField.ValueMember = "Field1"
        dgvTranslatorEditableSettingCellBankField.DataSource = bindingSrcEditableSettingsBankField

    End Sub

    Private Sub dgvEditable_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEditable.DataError
        e.Cancel = True
    End Sub

    Private Sub cboOutputTemplate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTemplate.LostFocus
        EnableDisableAdviceRecordSetting()

    End Sub
End Class
