Imports System.Windows.Forms
''' <summary>
''' Base Class for Swift Template UI
''' </summary>
''' <remarks></remarks>
Public Class ucSwiftTemplateBase
    Inherits System.Windows.Forms.UserControl

    Protected _parent As frmSwiftTemplate
    Protected _isNew As Boolean
    ''' <summary>
    ''' This Property is to Get/Set the Container Form 
    ''' </summary>
    ''' <value>Form</value>
    ''' <returns>Returns the Container Form</returns>
    ''' <remarks></remarks>
    Public Property ContainerForm() As frmSwiftTemplate
        Get
            Return _parent
        End Get
        Set(ByVal value As frmSwiftTemplate)
            _parent = value
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set whether the Template is a new one 
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is new</returns>
    ''' <remarks></remarks>
    Public Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
        Set(ByVal value As Boolean)
            _isNew = value
        End Set
    End Property

End Class

