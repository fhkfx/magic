Imports System.Windows.Forms
''' <summary>
''' Base Class for the GUI of Common Transaction Template for Text
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateTextBase
    Inherits System.Windows.Forms.UserControl

    Protected _parent As frmCommonTemplateText
    Protected _isNew As Boolean

    ''' <summary>
    ''' This function is to Get/Set Container Form of Common Transaction Template for Text
    ''' </summary>
    ''' <value>Form</value>
    ''' <returns>Returns Container Form</returns>
    ''' <remarks></remarks>
    Public Property ContainerForm() As frmCommonTemplateText
        Get
            Return _parent
        End Get
        Set(ByVal value As frmCommonTemplateText)
            _parent = value
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Template is New
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is new</returns>
    ''' <remarks></remarks>
    Public Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
        Set(ByVal value As Boolean)
            _isNew = value
        End Set
    End Property

End Class

''' <summary>
''' Enumerates the Direction of Movement of Records in GridView
''' </summary>
''' <remarks></remarks>
Public Enum GridRow
    Up
    Down
End Enum
