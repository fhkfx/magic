Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

''' <summary>
''' User Control for Previewing Common Template
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplatePreview
    Protected myContainer As frmPreview
    Private _objPreview As BaseFileFormat
    Private _objDataModel As ICommonTemplate
    Private _objMasterTemplateList As MasterTemplateList
    Public _isConsolidated As Boolean ' whether the Consolidate button is clicked
    Private _isQuickConversion As Boolean
    Private _trimData As Boolean = False
    Private _consolidateData As Boolean = False
    'Private _objConvert As IConvert

    ''' <summary>
    ''' Data Binding Source for Preview Data
    ''' </summary>
    ''' <remarks></remarks>
    Public _previewDataBindingSource As New BindingSource

    Dim _errorList As New System.Collections.Generic.SortedDictionary(Of Integer, List(Of BTMU.Magic.FileFormat.FileFormatError))

    ''' <summary>
    ''' Displays the Preview Screen
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template</param>
    ''' <param name="ObjMasterTemplateList">Master Template</param>
    ''' <param name="isQuickConversion">Boolean value indicating whether the preview is for a quick conversion</param>
    ''' <param name="Setup">Quick Configuration Setup Information</param>
    ''' <param name="ObjPreview">Preview Records</param>
    ''' <remarks></remarks>
    Sub ShowPreview(ByRef ObjDataModel As ICommonTemplate, _
                    ByRef ObjMasterTemplateList As MasterTemplateList, _
                    ByVal isQuickConversion As Boolean, _
                    ByVal Setup As QuickConfigurationItem, _
                            ByRef ObjPreview As BaseFileFormat)

        _objDataModel = ObjDataModel
        _objMasterTemplateList = ObjMasterTemplateList
        _isQuickConversion = isQuickConversion

        If Not Setup Is Nothing Then _trimData = Setup.TrimData

        _objPreview = ObjPreview

        If IsNothingOrEmptyString(_objPreview.IntermediaryFileName) Then
            ObjDataModel.PrepareForPreview(ObjMasterTemplateList.OutputFormat, _objPreview.SourceFileName, _objPreview.WorksheetName, _objPreview.TransactionEndRow, _objPreview.RemoveRowsFromEnd)
        Else
            ObjDataModel.PrepareForPreview(ObjMasterTemplateList.OutputFormat, _objPreview.IntermediaryFileName, _objPreview.WorksheetName, _objPreview.TransactionEndRow, _objPreview.RemoveRowsFromEnd)
        End If

        _objPreview.DoPreview(ObjDataModel, ObjMasterTemplateList, frmExcel2DB.DatabaseConnectionString)

        PrepareGridForDisplay()

    End Sub

    Private Sub btnConvert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConvert.Click

        If _objPreview.PreviewDataTable.Rows.Count = 0 Then
            MessageBox.Show("There are no rows to convert.", "Convert Transaction", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Dim temp As DataTable
        temp = _objPreview.PreviewDataTable
        Try
            _objDataModel.PreviewSourceData = _objPreview.PreviewDataTable
            _objDataModel.DuplicateCheck(_objMasterTemplateList.OutputFormat)
            ConvertData()
        Catch ex As Exception
            _objPreview.PreviewDataTable = temp
            _previewDataBindingSource.DataSource = _objPreview.PreviewDataTable
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", ex.Message)
        End Try


    End Sub
    ''' <summary>
    ''' Consolidates the Transaction Records
    ''' </summary>
    ''' <returns>Boolean value indicating whether the conversion is successful</returns>
    ''' <remarks></remarks>
    Public Function ConsolidateData() As Boolean

        _isConsolidated = True
        frmConsolidate.Visible = True
        frmConsolidate.ConsolidateData(_objMasterTemplateList, _objDataModel, _objPreview)
        frmConsolidate.Show()
        frmConsolidate.BringToFront()

    End Function

    ''' <summary>
    ''' Converts the Transaction Records to the target Format
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ConvertData()
        Try
            Dim sourceFileName As String = String.Empty

            'Step 1 : Set the color of all highlighted columns to default color
            dgvPreview.DefaultCellStyle.BackColor = Color.White

            _previewDataBindingSource.RaiseListChangedEvents = False

            'Step 2 : Proceed to convert the file  
            _objPreview.DoConvert(_objDataModel, _objMasterTemplateList)

            _previewDataBindingSource.RaiseListChangedEvents = True
            _previewDataBindingSource.ResetBindings(False)

            'Step 3 : Refresh the preview grid
            PrepareGridForDisplay()


            'Step 4 : Proceed to check for Consolidation, Omakase doesnot allow consolidation
            'CARROT (Changed by FHK 2013-03-05)
            'If _objMasterTemplateList.OutputFormat <> OMAKASEFormat.OutputFormatString Then
            If _objMasterTemplateList.OutputFormat <> OMAKASEFormat.OutputFormatString AndAlso _
                Not _objMasterTemplateList.OutputFormat.Equals(CarrotCustomerDataFormat.OutputFormatString, StringComparison.InvariantCultureIgnoreCase) Then
                If Not _isQuickConversion Then
                    ' Ignore this step if the conversion is called from Quick Conversion menu
                    If Not _isConsolidated And Not _objPreview.IsErrorWithMandatoryFields Then
                        If MessageBox.Show("Do you want to consolidate the data?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            ConsolidateData()
                            Exit Sub
                        End If
                    End If
                End If
            End If


            'Step 5 : If error at Mandatory Field, do not convert the file
            If _objPreview.IsErrorWithMandatoryFields Then
                'PrepareGridForDisplay()
                MessageBox.Show("The file cannot be converted as there are errors. Please check the Validation errors.", "Convert Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Step 6 : For GCMS Format, do not allow to convert the file with more than 1000 records
            If _objMasterTemplateList.OutputFormat = GCMSFormat.OutputFormatString Then
                If _objPreview.PreviewDataTable.Rows.Count > 1000 Then
                    MessageBox.Show("The file cannot be converted as the total number of records exceeds 1000!", "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If


            'Step 6.1 : For Mr. Omakase India Format II, check whether user wants to convert the file if it has different date values
            If _objMasterTemplateList.OutputFormat = OMAKASEFormatII.OutputFormatString() AndAlso Not _objPreview.UseValueDate Then
                If OMAKASEFormatII.IsSameDateValue = False Then
                    If MessageBox.Show(OMAKASEFormatII.MsgReader.GetString("E04000281"), "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                    MessageBox.Show("The file cannot be converted as the total number of records exceeds 1000!", "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If

            'Step 6.2 : For GCMS Plus Format, check whether allow to split files automatically by the system for >1000 records files
            'initialize default value again
            GCMSPlusFormat.SplitFile = False

            If _objMasterTemplateList.OutputFormat = GCMSPlusFormat.OutputFormatString AndAlso _objPreview.PreviewDataTable.Rows.Count > 1000 Then
                If MessageBox.Show("Number of records in source file exceeds 1000 records acceptable by GCMS Plus. Output file will be split into several files." & vbCrLf & _
                    "Do you want to proceed?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    GCMSPlusFormat.SplitFile = True
                Else
                    MessageBox.Show("Number of records in source file exceeds 1000 records acceptable by GCMS Plus." & _
                        " Please split the files and perform conversion again.", "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GCMSPlusFormat.SplitFile = False
                    Exit Sub
                End If
            End If



            'Step 7 : If there are errors other than Mandatory Field error,proceed to trim the data
            If _objPreview.ValidationErrors.Count > 0 Then
                If _isQuickConversion And _trimData Then
                    _objPreview.TrimData()
                Else
                    Dim strmessage As String = "There are still Customer Data greater than Max Length in Master Template." _
                                & vbNewLine & "Do you want the system to truncate the data on your behalf or, " _
                                & vbNewLine & "you want to modify the data manually* ?" _
                                & vbNewLine & vbNewLine & vbTab & "'Yes', Data will be truncated by the system." _
                                & vbNewLine & vbTab & "'No',  Modify data manually*." _
                                & vbNewLine & vbNewLine & "* If the particular field is set to 'editable'."

                    If MessageBox.Show(strmessage, "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        _objPreview.TrimData()
                    Else
                        Exit Sub
                    End If

                End If

            End If

            'Step 9 : If error at Mandatory Field, do not convert the file
            If _objPreview.IsErrorWithMandatoryFields Then
                MessageBox.Show("The file cannot be converted as there are errors. Please check the Validation errors.", "Convert Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Step 10 : Proceed to generate the file
            If ConversionModule.GenerateFile(_objDataModel, _objMasterTemplateList, False, _objPreview) Then
                ' Unload the preview form
                frmPreview.Close()
                frmConsolidate.Close()
            End If

        Catch ex As MagicException
            BTMUExceptionManager.LogAndShowError("Error", ex.Message)
        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error", "System Error. Please contact your System Administrator")
            BTMUExceptionManager.HandleException(ex)
        End Try
    End Sub

    Private Sub btnConsolidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsolidate.Click
        _previewDataBindingSource.RaiseListChangedEvents = False

        'Check Consolidate Field length validation
        'if Consolidate field length is greater than specified length in master template
        'stop from consolidation
        
        If _objPreview.ValidationErrors.Count > 0 AndAlso CheckConsoFieldError Then
            MessageBox.Show("Can not proceed the Consolidation due to one of the Fields' length is greater than the maximum length." & vbCrLf & "Please check the validation error list and correct the data before you proceed the consolidation.", "Convert Transaction", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        ConsolidateData()
        _previewDataBindingSource.RaiseListChangedEvents = True
        _previewDataBindingSource.ResetBindings(False)
    End Sub
    ''' <summary>
    ''' Prepares the GUI for Previewing Transaction Records
    ''' </summary>
    ''' <remarks></remarks>

    Public Sub PrepareGridForDisplay()

        dgvPreview.SuspendLayout()
        dgvPreview.TopLeftHeaderCell.Value = " Sl No "

        'Error
        _errorList.Clear()
        For Each _error As FileFormatError In _objPreview.ValidationErrors
            If Not _errorList.ContainsKey(_error.RecordNo) Then
                _errorList.Add(_error.RecordNo, New List(Of BTMU.Magic.FileFormat.FileFormatError))
            End If

            _errorList(_error.RecordNo).Add(_error)
        Next

        ' Assign the validation errors, error count etc

        _objPreview.PreviewDataTable.DefaultView.RowFilter = String.Empty
        dgvPreview.AutoGenerateColumns = False
        dgvPreview.DataSource = _previewDataBindingSource
        _previewDataBindingSource.DataSource = _objPreview.PreviewDataTable
        bindingSrcFileFormat.Clear()
        bindingSrcFileFormat.Add(_objPreview)

        ' #5. Apply editable fields on Preview Data

        dgvPreview.Columns.Clear()
        For Each dc As DataColumn In _objPreview.PreviewDataTable.Columns

            If _objDataModel.EditableSettings.IsFound(dc.ColumnName) Then

                'Editable Column
                If IsNothingOrEmptyString(_objPreview.TblBankFields(dc.ColumnName).DefaultValue) Then
                    'Read-Only Column
                    Dim col As New DataGridViewTextBoxColumn
                    col.ReadOnly = False
                    col.SortMode = DataGridViewColumnSortMode.NotSortable
                    col.Name = dc.ColumnName
                    col.HeaderText = dc.ColumnName
                    col.DataPropertyName = dc.ColumnName

                    dgvPreview.Columns.Add(col)
                Else

                    Dim col As New DataGridViewEditableComboBoxColumn
                    col.ReadOnly = False
                    col.SortMode = DataGridViewColumnSortMode.NotSortable
                    col.Name = dc.ColumnName
                    col.HeaderText = dc.ColumnName
                    col.DataPropertyName = dc.ColumnName
                    dgvPreview.Columns.Add(col)
                End If

            Else

                'Read-Only Column
                Dim col As New DataGridViewTextBoxColumn
                col.ReadOnly = True
                col.SortMode = DataGridViewColumnSortMode.NotSortable
                col.Name = dc.ColumnName
                col.HeaderText = dc.ColumnName
                col.DataPropertyName = dc.ColumnName
                dgvPreview.Columns.Add(col)

            End If

        Next

        ' Final Make up
        ' #6. Hide the columns which are not to be shown
        Dim colNo As Integer = 0
        For Each _bankField As MapBankField In _objDataModel.MapBankFields
            If _bankField.Detail.ToUpper = "No".ToUpper Then
                dgvPreview.Columns(colNo).Visible = False
            End If
            colNo += 1
        Next

        ' #7. Validation Error shall be hightlighted at preview GUI - set to background colour RGB(77, 236, 72)
        'its been pushed to the cellpainting method of dgvPreview


  
        If _objMasterTemplateList.OutputFormat = EPSFormat.OutputFormatString Then
            ' Right align the fields for ACNO and Amount

            ' ACNO
            ' Transaction Code
            ' Amount
            ' WHT

            dgvPreview.Columns("ACNO").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvPreview.Columns("Transaction Code").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvPreview.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvPreview.Columns("WHT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        End If

        dgvPreview.ResumeLayout()

    End Sub

     
    ''' <summary>
    ''' This function is to get Default values of Bank Field
    ''' </summary>
    ''' <param name="colName">Column having the Name of Bank Field</param>
    ''' <returns>Returns an array of string values</returns>
    ''' <remarks></remarks>
    Private Function getBankFieldDefaultValues(ByVal colName As String) As String()

        Dim bkfield As MapBankField = _objPreview.TblBankFields(colName)


        If IsNothingOrEmptyString(bkfield.DefaultValue) Then Return Nothing


        Return bkfield.DefaultValue.Split(",")

    End Function

    ''' <summary>
    ''' Highlight the cells having invalid data
    ''' </summary>
    ''' <param name="sender">Event Source</param>
    ''' <param name="e">Event Details</param>
    ''' <remarks></remarks>
    Private Sub dgvPreview_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvPreview.CellFormatting
        ' #7. Validation Error shall be hightlighted at preview GUI - set to background colour RGB(77, 236, 72)

        If _errorList.ContainsKey(e.RowIndex + 1) Then
            For Each _error As FileFormatError In _errorList(e.RowIndex + 1)
                If e.ColumnIndex = _error.ColumnOrdinalNo - 1 Then
                    e.CellStyle.BackColor = Color.Lime
                    'e.FormattingApplied = True
                Else
                    If e.CellStyle.BackColor <> Color.Lime Then
                        e.CellStyle.BackColor = Color.LightCyan
                        'e.FormattingApplied = True
                    End If

                End If
            Next
        End If


    End Sub

    'Its for highlighting those drop downs having invalid data
    Private Sub dgvPreview_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvPreview.CellPainting

        ' #7. Validation Error shall be hightlighted at preview GUI - set to background colour RGB(77, 236, 72)
        If e.ColumnIndex = -1 Or e.RowIndex = -1 Then Exit Sub
        If dgvPreview.Item(e.ColumnIndex, e.RowIndex).GetType() IsNot GetType(DataGridViewComboCell) Then Exit Sub
        If Not _errorList.ContainsKey(e.RowIndex + 1) Then Exit Sub

        For Each _error As FileFormatError In _errorList(e.RowIndex + 1)
            If e.ColumnIndex = _error.ColumnOrdinalNo - 1 Then e.CellStyle.BackColor = Color.Lime
        Next

    End Sub


    Private Sub dgvPreview_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvPreview.RowPostPaint
        Dim pID As String = ""
        Dim wID As String = ""
        Dim iID As String = ""



        If _objMasterTemplateList.OutputFormat = iFTS2MultiLineFormat.OutputFormatString Then
            Try
                Dim obj As CommonTemplateTextDetail = _objDataModel
                pID = obj.PIndicator
                wID = obj.WIndicator
                iID = obj.IIndicator
            Catch ex As InvalidCastException
                Dim obj As CommonTemplateExcelDetail = _objDataModel
                pID = obj.PIndicator
                wID = obj.WIndicator
                iID = obj.IIndicator
            End Try
          
            If "P".Equals(dgvPreview.Item(1, e.RowIndex).Value.ToString, StringComparison.InvariantCultureIgnoreCase) Then
                e.Graphics.DrawString("P", dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 40, e.RowBounds.Location.Y + 4)
            ElseIf "W".Equals(dgvPreview.Item(14, e.RowIndex).Value.ToString, StringComparison.InvariantCultureIgnoreCase) Then
                e.Graphics.DrawString("W", dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 40, e.RowBounds.Location.Y + 4)
            ElseIf "I".Equals(dgvPreview.Item(41, e.RowIndex).Value.ToString, StringComparison.InvariantCultureIgnoreCase) Then
                e.Graphics.DrawString("I", dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 40, e.RowBounds.Location.Y + 4)
            Else
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
            End If
        ElseIf _objMasterTemplateList.OutputFormat = OMAKASEFormat.OutputFormatString Then
            Try
                Dim obj As CommonTemplateTextDetail = _objDataModel
                pID = obj.PIndicator
                iID = obj.IIndicator
            Catch ex As InvalidCastException
                Dim obj As CommonTemplateExcelDetail = _objDataModel
                pID = obj.PIndicator
                iID = obj.IIndicator
            End Try

            If "P".Equals(dgvPreview.Item(13, e.RowIndex).Value.ToString, StringComparison.InvariantCultureIgnoreCase) Then
                e.Graphics.DrawString("P", dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 40, e.RowBounds.Location.Y + 4)
            ElseIf "I".Equals(dgvPreview.Item(43, e.RowIndex).Value.ToString, StringComparison.InvariantCultureIgnoreCase) Then
                e.Graphics.DrawString("I", dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 40, e.RowBounds.Location.Y + 4)
            Else
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
            End If
        Else
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), dgvPreview.Font, Brushes.Black, e.RowBounds.X + 20, e.RowBounds.Location.Y + 4)
        End If

    End Sub
    Private Sub dgvPreview_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvPreview.EditingControlShowing


        If e.Control.GetType() IsNot GetType(DataGridViewComboEditingControl) Then Exit Sub
       
        Dim freak As DataGridViewComboEditingControl = e.Control

        freak.AddItems(getBankFieldDefaultValues(dgvPreview.Columns(dgvPreview.CurrentCell.ColumnIndex).Name), True)

        'Select the Item based on the Column Name
        If freak.SelectedItemPersistent Is Nothing Then Exit Sub
        For Each item As String In freak.Items
            If item.ToString().Equals(freak.SelectedItemPersistent.ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                freak.SelectedItem = item
                Exit For
            End If
        Next
    End Sub

    Private Function CheckConsoFieldError() As Boolean
        'Check Consolidate Field is in the ErrorList or not
        Dim _colConsoField = "Consolidate Field"
        For Each ffe As FileFormatError In _objPreview.ValidationErrors
            If ffe.ColumnName.Equals(_colConsoField) Then
                Return True
            End If
        Next
        Return False

    End Function
 
 
End Class
