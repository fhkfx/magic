Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports System.Text.RegularExpressions
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports btmu.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

''' <summary>
''' User Control for GUI of Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateExcelDetail
    
#Region " Private Variables "
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    Private _templateObj As New BTMU.MAGIC.CommonTemplate.CommonTemplateExcelDetailCollection()
    Private _dataModel As BTMU.MAGIC.CommonTemplate.CommonTemplateExcelDetail
    Private _originalSourceFileName As String 'To store the original Source File Name on edit.
    Private _originalMasterTemplate As String 'To store the original Master Template on edit.
    Private SampleRowDataTable As DataTable
    Private _sampleTransactionDataTable As DataTable
    Private msgCaption As String = "Generate Common Transaction Template - Excel"
    Private dtEditableList As DataTable
    Private dtPaymentBankFields As DataTable
#End Region

#Region " Public Variables "
    ''' <summary>
    ''' Indicates whether the Template is being viewed
    ''' </summary>
    ''' <remarks></remarks>
    Public isView As Boolean = False
#End Region

#Region " Form Events "

    Private Sub ucCommonTemplateExcelDetail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateItemList()

        ' Data Binding
        bsDetailCollection.DataSource = _templateObj
        setDataSourceToBindingSource()
        ' Show the Setup tab on load
        tabDetail.SelectedIndex = 0


    End Sub

#End Region

#Region " Control Events "

#Region " Button "
    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim _rowSelected As Boolean = True
        'Is there a MapSource Field selected at the data grid on the left hand side?
        'Is there a MapBank Field selected at the datagrid on the right hand side
        If Not dgvSource.CurrentRow Is Nothing Then
            If dgvSource.CurrentRow.Index <> -1 AndAlso dgvBank.CurrentRow.Index <> -1 Then
                _rowSelected = True

                'There shall exist a Mapping between the MapSourceField and MapBankField
                If _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.IsFound(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index)) Then
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.Remove(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index))
                    dgvBank.Refresh()
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappingChanged()
                Else
                    MessageBox.Show(MsgReader.GetString("E02000420"), "Generate Common Transaction Template" _
                                               , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If
        If Not _rowSelected Then
            MessageBox.Show(MsgReader.GetString("E02000330"), "Generate Common Transaction Template" _
                                             , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAll.Click
        'Clears all mappedsourcefields from bankfields
        For Each objMapBankField As BTMU.Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            objMapBankField.MappedSourceFields.Clear()
            dgvBank.Refresh()
            objMapBankField.MappingChanged()
        Next

    End Sub

    Private Sub btnFilterUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterUp.Click
        Try
            SelectGridRow(GridRow.Up, dgvFilter, _dataModel.Filters(dgvFilter.CurrentRow.Index), _dataModel.Filters(dgvFilter.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnFilterDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterDown.Click
        Try
            SelectGridRow(GridRow.Down, dgvFilter, _dataModel.Filters(dgvFilter.CurrentRow.Index), _dataModel.Filters(dgvFilter.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnUpTranslator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpTranslator.Click

        Try
            SelectGridRow(GridRow.Up, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try


    End Sub

    Private Sub btnDownTranslator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownTranslator.Click
        Try
            SelectGridRow(GridRow.Down, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnUpEditable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpEditable.Click
        Try
            SelectGridRow(GridRow.Up, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnDownEditable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownEditable.Click
        Try
            SelectGridRow(GridRow.Down, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnLookupUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupUp.Click
        Try
            SelectGridRow(GridRow.Up, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnLookupDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupDown.Click
        Try
            SelectGridRow(GridRow.Down, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnUpCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpCalc.Click
        Try
            SelectGridRow(GridRow.Up, dgvCalc, _dataModel.CalculatedFields(dgvCalc.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalc.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnDownCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownCalc.Click
        Try
            SelectGridRow(GridRow.Down, dgvCalc, _dataModel.CalculatedFields(dgvCalc.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalc.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnUpStrManip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpStrManip.Click
        Try
            SelectGridRow(GridRow.Up, dgvStrManip, _dataModel.StringManipulations(dgvStrManip.CurrentRow.Index), _dataModel.StringManipulations(dgvStrManip.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnDownStrManip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownStrManip.Click
        Try
            SelectGridRow(GridRow.Down, dgvStrManip, _dataModel.StringManipulations(dgvStrManip.CurrentRow.Index), _dataModel.StringManipulations(dgvStrManip.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim dlgResult As DialogResult
        dlgResult = OpenFileDialog1.ShowDialog()

        If dlgResult = Windows.Forms.DialogResult.OK Then
            If _dataModel Is Nothing Then Exit Sub
            _dataModel.SampleSourceFile = OpenFileDialog1.FileName

            ErrorProvider1.SetError(txtSource, "")
            ErrorProvider1.SetError(cboWorksheet, "")
            If Not PopulateWorksheetNames() Then Exit Sub
            If _dataModel.MapSourceFields.Count = 0 Then Exit Sub
            DoFileChange()
        End If
    End Sub

    Private Sub btnRetrieve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click
        Dim isUsingRecordTypes As Boolean = False

        Try
            If _dataModel Is Nothing Then Exit Sub
            isUsingRecordTypes = IsFileFormatUsingRecordTypes()
            If Not ValidateBeforeRetrieveWorksheetData() Then Exit Sub
            If isUsingRecordTypes Then
                If Not ValidateRecordTypes Then Exit Sub
            End If
            If _dataModel.MapSourceFields.Count > 0 Then
                If MessageBox.Show(MsgReader.GetString("E10000050"), _
                                   msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = _
                        DialogResult.No Then Exit Sub
                _dataModel.Filters.Clear()
                _dataModel.MapSourceFields.Clear()
                _dataModel.MapBankFields.Clear()
                _dataModel.TranslatorSettings.Clear()
                _dataModel.EditableSettings.Clear()
                _dataModel.LookupSettings.Clear()
                _dataModel.CalculatedFields.Clear()
                _dataModel.StringManipulations.Clear()

            End If
            If isUsingRecordTypes Then
                SampleRowDataTable = _dataModel.PopulateSampleTransactionSet()
            Else
                SampleRowDataTable = _dataModel.PopulateSampleDataGrid(IsNew, True, True)
            End If

            If SampleRowDataTable.Rows.Count = 0 Then Exit Sub
            RefreshSource()
            PopulateMapBankField()

            FillEditableTable()
        Catch ex As MagicException
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(ex.Message.ToString, "")
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(String.Format("Error reading Source File : {0}", _dataModel.SampleSourceFile), ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        pnlTop.Enabled = True
        pnlFill.Enabled = True
        'pnlBottom.Enabled = True
        EnableDisableTabFields(True)
        EnableDisableRecordIndicator()
        EnableDisableAdviceRecordSetting()

        btnEdit.Enabled = False
        btnSave.Enabled = True
        btnSaveAsDraft.Enabled = True
        btnPreview.Enabled = True
        btnCancel.Enabled = True
        IsNew = False
        isView = False
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsDataSavable() Then
                'Checking duplicate template name
                If txtTemplateName.Enabled = True And _
                    (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                     File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                    MessageBox.Show(MsgReader.GetString("E10000034"), "Generate Common Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'MessageBox.Show("Template Name already exists. Please use different Template Name.", "Duplicate")
                    Exit Sub
                End If

                Dim fileToBeDeleted As String = ""
                If _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension

                _isNew = False
                _dataModel.IsDraft = False

                _dataModel.SaveToFile2(System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder, _
                _dataModel.CommonTemplateName.Trim) & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)

                If _isNew Then
                    Log_CreateCommonTemplate(frmLogin.GsUserName, _
                                System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                                            , txtTemplateName.Text) _
                                & CommonTemplateModule.CommonTemplateFileExtension)
                Else
                    Log_ModifyCommonTemplate(frmLogin.GsUserName, _
                                System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                                           , txtTemplateName.Text) _
                               & CommonTemplateModule.CommonTemplateFileExtension)
                End If


                MessageBox.Show(MsgReader.GetString("E10000074") _
                        , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

                ' Refresh the CommonTemplate in the list and Show the listview
                ContainerForm.ShowView()

            End If

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on saving Common Template : ", ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnSaveAsDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAsDraft.Click
        Try
            If Not IsSavableAsDraft() Then Exit Sub

            Dim fileToBeDeleted As String = ""
            If Not _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension

            _dataModel.IsDraft = True
            _dataModel.ApplyEdit()

            _dataModel.SaveToFile2(Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder, txtTemplateName.Text) _
            & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)

            If _isNew Then
                Log_CreateCommonTemplate(frmLogin.GsUserName, _
                            System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder _
                                        , txtTemplateName.Text) _
                            & CommonTemplateModule.CommonTemplateFileExtension)
            Else
                Log_ModifyCommonTemplate(frmLogin.GsUserName, _
                            System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder _
                                       , txtTemplateName.Text) _
                           & CommonTemplateModule.CommonTemplateFileExtension)
            End If
            _isNew = False
            MessageBox.Show(MsgReader.GetString("E10000044") _
                        , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ' Refresh the CommonTemplate in the list and Show the listview

            ContainerForm.ShowDraftView()

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on saving Common Template as Draft : ", ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If IsDataSavable() Then

            If bsOutputTemplate.Current Is Nothing Then
                Exit Sub
            End If

            Dim selMstTmp As BTMU.Magic.MasterTemplate.MasterTemplateList
            selMstTmp = bsOutputTemplate.Current

            Dim _objPreview As BaseFileFormat
            _objPreview = BaseFileFormat.CreateFileFormat(selMstTmp.OutputFormat)
            _objPreview.CommonTemplateName = _dataModel.CommonTemplateName
            _objPreview.MasterTemplateName = _dataModel.OutputTemplate
            _objPreview.SourceFileName = _dataModel.SampleSourceFile
            _objPreview.WorksheetName = _dataModel.WorksheetName
            _objPreview.TransactionEndRow = 0
            _objPreview.RemoveRowsFromEnd = 0
            _objPreview.EliminatedCharacter = ""
            _objPreview.UseValueDate = False
            _objPreview.ValueDate = Date.Today

            ' Pass the sample row as datatable to the preview screen
            frmPreview.ShowPreview(_dataModel, selMstTmp, False, Nothing, _objPreview)
            frmPreview.UcCommonTemplatePreview1.btnConsolidate.Visible = False
            frmPreview.UcCommonTemplatePreview1.btnConvert.Visible = False

        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ConfirmBeforeCloseAndCancel()
    End Sub

#End Region

#Region " CheckBox "
    Private Sub chkEnable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnable.CheckedChanged

        If chkEnable.Checked Then

            Log_EnableCommonTemplate(frmLogin.GsUserName, _
                           System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                                       , txtTemplateName.Text) _
                           & CommonTemplateModule.CommonTemplateFileExtension)

        Else

            Log_DisableCommonTemplate(frmLogin.GsUserName, _
                           System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                                       , txtTemplateName.Text) _
                           & CommonTemplateModule.CommonTemplateFileExtension)

        End If

    End Sub
#End Region

#Region " ComboBox "
    Private Sub cboOutputTemplate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTemplate.LostFocus
        'Enable Record Type GroupBox only for iFTS-2/Mr.Omakase file formats
        EnableDisableRecordIndicator()
        EnableDisableAdviceRecordSetting()  'Only for iRTMS formats (SG)
    End Sub
    Private Sub cboDateSep_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDateSep.SelectedIndexChanged
        If Not _dataModel Is Nothing Then
            If cboDateSep.Text = "No Space" Then
                _dataModel.IsZerosInDate = True
                chkZerosInDate.Enabled = False
            Else
                chkZerosInDate.Enabled = True
            End If
        End If
    End Sub

#End Region

#Region " DataGridView "
    Private Sub dgvSource_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSource.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvSource.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then dgvBank.DoDragDrop(String.Format("SourceFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvBank_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("SourceFields") Then Exit Sub

        Dim _rowIndexSourceFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtBankFieldsDataGrid As Point = dgvBank.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexBankFieldsDataGrid As Integer = dgvBank.HitTest(_dropOfPointAtBankFieldsDataGrid.X _
                                                        , _dropOfPointAtBankFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvBank_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragOver
        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("SourceFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    ' '' ''''''''''''''''''''''Drag-N-Drop from Bank Fields Grid To Map Source Fields Grid ''''''''''''''''
    Private Sub dgvBank_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBank.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvBank.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then If rowIndex > -1 Then dgvSource.DoDragDrop(String.Format("BankFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvSource_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("BankFields") Then Exit Sub

        Dim _rowIndexBankFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtSourceFieldsDataGrid As Point = dgvSource.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexSourceFieldsDataGrid As Integer = dgvSource.HitTest(_dropOfPointAtSourceFieldsDataGrid.X _
                                                        , _dropOfPointAtSourceFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvSource_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragOver

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("BankFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub dgvFilter_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFilter.CellContentClick
        Try

            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvFilter.Rows(e.RowIndex).Cells("dgvFilterbtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
            String.Format(MsgReader.GetString("E10000045") _
                    , "Filter Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.Filters.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvTranslator_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranslator.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvTranslator.Rows(e.RowIndex).Cells("dgvTranslatorbtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                    , "Translation Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.TranslatorSettings.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvEditable_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEditable.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvEditable.Rows(e.RowIndex).Cells("dgvEditablebtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                    , "Editable Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.EditableSettings.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvLookup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLookup.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvLookup.Rows(e.RowIndex).Cells("dgvLookupbtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                    , "Lookup Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.LookupSettings.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvCalc_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCalc.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvCalc.Rows(e.RowIndex).Cells("dgvCalcbtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                    , "Calculation Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.CalculatedFields.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvStrManip_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStrManip.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvStrManip.Rows(e.RowIndex).Cells("dgvStrManipbtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045") _
                    , "String Manipulation Setting", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bsDetailCollection.RaiseListChangedEvents = False
                _dataModel.StringManipulations.RemoveAt(e.RowIndex)
                bsDetailCollection.RaiseListChangedEvents = True
                bsDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Fillup the BankField of TranslationSetting with the default values of BankField
    Private Sub dgvTranslator_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTranslator.EditingControlShowing

        If ((Not (e.Control.GetType() Is GetType(DataGridViewComboEditingControl))) _
            Or (dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value Is Nothing)) Then Exit Sub


        For Each _bkfield As MapBankField In _dataModel.MapBankFields
            If _bkfield.BankField = dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value.ToString() Then

                If (_bkfield.DefaultValue <> String.Empty) Then

                    Dim freak As DataGridViewComboEditingControl = e.Control
                    freak.AddItems(_bkfield.DefaultValue.Split(","), True)  'an empty blank value is also displayed 

                End If

                Exit For
            End If
        Next
    End Sub

    'Query DB and fillup the Kookup Key and Value columns
    Private Sub dgvLookup_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvLookup.EditingControlShowing

        If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupKeyCombo" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupValueCombo" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupKeyCombo2" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupValueCombo2" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupKeyCombo3" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupLookupValueCombo3" _
        Then Exit Sub

        Try

            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo" Then
                If dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo2" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo2" Then
                If dgvLookup.Item(6, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo3" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo3" Then
                If dgvLookup.Item(9, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If

            Dim freak As DataGridViewComboEditingControl = e.Control
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo2" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo2" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(6, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupKeyCombo3" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupLookupValueCombo3" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(9, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If

            'Just in case the First Row of Tablexx has been changed
            'combobox Item Cxx-XXX would not be valid 
            'Work Around: Select the Item based on the Column Name
            If freak.SelectedItemPersistent Is Nothing Then Exit Sub
            For Each item As String In freak.Items
                If item.ToString().StartsWith(freak.SelectedItemPersistent.ToString().Split("-")(0)) Then
                    freak.SelectedItem = item
                    Exit For
                End If
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvStrManip_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStrManip.DataError
        If e.ColumnIndex = 3 Or e.ColumnIndex = 4 Then
            MessageBox.Show(MsgReader.GetString("E10000053"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region " RadioButton "
    Private Sub rdoDollars_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoDollars.CheckedChanged
        If rdoDollars.Checked Then
            _dataModel.RemittanceAmount = "Dollar"
        End If
    End Sub

    Private Sub rdoCents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoCents.CheckedChanged
        If rdoCents.Checked Then
            _dataModel.RemittanceAmount = "Cent"
        End If
    End Sub

    Private Sub rdoFilterAND_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoFilterAND.CheckedChanged
        If rdoFilterAND.Checked Then
            _dataModel.FilterCondition = "AND"
        End If
    End Sub

    Private Sub rdoFilterOR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoFilterOR.CheckedChanged
        If rdoFilterOR.Checked Then
            _dataModel.FilterCondition = "OR"
        End If
    End Sub

    Private Sub rdoAdviceForEveryRow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAdviceForEveryRow.CheckedChanged
        If rdoAdviceForEveryRow.Checked Then
            _dataModel.AdviceRecord = "EveryRow"
            _dataModel.AdviceRecChar = ""
            _dataModel.AdviceRecDelimiter = ""
            txtAdviceChar.Enabled = False
            txtAdviceDelimiter.Enabled = False
            ErrorProvider1.SetError(txtAdviceChar, "")
            ErrorProvider1.SetError(txtAdviceDelimiter, "")
        End If
    End Sub

    Private Sub rdoAdviceAftEveryChar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAdviceAftEveryChar.CheckedChanged
        If rdoAdviceAftEveryChar.Checked Then
            _dataModel.AdviceRecord = "EveryChar"
            _dataModel.AdviceRecDelimiter = ""
            txtAdviceChar.Enabled = True
            txtAdviceDelimiter.Enabled = False
            ErrorProvider1.SetError(txtAdviceChar, "")
            ErrorProvider1.SetError(txtAdviceDelimiter, "")
        End If
    End Sub

    Private Sub rdoAdviceDelimited_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAdviceDelimited.CheckedChanged
        If rdoAdviceDelimited.Checked Then
            _dataModel.AdviceRecord = "Delimiter"
            _dataModel.AdviceRecChar = ""
            txtAdviceChar.Enabled = False
            txtAdviceDelimiter.Enabled = True
            ErrorProvider1.SetError(txtAdviceChar, "")
            ErrorProvider1.SetError(txtAdviceDelimiter, "")
        End If
    End Sub
#End Region

#Region " Tab "
    Private Sub tabDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabDetail.SelectedIndexChanged
        If tabDetail.SelectedIndex = 3 Then
            If System.IO.File.Exists(frmExcel2DB.DatabasePath) Then
                ' Enable the grid
                dgvLookup.Enabled = True
            Else
                'disable the grid
                dgvLookup.Enabled = False
            End If
        End If
    End Sub
#End Region

#Region " TextBox "
    Private Sub txtSource_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSource.DragDrop
        Try
            Dim filename As String = ""
            Dim Filenames As String()
            txtSource.Focus()
            Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
            For Each file As String In Filenames
                If System.IO.File.Exists(file) Then
                    filename = file
                    Exit For
                End If
            Next

            If Not ExcelReaderInterop.IsValidExcelFile(filename) Then Exit Sub
            'If Not (filename.EndsWith(".xls") Or filename.EndsWith(".xlsx")) Then Exit Sub
            If (Not (_dataModel Is Nothing)) AndAlso filename <> "" Then
                _dataModel.SampleSourceFile = filename
                If Not PopulateWorksheetNames() Then Exit Sub
                If _dataModel.MapSourceFields.Count = 0 Then Exit Sub
                DoFileChange()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtSource_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSource.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.All
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    ''' <summary>
    ''' Used to limit the characters entered in the 'Start Column' textbox to "A-Z", "a-z"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtStartCol_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartCol.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
           Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
           Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub

    ''' <summary>
    ''' Used to limit the characters entered in the 'End Column' textbox to "A-Z","a-z"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtEndCol_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndCol.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
                   Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
                   Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' Used to limit the characters entered in the 'P Column' textbox to "A-Z","a-z"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub PColumnTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PColumnTextBox.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
                           Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
                           Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' Used to limit the characters entered in the 'W Column' textbox to "A-Z","a-z"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub WColumnTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles WColumnTextBox.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
                           Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
                           Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' Used to limit the characters entered in the 'I Column' textbox to "A-Z","a-z"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub IColumnTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles IColumnTextBox.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
                           Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
                           Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
#End Region

#End Region

#Region " Private Methods "

    ''' <summary>
    ''' Prepares the GUI and Initializes the form for adding data to New Template
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AddNew()
        _templateObj.Clear()
        _dataModel = New BTMU.Magic.CommonTemplate.CommonTemplateExcelDetail
        _templateObj.Add(_dataModel)
        FillEditableTable()
        InitializeForNewTemplate()
        IsNew = True
        isView = False
    End Sub

    Private Sub ClearCollection()
        _dataModel.Filters.Clear()
        _dataModel.MapSourceFields.Clear()
        _dataModel.MapBankFields.Clear()
        _dataModel.TranslatorSettings.Clear()
        _dataModel.EditableSettings.Clear()
        _dataModel.LookupSettings.Clear()
        _dataModel.CalculatedFields.Clear()
        _dataModel.StringManipulations.Clear()
    End Sub

    Private Sub DoFileChange()
        Dim isUsingRecordTypes As Boolean = False
        Try
            Dim hasMoreFields As Boolean = False
            isUsingRecordTypes = IsFileFormatUsingRecordTypes()
            If Not ValidateBeforeRetrieveWorksheetData() Then Exit Sub
            If isUsingRecordTypes Then
                If Not ValidateRecordTypes() Then Exit Sub
            End If
            FillEditableTable()
            If isUsingRecordTypes Then
                SampleRowDataTable = _dataModel.PopulateSampleTransactionSet()
            Else
                SampleRowDataTable = _dataModel.PopulateSampleDataGrid(IsNew, True, True)
            End If

            ' Get only the Sample Row to show in the Sample DataGrid
            If SampleRowDataTable.Rows.Count = 0 Then Exit Sub
            If _dataModel.MapSourceFields.Count = 0 Then
                RefreshSource()
                PopulateMapBankField()
                Exit Sub
            End If



            If AllowSampleFileChange() Then
                RefreshSource()
                If _dataModel.MapBankFields.Count = 0 Then
                    PopulateMapBankField()
                    Exit Sub
                End If
                For Each bankField As MapBankField In _dataModel.MapBankFields
                    If bankField.MappedSourceFields.Count > 0 Then
                        ' Refresh the Mapped SourceField Values
                        For Each mapField As MapSourceField In bankField.MappedSourceFields
                            If Not IsNothingOrEmptyString(mapField.SourceFieldName) Then
                                mapField.SourceFieldValue = GetMappedSourceFieldValue(mapField.SourceFieldName)
                            End If
                        Next
                    End If
                Next


            Else
                If IsNew Then
                    If Not IsNothingOrEmptyString(_originalSourceFileName) Then DuplicateExisting(_originalSourceFileName)
                Else
                    EditExisting(_originalSourceFileName)
                End If

            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error : ", ex.Message.ToString)
        End Try

    End Sub

    Private Sub DoMapping(ByVal _rowIndexSourceFieldsDataGrid As Int32, ByVal _rowIndexBankFieldsDataGrid As Int32)

        If _rowIndexBankFieldsDataGrid = -1 Or _rowIndexSourceFieldsDataGrid = -1 Then Exit Sub

        Dim _objMapSourceField As BTMU.Magic.CommonTemplate.MapSourceField
        Dim _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField

        Try

            If Not _dataModel.MapBankFields Is Nothing Then
                _objMapBankField = _dataModel.MapBankFields(_rowIndexBankFieldsDataGrid)

                If Not _dataModel.MapSourceFields Is Nothing Then
                    _objMapSourceField = _dataModel.MapSourceFields(_rowIndexSourceFieldsDataGrid)
                    Dim cloneSourceField As New BTMU.Magic.CommonTemplate.MapSourceField
                    cloneSourceField.SourceFieldName = _objMapSourceField.SourceFieldName
                    cloneSourceField.SourceFieldValue = _objMapSourceField.SourceFieldValue
                    '#### Need to check if the same mapping exists
                    If _objMapBankField.MappedSourceFields.IsFound(_objMapSourceField) Then
                        MessageBox.Show(MsgReader.GetString("E02000340"), "Generate Common Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        'MessageBox.Show("The Field has already been mapped.", "Generate Common Transaction Template" _
                        '             , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ElseIf Not VerifyCrossMapping(cloneSourceField.SourceFieldName, _objMapBankField) Then
                        If getOutputFileFormat() = OMAKASEFormat.OutputFormatString Then
                            MessageBox.Show(MsgReader.GetString("E02010121"), msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ElseIf getOutputFileFormat() = iFTS2MultiLineFormat.OutputFormatString Then
                            MessageBox.Show(MsgReader.GetString("E02010120"), msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If

                    ElseIf ValidateTypeOfMappedFields(_objMapBankField, cloneSourceField) Then
                        _objMapBankField.MappedSourceFields.Add(cloneSourceField)
                        _objMapBankField.MappingChanged()
                        dgvBank.Refresh()

                        For Each setting As TranslatorSetting In _dataModel.TranslatorSettings
                            If setting.BankFieldName = _objMapBankField.BankField AndAlso HelperModule.IsNothingOrEmptyString(setting.BankValue) AndAlso HelperModule.IsNothingOrEmptyString(setting.CustomerValue) AndAlso setting.BankValueEmpty = False Then
                                setting.SetDefaultCustomerValue(setting.BankFieldName)
                            End If
                        Next

                    End If

                End If

            End If

        Catch ex As Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error while Mapping : ", ex.Message)

        End Try

    End Sub

    ''' <summary>
    ''' Duplicates the Template
    ''' </summary>
    ''' <param name="commonTemplateFile">Common Template</param>
    ''' <remarks></remarks>
    Public Sub DuplicateExisting(ByVal commonTemplateFile As String)

        If commonTemplateFile = String.Empty Then
            ContainerForm.ShowView()
            Exit Sub
        End If

        _isNew = True
        _originalSourceFileName = commonTemplateFile

        Try
            _templateObj.Clear()
            'No selected item in drop down lists
            cboOutputTemplate.SelectedIndex = -1
            cboDateSep.SelectedIndex = -1
            cboDateType.SelectedIndex = -1
            cboDecimalSep.SelectedIndex = -1
            cboThousandSep.SelectedIndex = -1

            _dataModel = (New Magic.CommonTemplate.CommonTemplateExcelDetail).LoadFromFile2(commonTemplateFile)
            _dataModel.CommonTemplateName &= " Copy"
            _templateObj.Add(_dataModel)
            _dataModel.Validate()

            EnableDisableTabFields(True)
            EnableDisableRecordIndicator()
            EnableDisableAdviceRecordSetting()

            btnEdit.Enabled = False
            btnSave.Enabled = True
            btnSaveAsDraft.Enabled = True
            btnPreview.Enabled = True
            btnCancel.Enabled = True
            PrepareForLoad()
            _originalMasterTemplate = _dataModel.OutputTemplate
            ContainerForm.ShowDetail()
            txtTemplateName.Enabled = True
            chkEnable.Enabled = True
            cboOutputTemplate.Enabled = True
            ErrorProvider1.Clear()
            FillEditableTable()
            setDataSourceToBindingSource()
            SampleRowDataTable = New DataTable
            Dim sampleDataRow As DataRow
            Dim colNo As Integer
            colNo = 1
            For Each sourcefield As MapSourceField In _dataModel.MapSourceFields
                If IsNothingOrEmptyString(sourcefield.SourceFieldName) Then
                    SampleRowDataTable.Columns.Add("Field" & colNo.ToString, GetType(String))
                Else
                    SampleRowDataTable.Columns.Add(sourcefield.SourceFieldName)
                End If
                colNo += 1
            Next
            colNo = 0
            sampleDataRow = SampleRowDataTable.NewRow
            For Each sourcefield As MapSourceField In _dataModel.MapSourceFields

                If IsNothingOrEmptyString(sourcefield.SourceFieldValue) Then
                    sampleDataRow(colNo) = ""
                Else
                    sampleDataRow(colNo) = sourcefield.SourceFieldValue
                End If
                colNo += 1
            Next
            SampleRowDataTable.Rows.Add(sampleDataRow)
            dgvSampleData.DataSource = SampleRowDataTable


        Catch ex As System.Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error Loading " & commonTemplateFile, ex.Message)
            ContainerForm.ShowView()
        End Try

    End Sub

    ''' <summary>
    ''' Prepares the GUI to Edit the given template 
    ''' </summary>
    ''' <param name="commonTemplateFile">Common Template</param>
    ''' <remarks></remarks>
    Public Sub EditExisting(ByVal commonTemplateFile As String)

        If commonTemplateFile = String.Empty Then
            ContainerForm.ShowView()
            Exit Sub
        End If
        IsNew = False
        isView = False
        _originalSourceFileName = commonTemplateFile


        Try
            _templateObj.Clear()
            'No selected item in drop down lists
            cboOutputTemplate.SelectedIndex = -1
            cboDateSep.SelectedIndex = -1
            cboDateType.SelectedIndex = -1
            cboDecimalSep.SelectedIndex = -1
            cboThousandSep.SelectedIndex = -1



            _dataModel = (New Magic.CommonTemplate.CommonTemplateExcelDetail).LoadFromFile2(commonTemplateFile)
            _templateObj.Add(_dataModel)
            _dataModel.Validate()

            EnableDisableTabFields(True)
            EnableDisableRecordIndicator()
            EnableDisableAdviceRecordSetting()

            btnEdit.Enabled = False
            btnSave.Enabled = True
            btnSaveAsDraft.Enabled = True
            btnPreview.Enabled = True
            btnCancel.Enabled = True
            PrepareForLoad()
            _originalMasterTemplate = _dataModel.OutputTemplate

            ' Do not allow the Common Template Name to be edited
            txtTemplateName.Enabled = False
            chkEnable.Enabled = True
            cboOutputTemplate.Enabled = False
            ErrorProvider1.Clear()
            FillEditableTable()
            setDataSourceToBindingSource()
            tabDetail.SelectedIndex = 0

            SampleRowDataTable = New DataTable
            Dim sampleDataRow As DataRow
            Dim colNo As Integer
            colNo = 1
            For Each sourcefield As MapSourceField In _dataModel.MapSourceFields
                If IsNothingOrEmptyString(sourcefield.SourceFieldName) Then
                    SampleRowDataTable.Columns.Add("Field" & colNo.ToString, GetType(String))
                Else
                    SampleRowDataTable.Columns.Add(sourcefield.SourceFieldName)
                End If
                colNo += 1
            Next
            colNo = 0
            sampleDataRow = SampleRowDataTable.NewRow
            For Each sourcefield As MapSourceField In _dataModel.MapSourceFields

                If IsNothingOrEmptyString(sourcefield.SourceFieldValue) Then
                    sampleDataRow(colNo) = ""
                Else
                    sampleDataRow(colNo) = sourcefield.SourceFieldValue
                End If
                colNo += 1
            Next
            SampleRowDataTable.Rows.Add(sampleDataRow)
            dgvSampleData.DataSource = SampleRowDataTable

            'If _dataModel.Worksheets.Count = 0 Then
            Dim worksheetObj As ExcelWorksheet
            If System.IO.File.Exists(_dataModel.SampleSourceFile) Then
                Dim _worksheetDataTable As DataTable
                Try
                    _worksheetDataTable = ExcelHelperModule.GetWorksheetNames(_dataModel.SampleSourceFile)
                Catch ex As Exception
                    MessageBox.Show(ex.Message.ToString)
                    Exit Sub
                Finally
                    'FillEditableTable()
                End Try

                If _worksheetDataTable Is Nothing Then Exit Sub
                _dataModel.Worksheets.Clear()
                For Each _worksheet As DataRow In _worksheetDataTable.Rows
                    worksheetObj = New ExcelWorksheet
                    worksheetObj.ReadableWorksheetName = _worksheet("WorksheetName").ToString
                    worksheetObj.WorksheetName = _worksheet("WorksheetName").ToString
                    _dataModel.Worksheets.Add(worksheetObj)
                Next

                ''Modified by swamy on 12 Oct 2009
                'If _dataModel.Worksheets.Count > 0 AndAlso _dataModel.WorksheetName = String.Empty Then
                '    _dataModel.WorksheetName = _dataModel.Worksheets.Item(0).WorksheetName
                'End If 'Added by Swamy on 12 Oct 2009
                cboWorksheet.SelectedValue = _dataModel.WorksheetName
            Else
                If Not IsNothingOrEmptyString(_dataModel.WorksheetName) Then
                    worksheetObj = New ExcelWorksheet
                    worksheetObj.WorksheetName = _dataModel.WorksheetName
                    worksheetObj.ReadableWorksheetName = _dataModel.WorksheetName
                    _dataModel.Worksheets.Add(worksheetObj)
                    cboWorksheet.SelectedValue = (_dataModel.WorksheetName)
                End If
            End If

            'End If

            'FillEditableTable()

            
        Catch ex As MagicException
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(ex.Message.ToString, "")
        Catch ex As System.Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error Loading " & commonTemplateFile, ex.Message)
            ContainerForm.ShowView()
        Finally
            'FillEditableTable()
        End Try

    End Sub

    ''' <summary>
    ''' This procedure is used to enable or disable the 'Record Type Indicator' Groupbox.
    ''' The 'Record Type Indicator' Groupbox is enabled only for iFTS-2 and Mr. Omakase India file formats.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub EnableDisableRecordIndicator()
        If IsFileFormatUsingRecordTypes() Then
            RecordTypeIndicatorGroupBox.Enabled = True
            chkHeaderAsFieldName.Enabled = False
        Else
            RecordTypeIndicatorGroupBox.Enabled = False
            chkHeaderAsFieldName.Enabled = True
        End If
    End Sub

    '##########################################
    '# Set Enabled=True only for iRTMS format #
    '##########################################
    Private Sub EnableDisableAdviceRecordSetting()
        Dim FileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList

        If bsOutputTemplate.Current Is Nothing Then Exit Sub
        FileFormat = bsOutputTemplate.Current

        Select Case FileFormat.OutputFormat
            Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM", "OiRTMSCI", "OiRTMSGC", "OiRTMSGD", "OiRTMSRM"
                grpDetailAdviceSettings.Enabled = True
            Case Else
                grpDetailAdviceSettings.Enabled = False
        End Select

    End Sub

    ''' <summary>
    ''' Enables/Disables Form fields
    ''' </summary>
    ''' <param name="isEnabled">Boolean value indicating whether the form fields need to be enabled/disabled</param>
    ''' <remarks></remarks>
    Private Sub EnableDisableTabFields(ByVal isEnabled As Boolean)
        pnlTop.Enabled = isEnabled
        'pnlFill.Enabled = Not isEnabled
        ' Disable all the fields
        'pnlSetupTop.Enabled = isEnabled
        txtSource.Enabled = isEnabled
        btnBrowse.Enabled = isEnabled
        'cboWorksheet.Enabled = isEnabled
        'txtStartCol.Enabled = isEnabled
        'txtEndCol.Enabled = isEnabled
        'txtSampleRowNo.Enabled = isEnabled
        'txtHeaderAtRow.Enabled = isEnabled
        'txtTransactionStartRow.Enabled = isEnabled
        'chkHeaderAsFieldName.Enabled = isEnabled
        'btnRetrieve.Enabled = isEnabled
        ExcelSettingGroupBox.Enabled = isEnabled
        FieldSettingGroupBox.Enabled = isEnabled
        pnlSetupBottom.Enabled = isEnabled
        dgvSource.Enabled = isEnabled
        dgvBank.Enabled = isEnabled
        btnClear.Enabled = isEnabled
        btnClearAll.Enabled = isEnabled
        TranslationGroupBox.Enabled = isEnabled
        EditableGroupBox.Enabled = isEnabled
        LookupGroupBox.Enabled = isEnabled
        CalculationGroupBox.Enabled = isEnabled
        StringManipulationGroupBox.Enabled = isEnabled
        RecordTypeIndicatorGroupBox.Enabled = isEnabled
    End Sub

    ''' <summary>
    ''' Fills the Editable Settings Grid with the list of Bank Fields that can be edited
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditableTable()

        Dim _cnt As Int32 = 0

        Dim currentDetail As CommonTemplateExcelDetail = bsDetailCollection.Current
        Dim currentMaster As MasterTemplateList = bsOutputTemplate.Current


        If dtEditableList Is Nothing Then
            dtEditableList = New DataTable
            dtEditableList.Columns.Add("Field1", GetType(String))
        End If
        dtEditableList.Rows.Clear()

        If dtPaymentBankFields Is Nothing Then
            dtPaymentBankFields = New DataTable
            dtPaymentBankFields.Columns.Add("BankField", GetType(String))
        End If
        dtPaymentBankFields.Rows.Clear()


        If currentDetail IsNot Nothing AndAlso currentMaster IsNot Nothing Then
            '1. Editable Bank Fields
            For Each temp As String In BaseFileFormat.GetEditableFields(currentMaster.OutputFormat)
                dtEditableList.Rows.Add(temp)
            Next

            '2. Lookup and Calculation Bank Fields
            'For iFTS-2 Multiline and OMAKASE output file format
            'Look up and Calculation Settings shall have nothing but "Payment Bank Fields"
            If currentMaster.OutputFormat = iFTS2MultiLineFormat.OutputFormatString _
                OrElse currentMaster.OutputFormat = OMAKASEFormat.OutputFormatString Then

                If currentMaster.OutputFormat = iFTS2MultiLineFormat.OutputFormatString Then
                    If currentDetail.MapBankFields.Count > 14 Then
                        For _cnt = 1 To 13
                            dtPaymentBankFields.Rows.Add(currentDetail.MapBankFields(_cnt).BankField)
                        Next
                    End If
                ElseIf currentMaster.OutputFormat = OMAKASEFormat.OutputFormatString Then
                    If currentDetail.MapBankFields.Count > 34 Then
                        For _cnt = 13 To 42
                            dtPaymentBankFields.Rows.Add(currentDetail.MapBankFields(_cnt).BankField)
                        Next
                    End If
                End If
                bsLookupBankField.DataSource = dtPaymentBankFields
                dgvLookupBankFieldNameCombo.DisplayMember = "BankField"
                dgvLookupBankFieldNameCombo.ValueMember = "BankField"
                dgvLookupBankFieldNameCombo.DataSource = bsLookupBankField

                bsCalcBankField.DataSource = dtPaymentBankFields
                dgvCalcBankFieldNameCombo.DisplayMember = "BankField"
                dgvCalcBankFieldNameCombo.ValueMember = "BankField"
                dgvCalcBankFieldNameCombo.DataSource = bsCalcBankField
            Else

                bsLookupBankField.DataSource = bsDetailCollection
                bsLookupBankField.DataMember = "MapBankFields"
                dgvLookupBankFieldNameCombo.DisplayMember = "BankField"
                dgvLookupBankFieldNameCombo.ValueMember = "BankField"
                dgvLookupBankFieldNameCombo.DataPropertyName = "BankFieldName"
                dgvLookupBankFieldNameCombo.DataSource = bsLookupBankField

                bsCalcBankField.DataSource = bsDetailCollection
                bsCalcBankField.DataMember = "MapBankFields"
                dgvCalcBankFieldNameCombo.DisplayMember = "BankField"
                dgvCalcBankFieldNameCombo.ValueMember = "BankField"
                dgvCalcBankFieldNameCombo.DataPropertyName = "BankFieldName"
                dgvCalcBankFieldNameCombo.DataSource = bsCalcBankField
            End If

        End If

        bsEditableBankField.DataSource = dtEditableList
        dgvEditableBankFieldNameCombo.DisplayMember = "Field1"
        dgvEditableBankFieldNameCombo.ValueMember = "Field1"
        dgvEditableBankFieldNameCombo.DataSource = bsEditableBankField



    End Sub

    ''' <summary>
    ''' Initializes the Template
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InitializeForNewTemplate()
        Try
            rdoDollars.Checked = True
            rdoFilterAND.Checked = True
            rdoAdviceForEveryRow.Checked = True
            _dataModel.IsEnabled = True

            'No selected item in drop down lists
            cboOutputTemplate.SelectedIndex = -1
            cboDateSep.SelectedIndex = -1
            cboDateType.SelectedIndex = -1
            cboDecimalSep.SelectedIndex = -1
            cboThousandSep.SelectedIndex = -1

            ' Clear the Data Sample Grid
            dgvSampleData.DataSource = Nothing

            EnableDisableTabFields(True)
            RecordTypeIndicatorGroupBox.Enabled = True
            chkHeaderAsFieldName.Enabled = True


            btnEdit.Enabled = False
            btnSave.Enabled = True
            btnSaveAsDraft.Enabled = True
            btnPreview.Enabled = True
            btnCancel.Enabled = True
            txtTemplateName.Enabled = True
            chkEnable.Enabled = True
            cboOutputTemplate.Enabled = True
            ErrorProvider1.Clear()
            _originalSourceFileName = ""
            _originalMasterTemplate = ""
            setDataSourceToBindingSource()
            dtEditableList.Rows.Clear()

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error in creating new Common Template : ", ex.Message)
        End Try

    End Sub

    Private Sub PopulateMapBankField()
        Try
            If Not bsOutputTemplate.Current Is Nothing Then
                Dim selMstTmp As BTMU.Magic.MasterTemplate.MasterTemplateList
                Dim _bankField As BTMU.Magic.CommonTemplate.MapBankField
                selMstTmp = bsOutputTemplate.Current
                Dim _mstTmpUtility As New MasterTemplateSharedFunction
                If selMstTmp.IsFixed Then
                    Dim _objTmp As New BTMU.Magic.MasterTemplate.MasterTemplateFixed

                    _objTmp = _objTmp.LoadFromFile2(String.Format("{0}{1}", _
                                     Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, cboOutputTemplate.Text) _
                                      , _mstTmpUtility.MasterTemplateFileExtension))

                    _dataModel.MapBankFields.Clear()
                    For Each _masterFixedDetail As BTMU.Magic.MasterTemplate.MasterTemplateFixedDetail In _objTmp.MasterTemplateFixedDetailCollection
                        _bankField = New BTMU.Magic.CommonTemplate.MapBankField()
                        _bankField.Position = _masterFixedDetail.Position
                        _bankField.BankField = _masterFixedDetail.FieldName
                        '_bankField.CarriageReturn = _masterFixedDetail.CarriageReturn
                        If IsNothing(_masterFixedDetail.Mandatory) Then
                            _bankField.Mandatory = False
                        Else

                            _bankField.Mandatory = _masterFixedDetail.Mandatory
                        End If

                        _bankField.Header = _masterFixedDetail.Header

                        If _masterFixedDetail.Detail Is Nothing Then
                            _bankField.Detail = "No"
                        Else
                            _bankField.Detail = _masterFixedDetail.Detail
                        End If

                        _bankField.Trailer = _masterFixedDetail.Trailer




                        If _masterFixedDetail.MapSeparator Is Nothing Then
                            _bankField.MapSeparator = ""
                        Else
                            _bankField.MapSeparator = _masterFixedDetail.MapSeparator
                        End If

                        If _masterFixedDetail.DataSample Is Nothing Then
                            _bankField.BankSampleValue = ""
                        Else
                            _bankField.BankSampleValue = _masterFixedDetail.DataSample
                        End If

                        If _masterFixedDetail.DataType Is Nothing Then
                            _bankField.DataType = ""
                        Else
                            _bankField.DataType = _masterFixedDetail.DataType
                        End If
                        If _masterFixedDetail.DateFormat Is Nothing Then
                            _bankField.DateFormat = ""
                        Else
                            _bankField.DateFormat = _masterFixedDetail.DateFormat
                        End If
                        If _masterFixedDetail.DataLength.HasValue Then
                            _bankField.DataLength = _masterFixedDetail.DataLength
                        Else
                            _bankField.DataLength = 0
                        End If
                        If _masterFixedDetail.DefaultValue Is Nothing Then
                            _bankField.DefaultValue = ""
                        Else
                            _bankField.DefaultValue = _masterFixedDetail.DefaultValue
                        End If
                        If _masterFixedDetail.IncludeDecimal Is Nothing Then
                            _bankField.IncludeDecimal = " "
                        Else
                            _bankField.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                        End If

                        If _masterFixedDetail.DecimalSeparator Is Nothing Then
                            _bankField.DecimalSeparator = ""
                        Else
                            _bankField.DecimalSeparator = _masterFixedDetail.DecimalSeparator
                        End If


                        _dataModel.MapBankFields.Add(_bankField)
                    Next

                Else
                    Dim _objTmp As New BTMU.Magic.MasterTemplate.MasterTemplateNonFixed

                    _objTmp = _objTmp.LoadFromFile2(String.Format("{0}{1}", _
                                     Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, cboOutputTemplate.Text) _
                                      , _mstTmpUtility.MasterTemplateFileExtension))

                    If Not _dataModel Is Nothing Then
                        _dataModel.MapBankFields.Clear()

                        For Each _masterNonFixedDetail As BTMU.Magic.MasterTemplate.MasterTemplateNonFixedDetail In _objTmp.MasterTemplateNonFixedDetailCollection
                            _bankField = New BTMU.Magic.CommonTemplate.MapBankField()
                            _bankField.Position = _masterNonFixedDetail.Position
                            _bankField.BankField = _masterNonFixedDetail.FieldName
                            '_bankField.CarriageReturn = _masterNonFixedDetail.CarriageReturn
                            If IsNothing(_masterNonFixedDetail.Mandatory) Then
                                _bankField.Mandatory = False
                            Else

                                _bankField.Mandatory = _masterNonFixedDetail.Mandatory
                            End If
                            If IsNothing(_masterNonFixedDetail.Header) Then
                                _bankField.Header = False
                            Else
                                _bankField.Header = _masterNonFixedDetail.Header
                            End If
                            If _masterNonFixedDetail.Detail Is Nothing Then
                                _bankField.Detail = "No"
                            Else
                                _bankField.Detail = _masterNonFixedDetail.Detail
                            End If
                            If IsNothing(_masterNonFixedDetail.Trailer) Then
                                _bankField.Trailer = False
                            Else
                                _bankField.Trailer = _masterNonFixedDetail.Trailer
                            End If

                            If _masterNonFixedDetail.MapSeparator Is Nothing Then
                                _bankField.MapSeparator = ""
                            Else
                                _bankField.MapSeparator = _masterNonFixedDetail.MapSeparator
                            End If

                            If _masterNonFixedDetail.DataSample Is Nothing Then
                                _bankField.BankSampleValue = ""
                            Else
                                _bankField.BankSampleValue = _masterNonFixedDetail.DataSample
                            End If

                            If _masterNonFixedDetail.DataType Is Nothing Then
                                _bankField.DataType = ""
                            Else
                                _bankField.DataType = _masterNonFixedDetail.DataType
                            End If
                            If _masterNonFixedDetail.DateFormat Is Nothing Then
                                _bankField.DateFormat = ""
                            Else
                                _bankField.DateFormat = _masterNonFixedDetail.DateFormat
                            End If
                            If _masterNonFixedDetail.DataLength.HasValue Then
                                _bankField.DataLength = _masterNonFixedDetail.DataLength
                            Else
                                _bankField.DataLength = 0
                            End If
                            If _masterNonFixedDetail.DefaultValue Is Nothing Then
                                _bankField.DefaultValue = ""
                            Else
                                _bankField.DefaultValue = _masterNonFixedDetail.DefaultValue
                            End If

                            _dataModel.MapBankFields.Add(_bankField)
                            bsTranslatorCollection.ResetBindings(False)
                        Next

                    End If

                End If

            End If
            'Add bank fields with default values to Translation
            bsTranslatorCollection.RaiseListChangedEvents = False
            For Each _bankField As MapBankField In _dataModel.MapBankFields

                If _bankField.DefaultValue Is Nothing Or _bankField.DefaultValue = String.Empty Then Continue For

                Dim bnkfieldTranslated As New TranslatorSetting()
                bnkfieldTranslated.BankFieldName = _bankField.BankField
                'bnkfieldTranslated.DefaultValues = _bankField.DefaultValue
                _dataModel.TranslatorSettings.Add(bnkfieldTranslated)

            Next

            bsTranslatorCollection.RaiseListChangedEvents = True
            bsTranslatorCollection.ResetBindings(False)

            _dataModel.SetDefaultTranslation()

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub PopulateItemList()
        Try
            ' Load the Default list of values in comboboxes
            bsOutputTemplate.DataSource = CommonTemplateModule.AllMasterTemplates
            cboDateSep.DataSource = CommonTemplateModule.DateSeparator
            cboDateType.DataSource = CommonTemplateModule.DateType
            cboDecimalSep.DataSource = CommonTemplateModule.DecimalSeparator
            cboThousandSep.DataSource = CommonTemplateModule.ThousandSeparator
            dgvFilterFilterOperatorCombo.DataSource = CommonTemplateModule.FilterOperator
            dgvLookupTableNameCombo.DataSource = CommonTemplateModule.LookupTables
            dgvLookupTableNameCombo2.DataSource = CommonTemplateModule.LookupTables
            dgvLookupTableNameCombo3.DataSource = CommonTemplateModule.LookupTables
            dgvCalOperator1Combo.DataSource = CommonTemplateModule.Operators1
            dgvCalcOperator2Combo.DataSource = CommonTemplateModule.Operators2
            dgvStrManipFunctionNameCombo.DataSource = CommonTemplateModule.StringFunctions
            dgvFilterCellFilterTypeComboBox.DataSource = CommonTemplateModule.FilterDataType

            dgvFilter.EditMode = DataGridViewEditMode.EditOnEnter
            dgvTranslator.EditMode = DataGridViewEditMode.EditOnEnter
            dgvLookup.EditMode = DataGridViewEditMode.EditOnEnter
            dgvEditable.EditMode = DataGridViewEditMode.EditOnEnter
            dgvCalc.EditMode = DataGridViewEditMode.EditOnEnter
            dgvStrManip.EditMode = DataGridViewEditMode.EditOnEnter
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on loading : ", ex.Message.ToString)
        End Try
    End Sub

    Private Sub PopulateReferenceFieldCollection(ByVal mapSourceField As Magic.CommonTemplate.MapSourceFieldCollection)
        Dim sourceFieldDataTable As New DataTable
        Dim _curMapSourceField As BTMU.Magic.CommonTemplate.MapSourceField
        With sourceFieldDataTable
            .Columns.Add("SourceFieldName")
            .Rows.Add("")

            For Each _curMapSourceField In mapSourceField
                .Rows.Add(_curMapSourceField.SourceFieldName)
            Next
        End With
        bsRefField1Collection.DataSource = sourceFieldDataTable
        bsRefField2Collection.DataSource = sourceFieldDataTable
        bsRefField3Collection.DataSource = sourceFieldDataTable

        With cboRef1
            .DataSource = bsRefField1Collection
            .DisplayMember = sourceFieldDataTable.Columns(0).ColumnName
            .ValueMember = sourceFieldDataTable.Columns(0).ColumnName
        End With

        With cboRef2
            .DataSource = bsRefField2Collection
            .DisplayMember = sourceFieldDataTable.Columns(0).ColumnName
            .ValueMember = sourceFieldDataTable.Columns(0).ColumnName
        End With

        With cboRef3
            .DataSource = bsRefField3Collection
            .DisplayMember = sourceFieldDataTable.Columns(0).ColumnName
            .ValueMember = sourceFieldDataTable.Columns(0).ColumnName
        End With
        If Not _dataModel Is Nothing Then 'And Not IsNew
            cboRef1.SelectedValue = IIf(IsNothingOrEmptyString(_dataModel.ReferenceField1), "", _dataModel.ReferenceField1)
            cboRef2.SelectedValue = IIf(IsNothingOrEmptyString(_dataModel.ReferenceField2), "", _dataModel.ReferenceField2)
            cboRef3.SelectedValue = IIf(IsNothingOrEmptyString(_dataModel.ReferenceField3), "", _dataModel.ReferenceField3)
        End If

    End Sub

    Private Sub setDataSourceToBindingSource()
        If _dataModel Is Nothing Then Exit Sub
        Dim _pMapSourceFields As New MapSourceFieldCollection
        Dim _pMapBankFields As New MapBankFieldCollection

        If IsFileFormatUsingRecordTypes() Then
            For Each srcField As MapSourceField In _dataModel.MapSourceFields
                Dim cloneSourceField As New BTMU.Magic.CommonTemplate.MapSourceField
                cloneSourceField.SourceFieldName = srcField.SourceFieldName
                cloneSourceField.SourceFieldValue = srcField.SourceFieldValue
                If srcField.SourceFieldName.StartsWith("P-Field") Then
                    _pMapSourceFields.Add(cloneSourceField)
                End If
            Next
            bsFilterFieldCollection.DataSource = _pMapSourceFields
            bsLookupSourceField.DataSource = _pMapSourceFields
            bsCalOperand1SourceField.DataSource = _pMapSourceFields
            bsCalcOperand2SourceField.DataSource = _pMapSourceFields
            bsCalcOperand3SourceField.DataSource = _pMapSourceFields
            PopulateReferenceFieldCollection(_pMapSourceFields)
        Else
            bsFilterFieldCollection.DataSource = bsDetailCollection
            bsFilterFieldCollection.DataMember = "MapSourceFields"
            bsLookupSourceField.DataSource = bsDetailCollection
            bsLookupSourceField.DataMember = "MapSourceFields"
            bsCalOperand1SourceField.DataSource = bsDetailCollection
            bsCalOperand1SourceField.DataMember = "MapSourceFields"
            bsCalcOperand2SourceField.DataSource = bsDetailCollection
            bsCalcOperand2SourceField.DataMember = "MapSourceFields"
            bsCalcOperand3SourceField.DataSource = bsDetailCollection
            bsCalcOperand3SourceField.DataMember = "MapSourceFields"
            PopulateReferenceFieldCollection(_dataModel.MapSourceFields)
        End If
    End Sub

    ''' <summary>
    ''' Initializes the Template after loading from file
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PrepareForLoad()
        If Not _dataModel Is Nothing Then
            With _dataModel
                Select Case .RemittanceAmount
                    Case "Dollar"
                        rdoDollars.Checked = True
                    Case "Cent"
                        rdoCents.Checked = True
                    Case Else
                        rdoDollars.Checked = True
                End Select

                Select Case .FilterCondition
                    Case "AND"
                        rdoFilterAND.Checked = True
                    Case "OR"
                        rdoFilterOR.Checked = True
                    Case Else
                        rdoFilterAND.Checked = True
                End Select

                Select Case .AdviceRecord
                    Case "EveryRow"
                        rdoAdviceForEveryRow.Checked = True
                    Case "EveryChar"
                        rdoAdviceAftEveryChar.Checked = True
                    Case "Delimiter"
                        rdoAdviceDelimited.Checked = True
                    Case Else
                        rdoAdviceForEveryRow.Checked = True
                End Select
                PopulateReferenceFieldCollection(_dataModel.MapSourceFields)
            End With
        End If
    End Sub

    Private Sub prepareForSave()
        If rdoDollars.Checked Then
            _dataModel.RemittanceAmount = "Dollar"
        ElseIf rdoCents.Checked Then
            _dataModel.RemittanceAmount = "Cent"
        End If

        If rdoFilterAND.Checked Then
            _dataModel.FilterCondition = "AND"
        ElseIf rdoFilterOR.Checked Then
            _dataModel.FilterCondition = "OR"
        End If

        If rdoAdviceForEveryRow.Checked Then
            _dataModel.AdviceRecord = "EveryRow"
        ElseIf rdoAdviceAftEveryChar.Checked Then
            _dataModel.FilterCondition = "EveryChar"
        ElseIf rdoAdviceDelimited.Checked Then
            _dataModel.FilterCondition = "Delimiter"
        End If
    End Sub

    Private Sub SelectGridRow(ByVal row As GridRow, ByVal grid As DataGridView, ByRef Xobj As Object, ByRef YObj As Object)

        Try

            If grid.CurrentRow Is Nothing Or grid.Rows.Count <= 0 _
                Or grid.CurrentRow.Index + 1 = grid.Rows.Count Then
                Exit Sub
            End If

            If row = GridRow.Up Then ' Row Up

                If grid.CurrentRow.Index = 0 Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index - 1).Selected = True
            Else ' Row Down

                If grid.CurrentRow.Index + 2 = grid.Rows.Count Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index + 1).Selected = True
            End If

            bsDetailCollection.RaiseListChangedEvents = False

            Dim tmp As Object = Xobj

            Xobj = YObj : YObj = tmp

            bsDetailCollection.RaiseListChangedEvents = True
            bsDetailCollection.ResetBindings(False)

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Excel Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub RefreshSource()

        ' Bind the datagrid with the Datatable.
        dgvSampleData.DataSource = Nothing
        dgvSampleData.DataSource = SampleRowDataTable
        dgvSampleData.Refresh()

        'Populate Map Source Field, Lookup Source Field 
        _dataModel.PopulateSourceField(SampleRowDataTable, IsNew)

        ' Populate reference field 1,2,3
        PopulateReferenceFieldCollection(_dataModel.MapSourceFields)
        setDataSourceToBindingSource()
    End Sub

    Private Sub ValidateAllControls(ByVal ctl As Control)

        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                ValidateAllControls(ctlDetail)
            Else
                Control_Validated(ctlDetail, Nothing)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Displays the Template data and disables the fields to protect from modification
    ''' </summary>
    ''' <param name="commonTemplateFile">Common Template</param>
    ''' <remarks></remarks>
    Public Sub ViewExisting(ByVal commonTemplateFile As String)

        EditExisting(commonTemplateFile)
        EnableDisableTabFields(False)

        btnEdit.Enabled = True
        btnSave.Enabled = False
        btnSaveAsDraft.Enabled = False
        btnPreview.Enabled = True
        btnCancel.Enabled = True
        IsNew = False
        isView = True
    End Sub

    Private Function AllowSampleFileChange() As Boolean
        Dim mapFieldPosition As Integer
        Dim colNo As Integer
        Dim isFound As Boolean = False
        Dim isWorksheetFound As Boolean = False
        Dim returnValue As Boolean = False
        If _dataModel.MapSourceFields.Count = 0 Then Return True
        'If _dataModel.HeaderAsFieldName Then
        '#2.    All the FieldNames in the original Source File must exist in the same position.
        mapFieldPosition = 0
        For Each sourceField As MapSourceField In _dataModel.MapSourceFields
            isFound = False
            colNo = 0
            For Each col As DataColumn In SampleRowDataTable.Columns
                If col.ColumnName.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                    If colNo = mapFieldPosition Then
                        isFound = True
                        Exit For
                    End If
                End If
                colNo += 1
            Next
            If Not isFound Then
                If MessageBox.Show(MsgReader.GetString("E02010050"), _
                                msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    'Clear Mapping, Translator, Editable, Lookup, Translator, Calculation, 
                    'String Manipulation grids
                    ClearCollection()
                    Return True
                Else
                    Return False
                End If
            End If
            mapFieldPosition += 1
        Next
        'End If
        Return True
    End Function

    Private Function ConvertToFormatSeparatorDate(ByVal sText As String) As String

        Select Case cboDateType.Text
            Case "DDMMYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & sText.Substring(2, 2) & "/" & sText.Substring(4, 2)
                End If

            Case "DDMMMYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & Month(sText.Substring(2, 3)).ToString() & "/" & sText.Substring(5, 2)
                End If

            Case "DDMMYYYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & sText.Substring(2, 2) & "/" & sText.Substring(4, 4)
                End If

            Case "DDMMMYYYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & Month(sText.Substring(2, 3)).ToString() & "/" & sText.Substring(5, 4)
                End If

            Case "MMDDYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & sText.Substring(2, 2) & "/" & sText.Substring(4, 2)
                End If

            Case "MMMDDYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return Month(sText.Substring(0, 3)).ToString() & "/" & sText.Substring(3, 2) & "/" & sText.Substring(5, 2)
                End If

            Case "MMDDYYYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & sText.Substring(2, 2) & "/" & sText.Substring(4, 4)
                End If
            Case "MMMDDYYYY"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return Month(sText.Substring(0, 3)).ToString() & "/" & sText.Substring(3, 2) & "/" & sText.Substring(5, 4)
                End If
            Case "YYMMDD"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 2) & "/" & sText.Substring(2, 2) & "/" & sText.Substring(4, 2)
                End If

            Case "YYYYDDMM"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 4) & "/" & sText.Substring(6, 2) & "/" & sText.Substring(4, 2)
                End If

            Case "YYYYMMDD"
                If cboDateSep.Text <> "No Space" Then
                    If cboDateSep.Text = "<Space>" Then
                        Return sText.Replace(" ", "/")
                    Else
                        Return sText.Replace(cboDateSep.Text, "/")
                    End If
                Else
                    Return sText.Substring(0, 4) & "/" & sText.Substring(4, 2) & "/" & sText.Substring(6, 2)
                End If

        End Select

        Return String.Empty

    End Function

    Private Function GetMappedSourceFieldValue(ByVal SourceFieldName As String)
        Dim returnValue As String = String.Empty
        For Each mapField As MapSourceField In _dataModel.MapSourceFields
            If mapField.SourceFieldName.Trim.ToUpper = SourceFieldName.Trim.ToUpper Then
                returnValue = mapField.SourceFieldValue
            End If
        Next
        Return returnValue
    End Function

    Private Function GetRecordTypeStartIndex(ByVal format As String, ByVal recordType As String) As Integer
        Select Case format
            Case "iFTS-2 MultiLine"
                If recordType = "P" Then Return _dataModel.MapBankFields.IndexOf("Record Type-P")
                If recordType = "W" Then Return _dataModel.MapBankFields.IndexOf("Record Type-W")
                If recordType = "I" Then Return _dataModel.MapBankFields.IndexOf("Record Type-I")
                If recordType = "Consolidate Field" Then Return _dataModel.MapBankFields.IndexOf("Consolidate Field")

            Case "Mr.Omakase India"
                If recordType = "P" Then Return _dataModel.MapBankFields.IndexOf("File Type-P")
                If recordType = "W" Then Return -1
                If recordType = "I" Then Return _dataModel.MapBankFields.IndexOf("Record Type-I")
        End Select
    End Function

    ''' <summary>
    ''' <para>Validation - Cents setting and decimal separator should not co-exists</para>
    ''' </summary>
    ''' <returns>TRUE if valid, else false</returns>
    ''' <remarks></remarks>
    Private Function IsCentsSettingValid() As Boolean
        Dim _returnValue As Boolean = True
        Try
            If Not _dataModel Is Nothing Then
                With _dataModel
                    If IsNothingOrEmptyString(.DecimalSeparator) Then
                        Return True
                    End If
                    If .DecimalSeparator.Trim.Length = 1 AndAlso .IsAmountInCents Then
                        If MessageBox.Show(My.Resources.E02000080, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                            _returnValue = False
                        End If
                    End If

                End With

            End If
            Return _returnValue
        Catch ex As Exception
            MessageBox.Show("Error saving Common Template : " & ex.Message.ToString, "IsCentsSettingValid", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error saving Common Template : ", ex.Message)
        End Try
    End Function

    Private Function IsDataSavable() As Boolean

        _dataModel.Validate()
        If Not ValidateCommonTemplateName() Then
            'errMessage &= "Common Template Name contains invalid characters.Please do not input the following characters : [.,;!@#$%^&*+=/\?'""|:<>`~]+?)" & vbNewLine
            Return False
        End If
        'Checking duplicate template name
        If txtTemplateName.Enabled = True And _
            (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
             File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
            MessageBox.Show(MsgReader.GetString("E10000034"), "Generate Common Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'MessageBox.Show("Template Name already exists. Please use different Template Name.", "Duplicate")
            Return False
        End If
        If Not VerifyMapping() Then Return False
        If Not VerifyRecordTypeIndicators() Then Return False
        If Not IsCentsSettingValid() Then Return False
        If Not ValidateReferenceFields() Then Return False
        If ShowValidationError() Then
            prepareForSave()
            _dataModel.ApplyEdit()
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsFieldsInSettingFound(ByVal worksheetDataTable As DataTable) As Boolean
        Dim mapFieldPosition As Integer
        Dim colNo As Integer
        Dim isFound As Boolean = False
        Dim isUsed As Boolean = False
        Dim returnValue As Boolean = False

        If Not _dataModel Is Nothing Then
            If _dataModel.MapSourceFields.Count = 0 Then
                Return True
            Else
                mapFieldPosition = 0
                For Each sourceField As MapSourceField In _dataModel.MapSourceFields
                    isFound = False
                    colNo = 0
                    For Each col As DataColumn In worksheetDataTable.Columns
                        If col.ColumnName.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                            If colNo = mapFieldPosition Then
                                isFound = True
                                Exit For
                            End If
                        End If
                        colNo += 1
                    Next
                    If Not isFound Then
                        isUsed = False
                        ' Check if this field exists in any of the Settings
                        ' Reference Field Setting
                        If Not IsNothingOrEmptyString(_dataModel.ReferenceField1) Then
                            If _dataModel.ReferenceField1.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                isUsed = True
                            End If
                        End If
                        If Not IsNothingOrEmptyString(_dataModel.ReferenceField2) Then
                            If _dataModel.ReferenceField2.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                isUsed = True
                            End If
                        End If
                        If Not IsNothingOrEmptyString(_dataModel.ReferenceField3) Then
                            If _dataModel.ReferenceField3.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                isUsed = True
                            End If
                        End If

                        ' Filter Setting
                        If _dataModel.Filters.Count > 0 Then
                            For Each filter As RowFilter In _dataModel.Filters
                                If filter.FilterField.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                    isUsed = True
                                    Exit For
                                End If
                            Next
                        End If
                        'Mapping
                        If _dataModel.MapBankFields.Count > 0 Then
                            For Each bankField As MapBankField In _dataModel.MapBankFields
                                For Each mapField As MapSourceField In bankField.MappedSourceFields
                                    'If Not IsNothingOrEmptyString(mapField.SourceFieldName) Then
                                    If mapField.SourceFieldName.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                        isUsed = True
                                        Exit For
                                    End If
                                    'End If
                                Next
                                If isUsed Then Exit For
                            Next
                        End If
                        'Lookup
                        If _dataModel.TranslatorSettings.Count > 0 Then
                            For Each lookup As LookupSetting In _dataModel.LookupSettings
                                If lookup.SourceFieldName.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                    isUsed = True
                                    Exit For
                                End If
                            Next
                        End If
                        'Calculation
                        If _dataModel.CalculatedFields.Count > 0 Then
                            For Each calc As CalculatedField In _dataModel.CalculatedFields
                                If Not IsNothingOrEmptyString(calc.Operand1) Then
                                    If calc.Operand1.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                        isUsed = True
                                        Exit For
                                    End If
                                End If
                                If Not IsNothingOrEmptyString(calc.Operand2) Then
                                    If calc.Operand2.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                        isUsed = True
                                        Exit For
                                    End If
                                End If
                                If Not IsNothingOrEmptyString(calc.Operand3) Then
                                    If calc.Operand3.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                                        isUsed = True
                                        Exit For
                                    End If
                                End If
                            Next
                        End If


                        If isUsed Then
                            If MessageBox.Show("Some of the fields are not in the same order or does not exists. " & _
                                            "All settings will be cleared.Do you wish to continue?", _
                                            msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                                'Clear Mapping, Translator, Editable, Lookup, Translator, Calculation, 
                                'String Manipulation grids
                                With _dataModel
                                    '.MapSourceFields.Clear()
                                    .Filters.Clear()
                                    .MapBankFields.Clear()
                                    .TranslatorSettings.Clear()
                                    .EditableSettings.Clear()
                                    .LookupSettings.Clear()
                                    .CalculatedFields.Clear()
                                    .StringManipulations.Clear()
                                End With
                                PopulateMapBankField()
                                Return True
                            Else
                                Return False
                                'Set back all the changes
                            End If
                        Else
                            'Return True
                        End If

                    End If
                    mapFieldPosition += 1
                Next
            End If
        End If
        Return True
    End Function
    ''' <summary>
    ''' This function is used to check whether the output format is iFTS-2/Mr. Omakase India.
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function IsFileFormatUsingRecordTypes() As Boolean
        Dim _isFormatWithRecordTypes As Boolean = False
        Dim fileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList
        If bsOutputTemplate.Current Is Nothing Then Return False
        fileFormat = bsOutputTemplate.Current
        If BTMU.Magic.Common.HelperModule.IsFileFormatUsingRecordTypes(fileFormat.OutputFormat) Then
            _isFormatWithRecordTypes = True
        End If
        'For Each format As String In _formatNameArray
        '    If format.Trim.ToUpper = fileFormat.OutputFormat.Trim.ToUpper Then
        '        _isFormatWithRecordTypes = True
        '        Exit For
        '    End If
        'Next
        Return _isFormatWithRecordTypes
    End Function

    Private Function IsSavableAsDraft() As Boolean
        Dim errMessage As String = String.Empty

        ' Save as Draft - Validate the template Name
        If IsNothingOrEmptyString(_dataModel.CommonTemplateName) Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Common Template Name") & vbNewLine
            'errMessage &= "Please enter Common Template Name" & vbNewLine
        End If
        ' Validation - Common Template Name should not contain invalid characters
        If Not IsNothingOrEmptyString(_dataModel.CommonTemplateName) Then
            'Dim Name_Regex As New Regex(Regex.Escape("[.,;!@#$%^&*+=/\?'""|:<>`~]+?"))
            'If Name_Regex.IsMatch(_dataModel.CommonTemplateName) Then
            '    errMessage &= "Common Template Name contains invalid characters.Please do not input the following characters : [.,;!@#$%^&*+=/\?'""|:<>`~]+?)" & vbNewLine
            'End If
            If Not ValidateCommonTemplateName() Then
                'errMessage &= "Common Template Name contains invalid characters.Please do not input the following characters : [.,;!@#$%^&*+=/\?'""|:<>`~]+?)" & vbNewLine
                Return False
            End If
            'Checking duplicate template name
            If txtTemplateName.Enabled = True And _
                (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder, txtTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                errMessage &= MsgReader.GetString("E10000034") & vbNewLine
                'errMessage &= "Template Name already exists. Please use different Template Name."
            End If
        End If
        If IsNothingOrEmptyString(_dataModel.OutputTemplate) Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Master Template Name") & vbNewLine
            'errMessage &= "Please fill in Master Template" & vbNewLine
        End If
        If errMessage.Trim = String.Empty Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Save as Draft : ", errMessage)
            Return False
        End If
    End Function

    Private Function IsSourceFieldFound(ByVal worksheetDataTable As DataTable) As Boolean
        Dim isfound As Boolean = False
        If Not _dataModel Is Nothing Then
            For Each sourceField As MapSourceField In _dataModel.MapSourceFields
                isfound = False
                For Each _column As DataColumn In worksheetDataTable.Columns
                    If _column.ColumnName.Trim.ToUpper = sourceField.SourceFieldName.Trim.ToUpper Then
                        isfound = True
                        Exit For
                    End If
                Next
            Next
        End If
        Return isfound
    End Function

    Private Function IsWorksheetFound(ByVal worksheetDataTable As DataTable) As Boolean
        Dim worksheetDataRow As DataRow
        Dim isfound As Boolean = False

        If Not _dataModel Is Nothing Then
            isfound = False
            If _dataModel.MapSourceFields.Count = 0 Then
                isfound = True
            Else
                If IsNothingOrEmptyString(_dataModel.WorksheetName) Then Return True
                For Each worksheetDataRow In worksheetDataTable.Rows
                    If worksheetDataRow.Item(0).ToString.ToUpper = _dataModel.WorksheetName.ToUpper Then
                        isfound = True
                        Exit For
                    End If
                Next
            End If
        End If
        Return isfound
    End Function

    Private Function PopulateWorksheetNames() As Boolean
        Dim _worksheetDataTable As DataTable
        Dim worksheetCollection As New Magic.CommonTemplate.ExcelWorksheetCollection
        Dim isFound As Boolean = False
        Dim isEmpty As Boolean = False
        Try
            _worksheetDataTable = ExcelHelperModule.GetWorksheetNames(_dataModel.SampleSourceFile)
            If _worksheetDataTable Is Nothing Then Return False
            Dim worksheetObj As New Magic.CommonTemplate.ExcelWorksheet
            For Each _worksheet As DataRow In _worksheetDataTable.Rows
                worksheetObj = New ExcelWorksheet
                worksheetObj.ReadableWorksheetName = _worksheet("WorksheetName").ToString
                worksheetObj.WorksheetName = _worksheet("WorksheetName").ToString
                worksheetCollection.Add(worksheetObj)
            Next
            If _dataModel.Worksheets.Count = 0 Then
                isFound = True
            Else
                isEmpty = False
                'Check if the Worksheet Name exists in the new collection
                If IsWorksheetFound(_worksheetDataTable) Then
                    isFound = True
                Else
                    'prompt the message
                    If MessageBox.Show(String.Format(MsgReader.GetString("E02010040") _
                                       , _dataModel.WorksheetName), "Confirmation" _
                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question) _
                                       = DialogResult.Yes Then
                        isFound = True
                        isEmpty = True
                    Else
                        isFound = False
                    End If
                End If
            End If
            If isFound Then
                'Clear the collection and add
                _dataModel.Worksheets.Clear()
                Dim add As Boolean = False
                For Each worksheet As CommonTemplate.ExcelWorksheet In worksheetCollection
                    add = True
                    For Each sheet As ExcelWorksheet In _dataModel.Worksheets
                        If sheet.WorksheetName = worksheet.WorksheetName Then
                            add = False
                            Exit For
                        End If
                    Next
                    If add Then
                        _dataModel.Worksheets.Add(worksheet)
                    End If

                Next
                If IsNew Then
                    If _dataModel.Worksheets.Count > 0 Then
                        _dataModel.WorksheetName = _dataModel.Worksheets.Item(0).WorksheetName
                    End If
                Else
                    If isEmpty Then
                        If _dataModel.Worksheets.Count > 0 Then
                            _dataModel.WorksheetName = _dataModel.Worksheets.Item(0).WorksheetName
                        End If
                    Else
                        cboWorksheet.SelectedValue = _dataModel.WorksheetName
                    End If

                End If
            Else
                EditExisting(_originalSourceFileName)
            End If
            Return isFound
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error populating Worksheet Names : ", ex.Message)
        End Try
    End Function

    Private Function ShowValidationError() As Boolean
        Dim errorInfo As System.ComponentModel.IDataErrorInfo
        Dim errorMessage As String
        Dim firstError As New System.Text.StringBuilder
        Dim detailError As New System.Text.StringBuilder

        errorInfo = CType(_dataModel, System.ComponentModel.IDataErrorInfo)
        errorMessage = errorInfo.Error
        If Not IsNothingOrEmptyString(errorMessage) Then
            Dim errorList As String() = errorMessage.Split(Environment.NewLine)
            Dim temp As String
            For Each temp In errorList
                If firstError.Length > 0 Then
                    If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", temp))
                Else
                    If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("{0}", temp))
                End If
            Next
        End If

        '************************************************************************************************'
        ' Get Validation Error - Filter Settings
        For Each detailItem As Magic.CommonTemplate.RowFilter In _dataModel.Filters
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If

            If detailItem.FilterOperator Is Nothing Or detailItem.FilterType Is Nothing Or detailItem.FilterValue Is Nothing Then Continue For

            ''''''BEGIN - data type validation '''''''''''''''''
            Select Case detailItem.FilterOperator.ToUpper()

                Case "> AND <", "> AND <=", ">= AND <", ">= AND <="

                    Dim values2() As String = Regex.Split(detailItem.FilterValue.Trim(), Regex.Escape("||"))

                    If values2.Length = 2 Then
                        'validate data type
                        If detailItem.FilterType.ToUpper() = "NUMERIC" Then
                            Dim x As Decimal
                            For Each numbervalue As String In values2
                                If Not HelperModule.IsDecimalDataType(numbervalue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                                    If firstError.Length > 0 Then
                                        detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected", detailItem.Sequence, numbervalue))
                                    Else
                                        firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected", detailItem.Sequence, numbervalue))
                                    End If
                                End If
                            Next

                        ElseIf detailItem.FilterType.ToUpper() = "DATE" Then
                            For Each datevalue As String In values2
                                Dim x As Date
                                If Not HelperModule.IsDateDataType(datevalue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZerosInDate) Then
                                    If firstError.Length > 0 Then
                                        detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected", detailItem.Sequence, datevalue))
                                    Else
                                        firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected", detailItem.Sequence, datevalue))
                                    End If
                                End If
                            Next
                        End If
                    Else

                        If firstError.Length > 0 Then
                            detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value is not in expected format!, Expected Format: Value1||Value2", detailItem.Sequence))
                        Else
                            firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value is not in expected format!, Expected Format: Value1||Value2", detailItem.Sequence))
                        End If

                    End If

                Case Else
                    'validate data type
                    If detailItem.FilterType.ToUpper() = "NUMERIC" Then

                        Dim x As Decimal
                        If Not HelperModule.IsDecimalDataType(detailItem.FilterValue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                            If firstError.Length > 0 Then
                                detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected", detailItem.Sequence, detailItem.FilterValue))
                            Else
                                firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected", detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    ElseIf detailItem.FilterType.ToUpper() = "DATE" Then

                        Dim x As Date
                        If Not HelperModule.IsDateDataType(detailItem.FilterValue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZerosInDate) Then
                            If firstError.Length > 0 Then
                                detailError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected", detailItem.Sequence, detailItem.FilterValue))
                            Else
                                firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected", detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    End If

            End Select
            ''''''END - data type validation '''''''''''''''''

            'duplicate RowFilter?
            If _dataModel.Filters.IsDuplicateRowFilter(detailItem) Then
                If firstError.Length > 0 Then
                    detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - Duplicate Row Filter!", detailItem.Sequence))
                Else
                    firstError.AppendLine(String.Format("Filter Setting (Position: {0}) -  Duplicate Row Filter!", detailItem.Sequence))
                End If
            End If

            'Multiple Instances of a RowFilter Field with Different Filter Type (Data Type)?
            If _dataModel.Filters.hasDuplicateFilterType(detailItem) Then
                If firstError.Length > 0 Then
                    detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - Duplicate Row Filter Field '{1}' cannot have different Data Type!", detailItem.Sequence, detailItem.FilterField))
                Else
                    firstError.AppendLine(String.Format("Filter Setting (Position: {0}) - Duplicate Row Filter Field '{1}' cannot have different Data Type!", detailItem.Sequence, detailItem.FilterField))
                End If
            End If

        Next

        '************************************************************************************************'
        ' Get Validation Error - Mapping Settings
        For Each detailItem As Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        '************************************************************************************************'
        ' Get Validation Error - Translator Settings
        For Each detailItem As Magic.CommonTemplate.TranslatorSetting In _dataModel.TranslatorSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        '************************************************************************************************'
        ' Get Validation Error - Editable Settings
        For Each detailItem As Magic.CommonTemplate.EditableSetting In _dataModel.EditableSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        '************************************************************************************************'
        ' Get Validation Error - Lookup Settings
        For Each detailItem As Magic.CommonTemplate.LookupSetting In _dataModel.LookupSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next


        '************************************************************************************************'
        ' Get Validation Error - Calculation Settings
        For Each detailItem As Magic.CommonTemplate.CalculatedField In _dataModel.CalculatedFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        '************************************************************************************************'
        ' Get Validation Error - String Manipulation Settings
        For Each detailItem As Magic.CommonTemplate.StringManipulation In _dataModel.StringManipulations
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("String Manipulation  (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        'BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(firstError.ToString, detailError.ToString)
        ValidateAllControls(Me)

        If (firstError.ToString() = String.Empty AndAlso detailError.ToString() = String.Empty) Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", firstError.ToString & vbCrLf & detailError.ToString)
            Return False
        End If
    End Function

    ''' <summary>
    ''' <para>This function validates the data before retrieving the Excel Worksheet Data</para>
    ''' </summary>
    ''' <returns>"TRUE" if validated else "FALSE"</returns>
    ''' <remarks></remarks>
    Private Function ValidateBeforeRetrieveWorksheetData() As Boolean
        Dim _returnValue As Boolean = True
        Dim _errMessage As String = String.Empty

        Try
            If Not _dataModel Is Nothing Then
                With _dataModel
                    If IsNothingOrEmptyString(.OutputTemplate) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Master Template Name") & vbNewLine
                    End If

                    If IsNothingOrEmptyString(.SampleSourceFile) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Sample Source File") & vbNewLine
                    End If
                    If Not System.IO.File.Exists(.SampleSourceFile) Then
                        _errMessage &= String.Format(MsgReader.GetString("E02010020"), _dataModel.SampleSourceFile) & vbNewLine
                        '_errMessage &= String.Format("Excel File : '{0}' does not exists. Please check the file path", _dataModel.SampleSourceFile) & vbNewLine
                    End If
                    If IsNothingOrEmptyString(.WorksheetName) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Worksheet Name") & vbNewLine
                    End If
                    If IsNothingOrEmptyString(.StartColumn) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Start Column") & vbNewLine
                    End If
                    If IsNothingOrEmptyString(.EndColumn) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "End Column") & vbNewLine
                    End If
                    ' Validation - Start Column should be less than End Column
                    If Not IsNothingOrEmptyString(.StartColumn) And Not IsNothingOrEmptyString(.EndColumn) Then
                        If GetColumnIndex(.StartColumn) > GetColumnIndex(.EndColumn) Then
                            _errMessage &= MsgReader.GetString("E02010010") & vbNewLine
                        End If
                    End If

                    If IsNothingOrEmptyString(.SampleRowNo) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Sample Row") & vbNewLine
                    End If
                    If IsNothingOrEmptyString(.HeaderAtRow) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Header at Row") & vbNewLine
                    End If
                    If IsNothingOrEmptyString(.TransactionStartingRow) Then
                        _errMessage &= String.Format(MsgReader.GetString("E00000050"), "Transaction Start Row") & vbNewLine
                    End If

                    ' This validation need not be done for formats : iFTS-2 and Omakase
                    ' Validation - Sample Row should be greater than or equal to Transaction Start Row
                    If Not IsFileFormatUsingRecordTypes() Then
                        If Not IsNothingOrEmptyString(.SampleRowNo) And Not IsNothingOrEmptyString(.TransactionStartingRow) Then
                            If Convert.ToInt32(.SampleRowNo.Trim) < Convert.ToInt32(.TransactionStartingRow.Trim) Then
                                _errMessage &= String.Format(MsgReader.GetString("E02000040")) & vbNewLine
                            End If
                        End If
                    End If
                    
                    ' Validation - Transaction Start Row should be greater than Header At Row
                    If Not IsNothingOrEmptyString(.HeaderAtRow) And Not IsNothingOrEmptyString(.TransactionStartingRow) Then
                        If Convert.ToInt32(.HeaderAtRow.Trim) >= Convert.ToInt32(.TransactionStartingRow.Trim) Then
                            _errMessage &= String.Format(MsgReader.GetString("E02000160")) & vbNewLine
                        End If
                    End If


                    Dim outputTemplate As MasterTemplate.MasterTemplateList
                    outputTemplate = bsOutputTemplate.Current
                    If outputTemplate.OutputFormat.Trim = "iFTS-2" Then
                        ' Validate P-Column, W-Column, I-Column, P-Indicator, W-Indicator, I-Indicator

                    End If
                End With
                If _errMessage = String.Empty Then
                    Return True
                Else
                    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on retrieving Worksheet Data : ", _errMessage)
                    Return False
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error retrieving Worksheet Data : " & ex.Message.ToString, "ValidateBeforeRetrieveWorksheetData", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error retrieving Worksheet Data : ", ex.Message)
        End Try
    End Function

    Private Function ValidateCommonTemplateName() As Boolean

        Try
            Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

            Dim value As String = _dataModel.CommonTemplateName
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                Common.BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E01000010"), "Common Template Name"))

                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

        Return True

    End Function

    ''' <summary>
    ''' This function is used to validate whether any one of the Record Type Indicators are filled in.
    ''' The sample source file can be in any format with one or more recrod types.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>This validation is done only for iFTS-2 and Mr. Omakase India File Formats only.</remarks>
    Private Function ValidateRecordTypes()

        Dim _errMessage As New System.Text.StringBuilder
        Try
            ' P Indicator and P Indicator Column is must
            If IsNothingOrEmptyString(_dataModel.PColumn) Then
                _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "P Column"))
            End If
            If IsNothingOrEmptyString(_dataModel.PIndicator) Then
                _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "P Indicator"))
            End If

            If Not IsNothingOrEmptyString(_dataModel.WColumn) Then
                If IsNothingOrEmptyString(_dataModel.WIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "W Indicator"))
                End If
            Else
                If Not IsNothingOrEmptyString(_dataModel.WIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "W Column"))
                End If
            End If

            If Not IsNothingOrEmptyString(_dataModel.IColumn) Then
                If IsNothingOrEmptyString(_dataModel.IIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "I Indicator"))
                End If
            Else
                If Not IsNothingOrEmptyString(_dataModel.IIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "I Column"))
                End If
            End If
            If _errMessage.ToString = String.Empty Then
                Return True
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on retrieving worksheet data : ", _errMessage.ToString)
                Return False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on retrieving worksheet data : ", ex.Message.ToString)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' <para>Validation - For Reference Field 2 and Reference Field 3</para>
    ''' </summary>
    ''' <returns>TRUE, if Valid else FALSE</returns>
    ''' <remarks></remarks>
    Private Function ValidateReferenceFields() As Boolean
        Dim errMessage As String = String.Empty
        Try
            If Not _dataModel Is Nothing Then
                With _dataModel
                    If Not IsNothingOrEmptyString(.ReferenceField2) Then
                        ' Validation - Reference Field 1 should be selected
                        If IsNothingOrEmptyString(.ReferenceField1) Then
                            errMessage &= MsgReader.GetString("E02000170") & vbNewLine
                            'MessageBox.Show(My.Resources.E02000170, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'Return False
                        End If
                        ' Validation - Reference Field 2 should not be same as Reference Field 1
                        If Not (IsNothingOrEmptyString(.ReferenceField1) Or IsNothingOrEmptyString(.ReferenceField2)) Then
                            If .ReferenceField1.Trim = .ReferenceField2.Trim Then
                                errMessage &= MsgReader.GetString("E02000180") & vbNewLine
                                'MessageBox.Show(My.Resources.E02000180, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'Return False
                            End If
                        End If
                    End If

                    If Not IsNothingOrEmptyString(.ReferenceField3) Then
                        ' Validation - Reference Field 1 should be selected
                        If IsNothingOrEmptyString(.ReferenceField1) Then
                            errMessage &= MsgReader.GetString("E02000170") & vbNewLine
                            'MessageBox.Show(My.Resources.E02000170, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'Return False
                        End If
                        ' Validation - Reference Field 2 should be selected
                        If IsNothingOrEmptyString(.ReferenceField2) Then
                            errMessage &= MsgReader.GetString("E02000200") & vbNewLine
                            'MessageBox.Show(My.Resources.E02000200, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'Return False
                        End If

                        ' Validation - Reference Field 3 should not be same as Reference Field1
                        If Not (IsNothingOrEmptyString(.ReferenceField1) Or IsNothingOrEmptyString(.ReferenceField3)) Then
                            If .ReferenceField1.Trim = .ReferenceField3.Trim Then
                                errMessage &= MsgReader.GetString("E02000210") & vbNewLine
                                'MessageBox.Show(My.Resources.E02000210, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'Return False
                            End If
                        End If

                        ' Validation - Reference Field 3 should not be same as Reference Field 2
                        If Not (IsNothingOrEmptyString(.ReferenceField2) Or IsNothingOrEmptyString(.ReferenceField3)) Then
                            If .ReferenceField2.Trim = .ReferenceField3.Trim Then
                                errMessage &= MsgReader.GetString("E02000220") & vbNewLine
                                'MessageBox.Show(My.Resources.E02000220, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'Return False
                            End If
                        End If
                    End If
                End With
            End If
            If errMessage = String.Empty Then
                Return True
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Reference Field Setting : ", errMessage)
                Return False
            End If

        Catch ex As Exception
            MessageBox.Show("Error saving Common Template : " & ex.Message.ToString, "ValidateReferenceFields", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error saving Common Template : ", ex.Message)
        End Try

    End Function

    Private Function ValidateTypeOfMappedFields(ByVal _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField _
                    , ByVal objCusValue As BTMU.Magic.CommonTemplate.MapSourceField) As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        If _objMapBankField.DataType Is Nothing Then Return True

        Select Case _objMapBankField.DataType.ToUpper()
            Case "NUMBER", "CURRENCY"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                    If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                        errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                    End If
                End If
            Case "DATETIME"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                    If cboDateSep.Text <> "No Space" Then
                        If cboDateSep.Text = "<Space>" Then
                            value = value.Replace(" ", "")
                            'Else
                            '    value = value.Replace(cboDateSep.Text, "")
                        End If
                    End If
                    If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZerosInDate) Then
                        errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                    End If
                End If
        End Select

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function

    Private Function VerifyRecordTypeIndicators() As Boolean
        Dim isTranslated As Boolean = False
        ' Record Type Indicators must be P, W and I
        Select Case getOutputFileFormat()
            Case iFTS2MultiLineFormat.OutputFormatString
                For Each bankfield As MapBankField In _dataModel.MapBankFields
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-P' is not 'P'.Please translate the 'Record Type-P'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If

                            End If
                        End If
                    End If

                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-W", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "W".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-W", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "W".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-W' is not 'W'.Please translate the 'Record Type-W'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-I' is not 'I'.Please translate the 'Record Type-I'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If

                Next

            Case OMAKASEFormat.OutputFormatString
                For Each bankfield As MapBankField In _dataModel.MapBankFields
                    isTranslated = False
                    If bankfield.BankField.Equals("File Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("File Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'File Type-P' is not 'P'.Please translate the 'File Type-P'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-I' is not 'I'.Please translate the 'Record Type-I'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                Next
                'Case iFTS2SingleLineFormat.OutputFormatString
                '    For Each bankfield As MapBankField In _dataModel.MapBankFields
                '        isTranslated = False
                '        If bankfield.BankField.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                '            If IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                MessageBox.Show("Please map the bank field 'Record Type-P'")
                '                Return False
                '            ElseIf Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                'Check if translated to P
                '                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                    If trans.BankFieldName.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                '                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                            isTranslated = True
                '                            Exit For
                '                        End If
                '                    End If
                '                Next
                '                If Not isTranslated Then
                '                    MessageBox.Show("'Record Type-P' is not 'P'.Please translate the 'Record Type-P'.")
                '                    Return False
                '                End If
                '            End If
                '        End If
                '    Next
                '    For cnt As Integer = 1 To 3
                '        For Each bankfield As MapBankField In _dataModel.MapBankFields
                '            If bankfield.BankField.Equals(String.Format("W{0}-Record Type-W", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                    If Not "W".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                        'Check if translated to P
                '                        For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                            If trans.BankFieldName.Equals(String.Format("W{0}-Record Type-W", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                                If "W".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                                    isTranslated = True
                '                                    Exit For
                '                                End If
                '                            End If
                '                        Next
                '                        If Not isTranslated Then
                '                            MessageBox.Show(String.Format("W{0}-'Record Type-W' is not 'W'.Please translate the 'W{0}-Record Type-W'", cnt.ToString))
                '                            Return False
                '                        End If
                '                    End If
                '                End If
                '            End If
                '            If bankfield.BankField.Equals(String.Format("I{0}-Record Type-I", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                    If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                        'Check if translated to P
                '                        For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                            If trans.BankFieldName.Equals(String.Format("I{0}-Record Type-I", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                                If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                                    isTranslated = True
                '                                    Exit For
                '                                End If
                '                            End If
                '                        Next
                '                        If Not isTranslated Then
                '                            MessageBox.Show(String.Format("I{0}-'Record Type-I' is not 'I'.Please translate the 'I{0}-Record Type-I'", cnt.ToString))
                '                            Return False
                '                        End If
                '                    End If
                '                End If
                '            End If
                '        Next
                '    Next
            Case Else
                Return True
        End Select
        Return True
    End Function
    ''' <summary>
    ''' This function is used only for formats using recordtypes
    ''' Payment SourceFields can only be mapped to Payment Bank Fields
    ''' W SourceFields can only be mapped to W Bank Fields
    ''' I SourceFields can only be mapped to I Bank Fields
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function VerifyCrossMapping(ByVal sourceField As String, ByVal currentMapBankField As MapBankField) As Boolean

        'Limits to validate that Mapping made between Payment Source and Payment Bank Fields and likewise between Invoice and WHTax Fields  
        Dim stIndex As Int32 = 0
        Dim edIndex As Int32 = _dataModel.MapBankFields.Count
        Dim pIndex As Integer = 0
        Dim wIndex As Int32 = 0 ' calculate it. Points the very first WHTax Bank Field
        Dim iIndex As Int32 = 0 ' calculate it . Points the very first Invoice Bank Field 
        Dim masterTemplate As MasterTemplateList
        Dim OutputFormat As String
        Dim currentMapBankFieldIndex As Integer
        Dim conso_field_index As Int32 = 0


        masterTemplate = bsOutputTemplate.Current
        OutputFormat = masterTemplate.OutputFormat

        currentMapBankFieldIndex = _dataModel.MapBankFields.IndexOf(currentMapBankField)
        pIndex = GetRecordTypeStartIndex(OutputFormat, "P")
        wIndex = GetRecordTypeStartIndex(OutputFormat, "W")
        iIndex = GetRecordTypeStartIndex(OutputFormat, "I")
        conso_field_index = GetRecordTypeStartIndex(OutputFormat, "Consolidate Field")

        'added for an extra field used for consolidation
        If currentMapBankFieldIndex = conso_field_index Then
        Else
            If sourceField.StartsWith("P-Field") Then

                'P Field cannot be mapped to W or I Field
                If wIndex = -1 Then
                    If currentMapBankFieldIndex < pIndex Or currentMapBankFieldIndex >= iIndex Then Return False
                Else
                    If currentMapBankFieldIndex < pIndex Or currentMapBankFieldIndex >= wIndex Then Return False
                End If
            ElseIf sourceField.StartsWith("W-Field") Then
                'W Field cannot be mapped to P or I Fields
                If wIndex = -1 Then
                Else
                    If currentMapBankFieldIndex < wIndex Or currentMapBankFieldIndex >= iIndex Then Return False
                End If
            ElseIf sourceField.StartsWith("I-Field") Then
                'I Field cannot be mapped to P or W Fields
                If currentMapBankFieldIndex < iIndex Then
                    Return False
                End If
            End If
        End If
        Return True
    End Function

    ''' <summary>
    ''' Determines whether the mapped fields are of same data type
    ''' </summary>
    ''' <returns>Boolean value indicating whether the mapping is successful</returns>
    ''' <remarks></remarks>
    Private Function VerifyMapping() As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        Dim pid As String = ""
        Dim wid As String = ""
        Dim iid As String = ""
        Dim outputformat As String = getOutputFileFormat()

        Dim obj As CommonTemplateExcelDetail = _dataModel
        pid = IIf(IsNothingOrEmptyString(obj.PIndicator), "", obj.PIndicator)
        wid = IIf(IsNothingOrEmptyString(obj.WIndicator), "", obj.WIndicator)
        iid = IIf(IsNothingOrEmptyString(obj.IIndicator), "", obj.IIndicator)

        For Each objBankField As MapBankField In _dataModel.MapBankFields

            ' If iFTS-2 Multi Line format The Record Type Indicators must have been mapped .
            ' Its a 1st Line Requirement to proceed for Tranlating or other Bank Field Manipulation
            If outputformat = iFTS2MultiLineFormat.OutputFormatString Or outputformat = OMAKASEFormat.OutputFormatString Then

                If objBankField.BankField = "Record Type-P" Then
                    If Not pid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                        errMsg = "Bank Field 'Record Type-P' must be Mapped to Correct Record Indicator Field in the Source File!"
                        Exit For
                    End If
                End If

                If objBankField.BankField = "Record Type-I" Then
                    If Not iid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                        errMsg = "Bank Field 'Record Type-I' must be Mapped to Correct Record Indicator Field in the Source File!"
                        Exit For
                    End If
                End If

                If outputformat = iFTS2MultiLineFormat.OutputFormatString Then
                    If objBankField.BankField = "Record Type-W" Then
                        If Not wid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                            errMsg = "Bank Field 'Record Type-W' must be Mapped to Correct Record Indicator Field in the Source File!"
                            Exit For
                        End If
                    End If
                End If

            End If

            If objBankField.DataType Is Nothing Then Continue For

            Select Case objBankField.DataType.ToUpper()
                Case "NUMBER", "CURRENCY"

                    If objBankField.MappedSourceFields.Count > 1 Then If MessageBox.Show(String.Format("BankField: '{0}' is mapped to more than one source fields. Do you want to continue?", objBankField.BankField), "Confirm", MessageBoxButtons.YesNo) = DialogResult.No Then Return False

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty
                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then
                            value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")

                            If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                                errMsg = String.Format(MsgReader.GetString("E02010030"), objBankField.BankField, objSourceField.SourceFieldName)
                                'errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                            End If
                        End If
                    Next

                Case "DATETIME"

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty
                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then
                            value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")
                            If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZerosInDate) Then
                                errMsg = String.Format(MsgReader.GetString("E02010030"), objBankField.BankField, objSourceField.SourceFieldName)
                                'errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                            End If
                        End If
                        If cboDateSep.Text <> "No Space" Then If cboDateSep.Text = "<Space>" Then value = value.Replace(" ", "")
                    Next
            End Select
        Next

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function


#End Region

#Region " Friend Methods "
    Friend Function ConfirmBeforeCloseAndCancel() As Boolean
        'Cancel - Return to "View" screen			
        '1	User will be prompted with a message to save, if the data has been changed.	
        '2	On "Yes"	
        '	 	New - Template will be saved as Draft.
        '	 	If user edited the Draft template then the template will be saved as Draft.
        '	 	If user edited the Non-Draft template then the template will be saved as Non-Draft.
        '3	On "No"	
        '	 	The changes made will not be saved.
        '	 	Show the "View" screen.
        '4	On "Cancel"	
        '	 	The Entry screen will remain with the data changes.
        If _dataModel Is Nothing Then
            ContainerForm.ShowView()
            Exit Function
        End If

        If _dataModel.IsDirty Then

            Dim _decision As DialogResult = MessageBox.Show(MsgReader.GetString("E10000033") _
                    , "Generate Common Transaction Template", MessageBoxButtons.YesNoCancel)

            Select Case _decision

                Case DialogResult.Yes
                    If _isNew Then
                        If Not IsSavableAsDraft() Then Return True
                        btnSaveAsDraft_Click(Nothing, Nothing)
                        ContainerForm.ShowView()
                    Else
                        If _dataModel.IsDraft Then
                            If Not IsSavableAsDraft() Then Return True
                            btnSaveAsDraft_Click(Nothing, Nothing)
                            ContainerForm.ShowView()
                        Else
                            If Not IsDataSavable() Then Return True
                            btnSave_Click(Nothing, Nothing)
                            If _dataModel.IsSavable Then
                                ContainerForm.ShowView()
                            End If
                        End If
                    End If
                Case DialogResult.No
                    ContainerForm.ShowView()
                Case DialogResult.Cancel
                    Return True
            End Select

        Else
            ContainerForm.ShowView()
        End If
        Return False
    End Function
#End Region

#Region " Event Handlers "
    Private Sub Control_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
                            Handles cboOutputTemplate.Validated, txtTemplateName.Validated, _
                                    txtSource.Validated, cboWorksheet.Validated, _
                                    txtStartCol.Validated, txtEndCol.Validated, txtSampleRowNo.Validated, _
                                    txtHeaderAtRow.Validated, txtTransactionStartRow.Validated, _
                                    cboDecimalSep.Validated, cboThousandSep.Validated, _
                                    cboDateSep.Validated, cboDateType.Validated, _
                                    txtAdviceChar.Validated, txtAdviceDelimiter.Validated


        Dim ctl As Control = CType(sender, Control)
        Dim bnd As Binding
        Dim objE As Magic.CommonTemplate.CommonTemplateExcelDetail

        objE = bsDetailCollection.Current
        For Each bnd In ctl.DataBindings
            If bnd.IsBinding Then
                Dim obj As System.ComponentModel.IDataErrorInfo = CType(objE, System.ComponentModel.IDataErrorInfo)
                ErrorProvider1.SetError(ctl, obj.Item(bnd.BindingMemberInfo.BindingField))
            End If
        Next

    End Sub
#End Region

    Private Function getOutputFileFormat() As String

        Dim filefmt As String = ""

        Try
            If Not bsOutputTemplate.Current Is Nothing Then
                Dim selMstTmp As BTMU.Magic.MasterTemplate.MasterTemplateList
                selMstTmp = bsOutputTemplate.Current
                filefmt = selMstTmp.OutputFormat
            End If
        Finally
        End Try

        Return filefmt

    End Function

    Private Sub dgvEditable_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEditable.DataError
        e.Cancel = True
    End Sub

    Private Sub dgvFilter_CellLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFilter.CellLeave
        Try
            For Each row As RowFilter In _dataModel.Filters
                row.Validate()
            Next
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvStrManip_CellLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStrManip.CellLeave
        Try
            For Each row As StringManipulation In _dataModel.StringManipulations
                row.Validate()
            Next
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
