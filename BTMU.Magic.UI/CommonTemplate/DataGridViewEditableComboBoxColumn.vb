Imports System
Imports System.ComponentModel
Imports System.Windows.Forms

''' <summary>
''' DataGridView Column to display Editable Combo Boxes
''' </summary>
''' <remarks></remarks>
Public Class DataGridViewEditableComboBoxColumn
    Inherits DataGridViewColumn

    Sub New()
        MyBase.New(New DataGridViewComboCell())

    End Sub
    ''' <summary>
    ''' This Property is to Get/Set the Cells Template
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Property CellTemplate() As DataGridViewCell

        Get
            Return MyBase.CellTemplate
        End Get

        Set(ByVal value As DataGridViewCell)
            If Not (value Is Nothing) AndAlso Not value.GetType().IsAssignableFrom(GetType(DataGridViewComboCell)) Then
                Throw New InvalidCastException("DataGridViewComboCell is expected")
            End If
            MyBase.CellTemplate = value
        End Set

    End Property
    ''' <summary>
    ''' Determines whether this Column is read only
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Property [ReadOnly]() As Boolean
        Get
            Return False
        End Get
        Set(ByVal value As Boolean)
            'MyBase.[ReadOnly] = value
        End Set
    End Property

End Class

''' <summary>
''' This Class Implements the DataGridView ComboBox Cell
''' </summary>
''' <remarks></remarks>
Public Class DataGridViewComboCell
    Inherits DataGridViewComboBoxCell

    ''' <summary>
    ''' This property is to Get the Editable Control Type
    ''' </summary>
    ''' <value>Editable Control Type</value>
    ''' <returns>Returns the Editable Control Type</returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property EditType() As Type
        Get
            Return GetType(DataGridViewComboEditingControl)
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Formatted Value Type
    ''' </summary>
    ''' <value>Formatted Value Type</value>
    ''' <returns>Returns the Formatted Value Type</returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property FormattedValueType() As System.Type
        Get
            Return GetType(String)
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get the Type of Value
    ''' </summary>
    ''' <value>Type of Value</value>
    ''' <returns>Returns the Type of Value</returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property ValueType() As System.Type
        Get
            Return GetType(String)
        End Get
    End Property

    Protected Overrides Function GetFormattedValue(ByVal value As Object, ByVal rowIndex As Integer, ByRef cellStyle As _
                            System.Windows.Forms.DataGridViewCellStyle, ByVal valueTypeConverter As _
                            System.ComponentModel.TypeConverter, ByVal formattedValueTypeConverter _
                            As System.ComponentModel.TypeConverter, ByVal context As _
                                System.Windows.Forms.DataGridViewDataErrorContexts) As Object

        If value IsNot Nothing Then Return value Else Return cellStyle.NullValue()

    End Function
    ''' <summary>
    ''' This property is Get the Formatted Value from ComboBox
    ''' </summary>
    ''' <param name="formattedValue">formatted value</param>
    ''' <param name="cellStyle">Cell Style</param>
    ''' <param name="formattedValueTypeConverter">Type Converter for the Formatted value</param>
    ''' <param name="valueTypeConverter">Type Converter</param>
    ''' <returns>Returns the formatted value</returns>
    ''' <remarks></remarks>
    Public Overrides Function ParseFormattedValue(ByVal formattedValue As _
                            Object, ByVal cellStyle As System.Windows.Forms.DataGridViewCellStyle, _
                            ByVal formattedValueTypeConverter As _
                            System.ComponentModel.TypeConverter, ByVal valueTypeConverter As _
                            System.ComponentModel.TypeConverter) As Object

        Return DirectCast(formattedValue, String)

    End Function
    ''' <summary>
    ''' Initializes the Editing Control
    ''' </summary>
    ''' <param name="rowIndex">Row Number</param>
    ''' <param name="initialFormattedValue">Initial Value</param>
    ''' <param name="dataGridViewCellStyle">Cell Style</param>
    ''' <remarks></remarks>
    Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, _
                            ByVal initialFormattedValue As Object, ByVal dataGridViewCellStyle As _
                            System.Windows.Forms.DataGridViewCellStyle)

        MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle)
        Dim editingControl As DataGridViewComboEditingControl = TryCast(MyBase.DataGridView.EditingControl, _
                                                    DataGridViewComboEditingControl)

        If (Not editingControl Is Nothing) Then
            editingControl.DropDownStyle = ComboBoxStyle.DropDown
            If Not initialFormattedValue Is Nothing Then
                editingControl.SelectedText = initialFormattedValue.ToString()
                editingControl.SelectedItemPersistent = initialFormattedValue
            End If

        End If

    End Sub

End Class

''' <summary>
''' Editable DataGridViewComboBox Control
''' </summary>
''' <remarks></remarks>
Public Class DataGridViewComboEditingControl
    Inherits ComboBox
    Implements IDataGridViewEditingControl

    Private _dataGridView As DataGridView
    Private _rowIndex As Int32
    Private _valueChanged As Boolean
    Private _valueSelectedPersistent As Object

    Property SelectedItemPersistent() As Object
        Get
            Return _valueSelectedPersistent
        End Get
        Set(ByVal value As Object)
            _valueSelectedPersistent = value
        End Set
    End Property
    ''' <summary>
    ''' Applies the Cell Style to the Control
    ''' </summary>
    ''' <param name="dataGridViewCellStyle">Cell Style</param>
    ''' <remarks></remarks>
    Public Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As System.Windows.Forms.DataGridViewCellStyle) _
     Implements System.Windows.Forms.IDataGridViewEditingControl.ApplyCellStyleToEditingControl
        Me.Font = dataGridViewCellStyle.Font
        Me.ForeColor = dataGridViewCellStyle.ForeColor
        Me.BackColor = dataGridViewCellStyle.BackColor
    End Sub
    ''' <summary>
    ''' This Property is to Get/Set the Editing Control
    ''' </summary>
    ''' <value>Editing Control</value>
    ''' <returns>Returns the Editing Control</returns>
    ''' <remarks></remarks>
    Public Property EditingControlDataGridView() As System.Windows.Forms.DataGridView _
    Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlDataGridView
        Get
            Return Me._dataGridView
        End Get
        Set(ByVal value As System.Windows.Forms.DataGridView)
            Me._dataGridView = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Set/Get the Formatted Value of Editing Control
    ''' </summary>
    ''' <value>Formatted Value</value>
    ''' <returns>Returns formatted Value</returns>
    ''' <remarks></remarks>
    Public Property EditingControlFormattedValue() As Object _
     Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlFormattedValue
        Get

            Return Me.Text
        End Get
        Set(ByVal value As Object)
            Me.Text = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Row Number of Editing Control
    ''' </summary>
    ''' <value>Row Number of Editing Control</value>
    ''' <returns>Returns Row Number of Editing Control</returns>
    ''' <remarks></remarks>
    Public Property EditingControlRowIndex() As Integer _
     Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlRowIndex
        Get
            Return Me._rowIndex
        End Get
        Set(ByVal value As Integer)
            Me._rowIndex = value
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the value of the editing control is changed
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the value of editing control is changed</returns>
    ''' <remarks></remarks>
    Public Property EditingControlValueChanged() As Boolean _
    Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlValueChanged
        Get
            Return Me._valueChanged
        End Get
        Set(ByVal value As Boolean)
            Me._valueChanged = value
            _dataGridView.NotifyCurrentCellDirty(True)
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the editing control needs the given input key
    ''' </summary>
    ''' <param name="keyData">Key Data</param>
    ''' <param name="dataGridViewWantsInputKey">Boolean value indicating whether the editing control needs the input key</param>
    ''' <returns>Returns a boolean value indicating whether the editing control needs the input key</returns>
    ''' <remarks></remarks>
    Public Function EditingControlWantsInputKey(ByVal keyData As System.Windows.Forms.Keys, ByVal dataGridViewWantsInputKey As Boolean) _
    As Boolean Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlWantsInputKey
        If keyData = Keys.F5 Then Return True
        If keyData = (Keys.Alt And Keys.Down) And Me.Enabled Then Return True
        If keyData = Keys.Down And Not Me.DroppedDown Then Return False
        If keyData = Keys.Up And Not Me.DroppedDown Then Return False
        If keyData = Keys.Right And Not Me.DroppedDown Then Return True
        If keyData = Keys.Left And Not Me.DroppedDown Then Return True
        If keyData = Keys.End And Not Me.DroppedDown Then Return True
        If keyData = Keys.Home And Not Me.DroppedDown Then Return True
        If keyData = Keys.Escape And Me.DroppedDown Then Return True
        If keyData = Keys.Down And Me.DroppedDown Then Return True
        If keyData = Keys.Up And Me.DroppedDown Then Return True
        If keyData = Keys.Enter Then _dataGridView.EndEdit()
        Return True
    End Function
    ''' <summary>
    ''' This property is to Get the cursor for Editing Panel
    ''' </summary>
    ''' <value>Cursor</value>
    ''' <returns>Returns the Cursor</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditingPanelCursor() As System.Windows.Forms.Cursor Implements System.Windows.Forms.IDataGridViewEditingControl.EditingPanelCursor
        Get
            Return MyBase.Cursor
        End Get
    End Property
    ''' <summary>
    ''' Determines whether to reposition the control when value is changed
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether to reposition the control when value is changed</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property repositioneditingcontrolonvaluechange() As Boolean Implements IDataGridViewEditingControl.RepositionEditingControlOnValueChange
        Get
            Return True
        End Get
    End Property
    ''' <summary>
    ''' Prepares the editing control for edit
    ''' </summary>
    ''' <param name="x">Boolean value indicating whether the editing is successful</param>
    ''' <remarks></remarks>
    Public Sub prepareeditingcontrolforedit(ByVal x As Boolean) Implements IDataGridViewEditingControl.PrepareEditingControlForEdit


        If Me.Items.Count > 0 Then Me.DroppedDown = True Else Me.DroppedDown = False

        Cursor.Current = EditingPanelCursor 'This is Necessary. If not, It will hide the cursor

    End Sub
    ''' <summary>
    ''' This property is to Get the Formatted Value from editing control
    ''' </summary>
    ''' <param name="x">DataGridView Error Context object</param>
    ''' <returns>Returns the Formatted Value</returns>
    ''' <remarks></remarks>
    Public Function geteditingcontrolfmtctx(ByVal x As DataGridViewDataErrorContexts) As Object Implements IDataGridViewEditingControl.GetEditingControlFormattedValue

        Return EditingControlFormattedValue()
    End Function

    Private Sub DataGridViewComboEditingControl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.TextChanged
        EditingControlValueChanged = True
    End Sub
    ''' <summary>
    ''' Function to list down items in dropdownlist combobox
    ''' </summary>
    ''' <param name="arItems">Array of Items</param>
    ''' <param name="AddEmptyOption">Boolean value indicating whether an empty option is required</param>
    ''' <remarks></remarks>
    Public Sub AddItems(ByVal arItems() As String, Optional ByVal AddEmptyOption As Boolean = False)
        Me.DropDownStyle = ComboBoxStyle.DropDownList
        Me.Items.Clear()
        If AddEmptyOption Then Me.Items.Add(" ")
        Me.Items.AddRange(arItems)
        Me.SelectedItem = SelectedItemPersistent
    End Sub

End Class