<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonTemplateText
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonTemplateText))
        Me.UcCommonTemplateTextView1 = New BTMU.Magic.UI.ucCommonTemplateTextView
        Me.UcCommonTemplateTextDetail1 = New BTMU.Magic.UI.ucCommonTemplateTextDetail
        Me.SuspendLayout()
        '
        'UcCommonTemplateTextView1
        '
        Me.UcCommonTemplateTextView1.ContainerForm = Nothing
        Me.UcCommonTemplateTextView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplateTextView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcCommonTemplateTextView1.IsNew = False
        Me.UcCommonTemplateTextView1.Location = New System.Drawing.Point(0, 0)
        Me.UcCommonTemplateTextView1.Name = "UcCommonTemplateTextView1"
        Me.UcCommonTemplateTextView1.Size = New System.Drawing.Size(909, 569)
        Me.UcCommonTemplateTextView1.TabIndex = 0
        '
        'UcCommonTemplateTextDetail1
        '
        Me.UcCommonTemplateTextDetail1.AutoScroll = True
        Me.UcCommonTemplateTextDetail1.ContainerForm = Nothing
        Me.UcCommonTemplateTextDetail1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplateTextDetail1.IsNew = False
        Me.UcCommonTemplateTextDetail1.Location = New System.Drawing.Point(0, 0)
        Me.UcCommonTemplateTextDetail1.Name = "UcCommonTemplateTextDetail1"
        Me.UcCommonTemplateTextDetail1.Size = New System.Drawing.Size(909, 569)
        Me.UcCommonTemplateTextDetail1.TabIndex = 1
        '
        'frmCommonTemplateText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(909, 569)
        Me.Controls.Add(Me.UcCommonTemplateTextDetail1)
        Me.Controls.Add(Me.UcCommonTemplateTextView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(868, 603)
        Me.Name = "frmCommonTemplateText"
        Me.ShowIcon = False
        Me.Text = "Generate Common Transaction Template - Text (MA2000)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcCommonTemplateTextView1 As BTMU.Magic.UI.ucCommonTemplateTextView
    Friend WithEvents UcCommonTemplateTextDetail1 As BTMU.Magic.UI.ucCommonTemplateTextDetail
End Class
