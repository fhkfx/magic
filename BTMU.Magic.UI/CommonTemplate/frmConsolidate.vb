Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

''' <summary>
''' This Form is to show up the User control for Data Consolidation
''' </summary>
''' <remarks></remarks>
Public Class frmConsolidate

    ''' <summary>
    ''' Displays the Data Consolidation UI 
    ''' </summary>
    ''' <param name="_objMasterTemplateList">Master Template</param>
    ''' <param name="_objDataModel">Common Template</param>
    ''' <param name="_objPreview">Transaction Records</param>
    ''' <returns>Returns a value indicating whether consolidation is successful</returns>
    ''' <remarks></remarks>
    Public Function ConsolidateData(ByVal _objMasterTemplateList As MasterTemplateList, ByVal _objDataModel As ICommonTemplate, ByVal _objPreview As basefileformat) As Boolean

        UcCommonTemplateConlidate1.ConsolidateData(_objMasterTemplateList, _objDataModel, _objPreview)

        Me.Visible = True
        Me.Show()


    End Function

    Private Sub frmConsolidate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MdiParent = frmMain
        Me.Visible = False
        Me.WindowState = FormWindowState.Normal
    End Sub

End Class