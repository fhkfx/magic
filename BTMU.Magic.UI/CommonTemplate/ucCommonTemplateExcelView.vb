Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate

''' <summary>
''' User Control for Common Transaction Template - Excel
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateExcelView

    Private viewList As New CommonTemplateExcelListCollection
    Private draftList As New CommonTemplateExcelListCollection
    Private errMsgCaption As String = "Generate Common Transaction Template - Excel"
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)


#Region " Public Properties "
    ''' <summary>
    ''' This Property is to Get Name of Common Template
    ''' </summary>
    ''' <value>Name of Common Template</value>
    ''' <returns>Returns Name of Common Template</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CommonTemplateFile() As String
        Get

            Return System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                        , dgvView.CurrentRow.Cells("dgvViewCommonTemplateText").Value.ToString()) _
                        & CommonTemplateModule.CommonTemplateFileExtension

        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Draft Template Name
    ''' </summary>
    ''' <value>Draft Template Name</value>
    ''' <returns>Returns Draft Template Name</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CommonTemplateFileDraft() As String
        Get
            Return System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelDraftFolder _
                        , dgvDraft.CurrentRow.Cells("dgvDraftCommonTemplateText").Value.ToString()) _
                         & CommonTemplateModule.CommonTemplateFileExtension
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Master Template Name
    ''' </summary>
    ''' <value>Master Template Name</value>
    ''' <returns>Returns Master Template Name</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MasterTemplateName() As String
        Get

            Return dgvView.CurrentRow.Cells("dgvViewOutputTemplateText").Value.ToString()

        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Draft Master Template Name
    ''' </summary>
    ''' <value>Draft Master Template Name</value>
    ''' <returns>Returns Draft Master Template Name</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MasterTemplateNameDraft() As String
        Get

            Return dgvView.CurrentRow.Cells("OutputTemplateDataGridViewTextBoxColumn1").Value.ToString()

        End Get
    End Property
#End Region

#Region "Event Handlers for New, View, Edit, Duplicate, Close - View tab "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ContainerForm.ShowDetail()
        ContainerForm.UcCommonTemplateExcelDetail1.AddNew()
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If dgvView.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvView.CurrentRow.Index <> -1 Then
            If MasterTemplateExists(MasterTemplateName) Then
                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateExcelDetail1.ViewExisting(CommonTemplateFile)
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvView.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvView.CurrentRow.Index <> -1 Then
            If MasterTemplateExists(MasterTemplateName) Then
                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateExcelDetail1.EditExisting(CommonTemplateFile)
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If

    End Sub

    Private Sub btnDuplicate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click
        If dgvView.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvView.CurrentRow.Index <> -1 Then
            If MasterTemplateExists(MasterTemplateName) Then
                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateExcelDetail1.DuplicateExisting(CommonTemplateFile)
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        ContainerForm.Close()
    End Sub

#End Region

#Region " Event Handlers for New, View, Edit, Duplicate, Close - Draft tab "

    Private Sub btnNewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewDraft.Click
        ContainerForm.ShowDetail()
        ContainerForm.UcCommonTemplateExcelDetail1.AddNew()
    End Sub

    Private Sub btnViewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDraft.Click
        If dgvDraft.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvDraft.CurrentRow.Index <> -1 Then
            'If MasterTemplateExists(MasterTemplateNameDraft) Then
            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateExcelDetail1.ViewExisting(CommonTemplateFileDraft)
            'Else
            '    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            'End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If
    End Sub

    Private Sub btnEditDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDraft.Click
        If dgvDraft.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvDraft.CurrentRow.Index <> -1 Then
            ' If MasterTemplateExists(MasterTemplateNameDraft) Then
            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateExcelDetail1.EditExisting(CommonTemplateFileDraft)
            'Else
            '    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            'End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If
    End Sub

    Private Sub btnDuplicateDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicateDraft.Click
        If dgvDraft.RowCount = 0 Then
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
            Exit Sub
        End If
        If dgvDraft.CurrentRow.Index <> -1 Then
            'If MasterTemplateExists(MasterTemplateNameDraft) Then
            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateExcelDetail1.DuplicateExisting(CommonTemplateFileDraft)
            'Else
            '    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
            'End If
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
        End If

    End Sub

    Private Sub btnCloseDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseDraft.Click
        ContainerForm.Close()
    End Sub

#End Region


#Region " Load View/Draft Templates "

    Private Sub ucCommonTemplateExcelView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        bsViewListCollection.DataSource = viewList.DefaultView
        dgvView.DataSource = bsViewListCollection

        bsDraftListCollection.DataSource = draftList.DefaultView
        dgvDraft.DataSource = bsDraftListCollection

        PopulateViewAndDraftGrids()

    End Sub
    ''' <summary>
    ''' Loads Templates from base directories of excel common transaction files
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateViewAndDraftGrids()
        Dim filePath As String
        Dim content As String
        Dim commonTemplateFiles As New List(Of String)
        Dim files() As String
        Dim draftFiles() As String
        Dim excelListSerializer As New XmlSerializer(GetType(MAGIC.CommonTemplate.CommonTemplateExcelList))
        Try
            bsViewListCollection.RaiseListChangedEvents = False

            draftList.Clear()
            viewList.Clear()
            If Not System.IO.Directory.Exists(CommonTemplateModule.CommonTemplateExcelFolder) Then Directory.CreateDirectory(CommonTemplateModule.CommonTemplateExcelFolder)
            If Not System.IO.Directory.Exists(CommonTemplateModule.CommonTemplateExcelDraftFolder) Then Directory.CreateDirectory(CommonTemplateModule.CommonTemplateExcelDraftFolder)

            files = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateExcelFolder, "*" & CommonTemplateModule.CommonTemplateFileExtension)
            If files.Length > 0 Then
                commonTemplateFiles.AddRange(files)
            End If
            'Added the If Condition so as to prevent duplicate entries if CommonTemplateExcelFolder and CommonTemplateExcelDraftFolder are set as the same.
            'SRS/BTMU/2010/0051
            If String.Equals(CommonTemplateModule.CommonTemplateExcelFolder.Trim, CommonTemplateModule.CommonTemplateExcelDraftFolder.Trim, StringComparison.InvariantCultureIgnoreCase) Then
                draftFiles = Nothing
            Else
                draftFiles = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateExcelDraftFolder, "*" & CommonTemplateModule.CommonTemplateFileExtension)
                If draftFiles.Length > 0 Then
                    commonTemplateFiles.AddRange(draftFiles)
                End If
            End If

            For Each filePath In commonTemplateFiles
                Try
                    Dim doc As XPathDocument = New XPathDocument(filePath)
                    Dim nav As XPathNavigator = doc.CreateNavigator()
                    Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                    Dim item As New Magic.CommonTemplate.CommonTemplateExcelList

                    Iterator.MoveNext()
                    content = Iterator.Current.Value

                    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                    stream.Position = 0
                    item = CType(excelListSerializer.Deserialize(stream), Magic.CommonTemplate.CommonTemplateExcelList)
                    If item.IsDraft Then
                        draftList.Add(item)
                    Else
                        viewList.Add(item)
                    End If
                Catch ex As Exception
                    ' Do not show any invalid files.
                End Try
            Next

            bsViewListCollection.RaiseListChangedEvents = True
            bsViewListCollection.ResetBindings(False)

            bsDraftListCollection.RaiseListChangedEvents = True
            bsDraftListCollection.ResetBindings(False)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "MA2010 - PopulateViewAndDraftGrids", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

#Region " Filter Templates "

    Private Sub FilterTemplates(ByVal bs As BindingSource, ByVal filterString As String)

        If String.IsNullOrEmpty(filterString) Then
            bs.RemoveFilter()
        Else
            Try
                bs.Filter = "CommonTemplate = '" & filterString.Trim() & "%'"
                bs.ResetBindings(False)
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, errMsgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

    End Sub

    'Private Sub txtFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
    '    FilterTemplates(bsViewListCollection, txtFilter.Text.Trim)
    'End Sub

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        FilterTemplates(bsViewListCollection, txtFilter.Text.Trim)
    End Sub

    'Private Sub txtFilterDraft_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilterDraft.TextChanged
    '    FilterTemplates(bsDraftListCollection, txtFilterDraft.Text.Trim)
    'End Sub

    Private Sub btnFilterDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterDraft.Click
        FilterTemplates(bsDraftListCollection, txtFilterDraft.Text.Trim)
    End Sub

#End Region


    

End Class
