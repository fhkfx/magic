Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.Magic.Common
''' <summary>
''' User Control to display a list of Common Transaction Templates 
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateTextView

    Private fairTemplateList As New MAGIC.CommonTemplate.TextTemplateListCollection()
    Private draftTemplateList As New MAGIC.CommonTemplate.TextTemplateListCollection()

#Region " Public Property "
    ''' <summary>
    ''' This Property is to Get Common template file
    ''' </summary>
    ''' <value>Common template file</value>
    ''' <returns>Returns Common template file</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CommonTemplateFile() As String
        Get

            Return System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                        , dgvView.CurrentRow.Cells("dgvCellCommonTemplateName").Value.ToString()) _
                        & CommonTemplateModule.CommonTemplateFileExtension

        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Draft Common Template  
    ''' </summary>
    ''' <value>Common Template</value>
    ''' <returns>Returns Common Template</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CommonTemplateFileDraft() As String
        Get
            Return System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder _
                        , dgvDraft.CurrentRow.Cells("dgvDraftCellCommonTemplateName").Value.ToString()) _
                         & CommonTemplateModule.CommonTemplateFileExtension
        End Get
    End Property
    ''' <summary>
    '''  This Property is to Get Master Template  
    ''' </summary>
    ''' <value>Master Template</value>
    ''' <returns>Returns Master Template</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MasterTemplateName() As String
        Get

            Return dgvView.CurrentRow.Cells("dgvCellMasterTemplateName").Value.ToString()

        End Get
    End Property

#End Region

#Region " Handlers for Action New/Edit/View/Duplicate/Close "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseWindowCommonTemplateText()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ContainerForm.ShowDetail()
        ContainerForm.UcCommonTemplateTextDetail1.AddNew()
    End Sub

    Private Sub E02000010()
        Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.UI.Resources", GetType(CommonTemplateModule).Assembly)
        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000010"), "")
    End Sub

    Private Sub E02000020()
        Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.UI.Resources", GetType(CommonTemplateModule).Assembly)
        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E02000020"), "")
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click

        If dgvView.CurrentRow Is Nothing Then

            E02000010()
        Else

            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateTextDetail1.ViewExisting(CommonTemplateFile)

            Else
                 E02000020()
            End If

        End If

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If dgvView.CurrentRow Is Nothing Then

           E02000010()

        Else

            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateTextDetail1.EditExisting(CommonTemplateFile)

            Else
                E02000020()
            End If

        End If

    End Sub

    Private Sub btnDuplicate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click

        If dgvView.CurrentRow Is Nothing Then

         E02000010()

        Else

            If BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.MasterTemplateExists(MasterTemplateName) Then

                ContainerForm.ShowDetail()
                ContainerForm.UcCommonTemplateTextDetail1.DuplicateExisting(CommonTemplateFile)

            Else
               E02000020()
            End If

        End If

    End Sub

    Private Sub btnEditDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then

            E02000010()
        Else

            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateTextDetail1.EditExisting(CommonTemplateFileDraft)
        End If

    End Sub

    Private Sub btnDuplicateDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicateDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then
            E02000010()

        Else
            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateTextDetail1.DuplicateExisting(CommonTemplateFileDraft, True)

        End If

    End Sub

    Private Sub btnCloseDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseDraft.Click
        CloseWindowCommonTemplateText()
    End Sub

    Private Sub CloseWindowCommonTemplateText()
        ContainerForm.Dispose()
    End Sub


#End Region

#Region " Load Fair/Draft Templates "

    Private Sub ucCommonTemplateTextView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dgvView.DataSource = bindingSrcTextTemplateCollection
        bindingSrcTextTemplateCollection.DataSource = fairTemplateList.DefaultView

        dgvDraft.DataSource = bindingSrcTextTemplateDraftCollection
        bindingSrcTextTemplateDraftCollection.DataSource = draftTemplateList.DefaultView

        PopulateViewGrids()

        tabPgView.SelectedIndex = 0
        txtFilter.Focus()

    End Sub
    ''' <summary>
    ''' Loads DataGridView with list of Templates
    ''' </summary>
    ''' <remarks></remarks>
    Friend Sub PopulateViewGrids()

        Dim filePath As String
        Dim content As String
        Dim commonTemplateFiles As New List(Of String)
        Dim draftfiles() As String


        Dim serializerTextTemplateList As New XmlSerializer(GetType(Magic.CommonTemplate.TextTemplateList))

        bindingSrcTextTemplateCollection.RaiseListChangedEvents = False
        fairTemplateList.Clear()
        draftTemplateList.Clear()

        If Not File.Exists(CommonTemplateModule.CommonTemplateTextFolder) Then Directory.CreateDirectory(CommonTemplateModule.CommonTemplateTextFolder)
        If Not File.Exists(CommonTemplateModule.CommonTemplateTextDraftFolder) Then Directory.CreateDirectory(CommonTemplateModule.CommonTemplateTextDraftFolder)

        Dim files() As String = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateTextFolder _
                           , "*" & CommonTemplateModule.CommonTemplateFileExtension)

        If files.Length > 0 Then : commonTemplateFiles.AddRange(files) : End If

        'Added the If Condition so as to prevent duplicate entries if CommonTemplateTextFolder and CommonTemplateTextFolder are set as the same.
        'SRS/BTMU/2010/0051
        If String.Equals(CommonTemplateModule.CommonTemplateTextFolder.Trim, CommonTemplateModule.CommonTemplateTextDraftFolder.Trim, StringComparison.InvariantCultureIgnoreCase) Then
            draftfiles = Nothing
        Else
            draftfiles = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateTextDraftFolder _
                          , "*" & CommonTemplateModule.CommonTemplateFileExtension)
            If draftfiles.Length > 0 Then : commonTemplateFiles.AddRange(draftfiles) : End If
        End If

        

        For Each filePath In commonTemplateFiles

            Try
                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New Magic.CommonTemplate.TextTemplateList

                Iterator.MoveNext()
                content = Iterator.Current.Value


                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerTextTemplateList.Deserialize(stream), Magic.CommonTemplate.TextTemplateList)

                If item.IsDraft Then
                    draftTemplateList.Add(item)
                Else
                    fairTemplateList.Add(item)
                End If
                stream.Dispose()
            Catch ex As Exception
            End Try
        Next
        bindingSrcTextTemplateCollection.RaiseListChangedEvents = True
        bindingSrcTextTemplateCollection.ResetBindings(False)

    End Sub
    ''' <summary>
    ''' Reloads DataGridView with list of Templates
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub reloadTemplates()
        PopulateViewGrids()
    End Sub

#End Region

#Region " Filter Templates "

  
    Private Sub btnFilterTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterTemplate.Click

        FilterTemplates(bindingSrcTextTemplateCollection, txtFilter.Text)

    End Sub


    Private Sub btnFilterDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterDraft.Click

        FilterTemplates(bindingSrcTextTemplateDraftCollection, txtFilterDraft.Text)

    End Sub

    Private Sub FilterTemplates(ByVal bs As BindingSource, ByVal filterString As String)

        If String.IsNullOrEmpty(filterString) Then

            bs.RemoveFilter()

        Else

            Try
                bs.Filter = "CommonTemplate = '" & filterString.Trim() & "%'"
                bs.ResetBindings(False)
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

    End Sub

#End Region

    Private Sub btnNewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewDraft.Click
        btnNew_Click(sender, e)
    End Sub

    Private Sub btnViewDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDraft.Click

        If dgvDraft.CurrentRow Is Nothing Then
            E02000010()
        Else
            ContainerForm.ShowDetail()
            ContainerForm.UcCommonTemplateTextDetail1.ViewExisting(CommonTemplateFileDraft)
        End If

    End Sub

End Class
