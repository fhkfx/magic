Imports System.ComponentModel

''' <summary>
''' User Control to accept nothing but numbers as input
''' </summary>
''' <remarks></remarks>
Public Class NumberBox
    ''' <summary>
    ''' This Property is to Get/Set Number input
    ''' </summary>
    ''' <value>string of numbers</value>
    ''' <returns>Returns string of numbers</returns>
    ''' <remarks></remarks>
    <Description("Digits entered into the NumberBox")> _
    <DefaultValue("")> _
    <Category("Appearance")> _
    <Bindable(True, BindingDirection.TwoWay)> _
    Public Property Number() As String
        Get
            Return txtNumber.Text
        End Get
        Set(ByVal value As String)
            txtNumber.Text = value
        End Set
    End Property
    ''' <summary>
    ''' Specifies the maximum number of digits that can be entered into the NumberBox
    ''' </summary>
    ''' <value>Length of digits</value>
    ''' <returns>Returns the length of digits</returns>
    ''' <remarks></remarks>
    <Description("Specifies the maximum number of digits that can be entered into the NumberBox")> _
    <DefaultValue("5")> _
    <Category("Appearance")> _
    Public Property MaxLength() As Int32
        Get
            Return txtNumber.MaxLength
        End Get
        Set(ByVal value As Int32)
            txtNumber.MaxLength = value
        End Set
    End Property

    ''' <summary>
    ''' Clears the Digits entered into the NumberBox
    ''' </summary>   
    Public Sub ClearDigits()
        txtNumber.Text = ""
    End Sub

    Private Sub txtnumber_keypress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress

        If ((e.KeyChar < "0") _
                      Or (e.KeyChar > "9")) _
                      And (e.KeyChar <> vbBack) _
                      And (e.KeyChar <> vbTab) Then
            e.Handled = True
        End If

    End Sub

End Class
