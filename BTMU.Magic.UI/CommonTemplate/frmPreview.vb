Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat
''' <summary>
''' Form for previewing Common Template Transaction Records
''' </summary>
''' <remarks></remarks>
Public Class frmPreview

    Private Sub frmPreview_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmConsolidate.Close()
    End Sub

    Private Sub frmPreview_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.MdiParent = frmMain
        Me.Visible = False
        Me.WindowState = FormWindowState.Normal

    End Sub
    ''' <summary>
    ''' Displays the Preview User Control
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template</param>
    ''' <param name="ObjMasterTemplateList">Master Template</param>
    ''' <param name="isQuickConversion">Boolean value indicating whether the intended conversion is a quick conversion</param>
    ''' <param name="Setup">Quick Convertion Setup information</param>
    ''' <param name="ObjPreview">Transaction records</param>
    ''' <remarks></remarks>
    Public Sub ShowPreview(ByRef ObjDataModel As ICommonTemplate, _
                           ByRef ObjMasterTemplateList As MasterTemplateList, _
                           ByVal isQuickConversion As Boolean, _
                           ByVal Setup As QuickConfigurationItem, _
                                 Optional ByRef ObjPreview As BaseFileFormat = Nothing)
        Try

            For Each _flt As RowFilter In ObjDataModel.Filters
                For Each bankField As MapBankField In ObjDataModel.MapBankFields
                    If bankField.MappedFields.Contains(_flt.FilterField) Then
                        If bankField.DataType = "DateTime" Then
                            If _flt.FilterType <> "Date" Then
                                Throw New Exception(String.Format("Filter Field {0} : DataType not match", _flt.FilterField))
                            End If
                        ElseIf bankField.DataType = "Text" Then
                            If _flt.FilterType <> "String" Then
                                Throw New Exception(String.Format("Filter Field {0} : DataType not match", _flt.FilterField))
                            End If
                        Else
                            If _flt.FilterType <> "Numeric" Then
                                Throw New Exception(String.Format("Filter Field {0} : DataType not match", _flt.FilterField))
                            End If
                        End If
                    End If
                Next
            Next


            UcCommonTemplatePreview1.ShowPreview(ObjDataModel, ObjMasterTemplateList, _
                                                 isQuickConversion, Setup, _
                                                 ObjPreview)
            'CARROT (Changed by FHK 2013-03-05)
            'If ObjMasterTemplateList.OutputFormat = "Mr.Omakase India" Then
            If ObjMasterTemplateList.OutputFormat = "Mr.Omakase India" OrElse _
                ObjMasterTemplateList.OutputFormat.Equals(CarrotCustomerDataFormat.OutputFormatString, StringComparison.InvariantCultureIgnoreCase) Then
                UcCommonTemplatePreview1.btnConsolidate.Visible = False
            Else
                UcCommonTemplatePreview1.btnConsolidate.Visible = True
            End If

            UcCommonTemplatePreview1.btnConvert.Visible = True
            Me.Visible = True
            Me.Show()
            Me.BringToFront()

        Catch ex As UnequalNumberOfFields
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error - Unequal number of Fields!", ex.Message)
            Me.Close()

        Catch ex As MagicException
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", ex.Message.ToString)
            Me.Close()

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview(PrepareForPreview):", ex.Message.ToString)
            Me.Close()
        End Try


    End Sub



End Class



