<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCommonTemplateTextView
    Inherits MAGIC.UI.ucCommonTemplateTextBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tabPgView = New System.Windows.Forms.TabControl
        Me.tbPgView = New System.Windows.Forms.TabPage
        Me.pnlBody = New System.Windows.Forms.Panel
        Me.dgvView = New System.Windows.Forms.DataGridView
        Me.bindingSrcTextTemplateCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnDuplicate = New System.Windows.Forms.Button
        Me.btnView = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.btnFilterTemplate = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtFilter = New System.Windows.Forms.TextBox
        Me.tbPgDraft = New System.Windows.Forms.TabPage
        Me.pnlBottomDraft = New System.Windows.Forms.Panel
        Me.btnNewDraft = New System.Windows.Forms.Button
        Me.btnViewDraft = New System.Windows.Forms.Button
        Me.btnCloseDraft = New System.Windows.Forms.Button
        Me.btnDuplicateDraft = New System.Windows.Forms.Button
        Me.btnEditDraft = New System.Windows.Forms.Button
        Me.pnlBodyDraft = New System.Windows.Forms.Panel
        Me.dgvDraft = New System.Windows.Forms.DataGridView
        Me.bindingSrcTextTemplateDraftCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlTopDraft = New System.Windows.Forms.Panel
        Me.btnFilterDraft = New System.Windows.Forms.Button
        Me.lblFilterByDraft = New System.Windows.Forms.Label
        Me.lblCmnTmpltDraft = New System.Windows.Forms.Label
        Me.txtFilterDraft = New System.Windows.Forms.TextBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.dgvCellCommonTemplateName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvCellMasterTemplateName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SourceFileDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IsEnabledDataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgvDraftCellCommonTemplateName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutputTemplateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SourceFileDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IsEnabledDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.tabPgView.SuspendLayout()
        Me.tbPgView.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcTextTemplateCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBottom.SuspendLayout()
        Me.pnlTop.SuspendLayout()
        Me.tbPgDraft.SuspendLayout()
        Me.pnlBottomDraft.SuspendLayout()
        Me.pnlBodyDraft.SuspendLayout()
        CType(Me.dgvDraft, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSrcTextTemplateDraftCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTopDraft.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabPgView
        '
        Me.tabPgView.Controls.Add(Me.tbPgView)
        Me.tabPgView.Controls.Add(Me.tbPgDraft)
        Me.tabPgView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabPgView.Location = New System.Drawing.Point(0, 0)
        Me.tabPgView.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tabPgView.Name = "tabPgView"
        Me.tabPgView.SelectedIndex = 0
        Me.tabPgView.Size = New System.Drawing.Size(653, 511)
        Me.tabPgView.TabIndex = 0
        '
        'tbPgView
        '
        Me.tbPgView.Controls.Add(Me.pnlBody)
        Me.tbPgView.Controls.Add(Me.pnlBottom)
        Me.tbPgView.Controls.Add(Me.pnlTop)
        Me.tbPgView.Location = New System.Drawing.Point(4, 23)
        Me.tbPgView.Name = "tbPgView"
        Me.tbPgView.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPgView.Size = New System.Drawing.Size(645, 484)
        Me.tbPgView.TabIndex = 0
        Me.tbPgView.Text = "View"
        Me.tbPgView.UseVisualStyleBackColor = True
        '
        'pnlBody
        '
        Me.pnlBody.Controls.Add(Me.dgvView)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(3, 63)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(639, 366)
        Me.pnlBody.TabIndex = 7
        '
        'dgvView
        '
        Me.dgvView.AllowUserToAddRows = False
        Me.dgvView.AllowUserToDeleteRows = False
        Me.dgvView.AllowUserToResizeRows = False
        Me.dgvView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvView.AutoGenerateColumns = False
        Me.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvCellCommonTemplateName, Me.dgvCellMasterTemplateName, Me.SourceFileDataGridViewTextBoxColumn1, Me.IsEnabledDataGridViewCheckBoxColumn1})
        Me.dgvView.DataSource = Me.bindingSrcTextTemplateCollection
        Me.dgvView.Location = New System.Drawing.Point(3, 3)
        Me.dgvView.MultiSelect = False
        Me.dgvView.Name = "dgvView"
        Me.dgvView.ReadOnly = True
        Me.dgvView.RowHeadersVisible = False
        Me.dgvView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvView.Size = New System.Drawing.Size(632, 355)
        Me.dgvView.TabIndex = 0
        '
        'bindingSrcTextTemplateCollection
        '
        Me.bindingSrcTextTemplateCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.TextTemplateListCollection)
        Me.bindingSrcTextTemplateCollection.Filter = ""
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.btnClose)
        Me.pnlBottom.Controls.Add(Me.btnNew)
        Me.pnlBottom.Controls.Add(Me.btnDuplicate)
        Me.pnlBottom.Controls.Add(Me.btnView)
        Me.pnlBottom.Controls.Add(Me.btnEdit)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(3, 429)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(639, 52)
        Me.pnlBottom.TabIndex = 6
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(322, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 35)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(3, 9)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 35)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDuplicate
        '
        Me.btnDuplicate.Location = New System.Drawing.Point(243, 9)
        Me.btnDuplicate.Name = "btnDuplicate"
        Me.btnDuplicate.Size = New System.Drawing.Size(75, 35)
        Me.btnDuplicate.TabIndex = 3
        Me.btnDuplicate.Text = "Dupli&cate"
        Me.btnDuplicate.UseVisualStyleBackColor = True
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(83, 9)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(75, 35)
        Me.btnView.TabIndex = 1
        Me.btnView.Text = "&View"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(163, 9)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 35)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "E&dit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.btnFilterTemplate)
        Me.pnlTop.Controls.Add(Me.Label1)
        Me.pnlTop.Controls.Add(Me.Label2)
        Me.pnlTop.Controls.Add(Me.txtFilter)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(3, 3)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(639, 60)
        Me.pnlTop.TabIndex = 5
        '
        'btnFilterTemplate
        '
        Me.btnFilterTemplate.Location = New System.Drawing.Point(455, 24)
        Me.btnFilterTemplate.Name = "btnFilterTemplate"
        Me.btnFilterTemplate.Size = New System.Drawing.Size(75, 26)
        Me.btnFilterTemplate.TabIndex = 1
        Me.btnFilterTemplate.Text = "&Filter"
        Me.btnFilterTemplate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Filter By:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Common Template Name :"
        '
        'txtFilter
        '
        Me.txtFilter.Location = New System.Drawing.Point(150, 28)
        Me.txtFilter.MaxLength = 100
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(292, 20)
        Me.txtFilter.TabIndex = 0
        '
        'tbPgDraft
        '
        Me.tbPgDraft.Controls.Add(Me.pnlBottomDraft)
        Me.tbPgDraft.Controls.Add(Me.pnlBodyDraft)
        Me.tbPgDraft.Controls.Add(Me.pnlTopDraft)
        Me.tbPgDraft.Location = New System.Drawing.Point(4, 23)
        Me.tbPgDraft.Name = "tbPgDraft"
        Me.tbPgDraft.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPgDraft.Size = New System.Drawing.Size(645, 484)
        Me.tbPgDraft.TabIndex = 1
        Me.tbPgDraft.Text = "View Draft"
        Me.tbPgDraft.UseVisualStyleBackColor = True
        '
        'pnlBottomDraft
        '
        Me.pnlBottomDraft.Controls.Add(Me.btnNewDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnViewDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnCloseDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnDuplicateDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnEditDraft)
        Me.pnlBottomDraft.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottomDraft.Location = New System.Drawing.Point(3, 429)
        Me.pnlBottomDraft.Name = "pnlBottomDraft"
        Me.pnlBottomDraft.Size = New System.Drawing.Size(639, 52)
        Me.pnlBottomDraft.TabIndex = 9
        '
        'btnNewDraft
        '
        Me.btnNewDraft.Location = New System.Drawing.Point(3, 9)
        Me.btnNewDraft.Name = "btnNewDraft"
        Me.btnNewDraft.Size = New System.Drawing.Size(75, 35)
        Me.btnNewDraft.TabIndex = 0
        Me.btnNewDraft.Text = "&New"
        Me.btnNewDraft.UseVisualStyleBackColor = True
        '
        'btnViewDraft
        '
        Me.btnViewDraft.Location = New System.Drawing.Point(83, 9)
        Me.btnViewDraft.Name = "btnViewDraft"
        Me.btnViewDraft.Size = New System.Drawing.Size(75, 35)
        Me.btnViewDraft.TabIndex = 1
        Me.btnViewDraft.Text = "&View"
        Me.btnViewDraft.UseVisualStyleBackColor = True
        '
        'btnCloseDraft
        '
        Me.btnCloseDraft.Location = New System.Drawing.Point(322, 9)
        Me.btnCloseDraft.Name = "btnCloseDraft"
        Me.btnCloseDraft.Size = New System.Drawing.Size(75, 35)
        Me.btnCloseDraft.TabIndex = 4
        Me.btnCloseDraft.Text = "Close"
        Me.btnCloseDraft.UseVisualStyleBackColor = True
        '
        'btnDuplicateDraft
        '
        Me.btnDuplicateDraft.Location = New System.Drawing.Point(243, 9)
        Me.btnDuplicateDraft.Name = "btnDuplicateDraft"
        Me.btnDuplicateDraft.Size = New System.Drawing.Size(75, 35)
        Me.btnDuplicateDraft.TabIndex = 3
        Me.btnDuplicateDraft.Text = "Dupli&cate"
        Me.btnDuplicateDraft.UseVisualStyleBackColor = True
        '
        'btnEditDraft
        '
        Me.btnEditDraft.Location = New System.Drawing.Point(163, 9)
        Me.btnEditDraft.Name = "btnEditDraft"
        Me.btnEditDraft.Size = New System.Drawing.Size(75, 35)
        Me.btnEditDraft.TabIndex = 2
        Me.btnEditDraft.Text = "E&dit"
        Me.btnEditDraft.UseVisualStyleBackColor = True
        '
        'pnlBodyDraft
        '
        Me.pnlBodyDraft.Controls.Add(Me.dgvDraft)
        Me.pnlBodyDraft.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBodyDraft.Location = New System.Drawing.Point(3, 63)
        Me.pnlBodyDraft.Name = "pnlBodyDraft"
        Me.pnlBodyDraft.Size = New System.Drawing.Size(639, 418)
        Me.pnlBodyDraft.TabIndex = 8
        '
        'dgvDraft
        '
        Me.dgvDraft.AllowUserToAddRows = False
        Me.dgvDraft.AllowUserToDeleteRows = False
        Me.dgvDraft.AllowUserToResizeRows = False
        Me.dgvDraft.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDraft.AutoGenerateColumns = False
        Me.dgvDraft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDraft.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvDraftCellCommonTemplateName, Me.OutputTemplateDataGridViewTextBoxColumn, Me.SourceFileDataGridViewTextBoxColumn, Me.IsEnabledDataGridViewCheckBoxColumn})
        Me.dgvDraft.DataSource = Me.bindingSrcTextTemplateDraftCollection
        Me.dgvDraft.Location = New System.Drawing.Point(3, 3)
        Me.dgvDraft.MultiSelect = False
        Me.dgvDraft.Name = "dgvDraft"
        Me.dgvDraft.ReadOnly = True
        Me.dgvDraft.RowHeadersVisible = False
        Me.dgvDraft.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDraft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDraft.Size = New System.Drawing.Size(632, 355)
        Me.dgvDraft.TabIndex = 0
        '
        'bindingSrcTextTemplateDraftCollection
        '
        Me.bindingSrcTextTemplateDraftCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.TextTemplateListCollection)
        '
        'pnlTopDraft
        '
        Me.pnlTopDraft.Controls.Add(Me.btnFilterDraft)
        Me.pnlTopDraft.Controls.Add(Me.lblFilterByDraft)
        Me.pnlTopDraft.Controls.Add(Me.lblCmnTmpltDraft)
        Me.pnlTopDraft.Controls.Add(Me.txtFilterDraft)
        Me.pnlTopDraft.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTopDraft.Location = New System.Drawing.Point(3, 3)
        Me.pnlTopDraft.Name = "pnlTopDraft"
        Me.pnlTopDraft.Size = New System.Drawing.Size(639, 60)
        Me.pnlTopDraft.TabIndex = 6
        '
        'btnFilterDraft
        '
        Me.btnFilterDraft.Location = New System.Drawing.Point(455, 24)
        Me.btnFilterDraft.Name = "btnFilterDraft"
        Me.btnFilterDraft.Size = New System.Drawing.Size(75, 26)
        Me.btnFilterDraft.TabIndex = 1
        Me.btnFilterDraft.Text = "&Filter"
        Me.btnFilterDraft.UseVisualStyleBackColor = True
        '
        'lblFilterByDraft
        '
        Me.lblFilterByDraft.AutoSize = True
        Me.lblFilterByDraft.Location = New System.Drawing.Point(1, 8)
        Me.lblFilterByDraft.Name = "lblFilterByDraft"
        Me.lblFilterByDraft.Size = New System.Drawing.Size(49, 14)
        Me.lblFilterByDraft.TabIndex = 0
        Me.lblFilterByDraft.Text = "Filter By:"
        Me.lblFilterByDraft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCmnTmpltDraft
        '
        Me.lblCmnTmpltDraft.AutoSize = True
        Me.lblCmnTmpltDraft.Location = New System.Drawing.Point(1, 31)
        Me.lblCmnTmpltDraft.Name = "lblCmnTmpltDraft"
        Me.lblCmnTmpltDraft.Size = New System.Drawing.Size(130, 14)
        Me.lblCmnTmpltDraft.TabIndex = 1
        Me.lblCmnTmpltDraft.Text = "Common Template Name :"
        '
        'txtFilterDraft
        '
        Me.txtFilterDraft.Location = New System.Drawing.Point(150, 28)
        Me.txtFilterDraft.MaxLength = 100
        Me.txtFilterDraft.Name = "txtFilterDraft"
        Me.txtFilterDraft.Size = New System.Drawing.Size(292, 20)
        Me.txtFilterDraft.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(742, 401)
        Me.DataGridView1.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(377, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 28)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "C&lose"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(5, 6)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 28)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "&New"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(284, 6)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 28)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&Duplicate"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(98, 6)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 28)
        Me.Button4.TabIndex = 1
        Me.Button4.Text = "&View"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(191, 6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(87, 28)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&Edit"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Filter By:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Common Template Name :"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(175, 30)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(340, 20)
        Me.TextBox1.TabIndex = 2
        '
        'dgvCellCommonTemplateName
        '
        Me.dgvCellCommonTemplateName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCellCommonTemplateName.DataPropertyName = "CommonTemplate"
        Me.dgvCellCommonTemplateName.HeaderText = "Common Template Name"
        Me.dgvCellCommonTemplateName.Name = "dgvCellCommonTemplateName"
        Me.dgvCellCommonTemplateName.ReadOnly = True
        '
        'dgvCellMasterTemplateName
        '
        Me.dgvCellMasterTemplateName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCellMasterTemplateName.DataPropertyName = "OutputTemplate"
        Me.dgvCellMasterTemplateName.HeaderText = "Master Template Name"
        Me.dgvCellMasterTemplateName.Name = "dgvCellMasterTemplateName"
        Me.dgvCellMasterTemplateName.ReadOnly = True
        '
        'SourceFileDataGridViewTextBoxColumn1
        '
        Me.SourceFileDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SourceFileDataGridViewTextBoxColumn1.DataPropertyName = "SourceFile"
        Me.SourceFileDataGridViewTextBoxColumn1.HeaderText = "Source File Name"
        Me.SourceFileDataGridViewTextBoxColumn1.Name = "SourceFileDataGridViewTextBoxColumn1"
        Me.SourceFileDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'IsEnabledDataGridViewCheckBoxColumn1
        '
        Me.IsEnabledDataGridViewCheckBoxColumn1.DataPropertyName = "IsEnabled"
        Me.IsEnabledDataGridViewCheckBoxColumn1.HeaderText = "Template Enabled"
        Me.IsEnabledDataGridViewCheckBoxColumn1.Name = "IsEnabledDataGridViewCheckBoxColumn1"
        Me.IsEnabledDataGridViewCheckBoxColumn1.ReadOnly = True
        Me.IsEnabledDataGridViewCheckBoxColumn1.Width = 150
        '
        'dgvDraftCellCommonTemplateName
        '
        Me.dgvDraftCellCommonTemplateName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvDraftCellCommonTemplateName.DataPropertyName = "CommonTemplate"
        Me.dgvDraftCellCommonTemplateName.HeaderText = "Common Template Name"
        Me.dgvDraftCellCommonTemplateName.Name = "dgvDraftCellCommonTemplateName"
        Me.dgvDraftCellCommonTemplateName.ReadOnly = True
        '
        'OutputTemplateDataGridViewTextBoxColumn
        '
        Me.OutputTemplateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OutputTemplateDataGridViewTextBoxColumn.DataPropertyName = "OutputTemplate"
        Me.OutputTemplateDataGridViewTextBoxColumn.HeaderText = "Master Template Name"
        Me.OutputTemplateDataGridViewTextBoxColumn.Name = "OutputTemplateDataGridViewTextBoxColumn"
        Me.OutputTemplateDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SourceFileDataGridViewTextBoxColumn
        '
        Me.SourceFileDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SourceFileDataGridViewTextBoxColumn.DataPropertyName = "SourceFile"
        Me.SourceFileDataGridViewTextBoxColumn.HeaderText = "Source File Name"
        Me.SourceFileDataGridViewTextBoxColumn.Name = "SourceFileDataGridViewTextBoxColumn"
        Me.SourceFileDataGridViewTextBoxColumn.ReadOnly = True
        '
        'IsEnabledDataGridViewCheckBoxColumn
        '
        Me.IsEnabledDataGridViewCheckBoxColumn.DataPropertyName = "IsEnabled"
        Me.IsEnabledDataGridViewCheckBoxColumn.HeaderText = "Template Enabled"
        Me.IsEnabledDataGridViewCheckBoxColumn.Name = "IsEnabledDataGridViewCheckBoxColumn"
        Me.IsEnabledDataGridViewCheckBoxColumn.ReadOnly = True
        Me.IsEnabledDataGridViewCheckBoxColumn.Width = 150
        '
        'ucCommonTemplateTextView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.tabPgView)
        Me.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Name = "ucCommonTemplateTextView"
        Me.Size = New System.Drawing.Size(653, 511)
        Me.tabPgView.ResumeLayout(False)
        Me.tbPgView.ResumeLayout(False)
        Me.pnlBody.ResumeLayout(False)
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcTextTemplateCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBottom.ResumeLayout(False)
        Me.pnlTop.ResumeLayout(False)
        Me.pnlTop.PerformLayout()
        Me.tbPgDraft.ResumeLayout(False)
        Me.pnlBottomDraft.ResumeLayout(False)
        Me.pnlBodyDraft.ResumeLayout(False)
        CType(Me.dgvDraft, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSrcTextTemplateDraftCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTopDraft.ResumeLayout(False)
        Me.pnlTopDraft.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabPgView As System.Windows.Forms.TabControl
    Friend WithEvents tbPgView As System.Windows.Forms.TabPage
    Friend WithEvents tbPgDraft As System.Windows.Forms.TabPage
    Friend WithEvents pnlBody As System.Windows.Forms.Panel
    Friend WithEvents dgvView As System.Windows.Forms.DataGridView
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnDuplicate As System.Windows.Forms.Button
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents pnlBodyDraft As System.Windows.Forms.Panel
    Friend WithEvents dgvDraft As System.Windows.Forms.DataGridView
    Friend WithEvents pnlTopDraft As System.Windows.Forms.Panel
    Friend WithEvents lblFilterByDraft As System.Windows.Forms.Label
    Friend WithEvents lblCmnTmpltDraft As System.Windows.Forms.Label
    Friend WithEvents txtFilterDraft As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents pnlBottomDraft As System.Windows.Forms.Panel
    Friend WithEvents btnCloseDraft As System.Windows.Forms.Button
    Friend WithEvents btnDuplicateDraft As System.Windows.Forms.Button
    Friend WithEvents btnEditDraft As System.Windows.Forms.Button
    Friend WithEvents bindingSrcTextTemplateCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bindingSrcTextTemplateDraftCollection As System.Windows.Forms.BindingSource
    Friend WithEvents btnFilterTemplate As System.Windows.Forms.Button
    Friend WithEvents btnFilterDraft As System.Windows.Forms.Button
    Friend WithEvents btnNewDraft As System.Windows.Forms.Button
    Friend WithEvents btnViewDraft As System.Windows.Forms.Button
    Friend WithEvents dgvCellCommonTemplateName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvCellMasterTemplateName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceFileDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvDraftCellCommonTemplateName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutputTemplateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceFileDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class
