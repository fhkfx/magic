Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat

''' <summary>
''' User Control displaying the Consolidate Form 
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateConsolidate

    Private _preview As BaseFileFormat
    Private _groupByFields As DataTable
    Private _referenceFields As DataTable
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Private Sub btnConsolidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsolidate.Click
        DoConsolidate()
    End Sub

    ''' <summary>
    ''' Consolidates the Transaction Data
    ''' </summary>
    ''' <param name="_objMasterTemplateList">Master Template</param>
    ''' <param name="_objDataModel">Common Template</param>
    ''' <param name="_objPreview">Transaction Records</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsolidateData(ByVal _objMasterTemplateList As MasterTemplateList, _
                                    ByVal _objDataModel As ICommonTemplate, _
                                    ByVal _objPreview As BaseFileFormat) As Boolean

        _preview = _objPreview
        _preview.IsConsolidated = True
        _preview.PaymentOption = "+"

        Select Case _objMasterTemplateList.OutputFormat.Trim.ToUpper
            Case "IRTMSCI", "IRTMSGC", "IRTMSGD", "IRTMSRM", "OIRTMSCI", "OIRTMSGC", "OIRTMSGD", "OIRTMSRM"
                AdviceRecordGroupBox.Text = "Advice Record"
                chkSumUpRemittanceAmount.Text = "Sum up Remittance Amount"
                AdviceRecordGroupBox.Visible = True

                'added for iFTS-2 single line consolidation enhancment
            Case "IFTS-2 SINGLELINE", "IFTS-2 MULTILINE"
                AdviceRecordGroupBox.Text = "Payment Record"
                chkSumUpRemittanceAmount.Text = "Sum up Gross Amount"
                AdviceRecordGroupBox.Visible = True

            Case Else
                AdviceRecordGroupBox.Visible = False
        End Select

        'Populate Group By Fields
        _groupByFields = New DataTable
        _groupByFields.Columns.Add("Fields", GetType(String))

        For Each _field As String In _preview.GroupByFields
            _groupByFields.Rows.Add(_field)
        Next
        dgvGroupByCellFieldCombo.DataSource = _groupByFields
        dgvGroupByCellFieldCombo.DisplayMember = "Fields"
        dgvGroupByCellFieldCombo.ValueMember = "Fields"

        'Populate Reference Fields
        _referenceFields = New DataTable
        _referenceFields.Columns.Add("Fields", GetType(String))

        For Each _field As String In _preview.ReferenceFields
            _referenceFields.Rows.Add(_field)
        Next
        dgvReferenceCellFieldCombo.DataSource = _referenceFields
        dgvReferenceCellFieldCombo.DisplayMember = "Fields"
        dgvReferenceCellFieldCombo.ValueMember = "Fields"

        If _objMasterTemplateList.OutputFormat.Trim.ToUpper = "VPS" Then
            If dgvGroupBy.Rows.Count = 1 Then
                Dim venCode() As String = {"X", "VenCode"}
                Dim venName1() As String = {"X", "VenName1"}
                Dim venName2() As String = {"X", "VenName2"}
                Dim venAdd1() As String = {"X", "VenAdd1"}
                Dim venAdd2() As String = {"X", "VenAdd2"}
                Dim venAdd3() As String = {"X", "VenAdd3"}
                Dim venAdd4() As String = {"X", "VenAdd4"}
                dgvGroupBy.Rows.Add(venCode)
                dgvGroupBy.Rows.Add(venName1)
                dgvGroupBy.Rows.Add(venName2)
                dgvGroupBy.Rows.Add(venAdd1)
                dgvGroupBy.Rows.Add(venAdd2)
                dgvGroupBy.Rows.Add(venAdd3)
                dgvGroupBy.Rows.Add(venAdd4)
            End If
        End If

        'Added for Mr. Omakase II format
        'During UAT, requested by Casey (BTMU)
        'Fixed by Kay @ 18/10/2010
        If _objMasterTemplateList.OutputFormat.Trim.ToUpper() = "Mr.Omakase India II".ToUpper() Then
            If dgvGroupBy.Rows.Count = 1 Then

                Dim transType() As String = {"X", "Transaction Type"}
                Dim beneIFSC() As String = {"X", "Beneficiary Bank Branch IFSC code"}
                Dim beneBank() As String = {"X", "Beneficiary Bank Name"}
                Dim beneBranch() As String = {"X", "Beneficiary Branch Name"}
                Dim beneACNo() As String = {"X", "Beneficiary A/C No."}
                Dim beneName() As String = {"X", "Beneficiary Name"}
                Dim payeeName1() As String = {"X", "Payee Name-1"}
                Dim payeeName2() As String = {"X", "Payee Name-2"}
                Dim appEmailAdd() As String = {"X", "Applicant E-Mail Address"}
                Dim mailingName() As String = {"X", "Mailing Name"}
                dgvGroupBy.Rows.Add(transType)
                dgvGroupBy.Rows.Add(beneIFSC)
                dgvGroupBy.Rows.Add(beneBank)
                dgvGroupBy.Rows.Add(beneBranch)
                dgvGroupBy.Rows.Add(beneACNo)
                dgvGroupBy.Rows.Add(beneName)
                dgvGroupBy.Rows.Add(payeeName1)
                dgvGroupBy.Rows.Add(payeeName2)
                dgvGroupBy.Rows.Add(appEmailAdd)
                dgvGroupBy.Rows.Add(mailingName)
            End If
        End If


        'Added for GCMS, iFTS-2 and iRTMS formats
        'After UAT, requested by Casey (BTMU)
        'Fixed by Kay @ 29/04/2011

        'Added for GCMSPlus and FTI, AutoDebit, AutoCheque formats too.
        Select Case _objMasterTemplateList.OutputFormat.Trim.ToUpper
            Case "GCMS", "GCMSPLUS", "IRTMSCI", "IRTMSGC", "IRTMSGD", "IRTMSRM", "OIRTMSCI", "OIRTMSGC", "OIRTMSGD", "OIRTMSRM", "IFTS-2 SINGLELINE", "IFTS-2 MULTILINE", "FTI", "FTI II", "AUTODEBIT", "AUTOCHEQUE", "CARROTINVOICE", "CASHFORECASTING"
                If dgvGroupBy.Rows.Count = 1 Then
                    Dim consoField() As String = {"X", "Consolidate Field"}
                    Try
                        dgvGroupBy.Rows.Add(consoField)
                    Catch ex As Exception
                        Throw New Exception("Error on Mapping(CopyMapping) : " & vbCrLf & "Common Template or Master Template used is not compatible with this version. Please check the followings:" _
                                            & vbCrLf & "1.) Ensure the program version is later than 3.13." _
                                            & vbCrLf & "2.) Ensure the 'Consolidate Field' field is defined in master template." _
                                            & vbCrLf & "3.) Ensure the'Consolidate Field' is listed at the end of bank fields list in Common template mapping.")
                    End Try
                End If
        End Select

    End Function

    ''' <summary>
    ''' Applies the Reference Fields and Group by fields to consolidate data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DoConsolidate() As Boolean

        If Not frmPreview.Visible Then Exit Function

        Dim groupByFields As New List(Of String)
        Dim referenceFields As New List(Of String)
        Dim appendFields As New List(Of String)

        Try

            For intI As Integer = 0 To dgvGroupBy.RowCount - 2

                If dgvGroupBy.Item(1, intI).Value IsNot Nothing Then groupByFields.Add(dgvGroupBy.Item(1, intI).Value.ToString)

            Next

            For intI As Integer = 0 To dgvReferenceFields.RowCount - 2

                If dgvReferenceFields.Item(2, intI).Value IsNot Nothing Then
                    If dgvReferenceFields.Item(1, intI).Value Is Nothing Then
                        appendFields.Add("")
                    Else
                        appendFields.Add(dgvReferenceFields.Item(1, intI).Value.ToString)
                    End If

                    referenceFields.Add(dgvReferenceFields.Item(2, intI).Value.ToString)
                End If

            Next

            _preview.IsConsolidated = True
            _preview.IsNegativePaymentOption = rdoNegativePaymentOption.Checked
            _preview.IsPositivePaymentOption = rdoPositivePaymentOption.Checked
            _preview.IsCheckedSumAmount = chkSumUpRemittanceAmount.Checked

            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = False

            _preview.GenerateConsolidatedData(groupByFields, referenceFields, appendFields)

            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.RaiseListChangedEvents = True
            frmPreview.UcCommonTemplatePreview1._previewDataBindingSource.ResetBindings(False)

            ' Call the function for PrepareGridForDisplay
            frmPreview.UcCommonTemplatePreview1.PrepareGridForDisplay()
            'MsgBox(frmPreview.UcCommonTemplatePreview1.labelValueConsolidatedRecords.Text)
            frmPreview.UcCommonTemplatePreview1.labelValueConsolidatedRecords.Text = _preview.ConsolidatedRecords
            frmPreview.Show()

        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error on consolidating data", ex.Message)
        End Try

        _preview.PaymentOption = IIf(rdoPositivePaymentOption.Checked, "+", "-")

    End Function

    Private Sub dgvGroupBy_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGroupBy.CellContentClick
        Try

            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvGroupBy.Rows(e.RowIndex).Cells("dgvGroupByCellDeleteBtn").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
            String.Format(MsgReader.GetString("E10000045") _
                    , "Field", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                dgvGroupBy.Rows.RemoveAt(e.RowIndex)
            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Consolidate Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvReferenceFields_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReferenceFields.CellContentClick
        Try

            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvReferenceFields.Rows(e.RowIndex).Cells("dgvReferenceCellDeleteBtn").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
            String.Format(MsgReader.GetString("E10000045") _
                    , "Field", e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                dgvReferenceFields.Rows.RemoveAt(e.RowIndex)
            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Consolidate Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ucCommonTemplateConsolidate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AdviceRecordGroupBox.Visible = False ' Visible only for iRTMS formats
        rdoPositivePaymentOption.Checked = True ' By Default
        'Added the follwing code for Consolidation Enhancement made @ 2011 Mar 
        'If Me.ParentForm.Name.Equals(frmSwiftTemplate.Name) Or Me.ParentForm.Name.Contains("Common") Then
        '    btnConsolidate.Visible = False
        '    AdviceRecordGroupBox.Visible = True
        '    'panelBottom.Height = panelBottom.Height - btnConsolidate.Height
        '    'groupBoxPaymentOptions.Top = 
        'End If

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'check parent form

    End Sub
End Class
