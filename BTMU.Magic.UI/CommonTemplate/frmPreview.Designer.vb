<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UcCommonTemplatePreview1 = New BTMU.Magic.UI.ucCommonTemplatePreview
        Me.SuspendLayout()
        '
        'UcCommonTemplatePreview1
        '
        Me.UcCommonTemplatePreview1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplatePreview1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcCommonTemplatePreview1.Location = New System.Drawing.Point(0, 0)
        Me.UcCommonTemplatePreview1.Name = "UcCommonTemplatePreview1"
        Me.UcCommonTemplatePreview1.Size = New System.Drawing.Size(934, 664)
        Me.UcCommonTemplatePreview1.TabIndex = 0
        '
        'frmPreview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(934, 664)
        Me.Controls.Add(Me.UcCommonTemplatePreview1)
        Me.DoubleBuffered = True
        Me.MinimizeBox = False
        Me.Name = "frmPreview"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Preview Common Transaction"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcCommonTemplatePreview1 As BTMU.Magic.UI.ucCommonTemplatePreview
End Class
