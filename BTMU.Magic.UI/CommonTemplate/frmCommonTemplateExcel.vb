
''' <summary>
''' Form to dispaly the User Control of Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
Public Class frmCommonTemplateExcel

    Private Sub frmCommonTemplateExcel_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If UcCommonTemplateExcelDetail1.Visible And Not UcCommonTemplateExcelDetail1.isView Then
            If UcCommonTemplateExcelDetail1.ConfirmBeforeCloseAndCancel() Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CommonTemplateForExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UcCommonTemplateExcelView1.ContainerForm = Me
        UcCommonTemplateExcelDetail1.ContainerForm = Me
        ShowView()
    End Sub
    ''' <summary>
    ''' Displays the View Panel to list out templates
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowView()
        UcCommonTemplateExcelView1.PopulateViewAndDraftGrids()
        UcCommonTemplateExcelView1.Visible = True
        UcCommonTemplateExcelDetail1.Visible = False
        UcCommonTemplateExcelView1.tabPgView.SelectedIndex = 0
        UcCommonTemplateExcelView1.tbPgView.Focus()
        UcCommonTemplateExcelView1.tbPgView.Select()
    End Sub
    ''' <summary>
    ''' Displays the Detail Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDetail()
        UcCommonTemplateExcelView1.Visible = False
        UcCommonTemplateExcelDetail1.Visible = True
    End Sub
    ''' <summary>
    ''' Displays the View panel to list out the Draft Templates
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDraftView()
        ShowView()
        UcCommonTemplateExcelView1.tabPgView.SelectedIndex = 1
        UcCommonTemplateExcelView1.tbPgDraft.Focus()
        UcCommonTemplateExcelView1.tbPgDraft.Select()
    End Sub

    Private Sub UcCommonTemplateExcelView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UcCommonTemplateExcelView1.Load

    End Sub
End Class