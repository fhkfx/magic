<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCommonTemplateExcelView
    Inherits MAGIC.UI.ucCommonTemplateExcelBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnDuplicate = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlDraftTop = New System.Windows.Forms.Panel
        Me.btnFilterDraft = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtFilterDraft = New System.Windows.Forms.TextBox
        Me.dgvDraft = New System.Windows.Forms.DataGridView
        Me.dgvDraftCommonTemplateText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutputTemplateDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SourceFileDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WorksheetNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IsEnabledDataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.bsDraftListCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnView = New System.Windows.Forms.Button
        Me.btnEditDraft = New System.Windows.Forms.Button
        Me.pnlFill = New System.Windows.Forms.Panel
        Me.tabPgView = New System.Windows.Forms.TabControl
        Me.tbPgView = New System.Windows.Forms.TabPage
        Me.pnlViewFill = New System.Windows.Forms.Panel
        Me.dgvView = New System.Windows.Forms.DataGridView
        Me.dgvViewCommonTemplateText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvViewOutputTemplateText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvViewSourceFileText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvViewWorksheetNameText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvViewEnabledText = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.bsViewListCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlViewTop = New System.Windows.Forms.Panel
        Me.btnFilter = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtFilter = New System.Windows.Forms.TextBox
        Me.pnlViewBottom = New System.Windows.Forms.Panel
        Me.btnEdit = New System.Windows.Forms.Button
        Me.tbPgDraft = New System.Windows.Forms.TabPage
        Me.pnlDraftFill = New System.Windows.Forms.Panel
        Me.pnlBottomDraft = New System.Windows.Forms.Panel
        Me.btnCloseDraft = New System.Windows.Forms.Button
        Me.btnNewDraft = New System.Windows.Forms.Button
        Me.btnViewDraft = New System.Windows.Forms.Button
        Me.btnDuplicateDraft = New System.Windows.Forms.Button
        Me.pnlDraftTop.SuspendLayout()
        CType(Me.dgvDraft, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsDraftListCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFill.SuspendLayout()
        Me.tabPgView.SuspendLayout()
        Me.tbPgView.SuspendLayout()
        Me.pnlViewFill.SuspendLayout()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsViewListCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlViewTop.SuspendLayout()
        Me.pnlViewBottom.SuspendLayout()
        Me.tbPgDraft.SuspendLayout()
        Me.pnlDraftFill.SuspendLayout()
        Me.pnlBottomDraft.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(2, 18)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(87, 40)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDuplicate
        '
        Me.btnDuplicate.Location = New System.Drawing.Point(281, 18)
        Me.btnDuplicate.Name = "btnDuplicate"
        Me.btnDuplicate.Size = New System.Drawing.Size(87, 40)
        Me.btnDuplicate.TabIndex = 3
        Me.btnDuplicate.Text = "&Duplicate"
        Me.btnDuplicate.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 14)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Common Template Name :"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(374, 18)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 40)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "C&lose"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlDraftTop
        '
        Me.pnlDraftTop.Controls.Add(Me.btnFilterDraft)
        Me.pnlDraftTop.Controls.Add(Me.Label3)
        Me.pnlDraftTop.Controls.Add(Me.Label4)
        Me.pnlDraftTop.Controls.Add(Me.txtFilterDraft)
        Me.pnlDraftTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDraftTop.Location = New System.Drawing.Point(3, 3)
        Me.pnlDraftTop.Name = "pnlDraftTop"
        Me.pnlDraftTop.Size = New System.Drawing.Size(743, 71)
        Me.pnlDraftTop.TabIndex = 0
        '
        'btnFilterDraft
        '
        Me.btnFilterDraft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilterDraft.Location = New System.Drawing.Point(622, 30)
        Me.btnFilterDraft.Name = "btnFilterDraft"
        Me.btnFilterDraft.Size = New System.Drawing.Size(87, 29)
        Me.btnFilterDraft.TabIndex = 1
        Me.btnFilterDraft.Text = "&Filter"
        Me.btnFilterDraft.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Filter  By :"
        '
        'txtFilterDraft
        '
        Me.txtFilterDraft.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFilterDraft.Location = New System.Drawing.Point(158, 34)
        Me.txtFilterDraft.MaxLength = 100
        Me.txtFilterDraft.Name = "txtFilterDraft"
        Me.txtFilterDraft.Size = New System.Drawing.Size(439, 20)
        Me.txtFilterDraft.TabIndex = 0
        '
        'dgvDraft
        '
        Me.dgvDraft.AllowUserToAddRows = False
        Me.dgvDraft.AllowUserToDeleteRows = False
        Me.dgvDraft.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvDraft.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDraft.AutoGenerateColumns = False
        Me.dgvDraft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDraft.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvDraftCommonTemplateText, Me.OutputTemplateDataGridViewTextBoxColumn1, Me.SourceFileDataGridViewTextBoxColumn1, Me.WorksheetNameDataGridViewTextBoxColumn1, Me.IsEnabledDataGridViewCheckBoxColumn1})
        Me.dgvDraft.DataSource = Me.bsDraftListCollection
        Me.dgvDraft.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDraft.Location = New System.Drawing.Point(0, 0)
        Me.dgvDraft.Name = "dgvDraft"
        Me.dgvDraft.ReadOnly = True
        Me.dgvDraft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDraft.Size = New System.Drawing.Size(743, 355)
        Me.dgvDraft.TabIndex = 0
        '
        'dgvDraftCommonTemplateText
        '
        Me.dgvDraftCommonTemplateText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvDraftCommonTemplateText.DataPropertyName = "CommonTemplate"
        Me.dgvDraftCommonTemplateText.HeaderText = "Common Template Name"
        Me.dgvDraftCommonTemplateText.Name = "dgvDraftCommonTemplateText"
        Me.dgvDraftCommonTemplateText.ReadOnly = True
        '
        'OutputTemplateDataGridViewTextBoxColumn1
        '
        Me.OutputTemplateDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OutputTemplateDataGridViewTextBoxColumn1.DataPropertyName = "OutputTemplate"
        Me.OutputTemplateDataGridViewTextBoxColumn1.HeaderText = "Master Template Name"
        Me.OutputTemplateDataGridViewTextBoxColumn1.Name = "OutputTemplateDataGridViewTextBoxColumn1"
        Me.OutputTemplateDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'SourceFileDataGridViewTextBoxColumn1
        '
        Me.SourceFileDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SourceFileDataGridViewTextBoxColumn1.DataPropertyName = "SourceFile"
        Me.SourceFileDataGridViewTextBoxColumn1.HeaderText = "Source File Name"
        Me.SourceFileDataGridViewTextBoxColumn1.Name = "SourceFileDataGridViewTextBoxColumn1"
        Me.SourceFileDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'WorksheetNameDataGridViewTextBoxColumn1
        '
        Me.WorksheetNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.WorksheetNameDataGridViewTextBoxColumn1.DataPropertyName = "WorksheetName"
        Me.WorksheetNameDataGridViewTextBoxColumn1.HeaderText = "Worksheet Name"
        Me.WorksheetNameDataGridViewTextBoxColumn1.Name = "WorksheetNameDataGridViewTextBoxColumn1"
        Me.WorksheetNameDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'IsEnabledDataGridViewCheckBoxColumn1
        '
        Me.IsEnabledDataGridViewCheckBoxColumn1.DataPropertyName = "IsEnabled"
        Me.IsEnabledDataGridViewCheckBoxColumn1.HeaderText = "Template Enabled"
        Me.IsEnabledDataGridViewCheckBoxColumn1.Name = "IsEnabledDataGridViewCheckBoxColumn1"
        Me.IsEnabledDataGridViewCheckBoxColumn1.ReadOnly = True
        '
        'bsDraftListCollection
        '
        Me.bsDraftListCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.CommonTemplateExcelListCollection)
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(95, 18)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(87, 40)
        Me.btnView.TabIndex = 1
        Me.btnView.Text = "&View"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'btnEditDraft
        '
        Me.btnEditDraft.Location = New System.Drawing.Point(187, 18)
        Me.btnEditDraft.Name = "btnEditDraft"
        Me.btnEditDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnEditDraft.TabIndex = 2
        Me.btnEditDraft.Text = "&Edit"
        Me.btnEditDraft.UseVisualStyleBackColor = True
        '
        'pnlFill
        '
        Me.pnlFill.Controls.Add(Me.tabPgView)
        Me.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFill.Location = New System.Drawing.Point(0, 0)
        Me.pnlFill.Name = "pnlFill"
        Me.pnlFill.Size = New System.Drawing.Size(757, 520)
        Me.pnlFill.TabIndex = 6
        '
        'tabPgView
        '
        Me.tabPgView.Controls.Add(Me.tbPgView)
        Me.tabPgView.Controls.Add(Me.tbPgDraft)
        Me.tabPgView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabPgView.Location = New System.Drawing.Point(0, 0)
        Me.tabPgView.Name = "tabPgView"
        Me.tabPgView.SelectedIndex = 0
        Me.tabPgView.Size = New System.Drawing.Size(757, 520)
        Me.tabPgView.TabIndex = 0
        '
        'tbPgView
        '
        Me.tbPgView.Controls.Add(Me.pnlViewFill)
        Me.tbPgView.Controls.Add(Me.pnlViewTop)
        Me.tbPgView.Controls.Add(Me.pnlViewBottom)
        Me.tbPgView.Location = New System.Drawing.Point(4, 23)
        Me.tbPgView.Name = "tbPgView"
        Me.tbPgView.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPgView.Size = New System.Drawing.Size(749, 493)
        Me.tbPgView.TabIndex = 0
        Me.tbPgView.Text = "View"
        Me.tbPgView.UseVisualStyleBackColor = True
        '
        'pnlViewFill
        '
        Me.pnlViewFill.Controls.Add(Me.dgvView)
        Me.pnlViewFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlViewFill.Location = New System.Drawing.Point(3, 74)
        Me.pnlViewFill.Name = "pnlViewFill"
        Me.pnlViewFill.Size = New System.Drawing.Size(743, 355)
        Me.pnlViewFill.TabIndex = 9
        '
        'dgvView
        '
        Me.dgvView.AllowUserToAddRows = False
        Me.dgvView.AllowUserToDeleteRows = False
        Me.dgvView.AllowUserToResizeRows = False
        Me.dgvView.AutoGenerateColumns = False
        Me.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvViewCommonTemplateText, Me.dgvViewOutputTemplateText, Me.dgvViewSourceFileText, Me.dgvViewWorksheetNameText, Me.dgvViewEnabledText})
        Me.dgvView.DataSource = Me.bsViewListCollection
        Me.dgvView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvView.Location = New System.Drawing.Point(0, 0)
        Me.dgvView.MultiSelect = False
        Me.dgvView.Name = "dgvView"
        Me.dgvView.ReadOnly = True
        Me.dgvView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvView.Size = New System.Drawing.Size(743, 355)
        Me.dgvView.TabIndex = 0
        '
        'dgvViewCommonTemplateText
        '
        Me.dgvViewCommonTemplateText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvViewCommonTemplateText.DataPropertyName = "CommonTemplate"
        Me.dgvViewCommonTemplateText.HeaderText = "Common Template Name"
        Me.dgvViewCommonTemplateText.Name = "dgvViewCommonTemplateText"
        Me.dgvViewCommonTemplateText.ReadOnly = True
        '
        'dgvViewOutputTemplateText
        '
        Me.dgvViewOutputTemplateText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvViewOutputTemplateText.DataPropertyName = "OutputTemplate"
        Me.dgvViewOutputTemplateText.HeaderText = "Master Template Name"
        Me.dgvViewOutputTemplateText.Name = "dgvViewOutputTemplateText"
        Me.dgvViewOutputTemplateText.ReadOnly = True
        '
        'dgvViewSourceFileText
        '
        Me.dgvViewSourceFileText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvViewSourceFileText.DataPropertyName = "SourceFile"
        Me.dgvViewSourceFileText.HeaderText = "Source File Name"
        Me.dgvViewSourceFileText.Name = "dgvViewSourceFileText"
        Me.dgvViewSourceFileText.ReadOnly = True
        '
        'dgvViewWorksheetNameText
        '
        Me.dgvViewWorksheetNameText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvViewWorksheetNameText.DataPropertyName = "WorksheetName"
        Me.dgvViewWorksheetNameText.HeaderText = "Worksheet Name"
        Me.dgvViewWorksheetNameText.Name = "dgvViewWorksheetNameText"
        Me.dgvViewWorksheetNameText.ReadOnly = True
        '
        'dgvViewEnabledText
        '
        Me.dgvViewEnabledText.DataPropertyName = "IsEnabled"
        Me.dgvViewEnabledText.HeaderText = "Template Enabled"
        Me.dgvViewEnabledText.Name = "dgvViewEnabledText"
        Me.dgvViewEnabledText.ReadOnly = True
        '
        'bsViewListCollection
        '
        Me.bsViewListCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.CommonTemplateExcelListCollection)
        '
        'pnlViewTop
        '
        Me.pnlViewTop.Controls.Add(Me.btnFilter)
        Me.pnlViewTop.Controls.Add(Me.Label1)
        Me.pnlViewTop.Controls.Add(Me.Label2)
        Me.pnlViewTop.Controls.Add(Me.txtFilter)
        Me.pnlViewTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlViewTop.Location = New System.Drawing.Point(3, 3)
        Me.pnlViewTop.Name = "pnlViewTop"
        Me.pnlViewTop.Size = New System.Drawing.Size(743, 71)
        Me.pnlViewTop.TabIndex = 8
        '
        'btnFilter
        '
        Me.btnFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFilter.Location = New System.Drawing.Point(622, 30)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(87, 29)
        Me.btnFilter.TabIndex = 1
        Me.btnFilter.Text = "&Filter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 14)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Filter By :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 14)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Common Template Name :"
        '
        'txtFilter
        '
        Me.txtFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFilter.Location = New System.Drawing.Point(158, 34)
        Me.txtFilter.MaxLength = 100
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(439, 20)
        Me.txtFilter.TabIndex = 0
        '
        'pnlViewBottom
        '
        Me.pnlViewBottom.Controls.Add(Me.btnClose)
        Me.pnlViewBottom.Controls.Add(Me.btnNew)
        Me.pnlViewBottom.Controls.Add(Me.btnDuplicate)
        Me.pnlViewBottom.Controls.Add(Me.btnView)
        Me.pnlViewBottom.Controls.Add(Me.btnEdit)
        Me.pnlViewBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlViewBottom.Location = New System.Drawing.Point(3, 429)
        Me.pnlViewBottom.Name = "pnlViewBottom"
        Me.pnlViewBottom.Size = New System.Drawing.Size(743, 61)
        Me.pnlViewBottom.TabIndex = 0
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(188, 18)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(87, 40)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'tbPgDraft
        '
        Me.tbPgDraft.Controls.Add(Me.pnlDraftFill)
        Me.tbPgDraft.Controls.Add(Me.pnlDraftTop)
        Me.tbPgDraft.Controls.Add(Me.pnlBottomDraft)
        Me.tbPgDraft.Location = New System.Drawing.Point(4, 23)
        Me.tbPgDraft.Name = "tbPgDraft"
        Me.tbPgDraft.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPgDraft.Size = New System.Drawing.Size(749, 493)
        Me.tbPgDraft.TabIndex = 1
        Me.tbPgDraft.Text = "View Draft"
        Me.tbPgDraft.UseVisualStyleBackColor = True
        '
        'pnlDraftFill
        '
        Me.pnlDraftFill.Controls.Add(Me.dgvDraft)
        Me.pnlDraftFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDraftFill.Location = New System.Drawing.Point(3, 74)
        Me.pnlDraftFill.Name = "pnlDraftFill"
        Me.pnlDraftFill.Size = New System.Drawing.Size(743, 355)
        Me.pnlDraftFill.TabIndex = 9
        '
        'pnlBottomDraft
        '
        Me.pnlBottomDraft.Controls.Add(Me.btnCloseDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnNewDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnViewDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnDuplicateDraft)
        Me.pnlBottomDraft.Controls.Add(Me.btnEditDraft)
        Me.pnlBottomDraft.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottomDraft.Location = New System.Drawing.Point(3, 429)
        Me.pnlBottomDraft.Name = "pnlBottomDraft"
        Me.pnlBottomDraft.Size = New System.Drawing.Size(743, 61)
        Me.pnlBottomDraft.TabIndex = 6
        '
        'btnCloseDraft
        '
        Me.btnCloseDraft.Location = New System.Drawing.Point(373, 18)
        Me.btnCloseDraft.Name = "btnCloseDraft"
        Me.btnCloseDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnCloseDraft.TabIndex = 4
        Me.btnCloseDraft.Text = "C&lose"
        Me.btnCloseDraft.UseVisualStyleBackColor = True
        '
        'btnNewDraft
        '
        Me.btnNewDraft.Location = New System.Drawing.Point(1, 18)
        Me.btnNewDraft.Name = "btnNewDraft"
        Me.btnNewDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnNewDraft.TabIndex = 0
        Me.btnNewDraft.Text = "&New"
        Me.btnNewDraft.UseVisualStyleBackColor = True
        '
        'btnViewDraft
        '
        Me.btnViewDraft.Location = New System.Drawing.Point(94, 18)
        Me.btnViewDraft.Name = "btnViewDraft"
        Me.btnViewDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnViewDraft.TabIndex = 1
        Me.btnViewDraft.Text = "&View"
        Me.btnViewDraft.UseVisualStyleBackColor = True
        '
        'btnDuplicateDraft
        '
        Me.btnDuplicateDraft.Location = New System.Drawing.Point(280, 18)
        Me.btnDuplicateDraft.Name = "btnDuplicateDraft"
        Me.btnDuplicateDraft.Size = New System.Drawing.Size(87, 40)
        Me.btnDuplicateDraft.TabIndex = 3
        Me.btnDuplicateDraft.Text = "&Duplicate"
        Me.btnDuplicateDraft.UseVisualStyleBackColor = True
        '
        'ucCommonTemplateExcelView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.pnlFill)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucCommonTemplateExcelView"
        Me.Size = New System.Drawing.Size(757, 520)
        Me.pnlDraftTop.ResumeLayout(False)
        Me.pnlDraftTop.PerformLayout()
        CType(Me.dgvDraft, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsDraftListCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFill.ResumeLayout(False)
        Me.tabPgView.ResumeLayout(False)
        Me.tbPgView.ResumeLayout(False)
        Me.pnlViewFill.ResumeLayout(False)
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsViewListCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlViewTop.ResumeLayout(False)
        Me.pnlViewTop.PerformLayout()
        Me.pnlViewBottom.ResumeLayout(False)
        Me.tbPgDraft.ResumeLayout(False)
        Me.pnlDraftFill.ResumeLayout(False)
        Me.pnlBottomDraft.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnDuplicate As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents pnlDraftTop As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFilterDraft As System.Windows.Forms.TextBox
    Friend WithEvents dgvDraft As System.Windows.Forms.DataGridView
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents btnEditDraft As System.Windows.Forms.Button
    Friend WithEvents pnlFill As System.Windows.Forms.Panel
    Friend WithEvents tabPgView As System.Windows.Forms.TabControl
    Friend WithEvents tbPgView As System.Windows.Forms.TabPage
    Friend WithEvents pnlViewFill As System.Windows.Forms.Panel
    Friend WithEvents dgvView As System.Windows.Forms.DataGridView
    Friend WithEvents pnlViewTop As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents pnlViewBottom As System.Windows.Forms.Panel
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents tbPgDraft As System.Windows.Forms.TabPage
    Friend WithEvents pnlDraftFill As System.Windows.Forms.Panel
    Friend WithEvents pnlBottomDraft As System.Windows.Forms.Panel
    Friend WithEvents btnDuplicateDraft As System.Windows.Forms.Button
    Friend WithEvents bsViewListCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsDraftListCollection As System.Windows.Forms.BindingSource
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents btnFilterDraft As System.Windows.Forms.Button
    Friend WithEvents dgvViewCommonTemplateText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvViewOutputTemplateText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvViewSourceFileText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvViewWorksheetNameText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvViewEnabledText As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvDraftCommonTemplateText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutputTemplateDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceFileDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WorksheetNameDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsEnabledDataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnCloseDraft As System.Windows.Forms.Button
    Friend WithEvents btnNewDraft As System.Windows.Forms.Button
    Friend WithEvents btnViewDraft As System.Windows.Forms.Button

End Class
