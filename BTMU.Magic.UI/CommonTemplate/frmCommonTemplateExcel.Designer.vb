<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonTemplateExcel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonTemplateExcel))
        Me.UcCommonTemplateExcelDetail1 = New BTMU.Magic.UI.ucCommonTemplateExcelDetail
        Me.UcCommonTemplateExcelView1 = New BTMU.Magic.UI.ucCommonTemplateExcelView
        Me.SuspendLayout()
        '
        'UcCommonTemplateExcelDetail1
        '
        Me.UcCommonTemplateExcelDetail1.ContainerForm = Nothing
        Me.UcCommonTemplateExcelDetail1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplateExcelDetail1.IsNew = False
        Me.UcCommonTemplateExcelDetail1.Location = New System.Drawing.Point(0, 0)
        Me.UcCommonTemplateExcelDetail1.Name = "UcCommonTemplateExcelDetail1"
        Me.UcCommonTemplateExcelDetail1.Size = New System.Drawing.Size(926, 611)
        Me.UcCommonTemplateExcelDetail1.TabIndex = 1
        '
        'UcCommonTemplateExcelView1
        '
        Me.UcCommonTemplateExcelView1.ContainerForm = Nothing
        Me.UcCommonTemplateExcelView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplateExcelView1.IsNew = False
        Me.UcCommonTemplateExcelView1.Location = New System.Drawing.Point(0, 0)
        Me.UcCommonTemplateExcelView1.Name = "UcCommonTemplateExcelView1"
        Me.UcCommonTemplateExcelView1.Size = New System.Drawing.Size(926, 611)
        Me.UcCommonTemplateExcelView1.TabIndex = 0
        '
        'frmCommonTemplateExcel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 611)
        Me.Controls.Add(Me.UcCommonTemplateExcelView1)
        Me.Controls.Add(Me.UcCommonTemplateExcelDetail1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCommonTemplateExcel"
        Me.ShowIcon = False
        Me.Text = "Generate Common Transaction Template - Excel (MA2010)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcCommonTemplateExcelView1 As BTMU.MAGIC.UI.ucCommonTemplateExcelView
    Friend WithEvents UcCommonTemplateExcelDetail1 As BTMU.MAGIC.UI.ucCommonTemplateExcelDetail
End Class
