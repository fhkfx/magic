Imports System.Windows.Forms
''' <summary>
''' Base Class for GUI of Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateExcelBase
    Inherits System.Windows.Forms.UserControl

    Protected _parent As frmCommonTemplateExcel
    Protected _isNew As Boolean
    ''' <summary>
    ''' This property is to Get/Set Container form 
    ''' </summary>
    ''' <value>Container form</value>
    ''' <returns>Returns Container form</returns>
    ''' <remarks></remarks>
    Public Property ContainerForm() As frmCommonTemplateExcel
        Get
            Return _parent
        End Get
        Set(ByVal value As frmCommonTemplateExcel)
            _parent = value
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Template is New
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Boolean value indicating whether the Template is new</returns>
    ''' <remarks></remarks>
    Public Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
        Set(ByVal value As Boolean)
            _isNew = value
        End Set
    End Property

End Class
