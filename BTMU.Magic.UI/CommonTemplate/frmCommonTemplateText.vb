Imports BTMU.Magic.Common

''' <summary>
''' This Form is to display the User Control of Common Template Transaction Template for Text
''' </summary>
''' <remarks></remarks>
Public Class frmCommonTemplateText

    Private Sub frmCommonTemplateText_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If UcCommonTemplateTextDetail1.Visible Then
            If UcCommonTemplateTextDetail1.ConfirmBeforeCloseAndCancel() Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CommonTemplateForText_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        UcCommonTemplateTextDetail1.ContainerForm = Me
        UcCommonTemplateTextView1.ContainerForm = Me

    End Sub
    ''' <summary>
    ''' Displays the Detail Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDetail()
        UcCommonTemplateTextView1.Visible = False
        UcCommonTemplateTextDetail1.Visible = True
    End Sub
    ''' <summary>
    ''' Displays the Detail Panel and initializes the Source and Definition File
    ''' </summary>
    ''' <param name="SrcFileName">Source File Name</param>
    ''' <param name="DfnFileName">Definition File Name</param>
    ''' <remarks></remarks>
    Public Sub ShowDetail(ByVal SrcFileName As String, ByVal DfnFileName As String)
        UcCommonTemplateTextView1.Visible = False
        UcCommonTemplateTextDetail1.Visible = True
        UcCommonTemplateTextDetail1.AddNew()
        UcCommonTemplateTextDetail1.SetSourceFile(SrcFileName, DfnFileName)
    End Sub
    ''' <summary>
    ''' Displays the view Panel listing out all existing templates
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowView()
        UcCommonTemplateTextView1.reloadTemplates()
        UcCommonTemplateTextView1.Visible = True
        UcCommonTemplateTextDetail1.Visible = False
        UcCommonTemplateTextView1.tabPgView.SelectedIndex = 0
        UcCommonTemplateTextView1.txtFilter.Text = ""
        UcCommonTemplateTextView1.txtFilter.Focus()
    End Sub
    ''' <summary>
    ''' Displays the view Panel listing out all existing draft templates
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowDraftView()
        ShowView()
        UcCommonTemplateTextView1.tabPgView.SelectedIndex = 1
        UcCommonTemplateTextView1.txtFilterDraft.Text = ""
        UcCommonTemplateTextView1.txtFilterDraft.Focus()
    End Sub

    Private Sub UcCommonTemplateTextView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UcCommonTemplateTextView1.Load
        ShowView()
    End Sub
End Class

