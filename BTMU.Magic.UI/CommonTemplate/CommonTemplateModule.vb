Imports System.IO
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate

''' <summary>
''' Helper Module providing utility functions to Common Transaction Template Object
''' </summary>
''' <remarks></remarks>
Public Class CommonTemplateModule

    Private Shared _dateSeparator() As String
    Private Shared _dateType() As String
    Private Shared _decimalSeparator() As String
    Private Shared _thousandSeparator() As String
    Private Shared _enclosureCharacters() As String
    Private Shared _delimiterCharacters() As String
    Private Shared _filterOperator() As String
    Private Shared _operators1() As String
    Private Shared _operators2() As String
    Private Shared _lookupTables() As String
    Private Shared _stringFunctions() As String
    Private Shared _strFilterDataType() As String
    Private Shared _shortDateType() As String
    ''' <summary>
    ''' This is to Get the Short Form of Date Types
    ''' </summary>
    ''' <value>Short Form of Date Type</value>
    ''' <returns>Returns an Array of Short Form of Date Types</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ShortDateType() As String()
        Get
            Return _shortDateType
        End Get
    End Property
    ''' <summary>
    ''' This is to Get the Row Filter Data Types
    ''' </summary>
    ''' <value>Row Filter Data Types</value>
    ''' <returns>Returns an Array of Row Filter Data Types</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property FilterDataType() As String()
        Get
            Return _strFilterDataType
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Excel File Folder
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Excel File Folder</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CommonTemplateExcelFolder() As String
        Get
            Return My.Settings.CommonTemplateExcelFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Excel File Folder for Draft Templates
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Excel File Folder for Draft Templates</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CommonTemplateExcelDraftFolder() As String
        Get
            Return My.Settings.CommonTemplateExcelDraftFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Text File Folder
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Text File Folder</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CommonTemplateTextFolder() As String
        Get
            Return My.Settings.CommonTemplateTextFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Text File Folder for Draft Templates
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Text File Folder for Draft Templates</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CommonTemplateTextDraftFolder() As String
        Get
            Return My.Settings.CommonTemplateTextDraftFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Swift File Folder
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Swift File Folder</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SwiftFolder() As String
        Get
            Return My.Settings.SwiftFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template Swift File Folder for Draft Templates
    ''' </summary>
    ''' <value>File Folder</value>
    ''' <returns>Returns Common Template Swift File Folder for Draft Templates</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SwiftDraftFolder() As String
        Get
            Return My.Settings.SwiftDraftFolder
        End Get
    End Property
    ''' <summary>
    ''' This is to Get Common Template File Extension
    ''' </summary>
    ''' <value>Common Template File Extension</value>
    ''' <returns>Returns Common Template File Extension</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CommonTemplateFileExtension() As String
        Get
            Return ".mct"
        End Get
    End Property
    ''' <summary>
    ''' This is to Get List of Master Templates
    ''' </summary>
    ''' <value>List of Master Templates</value>
    ''' <returns>Returns List of Master Templates</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AllMasterTemplates() As BTMU.Magic.MasterTemplate.MasterTemplateListCollection

        Get
            Dim lst As New BTMU.Magic.MasterTemplate.MasterTemplateListCollection
            For Each fairMstTmp As BTMU.Magic.MasterTemplate.MasterTemplateList In ListAllMasterTemplateList() 'BTMU.Magic.MasterTemplate.MasterTemplateSharedFunction.
                If Not fairMstTmp.IsDraft Then
                    If fairMstTmp.TemplateStatus Then
                        lst.Add(fairMstTmp)
                    End If
                End If
            Next
            Return lst
        End Get

    End Property
    ''' <summary>
    ''' This Property is to Get Master Template List
    ''' </summary>
    ''' <param name="masterTemplateName">Master Template Name</param>
    ''' <value>Master Template List </value>
    ''' <returns>Returns Master Template List</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GetMasterTemplateList(ByVal masterTemplateName As String) As MasterTemplate.MasterTemplateList
        Get
            Dim objMasterTemplateList As New MasterTemplate.MasterTemplateList
            For Each objmaster As MasterTemplate.MasterTemplateList In ListAllMasterTemplateList()
                If objmaster.TemplateName.Trim.ToUpper = masterTemplateName.Trim.ToUpper Then
                    objMasterTemplateList = objmaster
                    Exit For
                End If
            Next
            Return objMasterTemplateList
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Date Separator
    ''' </summary>
    ''' <value>Date Separator</value>
    ''' <returns>Returns an Array of Date Separator</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DateSeparator() As String()
        Get
            Return _dateSeparator
        End Get

    End Property
    ''' <summary>
    ''' This Property is to Get Date Type
    ''' </summary>
    ''' <value>Date Type</value>
    ''' <returns>Returns an Array of Date Type</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DateType() As String()
        Get
            Return _dateType
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Decimal Separator
    ''' </summary>
    ''' <value>Decimal Separator</value>
    ''' <returns>Returns an Array of Decimal Separator</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DecimalSeparator() As String()
        Get
            Return _decimalSeparator
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Thousand Separator
    ''' </summary>
    ''' <value>Thousand Separator</value>
    ''' <returns>Returns an Array of Thousand Separator</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ThousandSeparator() As String()
        Get
            Return _thousandSeparator
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Enclosure Characters
    ''' </summary>
    ''' <value>Enclosure Characters</value>
    ''' <returns>Returns an Array of Enclosure Characters</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EnclosureCharacters() As String()
        Get
            Return _enclosureCharacters
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Delimiter Characters
    ''' </summary>
    ''' <value>Delimiter Characters</value>
    ''' <returns>Returns an Array of Delimiter Characters</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DelimiterCharacters() As String()
        Get
            Return _delimiterCharacters
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Filter Operators
    ''' </summary>
    ''' <value>Filter Operators</value>
    ''' <returns>Returns an Array of Filter Operators</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property FilterOperator() As String()
        Get
            Return _filterOperator
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Operators + and -
    ''' </summary>
    ''' <value>Operators + and -</value>
    ''' <returns>Returns an Array of Operators + and -</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Operators1() As String()
        Get
            Return _operators1
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get Operators +,- and None
    ''' </summary>
    ''' <value>Operators +,- and None</value>
    ''' <returns>Returns an Array of Operators +,- and None</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Operators2() As String()
        Get
            Return _operators2
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get a List of Lookup Tables
    ''' </summary>
    ''' <value>Lookup Tables</value>
    ''' <returns>Returns an Array of Lookup Tables</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property LookupTables() As String()
        Get
            Return _lookupTables
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get a List of String Functions
    ''' </summary>
    ''' <value>String Functions</value>
    ''' <returns>Returns an Array of String Functions</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StringFunctions() As String()
        Get
            Return _stringFunctions
        End Get
    End Property
    ''' <summary>
    ''' This is to Get a DateFormat Pattern for the given date separator and date type
    ''' </summary>
    ''' <param name="dtseparator">Date Separator</param>
    ''' <param name="dttype">Date Type</param>
    ''' <returns>Returns Date Format Pattern</returns>
    ''' <remarks></remarks>
    Public Shared Function GetDateFormatPattern(ByVal dtseparator As String, ByVal dttype As String) As String

        Dim dtpattern As String = ""

        If dtseparator = "No Space" Then
            Return dttype
        ElseIf dtseparator = "<Space>" Then
            dtseparator = " "
        End If

        Select Case dttype
            Case "DDMMYY"
                Return String.Format("DD{0}MM{0}YY", dtseparator)
            Case "DDMMMYY"
                Return String.Format("DD{0}MMM{0}YY", dtseparator)
            Case "DDMMYYYY"
                Return String.Format("DD{0}MM{0}YYYY", dtseparator)
            Case "DDMMMYYYY"
                Return String.Format("DD{0}MMM{0}YYYY", dtseparator)
            Case "MMDDYY"
                Return String.Format("MM{0}DD{0}YY", dtseparator)
            Case "MMMDDYY"
                Return String.Format("MMM{0}DD{0}YY", dtseparator)
            Case "MMDDYYYY"
                Return String.Format("MM{0}DD{0}YYYY", dtseparator)
            Case "MMMDDYYYY"
                Return String.Format("MMM{0}DD{0}YYYY", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
            Case "YYYYDDMM"
                Return String.Format("YYYY{0}DD{0}MM", dtseparator)
            Case "YYYYMMDD"
                Return String.Format("YYYY{0}MM{0}DD", dtseparator)
            Case "DDMMYY"
                Return String.Format("DD{0}MM{0}YY", dtseparator)
            Case "MMDDYY"
                Return String.Format("MM{0}DD{0}YY", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
            Case "YYMMDD"
                Return String.Format("YY{0}MM{0}DD", dtseparator)
        End Select

        Return dtpattern

    End Function

    ''' <summary>
    ''' Initializes General options used in Common Templates
    ''' </summary>
    ''' <remarks></remarks>
    Shared Sub New()

        _dateSeparator = New String() {"No Space", ".", "/", "-", "<Space>"}

        _dateType = New String() { _
                                "DDMMYY", "DDMMMYY", "DDMMYYYY", "DDMMMYYYY", "MMDDYY", "MMMDDYY", _
                                "MMDDYYYY", "MMMDDYYYY", "YYMMDD", "YYYYDDMM", "YYYYMMDD" _
                                }

        _shortDateType = New String() {"DDMMYY", "MMDDYY", "YYMMDD"}

        _decimalSeparator = New String() {"No Separator", ".", ","}

        _thousandSeparator = New String() {"No Separator", ".", ","}

        _enclosureCharacters = New String() {"Double Quote("")", "Single Quote(')", "None"}

        _delimiterCharacters = New String() {";", ",", "@", "^", "<Tab>", "<Space>", "Other"}


        _filterOperator = New String() { _
                                            "=", "<>", "<", ">", "<=", ">=" _
                                            , "Contain", "Not Contain" _
                                            , "> AND <", "> AND <=", ">= AND <", ">= AND <=" _
                                        }

        _operators1 = New String() {"+", "-", "*", "/"}

        _operators2 = New String() {"None", "+", "-", "*", "/"}

        _stringFunctions = New String() {"LEFT", "RIGHT", "SUBSTRING", "REMOVE", "REMOVE LAST" _
                                        , "TRIM", "LEFT & TRIM", "RIGHT & TRIM", "SUBSTRING & TRIM", "REMOVE & TRIM", "REMOVE LAST & TRIM"}

        _lookupTables = New String() {"Table1", "Table2", "Table3", "Table4", "Table5", "Table6", "Table7", "Table8", "Table9", "Table10", "Table11"}

        _strFilterDataType = New String() {"String", "Numeric", "Date"}

    End Sub
    ''' <summary>
    ''' This function returns all the enabled Text Templates
    ''' </summary>
    ''' <returns>All Enabled Text Templates</returns>
    ''' <remarks></remarks>
    Public Shared Function ListAllTextCommonTemplates() As TextTemplateListCollection
        Dim filePath As String
        Dim content As String
        Dim commonTemplateList As New TextTemplateListCollection
        Dim commonTemplateFiles As New List(Of String)
        Dim files() As String

        Dim textListSerializer As New XmlSerializer(GetType(Magic.CommonTemplate.TextTemplateList))
        Try
            commonTemplateList.Clear()
            files = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateTextFolder, "*" & CommonTemplateModule.CommonTemplateFileExtension)
            If files.Length > 0 Then
                commonTemplateFiles.AddRange(files)
            End If

            For Each filePath In commonTemplateFiles
                Try
                    Dim doc As XPathDocument = New XPathDocument(filePath)
                    Dim nav As XPathNavigator = doc.CreateNavigator()
                    Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                    Dim item As New Magic.CommonTemplate.TextTemplateList

                    Iterator.MoveNext()
                    content = Iterator.Current.Value

                    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                    stream.Position = 0
                    item = CType(textListSerializer.Deserialize(stream), Magic.CommonTemplate.TextTemplateList)
                    If item.IsEnabled Then
                        commonTemplateList.Add(item)
                    End If
                Catch ex As Exception
                    ' Do not show any invalid files.
                End Try
            Next

        Catch ex As Exception
            'Throw New Exception(ex.Message.ToString)
        End Try
        Return commonTemplateList

    End Function
    ''' <summary>
    ''' This function returns all the enabled ExcelCommonTemplates
    ''' </summary>
    ''' <returns>Returns all enabled Common Templates for Excel</returns>
    ''' <remarks></remarks>
    Public Shared Function ListAllExcelCommonTemplates() As CommonTemplateExcelListCollection
        Dim filePath As String
        Dim content As String
        Dim commonTemplateFiles As New List(Of String)
        Dim commonTemplateList As New CommonTemplateExcelListCollection
        Dim files() As String

        Dim excelListSerializer As New XmlSerializer(GetType(Magic.CommonTemplate.CommonTemplateExcelList))
        Try
            commonTemplateList.Clear()
            files = System.IO.Directory.GetFiles(CommonTemplateModule.CommonTemplateExcelFolder, "*" & CommonTemplateModule.CommonTemplateFileExtension)
            If files.Length > 0 Then
                commonTemplateFiles.AddRange(files)
            End If

            For Each filePath In commonTemplateFiles
                Try
                    Dim doc As XPathDocument = New XPathDocument(filePath)
                    Dim nav As XPathNavigator = doc.CreateNavigator()
                    Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                    Dim item As New Magic.CommonTemplate.CommonTemplateExcelList

                    Iterator.MoveNext()
                    content = Iterator.Current.Value

                    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                    stream.Position = 0
                    item = CType(excelListSerializer.Deserialize(stream), Magic.CommonTemplate.CommonTemplateExcelList)
                    If item.IsEnabled Then
                        commonTemplateList.Add(item)
                    End If
                Catch ex As Exception
                    ' Do not show any invalid files.
                End Try
            Next

        Catch ex As Exception
            'Throw New Exception(ex.Message.ToString)
        End Try

        Return commonTemplateList
    End Function
    ''' <summary>
    ''' This function returns all the enabled SWIFT Templates
    ''' </summary>
    ''' <returns>Returns all enabled Swift Templates</returns>
    ''' <remarks></remarks>
    Public Shared Function ListAllSWIFTCommonTemplates() As SwiftTemplateListCollection
        Dim filePath As String
        Dim content As String
        Dim commonTemplateList As New SwiftTemplateListCollection
        Dim commonTemplateFiles As New List(Of String)
        Dim files() As String

        Dim SWIFTListSerializer As New XmlSerializer(GetType(Magic.CommonTemplate.SwiftTemplateList))
        Try
            commonTemplateList.Clear()
            files = System.IO.Directory.GetFiles(CommonTemplateModule.SwiftFolder, "*" & CommonTemplateModule.CommonTemplateFileExtension)
            If files.Length > 0 Then
                commonTemplateFiles.AddRange(files)
            End If

            For Each filePath In commonTemplateFiles
                Try
                    Dim doc As XPathDocument = New XPathDocument(filePath)
                    Dim nav As XPathNavigator = doc.CreateNavigator()
                    Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                    Dim item As New Magic.CommonTemplate.SwiftTemplateList

                    Iterator.MoveNext()
                    content = Iterator.Current.Value

                    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                    stream.Position = 0
                    item = CType(SWIFTListSerializer.Deserialize(stream), Magic.CommonTemplate.SwiftTemplateList)
                    If item.IsEnabled Then
                        commonTemplateList.Add(item)
                    End If

                Catch ex As Exception
                    ' Do not show any invalid files.
                End Try
            Next

        Catch ex As Exception
            'Throw New Exception(ex.Message.ToString)
        End Try
        Return commonTemplateList

    End Function
    ''' <summary>
    ''' Loads Common Template
    ''' </summary>
    ''' <param name="CommonTemplateName">Common Template Name</param>
    ''' <returns>Returns the loaded Common Template</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadTextCommonTemplate(ByVal CommonTemplateName As String) As CommonTemplateTextDetail
        Dim commonTemplateFile As String = String.Empty
        Dim commonTemplateText As CommonTemplateTextDetail
        ' Load the Common Template Text
        commonTemplateFile = System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                    , CommonTemplateName _
                    & CommonTemplateModule.CommonTemplateFileExtension)
        commonTemplateText = CommonTemplateTextDetail.LoadFromFile2(commonTemplateFile)
        Return commonTemplateText
    End Function
    ''' <summary>
    ''' Loads Excel Template
    ''' </summary>
    ''' <param name="CommonTemplateName">Excel Template Name</param>
    ''' <returns>Returns the loaded Template</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadExcelCommonTemplate(ByVal CommonTemplateName As String) As CommonTemplateExcelDetail
        Dim commonTemplateFile As String = String.Empty
        Dim commonTemplateExcel As CommonTemplateExcelDetail
        ' Load the Common Template Excel 
        commonTemplateFile = System.IO.Path.Combine(CommonTemplateModule.CommonTemplateExcelFolder _
                    , CommonTemplateName _
                    & CommonTemplateModule.CommonTemplateFileExtension)

        commonTemplateExcel = (New Magic.CommonTemplate.CommonTemplateExcelDetail).LoadFromFile2(commonTemplateFile)
        Return commonTemplateExcel
    End Function
    ''' <summary>
    ''' Loads Swift Template
    ''' </summary>
    ''' <param name="CommonTemplateName">Swift Template Name</param>
    ''' <returns>Returns the loaded Template</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadSWIFTCommonTemplate(ByVal CommonTemplateName As String) As SwiftTemplate
        Dim commonTemplateFile As String = String.Empty
        Dim SWIFTTemplate As SwiftTemplate
        ' Load the Common Template Excel 
        commonTemplateFile = System.IO.Path.Combine(CommonTemplateModule.SwiftFolder _
                    , CommonTemplateName _
                    & CommonTemplateModule.CommonTemplateFileExtension)
        SWIFTTemplate = SWIFTTemplate.LoadFromFile2(commonTemplateFile)
        Return SWIFTTemplate
    End Function
End Class
