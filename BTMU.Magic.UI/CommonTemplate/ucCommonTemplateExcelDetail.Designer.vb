<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCommonTemplateExcelDetail
    Inherits MAGIC.UI.ucCommonTemplateExcelBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.pnlMapBottom = New System.Windows.Forms.Panel
        Me.btnClearAll = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.dgvBank = New System.Windows.Forms.DataGridView
        Me.dgvBankBankFieldNameText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvBankMappedFieldsText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvBankMappedValuesText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvBankBankSampleValueText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MapSeparator = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CarriageReturn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.bsMapBankCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsDetailCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvSource = New System.Windows.Forms.DataGridView
        Me.Sequence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvSourceSourceFieldNameText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvSourceSourceFieldValueText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bsMapSourceCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvSampleData = New System.Windows.Forms.DataGridView
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtSource = New System.Windows.Forms.TextBox
        Me.FieldSettingGroupBox = New System.Windows.Forms.GroupBox
        Me.txtTransactionStartRow = New BTMU.Magic.UI.NumberBox
        Me.txtHeaderAtRow = New BTMU.Magic.UI.NumberBox
        Me.btnRetrieve = New System.Windows.Forms.Button
        Me.chkHeaderAsFieldName = New System.Windows.Forms.CheckBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.ExcelSettingGroupBox = New System.Windows.Forms.GroupBox
        Me.txtSampleRowNo = New BTMU.Magic.UI.NumberBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtEndCol = New System.Windows.Forms.TextBox
        Me.txtStartCol = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboWorksheet = New System.Windows.Forms.ComboBox
        Me.bsWorksheetCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label17 = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.dgvEditable = New System.Windows.Forms.DataGridView
        Me.dgvEditablebtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvEditableBankFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsEditableBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsEditableCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnUpTranslator = New System.Windows.Forms.Button
        Me.TranslationGroupBox = New System.Windows.Forms.GroupBox
        Me.btnDownTranslator = New System.Windows.Forms.Button
        Me.dgvTranslator = New System.Windows.Forms.DataGridView
        Me.dgvTranslatorbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvTranslatorBankFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsTranslateBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvTranslatorCustomerValueText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvTranslatorCellBankValueEditCbo = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.BankValueEmpty = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.bsTranslatorCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnUpEditable = New System.Windows.Forms.Button
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.EditableGroupBox = New System.Windows.Forms.GroupBox
        Me.btnDownEditable = New System.Windows.Forms.Button
        Me.btnDownStrManip = New System.Windows.Forms.Button
        Me.StringManipulationGroupBox = New System.Windows.Forms.GroupBox
        Me.btnUpStrManip = New System.Windows.Forms.Button
        Me.dgvStrManip = New System.Windows.Forms.DataGridView
        Me.dgvStrManipbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvStrManipBankFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsStrManipBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvStrManipFunctionNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvStrManipStartPositionText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvStrManipNoOfCharactersText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bsStrManipCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabPage7 = New System.Windows.Forms.TabPage
        Me.txtTemplateName = New System.Windows.Forms.TextBox
        Me.cboOutputTemplate = New System.Windows.Forms.ComboBox
        Me.bsOutputTemplate = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlFill = New System.Windows.Forms.Panel
        Me.tabDetail = New System.Windows.Forms.TabControl
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.pnlSetupBottom = New System.Windows.Forms.Panel
        Me.RecordTypeIndicatorGroupBox = New System.Windows.Forms.GroupBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.IIndicatorTextBox = New System.Windows.Forms.TextBox
        Me.IColumnTextBox = New System.Windows.Forms.TextBox
        Me.WIndicatorTextBox = New System.Windows.Forms.TextBox
        Me.PIndicatorTextBox = New System.Windows.Forms.TextBox
        Me.PColumnTextBox = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.WColumnTextBox = New System.Windows.Forms.TextBox
        Me.grpDetailFilterSettings = New System.Windows.Forms.GroupBox
        Me.btnFilterUp = New System.Windows.Forms.Button
        Me.btnFilterDown = New System.Windows.Forms.Button
        Me.rdoFilterOR = New System.Windows.Forms.RadioButton
        Me.rdoFilterAND = New System.Windows.Forms.RadioButton
        Me.dgvFilter = New System.Windows.Forms.DataGridView
        Me.dgvFilterbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvFilterFilterFieldCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsFilterFieldCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvFilterCellFilterTypeComboBox = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvFilterFilterOperatorCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvFilterFilterValueText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bsRowFilterCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.rdoCents = New System.Windows.Forms.RadioButton
        Me.rdoDollars = New System.Windows.Forms.RadioButton
        Me.grpDetailAdviceSettings = New System.Windows.Forms.GroupBox
        Me.txtAdviceDelimiter = New System.Windows.Forms.TextBox
        Me.txtAdviceChar = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.rdoAdviceDelimited = New System.Windows.Forms.RadioButton
        Me.rdoAdviceAftEveryChar = New System.Windows.Forms.RadioButton
        Me.rdoAdviceForEveryRow = New System.Windows.Forms.RadioButton
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.cboRef3 = New System.Windows.Forms.ComboBox
        Me.cboRef2 = New System.Windows.Forms.ComboBox
        Me.cboRef1 = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.chkZerosInDate = New System.Windows.Forms.CheckBox
        Me.cboDateType = New System.Windows.Forms.ComboBox
        Me.cboDateSep = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.cboThousandSep = New System.Windows.Forms.ComboBox
        Me.cboDecimalSep = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.pnlSetup = New System.Windows.Forms.Panel
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.LookupGroupBox = New System.Windows.Forms.GroupBox
        Me.dgvLookup = New System.Windows.Forms.DataGridView
        Me.bsLookupBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsLookupSourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsLookupCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnLookupDown = New System.Windows.Forms.Button
        Me.btnLookupUp = New System.Windows.Forms.Button
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.CalculationGroupBox = New System.Windows.Forms.GroupBox
        Me.btnDownCalc = New System.Windows.Forms.Button
        Me.btnUpCalc = New System.Windows.Forms.Button
        Me.dgvCalc = New System.Windows.Forms.DataGridView
        Me.dgvCalcbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvCalcBankFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsCalcBankField = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvCalcOperand1Combo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsCalOperand1SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvCalOperator1Combo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalcOperand2Combo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsCalcOperand2SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgvCalcOperator2Combo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvCalcOperand3Combo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.bsCalcOperand3SourceField = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsCalcCollection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsRefField3Collection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsRefField2Collection = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsRefField1Collection = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkEnable = New System.Windows.Forms.CheckBox
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnSaveAsDraft = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.ErrorProvider1 = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.dgvLookupbtnDelete = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvLookupBankFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupSourceFieldNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupTableNameCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupLookupKeyCombo = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupLookupValueCombo = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupTableNameCombo2 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupLookupKeyCombo2 = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupLookupValueCombo2 = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupTableNameCombo3 = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgvLookupLookupKeyCombo3 = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.dgvLookupLookupValueCombo3 = New BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
        Me.TabPage3.SuspendLayout()
        Me.pnlMapBottom.SuspendLayout()
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsMapBankCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsDetailCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsMapSourceCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSampleData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.FieldSettingGroupBox.SuspendLayout()
        Me.ExcelSettingGroupBox.SuspendLayout()
        CType(Me.bsWorksheetCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEditable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsEditableBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsEditableCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TranslationGroupBox.SuspendLayout()
        CType(Me.dgvTranslator, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTranslateBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsTranslatorCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.EditableGroupBox.SuspendLayout()
        Me.StringManipulationGroupBox.SuspendLayout()
        CType(Me.dgvStrManip, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsStrManipBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsStrManipCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        CType(Me.bsOutputTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFill.SuspendLayout()
        Me.tabDetail.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.pnlSetupBottom.SuspendLayout()
        Me.RecordTypeIndicatorGroupBox.SuspendLayout()
        Me.grpDetailFilterSettings.SuspendLayout()
        CType(Me.dgvFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsFilterFieldCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRowFilterCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox9.SuspendLayout()
        Me.grpDetailAdviceSettings.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.LookupGroupBox.SuspendLayout()
        CType(Me.dgvLookup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsLookupBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsLookupSourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsLookupCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        Me.CalculationGroupBox.SuspendLayout()
        CType(Me.dgvCalc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCalcBankField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCalOperand1SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCalcOperand2SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCalcOperand3SourceField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsCalcCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRefField3Collection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRefField2Collection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRefField1Collection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.pnlTop.SuspendLayout()
        Me.pnlBottom.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.pnlMapBottom)
        Me.TabPage3.Controls.Add(Me.dgvBank)
        Me.TabPage3.Controls.Add(Me.dgvSource)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(907, 440)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "         Map         "
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'pnlMapBottom
        '
        Me.pnlMapBottom.Controls.Add(Me.btnClearAll)
        Me.pnlMapBottom.Controls.Add(Me.btnClear)
        Me.pnlMapBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlMapBottom.Location = New System.Drawing.Point(0, 393)
        Me.pnlMapBottom.Name = "pnlMapBottom"
        Me.pnlMapBottom.Size = New System.Drawing.Size(907, 47)
        Me.pnlMapBottom.TabIndex = 2
        '
        'btnClearAll
        '
        Me.btnClearAll.Location = New System.Drawing.Point(99, 3)
        Me.btnClearAll.Name = "btnClearAll"
        Me.btnClearAll.Size = New System.Drawing.Size(87, 27)
        Me.btnClearAll.TabIndex = 1
        Me.btnClearAll.Text = "Clear All"
        Me.btnClearAll.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(6, 3)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(87, 27)
        Me.btnClear.TabIndex = 0
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'dgvBank
        '
        Me.dgvBank.AllowDrop = True
        Me.dgvBank.AllowUserToAddRows = False
        Me.dgvBank.AllowUserToDeleteRows = False
        Me.dgvBank.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvBank.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBank.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBank.AutoGenerateColumns = False
        Me.dgvBank.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvBankBankFieldNameText, Me.dgvBankMappedFieldsText, Me.dgvBankMappedValuesText, Me.dgvBankBankSampleValueText, Me.MapSeparator, Me.CarriageReturn})
        Me.dgvBank.DataSource = Me.bsMapBankCollection
        Me.dgvBank.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvBank.Location = New System.Drawing.Point(446, 4)
        Me.dgvBank.MultiSelect = False
        Me.dgvBank.Name = "dgvBank"
        Me.dgvBank.RowHeadersVisible = False
        Me.dgvBank.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBank.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBank.Size = New System.Drawing.Size(458, 385)
        Me.dgvBank.TabIndex = 1
        '
        'dgvBankBankFieldNameText
        '
        Me.dgvBankBankFieldNameText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvBankBankFieldNameText.DataPropertyName = "BankFieldExt"
        Me.dgvBankBankFieldNameText.HeaderText = "Bank Field"
        Me.dgvBankBankFieldNameText.Name = "dgvBankBankFieldNameText"
        Me.dgvBankBankFieldNameText.ReadOnly = True
        '
        'dgvBankMappedFieldsText
        '
        Me.dgvBankMappedFieldsText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvBankMappedFieldsText.DataPropertyName = "MappedFields"
        Me.dgvBankMappedFieldsText.HeaderText = "Mapping Fields"
        Me.dgvBankMappedFieldsText.Name = "dgvBankMappedFieldsText"
        Me.dgvBankMappedFieldsText.ReadOnly = True
        '
        'dgvBankMappedValuesText
        '
        Me.dgvBankMappedValuesText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvBankMappedValuesText.DataPropertyName = "MappedValues"
        Me.dgvBankMappedValuesText.HeaderText = "Bank Value"
        Me.dgvBankMappedValuesText.Name = "dgvBankMappedValuesText"
        Me.dgvBankMappedValuesText.ReadOnly = True
        '
        'dgvBankBankSampleValueText
        '
        Me.dgvBankBankSampleValueText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvBankBankSampleValueText.DataPropertyName = "BankSampleValue"
        Me.dgvBankBankSampleValueText.HeaderText = "Bank Sample Value"
        Me.dgvBankBankSampleValueText.Name = "dgvBankBankSampleValueText"
        Me.dgvBankBankSampleValueText.ReadOnly = True
        '
        'MapSeparator
        '
        Me.MapSeparator.DataPropertyName = "MapSeparator"
        Me.MapSeparator.HeaderText = "MapSeparator"
        Me.MapSeparator.Name = "MapSeparator"
        '
        'CarriageReturn
        '
        Me.CarriageReturn.DataPropertyName = "CarriageReturn"
        Me.CarriageReturn.HeaderText = "CarriageReturn"
        Me.CarriageReturn.Name = "CarriageReturn"
        '
        'bsMapBankCollection
        '
        Me.bsMapBankCollection.DataMember = "MapBankFields"
        Me.bsMapBankCollection.DataSource = Me.bsDetailCollection
        '
        'bsDetailCollection
        '
        Me.bsDetailCollection.DataSource = GetType(BTMU.Magic.CommonTemplate.CommonTemplateExcelDetailCollection)
        '
        'dgvSource
        '
        Me.dgvSource.AllowDrop = True
        Me.dgvSource.AllowUserToAddRows = False
        Me.dgvSource.AllowUserToDeleteRows = False
        Me.dgvSource.AllowUserToResizeRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvSource.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvSource.AutoGenerateColumns = False
        Me.dgvSource.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sequence, Me.dgvSourceSourceFieldNameText, Me.dgvSourceSourceFieldValueText})
        Me.dgvSource.DataSource = Me.bsMapSourceCollection
        Me.dgvSource.Location = New System.Drawing.Point(6, 4)
        Me.dgvSource.MultiSelect = False
        Me.dgvSource.Name = "dgvSource"
        Me.dgvSource.ReadOnly = True
        Me.dgvSource.RowHeadersVisible = False
        Me.dgvSource.RowHeadersWidth = 30
        Me.dgvSource.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSource.Size = New System.Drawing.Size(436, 385)
        Me.dgvSource.TabIndex = 0
        '
        'Sequence
        '
        Me.Sequence.DataPropertyName = "Sequence"
        Me.Sequence.HeaderText = "No."
        Me.Sequence.Name = "Sequence"
        Me.Sequence.ReadOnly = True
        Me.Sequence.Width = 50
        '
        'dgvSourceSourceFieldNameText
        '
        Me.dgvSourceSourceFieldNameText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvSourceSourceFieldNameText.DataPropertyName = "SourceFieldName"
        Me.dgvSourceSourceFieldNameText.HeaderText = "Source Field Name"
        Me.dgvSourceSourceFieldNameText.Name = "dgvSourceSourceFieldNameText"
        Me.dgvSourceSourceFieldNameText.ReadOnly = True
        '
        'dgvSourceSourceFieldValueText
        '
        Me.dgvSourceSourceFieldValueText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvSourceSourceFieldValueText.DataPropertyName = "SourceFieldValue"
        Me.dgvSourceSourceFieldValueText.HeaderText = "Source Field Value"
        Me.dgvSourceSourceFieldValueText.Name = "dgvSourceSourceFieldValueText"
        Me.dgvSourceSourceFieldValueText.ReadOnly = True
        '
        'bsMapSourceCollection
        '
        Me.bsMapSourceCollection.DataMember = "MapSourceFields"
        Me.bsMapSourceCollection.DataSource = Me.bsDetailCollection
        '
        'dgvSampleData
        '
        Me.dgvSampleData.AllowUserToAddRows = False
        Me.dgvSampleData.AllowUserToDeleteRows = False
        Me.dgvSampleData.AllowUserToResizeRows = False
        Me.dgvSampleData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvSampleData.Location = New System.Drawing.Point(6, 18)
        Me.dgvSampleData.Name = "dgvSampleData"
        Me.dgvSampleData.ReadOnly = True
        Me.dgvSampleData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSampleData.Size = New System.Drawing.Size(867, 73)
        Me.dgvSampleData.TabIndex = 0
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.dgvSampleData)
        Me.GroupBox6.Location = New System.Drawing.Point(5, 138)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(879, 96)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Sample Row Data"
        '
        'txtSource
        '
        Me.txtSource.AllowDrop = True
        Me.txtSource.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "SampleSourceFile", True))
        Me.txtSource.Location = New System.Drawing.Point(136, 9)
        Me.txtSource.Name = "txtSource"
        Me.txtSource.ReadOnly = True
        Me.txtSource.Size = New System.Drawing.Size(507, 20)
        Me.txtSource.TabIndex = 1
        '
        'FieldSettingGroupBox
        '
        Me.FieldSettingGroupBox.Controls.Add(Me.txtTransactionStartRow)
        Me.FieldSettingGroupBox.Controls.Add(Me.txtHeaderAtRow)
        Me.FieldSettingGroupBox.Controls.Add(Me.btnRetrieve)
        Me.FieldSettingGroupBox.Controls.Add(Me.chkHeaderAsFieldName)
        Me.FieldSettingGroupBox.Controls.Add(Me.Label18)
        Me.FieldSettingGroupBox.Controls.Add(Me.Label19)
        Me.FieldSettingGroupBox.Location = New System.Drawing.Point(640, 29)
        Me.FieldSettingGroupBox.Name = "FieldSettingGroupBox"
        Me.FieldSettingGroupBox.Size = New System.Drawing.Size(244, 109)
        Me.FieldSettingGroupBox.TabIndex = 5
        Me.FieldSettingGroupBox.TabStop = False
        Me.FieldSettingGroupBox.Text = "Field Settings"
        '
        'txtTransactionStartRow
        '
        Me.txtTransactionStartRow.DataBindings.Add(New System.Windows.Forms.Binding("Number", Me.bsDetailCollection, "TransactionStartingRow", True))
        Me.txtTransactionStartRow.Location = New System.Drawing.Point(143, 47)
        Me.txtTransactionStartRow.MaxLength = 4
        Me.txtTransactionStartRow.Name = "txtTransactionStartRow"
        Me.txtTransactionStartRow.Size = New System.Drawing.Size(76, 22)
        Me.txtTransactionStartRow.TabIndex = 1
        '
        'txtHeaderAtRow
        '
        Me.txtHeaderAtRow.DataBindings.Add(New System.Windows.Forms.Binding("Number", Me.bsDetailCollection, "HeaderAtRow", True))
        Me.txtHeaderAtRow.Location = New System.Drawing.Point(143, 20)
        Me.txtHeaderAtRow.MaxLength = 4
        Me.txtHeaderAtRow.Name = "txtHeaderAtRow"
        Me.txtHeaderAtRow.Size = New System.Drawing.Size(76, 22)
        Me.txtHeaderAtRow.TabIndex = 0
        '
        'btnRetrieve
        '
        Me.btnRetrieve.Location = New System.Drawing.Point(143, 75)
        Me.btnRetrieve.Name = "btnRetrieve"
        Me.btnRetrieve.Size = New System.Drawing.Size(76, 27)
        Me.btnRetrieve.TabIndex = 3
        Me.btnRetrieve.Text = "&Retrieve"
        Me.btnRetrieve.UseVisualStyleBackColor = True
        '
        'chkHeaderAsFieldName
        '
        Me.chkHeaderAsFieldName.AutoSize = True
        Me.chkHeaderAsFieldName.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.bsDetailCollection, "HeaderAsFieldName", True))
        Me.chkHeaderAsFieldName.Location = New System.Drawing.Point(9, 80)
        Me.chkHeaderAsFieldName.Name = "chkHeaderAsFieldName"
        Me.chkHeaderAsFieldName.Size = New System.Drawing.Size(131, 17)
        Me.chkHeaderAsFieldName.TabIndex = 2
        Me.chkHeaderAsFieldName.Text = "Header as Field Name"
        Me.chkHeaderAsFieldName.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(6, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(134, 16)
        Me.Label18.TabIndex = 3
        Me.Label18.Text = "Transaction Start Row :"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 25)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(85, 13)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Header at Row :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(32, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Source File Name :"
        '
        'ExcelSettingGroupBox
        '
        Me.ExcelSettingGroupBox.Controls.Add(Me.txtSampleRowNo)
        Me.ExcelSettingGroupBox.Controls.Add(Me.Label3)
        Me.ExcelSettingGroupBox.Controls.Add(Me.txtEndCol)
        Me.ExcelSettingGroupBox.Controls.Add(Me.txtStartCol)
        Me.ExcelSettingGroupBox.Controls.Add(Me.Label8)
        Me.ExcelSettingGroupBox.Controls.Add(Me.Label9)
        Me.ExcelSettingGroupBox.Controls.Add(Me.cboWorksheet)
        Me.ExcelSettingGroupBox.Controls.Add(Me.Label17)
        Me.ExcelSettingGroupBox.Location = New System.Drawing.Point(5, 29)
        Me.ExcelSettingGroupBox.Name = "ExcelSettingGroupBox"
        Me.ExcelSettingGroupBox.Size = New System.Drawing.Size(391, 109)
        Me.ExcelSettingGroupBox.TabIndex = 3
        Me.ExcelSettingGroupBox.TabStop = False
        Me.ExcelSettingGroupBox.Text = "Excel   Settings"
        '
        'txtSampleRowNo
        '
        Me.txtSampleRowNo.DataBindings.Add(New System.Windows.Forms.Binding("Number", Me.bsDetailCollection, "SampleRowNo", True))
        Me.txtSampleRowNo.Location = New System.Drawing.Point(114, 85)
        Me.txtSampleRowNo.MaxLength = 4
        Me.txtSampleRowNo.Name = "txtSampleRowNo"
        Me.txtSampleRowNo.Size = New System.Drawing.Size(79, 22)
        Me.txtSampleRowNo.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sample Row :"
        '
        'txtEndCol
        '
        Me.txtEndCol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEndCol.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "EndColumn", True))
        Me.txtEndCol.Location = New System.Drawing.Point(114, 63)
        Me.txtEndCol.MaxLength = 2
        Me.txtEndCol.Name = "txtEndCol"
        Me.txtEndCol.Size = New System.Drawing.Size(79, 20)
        Me.txtEndCol.TabIndex = 2
        '
        'txtStartCol
        '
        Me.txtStartCol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtStartCol.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "StartColumn", True))
        Me.txtStartCol.Location = New System.Drawing.Point(114, 40)
        Me.txtStartCol.MaxLength = 2
        Me.txtStartCol.Name = "txtStartCol"
        Me.txtStartCol.Size = New System.Drawing.Size(79, 20)
        Me.txtStartCol.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 65)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "End Column :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(73, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Start Column :"
        '
        'cboWorksheet
        '
        Me.cboWorksheet.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsDetailCollection, "WorksheetName", True))
        Me.cboWorksheet.DataSource = Me.bsWorksheetCollection
        Me.cboWorksheet.DisplayMember = "WorksheetName"
        Me.cboWorksheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorksheet.FormattingEnabled = True
        Me.cboWorksheet.Location = New System.Drawing.Point(114, 16)
        Me.cboWorksheet.Name = "cboWorksheet"
        Me.cboWorksheet.Size = New System.Drawing.Size(246, 21)
        Me.cboWorksheet.TabIndex = 0
        Me.cboWorksheet.ValueMember = "WorksheetName"
        '
        'bsWorksheetCollection
        '
        Me.bsWorksheetCollection.DataMember = "Worksheets"
        Me.bsWorksheetCollection.DataSource = Me.bsDetailCollection
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(12, 18)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(96, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Worksheet Name :"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(649, 5)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 24)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "&Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'dgvEditable
        '
        Me.dgvEditable.AllowUserToResizeRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvEditable.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvEditable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvEditable.AutoGenerateColumns = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEditable.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvEditable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEditable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvEditablebtnDelete, Me.dgvEditableBankFieldNameCombo})
        Me.dgvEditable.DataSource = Me.bsEditableCollection
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEditable.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvEditable.Location = New System.Drawing.Point(6, 50)
        Me.dgvEditable.MultiSelect = False
        Me.dgvEditable.Name = "dgvEditable"
        Me.dgvEditable.RowHeadersVisible = False
        Me.dgvEditable.RowHeadersWidth = 30
        Me.dgvEditable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvEditable.Size = New System.Drawing.Size(886, 118)
        Me.dgvEditable.TabIndex = 2
        '
        'dgvEditablebtnDelete
        '
        Me.dgvEditablebtnDelete.HeaderText = "X"
        Me.dgvEditablebtnDelete.Name = "dgvEditablebtnDelete"
        Me.dgvEditablebtnDelete.Text = "X"
        Me.dgvEditablebtnDelete.UseColumnTextForButtonValue = True
        Me.dgvEditablebtnDelete.Width = 20
        '
        'dgvEditableBankFieldNameCombo
        '
        Me.dgvEditableBankFieldNameCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvEditableBankFieldNameCombo.DataPropertyName = "BankFieldName"
        Me.dgvEditableBankFieldNameCombo.DataSource = Me.bsEditableBankField
        Me.dgvEditableBankFieldNameCombo.HeaderText = "Bank Field"
        Me.dgvEditableBankFieldNameCombo.Name = "dgvEditableBankFieldNameCombo"
        Me.dgvEditableBankFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEditableBankFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'bsEditableBankField
        '
        Me.bsEditableBankField.DataMember = "EditableBankFields"
        Me.bsEditableBankField.DataSource = Me.bsDetailCollection
        '
        'bsEditableCollection
        '
        Me.bsEditableCollection.DataMember = "EditableSettings"
        Me.bsEditableCollection.DataSource = Me.bsDetailCollection
        '
        'btnUpTranslator
        '
        Me.btnUpTranslator.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpTranslator.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpTranslator.Location = New System.Drawing.Point(6, 18)
        Me.btnUpTranslator.Name = "btnUpTranslator"
        Me.btnUpTranslator.Size = New System.Drawing.Size(27, 25)
        Me.btnUpTranslator.TabIndex = 0
        Me.btnUpTranslator.Text = "▲"
        Me.btnUpTranslator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUpTranslator.UseVisualStyleBackColor = True
        '
        'TranslationGroupBox
        '
        Me.TranslationGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TranslationGroupBox.Controls.Add(Me.btnUpTranslator)
        Me.TranslationGroupBox.Controls.Add(Me.btnDownTranslator)
        Me.TranslationGroupBox.Controls.Add(Me.dgvTranslator)
        Me.TranslationGroupBox.Location = New System.Drawing.Point(6, 4)
        Me.TranslationGroupBox.Name = "TranslationGroupBox"
        Me.TranslationGroupBox.Size = New System.Drawing.Size(898, 260)
        Me.TranslationGroupBox.TabIndex = 0
        Me.TranslationGroupBox.TabStop = False
        Me.TranslationGroupBox.Text = "Translator Settings"
        '
        'btnDownTranslator
        '
        Me.btnDownTranslator.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownTranslator.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDownTranslator.Location = New System.Drawing.Point(39, 18)
        Me.btnDownTranslator.Name = "btnDownTranslator"
        Me.btnDownTranslator.Size = New System.Drawing.Size(27, 25)
        Me.btnDownTranslator.TabIndex = 1
        Me.btnDownTranslator.Text = "▼"
        Me.btnDownTranslator.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDownTranslator.UseVisualStyleBackColor = True
        '
        'dgvTranslator
        '
        Me.dgvTranslator.AllowUserToDeleteRows = False
        Me.dgvTranslator.AllowUserToResizeRows = False
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvTranslator.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvTranslator.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTranslator.AutoGenerateColumns = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTranslator.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvTranslator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTranslator.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvTranslatorbtnDelete, Me.dgvTranslatorBankFieldNameCombo, Me.dgvTranslatorCustomerValueText, Me.dgvTranslatorCellBankValueEditCbo, Me.BankValueEmpty})
        Me.dgvTranslator.DataSource = Me.bsTranslatorCollection
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTranslator.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvTranslator.Location = New System.Drawing.Point(6, 50)
        Me.dgvTranslator.MultiSelect = False
        Me.dgvTranslator.Name = "dgvTranslator"
        Me.dgvTranslator.RowHeadersVisible = False
        Me.dgvTranslator.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvTranslator.Size = New System.Drawing.Size(886, 205)
        Me.dgvTranslator.TabIndex = 2
        '
        'dgvTranslatorbtnDelete
        '
        Me.dgvTranslatorbtnDelete.HeaderText = "X"
        Me.dgvTranslatorbtnDelete.Name = "dgvTranslatorbtnDelete"
        Me.dgvTranslatorbtnDelete.Text = "X"
        Me.dgvTranslatorbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvTranslatorbtnDelete.Width = 20
        '
        'dgvTranslatorBankFieldNameCombo
        '
        Me.dgvTranslatorBankFieldNameCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorBankFieldNameCombo.DataPropertyName = "BankFieldName"
        Me.dgvTranslatorBankFieldNameCombo.DataSource = Me.bsTranslateBankField
        Me.dgvTranslatorBankFieldNameCombo.DisplayMember = "BankField"
        Me.dgvTranslatorBankFieldNameCombo.HeaderText = "Bank Field"
        Me.dgvTranslatorBankFieldNameCombo.Name = "dgvTranslatorBankFieldNameCombo"
        Me.dgvTranslatorBankFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTranslatorBankFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvTranslatorBankFieldNameCombo.ValueMember = "BankField"
        '
        'bsTranslateBankField
        '
        Me.bsTranslateBankField.DataMember = "MapBankFields"
        Me.bsTranslateBankField.DataSource = Me.bsDetailCollection
        '
        'dgvTranslatorCustomerValueText
        '
        Me.dgvTranslatorCustomerValueText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorCustomerValueText.DataPropertyName = "CustomerValue"
        Me.dgvTranslatorCustomerValueText.HeaderText = "Source Value"
        Me.dgvTranslatorCustomerValueText.Name = "dgvTranslatorCustomerValueText"
        '
        'dgvTranslatorCellBankValueEditCbo
        '
        Me.dgvTranslatorCellBankValueEditCbo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvTranslatorCellBankValueEditCbo.DataPropertyName = "BankValue"
        Me.dgvTranslatorCellBankValueEditCbo.HeaderText = "Bank Value"
        Me.dgvTranslatorCellBankValueEditCbo.Name = "dgvTranslatorCellBankValueEditCbo"
        '
        'BankValueEmpty
        '
        Me.BankValueEmpty.DataPropertyName = "BankValueEmpty"
        Me.BankValueEmpty.HeaderText = "Bank Value Empty"
        Me.BankValueEmpty.Name = "BankValueEmpty"
        '
        'bsTranslatorCollection
        '
        Me.bsTranslatorCollection.DataMember = "TranslatorSettings"
        Me.bsTranslatorCollection.DataSource = Me.bsDetailCollection
        '
        'btnUpEditable
        '
        Me.btnUpEditable.BackColor = System.Drawing.Color.Transparent
        Me.btnUpEditable.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpEditable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpEditable.Location = New System.Drawing.Point(6, 18)
        Me.btnUpEditable.Name = "btnUpEditable"
        Me.btnUpEditable.Size = New System.Drawing.Size(27, 25)
        Me.btnUpEditable.TabIndex = 0
        Me.btnUpEditable.Text = "▲"
        Me.btnUpEditable.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUpEditable.UseVisualStyleBackColor = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.EditableGroupBox)
        Me.TabPage4.Controls.Add(Me.TranslationGroupBox)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(907, 440)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "         Translation         "
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'EditableGroupBox
        '
        Me.EditableGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EditableGroupBox.BackColor = System.Drawing.Color.Transparent
        Me.EditableGroupBox.Controls.Add(Me.btnDownEditable)
        Me.EditableGroupBox.Controls.Add(Me.btnUpEditable)
        Me.EditableGroupBox.Controls.Add(Me.dgvEditable)
        Me.EditableGroupBox.Location = New System.Drawing.Point(6, 271)
        Me.EditableGroupBox.Name = "EditableGroupBox"
        Me.EditableGroupBox.Size = New System.Drawing.Size(898, 211)
        Me.EditableGroupBox.TabIndex = 1
        Me.EditableGroupBox.TabStop = False
        Me.EditableGroupBox.Text = "Editable Settings"
        '
        'btnDownEditable
        '
        Me.btnDownEditable.BackColor = System.Drawing.Color.Transparent
        Me.btnDownEditable.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownEditable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDownEditable.Location = New System.Drawing.Point(39, 18)
        Me.btnDownEditable.Name = "btnDownEditable"
        Me.btnDownEditable.Size = New System.Drawing.Size(27, 25)
        Me.btnDownEditable.TabIndex = 1
        Me.btnDownEditable.Text = "▼"
        Me.btnDownEditable.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDownEditable.UseVisualStyleBackColor = False
        '
        'btnDownStrManip
        '
        Me.btnDownStrManip.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownStrManip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDownStrManip.Location = New System.Drawing.Point(39, 23)
        Me.btnDownStrManip.Name = "btnDownStrManip"
        Me.btnDownStrManip.Size = New System.Drawing.Size(27, 25)
        Me.btnDownStrManip.TabIndex = 1
        Me.btnDownStrManip.Text = "▼"
        Me.btnDownStrManip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDownStrManip.UseVisualStyleBackColor = True
        '
        'StringManipulationGroupBox
        '
        Me.StringManipulationGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.StringManipulationGroupBox.Controls.Add(Me.btnDownStrManip)
        Me.StringManipulationGroupBox.Controls.Add(Me.btnUpStrManip)
        Me.StringManipulationGroupBox.Controls.Add(Me.dgvStrManip)
        Me.StringManipulationGroupBox.Location = New System.Drawing.Point(6, 4)
        Me.StringManipulationGroupBox.Name = "StringManipulationGroupBox"
        Me.StringManipulationGroupBox.Size = New System.Drawing.Size(898, 478)
        Me.StringManipulationGroupBox.TabIndex = 0
        Me.StringManipulationGroupBox.TabStop = False
        Me.StringManipulationGroupBox.Text = "String Manipulation"
        '
        'btnUpStrManip
        '
        Me.btnUpStrManip.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpStrManip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpStrManip.Location = New System.Drawing.Point(6, 23)
        Me.btnUpStrManip.Name = "btnUpStrManip"
        Me.btnUpStrManip.Size = New System.Drawing.Size(27, 25)
        Me.btnUpStrManip.TabIndex = 0
        Me.btnUpStrManip.Text = "▲"
        Me.btnUpStrManip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUpStrManip.UseVisualStyleBackColor = True
        '
        'dgvStrManip
        '
        Me.dgvStrManip.AllowUserToResizeRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvStrManip.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvStrManip.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvStrManip.AutoGenerateColumns = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStrManip.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvStrManip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStrManip.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvStrManipbtnDelete, Me.dgvStrManipBankFieldNameCombo, Me.dgvStrManipFunctionNameCombo, Me.dgvStrManipStartPositionText, Me.dgvStrManipNoOfCharactersText})
        Me.dgvStrManip.DataSource = Me.bsStrManipCollection
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvStrManip.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvStrManip.Location = New System.Drawing.Point(6, 54)
        Me.dgvStrManip.MultiSelect = False
        Me.dgvStrManip.Name = "dgvStrManip"
        Me.dgvStrManip.RowHeadersVisible = False
        Me.dgvStrManip.RowHeadersWidth = 30
        Me.dgvStrManip.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvStrManip.Size = New System.Drawing.Size(885, 380)
        Me.dgvStrManip.TabIndex = 2
        '
        'dgvStrManipbtnDelete
        '
        Me.dgvStrManipbtnDelete.HeaderText = "X"
        Me.dgvStrManipbtnDelete.Name = "dgvStrManipbtnDelete"
        Me.dgvStrManipbtnDelete.Text = "X"
        Me.dgvStrManipbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvStrManipbtnDelete.Width = 20
        '
        'dgvStrManipBankFieldNameCombo
        '
        Me.dgvStrManipBankFieldNameCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvStrManipBankFieldNameCombo.DataPropertyName = "BankFieldName"
        Me.dgvStrManipBankFieldNameCombo.DataSource = Me.bsStrManipBankField
        Me.dgvStrManipBankFieldNameCombo.DisplayMember = "BankField"
        Me.dgvStrManipBankFieldNameCombo.HeaderText = "Bank Field"
        Me.dgvStrManipBankFieldNameCombo.Name = "dgvStrManipBankFieldNameCombo"
        Me.dgvStrManipBankFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStrManipBankFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'bsStrManipBankField
        '
        Me.bsStrManipBankField.DataMember = "MapBankFields"
        Me.bsStrManipBankField.DataSource = Me.bsDetailCollection
        '
        'dgvStrManipFunctionNameCombo
        '
        Me.dgvStrManipFunctionNameCombo.DataPropertyName = "FunctionName"
        Me.dgvStrManipFunctionNameCombo.HeaderText = "Manipulation Function"
        Me.dgvStrManipFunctionNameCombo.Name = "dgvStrManipFunctionNameCombo"
        Me.dgvStrManipFunctionNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStrManipFunctionNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvStrManipFunctionNameCombo.Width = 259
        '
        'dgvStrManipStartPositionText
        '
        Me.dgvStrManipStartPositionText.DataPropertyName = "StartingPosition"
        Me.dgvStrManipStartPositionText.HeaderText = "Start Position"
        Me.dgvStrManipStartPositionText.MaxInputLength = 3
        Me.dgvStrManipStartPositionText.Name = "dgvStrManipStartPositionText"
        Me.dgvStrManipStartPositionText.Width = 172
        '
        'dgvStrManipNoOfCharactersText
        '
        Me.dgvStrManipNoOfCharactersText.DataPropertyName = "NumberOfCharacters"
        Me.dgvStrManipNoOfCharactersText.HeaderText = "No: Of Characters"
        Me.dgvStrManipNoOfCharactersText.MaxInputLength = 3
        Me.dgvStrManipNoOfCharactersText.Name = "dgvStrManipNoOfCharactersText"
        Me.dgvStrManipNoOfCharactersText.Width = 171
        '
        'bsStrManipCollection
        '
        Me.bsStrManipCollection.DataMember = "StringManipulations"
        Me.bsStrManipCollection.DataSource = Me.bsDetailCollection
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.StringManipulationGroupBox)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(907, 440)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "      String Manipulation       "
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'txtTemplateName
        '
        Me.txtTemplateName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "CommonTemplateName", True))
        Me.txtTemplateName.Location = New System.Drawing.Point(155, 40)
        Me.txtTemplateName.MaxLength = 100
        Me.txtTemplateName.Name = "txtTemplateName"
        Me.txtTemplateName.Size = New System.Drawing.Size(572, 20)
        Me.txtTemplateName.TabIndex = 1
        '
        'cboOutputTemplate
        '
        Me.cboOutputTemplate.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsDetailCollection, "OutputTemplate", True))
        Me.cboOutputTemplate.DataSource = Me.bsOutputTemplate
        Me.cboOutputTemplate.DisplayMember = "TemplateName"
        Me.cboOutputTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOutputTemplate.FormattingEnabled = True
        Me.cboOutputTemplate.Location = New System.Drawing.Point(155, 13)
        Me.cboOutputTemplate.Name = "cboOutputTemplate"
        Me.cboOutputTemplate.Size = New System.Drawing.Size(572, 21)
        Me.cboOutputTemplate.TabIndex = 0
        Me.cboOutputTemplate.ValueMember = "TemplateName"
        '
        'bsOutputTemplate
        '
        Me.bsOutputTemplate.DataSource = GetType(BTMU.Magic.MasterTemplate.MasterTemplateListCollection)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Common Template Name :"
        '
        'pnlFill
        '
        Me.pnlFill.Controls.Add(Me.tabDetail)
        Me.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFill.Location = New System.Drawing.Point(0, 81)
        Me.pnlFill.Name = "pnlFill"
        Me.pnlFill.Size = New System.Drawing.Size(915, 466)
        Me.pnlFill.TabIndex = 1
        '
        'tabDetail
        '
        Me.tabDetail.Controls.Add(Me.TabPage2)
        Me.tabDetail.Controls.Add(Me.TabPage3)
        Me.tabDetail.Controls.Add(Me.TabPage4)
        Me.tabDetail.Controls.Add(Me.TabPage5)
        Me.tabDetail.Controls.Add(Me.TabPage6)
        Me.tabDetail.Controls.Add(Me.TabPage7)
        Me.tabDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabDetail.Location = New System.Drawing.Point(0, 0)
        Me.tabDetail.Name = "tabDetail"
        Me.tabDetail.SelectedIndex = 0
        Me.tabDetail.Size = New System.Drawing.Size(915, 466)
        Me.tabDetail.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.pnlSetupBottom)
        Me.TabPage2.Controls.Add(Me.pnlSetup)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(907, 440)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "         Setup         "
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'pnlSetupBottom
        '
        Me.pnlSetupBottom.Controls.Add(Me.RecordTypeIndicatorGroupBox)
        Me.pnlSetupBottom.Controls.Add(Me.FieldSettingGroupBox)
        Me.pnlSetupBottom.Controls.Add(Me.btnBrowse)
        Me.pnlSetupBottom.Controls.Add(Me.Label7)
        Me.pnlSetupBottom.Controls.Add(Me.ExcelSettingGroupBox)
        Me.pnlSetupBottom.Controls.Add(Me.GroupBox6)
        Me.pnlSetupBottom.Controls.Add(Me.txtSource)
        Me.pnlSetupBottom.Controls.Add(Me.grpDetailFilterSettings)
        Me.pnlSetupBottom.Controls.Add(Me.GroupBox9)
        Me.pnlSetupBottom.Controls.Add(Me.grpDetailAdviceSettings)
        Me.pnlSetupBottom.Controls.Add(Me.GroupBox10)
        Me.pnlSetupBottom.Controls.Add(Me.GroupBox7)
        Me.pnlSetupBottom.Controls.Add(Me.GroupBox8)
        Me.pnlSetupBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSetupBottom.Location = New System.Drawing.Point(3, 3)
        Me.pnlSetupBottom.Name = "pnlSetupBottom"
        Me.pnlSetupBottom.Size = New System.Drawing.Size(901, 434)
        Me.pnlSetupBottom.TabIndex = 0
        '
        'RecordTypeIndicatorGroupBox
        '
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.Label32)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.Label4)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.IIndicatorTextBox)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.IColumnTextBox)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.WIndicatorTextBox)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.PIndicatorTextBox)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.PColumnTextBox)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.Label31)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.Label6)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.Label5)
        Me.RecordTypeIndicatorGroupBox.Controls.Add(Me.WColumnTextBox)
        Me.RecordTypeIndicatorGroupBox.Location = New System.Drawing.Point(402, 29)
        Me.RecordTypeIndicatorGroupBox.Name = "RecordTypeIndicatorGroupBox"
        Me.RecordTypeIndicatorGroupBox.Size = New System.Drawing.Size(231, 109)
        Me.RecordTypeIndicatorGroupBox.TabIndex = 4
        Me.RecordTypeIndicatorGroupBox.TabStop = False
        Me.RecordTypeIndicatorGroupBox.Text = "Record-Type Indicators"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(110, 14)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 13)
        Me.Label32.TabIndex = 15
        Me.Label32.Text = "Indicator"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Column"
        '
        'IIndicatorTextBox
        '
        Me.IIndicatorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "IIndicator", True))
        Me.IIndicatorTextBox.Location = New System.Drawing.Point(109, 81)
        Me.IIndicatorTextBox.MaxLength = 8
        Me.IIndicatorTextBox.Name = "IIndicatorTextBox"
        Me.IIndicatorTextBox.Size = New System.Drawing.Size(81, 20)
        Me.IIndicatorTextBox.TabIndex = 5
        '
        'IColumnTextBox
        '
        Me.IColumnTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "IColumn", True))
        Me.IColumnTextBox.Location = New System.Drawing.Point(38, 81)
        Me.IColumnTextBox.MaxLength = 2
        Me.IColumnTextBox.Name = "IColumnTextBox"
        Me.IColumnTextBox.Size = New System.Drawing.Size(50, 20)
        Me.IColumnTextBox.TabIndex = 4
        '
        'WIndicatorTextBox
        '
        Me.WIndicatorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "WIndicator", True))
        Me.WIndicatorTextBox.Location = New System.Drawing.Point(109, 55)
        Me.WIndicatorTextBox.MaxLength = 8
        Me.WIndicatorTextBox.Name = "WIndicatorTextBox"
        Me.WIndicatorTextBox.Size = New System.Drawing.Size(81, 20)
        Me.WIndicatorTextBox.TabIndex = 3
        '
        'PIndicatorTextBox
        '
        Me.PIndicatorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "PIndicator", True))
        Me.PIndicatorTextBox.Location = New System.Drawing.Point(109, 29)
        Me.PIndicatorTextBox.MaxLength = 8
        Me.PIndicatorTextBox.Name = "PIndicatorTextBox"
        Me.PIndicatorTextBox.Size = New System.Drawing.Size(81, 20)
        Me.PIndicatorTextBox.TabIndex = 1
        '
        'PColumnTextBox
        '
        Me.PColumnTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "PColumn", True))
        Me.PColumnTextBox.Location = New System.Drawing.Point(38, 29)
        Me.PColumnTextBox.MaxLength = 2
        Me.PColumnTextBox.Name = "PColumnTextBox"
        Me.PColumnTextBox.Size = New System.Drawing.Size(50, 20)
        Me.PColumnTextBox.TabIndex = 0
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(9, 85)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(16, 13)
        Me.Label31.TabIndex = 4
        Me.Label31.Text = "I :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 59)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "W :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "P :"
        '
        'WColumnTextBox
        '
        Me.WColumnTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "WColumn", True))
        Me.WColumnTextBox.Location = New System.Drawing.Point(38, 55)
        Me.WColumnTextBox.MaxLength = 2
        Me.WColumnTextBox.Name = "WColumnTextBox"
        Me.WColumnTextBox.Size = New System.Drawing.Size(50, 20)
        Me.WColumnTextBox.TabIndex = 2
        '
        'grpDetailFilterSettings
        '
        Me.grpDetailFilterSettings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpDetailFilterSettings.Controls.Add(Me.btnFilterUp)
        Me.grpDetailFilterSettings.Controls.Add(Me.btnFilterDown)
        Me.grpDetailFilterSettings.Controls.Add(Me.rdoFilterOR)
        Me.grpDetailFilterSettings.Controls.Add(Me.rdoFilterAND)
        Me.grpDetailFilterSettings.Controls.Add(Me.dgvFilter)
        Me.grpDetailFilterSettings.Location = New System.Drawing.Point(7, 331)
        Me.grpDetailFilterSettings.Name = "grpDetailFilterSettings"
        Me.grpDetailFilterSettings.Size = New System.Drawing.Size(601, 102)
        Me.grpDetailFilterSettings.TabIndex = 11
        Me.grpDetailFilterSettings.TabStop = False
        Me.grpDetailFilterSettings.Text = "Filter Setting"
        '
        'btnFilterUp
        '
        Me.btnFilterUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilterUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFilterUp.Location = New System.Drawing.Point(7, 12)
        Me.btnFilterUp.Name = "btnFilterUp"
        Me.btnFilterUp.Size = New System.Drawing.Size(25, 25)
        Me.btnFilterUp.TabIndex = 0
        Me.btnFilterUp.Text = "▲"
        Me.btnFilterUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFilterUp.UseVisualStyleBackColor = True
        '
        'btnFilterDown
        '
        Me.btnFilterDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilterDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFilterDown.Location = New System.Drawing.Point(38, 12)
        Me.btnFilterDown.Name = "btnFilterDown"
        Me.btnFilterDown.Size = New System.Drawing.Size(25, 25)
        Me.btnFilterDown.TabIndex = 1
        Me.btnFilterDown.Text = "▼"
        Me.btnFilterDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFilterDown.UseVisualStyleBackColor = True
        '
        'rdoFilterOR
        '
        Me.rdoFilterOR.AutoSize = True
        Me.rdoFilterOR.Location = New System.Drawing.Point(297, 15)
        Me.rdoFilterOR.Name = "rdoFilterOR"
        Me.rdoFilterOR.Size = New System.Drawing.Size(41, 17)
        Me.rdoFilterOR.TabIndex = 3
        Me.rdoFilterOR.TabStop = True
        Me.rdoFilterOR.Text = "OR"
        Me.rdoFilterOR.UseVisualStyleBackColor = True
        '
        'rdoFilterAND
        '
        Me.rdoFilterAND.AutoSize = True
        Me.rdoFilterAND.Location = New System.Drawing.Point(233, 15)
        Me.rdoFilterAND.Name = "rdoFilterAND"
        Me.rdoFilterAND.Size = New System.Drawing.Size(48, 17)
        Me.rdoFilterAND.TabIndex = 2
        Me.rdoFilterAND.TabStop = True
        Me.rdoFilterAND.Text = "AND"
        Me.rdoFilterAND.UseVisualStyleBackColor = True
        '
        'dgvFilter
        '
        Me.dgvFilter.AllowUserToDeleteRows = False
        Me.dgvFilter.AllowUserToResizeRows = False
        Me.dgvFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvFilter.AutoGenerateColumns = False
        Me.dgvFilter.BackgroundColor = System.Drawing.SystemColors.ControlDark
        Me.dgvFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFilter.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvFilterbtnDelete, Me.dgvFilterFilterFieldCombo, Me.dgvFilterCellFilterTypeComboBox, Me.dgvFilterFilterOperatorCombo, Me.dgvFilterFilterValueText})
        Me.dgvFilter.DataSource = Me.bsRowFilterCollection
        Me.dgvFilter.Location = New System.Drawing.Point(8, 42)
        Me.dgvFilter.MultiSelect = False
        Me.dgvFilter.Name = "dgvFilter"
        Me.dgvFilter.RowHeadersVisible = False
        Me.dgvFilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFilter.Size = New System.Drawing.Size(582, 53)
        Me.dgvFilter.TabIndex = 4
        '
        'dgvFilterbtnDelete
        '
        Me.dgvFilterbtnDelete.HeaderText = "X"
        Me.dgvFilterbtnDelete.Name = "dgvFilterbtnDelete"
        Me.dgvFilterbtnDelete.Text = "X"
        Me.dgvFilterbtnDelete.ToolTipText = "Delete"
        Me.dgvFilterbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvFilterbtnDelete.Width = 20
        '
        'dgvFilterFilterFieldCombo
        '
        Me.dgvFilterFilterFieldCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvFilterFilterFieldCombo.DataPropertyName = "FilterField"
        Me.dgvFilterFilterFieldCombo.DataSource = Me.bsFilterFieldCollection
        Me.dgvFilterFilterFieldCombo.DisplayMember = "SourceFieldName"
        Me.dgvFilterFilterFieldCombo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.dgvFilterFilterFieldCombo.HeaderText = "Filter Field"
        Me.dgvFilterFilterFieldCombo.Name = "dgvFilterFilterFieldCombo"
        Me.dgvFilterFilterFieldCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilterFilterFieldCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvFilterFilterFieldCombo.ValueMember = "SourceFieldName"
        '
        'bsFilterFieldCollection
        '
        Me.bsFilterFieldCollection.DataMember = "MapSourceFields"
        Me.bsFilterFieldCollection.DataSource = Me.bsDetailCollection
        '
        'dgvFilterCellFilterTypeComboBox
        '
        Me.dgvFilterCellFilterTypeComboBox.DataPropertyName = "FilterType"
        Me.dgvFilterCellFilterTypeComboBox.HeaderText = "Data Type"
        Me.dgvFilterCellFilterTypeComboBox.Name = "dgvFilterCellFilterTypeComboBox"
        Me.dgvFilterCellFilterTypeComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilterCellFilterTypeComboBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvFilterFilterOperatorCombo
        '
        Me.dgvFilterFilterOperatorCombo.DataPropertyName = "FilterOperator"
        Me.dgvFilterFilterOperatorCombo.HeaderText = "Filter Operator"
        Me.dgvFilterFilterOperatorCombo.Name = "dgvFilterFilterOperatorCombo"
        Me.dgvFilterFilterOperatorCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilterFilterOperatorCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvFilterFilterValueText
        '
        Me.dgvFilterFilterValueText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvFilterFilterValueText.DataPropertyName = "FilterValue"
        Me.dgvFilterFilterValueText.HeaderText = "Filter Value"
        Me.dgvFilterFilterValueText.Name = "dgvFilterFilterValueText"
        '
        'bsRowFilterCollection
        '
        Me.bsRowFilterCollection.DataMember = "Filters"
        Me.bsRowFilterCollection.DataSource = Me.bsDetailCollection
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.rdoCents)
        Me.GroupBox9.Controls.Add(Me.rdoDollars)
        Me.GroupBox9.Location = New System.Drawing.Point(487, 239)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(121, 89)
        Me.GroupBox9.TabIndex = 9
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Remittance Amount Settings"
        '
        'rdoCents
        '
        Me.rdoCents.AutoSize = True
        Me.rdoCents.Location = New System.Drawing.Point(20, 60)
        Me.rdoCents.Name = "rdoCents"
        Me.rdoCents.Size = New System.Drawing.Size(52, 17)
        Me.rdoCents.TabIndex = 1
        Me.rdoCents.TabStop = True
        Me.rdoCents.Text = "Cents"
        Me.rdoCents.UseVisualStyleBackColor = True
        '
        'rdoDollars
        '
        Me.rdoDollars.AutoSize = True
        Me.rdoDollars.Location = New System.Drawing.Point(20, 36)
        Me.rdoDollars.Name = "rdoDollars"
        Me.rdoDollars.Size = New System.Drawing.Size(57, 17)
        Me.rdoDollars.TabIndex = 0
        Me.rdoDollars.TabStop = True
        Me.rdoDollars.Text = "Dollars"
        Me.rdoDollars.UseVisualStyleBackColor = True
        '
        'grpDetailAdviceSettings
        '
        Me.grpDetailAdviceSettings.Controls.Add(Me.txtAdviceDelimiter)
        Me.grpDetailAdviceSettings.Controls.Add(Me.txtAdviceChar)
        Me.grpDetailAdviceSettings.Controls.Add(Me.Label23)
        Me.grpDetailAdviceSettings.Controls.Add(Me.Label22)
        Me.grpDetailAdviceSettings.Controls.Add(Me.rdoAdviceDelimited)
        Me.grpDetailAdviceSettings.Controls.Add(Me.rdoAdviceAftEveryChar)
        Me.grpDetailAdviceSettings.Controls.Add(Me.rdoAdviceForEveryRow)
        Me.grpDetailAdviceSettings.Location = New System.Drawing.Point(624, 331)
        Me.grpDetailAdviceSettings.Name = "grpDetailAdviceSettings"
        Me.grpDetailAdviceSettings.Size = New System.Drawing.Size(260, 102)
        Me.grpDetailAdviceSettings.TabIndex = 12
        Me.grpDetailAdviceSettings.TabStop = False
        Me.grpDetailAdviceSettings.Text = "For iRTMS Format"
        '
        'txtAdviceDelimiter
        '
        Me.txtAdviceDelimiter.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "AdviceRecDelimiter", True))
        Me.txtAdviceDelimiter.Location = New System.Drawing.Point(165, 75)
        Me.txtAdviceDelimiter.MaxLength = 1
        Me.txtAdviceDelimiter.Name = "txtAdviceDelimiter"
        Me.txtAdviceDelimiter.Size = New System.Drawing.Size(31, 20)
        Me.txtAdviceDelimiter.TabIndex = 4
        '
        'txtAdviceChar
        '
        Me.txtAdviceChar.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsDetailCollection, "AdviceRecChar", True))
        Me.txtAdviceChar.Location = New System.Drawing.Point(112, 52)
        Me.txtAdviceChar.MaxLength = 4
        Me.txtAdviceChar.Name = "txtAdviceChar"
        Me.txtAdviceChar.Size = New System.Drawing.Size(31, 20)
        Me.txtAdviceChar.TabIndex = 2
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(162, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(52, 13)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "character"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(22, 15)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(106, 13)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "New Advice Record "
        '
        'rdoAdviceDelimited
        '
        Me.rdoAdviceDelimited.AutoSize = True
        Me.rdoAdviceDelimited.Location = New System.Drawing.Point(25, 77)
        Me.rdoAdviceDelimited.Name = "rdoAdviceDelimited"
        Me.rdoAdviceDelimited.Size = New System.Drawing.Size(127, 17)
        Me.rdoAdviceDelimited.TabIndex = 3
        Me.rdoAdviceDelimited.TabStop = True
        Me.rdoAdviceDelimited.Text = "separated by delimiter"
        Me.rdoAdviceDelimited.UseVisualStyleBackColor = True
        '
        'rdoAdviceAftEveryChar
        '
        Me.rdoAdviceAftEveryChar.AutoSize = True
        Me.rdoAdviceAftEveryChar.Location = New System.Drawing.Point(25, 54)
        Me.rdoAdviceAftEveryChar.Name = "rdoAdviceAftEveryChar"
        Me.rdoAdviceAftEveryChar.Size = New System.Drawing.Size(78, 17)
        Me.rdoAdviceAftEveryChar.TabIndex = 1
        Me.rdoAdviceAftEveryChar.TabStop = True
        Me.rdoAdviceAftEveryChar.Text = " after every"
        Me.rdoAdviceAftEveryChar.UseVisualStyleBackColor = True
        '
        'rdoAdviceForEveryRow
        '
        Me.rdoAdviceForEveryRow.AutoSize = True
        Me.rdoAdviceForEveryRow.Location = New System.Drawing.Point(25, 30)
        Me.rdoAdviceForEveryRow.Name = "rdoAdviceForEveryRow"
        Me.rdoAdviceForEveryRow.Size = New System.Drawing.Size(122, 17)
        Me.rdoAdviceForEveryRow.TabIndex = 0
        Me.rdoAdviceForEveryRow.TabStop = True
        Me.rdoAdviceForEveryRow.Text = "for every row of data"
        Me.rdoAdviceForEveryRow.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.cboRef3)
        Me.GroupBox10.Controls.Add(Me.cboRef2)
        Me.GroupBox10.Controls.Add(Me.cboRef1)
        Me.GroupBox10.Controls.Add(Me.Label16)
        Me.GroupBox10.Controls.Add(Me.Label15)
        Me.GroupBox10.Controls.Add(Me.Label14)
        Me.GroupBox10.Location = New System.Drawing.Point(626, 237)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(260, 89)
        Me.GroupBox10.TabIndex = 10
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Duplicate Transaction Reference"
        '
        'cboRef3
        '
        Me.cboRef3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsDetailCollection, "ReferenceField3", True))
        Me.cboRef3.DisplayMember = "SourceFieldName"
        Me.cboRef3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRef3.FormattingEnabled = True
        Me.cboRef3.Location = New System.Drawing.Point(89, 62)
        Me.cboRef3.Name = "cboRef3"
        Me.cboRef3.Size = New System.Drawing.Size(127, 21)
        Me.cboRef3.TabIndex = 2
        Me.cboRef3.ValueMember = "SourceFieldName"
        '
        'cboRef2
        '
        Me.cboRef2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsDetailCollection, "ReferenceField2", True))
        Me.cboRef2.DisplayMember = "SourceFieldName"
        Me.cboRef2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRef2.FormattingEnabled = True
        Me.cboRef2.Location = New System.Drawing.Point(89, 39)
        Me.cboRef2.Name = "cboRef2"
        Me.cboRef2.Size = New System.Drawing.Size(127, 21)
        Me.cboRef2.TabIndex = 1
        Me.cboRef2.ValueMember = "SourceFieldName"
        '
        'cboRef1
        '
        Me.cboRef1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.bsDetailCollection, "ReferenceField1", True))
        Me.cboRef1.DisplayMember = "SourceFieldName"
        Me.cboRef1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRef1.FormattingEnabled = True
        Me.cboRef1.Location = New System.Drawing.Point(89, 16)
        Me.cboRef1.Name = "cboRef1"
        Me.cboRef1.Size = New System.Drawing.Size(127, 21)
        Me.cboRef1.TabIndex = 0
        Me.cboRef1.ValueMember = "SourceFieldName"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(25, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(64, 13)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Ref. Field2 :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(25, 69)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Ref. Field3 :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(25, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Ref. Field1 :"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chkZerosInDate)
        Me.GroupBox7.Controls.Add(Me.cboDateType)
        Me.GroupBox7.Controls.Add(Me.cboDateSep)
        Me.GroupBox7.Controls.Add(Me.Label10)
        Me.GroupBox7.Controls.Add(Me.Label11)
        Me.GroupBox7.Location = New System.Drawing.Point(254, 239)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(215, 89)
        Me.GroupBox7.TabIndex = 8
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Date Settings"
        '
        'chkZerosInDate
        '
        Me.chkZerosInDate.AutoSize = True
        Me.chkZerosInDate.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.bsDetailCollection, "IsZerosInDate", True))
        Me.chkZerosInDate.Location = New System.Drawing.Point(12, 68)
        Me.chkZerosInDate.Name = "chkZerosInDate"
        Me.chkZerosInDate.Size = New System.Drawing.Size(173, 17)
        Me.chkZerosInDate.TabIndex = 2
        Me.chkZerosInDate.Text = "Zeros in Date(Eg:-1900/01/01)"
        Me.chkZerosInDate.UseVisualStyleBackColor = True
        '
        'cboDateType
        '
        Me.cboDateType.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.bsDetailCollection, "DateType", True))
        Me.cboDateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDateType.FormattingEnabled = True
        Me.cboDateType.Location = New System.Drawing.Point(106, 39)
        Me.cboDateType.Name = "cboDateType"
        Me.cboDateType.Size = New System.Drawing.Size(98, 21)
        Me.cboDateType.TabIndex = 1
        '
        'cboDateSep
        '
        Me.cboDateSep.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.bsDetailCollection, "DateSeparator", True))
        Me.cboDateSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDateSep.FormattingEnabled = True
        Me.cboDateSep.Location = New System.Drawing.Point(106, 13)
        Me.cboDateSep.Name = "cboDateSep"
        Me.cboDateSep.Size = New System.Drawing.Size(98, 21)
        Me.cboDateSep.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Date Separator :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Date Type :"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.cboThousandSep)
        Me.GroupBox8.Controls.Add(Me.cboDecimalSep)
        Me.GroupBox8.Controls.Add(Me.Label13)
        Me.GroupBox8.Controls.Add(Me.Label12)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 239)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(230, 89)
        Me.GroupBox8.TabIndex = 7
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Number Settings"
        '
        'cboThousandSep
        '
        Me.cboThousandSep.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.bsDetailCollection, "ThousandSeparator", True))
        Me.cboThousandSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboThousandSep.FormattingEnabled = True
        Me.cboThousandSep.Location = New System.Drawing.Point(126, 58)
        Me.cboThousandSep.Name = "cboThousandSep"
        Me.cboThousandSep.Size = New System.Drawing.Size(94, 21)
        Me.cboThousandSep.TabIndex = 1
        '
        'cboDecimalSep
        '
        Me.cboDecimalSep.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.bsDetailCollection, "DecimalSeparator", True))
        Me.cboDecimalSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDecimalSep.FormattingEnabled = True
        Me.cboDecimalSep.Location = New System.Drawing.Point(126, 22)
        Me.cboDecimalSep.Name = "cboDecimalSep"
        Me.cboDecimalSep.Size = New System.Drawing.Size(95, 21)
        Me.cboDecimalSep.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(6, 62)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(113, 17)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Thousand Separator :"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(6, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(102, 17)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Decimal Separator :"
        '
        'pnlSetup
        '
        Me.pnlSetup.AutoScroll = True
        Me.pnlSetup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSetup.Location = New System.Drawing.Point(3, 3)
        Me.pnlSetup.Name = "pnlSetup"
        Me.pnlSetup.Size = New System.Drawing.Size(901, 434)
        Me.pnlSetup.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.LookupGroupBox)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(907, 440)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "         Lookup         "
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'LookupGroupBox
        '
        Me.LookupGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LookupGroupBox.Controls.Add(Me.dgvLookup)
        Me.LookupGroupBox.Controls.Add(Me.btnLookupDown)
        Me.LookupGroupBox.Controls.Add(Me.btnLookupUp)
        Me.LookupGroupBox.Location = New System.Drawing.Point(6, 4)
        Me.LookupGroupBox.Name = "LookupGroupBox"
        Me.LookupGroupBox.Size = New System.Drawing.Size(898, 483)
        Me.LookupGroupBox.TabIndex = 0
        Me.LookupGroupBox.TabStop = False
        Me.LookupGroupBox.Text = "Lookup"
        '
        'dgvLookup
        '
        Me.dgvLookup.AllowUserToResizeRows = False
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvLookup.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvLookup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLookup.AutoGenerateColumns = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookup.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvLookup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLookup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvLookupbtnDelete, Me.dgvLookupBankFieldNameCombo, Me.dgvLookupSourceFieldNameCombo, Me.dgvLookupTableNameCombo, Me.dgvLookupLookupKeyCombo, Me.dgvLookupLookupValueCombo, Me.dgvLookupTableNameCombo2, Me.dgvLookupLookupKeyCombo2, Me.dgvLookupLookupValueCombo2, Me.dgvLookupTableNameCombo3, Me.dgvLookupLookupKeyCombo3, Me.dgvLookupLookupValueCombo3})
        Me.dgvLookup.DataSource = Me.bsLookupCollection
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLookup.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgvLookup.Location = New System.Drawing.Point(6, 54)
        Me.dgvLookup.MultiSelect = False
        Me.dgvLookup.Name = "dgvLookup"
        Me.dgvLookup.RowHeadersVisible = False
        Me.dgvLookup.RowHeadersWidth = 30
        Me.dgvLookup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvLookup.Size = New System.Drawing.Size(886, 381)
        Me.dgvLookup.TabIndex = 2
        '
        'bsLookupBankField
        '
        Me.bsLookupBankField.DataMember = "MapBankFields"
        Me.bsLookupBankField.DataSource = Me.bsDetailCollection
        '
        'bsLookupSourceField
        '
        Me.bsLookupSourceField.DataMember = "MapSourceFields"
        Me.bsLookupSourceField.DataSource = Me.bsDetailCollection
        '
        'bsLookupCollection
        '
        Me.bsLookupCollection.DataMember = "LookupSettings"
        Me.bsLookupCollection.DataSource = Me.bsDetailCollection
        '
        'btnLookupDown
        '
        Me.btnLookupDown.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLookupDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLookupDown.Location = New System.Drawing.Point(40, 21)
        Me.btnLookupDown.Name = "btnLookupDown"
        Me.btnLookupDown.Size = New System.Drawing.Size(27, 25)
        Me.btnLookupDown.TabIndex = 1
        Me.btnLookupDown.Text = "▼"
        Me.btnLookupDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLookupDown.UseVisualStyleBackColor = True
        '
        'btnLookupUp
        '
        Me.btnLookupUp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLookupUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLookupUp.Location = New System.Drawing.Point(6, 21)
        Me.btnLookupUp.Name = "btnLookupUp"
        Me.btnLookupUp.Size = New System.Drawing.Size(27, 25)
        Me.btnLookupUp.TabIndex = 0
        Me.btnLookupUp.Text = "▲"
        Me.btnLookupUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLookupUp.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.CalculationGroupBox)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(907, 440)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "          Calculation       "
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'CalculationGroupBox
        '
        Me.CalculationGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CalculationGroupBox.Controls.Add(Me.btnDownCalc)
        Me.CalculationGroupBox.Controls.Add(Me.btnUpCalc)
        Me.CalculationGroupBox.Controls.Add(Me.dgvCalc)
        Me.CalculationGroupBox.Location = New System.Drawing.Point(6, 4)
        Me.CalculationGroupBox.Name = "CalculationGroupBox"
        Me.CalculationGroupBox.Size = New System.Drawing.Size(898, 479)
        Me.CalculationGroupBox.TabIndex = 0
        Me.CalculationGroupBox.TabStop = False
        Me.CalculationGroupBox.Text = "Calculation Fields"
        '
        'btnDownCalc
        '
        Me.btnDownCalc.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownCalc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDownCalc.Location = New System.Drawing.Point(40, 23)
        Me.btnDownCalc.Name = "btnDownCalc"
        Me.btnDownCalc.Size = New System.Drawing.Size(27, 25)
        Me.btnDownCalc.TabIndex = 1
        Me.btnDownCalc.Text = "▼"
        Me.btnDownCalc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDownCalc.UseVisualStyleBackColor = True
        '
        'btnUpCalc
        '
        Me.btnUpCalc.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpCalc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpCalc.Location = New System.Drawing.Point(6, 23)
        Me.btnUpCalc.Name = "btnUpCalc"
        Me.btnUpCalc.Size = New System.Drawing.Size(27, 25)
        Me.btnUpCalc.TabIndex = 0
        Me.btnUpCalc.Text = "▲"
        Me.btnUpCalc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUpCalc.UseVisualStyleBackColor = True
        '
        'dgvCalc
        '
        Me.dgvCalc.AllowUserToResizeRows = False
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvCalc.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvCalc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCalc.AutoGenerateColumns = False
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalc.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvCalc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCalc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvCalcbtnDelete, Me.dgvCalcBankFieldNameCombo, Me.dgvCalcOperand1Combo, Me.dgvCalOperator1Combo, Me.dgvCalcOperand2Combo, Me.dgvCalcOperator2Combo, Me.dgvCalcOperand3Combo})
        Me.dgvCalc.DataSource = Me.bsCalcCollection
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCalc.DefaultCellStyle = DataGridViewCellStyle17
        Me.dgvCalc.Location = New System.Drawing.Point(6, 54)
        Me.dgvCalc.MultiSelect = False
        Me.dgvCalc.Name = "dgvCalc"
        Me.dgvCalc.RowHeadersVisible = False
        Me.dgvCalc.RowHeadersWidth = 30
        Me.dgvCalc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCalc.Size = New System.Drawing.Size(885, 381)
        Me.dgvCalc.TabIndex = 2
        '
        'dgvCalcbtnDelete
        '
        Me.dgvCalcbtnDelete.HeaderText = "X"
        Me.dgvCalcbtnDelete.Name = "dgvCalcbtnDelete"
        Me.dgvCalcbtnDelete.Text = "X"
        Me.dgvCalcbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvCalcbtnDelete.Width = 20
        '
        'dgvCalcBankFieldNameCombo
        '
        Me.dgvCalcBankFieldNameCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalcBankFieldNameCombo.DataPropertyName = "BankFieldName"
        Me.dgvCalcBankFieldNameCombo.DataSource = Me.bsCalcBankField
        Me.dgvCalcBankFieldNameCombo.DisplayMember = "BankField"
        Me.dgvCalcBankFieldNameCombo.HeaderText = "Bank Field"
        Me.dgvCalcBankFieldNameCombo.Name = "dgvCalcBankFieldNameCombo"
        Me.dgvCalcBankFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalcBankFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'bsCalcBankField
        '
        Me.bsCalcBankField.DataMember = "MapBankFields"
        Me.bsCalcBankField.DataSource = Me.bsDetailCollection
        '
        'dgvCalcOperand1Combo
        '
        Me.dgvCalcOperand1Combo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalcOperand1Combo.DataPropertyName = "Operand1"
        Me.dgvCalcOperand1Combo.DataSource = Me.bsCalOperand1SourceField
        Me.dgvCalcOperand1Combo.DisplayMember = "SourceFieldName"
        Me.dgvCalcOperand1Combo.HeaderText = "Operand 1"
        Me.dgvCalcOperand1Combo.Name = "dgvCalcOperand1Combo"
        Me.dgvCalcOperand1Combo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalcOperand1Combo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCalcOperand1Combo.ValueMember = "SourceFieldName"
        '
        'bsCalOperand1SourceField
        '
        Me.bsCalOperand1SourceField.DataMember = "MapSourceFields"
        Me.bsCalOperand1SourceField.DataSource = Me.bsDetailCollection
        '
        'dgvCalOperator1Combo
        '
        Me.dgvCalOperator1Combo.DataPropertyName = "Operator1"
        Me.dgvCalOperator1Combo.HeaderText = "Operator 1"
        Me.dgvCalOperator1Combo.Name = "dgvCalOperator1Combo"
        Me.dgvCalOperator1Combo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalOperator1Combo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCalOperator1Combo.Width = 84
        '
        'dgvCalcOperand2Combo
        '
        Me.dgvCalcOperand2Combo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalcOperand2Combo.DataPropertyName = "Operand2"
        Me.dgvCalcOperand2Combo.DataSource = Me.bsCalcOperand2SourceField
        Me.dgvCalcOperand2Combo.DisplayMember = "SourceFieldName"
        Me.dgvCalcOperand2Combo.HeaderText = "Operand 2"
        Me.dgvCalcOperand2Combo.Name = "dgvCalcOperand2Combo"
        Me.dgvCalcOperand2Combo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalcOperand2Combo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCalcOperand2Combo.ValueMember = "SourceFieldName"
        '
        'bsCalcOperand2SourceField
        '
        Me.bsCalcOperand2SourceField.DataMember = "MapSourceFields"
        Me.bsCalcOperand2SourceField.DataSource = Me.bsDetailCollection
        '
        'dgvCalcOperator2Combo
        '
        Me.dgvCalcOperator2Combo.DataPropertyName = "Operator2"
        Me.dgvCalcOperator2Combo.HeaderText = "Operator 2"
        Me.dgvCalcOperator2Combo.Name = "dgvCalcOperator2Combo"
        Me.dgvCalcOperator2Combo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalcOperator2Combo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCalcOperator2Combo.Width = 84
        '
        'dgvCalcOperand3Combo
        '
        Me.dgvCalcOperand3Combo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvCalcOperand3Combo.DataPropertyName = "Operand3"
        Me.dgvCalcOperand3Combo.DataSource = Me.bsCalcOperand3SourceField
        Me.dgvCalcOperand3Combo.DisplayMember = "SourceFieldName"
        Me.dgvCalcOperand3Combo.HeaderText = "Operand 3"
        Me.dgvCalcOperand3Combo.Name = "dgvCalcOperand3Combo"
        Me.dgvCalcOperand3Combo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalcOperand3Combo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvCalcOperand3Combo.ValueMember = "SourceFieldName"
        '
        'bsCalcOperand3SourceField
        '
        Me.bsCalcOperand3SourceField.DataMember = "MapSourceFields"
        Me.bsCalcOperand3SourceField.DataSource = Me.bsDetailCollection
        '
        'bsCalcCollection
        '
        Me.bsCalcCollection.DataMember = "CalculatedFields"
        Me.bsCalcCollection.DataSource = Me.bsDetailCollection
        '
        'bsRefField3Collection
        '
        Me.bsRefField3Collection.DataMember = "MapSourceFields"
        Me.bsRefField3Collection.DataSource = Me.bsDetailCollection
        '
        'bsRefField2Collection
        '
        Me.bsRefField2Collection.DataMember = "MapSourceFields"
        Me.bsRefField2Collection.DataSource = Me.bsDetailCollection
        '
        'bsRefField1Collection
        '
        Me.bsRefField1Collection.DataMember = "MapSourceFields"
        Me.bsRefField1Collection.DataSource = Me.bsDetailCollection
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Master Template Name :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkEnable)
        Me.GroupBox1.Controls.Add(Me.txtTemplateName)
        Me.GroupBox1.Controls.Add(Me.cboOutputTemplate)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(902, 68)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Template Settings"
        '
        'chkEnable
        '
        Me.chkEnable.AutoSize = True
        Me.chkEnable.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.bsDetailCollection, "IsEnabled", True))
        Me.chkEnable.Location = New System.Drawing.Point(755, 42)
        Me.chkEnable.Name = "chkEnable"
        Me.chkEnable.Size = New System.Drawing.Size(59, 17)
        Me.chkEnable.TabIndex = 2
        Me.chkEnable.Text = "Enable"
        Me.chkEnable.UseVisualStyleBackColor = True
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.GroupBox1)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(915, 81)
        Me.pnlTop.TabIndex = 0
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(294, 3)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 37)
        Me.btnPreview.TabIndex = 53
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(387, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 37)
        Me.btnCancel.TabIndex = 54
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(107, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 37)
        Me.btnSave.TabIndex = 51
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.btnEdit)
        Me.pnlBottom.Controls.Add(Me.btnCancel)
        Me.pnlBottom.Controls.Add(Me.btnPreview)
        Me.pnlBottom.Controls.Add(Me.btnSave)
        Me.pnlBottom.Controls.Add(Me.btnSaveAsDraft)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 547)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(915, 42)
        Me.pnlBottom.TabIndex = 2
        '
        'btnEdit
        '
        Me.btnEdit.Enabled = False
        Me.btnEdit.Location = New System.Drawing.Point(14, 2)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(87, 37)
        Me.btnEdit.TabIndex = 50
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnSaveAsDraft
        '
        Me.btnSaveAsDraft.Location = New System.Drawing.Point(201, 3)
        Me.btnSaveAsDraft.Name = "btnSaveAsDraft"
        Me.btnSaveAsDraft.Size = New System.Drawing.Size(87, 37)
        Me.btnSaveAsDraft.TabIndex = 52
        Me.btnSaveAsDraft.Text = "Save As &Draft"
        Me.btnSaveAsDraft.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Excel Files(*.xls)|*.xls|Excel 2007 Files(*.xlsx)|*.xlsx"
        Me.OpenFileDialog1.RestoreDirectory = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'dgvLookupbtnDelete
        '
        Me.dgvLookupbtnDelete.HeaderText = "X"
        Me.dgvLookupbtnDelete.Name = "dgvLookupbtnDelete"
        Me.dgvLookupbtnDelete.Text = "X"
        Me.dgvLookupbtnDelete.UseColumnTextForButtonValue = True
        Me.dgvLookupbtnDelete.Width = 20
        '
        'dgvLookupBankFieldNameCombo
        '
        Me.dgvLookupBankFieldNameCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupBankFieldNameCombo.DataPropertyName = "BankFieldName"
        Me.dgvLookupBankFieldNameCombo.DataSource = Me.bsLookupBankField
        Me.dgvLookupBankFieldNameCombo.DisplayMember = "BankField"
        Me.dgvLookupBankFieldNameCombo.HeaderText = "Bank Field"
        Me.dgvLookupBankFieldNameCombo.Name = "dgvLookupBankFieldNameCombo"
        Me.dgvLookupBankFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupBankFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupSourceFieldNameCombo
        '
        Me.dgvLookupSourceFieldNameCombo.DataPropertyName = "SourceFieldName"
        Me.dgvLookupSourceFieldNameCombo.DataSource = Me.bsLookupSourceField
        Me.dgvLookupSourceFieldNameCombo.DisplayMember = "SourceFieldName"
        Me.dgvLookupSourceFieldNameCombo.HeaderText = "Source Field"
        Me.dgvLookupSourceFieldNameCombo.Name = "dgvLookupSourceFieldNameCombo"
        Me.dgvLookupSourceFieldNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupSourceFieldNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvLookupSourceFieldNameCombo.ValueMember = "SourceFieldName"
        Me.dgvLookupSourceFieldNameCombo.Width = 194
        '
        'dgvLookupTableNameCombo
        '
        Me.dgvLookupTableNameCombo.DataPropertyName = "TableName"
        Me.dgvLookupTableNameCombo.HeaderText = "Table"
        Me.dgvLookupTableNameCombo.Name = "dgvLookupTableNameCombo"
        Me.dgvLookupTableNameCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupTableNameCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.dgvLookupTableNameCombo.Width = 90
        '
        'dgvLookupLookupKeyCombo
        '
        Me.dgvLookupLookupKeyCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupKeyCombo.DataPropertyName = "LookupKey"
        Me.dgvLookupLookupKeyCombo.HeaderText = "Lookup Key"
        Me.dgvLookupLookupKeyCombo.Name = "dgvLookupLookupKeyCombo"
        Me.dgvLookupLookupKeyCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupLookupKeyCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupLookupValueCombo
        '
        Me.dgvLookupLookupValueCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupValueCombo.DataPropertyName = "LookupValue"
        Me.dgvLookupLookupValueCombo.HeaderText = "Lookup Value"
        Me.dgvLookupLookupValueCombo.Name = "dgvLookupLookupValueCombo"
        Me.dgvLookupLookupValueCombo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLookupLookupValueCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupTableNameCombo2
        '
        Me.dgvLookupTableNameCombo2.DataPropertyName = "TableName2"
        Me.dgvLookupTableNameCombo2.HeaderText = "Table2"
        Me.dgvLookupTableNameCombo2.Name = "dgvLookupTableNameCombo2"
        Me.dgvLookupTableNameCombo2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupLookupKeyCombo2
        '
        Me.dgvLookupLookupKeyCombo2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupKeyCombo2.DataPropertyName = "LookupKey2"
        Me.dgvLookupLookupKeyCombo2.HeaderText = "Lookup Key2"
        Me.dgvLookupLookupKeyCombo2.Name = "dgvLookupLookupKeyCombo2"
        Me.dgvLookupLookupKeyCombo2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupLookupValueCombo2
        '
        Me.dgvLookupLookupValueCombo2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupValueCombo2.DataPropertyName = "LookupValue2"
        Me.dgvLookupLookupValueCombo2.HeaderText = "Lookup Value2"
        Me.dgvLookupLookupValueCombo2.Name = "dgvLookupLookupValueCombo2"
        Me.dgvLookupLookupValueCombo2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupTableNameCombo3
        '
        Me.dgvLookupTableNameCombo3.DataPropertyName = "TableName3"
        Me.dgvLookupTableNameCombo3.HeaderText = "Table3"
        Me.dgvLookupTableNameCombo3.Name = "dgvLookupTableNameCombo3"
        Me.dgvLookupTableNameCombo3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupLookupKeyCombo3
        '
        Me.dgvLookupLookupKeyCombo3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupKeyCombo3.DataPropertyName = "LookupKey3"
        Me.dgvLookupLookupKeyCombo3.HeaderText = "Lookup Key3"
        Me.dgvLookupLookupKeyCombo3.Name = "dgvLookupLookupKeyCombo3"
        Me.dgvLookupLookupKeyCombo3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgvLookupLookupValueCombo3
        '
        Me.dgvLookupLookupValueCombo3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvLookupLookupValueCombo3.DataPropertyName = "LookupValue3"
        Me.dgvLookupLookupValueCombo3.HeaderText = "Lookup Value3"
        Me.dgvLookupLookupValueCombo3.Name = "dgvLookupLookupValueCombo3"
        Me.dgvLookupLookupValueCombo3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ucCommonTemplateExcelDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.pnlFill)
        Me.Controls.Add(Me.pnlTop)
        Me.Controls.Add(Me.pnlBottom)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucCommonTemplateExcelDetail"
        Me.Size = New System.Drawing.Size(915, 589)
        Me.TabPage3.ResumeLayout(False)
        Me.pnlMapBottom.ResumeLayout(False)
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsMapBankCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsDetailCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsMapSourceCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSampleData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.FieldSettingGroupBox.ResumeLayout(False)
        Me.FieldSettingGroupBox.PerformLayout()
        Me.ExcelSettingGroupBox.ResumeLayout(False)
        Me.ExcelSettingGroupBox.PerformLayout()
        CType(Me.bsWorksheetCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEditable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsEditableBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsEditableCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TranslationGroupBox.ResumeLayout(False)
        CType(Me.dgvTranslator, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTranslateBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsTranslatorCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.EditableGroupBox.ResumeLayout(False)
        Me.StringManipulationGroupBox.ResumeLayout(False)
        CType(Me.dgvStrManip, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsStrManipBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsStrManipCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        CType(Me.bsOutputTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFill.ResumeLayout(False)
        Me.tabDetail.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.pnlSetupBottom.ResumeLayout(False)
        Me.pnlSetupBottom.PerformLayout()
        Me.RecordTypeIndicatorGroupBox.ResumeLayout(False)
        Me.RecordTypeIndicatorGroupBox.PerformLayout()
        Me.grpDetailFilterSettings.ResumeLayout(False)
        Me.grpDetailFilterSettings.PerformLayout()
        CType(Me.dgvFilter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsFilterFieldCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRowFilterCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.grpDetailAdviceSettings.ResumeLayout(False)
        Me.grpDetailAdviceSettings.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.LookupGroupBox.ResumeLayout(False)
        CType(Me.dgvLookup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsLookupBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsLookupSourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsLookupCollection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.CalculationGroupBox.ResumeLayout(False)
        CType(Me.dgvCalc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCalcBankField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCalOperand1SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCalcOperand2SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCalcOperand3SourceField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsCalcCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRefField3Collection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRefField2Collection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRefField1Collection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlTop.ResumeLayout(False)
        Me.pnlBottom.ResumeLayout(False)
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents pnlMapBottom As System.Windows.Forms.Panel
    Friend WithEvents btnClearAll As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents dgvBank As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSource As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSampleData As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvEditable As System.Windows.Forms.DataGridView
    Friend WithEvents btnUpTranslator As System.Windows.Forms.Button
    Friend WithEvents TranslationGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnDownTranslator As System.Windows.Forms.Button
    Friend WithEvents dgvTranslator As System.Windows.Forms.DataGridView
    Friend WithEvents btnUpEditable As System.Windows.Forms.Button
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents EditableGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnDownEditable As System.Windows.Forms.Button
    Friend WithEvents btnDownStrManip As System.Windows.Forms.Button
    Friend WithEvents StringManipulationGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnUpStrManip As System.Windows.Forms.Button
    Friend WithEvents dgvStrManip As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents txtTemplateName As System.Windows.Forms.TextBox
    Friend WithEvents cboOutputTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pnlFill As System.Windows.Forms.Panel
    Friend WithEvents tabDetail As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents pnlSetup As System.Windows.Forms.Panel
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents LookupGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents dgvLookup As System.Windows.Forms.DataGridView
    Friend WithEvents btnLookupDown As System.Windows.Forms.Button
    Friend WithEvents btnLookupUp As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents CalculationGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnDownCalc As System.Windows.Forms.Button
    Friend WithEvents btnUpCalc As System.Windows.Forms.Button
    Friend WithEvents dgvCalc As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents btnSaveAsDraft As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents FieldSettingGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents chkHeaderAsFieldName As System.Windows.Forms.CheckBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ExcelSettingGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents txtEndCol As System.Windows.Forms.TextBox
    Friend WithEvents txtStartCol As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboWorksheet As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents pnlSetupBottom As System.Windows.Forms.Panel
    Friend WithEvents grpDetailFilterSettings As System.Windows.Forms.GroupBox
    Friend WithEvents btnFilterUp As System.Windows.Forms.Button
    Friend WithEvents btnFilterDown As System.Windows.Forms.Button
    Friend WithEvents rdoFilterOR As System.Windows.Forms.RadioButton
    Friend WithEvents rdoFilterAND As System.Windows.Forms.RadioButton
    Friend WithEvents dgvFilter As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents rdoCents As System.Windows.Forms.RadioButton
    Friend WithEvents rdoDollars As System.Windows.Forms.RadioButton
    Friend WithEvents grpDetailAdviceSettings As System.Windows.Forms.GroupBox
    Friend WithEvents txtAdviceDelimiter As System.Windows.Forms.TextBox
    Friend WithEvents txtAdviceChar As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents rdoAdviceDelimited As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAdviceAftEveryChar As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAdviceForEveryRow As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents cboRef3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboRef2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboRef1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkZerosInDate As System.Windows.Forms.CheckBox
    Friend WithEvents cboDateType As System.Windows.Forms.ComboBox
    Friend WithEvents cboDateSep As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents cboThousandSep As System.Windows.Forms.ComboBox
    Friend WithEvents cboDecimalSep As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnRetrieve As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bsDetailCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsRowFilterCollection As System.Windows.Forms.BindingSource
    Friend WithEvents chkEnable As System.Windows.Forms.CheckBox
    Friend WithEvents bsMapSourceCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsMapBankCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsTranslatorCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsEditableCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsLookupCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsCalcCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsStrManipCollection As System.Windows.Forms.BindingSource
    Friend WithEvents dgvBankMappingFieldNamesText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvBankBankValueText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtSource As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtHeaderAtRow As BTMU.MAGIC.UI.NumberBox
    Friend WithEvents txtTransactionStartRow As BTMU.MAGIC.UI.NumberBox
    Friend WithEvents txtSampleRowNo As BTMU.MAGIC.UI.NumberBox
    Friend WithEvents bsRefField1Collection As System.Windows.Forms.BindingSource
    Friend WithEvents bsRefField2Collection As System.Windows.Forms.BindingSource
    Friend WithEvents bsRefField3Collection As System.Windows.Forms.BindingSource
    Friend WithEvents bsWorksheetCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsFilterFieldCollection As System.Windows.Forms.BindingSource
    Friend WithEvents bsTranslateBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bsEditableBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bsLookupBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bsLookupSourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bsCalcBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bsCalOperand1SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bsCalcOperand2SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bsCalcOperand3SourceField As System.Windows.Forms.BindingSource
    Friend WithEvents bsStrManipBankField As System.Windows.Forms.BindingSource
    Friend WithEvents bsOutputTemplate As System.Windows.Forms.BindingSource
    Friend WithEvents Sequence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvSourceSourceFieldNameText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvSourceSourceFieldValueText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ErrorProvider1 As BTMU.Magic.Common.ErrorProviderFixed
    Friend WithEvents dgvTranslatorbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvTranslatorBankFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvTranslatorCustomerValueText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvTranslatorCellBankValueEditCbo As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents BankValueEmpty As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvCalcbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvCalcBankFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalcOperand1Combo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalOperator1Combo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalcOperand2Combo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalcOperator2Combo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvCalcOperand3Combo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvStrManipbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvStrManipBankFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvStrManipFunctionNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvStrManipStartPositionText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvStrManipNoOfCharactersText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvFilterbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvFilterFilterFieldCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvFilterCellFilterTypeComboBox As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvFilterFilterOperatorCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvFilterFilterValueText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RecordTypeIndicatorGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents WIndicatorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PIndicatorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PColumnTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents WColumnTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents IIndicatorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IColumnTextBox As System.Windows.Forms.TextBox
    Friend WithEvents dgvEditablebtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvEditableBankFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvBankBankFieldNameText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvBankMappedFieldsText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvBankMappedValuesText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvBankBankSampleValueText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MapSeparator As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CarriageReturn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvLookupbtnDelete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvLookupBankFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupSourceFieldNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupTableNameCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupLookupKeyCombo As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupLookupValueCombo As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupTableNameCombo2 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupLookupKeyCombo2 As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupLookupValueCombo2 As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupTableNameCombo3 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvLookupLookupKeyCombo3 As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn
    Friend WithEvents dgvLookupLookupValueCombo3 As BTMU.Magic.UI.DataGridViewEditableComboBoxColumn

End Class
