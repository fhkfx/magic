Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports System.Text.RegularExpressions
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat
Imports System.IO

''' <summary>
''' User Control for Common Transaction Template - Text 
''' </summary>
''' <remarks></remarks>
Public Class ucCommonTemplateTextDetail

#Region " Members "

    Private dtEditableList As DataTable
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    Private _dataList As New BTMU.Magic.CommonTemplate.CommonTemplateTextDetailCollection()
    Private _dataModel As Magic.CommonTemplate.CommonTemplateTextDetail
    Private _srcStructFilename As String
    Private dtPaymentBankFields As DataTable

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
                Handles Me.Load


        bindingSrcOutputTemplate.DataSource = CommonTemplateModule.AllMasterTemplates
        cboDateSep.DataSource = CommonTemplateModule.DateSeparator
        cboDateType.DataSource = CommonTemplateModule.DateType
        cboDecimalSep.DataSource = CommonTemplateModule.DecimalSeparator
        cboThousandSep.DataSource = CommonTemplateModule.ThousandSeparator
        cboEnclosureChar.DataSource = CommonTemplateModule.EnclosureCharacters
        cboDelimiter.DataSource = CommonTemplateModule.DelimiterCharacters
        dgvCmbFilterOperator.DataSource = CommonTemplateModule.FilterOperator
        dgvLookupCellTable.DataSource = CommonTemplateModule.LookupTables
        dgvLookupCellTable2.DataSource = CommonTemplateModule.LookupTables
        dgvLookupCellTable3.DataSource = CommonTemplateModule.LookupTables
        dgvCalculatedFieldsCellOperator1.DataSource = CommonTemplateModule.Operators1
        dgvCalculatedFieldsCellOperator2.DataSource = CommonTemplateModule.Operators2
        dgvStringManipulationCellStringFunction.DataSource = CommonTemplateModule.StringFunctions
        dgvCmbFilterDataType.DataSource = CommonTemplateModule.FilterDataType
        bindingSrcCommonTemplateTextDetailCollection.DataSource = _dataList

        dgvFilterSettings.EditMode = DataGridViewEditMode.EditOnEnter
        dgvTranslator.EditMode = DataGridViewEditMode.EditOnEnter
        dgvEditable.EditMode = DataGridViewEditMode.EditOnEnter

        dgvLookup.EditMode = DataGridViewEditMode.EditOnEnter
        dgvCalculated.EditMode = DataGridViewEditMode.EditOnEnter
        dgvStringManipulation.EditMode = DataGridViewEditMode.EditOnEnter
    End Sub

    ''' <summary>
    ''' Prepares and Initializes the GUI for Adding data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AddNew()

        _dataList.Clear()
        _dataModel = New BTMU.Magic.CommonTemplate.CommonTemplateTextDetail
        _dataList.Add(_dataModel)
        populateReferenceFields()
        _isNew = True

        'Init Databound/Unbound Controls
        FillEditableTable()
        InitializeNewTemplate()
        txtSampleRowData.ReadOnly = Not (chkFixedWidth.Checked)
        tabDetail.SelectedIndex = 0

        EnableDisableForm(True)

    End Sub

    Private Sub InitializeNewTemplate()

        _dataModel.IsEnabled = True
        _dataModel.IsCarriageReturn = True
        _dataModel.IsAmountInDollars = True
        _dataModel.IsAdviceRecordForEveryRow = True
        _dataModel.IsFilterConditionAND = True
        txtSampleRowData.ReadOnly = True
        cboOutputTemplate.Enabled = True
        txtCommonTemplateName.Enabled = True
        btnModify.Enabled = False
        txtOtherDelimiter.Enabled = False

        rdoCarriageReturn.Checked = _dataModel.IsCarriageReturn
        rdoDelimited.Checked = _dataModel.IsDelimited
        rdoCents.Checked = _dataModel.IsAmountInCents
        rdoDollars.Checked = _dataModel.IsAmountInDollars
        rdoFilterSettingAND.Checked = _dataModel.IsFilterConditionAND
        rdoFilterSettingOR.Checked = _dataModel.IsFilterConditionOR
        rdoIRTMSEveryRow.Checked = _dataModel.IsAdviceRecordForEveryRow
        rdoIRTMSCharPos.Checked = _dataModel.IsAdviceRecordAfterCharacter
        rdoIRTMSDelimiter.Checked = _dataModel.IsAdviceRecordDelimited
        chkFixedWidth.Checked = _dataModel.IsFixedWidthFile

        RecordTypeIndicatorGroupBox.Enabled = True
        chkHeaderAsField.Enabled = True
        chkFixedWidth.Enabled = True

        rdoDelimited_CheckedChanged(Nothing, Nothing)
        rdoCarriageReturn_CheckedChanged(Nothing, Nothing)
        IRTMSFormatChanged()

        'No selected item in drop down lists
        cboOutputTemplate.SelectedIndex = -1
        cboDateSep.SelectedIndex = -1
        cboDateType.SelectedIndex = -1
        cboDecimalSep.SelectedIndex = -1
        cboThousandSep.SelectedIndex = -1
        cboEnclosureChar.SelectedIndex = -1
        cboDelimiter.SelectedIndex = -1

        

        setDataSourceToBindingSource()
        dtEditableList.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Duplicates the given template and edits the duplicated template for modification
    ''' </summary>
    ''' <param name="CommonTemplateFile">Common Template</param>
    ''' <param name="IsDraft">Boolean value indicating whether the Template is a Draft</param>
    ''' <remarks></remarks>
    Public Sub DuplicateExisting(ByVal CommonTemplateFile As String, Optional ByVal IsDraft As Boolean = False)

        EditExisting(CommonTemplateFile)
        txtCommonTemplateName.Enabled = True
        If _dataModel Is Nothing Then
            Exit Sub
        End If

        _dataModel.CommonTemplateName = _dataModel.CommonTemplateName & " Copy"
        _dataModel.IsDraft = IsDraft
        IsNew = True

        txtCommonTemplateName.Enabled = True

    End Sub

    ''' <summary>
    ''' Prepares the GUI and Edits the Template for modification
    ''' </summary>
    ''' <param name="commonTemplateFile">Common Template</param>
    ''' <remarks></remarks>
    Public Sub EditExisting(ByVal commonTemplateFile As String)

        If commonTemplateFile = String.Empty Then
            ContainerForm.ShowView()
            Exit Sub
        End If

        Try
            EnableDisableForm(True)

            _dataList.Clear()

            _dataModel = CommonTemplateTextDetail.LoadFromFile2(commonTemplateFile)
            _dataList.Add(_dataModel)
            populateReferenceFields()
            _dataModel.Validate()

            If _dataModel.IsCarriageReturn Then

                RemoveHandler rdoCarriageReturn.CheckedChanged, AddressOf rdoCarriageReturn_CheckedChanged
                rdoCarriageReturn.Checked = True
                AddHandler rdoCarriageReturn.CheckedChanged, AddressOf rdoCarriageReturn_CheckedChanged

                txtSampleRow.Enabled = True
                rdoDelimited.Enabled = False
                txtStartingChar.Enabled = False
                txtLenOfChar.Enabled = False
                txtHeaderRowNo.Enabled = True
                txtTransactionStartRow.Enabled = True

            Else

                RemoveHandler rdoCarriageReturn.CheckedChanged, AddressOf rdoCarriageReturn_CheckedChanged
                rdoCarriageReturn.Checked = False
                AddHandler rdoCarriageReturn.CheckedChanged, AddressOf rdoCarriageReturn_CheckedChanged

                txtSampleRow.Enabled = False
                rdoDelimited.Enabled = True
                txtStartingChar.Enabled = True
                txtLenOfChar.Enabled = True
                txtHeaderRowNo.Enabled = False
                txtTransactionStartRow.Enabled = False

                RemoveHandler rdoDelimited.CheckedChanged, AddressOf rdoDelimited_CheckedChanged
                rdoDelimited.Checked = True
                AddHandler rdoDelimited.CheckedChanged, AddressOf rdoDelimited_CheckedChanged

            End If


            RemoveHandler chkFixedWidth.CheckedChanged, AddressOf chkFixedWidth_CheckedChanged
            chkFixedWidth.Checked = _dataModel.IsFixedWidthFile
            txtSampleRowData.ReadOnly = Not (chkFixedWidth.Checked)
            cboEnclosureChar.Enabled = Not (chkFixedWidth.Checked)
            rdoDelimited.Enabled = chkFixedWidth.Checked
            If chkFixedWidth.Checked Then
                cboDelimiter.DataSource = Nothing
                cboDelimiter.Items.Clear()
                cboDelimiter.Items.Add(";")
                cboDelimiter.SelectedIndex = 0
                _dataModel.OtherSeparator = ""
                txtOtherDelimiter.Enabled = False
            Else
                cboDelimiter.DataSource = CommonTemplateModule.DelimiterCharacters
                cboDelimiter.SelectedIndex = -1
            End If
            AddHandler chkFixedWidth.CheckedChanged, AddressOf chkFixedWidth_CheckedChanged


            ChkZerosInDate.Checked = _dataModel.IsZeroInDate
            ChkZerosInDate.Enabled = IIf(_dataModel.DateSeparator = "No Space", False, True)
            cboDateSep.Text = _dataModel.DateSeparator
            cboDateType.Text = _dataModel.DateType

            cboDecimalSep.Text = _dataModel.DecimalSeparator
            cboThousandSep.Text = _dataModel.ThousandSeparator

            RemoveHandler cboDelimiter.SelectedIndexChanged, AddressOf cboDelimiter_SelectedIndexChanged
            cboDelimiter.Text = _dataModel.DelimiterCharacter
            AddHandler cboDelimiter.SelectedIndexChanged, AddressOf cboDelimiter_SelectedIndexChanged

            txtOtherDelimiter.Text = _dataModel.OtherSeparator
            txtOtherDelimiter.Enabled = IIf(_dataModel.DelimiterCharacter = "Other", True, False)

            If _dataModel.IsAmountInCents Then
                rdoCents.Checked = True
                rdoDollars.Checked = False
            Else
                rdoCents.Checked = False
                rdoDollars.Checked = True
            End If
            If _dataModel.IsFilterConditionAND Then
                rdoFilterSettingAND.Checked = True
                rdoFilterSettingOR.Checked = False
            Else
                rdoFilterSettingAND.Checked = False
                rdoFilterSettingOR.Checked = True
            End If

            RemoveHandler rdoIRTMSEveryRow.CheckedChanged, AddressOf rdoIRTMSEveryRow_CheckedChanged
            RemoveHandler rdoIRTMSCharPos.CheckedChanged, AddressOf rdoIRTMSCharPos_CheckedChanged
            RemoveHandler rdoIRTMSDelimiter.CheckedChanged, AddressOf rdoIRTMSDelimiter_CheckedChanged

            If _dataModel.IsAdviceRecordAfterCharacter Then
                rdoIRTMSEveryRow.Checked = False
                rdoIRTMSCharPos.Checked = True
                rdoIRTMSDelimiter.Checked = False
                txtAdviseRecordDelimiter.Enabled = False
                txtNoOfCharsPerAdviseRecord.Enabled = True
            ElseIf _dataModel.IsAdviceRecordDelimited Then
                rdoIRTMSEveryRow.Checked = False
                rdoIRTMSCharPos.Checked = False
                rdoIRTMSDelimiter.Checked = True
                txtAdviseRecordDelimiter.Enabled = True
                txtNoOfCharsPerAdviseRecord.Enabled = False
            Else 'defaulted to Advice Record For Every Row of Data
                rdoIRTMSEveryRow.Checked = True
                rdoIRTMSCharPos.Checked = False
                rdoIRTMSDelimiter.Checked = False
                txtAdviseRecordDelimiter.Enabled = False
                txtNoOfCharsPerAdviseRecord.Enabled = False
            End If

            AddHandler rdoIRTMSEveryRow.CheckedChanged, AddressOf rdoIRTMSEveryRow_CheckedChanged
            AddHandler rdoIRTMSCharPos.CheckedChanged, AddressOf rdoIRTMSCharPos_CheckedChanged
            AddHandler rdoIRTMSDelimiter.CheckedChanged, AddressOf rdoIRTMSDelimiter_CheckedChanged



        Catch ex As System.Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error Loading " & commonTemplateFile _
                                , ex.Message)
            ContainerForm.ShowView()
        End Try

        txtSampleRowData.ReadOnly = Not (chkFixedWidth.Checked)

        tabDetail.SelectedIndex = 0

        txtCommonTemplateName.Enabled = False
        cboOutputTemplate.Enabled = False

        If Not _dataModel Is Nothing Then _dataModel.SetDirty(True) ' Added the If condition to prevent unhandled exception error SRS/BTMU/2010/0050

        FillEditableTable()
        EnableDisableRecordIndicator()
        EnableDisableAdviceRecordSetting()
        setDataSourceToBindingSource()

        cboDuplicateTxnRefField1.DataBindings("Text").ReadValue()
        cboDuplicateTxnRefField2.DataBindings("Text").ReadValue()
        cboDuplicateTxnRefField3.DataBindings("Text").ReadValue()

    End Sub

    ''' <summary>
    ''' Prepares the GUI to display the Template data in read-only mode by disabling the controls 
    ''' </summary>
    ''' <param name="commonTemplateFile">Common Template file</param>
    ''' <remarks></remarks>
    Public Sub ViewExisting(ByVal commonTemplateFile As String)

        EditExisting(commonTemplateFile)

        _dataModel.ApplyEdit()

        EnableDisableForm(False)

    End Sub
    ''' <summary>
    ''' Enables/Disables Form Fields
    ''' </summary>
    ''' <param name="_ctrStatus">Boolean value indicating whether the form shall be disabled/enabled</param>
    ''' <remarks></remarks>
    Sub EnableDisableForm(ByVal _ctrStatus As Boolean)

        txtCommonTemplateName.Enabled = _ctrStatus
        cboOutputTemplate.Enabled = _ctrStatus
        chkEnable.Enabled = _ctrStatus

        txtSourceFileName.Enabled = _ctrStatus
        btnBrowseSourceFile.Enabled = _ctrStatus
        chkFixedWidth.Enabled = _ctrStatus
        gbxSampleRowData.Enabled = _ctrStatus
        gbxDateSettings.Enabled = _ctrStatus
        gbxNumberSettings.Enabled = _ctrStatus
        gbxRemittanceAmtSettings.Enabled = _ctrStatus
        gbxSeparatorSettings.Enabled = _ctrStatus
        gbxFieldSettings.Enabled = _ctrStatus
        gbxDuplicateTxnRef.Enabled = _ctrStatus
        gbxDetailAdviceSettings.Enabled = _ctrStatus
        gbxFilterSetting.Enabled = _ctrStatus

        dgvBank.Enabled = _ctrStatus
        dgvSource.Enabled = _ctrStatus
        btnClear.Enabled = _ctrStatus
        btnClearAll.Enabled = _ctrStatus

        gbxTranslatorSetting.Enabled = _ctrStatus
        gbxEditableSetting.Enabled = _ctrStatus

        gbxLookup.Enabled = _ctrStatus

        gbxCalculatedValues.Enabled = _ctrStatus

        gbxStringManipulation.Enabled = _ctrStatus

        btnModify.Enabled = Not (_ctrStatus)
        btnSave.Enabled = _ctrStatus
        btnSaveAsDraft.Enabled = _ctrStatus

    End Sub

#Region "Event Handlers and Initializers"

    Private Sub chkFixedWidth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFixedWidth.CheckedChanged

        ' If FixedWidth Is Checked 
        '       "Separate" textbox will be enabled.
        '       "Delimiter" radiobutton will be enabled.
        '       "Carriage Return" radiobutton will be enabled and selected.
        '       "Enclosure Character" will be disabled.
        '       "Delimiter" combobox has only semicolon(;) value.
        ' Else
        '       "Separate" textbox will be disabled.
        '       "Delimiter" radiobutton will be disabled.
        '       "Carriage Return" radiobutton will be enabled and selected.
        '       "Enclosure Character" will be enabled.
        '       "Delimiter" combobox has all the allowed values.

        If Not _dataModel Is Nothing Then

            txtSampleRowData.ReadOnly = Not (chkFixedWidth.Checked)
            rdoDelimited.Enabled = chkFixedWidth.Checked
            rdoCarriageReturn.Enabled = True
            rdoCarriageReturn.Checked = True
            cboEnclosureChar.Enabled = Not (chkFixedWidth.Checked)
            If chkFixedWidth.Checked Then
                cboDelimiter.DataSource = Nothing
                cboDelimiter.Items.Clear()
                cboDelimiter.Items.Add(";")
                cboDelimiter.SelectedIndex = 0
                _dataModel.OtherSeparator = ""
                txtOtherDelimiter.Enabled = False
            Else
                cboDelimiter.DataSource = CommonTemplateModule.DelimiterCharacters
                cboDelimiter.SelectedIndex = -1
            End If
            _dataModel.SampleRowNumber = ""
            _dataModel.HeaderRowNumber = ""
            _dataModel.TransactionStartRowNumber = ""
            _dataModel.StartingCharacterPos = ""
            _dataModel.LenOfStartingCharacter = ""
            txtStartingChar.Enabled = rdoDelimited.Checked
            txtLenOfChar.Enabled = rdoDelimited.Checked

        End If

    End Sub

    Private Sub rdoCarriageReturn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoCarriageReturn.CheckedChanged

        'If selected Then
        '   	"Carriage Return" panel will be enabled.
        '   	"Header at Row" textbox will be enabled.
        '   	"Transaction Starting Row" textbox will be enabled.
        '   	"Delimiter" panel will be disabled.
        ' else
        '   	"Carriage Return" panel will be disabled.
        '   	"Header at Row" text box will be disabled.
        '   	"Transaction Starting Row" text box will be disabled.
        '   	"Delimiter" panel will be enabled.

        If Not _dataModel Is Nothing Then
            txtSampleRow.Enabled = rdoCarriageReturn.Checked
            txtHeaderRowNo.Enabled = rdoCarriageReturn.Checked
            txtTransactionStartRow.Enabled = rdoCarriageReturn.Checked
            _dataModel.SampleRowNumber = ""
            _dataModel.HeaderRowNumber = ""
            _dataModel.TransactionStartRowNumber = ""
        End If



    End Sub

    Private Sub rdoDelimited_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoDelimited.CheckedChanged

        'If selected Then
        '   	"Delimiter" panel will be enabled.
        '   	"Header at Row" text box will be disabled.
        '   	"Transaction Starting Row" text box will be disabled.
        '   	"Carriage Return" panel will be disabled.
        'Else
        '   	"Delimiter" panel will be disabled.
        '   	"Header at Row" textbox will be enabled.
        '   	"Transaction Starting Row" textbox will be enabled.
        '   	"Carriage Return" panel will be enabled.

        If Not _dataModel Is Nothing Then
            _dataModel.StartingCharacterPos = ""
            _dataModel.LenOfStartingCharacter = ""
            txtStartingChar.Enabled = rdoDelimited.Checked
            txtLenOfChar.Enabled = rdoDelimited.Checked
        End If

    End Sub

    Private Sub cboDelimiter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDelimiter.SelectedIndexChanged

        If Not _dataModel Is Nothing Then

            _dataModel.OtherSeparator = ""
            txtOtherDelimiter.Enabled = IIf(cboDelimiter.Text = "Other", True, False)

        End If

    End Sub

    Private Sub cboDateSep_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDateSep.SelectedIndexChanged

        If Not _dataModel Is Nothing Then

            If cboDateSep.Text = "No Space" Then
                _dataModel.IsZeroInDate = True
                ChkZerosInDate.Enabled = False
            Else
                ChkZerosInDate.Enabled = True
            End If

        End If

    End Sub

    Private Sub btnBrowseSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSourceFile.Click

        Dim filename As String = ""
        Dim SrcFileDialog As New OpenFileDialog()
        SrcFileDialog.Filter = "Text Documents (*.txt)|*.txt|CSV Files (*.csv)|*.csv|All Files (*.*)|*.*"
        SrcFileDialog.Multiselect = False
        SrcFileDialog.InitialDirectory = My.Settings.SampleSourceFolder
        SrcFileDialog.CheckFileExists = True
        SrcFileDialog.CheckPathExists = True
        If SrcFileDialog.ShowDialog(Me) = DialogResult.OK Then
            filename = SrcFileDialog.FileName
            'Else
            '    filename = ""
        End If

        SrcFileDialog.Dispose()

        If Not ValidateNewSourceFile(filename) Then Exit Sub

        If Not _dataModel Is Nothing Then
            _dataModel.SourceFileName = filename
        End If

    End Sub

    Private Function ValidateMandatoryFieldsForNewSourceFile() As Boolean

        If Not (ValidateOutputTemplate()) Then Return False

        If rdoDelimited.Checked Then
            If Not (ValidateSampleStartingCharacter() AndAlso ValidateLengthOfCharacterTransaction()) Then Return False
        Else
            If Not (ValidateSampleRow() AndAlso ValidateHeaderRow() AndAlso ValidateTransactionStartRow()) Then Return False
        End If

        If Not chkFixedWidth.Checked Then
            If Not ValidateEnclosureCharacter() Then Return False
        End If

        If Not (ValidateDelimiterCharacter() AndAlso ValidateNumberSetting() AndAlso ValidateDateSetting()) Then Return False


        If Not (ValidateTxnReferenceFields() AndAlso ValidateCharsPerAdviseRecord() AndAlso ValidateAdviseRecordDelimiter()) Then Return False

        Return True

    End Function

    Private Function ValidateNewSourceFile(ByVal newSrcFilename As String) As Boolean

        If newSrcFilename = "" Then Return False

        'If there are Map Source Fields, Check if they are equal to those found in the new file
        If _dataModel.MapSourceFields.Count = 0 Then Return True


        'If the Source Fields - Map Bank Fields Mapping Exists - Update the Source Fields

        Try
            If _dataModel.MapSourceFields.Count > 0 Then

                If IsFileFormatUsingRecordTypes() Then
                    'Validate the sample row, record type indicators, enclosure and delimited characters before
                    If Not (ValidateSampleRow() AndAlso ValidateRecordTypes() AndAlso ValidateEnclosureCharacter() AndAlso ValidateDelimiterCharacter()) Then Return False
                    Return UpdateMapSourceFieldsWithRecordTypes(newSrcFilename)
                Else
                    Return UpdateMapSourceFields(newSrcFilename)
                End If
            End If

        Catch ex As Exception
            BTMUExceptionManager.LogAndShowError("Error in Source File:", ex.Message)
            Return False
        End Try

        Return False


    End Function

#Region " Source Filename Drag and Drop "

    Private Sub txtSourceFileName_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSourceFileName.DragEnter

        If IsInSupportedFormat(e) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If


    End Sub

    Private Sub txtSourceFileName_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtSourceFileName.DragDrop

        Dim filename As String = ""
        Dim Filenames As String()
        txtSourceFileName.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)

        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase) Or _
                filename.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase)) Then Exit Sub

        If Not ValidateNewSourceFile(filename) Then Exit Sub

        If Not _dataModel Is Nothing Then
            _dataModel.SourceFileName = filename
        End If

    End Sub

    Private Function IsInSupportedFormat(ByVal e As System.Windows.Forms.DragEventArgs) As Boolean
        Dim fmt As String
        Dim supported As Boolean = False

        For Each fmt In e.Data.GetFormats()
            If fmt = System.Windows.Forms.DataFormats.FileDrop Then
                supported = True
                Exit For
            End If
        Next
        Return supported
    End Function

#End Region

    Private Sub rdoIRTMSEveryRow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSEveryRow.CheckedChanged
        If rdoIRTMSEveryRow.Checked Then
            IRTMSFormatChanged()
        End If

    End Sub

    Private Sub rdoIRTMSCharPos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSCharPos.CheckedChanged

        If rdoIRTMSCharPos.Checked Then
            IRTMSFormatChanged()
        End If
    End Sub

    Private Sub rdoIRTMSDelimiter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoIRTMSDelimiter.CheckedChanged

        If rdoIRTMSDelimiter.Checked Then
            IRTMSFormatChanged()
        End If

    End Sub

    Private Sub IRTMSFormatChanged()

        If Not _dataModel Is Nothing Then

            If rdoIRTMSEveryRow.Checked Then

                _dataModel.AdviceRecordCharPos = ""
                _dataModel.AdviceRecordDelimiter = ""
                txtNoOfCharsPerAdviseRecord.Enabled = False
                txtAdviseRecordDelimiter.Enabled = False

            ElseIf rdoIRTMSCharPos.Checked Then

                txtNoOfCharsPerAdviseRecord.Enabled = True
                _dataModel.AdviceRecordDelimiter = ""
                txtAdviseRecordDelimiter.Enabled = False

            ElseIf rdoIRTMSDelimiter.Checked Then

                _dataModel.AdviceRecordDelimiter = ""
                txtAdviseRecordDelimiter.Enabled = True
                _dataModel.AdviceRecordCharPos = ""
                txtNoOfCharsPerAdviseRecord.Enabled = False

            End If

        End If

    End Sub

    Private Sub chkEnable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEnable.CheckedChanged

        If chkEnable.Checked Then

            Log_EnableCommonTemplate(frmLogin.GsUserName, _
                           System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                                       , txtCommonTemplateName.Text) _
                           & CommonTemplateModule.CommonTemplateFileExtension)

        Else

            Log_DisableCommonTemplate(frmLogin.GsUserName, _
                                     System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                                                 , txtCommonTemplateName.Text) _
                                     & CommonTemplateModule.CommonTemplateFileExtension)


        End If

    End Sub

    Private Sub txtSampleRowData_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSampleRowData.KeyPress

        If (e.KeyChar <> ";") AndAlso (e.KeyChar <> vbTab) AndAlso (e.KeyChar <> vbBack) Then
            e.Handled = True
        End If

    End Sub

    Private Sub btnRetrieve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieve.Click
        Dim lineDataFound As Boolean = False
        Dim lineText As String = ""
        Try


            If Not ValidateSourceFile() Then
                Exit Sub
            End If

            If IsFileFormatUsingRecordTypes() Then
                If Not (ValidateSampleRow() AndAlso ValidateFieldsForFormatsWithRecordTypes()) Then Exit Sub
                'lineText = PopulateSampleTransactionSet(file)
                lineText = _dataModel.PopulateSampleTransactionSet()
                lineDataFound = True
            Else
                Dim counter As Int32 = 1
                Dim filestrm As FileStream = Nothing
                Dim file As System.IO.StreamReader = Nothing
                Dim encoding As System.Text.Encoding = GetFileEncoding(txtSourceFileName.Text)
                filestrm = New System.IO.FileStream(txtSourceFileName.Text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                file = New System.IO.StreamReader(filestrm, encoding)
                Try
                    If rdoCarriageReturn.Checked Then
                        If Not ValidateSampleRow() Then
                            Exit Sub
                        End If
                        While Not file.EndOfStream
                            lineText = file.ReadLine()
                            If Convert.ToInt32(txtSampleRow.Number) = counter Then
                                lineDataFound = True
                                Exit While
                            End If
                            counter = counter + 1
                        End While
                    ElseIf rdoDelimited.Checked Then
                        If Not (ValidateSampleStartingCharacter() AndAlso ValidateLengthOfCharacterTransaction()) Then
                            Exit Sub
                        End If
                        lineDataFound = True
                        lineText = file.ReadLine()
                        lineText = lineText.Substring(Convert.ToInt32(txtStartingChar.Number) - 1 _
                                        , Convert.ToInt32(txtLenOfChar.Number))
                    End If
                Catch ex As Exception
                    BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error reading Source file.", ex.Message)
                Finally
                    If Not (file Is Nothing) Then file.Close()
                    If Not (filestrm Is Nothing) Then filestrm.Close()
                End Try
            End If

            If Not _dataModel Is Nothing Then
                If lineDataFound Then
                    _dataModel.SampleRow = lineText
                Else
                    _dataModel.SampleRow = ""
                End If
            End If
        Catch ex As MagicException
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(ex.Message.ToString, "")
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(ex.Message.ToString, "")
        End Try
    End Sub
    'Public Function PopulateSampleTransactionSet(ByVal file As StreamReader) As String
    '    Dim lineText As String
    '    Dim counter As Integer = 0
    '    Dim transactionText As New System.Text.StringBuilder
    '    Dim sDelimiter As String
    '    Dim found As Boolean = False
    '    Dim values As New List(Of String)
    '    Dim isPFound As Boolean = False
    '    Dim isWFound As Boolean = False
    '    Dim isIFound As Boolean = False

    '    ' Step 1 : Get the delimiter used to separate the fields
    '    Select Case cboDelimiter.Text
    '        Case "Other"
    '            sDelimiter = txtOtherDelimiter.Text
    '        Case "<Tab>"
    '            sDelimiter = vbTab
    '        Case "<Space>"
    '            sDelimiter = " "
    '        Case Else
    '            sDelimiter = cboDelimiter.Text
    '    End Select

    '    ' Step 4 : Get one set of transaction
    '    isPFound = False
    '    isWFound = False
    '    isIFound = False
    '    While Not file.EndOfStream
    '        lineText = file.ReadLine()
    '        found = IsDelimitedValueFound(lineText, sDelimiter)
    '        values = New List(Of String)
    '        If Not found Then
    '            Me.Cursor = Cursors.Default
    '            MessageBox.Show(MsgReader.GetString("E10000057"), "Generate Common Transaction Template" _
    '                            , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '            cboDelimiter.Focus()
    '            Return False
    '        End If
    '        If Convert.ToInt32(txtSampleRow.Number) - 1 = counter Then
    '            If _dataModel.CheckRecordTypeOfLine(lineText, PIndicatorTextBox.Text.Trim, sDelimiter, values) Then
    '                If Not isPFound Then
    '                    transactionText.AppendLine(lineText)
    '                    isPFound = True
    '                End If
    '            Else
    '                MessageBox.Show("Invalid Sample Row")
    '                Return ""
    '            End If
    '        ElseIf counter > Convert.ToInt32(txtSampleRow.Number) - 1 Then
    '            If Not isPFound Then
    '                MessageBox.Show("Invalid Sample Row")
    '            Else
    '                If Not isWFound Then
    '                    If _dataModel.CheckRecordTypeOfLine(lineText, WIndicatorTextBox.Text.Trim, sDelimiter, values) Then
    '                        transactionText.AppendLine(lineText)
    '                        isWFound = True
    '                    End If
    '                ElseIf Not isIFound Then
    '                    If _dataModel.CheckRecordTypeOfLine(lineText, IIndicatorTextBox.Text.Trim, sDelimiter, values) Then
    '                        transactionText.AppendLine(lineText)
    '                        isIFound = True
    '                    End If
    '                Else
    '                    If _dataModel.CheckRecordTypeOfLine(lineText, PIndicatorTextBox.Text.Trim, sDelimiter, values) Then
    '                        Exit While
    '                    End If
    '                End If
    '            End If
    '        End If
    '        counter = counter + 1
    '    End While

    '    Return transactionText.ToString
    'End Function

    'Private Function CheckRecordTypeOfLine(ByVal lineText As String, ByVal recordType As String, ByVal sDelimiter As String, ByRef values As List(Of String)) As Boolean
    '    Dim sEnclosureCharacter As String

    '    Dim columnIndex As Integer = 0
    '    Dim found As Boolean = False
    '    Dim indicator As String = ""

    '    If cboEnclosureChar.Text <> "None" Then 'Field Names and Values are enclosed
    '        '#1 - Check if there exists a value enclosed by given enclosure character
    '        sEnclosureCharacter = IIf(cboEnclosureChar.Text = "Single Quote(')", "'", """")
    '        found = IsEnclosedValueFound(lineText, sEnclosureCharacter)
    '        '#2 - Extract enclosed field values
    '        values.AddRange(HelperModule.Split(lineText, sDelimiter, sEnclosureCharacter, True))
    '    Else
    '        '#2 - Extract field values (could be enclosed in either ' or "") delimited by given delimiter 
    '        values.AddRange(HelperModule.Split(lineText, sDelimiter, Nothing, True))
    '    End If

    '    Select Case recordType
    '        Case PIndicatorTextBox.Text
    '            columnIndex = Convert.ToInt32(PColumnNumberBox.Number)
    '            indicator = PIndicatorTextBox.Text.Trim
    '        Case WIndicatorTextBox.Text
    '            columnIndex = Convert.ToInt32(WColumnNumberBox.Number)
    '            indicator = WIndicatorTextBox.Text.Trim
    '        Case IIndicatorTextBox.Text
    '            columnIndex = Convert.ToInt32(IColumnNumberBox.Number)
    '            indicator = IIndicatorTextBox.Text.Trim
    '    End Select

    '    If values.Count > columnIndex Then
    '        If values(columnIndex - 1) = indicator Then
    '            Return True
    '        Else
    '            Return False
    '        End If
    '    Else
    '        'Invalid Column Index
    '        Return False
    '    End If
    '    Return False
    'End Function

    Private Function GetCustomerValueWithRecordTypes(ByVal lineText As String, ByVal recordType As String, ByVal sDelimiter As String) As List(Of String)
        Dim sEnclosureCharacter As String
        Dim values As New List(Of String)
        Dim columnIndex As Integer = 0
        Dim found As Boolean = False
        Dim indicator As String = ""

        If cboEnclosureChar.Text <> "None" Then 'Field Names and Values are enclosed
            '#1 - Check if there exists a value enclosed by given enclosure character
            sEnclosureCharacter = IIf(cboEnclosureChar.Text = "Single Quote(')", "'", """")
            found = IsEnclosedValueFound(lineText, sEnclosureCharacter)
            '#2 - Extract enclosed field values
            values.AddRange(HelperModule.Split(lineText, sDelimiter, sEnclosureCharacter, True))
        Else
            '#2 - Extract field values (could be enclosed in either ' or "") delimited by given delimiter 
            values.AddRange(HelperModule.Split(lineText, sDelimiter, Nothing, True))
        End If

        Select Case recordType
            Case PIndicatorTextBox.Text
                columnIndex = Convert.ToInt32(PColumnNumberBox.Number)
                indicator = PIndicatorTextBox.Text.Trim
            Case WIndicatorTextBox.Text
                columnIndex = Convert.ToInt32(WColumnNumberBox.Number)
                indicator = WIndicatorTextBox.Text.Trim
            Case IIndicatorTextBox.Text
                columnIndex = Convert.ToInt32(IColumnNumberBox.Number)
                indicator = IIndicatorTextBox.Text.Trim
        End Select

        If values.Count > columnIndex Then
            If values(columnIndex - 1) = indicator Then
                Return values
            Else
                Return values
            End If
        Else
            'Invalid Column Index
            Return values
        End If
        Return values
    End Function
#End Region

#Region "Save/Save As/Cancel/Update/Validate"

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ConfirmBeforeCloseAndCancel()
    End Sub

    Friend Function ConfirmBeforeCloseAndCancel() As Boolean
        'Cancel - Return to "View" screen			
        '1	User will be prompted with a message to save, if the data has been changed.	
        '2	On "Yes"	
        '	 	New - Template will be saved as Draft.
        '	 	If user edited the Draft template then the template will be saved as Draft.
        '	 	If user edited the Non-Draft template then the template will be saved as Non-Draft.
        '3	On "No"	
        '	 	The changes made will not be saved.
        '	 	Show the "View" screen.
        '4	On "Cancel"	
        '	 	The Entry screen will remain with the data changes.
        If _dataModel Is Nothing Then
            ContainerForm.ShowView()
            Return False
        End If

        If _dataModel.IsDirty Then

            Dim _decision As DialogResult = MessageBox.Show(MsgReader.GetString("E10000033") _
                    , "Generate Common Transaction Template", MessageBoxButtons.YesNoCancel)

            Select Case _decision

                Case DialogResult.Yes
                    If _isNew Then
                        If Not IsSavableAsDraft() Then Return True
                        btnSaveAsDraft_Click(Nothing, Nothing)

                    Else
                        If _dataModel.IsDraft Then
                            If Not IsSavableAsDraft() Then Return True
                            btnSaveAsDraft_Click(Nothing, Nothing)

                        Else
                            If Not IsDataSavable() Then Return True
                            btnSave_Click(Nothing, Nothing)

                        End If
                    End If
                Case DialogResult.No
                    ContainerForm.ShowView()
                Case DialogResult.Cancel
                    Return True
            End Select

        Else
            ContainerForm.ShowView()
        End If

        Return False

    End Function

    Private Sub Control_Validated(ByVal sender As Object, _
                                    ByVal e As System.EventArgs) Handles txtCommonTemplateName.Validated

        Dim ctl As Control = CType(sender, Control)
        Dim bnd As Binding

        For Each bnd In ctl.DataBindings
            If bnd.IsBinding Then
                Dim obj As System.ComponentModel.IDataErrorInfo = _
                  CType(_dataModel, System.ComponentModel.IDataErrorInfo)
                ErrorProvider1.SetError( _
                  ctl, obj.Item(bnd.BindingMemberInfo.BindingField))
            End If
        Next

    End Sub

    Private Sub ValidateAllControls(ByVal ctl As Control)
        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                ValidateAllControls(ctlDetail)
            Else
                Control_Validated(ctlDetail, Nothing)
            End If
        Next

    End Sub

    Private Sub prepareForSave()

        _dataModel.IsCarriageReturn = rdoCarriageReturn.Checked
        _dataModel.IsDelimited = rdoDelimited.Checked
        _dataModel.IsAmountInCents = rdoCents.Checked
        _dataModel.IsAmountInDollars = rdoDollars.Checked
        _dataModel.IsFilterConditionAND = rdoFilterSettingAND.Checked
        _dataModel.IsFilterConditionOR = rdoFilterSettingOR.Checked
        _dataModel.IsAdviceRecordForEveryRow = rdoIRTMSEveryRow.Checked
        _dataModel.IsAdviceRecordAfterCharacter = rdoIRTMSCharPos.Checked
        _dataModel.IsAdviceRecordDelimited = rdoIRTMSDelimiter.Checked
        _dataModel.IsFixedWidthFile = chkFixedWidth.Checked
        _dataModel.DateSeparator = cboDateSep.Text
        _dataModel.DelimiterCharacter = cboDelimiter.Text
        _dataModel.OtherSeparator = txtOtherDelimiter.Text

    End Sub

    Private Function ShowValidationError() As Boolean
        Dim errorInfo As System.ComponentModel.IDataErrorInfo
        Dim errorMessage As String
        Dim firstError As New System.Text.StringBuilder
        Dim detailError As New System.Text.StringBuilder

        errorInfo = CType(_dataModel, System.ComponentModel.IDataErrorInfo)
        errorMessage = errorInfo.Error
        If Not IsNothingOrEmptyString(errorMessage) Then
            Dim errorList As String() = errorMessage.Split(Environment.NewLine)
            Dim temp As String
            For Each temp In errorList
                If firstError.Length > 0 Then
                    If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", temp))
                Else
                    If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("{0}", temp))
                End If
            Next
        End If

        For Each detailItem As Magic.CommonTemplate.RowFilter In _dataModel.Filters
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If

            If detailItem.FilterOperator Is Nothing Or detailItem.FilterType Is Nothing Or detailItem.FilterValue Is Nothing Then Continue For

            ''''''BEGIN - data type validation '''''''''''''''''
            Select Case detailItem.FilterOperator.ToUpper()

                Case "> AND <", "> AND <=", ">= AND <", ">= AND <="

                    Dim values2() As String = Regex.Split(detailItem.FilterValue.Trim(), Regex.Escape("||"))

                    If values2.Length = 2 Then
                        'validate data type
                        If detailItem.FilterType.ToUpper() = "NUMERIC" Then
                            Dim x As Decimal
                            For Each numbervalue As String In values2
                                If Not HelperModule.IsDecimalDataType(numbervalue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                                    If firstError.Length > 0 Then
                                        detailError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, numbervalue))
                                    Else
                                        firstError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, numbervalue))
                                    End If
                                End If
                            Next

                        ElseIf detailItem.FilterType.ToUpper() = "DATE" Then
                            For Each datevalue As String In values2
                                Dim x As Date
                                If Not HelperModule.IsDateDataType(datevalue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZeroInDate) Then
                                    If firstError.Length > 0 Then
                                        detailError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, datevalue))
                                    Else
                                        firstError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, datevalue))
                                    End If
                                End If
                            Next
                        End If
                    Else

                        If firstError.Length > 0 Then
                            detailError.AppendLine(String.Format(MsgReader.GetString("E10000040"), detailItem.Sequence))
                        Else
                            firstError.AppendLine(String.Format(MsgReader.GetString("E10000040"), detailItem.Sequence))
                        End If

                    End If

                Case Else
                    'validate data type
                    If detailItem.FilterType.ToUpper() = "NUMERIC" Then

                        Dim x As Decimal
                        If Not HelperModule.IsDecimalDataType(detailItem.FilterValue, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, x) Then
                            If firstError.Length > 0 Then
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Numeric Value is expected"
                                detailError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, detailItem.FilterValue))
                            Else
                                firstError.AppendLine(String.Format(MsgReader.GetString("E10000038"), detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    ElseIf detailItem.FilterType.ToUpper() = "DATE" Then

                        Dim x As Date
                        If Not HelperModule.IsDateDataType(detailItem.FilterValue, _dataModel.DateSeparator, _dataModel.DateType, x, _dataModel.IsZeroInDate) Then
                            If firstError.Length > 0 Then
                                '"Filter Setting (Position: {0}) -  Filter Value '{1}' is not in expected format!, Date Value is expected"
                                detailError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, detailItem.FilterValue))
                            Else
                                firstError.AppendLine(String.Format(MsgReader.GetString("E10000039"), detailItem.Sequence, detailItem.FilterValue))
                            End If
                        End If

                    End If

            End Select
            ''''''END - data type validation '''''''''''''''''

            'duplicate RowFilter?
            If _dataModel.Filters.IsDuplicateRowFilter(detailItem) Then
                If firstError.Length > 0 Then
                    '"Filter Setting (Position: {0}) - Duplicate Row Filter!"
                    detailError.AppendLine(String.Format(MsgReader.GetString("E10000041"), detailItem.Sequence))
                Else
                    firstError.AppendLine(String.Format(MsgReader.GetString("E10000041"), detailItem.Sequence))
                End If
            End If

            'Multiple Instances of a RowFilter Field with Different Filter Type (Data Type)?
            If _dataModel.Filters.hasDuplicateFilterType(detailItem) Then
                If firstError.Length > 0 Then
                    detailError.AppendLine(String.Format("Filter Setting (Position: {0}) - Duplicate Row Filter Field '{1}' cannot have different Data Type!", detailItem.Sequence, detailItem.FilterField))
                Else
                    firstError.AppendLine(String.Format("Filter Setting (Position: {0}) - Duplicate Row Filter Field '{1}' cannot have different Data Type!", detailItem.Sequence, detailItem.FilterField))
                End If
            End If

        Next


        For Each detailItem As Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Map (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.TranslatorSetting In _dataModel.TranslatorSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Translation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next


        For Each detailItem As Magic.CommonTemplate.EditableSetting In _dataModel.EditableSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Editable Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.LookupSetting In _dataModel.LookupSettings
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Lookup Setting (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.CalculatedField In _dataModel.CalculatedFields
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("Calculation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        For Each detailItem As Magic.CommonTemplate.StringManipulation In _dataModel.StringManipulations
            errorInfo = CType(detailItem, System.ComponentModel.IDataErrorInfo)
            errorMessage = errorInfo.Error
            If firstError.Length = 0 Then
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If firstError.Length > 0 Then
                            If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                        Else
                            If (temp.Trim.Length > 0) Then firstError.AppendLine(String.Format("String Manipulation  (Position: {0}) - {1}", detailItem.Sequence, temp))
                        End If
                    Next
                End If
            Else
                If Not IsNothingOrEmptyString(errorMessage) Then
                    Dim errorList As String() = errorMessage.Split(Environment.NewLine)
                    Dim temp As String
                    For Each temp In errorList
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("String Manipulation (Position: {0}) - {1}", detailItem.Sequence, temp))
                    Next
                End If
            End If
        Next

        ValidateAllControls(Me)

        If (firstError.ToString() = String.Empty AndAlso detailError.ToString() = String.Empty) Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", firstError.ToString & vbCrLf & detailError.ToString)
            Return False
        End If

    End Function

    Private Function CanSaveCleanData() As Boolean

        _dataModel.Validate()

        If Not (ValidateMandatoryField()) Then Return False

        'If Not VerifyMapping() Then Return False

        If ShowValidationError() Then

            prepareForSave()
            Dim dirtFlag As Boolean = _dataModel.IsDirty
            _dataModel.ApplyEdit()
            _dataModel.SetDirty(dirtFlag)
            If Not VerifyMapping() Then Return False
            If Not VerifyRecordTypeIndicators() Then Return False

            Return True
        Else
            Return False
        End If


    End Function

    ''' <summary>
    ''' This function is to Set a new source file name and definition file
    ''' </summary>
    ''' <param name="SrcFileName">Source file name</param>
    ''' <param name="DfnFileName">Definition file name</param>
    ''' <remarks></remarks>
    Public Sub SetSourceFile(ByVal SrcFileName As String, ByVal DfnFileName As String)

        _dataModel.SourceFileName = SrcFileName
        _dataModel.DefinitionFileName = DfnFileName
        _dataModel.SetDefaultTranslation()
    End Sub

    Private Function IsDataSavable() As Boolean

        If CanSaveCleanData() Then

            'Checking duplicate template name
            If txtCommonTemplateName.Enabled = True And _
                (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                 File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                '"Template Name already exists. Please use different Template Name."
                MessageBox.Show(MsgReader.GetString("E10000034"), "Duplicate")
                Return False
            Else
                Return True
            End If
        End If

        Return False

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try


            If CanSaveCleanData() Then

                'Checking duplicate template name
                If txtCommonTemplateName.Enabled = True And _
                    (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                     File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                    '"Template Name already exists. Please use different Template Name."
                    MessageBox.Show(MsgReader.GetString("E10000034"), "Duplicate")
                    Exit Sub
                End If


                Dim fileToBeDeleted As String = ""
                If _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension

                _dataModel.IsDraft = False
                _isNew = False

                _dataModel.SaveToFile2( _
                            System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                                                    , txtCommonTemplateName.Text) _
                            & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)

                If _isNew Then
                    Log_CreateCommonTemplate(frmLogin.GsUserName, _
                                System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                                            , txtCommonTemplateName.Text) _
                                & CommonTemplateModule.CommonTemplateFileExtension)
                Else
                    Log_ModifyCommonTemplate(frmLogin.GsUserName, _
                               System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextFolder _
                                           , txtCommonTemplateName.Text) _
                               & CommonTemplateModule.CommonTemplateFileExtension)
                End If


                '"Common Transaction Template was saved successfully"
                MessageBox.Show(MsgReader.GetString("E10000074") _
                                , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)


                ' Refresh the CommonTemplate in the list and Show the listview
                ContainerForm.ShowView()
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        EnableDisableForm(True)
        EnableDisableRecordIndicator()
        EnableDisableAdviceRecordSetting()
        'setDataSourceToBindingSource()

        _dataModel.SetDirty(True)

        txtCommonTemplateName.Enabled = False
        cboOutputTemplate.Enabled = False

    End Sub

    Private Function ValidateCommonTemplateName() As Boolean

        Try
            Dim MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

            Dim value As String = txtCommonTemplateName.Text
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                Common.BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E01000010"), "Common Template Name"))

                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

        Return True

    End Function

    Private Function IsSavableAsDraft() As Boolean
        Try


            Dim errMessage As String = String.Empty
            '"Please select Master Template" 
            If _dataModel.OutputTemplateName = "" Then
                errMessage &= MsgReader.GetString("E10000035") & vbCrLf
            End If
            '"Please fill in Common Template Name"
            If _dataModel.CommonTemplateName = "" Then
                errMessage &= MsgReader.GetString("E10000073") & vbCrLf
            End If

            ValidateCommonTemplateName()

            'Checking duplicate template name
            If txtCommonTemplateName.Enabled = True And _
                (File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension) Or _
                 File.Exists(Path.Combine(CommonTemplateModule.CommonTemplateTextFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension)) Then
                errMessage &= MsgReader.GetString("E10000034") & vbCrLf
                '"Template Name already exists. Please use different Template Name."
            End If

            If errMessage.Trim = String.Empty Then
                Return True
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Save as Draft : ", errMessage)
                Return False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Failed to save Template!", ex.Message)
        End Try

    End Function

    Private Sub btnSaveAsDraft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAsDraft.Click



        If Not IsSavableAsDraft() Then Exit Sub

        Dim fileToBeDeleted As String = ""
        If Not _dataModel.IsDraft Then fileToBeDeleted = Path.Combine(CommonTemplateModule.CommonTemplateTextFolder, txtCommonTemplateName.Text) & CommonTemplateModule.CommonTemplateFileExtension


        _dataModel.IsDraft = True
        _dataModel.ApplyEdit()
        _isNew = False
        prepareForSave()

        _dataModel.SaveToFile2( _
            System.IO.Path.Combine(CommonTemplateModule.CommonTemplateTextDraftFolder _
                                , txtCommonTemplateName.Text) _
            & CommonTemplateModule.CommonTemplateFileExtension, fileToBeDeleted)
        'Draft was saved successfully
        MessageBox.Show(MsgReader.GetString("E10000044") _
                    , "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

        ContainerForm.ShowDraftView()

    End Sub

#End Region

#Region " Up/Down/Delete and Input at Grid "

#Region "Handler for Grid Row Up/Down Buttons"

    Private Sub SelectGridRow(ByVal row As GridRow, ByVal grid As DataGridView, ByRef Xobj As Object, ByRef YObj As Object)

        Try

            If grid.CurrentRow Is Nothing Or grid.Rows.Count <= 0 _
                Or grid.CurrentRow.Index + 1 = grid.Rows.Count Then
                Exit Sub
            End If

            If row = GridRow.Up Then ' Row Up

                If grid.CurrentRow.Index = 0 Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index - 1).Selected = True
            Else ' Row Down

                If grid.CurrentRow.Index + 2 = grid.Rows.Count Then
                    Exit Sub
                End If
                'move selection up
                grid.Item(0, grid.CurrentRow.Index + 1).Selected = True
            End If

            bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False

            Dim tmp As Object = Xobj

            Xobj = YObj : YObj = tmp

            bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
            bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnFilterSettingsUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterSettingsUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvFilterSettings, _dataModel.Filters(dgvFilterSettings.CurrentRow.Index), _dataModel.Filters(dgvFilterSettings.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnFilterSettingsDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilterSettingsDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvFilterSettings, _dataModel.Filters(dgvFilterSettings.CurrentRow.Index), _dataModel.Filters(dgvFilterSettings.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnTranslatorSettingRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTranslatorSettingRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnTranslatorSettingRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTranslatorSettingRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvTranslator, _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index), _dataModel.TranslatorSettings(dgvTranslator.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnEditableSettingRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditableSettingRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnEditableSettingRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditableSettingRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvEditable, _dataModel.EditableSettings(dgvEditable.CurrentRow.Index), _dataModel.EditableSettings(dgvEditable.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnLookupRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnLookupRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvLookup, _dataModel.LookupSettings(dgvLookup.CurrentRow.Index), _dataModel.LookupSettings(dgvLookup.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnCalculatedFieldsRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculatedFieldsRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvCalculated, _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnCalculatedFieldsRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculatedFieldsRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvCalculated, _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index), _dataModel.CalculatedFields(dgvCalculated.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnStringManipulationRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStringManipulationRowUp.Click

        Try
            SelectGridRow(GridRow.Up, dgvStringManipulation, _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index), _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index - 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

    Private Sub btnStringManipulationRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStringManipulationRowDown.Click

        Try
            SelectGridRow(GridRow.Down, dgvStringManipulation, _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index), _dataModel.StringManipulations(dgvStringManipulation.CurrentRow.Index + 1))
        Catch ex As Exception
            'dont bother
        End Try

    End Sub

#End Region

#Region "Handler for GridRow Delete Buttons"

    

    Private Sub dgvFilterSettings_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFilterSettings.CellContentClick

        Try

            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvFilterSettings.Rows(e.RowIndex).Cells("dgvBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
            String.Format(MsgReader.GetString("E10000045"), "Filter Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.Filters.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub dgvTranslator_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranslator.CellContentClick
        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvTranslator.Rows(e.RowIndex).Cells("dgvTranslatorBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Translation Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.TranslatorSettings.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub



    Private Sub dgvEditable_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEditable.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvEditable.Rows(e.RowIndex).Cells("dgvEditableBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Editable Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.EditableSettings.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvLookup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLookup.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvLookup.Rows(e.RowIndex).Cells("dgvLookupBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Lookup Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.LookupSettings.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvCalculated_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCalculated.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvCalculated.Rows(e.RowIndex).Cells("dgvCalculatedBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "Calculation Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.CalculatedFields.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvStringManipulation_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStringManipulation.CellContentClick

        Try
            If e.RowIndex = -1 Or e.ColumnIndex <> 0 Then
                Exit Sub
            End If

            If dgvStringManipulation.Rows(e.RowIndex).Cells("dgvStringManipulationBtnDelete").Value Is Nothing Then
                Exit Sub
            End If

            If MessageBox.Show( _
                    String.Format(MsgReader.GetString("E10000045"), "String Manipulation Setting" _
                    , e.RowIndex + 1), "Confirmation" _
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question _
                    , MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = False
                _dataModel.StringManipulations.RemoveAt(e.RowIndex)
                bindingSrcCommonTemplateTextDetailCollection.RaiseListChangedEvents = True
                bindingSrcCommonTemplateTextDetailCollection.ResetBindings(False)

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

#End Region

#End Region

#End Region

#Region "Validation Functions"
    Private Function ValidateEnclosureCharacter() As Boolean
        '"'Enclosure Character' has not been selected yet, please select 'Enclosure Character'."
        If cboEnclosureChar.SelectedIndex = -1 Then
            MessageBox.Show(MsgReader.GetString("E10000072"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboEnclosureChar.Focus()
            Return False
        End If
        Return True

    End Function

    Private Function ValidateSampleStartingCharacter() As Boolean
        '"'Sample Starting Character' is empty, please fill in 'Sample Starting Character'." 
        If txtStartingChar.Number = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000071") _
            , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtStartingChar.Focus()
            Return False
        End If


        If Convert.ToInt32(txtStartingChar.Number) <= 0 Then
            MessageBox.Show(MsgReader.GetString("E10000075") _
            , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtStartingChar.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidateLengthOfCharacterTransaction() As Boolean
        '"'Length of Each Character Transaction' is empty, please fill in 'Length of Each Character Transaction'." 
        If txtLenOfChar.Number = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000070") _
            , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtLenOfChar.Focus()
            Return False
        End If
        If Convert.ToInt32(txtLenOfChar.Number) <= 0 Then
            MessageBox.Show(MsgReader.GetString("E10000076") _
            , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtLenOfChar.Focus()
            Return False
        End If
        Return True
    End Function

    Private Function ValidateDelimiterCharacter() As Boolean
        If cboDelimiter.SelectedIndex = -1 Then
            '"'Delimiter' has not been selected yet, please select 'Delimiter'."
            MessageBox.Show(MsgReader.GetString("E10000069"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDelimiter.Focus()
            Return False
        Else
            If cboDelimiter.Text = "Other" Then
                If txtOtherDelimiter.Text.Trim() = "" Then
                    '"'Other Delimiter' is empty, please fill in 'Other Delimiter'."
                    MessageBox.Show(MsgReader.GetString("E10000068"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtOtherDelimiter.Focus()
                    Return False
                End If
            End If
        End If
        Return True
    End Function

    Private Function ValidateNumberSetting() As Boolean

        If cboDecimalSep.SelectedIndex = -1 Then
            '"'Decimal Separator' has not been selected yet, please select 'Decimal Separator'."
            MessageBox.Show(MsgReader.GetString("E10000046"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDecimalSep.Focus()
            Return False
        ElseIf cboThousandSep.SelectedIndex = -1 Then
            '"'Thousand Separator' has not been selected yet, please select 'Thousand Separator'."
            MessageBox.Show(MsgReader.GetString("E10000067"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboThousandSep.Focus()
            Return False
        ElseIf (cboDecimalSep.Text = "." And cboThousandSep.Text = ".") Or (cboDecimalSep.Text = "," And cboThousandSep.Text = ",") Then
            '"'Decimal Separator' and 'Thousand Separator' must be different." 
            MessageBox.Show(MsgReader.GetString("E02000070"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDecimalSep.Focus()
            Return False
        End If

        ' cents & decimal separator should not co-exist!
        If ((cboDecimalSep.Text = "." Or cboDecimalSep.Text = ",") _
                AndAlso rdoCents.Checked) Then
            If MessageBox.Show(MsgReader.GetString("E10000047") _
                    , "Generate Common Transaction Template", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                cboDecimalSep.Focus()
                Return False
            End If
        End If

        Return True

    End Function

    Private Function ValidateDateSetting() As Boolean
        '"'Date Type' has not been selected yet, please select 'Date Type'."
        If cboDateType.SelectedIndex = -1 Then
            MessageBox.Show(MsgReader.GetString("E10000066"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDateType.Focus()
            Return False
        ElseIf cboDateSep.SelectedIndex = -1 Then
            '"'Date Separator' has not been selected yet, please select 'Date Separator'."
            MessageBox.Show(MsgReader.GetString("E10000065"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDateSep.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateHeaderRow() As Boolean
        '"'Header at Row' is empty, please fill in 'Header at Row'."
        If txtHeaderRowNo.Number.Trim() = "" Then
            MessageBox.Show(MsgReader.GetString("E10000064"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtHeaderRowNo.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateTransactionStartRow() As Boolean

        If txtTransactionStartRow.Number.Trim() = "" Then
            '"'Transaction Starting Row' is empty, please fill in 'Transaction Starting Row'."
            MessageBox.Show(MsgReader.GetString("E10000063"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtTransactionStartRow.Focus()
            Return False
            '"'Transaction Starting Row' should be greater than 0, please fill in 'Transaction Starting Row' again."
        ElseIf Convert.ToInt32(txtTransactionStartRow.Number) < 1 Then
            MessageBox.Show(MsgReader.GetString("E10000062"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtTransactionStartRow.Focus()
            Return False '"'Transaction Starting Row' must be greater than 'Header at Row', please fill in 'Transaction Starting Row' again."
        ElseIf CInt(txtTransactionStartRow.Number) <= Convert.ToInt32(txtHeaderRowNo.Number) Then
            MessageBox.Show(MsgReader.GetString("E02000160"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtTransactionStartRow.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateSampleRow() As Boolean
        '"'Sample Row' is empty, please fill in 'Sample Row'"
        If txtSampleRow.Number = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000061"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtSampleRow.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub cboDuplicateTxnRefField2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDuplicateTxnRefField2.Enter
        If cboDuplicateTxnRefField1.Items.Count = 0 Then Exit Sub
        '"'Reference Field 1' has not been selected yet. Please select 'Reference Field 1' first." 
        If cboDuplicateTxnRefField1.Text.Trim() = "" Then
            MessageBox.Show(MsgReader.GetString("E02000170") _
                    , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDuplicateTxnRefField1.Focus()

        Else
            '"'Reference Field 2' is the same as 'Reference Field 1'. Please select another reference field."
            If cboDuplicateTxnRefField1.Text = cboDuplicateTxnRefField2.Text Then
                MessageBox.Show(MsgReader.GetString("E02000180") _
                , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()
            End If
        End If
    End Sub

    Private Sub cboDuplicateTxnRefField3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDuplicateTxnRefField3.Enter

        If cboDuplicateTxnRefField1.Items.Count = 0 Then Exit Sub

        '"'Reference Field 1' has not been selected yet. Please select 'Reference Field 1' first."
        If cboDuplicateTxnRefField1.Text.Trim() = "" Then
            MessageBox.Show(MsgReader.GetString("E02000170") _
                  , "Generate Swift Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDuplicateTxnRefField1.Focus()

        Else
            '"'Reference Field 2' has not been selected yet. Please select  'Reference Field 2' first."
            If cboDuplicateTxnRefField2.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E02000200") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()

            Else
                '"'Reference Field 3' is the same as 'Reference Field 1'. Please select another reference field."
                If cboDuplicateTxnRefField1.Text = cboDuplicateTxnRefField3.Text Then
                    MessageBox.Show(MsgReader.GetString("E02000210") _
                                    , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                    MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField3.Focus()

                End If
                '"'Reference Field 3' is the same as 'Reference Field 2'. Please select another reference field." 
                If cboDuplicateTxnRefField2.Text = cboDuplicateTxnRefField3.Text Then
                    MessageBox.Show(MsgReader.GetString("E02000220") _
                                , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField3.Focus()

                End If

            End If

        End If
    End Sub

    Private Function ValidateOutputTemplate() As Boolean
        '"Master Template has not been selected yet, please select Master Template"
        If cboOutputTemplate.Text = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000035"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cboOutputTemplate.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidateSourceFile() As Boolean
        '"There is no source file name, please enter a source file name."
        If txtSourceFileName.Text = String.Empty Then
            MessageBox.Show(MsgReader.GetString("E10000028"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            btnBrowseSourceFile.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function ValidateMandatoryField() As Boolean

        If Not (ValidateOutputTemplate() AndAlso ValidateSourceFile()) Then Return False

        If rdoDelimited.Checked Then
            If Not (ValidateSampleStartingCharacter() AndAlso ValidateLengthOfCharacterTransaction()) Then Return False
        Else
            If Not (ValidateSampleRow() AndAlso ValidateHeaderRow() AndAlso ValidateTransactionStartRow()) Then Return False
        End If

        If Not chkFixedWidth.Checked Then
            If Not ValidateEnclosureCharacter() Then Return False
        End If

        If Not (ValidateDelimiterCharacter() AndAlso ValidateNumberSetting() AndAlso ValidateDateSetting()) Then Return False


        If Not (ValidateTxnReferenceFields() AndAlso ValidateCharsPerAdviseRecord() AndAlso ValidateAdviseRecordDelimiter()) Then Return False

        Return True

    End Function

    Private Function ValidateTxnReferenceFields() As Boolean

        If cboDuplicateTxnRefField1.Items.Count = 0 Then Return True

        ' 3rd txnref is given, 2nd txnref is mandatory and should not be equal to 3
        If cboDuplicateTxnRefField3.Text.Trim() <> "" Then
            '"'Reference Field 2' has not been selected yet. Please select  'Reference Field 2' first."
            If cboDuplicateTxnRefField2.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E02000200") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField2.Focus()
                Return False
            Else
                '"'Reference Field 3' is the same as 'Reference Field 2'. Please select another reference field."
                If cboDuplicateTxnRefField3.Text.Trim() = cboDuplicateTxnRefField2.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000220") _
                                                   , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField2.Focus()
                    Return False
                End If
                '"'Reference Field 3' is the same as 'Reference Field 1'. Please select another reference field." 
                If cboDuplicateTxnRefField3.Text.Trim() = cboDuplicateTxnRefField1.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000210") _
                                                   , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField1.Focus()
                    Return False
                End If

            End If
        End If


        ' 2nd txnref is given, 1st txnref is mandatory and should not be equal to 2
        If cboDuplicateTxnRefField2.Text.Trim() <> "" Then
            '"'Reference Field 1' has not been selected yet. Please select  'Reference Field 1' first." 
            If cboDuplicateTxnRefField1.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E02000170") _
                         , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                cboDuplicateTxnRefField1.Focus()
                Return False
            Else
                '"'Reference Field 2' is the same as 'Reference Field 1'. Please select another reference field."
                If cboDuplicateTxnRefField1.Text.Trim() = cboDuplicateTxnRefField2.Text.Trim() Then
                    MessageBox.Show(MsgReader.GetString("E02000180") _
                                                   , "Generate Swift Transaction Template", MessageBoxButtons.OK, _
                                                   MessageBoxIcon.Exclamation)
                    cboDuplicateTxnRefField1.Focus()
                    Return False
                End If
            End If
        End If

        Return True

    End Function

    Private Function ValidateCharsPerAdviseRecord() As Boolean
        '"'No. of Characters per Advice Record' is empty. Please fill in no. of characters."
        If rdoIRTMSCharPos.Checked AndAlso txtNoOfCharsPerAdviseRecord.Number = "" Then
            MessageBox.Show(MsgReader.GetString("E02000270"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtNoOfCharsPerAdviseRecord.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateAdviseRecordDelimiter() As Boolean
        '"Delimeter for the advice record is empty. Please fill in delimiter."
        If rdoIRTMSDelimiter.Checked AndAlso txtAdviseRecordDelimiter.Text = "" Then
            MessageBox.Show(MsgReader.GetString("E02000280"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtAdviseRecordDelimiter.Focus()
            Return False
        End If

        Return True

    End Function

    Private Function ValidateTypeOfMappedFields(ByVal _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField _
                    , ByVal objCusValue As BTMU.Magic.CommonTemplate.MapSourceField) As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        If _objMapBankField.DataType Is Nothing Then Return True

        Select Case _objMapBankField.DataType.ToUpper()
            Case "NUMBER", "CURRENCY"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                    If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                        errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                    End If
                End If
            Case "DATETIME"
                If Not objCusValue.SourceFieldValue Is Nothing Then
                    value = objCusValue.SourceFieldValue.Replace("""", "").Replace("'", "")
                    If cboDateSep.Text <> "No Space" Then
                        If cboDateSep.Text = "<Space>" Then
                            value = value.Replace(" ", "")
                            'Else
                            '    value = value.Replace(cboDateSep.Text, "")
                        End If
                    End If
                    If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZeroInDate) Then
                        errMsg = "Field type '" & _objMapBankField.BankField & "' and '" & objCusValue.SourceFieldName & "' do not match."
                    End If
                End If
        End Select

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function

    Private Function ValidateFieldsForFormatsWithRecordTypes() As Boolean
        If Not (ValidateRecordTypes() AndAlso ValidateEnclosureCharacter() AndAlso ValidateDelimiterCharacter()) Then Return False
        'If Not ValidateEnclosureCharacter() Then Return False
        'If Not ValidateDelimiterCharacter() Then Return False

        Return True

    End Function
    ''' <summary>
    ''' This function is used to validate whether any one of the Record Type Indicators are filled in.
    ''' The sample source file can be in any format with one or more recrod types.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>This validation is done only for iFTS-2 and Mr. Omakase India File Formats only.</remarks>
    Private Function ValidateRecordTypes()

        Dim _errMessage As New System.Text.StringBuilder
        Try
            ' P Indicator and P Indicator Column is must
            If IsNothingOrEmptyString(_dataModel.PColumn) Then
                _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "P Column"))
            End If
            If IsNothingOrEmptyString(_dataModel.PIndicator) Then
                _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "P Indicator"))
            End If

            If Not IsNothingOrEmptyString(_dataModel.WColumn) Then
                If IsNothingOrEmptyString(_dataModel.WIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "W Indicator"))
                End If
            Else
                If Not IsNothingOrEmptyString(_dataModel.WIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "W Column"))
                End If
            End If

            If Not IsNothingOrEmptyString(_dataModel.IColumn) Then
                If IsNothingOrEmptyString(_dataModel.IIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "I Indicator"))
                End If
            Else
                If Not IsNothingOrEmptyString(_dataModel.IIndicator) Then
                    _errMessage.AppendLine(String.Format(MsgReader.GetString("E00000050"), "I Column"))
                End If
            End If
            If _errMessage.ToString = String.Empty Then
                Return True
            Else
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on getting sample row : ", _errMessage.ToString)
                Return False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on getting sample row : ", ex.Message.ToString)
            Return False
        End Try
    End Function
#End Region

#Region "Separate Field Names and Values"

    Private Function getOutputFileFormat() As String

        Dim filefmt As String = ""

        Try
            If Not bindingSrcOutputTemplate.Current Is Nothing Then
                Dim selMstTmp As BTMU.Magic.MasterTemplate.MasterTemplateList
                selMstTmp = bindingSrcOutputTemplate.Current
                filefmt = selMstTmp.OutputFormat
            End If
        Finally
        End Try

        Return filefmt

    End Function

    Private Function UpdateMapSourceFields(ByVal newSrcFilename As String) As Boolean

        If newSrcFilename = "" Or _dataModel.MapSourceFields.Count = 0 Then Exit Function

        Dim names As New List(Of String)
        Dim values As New List(Of String)
        Dim SuccessfullySeparated As Boolean = False

        Dim sOutputFileFormat As String = getOutputFileFormat()

        If chkFixedWidth.Checked Then 'fixed width file format is not applicable for iFTS2 format
            '"'Separate' field is empty, please click on 'Retrieve' button to get 'Separate' field."
            If txtSampleRowData.Text.Trim() = "" Then
                MessageBox.Show(MsgReader.GetString("E10000060") _
                    , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                btnRetrieve.Focus()
                Return False
            End If
            '"'Delimiter' is not found. Please separate fields first with ';' delimiter."
            If txtSampleRowData.Text.LastIndexOf(";") = -1 Then
                MessageBox.Show(MsgReader.GetString("E10000059") _
                    , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtSampleRowData.Focus()
                Return False
            End If


            'Array of Indices of Delimiter(;)
            Dim delIndices As New List(Of Integer)
            Dim delIndex = 0

            Do

                delIndex = txtSampleRowData.Text.IndexOf(";", delIndex)
                If delIndex = -1 Then Exit Do
                delIndices.Add(delIndex)
                delIndex = delIndex + 1
                If delIndex >= txtSampleRowData.Text.Length Then Exit Do

            Loop Until delIndex = -1

            SuccessfullySeparated = SeparateFixedWidthFile(newSrcFilename, names, values, delIndices)
            If Not SuccessfullySeparated Then Return False

        Else 'Non-fixed width source file''''''''''''''''''''''''''''''''''''''''''''

            SuccessfullySeparated = SeparateNonFixedWidthFile(newSrcFilename, names, values, sOutputFileFormat)
            If Not SuccessfullySeparated Then Return False
        End If


        '1. Does new file have the equal number of fields as that of existing source file?
        If names.Count < _dataModel.MapSourceFields.Count Then
            BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E10000049"), newSrcFilename))
            Return False
        End If

        '2. Do the field names of new file match with that of existing file?
        If chkHeaderAsField.Checked AndAlso Convert.ToInt32(txtHeaderRowNo.Number) <> 0 Then
            Dim _counter As Int32 = 0
            For _counter = 0 To _dataModel.MapSourceFields.Count - 1
                If _dataModel.MapSourceFields.Item(_counter).SourceFieldName = names.Item(_counter) Then Continue For
                BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E10000049"), newSrcFilename))
                Return False
            Next
        End If

        'update existing fields and add new fields
        Dim counter As Integer = 0
        Dim _nsrcfield As MapSourceField

        For Each _nname As String In names

            If counter >= _dataModel.MapSourceFields.Count Then
                _nsrcfield = New MapSourceField()

                _nsrcfield.SourceFieldName = _nname
                _nsrcfield.SourceFieldValue = values.Item(counter)
                If _dataModel.MapSourceFields.IsFound(_nsrcfield) Then _nsrcfield.SourceFieldName = String.Format("{0}_{1}", _nname, counter + 1)
                _dataModel.MapSourceFields.Add(_nsrcfield)

                'UPDATE THE Reference Fields
                cboDuplicateTxnRefField1.Items.Add(_nname)
                cboDuplicateTxnRefField2.Items.Add(_nname)
                cboDuplicateTxnRefField3.Items.Add(_nname)
            Else
                _dataModel.MapSourceFields.Item(counter).SourceFieldValue = values.Item(counter)
            End If
            counter += 1
        Next

        FillEditableTable()
        Return True
    End Function

    ''' <summary>
    ''' This function is used only for iFTS-2 MulitLine/Mr. Omakase India Formats.
    ''' The function checks if there is any new source field for P,W or I records in the new
    ''' sample source file selected.
    ''' </summary>
    ''' <param name="newSrcFileName">The newly selected sample source file</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function UpdateMapSourceFieldsWithRecordTypes(ByVal newSrcFileName As String) As Boolean
        Dim pFieldCount As Integer = 0
        Dim wFieldCount As Integer = 0
        Dim iFieldCount As Integer = 0
        Dim newPFieldCount As Integer = 0
        Dim newWFieldCount As Integer = 0
        Dim newIFieldCount As Integer = 0

        If newSrcFileName = "" Or _dataModel.MapSourceFields.Count = 0 Then Exit Function
        Dim names As New List(Of String)
        Dim values As New List(Of String)
        Dim SuccessfullySeparated As Boolean = False
        Dim sOutputFileFormat As String = getOutputFileFormat()
        If _dataModel.MapSourceFields.Count = 0 Then Exit Function
        

        SuccessfullySeparated = _dataModel.SeparateNonFixedWidthFileWithRecordTypes(newSrcFileName, names, values, sOutputFileFormat)
        If Not SuccessfullySeparated Then Return False

        ' Does the new file has the same no:of source fields?
        If names.Count < _dataModel.MapSourceFields.Count Then
            BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E10000049"), newSrcFileName))
            Return False
        End If

        ' Does the new file has the same no:of P-Fields, W-Fields and I-Fields?
        For Each mapField As MapSourceField In _dataModel.MapSourceFields
            If mapField.SourceFieldName.StartsWith("P-Field") Then
                pFieldCount += 1
            ElseIf mapField.SourceFieldName.StartsWith("W-Field") Then
                wFieldCount += 1
            ElseIf mapField.SourceFieldName.StartsWith("I-Field") Then
                iFieldCount += 1
            End If
        Next

        For intI As Integer = 0 To names.Count - 1
            If names(intI).StartsWith("P-Field") Then
                newPFieldCount += 1
            ElseIf names(intI).StartsWith("W-Field") Then
                newWFieldCount += 1
            ElseIf names(intI).StartsWith("I-Field") Then
                newIFieldCount += 1
            End If
        Next

        If newPFieldCount < pFieldCount OrElse newWFieldCount < wFieldCount OrElse newIFieldCount < iFieldCount Then
            BTMUExceptionManager.LogAndShowError("Error", String.Format(MsgReader.GetString("E10000049"), newSrcFileName))
            Return False
        End If

        'update existing fields and add new fields
        Dim counter As Integer = 0
        Dim _nsrcfield As MapSourceField
        For Each _nname As String In names
            If _nname.StartsWith("P-Field") Then
                _nsrcfield = New MapSourceField()
                _nsrcfield.SourceFieldName = _nname
                _nsrcfield.SourceFieldValue = values.Item(counter)
                If _dataModel.MapSourceFields.IsFound(_nsrcfield) Then
                    _dataModel.MapSourceFields.Item(counter).SourceFieldValue = values.Item(counter)
                Else
                    _dataModel.MapSourceFields.Insert(counter, _nsrcfield)
                    cboDuplicateTxnRefField1.Items.Add(_nname)
                    cboDuplicateTxnRefField2.Items.Add(_nname)
                    cboDuplicateTxnRefField3.Items.Add(_nname)
                End If
            ElseIf _nname.StartsWith("W-Field") Then
                _nsrcfield = New MapSourceField()
                _nsrcfield.SourceFieldName = _nname
                _nsrcfield.SourceFieldValue = values.Item(counter)
                If _dataModel.MapSourceFields.IsFound(_nsrcfield) Then
                    _dataModel.MapSourceFields.Item(counter).SourceFieldValue = values.Item(counter)
                Else
                    _dataModel.MapSourceFields.Insert(counter, _nsrcfield)
                End If
            ElseIf _nname.StartsWith("I-Field") Then
                _nsrcfield = New MapSourceField()
                _nsrcfield.SourceFieldName = _nname
                _nsrcfield.SourceFieldValue = values.Item(counter)
                If _dataModel.MapSourceFields.IsFound(_nsrcfield) Then
                    _dataModel.MapSourceFields.Item(counter).SourceFieldValue = values.Item(counter)
                Else
                    _dataModel.MapSourceFields.Insert(counter, _nsrcfield)
                End If
                _nsrcfield.SourceFieldName = String.Format("{0}_{1}", _nname, counter + 1)
            End If
            counter += 1
        Next
        FillEditableTable()
        Return True
    End Function
    Private Sub btnSeparate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeparate.Click
        Try
            If txtSampleRowData.Text.Trim() = "" Then
                Me.Cursor = Cursors.Default
                ''Separate' field is empty, please click on 'Retrieve' button to get 'Separate' field.
                MessageBox.Show(MsgReader.GetString("E10000060") _
                    , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                btnRetrieve.Focus()
                Exit Sub
            End If

            If Not (ValidateMandatoryField()) Then
                Exit Sub
            End If

            If IsFileFormatUsingRecordTypes() Then
                If Not ValidateRecordTypes() Then Exit Sub
            End If

            ' '' ''If the Source Fields - Map Bank Fields Mapping Exists - Update the Source Fields
            'Warn User of Loss of Data Pertaining to Settings
            If _dataModel.MapSourceFields.Count > 0 Then
                'This action will clear all settings! Are you sure you want to proceed?
                If MessageBox.Show(MsgReader.GetString("E10000050"), "Confirm", MessageBoxButtons.YesNo) <> DialogResult.Yes Then Exit Sub
            End If

            Dim names As New List(Of String)
            Dim values As New List(Of String)
            Dim SuccessfullySeparated As Boolean = False

            Dim sOutputFileFormat As String = getOutputFileFormat()
            Me.Cursor = Cursors.AppStarting

            'fixed width input source file format is not applicable for iFTS2 format
            If chkFixedWidth.Checked Then



                If txtSampleRowData.Text.LastIndexOf(";") = -1 Then
                    Me.Cursor = Cursors.Default
                    MessageBox.Show(MsgReader.GetString("E10000059") _
                        , "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtSampleRowData.Focus()
                    Exit Sub
                End If

                'Array of Indices of Delimiter(;)
                Dim delIndices As New List(Of Integer)
                Dim delIndex = 0

                Do

                    delIndex = txtSampleRowData.Text.IndexOf(";", delIndex)
                    If delIndex = -1 Then Exit Do
                    delIndices.Add(delIndex)
                    delIndex = delIndex + 1
                    If delIndex >= txtSampleRowData.Text.Length Then Exit Do

                Loop Until delIndex = -1

                SuccessfullySeparated = SeparateFixedWidthFile(txtSourceFileName.Text, names, values, delIndices)
                If Not SuccessfullySeparated Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            Else 'Non-fixed width source file''''''''''''''''''''''''''''''''''''''''''''
                If IsFileFormatUsingRecordTypes() Then
                    SuccessfullySeparated = _dataModel.SeparateNonFixedWidthFileWithRecordTypes(txtSourceFileName.Text, names, values, sOutputFileFormat)
                Else
                    SuccessfullySeparated = SeparateNonFixedWidthFile(txtSourceFileName.Text, names, values, sOutputFileFormat)
                End If
                If Not SuccessfullySeparated Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            End If

            ' CODE TO POPULATE BANK FIELDS UNDER DROP DOWNS (Row filter/Advice Record/Lookup/...) and Mapping

            ' clear Filters/translation/editable/lookup/calculation and string mani grids
            _dataModel.ClearSourceFieldDependantCollections()

            Dim counter As Int32 = 0
            'populate field names and values 
            _dataModel.MapSourceFields.Clear()
            If Not _dataModel Is Nothing Then

                For counter = 0 To names.Count - 1
                    Dim _objMapSrcFld As New BTMU.Magic.CommonTemplate.MapSourceField()
                    _objMapSrcFld.SourceFieldName = names.Item(counter)
                    _objMapSrcFld.SourceFieldValue = values.Item(counter)
                    _dataModel.MapSourceFields.Add(_objMapSrcFld)
                Next

            End If

            populateReferenceFields()

            cboDuplicateTxnRefField1.SelectedIndex = 0
            cboDuplicateTxnRefField2.SelectedIndex = 0
            cboDuplicateTxnRefField3.SelectedIndex = 0

            PopulateMapBankField()

            FillEditableTable()
            setDataSourceToBindingSource()
        Catch ex As UnequalNumberOfFields
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", String.Format("At Record #{0}. {1}", txtSampleRow.Number, ex.Message))
        Catch ex As IOException
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", ex.Message)
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E10000031"), MsgReader.GetString("E10000030"))
            BTMU.Magic.Common.BTMUExceptionManager.HandleException(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub
    Private Sub setDataSourceToBindingSource()
        If _dataModel Is Nothing Then Exit Sub
        Dim _pMapSourceFields As New MapSourceFieldCollection
        Dim _pMapBankFields As New MapBankFieldCollection

        If IsFileFormatUsingRecordTypes() Then
            For Each srcField As MapSourceField In _dataModel.MapSourceFields
                Dim cloneSourceField As New BTMU.Magic.CommonTemplate.MapSourceField
                cloneSourceField.SourceFieldName = srcField.SourceFieldName
                cloneSourceField.SourceFieldValue = srcField.SourceFieldValue
                If srcField.SourceFieldName.StartsWith("P-Field") Then
                    _pMapSourceFields.Add(cloneSourceField)
                End If
            Next
            bindingSrcRowFilterSourceField.DataSource = _pMapSourceFields
            bindingSrcLookupSourceField.DataSource = _pMapSourceFields
            bindingSrcOperand1SourceField.DataSource = _pMapSourceFields
            bindingSrcOperand2SourceField.DataSource = _pMapSourceFields
            bindingSrcOperand3SourceField.DataSource = _pMapSourceFields
            PopulateReferenceFieldCollection(_pMapSourceFields)
        Else
            bindingSrcRowFilterSourceField.DataSource = bindingSrcCommonTemplateTextDetailCollection
            bindingSrcRowFilterSourceField.DataMember = "MapSourceFields"
            bindingSrcLookupSourceField.DataSource = bindingSrcCommonTemplateTextDetailCollection
            bindingSrcLookupSourceField.DataMember = "MapSourceFields"
            bindingSrcOperand1SourceField.DataSource = bindingSrcCommonTemplateTextDetailCollection
            bindingSrcOperand1SourceField.DataMember = "MapSourceFields"
            bindingSrcOperand2SourceField.DataSource = bindingSrcCommonTemplateTextDetailCollection
            bindingSrcOperand2SourceField.DataMember = "MapSourceFields"
            bindingSrcOperand3SourceField.DataSource = bindingSrcCommonTemplateTextDetailCollection
            bindingSrcOperand3SourceField.DataMember = "MapSourceFields"
            populateReferenceFields()
        End If
    End Sub
    ''' <summary>
    ''' Populates Reference Fields
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReferenceFields()

        If _dataModel Is Nothing Then Exit Sub

        cboDuplicateTxnRefField1.Items.Clear()
        cboDuplicateTxnRefField2.Items.Clear()
        cboDuplicateTxnRefField3.Items.Clear()

        For Each _srcFld As MapSourceField In _dataModel.MapSourceFields

            cboDuplicateTxnRefField1.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField2.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField3.Items.Add(_srcFld.SourceFieldName)
        Next

        cboDuplicateTxnRefField1.Items.Insert(0, "")
        cboDuplicateTxnRefField2.Items.Insert(0, "")
        cboDuplicateTxnRefField3.Items.Insert(0, "")


    End Sub

    Private Sub PopulateReferenceFieldCollection(ByVal mapsourcFields As MapSourceFieldCollection)
        If _dataModel Is Nothing Then Exit Sub

        cboDuplicateTxnRefField1.Items.Clear()
        cboDuplicateTxnRefField2.Items.Clear()
        cboDuplicateTxnRefField3.Items.Clear()

        For Each _srcFld As MapSourceField In mapsourcFields

            cboDuplicateTxnRefField1.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField2.Items.Add(_srcFld.SourceFieldName)
            cboDuplicateTxnRefField3.Items.Add(_srcFld.SourceFieldName)
        Next

        cboDuplicateTxnRefField1.Items.Insert(0, "")
        cboDuplicateTxnRefField2.Items.Insert(0, "")
        cboDuplicateTxnRefField3.Items.Insert(0, "")
    End Sub


    Private Function IsDelimitedValueFound(ByVal sCustomerFieldValue As String _
                                , ByVal sDelimiter As String) As Boolean

        Return System.Text.RegularExpressions.Regex.IsMatch(sCustomerFieldValue _
                          , System.Text.RegularExpressions.Regex.Escape(sDelimiter))
    End Function

    Private Function IsEnclosedValueFound(ByVal sCustomerFieldValue As String _
                            , ByVal sEnclosureCharacter As String) As Boolean

        Return System.Text.RegularExpressions.Regex.IsMatch(sCustomerFieldValue _
                    , System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                    & ".*?" & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter))

    End Function

    Private Sub getCustomerFieldNamesAndFieldValues(ByVal filename As String, ByRef sCustomerFieldName As String _
                    , ByRef sCustomerFieldValue As String, ByVal sOutputFileFormat As String)

        Dim counter As Int32 = 0
        Dim sText As String

        sCustomerFieldValue = String.Empty
        sCustomerFieldName = String.Empty

        If Convert.ToInt32(txtHeaderRowNo.Number) <> 0 Then
            counter = 1

            Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
            Dim file As System.IO.StreamReader = New System.IO.StreamReader(filename, encoding)

            Dim _decideNameValue As Int32 = Convert.ToInt32(txtHeaderRowNo.Number).CompareTo(Convert.ToInt32(txtSampleRow.Number))

            While Not file.EndOfStream
                sText = file.ReadLine()
                Select Case _decideNameValue
                    Case -1
                        If counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                        ElseIf counter = CInt(txtSampleRow.Number) Then
                            sCustomerFieldValue = sText
                            Exit While
                        End If
                    Case 0
                        If counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                            sCustomerFieldValue = sText
                            Exit While
                        End If
                    Case 1
                        If counter = Convert.ToInt32(txtSampleRow.Number) Then
                            sCustomerFieldValue = sText
                        ElseIf counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                            Exit While
                        End If
                End Select
                counter = counter + 1
            End While
            file.Close()
        Else
            'Read File
            counter = 1
            Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
            Dim file As System.IO.StreamReader = New System.IO.StreamReader(filename, encoding)

            While Not file.EndOfStream
                sText = file.ReadLine()
                If counter = Convert.ToInt32(txtSampleRow.Number) Then
                    sCustomerFieldValue = sText
                    Exit While
                End If
                counter = counter + 1
            End While
            file.Close()
        End If
    End Sub

    Private Function SeparateNonFixedWidthFile(ByVal filename As String, ByVal names As List(Of String), ByVal values As List(Of String) _
                            , ByVal sOutputFileFormat As String) As Boolean

        Dim sCustomerFieldName As String = String.Empty
        Dim sCustomerFieldValue As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim found As Boolean = False
        Dim counter As Int32

        'get Customer Field Names and Field Values
        getCustomerFieldNamesAndFieldValues(filename, sCustomerFieldName, sCustomerFieldValue, sOutputFileFormat)

        If sCustomerFieldName = String.Empty AndAlso sCustomerFieldValue = String.Empty Then
            Me.Cursor = Cursors.Default
            ''Failed to separate!, please check, if the 'Transaction Starting Row' refers to an existing Row in the Source file.
            MessageBox.Show(MsgReader.GetString("E10000058") _
                            , "Generate Common Transaction Template" _
                            , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        '#1 - Check if there is a value (could be enclosed in either ' or "") delimited by given delimiter
        Dim sDelimiter As String
        Select Case cboDelimiter.Text
            Case "Other"
                sDelimiter = txtOtherDelimiter.Text
            Case "<Tab>"
                sDelimiter = vbTab
            Case "<Space>"
                sDelimiter = " "
            Case Else
                sDelimiter = cboDelimiter.Text
        End Select

        found = IsDelimitedValueFound(sCustomerFieldValue, sDelimiter)

        If Not found Then
            Me.Cursor = Cursors.Default
            '"'Delimiter' is not found."
            MessageBox.Show(MsgReader.GetString("E10000057"), "Generate Common Transaction Template" _
                            , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboDelimiter.Focus()
            Exit Function
        End If

        If cboEnclosureChar.Text <> "None" Then 'Field Names and Values are enclosed

            '#1 - Check if there exists a value enclosed by given enclosure character
            sEnclosureCharacter = IIf(cboEnclosureChar.Text = "Single Quote(')", "'", """")
            found = IsEnclosedValueFound(sCustomerFieldValue, sEnclosureCharacter)

            'If Not found Then
            '    Me.Cursor = Cursors.Default
            '    MessageBox.Show("'Enclosure Character' is not found." _
            '                    , "Generate Common Transaction Template" _
            '                    , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    cboEnclosureChar.Focus()
            '    Return False
            'End If

            '#2 - Extract enclosed field values
            values.AddRange(HelperModule.Split(sCustomerFieldValue, sDelimiter, sEnclosureCharacter, True))

            If chkHeaderAsField.Checked AndAlso Convert.ToInt32(txtHeaderRowNo.Number) <> 0 Then
                '#3 -  Extract enclosed field names  
                names.AddRange(HelperModule.Split(sCustomerFieldName, sDelimiter, sEnclosureCharacter, True))
                HelperModule.FormatMapSourceFields(names)
            Else
                '#3 -  Auto populate (such as field 1, field 2, field 3, ...) as number of fields as values
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If

        Else ' Field names and values are not enclosed but delimited. Field values are delimited and may've optionally been enclosed in ' or " 

            '#2 - Extract field values (could be enclosed in either ' or "") delimited by given delimiter 
            values.AddRange(HelperModule.Split(sCustomerFieldValue, sDelimiter, Nothing, True))

            If chkHeaderAsField.Checked AndAlso Convert.ToInt32(txtHeaderRowNo.Number) <> 0 Then
                '#3 -  Extract field names delimited by given delimiter
                names.AddRange(HelperModule.Split(sCustomerFieldName, sDelimiter, Nothing, True))
                HelperModule.FormatMapSourceFields(names)
            Else
                '#3 -  Auto populate (such as field 1, field 2, field 3, ...) as number of fields as values
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If

        End If

        If names.Count <> values.Count Then Throw New UnequalNumberOfFields(MsgReader.GetString("E10000054"))

        Return True

    End Function

    Private Function SeparateFixedWidthFile(ByVal filename As String, ByVal names As List(Of String) _
            , ByVal values As List(Of String), ByVal delIndices As List(Of Integer)) As Boolean

        Dim sCustomerFieldName As String = String.Empty
        Dim sCustomerFieldValue As String = String.Empty
        Dim sText As String = ""
        Dim counter As Int32 = 0

        If rdoCarriageReturn.Checked = True Then

            counter = 1

            Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
            Dim file As System.IO.StreamReader = New System.IO.StreamReader(filename, encoding)

            Dim _decideNameValue As Int32 = Convert.ToInt32(txtHeaderRowNo.Number).CompareTo(Convert.ToInt32(txtSampleRow.Number))

            While Not file.EndOfStream
                sText = file.ReadLine()
                Select Case _decideNameValue
                    Case -1
                        If counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                        ElseIf counter = CInt(txtSampleRow.Number) Then
                            sCustomerFieldValue = sText
                            Exit While
                        End If
                    Case 0
                        If counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                            sCustomerFieldValue = sText
                            Exit While
                        End If
                    Case 1
                        If counter = Convert.ToInt32(txtSampleRow.Number) Then
                            sCustomerFieldValue = sText
                        ElseIf counter = Convert.ToInt32(txtHeaderRowNo.Number) Then
                            sCustomerFieldName = sText
                            Exit While
                        End If
                End Select
                counter = counter + 1
            End While
            file.Close()

            'update customer fieldname
            If Not IsNothingOrEmptyString(sCustomerFieldName) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldName.Length Then
                        sCustomerFieldName = sCustomerFieldName.Insert(delIndices(indexCounter), ";")
                    End If

                Next
            End If
            'update customer fieldvalue

            If Not IsNothingOrEmptyString(sCustomerFieldValue) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldValue.Length Then
                        sCustomerFieldValue = sCustomerFieldValue.Insert(delIndices(indexCounter), ";")
                    End If

                Next
            End If

            Dim _values() As String = HelperModule.Split(sCustomerFieldValue, ";", Nothing, True)
            If _values IsNot Nothing Then
                For Each value As String In _values
                    values.Add(value)
                Next
            End If


            If chkHeaderAsField.Checked AndAlso Convert.ToInt32(txtHeaderRowNo.Number) <> 0 Then

                names.AddRange(HelperModule.Split(sCustomerFieldName, ";", Nothing, True))
                HelperModule.FormatMapSourceFields(names)

            Else

                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next

            End If

        ElseIf rdoDelimited.Checked Then

            'Read File
            Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
            Dim file As System.IO.StreamReader = New System.IO.StreamReader(filename, encoding)

            sText = file.ReadLine()
            file.Close()

            sCustomerFieldName = sText
            'Start Character Position is falling outside the range of Sample Row
            If Convert.ToInt32(txtStartingChar.Number) > sText.Length Then Throw New MagicException(MsgReader.GetString("E10000055"))
            'Length of Character Transaction is falling outside the range of Sample Row
            If (Convert.ToInt32(txtStartingChar.Number) + Convert.ToInt32(txtLenOfChar.Number)) > sText.Length Then Throw New MagicException(MsgReader.GetString("E10000056"))

            sCustomerFieldValue = sText.Substring(Convert.ToInt32(txtStartingChar.Number) - 1, Convert.ToInt32(txtLenOfChar.Number))


            'update customer fieldname
            If Not IsNothingOrEmptyString(sCustomerFieldName) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldName.Length Then
                        sCustomerFieldName = sCustomerFieldName.Insert(delIndices(indexCounter), ";")
                    End If

                Next
            End If
            'update customer fieldvalue

            If Not IsNothingOrEmptyString(sCustomerFieldValue) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldValue.Length Then
                        sCustomerFieldValue = sCustomerFieldValue.Insert(delIndices(indexCounter), ";")
                    End If

                Next
            End If


            Dim _values() As String = HelperModule.Split(sCustomerFieldValue, ";", Nothing, True)
            If _values IsNot Nothing Then
                For Each value As String In _values
                    values.Add(value)
                Next
            End If


            If chkHeaderAsField.Checked AndAlso Convert.ToInt32(txtStartingChar.Number) > 1 Then

                names.AddRange(HelperModule.Split(sCustomerFieldName, ";", Nothing, True))
                HelperModule.FormatMapSourceFields(names)

            Else
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If

        End If

        If names.Count <> values.Count Then Throw New UnequalNumberOfFields(MsgReader.GetString("E10000054"))

        Return True

    End Function
    'Private Function SeparateNonFixedWidthFileWithRecordTypes(ByVal filename As String, ByVal names As List(Of String), ByVal values As List(Of String) _
    '                            , ByVal sOutputFileFormat As String) As Boolean

    '    Dim sCustomerFieldName As String = String.Empty
    '    Dim sCustomerFieldValue As String = String.Empty
    '    Dim found As Boolean = False
    '    Dim counter As Int32
    '    Dim isPFound As Boolean = False
    '    Dim isWFound As Boolean = False
    '    Dim isIFound As Boolean = False
    '    Dim filestrm As FileStream = Nothing
    '    Dim file As System.IO.StreamReader = Nothing
    '    Dim lineText As String
    '    Dim lineValue As New List(Of String)

    '    '#1 - Check if there is a value (could be enclosed in either ' or "") delimited by given delimiter
    '    Dim sDelimiter As String
    '    Select Case cboDelimiter.Text
    '        Case "Other"
    '            sDelimiter = txtOtherDelimiter.Text
    '        Case "<Tab>"
    '            sDelimiter = vbTab
    '        Case "<Space>"
    '            sDelimiter = " "
    '        Case Else
    '            sDelimiter = cboDelimiter.Text
    '    End Select

    '    ' Step 4 : Get one set of transaction
    '    isPFound = False
    '    isWFound = False
    '    isIFound = False
    '    Dim encoding As System.Text.Encoding = GetFileEncoding(txtSourceFileName.Text)
    '    filestrm = New System.IO.FileStream(txtSourceFileName.Text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
    '    file = New System.IO.StreamReader(filestrm, encoding)

    '    While Not file.EndOfStream
    '        lineText = file.ReadLine()
    '        found = IsDelimitedValueFound(lineText, sDelimiter)
    '        lineValue = New List(Of String)
    '        If Not found Then
    '            Me.Cursor = Cursors.Default
    '            MessageBox.Show(MsgReader.GetString("E10000057"), "Generate Common Transaction Template" _
    '                            , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '            cboDelimiter.Focus()
    '            Return False
    '        End If
    '        If Convert.ToInt32(txtSampleRow.Number) - 1 = counter Then
    '            If CheckRecordTypeOfLine(lineText, PIndicatorTextBox.Text.Trim, sDelimiter, lineValue) Then
    '                If Not isPFound Then
    '                    'Get the name and value
    '                    For counter = 1 To lineValue.Count
    '                        names.Add(String.Format("P-Field{0}", counter))
    '                    Next
    '                    values.AddRange(lineValue)

    '                    isPFound = True
    '                End If
    '            Else
    '                MessageBox.Show("Invalid Sample Row")
    '                Return ""
    '            End If
    '        ElseIf counter > Convert.ToInt32(txtSampleRow.Number) - 1 Then
    '            If Not isPFound Then
    '                MessageBox.Show("Invalid Sample Row")
    '            Else
    '                If Not isWFound Then
    '                    If CheckRecordTypeOfLine(lineText, WIndicatorTextBox.Text.Trim, sDelimiter, lineValue) Then
    '                        'Get the name and value
    '                        For counter = 1 To lineValue.Count
    '                            names.Add(String.Format("W-Field{0}", counter))
    '                        Next
    '                        values.AddRange(lineValue)
    '                        isWFound = True
    '                    End If
    '                ElseIf Not isIFound Then
    '                    If CheckRecordTypeOfLine(lineText, IIndicatorTextBox.Text.Trim, sDelimiter, lineValue) Then
    '                        'Get the name and value
    '                        For counter = 1 To lineValue.Count
    '                            names.Add(String.Format("I-Field{0}", counter))
    '                        Next
    '                        values.AddRange(lineValue)
    '                        isIFound = True
    '                    End If
    '                Else
    '                    If CheckRecordTypeOfLine(lineText, PIndicatorTextBox.Text.Trim, sDelimiter, lineValue) Then
    '                        Exit While
    '                    End If
    '                End If
    '            End If
    '        End If
    '        counter = counter + 1
    '    End While



    '    Return True

    'End Function


    Private Sub PopulateMapBankField()

        Try

            If _dataModel Is Nothing Or bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub

            Dim selMstTmp As MasterTemplateList = bindingSrcOutputTemplate.Current


            Dim _bankField As MapBankField

            Dim _mstTmpUtility As New MasterTemplateSharedFunction

            If selMstTmp.IsFixed Then

                Dim _objTmp As New MasterTemplateFixed()

                _objTmp = _objTmp.LoadFromFile2(String.Format("{0}{1}", _
                   Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, cboOutputTemplate.Text) _
                    , _mstTmpUtility.MasterTemplateFileExtension))

                For Each _masterFixedDetail As MasterTemplateFixedDetail In _objTmp.MasterTemplateFixedDetailCollection

                    _bankField = New MapBankField()
                    _bankField.Position = _masterFixedDetail.Position
                    _bankField.BankField = _masterFixedDetail.FieldName
                    _bankField.Mandatory = _masterFixedDetail.Mandatory
                    '_bankField.CarriageReturn = _masterFixedDetail.CarriageReturn
                    _bankField.Header = _masterFixedDetail.Header
                    _bankField.Detail = _masterFixedDetail.Detail
                    _bankField.Trailer = _masterFixedDetail.Trailer

                    If _masterFixedDetail.MapSeparator Is Nothing Then
                        _bankField.MapSeparator = ""
                    Else
                        _bankField.MapSeparator = _masterFixedDetail.MapSeparator
                    End If

                    If _masterFixedDetail.DataSample Is Nothing Then
                        _bankField.BankSampleValue = ""
                    Else
                        _bankField.BankSampleValue = _masterFixedDetail.DataSample
                    End If

                    _bankField.DataType = _masterFixedDetail.DataType
                    _bankField.DateFormat = _masterFixedDetail.DateFormat
                    _bankField.DataLength = _masterFixedDetail.DataLength
                    _bankField.DefaultValue = _masterFixedDetail.DefaultValue

                    _bankField.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                    _bankField.DecimalSeparator = _masterFixedDetail.DecimalSeparator

                    _dataModel.MapBankFields.Add(_bankField)

                Next

            Else

                Dim _objTmp As New MasterTemplateNonFixed()

                _objTmp = _objTmp.LoadFromFile2(String.Format("{0}{1}", _
                 Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, cboOutputTemplate.Text) _
                  , _mstTmpUtility.MasterTemplateFileExtension))


                For Each _masterNonFixedDetail As MasterTemplateNonFixedDetail In _objTmp.MasterTemplateNonFixedDetailCollection

                    _bankField = New MapBankField()
                    _bankField.Position = _masterNonFixedDetail.Position
                    _bankField.BankField = _masterNonFixedDetail.FieldName
                    _bankField.Mandatory = _masterNonFixedDetail.Mandatory
                    '_bankField.CarriageReturn = _masterNonFixedDetail.CarriageReturn
                    _bankField.Header = _masterNonFixedDetail.Header
                    _bankField.Detail = _masterNonFixedDetail.Detail
                    _bankField.Trailer = _masterNonFixedDetail.Trailer

                    If _masterNonFixedDetail.MapSeparator Is Nothing Then
                        _bankField.MapSeparator = ""
                    Else
                        _bankField.MapSeparator = _masterNonFixedDetail.MapSeparator
                    End If

                    If _masterNonFixedDetail.DataSample Is Nothing Then
                        _bankField.BankSampleValue = ""
                    Else
                        _bankField.BankSampleValue = _masterNonFixedDetail.DataSample
                    End If

                    _bankField.DataType = _masterNonFixedDetail.DataType
                    _bankField.DateFormat = _masterNonFixedDetail.DateFormat
                    _bankField.DataLength = _masterNonFixedDetail.DataLength
                    _bankField.DefaultValue = _masterNonFixedDetail.DefaultValue

                    _dataModel.MapBankFields.Add(_bankField)

                Next

            End If

            'Add bank fields with default values to Translation
            bindingSrcTranslatorSettingCollection.RaiseListChangedEvents = False
            For Each _bankField In _dataModel.MapBankFields

                If _bankField.DefaultValue Is Nothing Or _bankField.DefaultValue = String.Empty Then Continue For

                Dim bnkfieldTranslated As New TranslatorSetting()
                bnkfieldTranslated.BankFieldName = _bankField.BankField
                bnkfieldTranslated.DefaultValues = _bankField.DefaultValue
                _dataModel.TranslatorSettings.Add(bnkfieldTranslated)

            Next

            bindingSrcTranslatorSettingCollection.RaiseListChangedEvents = True
            bindingSrcTranslatorSettingCollection.ResetBindings(False)

            _dataModel.SetDefaultTranslation()



        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK _
                        , MessageBoxIcon.Exclamation)
        End Try

    End Sub

#End Region

#Region " Drag n Drop for Bank Field - Source Field Mapping "

    Private Sub dgvSource_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSource.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvSource.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then dgvBank.DoDragDrop(String.Format("SourceFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvBank_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("SourceFields") Then Exit Sub

        Dim _rowIndexSourceFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtBankFieldsDataGrid As Point = dgvBank.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexBankFieldsDataGrid As Integer = dgvBank.HitTest(_dropOfPointAtBankFieldsDataGrid.X _
                                                        , _dropOfPointAtBankFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvBank_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvBank.DragOver
        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("SourceFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    ' '' ''''''''''''''''''''''Drag-N-Drop from Bank Fields Grid To Map Source Fields Grid ''''''''''''''''
    Private Sub dgvBank_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBank.MouseDown

        Dim rowIndex As Integer
        rowIndex = dgvBank.HitTest(e.X, e.Y).RowIndex

        If rowIndex > -1 Then If rowIndex > -1 Then dgvSource.DoDragDrop(String.Format("BankFields;{0}", rowIndex), DragDropEffects.Copy)

    End Sub

    Private Sub dgvSource_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragDrop

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If Not dragData.StartsWith("BankFields") Then Exit Sub

        Dim _rowIndexBankFieldsDataGrid As Integer = Convert.ToInt32(dragData.Split(";")(1))
        Dim _dropOfPointAtSourceFieldsDataGrid As Point = dgvSource.PointToClient(New Point(e.X, e.Y))
        Dim _rowIndexSourceFieldsDataGrid As Integer = dgvSource.HitTest(_dropOfPointAtSourceFieldsDataGrid.X _
                                                        , _dropOfPointAtSourceFieldsDataGrid.Y).RowIndex

        DoMapping(_rowIndexSourceFieldsDataGrid, _rowIndexBankFieldsDataGrid)

    End Sub

    Private Sub dgvSource_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvSource.DragOver

        Dim dragData As String = e.Data.GetData(Type.GetType("System.String"))
        If dragData.StartsWith("BankFields") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub DoMapping(ByVal _rowIndexSourceFieldsDataGrid As Int32, ByVal _rowIndexBankFieldsDataGrid As Int32)

        If _rowIndexBankFieldsDataGrid = -1 Or _rowIndexSourceFieldsDataGrid = -1 Then Exit Sub

        Dim _objMapSourceField As BTMU.Magic.CommonTemplate.MapSourceField
        Dim _objMapBankField As BTMU.Magic.CommonTemplate.MapBankField

        Try

            If Not _dataModel.MapBankFields Is Nothing Then
                _objMapBankField = _dataModel.MapBankFields(_rowIndexBankFieldsDataGrid)

                If Not _dataModel.MapSourceFields Is Nothing Then
                    _objMapSourceField = _dataModel.MapSourceFields(_rowIndexSourceFieldsDataGrid)
                    Dim cloneSourceField As New BTMU.Magic.CommonTemplate.MapSourceField
                    cloneSourceField.SourceFieldName = _objMapSourceField.SourceFieldName
                    cloneSourceField.SourceFieldValue = _objMapSourceField.SourceFieldValue
                    '#### Need to check if the same mapping exists
                    If _objMapBankField.MappedSourceFields.IsFound(_objMapSourceField) Then
                        '"The Field has already been mapped."
                        MessageBox.Show(MsgReader.GetString("E02000340"), "Generate Common Transaction Template" _
                                               , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ElseIf Not VerifyCrossMapping(cloneSourceField.SourceFieldName, _objMapBankField) Then
                        If getOutputFileFormat() = OMAKASEFormat.OutputFormatString Then
                            MessageBox.Show(MsgReader.GetString("E02010121"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ElseIf getOutputFileFormat() = iFTS2MultiLineFormat.OutputFormatString Then
                            MessageBox.Show(MsgReader.GetString("E02010120"), "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    ElseIf ValidateTypeOfMappedFields(_objMapBankField, cloneSourceField) Then
                        _objMapBankField.MappedSourceFields.Add(cloneSourceField)
                        _objMapBankField.MappingChanged()
                        dgvBank.Refresh()

                        For Each setting As TranslatorSetting In _dataModel.TranslatorSettings
                            If setting.BankFieldName = _objMapBankField.BankField AndAlso HelperModule.IsNothingOrEmptyString(setting.BankValue) AndAlso HelperModule.IsNothingOrEmptyString(setting.CustomerValue) AndAlso setting.BankValueEmpty = False Then
                                setting.SetDefaultCustomerValue(setting.BankFieldName)
                            End If
                        Next

                    End If

                End If

            End If

        Catch ex As Exception

            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error while Mapping : ", ex.Message)

        End Try

    End Sub



#Region " Clear Source Field - Bank Field Mapping "

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim _rowSelected As Boolean = False
        'Is there a MapSource Field selected at the data grid on the left hand side?
        'Is there a MapBank Field selected at the datagrid on the right hand side

        If Not dgvSource.CurrentRow Is Nothing Then

            If dgvSource.CurrentRow.Index <> -1 AndAlso dgvBank.CurrentRow.Index <> -1 Then
                _rowSelected = True

                'There shall exist a Mapping between the MapSourceField and MapBankField
                If _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.IsFound(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index)) Then
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappedSourceFields.Remove(_dataModel.MapSourceFields(dgvSource.CurrentRow.Index))
                    dgvBank.Refresh()
                    _dataModel.MapBankFields(dgvBank.CurrentRow.Index).MappingChanged()
                Else
                    MessageBox.Show("Fields are not mapped to each other.", "Generate Common Transaction Template" _
                                               , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If

        If Not _rowSelected Then
            MessageBox.Show("Please choose the field to be removed.", "Generate Common Transaction Template" _
                                             , MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAll.Click

        'Clears all mappedsourcefields from bankfields
        For Each objMapBankField As BTMU.Magic.CommonTemplate.MapBankField In _dataModel.MapBankFields
            objMapBankField.MappedSourceFields.Clear()
            dgvBank.Refresh()
            objMapBankField.MappingChanged()
        Next

    End Sub

#End Region

#End Region

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        If CanSaveCleanData() Then

            If bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub

            Dim selectedMasterTemplate As MasterTemplateList = bindingSrcOutputTemplate.Current

            Try
                Dim _objPreview As BaseFileFormat
                _objPreview = BaseFileFormat.CreateFileFormat(selectedMasterTemplate.OutputFormat)
                _objPreview.CommonTemplateName = _dataModel.CommonTemplateName
                _objPreview.MasterTemplateName = _dataModel.OutputTemplateName
                _objPreview.SourceFileName = _dataModel.SourceFileName
                _objPreview.TransactionEndRow = 0
                _objPreview.RemoveRowsFromEnd = 0
                _objPreview.EliminatedCharacter = ""
                _objPreview.UseValueDate = False
                _objPreview.ValueDate = Date.Today
                frmPreview.ShowPreview(_dataModel, selectedMasterTemplate, False, Nothing, _objPreview)
                frmPreview.UcCommonTemplatePreview1.btnConsolidate.Visible = False
                frmPreview.UcCommonTemplatePreview1.btnConvert.Visible = False

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

    End Sub

    'Fillup the BankField of TranslationSetting with the default values of BankField
    Private Sub dgvTranslator_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTranslator.EditingControlShowing

        If ((Not (e.Control.GetType() Is GetType(DataGridViewComboEditingControl))) _
            Or (dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value Is Nothing)) Then Exit Sub


        For Each _bkfield As MapBankField In _dataModel.MapBankFields
            If _bkfield.BankField = dgvTranslator.Item(1, dgvTranslator.CurrentRow.Index).Value.ToString() Then

                If (_bkfield.DefaultValue <> String.Empty) Then

                    Dim freak As DataGridViewComboEditingControl = e.Control
                    freak.AddItems(_bkfield.DefaultValue.Split(","), True)  'an empty blank value is also displayed 

                End If

                Exit For
            End If
        Next
    End Sub

    'Query DB and fillup the Lookup Key and Value columns
    Private Sub dgvLookup_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvLookup.EditingControlShowing

        If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupKey" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupValue" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupKey2" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupValue2" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupKey3" _
        And dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name <> "dgvLookupCellCboLookupValue3" _
        Then Exit Sub

        Try

            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue" Then
                If dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    '"Please select the Table first."
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey2" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue2" Then
                If dgvLookup.Item(6, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    '"Please select the Table first."
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey3" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue3" Then
                If dgvLookup.Item(9, dgvLookup.CurrentRow.Index).Value Is Nothing Then
                    '"Please select the Table first."
                    MessageBox.Show(MsgReader.GetString("E10000052"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If


            Dim freak As DataGridViewComboEditingControl = e.Control
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(3, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey2" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue2" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(6, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If
            If dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupKey3" _
            Or dgvLookup.Columns(dgvLookup.CurrentCell.ColumnIndex).Name = "dgvLookupCellCboLookupValue3" Then
                freak.AddItems(frmExcel2DB.GetSampleRow(dgvLookup.Item(9, dgvLookup.CurrentRow.Index).Value.ToString()))
            End If

            'Just in case the First Row of Tablexx has been changed
            'combobox Item Cxx-XXX would not be valid 
            'Work Around: Select the Item based on the Column Name
            If freak.SelectedItemPersistent Is Nothing Then Exit Sub
            For Each item As String In freak.Items
                If item.ToString().StartsWith(freak.SelectedItemPersistent.ToString().Split("-")(0)) Then
                    freak.SelectedItem = item
                    Exit For
                End If
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvStringManipulation_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStringManipulation.DataError
        If e.ColumnIndex = 3 Or e.ColumnIndex = 4 Then
            '"Numeric value is required!", 
            MessageBox.Show(MsgReader.GetString("E10000053"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub tabDetail_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabDetail.SelectedIndexChanged

        'If there is no database file, then disable the look up grid
        If tabDetail.SelectedIndex = 3 Then dgvLookup.Enabled = frmExcel2DB.LookupDatabaseExists()

    End Sub
    Private Function VerifyRecordTypeIndicators() As Boolean
        Dim isTranslated As Boolean = False
        ' Record Type Indicators must be P, W and I
        Select Case getOutputFileFormat()
            Case iFTS2MultiLineFormat.OutputFormatString
                For Each bankfield As MapBankField In _dataModel.MapBankFields
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-P' is not 'P'.Please translate the 'Record Type-P'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If

                            End If
                        End If
                    End If

                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-W", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "W".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-W", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "W".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-W' is not 'W'.Please translate the 'Record Type-W'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-I' is not 'I'.Please translate the 'Record Type-I'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If

                Next

            Case OMAKASEFormat.OutputFormatString
                For Each bankfield As MapBankField In _dataModel.MapBankFields
                    isTranslated = False
                    If bankfield.BankField.Equals("File Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("File Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'File Type-P' is not 'P'.Please translate the 'File Type-P'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                    isTranslated = False
                    If bankfield.BankField.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                        If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                            If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                                'Check if translated to P
                                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                                    If trans.BankFieldName.Equals("Record Type-I", StringComparison.InvariantCultureIgnoreCase) Then
                                        If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                                            isTranslated = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Not isTranslated Then
                                    MessageBox.Show("Bank Field 'Record Type-I' is not 'I'.Please translate the 'Record Type-I'.", "Generate Common Template Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Return False
                                End If
                            End If
                        End If
                    End If
                Next
                'Case iFTS2SingleLineFormat.OutputFormatString
                '    For Each bankfield As MapBankField In _dataModel.MapBankFields
                '        isTranslated = False
                '        If bankfield.BankField.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                '            If IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                MessageBox.Show("Please map the bank field 'Record Type-P'")
                '                Return False
                '            ElseIf Not "P".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                'Check if translated to P
                '                For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                    If trans.BankFieldName.Equals("Record Type-P", StringComparison.InvariantCultureIgnoreCase) Then
                '                        If "P".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                            isTranslated = True
                '                            Exit For
                '                        End If
                '                    End If
                '                Next
                '                If Not isTranslated Then
                '                    MessageBox.Show("'Record Type-P' is not 'P'.Please translate the 'Record Type-P'.")
                '                    Return False
                '                End If
                '            End If
                '        End If
                '    Next
                '    For cnt As Integer = 1 To 3
                '        For Each bankfield As MapBankField In _dataModel.MapBankFields
                '            If bankfield.BankField.Equals(String.Format("W{0}-Record Type-W", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                    If Not "W".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                        'Check if translated to P
                '                        For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                            If trans.BankFieldName.Equals(String.Format("W{0}-Record Type-W", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                                If "W".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                                    isTranslated = True
                '                                    Exit For
                '                                End If
                '                            End If
                '                        Next
                '                        If Not isTranslated Then
                '                            MessageBox.Show(String.Format("W{0}-'Record Type-W' is not 'W'.Please translate the 'W{0}-Record Type-W'", cnt.ToString))
                '                            Return False
                '                        End If
                '                    End If
                '                End If
                '            End If
                '            If bankfield.BankField.Equals(String.Format("I{0}-Record Type-I", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                If Not IsNothingOrEmptyString(bankfield.MappedValues) Then
                '                    If Not "I".Equals(bankfield.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                '                        'Check if translated to P
                '                        For Each trans As TranslatorSetting In _dataModel.TranslatorSettings
                '                            If trans.BankFieldName.Equals(String.Format("I{0}-Record Type-I", cnt.ToString), StringComparison.InvariantCultureIgnoreCase) Then
                '                                If "I".Equals(trans.BankValue, StringComparison.InvariantCultureIgnoreCase) Then
                '                                    isTranslated = True
                '                                    Exit For
                '                                End If
                '                            End If
                '                        Next
                '                        If Not isTranslated Then
                '                            MessageBox.Show(String.Format("I{0}-'Record Type-I' is not 'I'.Please translate the 'I{0}-Record Type-I'", cnt.ToString))
                '                            Return False
                '                        End If
                '                    End If
                '                End If
                '            End If
                '        Next
                '    Next
            Case Else
                Return True
        End Select
        Return True
    End Function
    ''' <summary>
    ''' Determines whether the Mapped fields are of same data type
    ''' </summary>
    ''' <returns>Boolean value indicating whether the mapping is permitted</returns>
    ''' <remarks></remarks>
    Private Function VerifyMapping() As Boolean

        Dim value As String = String.Empty
        Dim errMsg As String = String.Empty
        Dim notReallyUsed As Decimal = 0
        Dim notReallyUsedDate As Date = DateTime.Today

        Dim outputformat As String = getOutputFileFormat()

        Dim pid As String = ""
        Dim wid As String = ""
        Dim iid As String = ""

        Dim obj As CommonTemplateTextDetail = _dataModel
        pid = IIf(IsNothingOrEmptyString(obj.PIndicator), "", obj.PIndicator)
        wid = IIf(IsNothingOrEmptyString(obj.WIndicator), "", obj.WIndicator)
        iid = IIf(IsNothingOrEmptyString(obj.IIndicator), "", obj.IIndicator)

        For Each objBankField As MapBankField In _dataModel.MapBankFields

            ' If iFTS-2 Multi Line format The Record Type Indicators must have been mapped .
            ' Its a 1st Line Requirement to proceed for Tranlating or other Bank Field Manipulation
            If outputformat = iFTS2MultiLineFormat.OutputFormatString Or outputformat = OMAKASEFormat.OutputFormatString Then

                If objBankField.BankField = "Record Type-P" Then
                    If Not pid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                        errMsg = "Bank Field 'Record Type-P' must be Mapped to Correct Record Indicator Field in the Source File!"
                        Exit For
                    End If
                End If

                If objBankField.BankField = "Record Type-I" Then
                    If Not iid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                        errMsg = "Bank Field 'Record Type-I' must be Mapped to Correct Record Indicator Field in the Source File!"
                        Exit For
                    End If
                End If

                If outputformat = iFTS2MultiLineFormat.OutputFormatString Then
                    If objBankField.BankField = "Record Type-W" Then
                        If Not wid.Equals(objBankField.MappedValues, StringComparison.InvariantCultureIgnoreCase) Then
                            errMsg = "Bank Field 'Record Type-W' must be Mapped to Correct Record Indicator Field in the Source File!"
                            Exit For
                        End If
                    End If
                End If

            End If

            If objBankField.DataType Is Nothing Then Continue For

            Select Case objBankField.DataType.ToUpper()
                Case "NUMBER", "CURRENCY"

                    If objBankField.MappedSourceFields.Count > 1 Then If MessageBox.Show(String.Format("BankField: '{0}' is mapped to more than one source fields. Do you want to continue?", objBankField.BankField), "Confirm", MessageBoxButtons.YesNo) = DialogResult.No Then Return False

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty
                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then
                            value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")

                            If Not IsDecimalDataType(value, _dataModel.DecimalSeparator, _dataModel.ThousandSeparator, notReallyUsed) Then
                                errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                            End If
                        End If
                    Next

                Case "DATETIME"

                    For Each objSourceField As MapSourceField In objBankField.MappedSourceFields
                        value = String.Empty

                        If Not IsNothingOrEmptyString(objSourceField.SourceFieldValue) Then
                            value = objSourceField.SourceFieldValue.Replace("""", "").Replace("'", "")
                            If Not Common.HelperModule.IsDateDataType(value, cboDateSep.Text, cboDateType.Text, _dataModel.IsZeroInDate) Then
                                errMsg = "Field type of '" & objBankField.BankField & "' and '" & objSourceField.SourceFieldName & "' do not match."
                            End If
                        End If
                        If cboDateSep.Text <> "No Space" Then If cboDateSep.Text = "<Space>" Then value = value.Replace(" ", "")
                    Next
            End Select
        Next

        If errMsg <> String.Empty Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return (errMsg = String.Empty)

    End Function

    ''' <summary>
    ''' Fills the Editable Settings Grid with the list of Bank Fields that can be edited
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditableTable()

        If dtEditableList Is Nothing Then
            dtEditableList = New DataTable
            dtEditableList.Columns.Add("Field1", GetType(String))
        End If
        dtEditableList.Rows.Clear()

        If dtPaymentBankFields Is Nothing Then
            dtPaymentBankFields = New DataTable
            dtPaymentBankFields.Columns.Add("BankField", GetType(String))
        End If
        dtPaymentBankFields.Rows.Clear()


        Dim currentDetail As CommonTemplateTextDetail = bindingSrcCommonTemplateTextDetailCollection.Current
        Dim currentMaster As MasterTemplateList = bindingSrcOutputTemplate.Current

        If currentDetail IsNot Nothing AndAlso currentMaster IsNot Nothing Then

            '1. Editable Bank Fields
            For Each temp As String In BaseFileFormat.GetEditableFields(currentMaster.OutputFormat)
                dtEditableList.Rows.Add(temp)
            Next

            '2. Lookup and Calculation Bank Fields
            'For iFTS-2 Multiline and OMAKASE output file format
            'Look up and Calculation Settings shall have nothing but "Payment Bank Fields"
            If currentMaster.OutputFormat = iFTS2MultiLineFormat.OutputFormatString _
                          OrElse currentMaster.OutputFormat = OMAKASEFormat.OutputFormatString Then

                Dim _cnt As Int32 = 0

                If currentMaster.OutputFormat = iFTS2MultiLineFormat.OutputFormatString Then
                    If currentDetail.MapBankFields.Count > 14 Then
                        For _cnt = 1 To 13
                            dtPaymentBankFields.Rows.Add(currentDetail.MapBankFields(_cnt).BankField)
                        Next
                    End If

                ElseIf currentMaster.OutputFormat = OMAKASEFormat.OutputFormatString Then
                    If currentDetail.MapBankFields.Count > 34 Then
                        For _cnt = 13 To 42
                            dtPaymentBankFields.Rows.Add(currentDetail.MapBankFields(_cnt).BankField)
                        Next
                    End If
                End If
                bindingSrcLookupBankField.DataSource = dtPaymentBankFields
                dgvLookupCellCmbBankField.DisplayMember = "BankField"
                dgvLookupCellCmbBankField.ValueMember = "BankField"
                dgvLookupCellCmbBankField.DataSource = bindingSrcLookupBankField

                bindingSrcCalculatedFieldsBankField.DataSource = dtPaymentBankFields
                'dgvCalculatedFieldsCellCmbBankField.DataSource = bindingSrcCalculatedFieldsBankField
                dgvCalculatedFieldsCellCmbBankField.DisplayMember = "BankField"
                dgvCalculatedFieldsCellCmbBankField.ValueMember = "BankField"
                dgvCalculatedFieldsCellCmbBankField.DataSource = bindingSrcCalculatedFieldsBankField
            Else
                bindingSrcLookupBankField.DataSource = bindingSrcCommonTemplateTextDetailCollection
                bindingSrcLookupBankField.DataMember = "MapBankFields"
                dgvLookupCellCmbBankField.DisplayMember = "BankField"
                dgvLookupCellCmbBankField.ValueMember = "BankField"
                dgvLookupCellCmbBankField.DataPropertyName = "BankFieldName"
                dgvLookupCellCmbBankField.DataSource = bindingSrcLookupBankField

                bindingSrcCalculatedFieldsBankField.DataSource = bindingSrcCommonTemplateTextDetailCollection
                bindingSrcCalculatedFieldsBankField.DataMember = "MapBankFields"
                dgvCalculatedFieldsCellCmbBankField.DisplayMember = "BankField"
                dgvCalculatedFieldsCellCmbBankField.ValueMember = "BankField"
                dgvCalculatedFieldsCellCmbBankField.DataPropertyName = "BankFieldName"
                dgvCalculatedFieldsCellCmbBankField.DataSource = bindingSrcCalculatedFieldsBankField
            End If

        End If

        bindingSrcEditableSettingsBankField.DataSource = dtEditableList
        dgvTranslatorEditableSettingCellBankField.DisplayMember = "Field1"
        dgvTranslatorEditableSettingCellBankField.ValueMember = "Field1"
        dgvTranslatorEditableSettingCellBankField.DataSource = bindingSrcEditableSettingsBankField



    End Sub

    Private Sub cboOutputTemplate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTemplate.LostFocus
        'Enable Record Type GroupBox only for iFTS-2/Mr.Omakase file formats
        EnableDisableRecordIndicator()
        EnableDisableAdviceRecordSetting()

    End Sub
    Private Sub EnableDisableRecordIndicator()
        If IsFileFormatUsingRecordTypes() Then
            RecordTypeIndicatorGroupBox.Enabled = True
            chkHeaderAsField.Enabled = False
            chkFixedWidth.Enabled = False
        ElseIf isIFTS2SingleLine() Then
            RecordTypeIndicatorGroupBox.Enabled = False
            chkHeaderAsField.Enabled = True
            chkFixedWidth.Enabled = False
        Else
            RecordTypeIndicatorGroupBox.Enabled = False
            chkHeaderAsField.Enabled = True
            chkFixedWidth.Enabled = True
        End If
    End Sub
    '##########################################
    '# Set Enabled=True only for iRTMS format #
    '##########################################
    Private Sub EnableDisableAdviceRecordSetting()
        Dim FileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList

        If bindingSrcOutputTemplate.Current Is Nothing Then Exit Sub
        FileFormat = bindingSrcOutputTemplate.Current
       
        Select Case FileFormat.OutputFormat
            Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM", "OiRTMSCI", "OiRTMSGC", "OiRTMSGD", "OiRTMSRM"
                gbxDetailAdviceSettings.Enabled = True
            Case Else
                gbxDetailAdviceSettings.Enabled = False
        End Select

    End Sub

    Private Function isIFTS2SingleLine() As Boolean
        Dim fileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList
        Dim _isIFTS2SingleLine As Boolean = False

        If bindingSrcOutputTemplate.Current Is Nothing Then Return False
        fileFormat = bindingSrcOutputTemplate.Current
        If BTMU.Magic.Common.HelperModule.IsIFTS2SingleLine(fileFormat.OutputFormat) Then
            _isIFTS2SingleLine = True
        End If
        Return _isIFTS2SingleLine
    End Function
    Private Function IsFileFormatUsingRecordTypes() As Boolean
        Dim fileFormat As BTMU.Magic.MasterTemplate.MasterTemplateList
        Dim _isFormatWithRecordTypes As Boolean = False

        If bindingSrcOutputTemplate.Current Is Nothing Then Return False
        fileFormat = bindingSrcOutputTemplate.Current
        If BTMU.Magic.Common.HelperModule.IsFileFormatUsingRecordTypes(fileFormat.OutputFormat) Then
            _isFormatWithRecordTypes = True
        End If
        Return _isFormatWithRecordTypes
    End Function
    Private Function GetRecordTypeStartIndex(ByVal format As String, ByVal recordType As String) As Integer
        Select Case format
            Case "iFTS-2 MultiLine"
                If recordType = "P" Then Return _dataModel.MapBankFields.IndexOf("Record Type-P")
                If recordType = "W" Then Return _dataModel.MapBankFields.IndexOf("Record Type-W")
                If recordType = "I" Then Return _dataModel.MapBankFields.IndexOf("Record Type-I")
            Case "Mr.Omakase India"
                If recordType = "P" Then Return _dataModel.MapBankFields.IndexOf("File Type-P")
                If recordType = "W" Then Return -1
                If recordType = "I" Then Return _dataModel.MapBankFields.IndexOf("Record Type-I")
        End Select
    End Function

    ''' <summary>
    ''' This function is used only for formats using recordtypes
    ''' Payment SourceFields can only be mapped to Payment Bank Fields
    ''' W SourceFields can only be mapped to W Bank Fields
    ''' I SourceFields can only be mapped to I Bank Fields
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function VerifyCrossMapping(ByVal sourceField As String, ByVal currentMapBankField As MapBankField) As Boolean

        'Limits to validate that Mapping made between Payment Source and Payment Bank Fields and likewise between Invoice and WHTax Fields  
        Dim stIndex As Int32 = 0
        Dim edIndex As Int32 = _dataModel.MapBankFields.Count
        Dim pIndex As Integer = 0
        Dim wIndex As Int32 = 0 ' calculate it. Points the very first WHTax Bank Field
        Dim iIndex As Int32 = 0 ' calculate it . Points the very first Invoice Bank Field 
        Dim masterTemplate As MasterTemplateList
        Dim OutputFormat As String
        Dim currentMapBankFieldIndex As Integer

        masterTemplate = bindingSrcOutputTemplate.Current
        OutputFormat = masterTemplate.OutputFormat

        currentMapBankFieldIndex = _dataModel.MapBankFields.IndexOf(currentMapBankField)
        pIndex = GetRecordTypeStartIndex(OutputFormat, "P")
        wIndex = GetRecordTypeStartIndex(OutputFormat, "W")
        iIndex = GetRecordTypeStartIndex(OutputFormat, "I")
        If sourceField.StartsWith("P-Field") Then
            'P Field cannot be mapped to W or I Field
            If wIndex = -1 Then
                If currentMapBankFieldIndex < pIndex Or currentMapBankFieldIndex >= iIndex Then Return False
            Else
                If currentMapBankFieldIndex < pIndex Or currentMapBankFieldIndex >= wIndex Then Return False
            End If

        ElseIf sourceField.StartsWith("W-Field") Then
            'W Field cannot be mapped to P or I Fields
            If wIndex = -1 Then
            Else
                If currentMapBankFieldIndex < wIndex Or currentMapBankFieldIndex >= iIndex Then Return False
            End If

        ElseIf sourceField.StartsWith("I-Field") Then
            'I Field cannot be mapped to P or W Fields
            If currentMapBankFieldIndex < iIndex Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub dgvEditable_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEditable.DataError
        e.Cancel = True

    End Sub

    Private Sub dgvFilterSettings_CellLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFilterSettings.CellLeave
        Try
            For Each row As RowFilter In _dataModel.Filters
                row.Validate()
            Next
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvStringManipulation_CellLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStringManipulation.CellLeave
        Try
            For Each row As StringManipulation In _dataModel.StringManipulations
                row.Validate()
            Next
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Generate Common Transaction Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
