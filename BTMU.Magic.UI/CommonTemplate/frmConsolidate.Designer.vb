<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsolidate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UcCommonTemplateConlidate1 = New BTMU.Magic.UI.ucCommonTemplateConsolidate
        Me.SuspendLayout()
        '
        'UcCommonTemplateConlidate1
        '
        Me.UcCommonTemplateConlidate1.ContainerForm = Nothing
        Me.UcCommonTemplateConlidate1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcCommonTemplateConlidate1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcCommonTemplateConlidate1.IsNew = False
        Me.UcCommonTemplateConlidate1.Location = New System.Drawing.Point(3, 3)
        Me.UcCommonTemplateConlidate1.Margin = New System.Windows.Forms.Padding(5)
        Me.UcCommonTemplateConlidate1.Name = "UcCommonTemplateConlidate1"
        Me.UcCommonTemplateConlidate1.Padding = New System.Windows.Forms.Padding(3)
        Me.UcCommonTemplateConlidate1.Size = New System.Drawing.Size(843, 349)
        Me.UcCommonTemplateConlidate1.TabIndex = 0
        '
        'frmConsolidate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 355)
        Me.Controls.Add(Me.UcCommonTemplateConlidate1)
        Me.Name = "frmConsolidate"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consolidate Common Transaction Data"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcCommonTemplateConlidate1 As BTMU.Magic.UI.ucCommonTemplateConsolidate
End Class
