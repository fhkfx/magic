<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCommonTemplateConsolidate
    Inherits Magic.UI.ucCommonTemplateTextBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.btnConsolidate = New System.Windows.Forms.Button
        Me.groupBoxConsolidate = New System.Windows.Forms.GroupBox
        Me.panelMain = New System.Windows.Forms.Panel
        Me.groupBoxGroupBy = New System.Windows.Forms.GroupBox
        Me.dgvGroupBy = New System.Windows.Forms.DataGridView
        Me.dgvGroupByCellDeleteBtn = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvGroupByCellFieldCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.groupBoxReferenceFields = New System.Windows.Forms.GroupBox
        Me.dgvReferenceFields = New System.Windows.Forms.DataGridView
        Me.dgvReferenceCellDeleteBtn = New System.Windows.Forms.DataGridViewButtonColumn
        Me.dgvReferenceCellAppendFieldText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvReferenceCellFieldCombo = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.panelBottom = New System.Windows.Forms.Panel
        Me.groupBoxPaymentOptions = New System.Windows.Forms.GroupBox
        Me.rdoNegativePaymentOption = New System.Windows.Forms.RadioButton
        Me.rdoPositivePaymentOption = New System.Windows.Forms.RadioButton
        Me.AdviceRecordGroupBox = New System.Windows.Forms.GroupBox
        Me.chkSumUpRemittanceAmount = New System.Windows.Forms.CheckBox
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBoxConsolidate.SuspendLayout()
        Me.panelMain.SuspendLayout()
        Me.groupBoxGroupBy.SuspendLayout()
        CType(Me.dgvGroupBy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBoxReferenceFields.SuspendLayout()
        CType(Me.dgvReferenceFields, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelBottom.SuspendLayout()
        Me.groupBoxPaymentOptions.SuspendLayout()
        Me.AdviceRecordGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(742, 401)
        Me.DataGridView1.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(377, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 28)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "C&lose"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(5, 6)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 28)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "&New"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(284, 6)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 28)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&Duplicate"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(98, 6)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 28)
        Me.Button4.TabIndex = 1
        Me.Button4.Text = "&View"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(191, 6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(87, 28)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&Edit"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Filter By:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Common Template Name :"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(175, 30)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(340, 20)
        Me.TextBox1.TabIndex = 2
        '
        'btnConsolidate
        '
        Me.btnConsolidate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnConsolidate.Location = New System.Drawing.Point(11, 77)
        Me.btnConsolidate.Name = "btnConsolidate"
        Me.btnConsolidate.Size = New System.Drawing.Size(87, 37)
        Me.btnConsolidate.TabIndex = 3
        Me.btnConsolidate.Text = "Consoli&date"
        Me.btnConsolidate.UseVisualStyleBackColor = True
        '
        'groupBoxConsolidate
        '
        Me.groupBoxConsolidate.Controls.Add(Me.panelMain)
        Me.groupBoxConsolidate.Controls.Add(Me.panelBottom)
        Me.groupBoxConsolidate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxConsolidate.Location = New System.Drawing.Point(3, 3)
        Me.groupBoxConsolidate.Name = "groupBoxConsolidate"
        Me.groupBoxConsolidate.Size = New System.Drawing.Size(792, 387)
        Me.groupBoxConsolidate.TabIndex = 4
        Me.groupBoxConsolidate.TabStop = False
        Me.groupBoxConsolidate.Text = "Consolidate"
        '
        'panelMain
        '
        Me.panelMain.Controls.Add(Me.groupBoxGroupBy)
        Me.panelMain.Controls.Add(Me.groupBoxReferenceFields)
        Me.panelMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelMain.Location = New System.Drawing.Point(3, 16)
        Me.panelMain.Name = "panelMain"
        Me.panelMain.Size = New System.Drawing.Size(786, 246)
        Me.panelMain.TabIndex = 9
        '
        'groupBoxGroupBy
        '
        Me.groupBoxGroupBy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.groupBoxGroupBy.Controls.Add(Me.dgvGroupBy)
        Me.groupBoxGroupBy.Location = New System.Drawing.Point(11, 8)
        Me.groupBoxGroupBy.Name = "groupBoxGroupBy"
        Me.groupBoxGroupBy.Size = New System.Drawing.Size(337, 228)
        Me.groupBoxGroupBy.TabIndex = 5
        Me.groupBoxGroupBy.TabStop = False
        Me.groupBoxGroupBy.Text = "Group By"
        '
        'dgvGroupBy
        '
        Me.dgvGroupBy.AllowUserToDeleteRows = False
        Me.dgvGroupBy.AllowUserToOrderColumns = True
        Me.dgvGroupBy.AllowUserToResizeColumns = False
        Me.dgvGroupBy.AllowUserToResizeRows = False
        Me.dgvGroupBy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGroupBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGroupBy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvGroupByCellDeleteBtn, Me.dgvGroupByCellFieldCombo})
        Me.dgvGroupBy.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvGroupBy.Location = New System.Drawing.Point(5, 19)
        Me.dgvGroupBy.MultiSelect = False
        Me.dgvGroupBy.Name = "dgvGroupBy"
        Me.dgvGroupBy.RowHeadersVisible = False
        Me.dgvGroupBy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvGroupBy.Size = New System.Drawing.Size(327, 203)
        Me.dgvGroupBy.TabIndex = 0
        '
        'dgvGroupByCellDeleteBtn
        '
        Me.dgvGroupByCellDeleteBtn.HeaderText = "X"
        Me.dgvGroupByCellDeleteBtn.Name = "dgvGroupByCellDeleteBtn"
        Me.dgvGroupByCellDeleteBtn.Text = "X"
        Me.dgvGroupByCellDeleteBtn.UseColumnTextForButtonValue = True
        Me.dgvGroupByCellDeleteBtn.Width = 25
        '
        'dgvGroupByCellFieldCombo
        '
        Me.dgvGroupByCellFieldCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvGroupByCellFieldCombo.HeaderText = "Group By"
        Me.dgvGroupByCellFieldCombo.Name = "dgvGroupByCellFieldCombo"
        '
        'groupBoxReferenceFields
        '
        Me.groupBoxReferenceFields.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxReferenceFields.Controls.Add(Me.dgvReferenceFields)
        Me.groupBoxReferenceFields.Location = New System.Drawing.Point(364, 8)
        Me.groupBoxReferenceFields.Name = "groupBoxReferenceFields"
        Me.groupBoxReferenceFields.Size = New System.Drawing.Size(409, 228)
        Me.groupBoxReferenceFields.TabIndex = 6
        Me.groupBoxReferenceFields.TabStop = False
        Me.groupBoxReferenceFields.Text = "Reference Fields"
        '
        'dgvReferenceFields
        '
        Me.dgvReferenceFields.AllowUserToDeleteRows = False
        Me.dgvReferenceFields.AllowUserToOrderColumns = True
        Me.dgvReferenceFields.AllowUserToResizeColumns = False
        Me.dgvReferenceFields.AllowUserToResizeRows = False
        Me.dgvReferenceFields.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvReferenceFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvReferenceFields.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgvReferenceCellDeleteBtn, Me.dgvReferenceCellAppendFieldText, Me.dgvReferenceCellFieldCombo})
        Me.dgvReferenceFields.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvReferenceFields.Location = New System.Drawing.Point(5, 19)
        Me.dgvReferenceFields.MultiSelect = False
        Me.dgvReferenceFields.Name = "dgvReferenceFields"
        Me.dgvReferenceFields.RowHeadersVisible = False
        Me.dgvReferenceFields.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvReferenceFields.Size = New System.Drawing.Size(398, 203)
        Me.dgvReferenceFields.TabIndex = 1
        '
        'dgvReferenceCellDeleteBtn
        '
        Me.dgvReferenceCellDeleteBtn.HeaderText = "X"
        Me.dgvReferenceCellDeleteBtn.Name = "dgvReferenceCellDeleteBtn"
        Me.dgvReferenceCellDeleteBtn.Text = "X"
        Me.dgvReferenceCellDeleteBtn.UseColumnTextForButtonValue = True
        Me.dgvReferenceCellDeleteBtn.Width = 25
        '
        'dgvReferenceCellAppendFieldText
        '
        Me.dgvReferenceCellAppendFieldText.HeaderText = "Append Field"
        Me.dgvReferenceCellAppendFieldText.Name = "dgvReferenceCellAppendFieldText"
        '
        'dgvReferenceCellFieldCombo
        '
        Me.dgvReferenceCellFieldCombo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvReferenceCellFieldCombo.HeaderText = "Reference Fields"
        Me.dgvReferenceCellFieldCombo.Name = "dgvReferenceCellFieldCombo"
        '
        'panelBottom
        '
        Me.panelBottom.Controls.Add(Me.groupBoxPaymentOptions)
        Me.panelBottom.Controls.Add(Me.btnConsolidate)
        Me.panelBottom.Controls.Add(Me.AdviceRecordGroupBox)
        Me.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelBottom.Location = New System.Drawing.Point(3, 262)
        Me.panelBottom.Name = "panelBottom"
        Me.panelBottom.Size = New System.Drawing.Size(786, 122)
        Me.panelBottom.TabIndex = 10
        '
        'groupBoxPaymentOptions
        '
        Me.groupBoxPaymentOptions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.groupBoxPaymentOptions.Controls.Add(Me.rdoNegativePaymentOption)
        Me.groupBoxPaymentOptions.Controls.Add(Me.rdoPositivePaymentOption)
        Me.groupBoxPaymentOptions.Location = New System.Drawing.Point(11, 8)
        Me.groupBoxPaymentOptions.Name = "groupBoxPaymentOptions"
        Me.groupBoxPaymentOptions.Size = New System.Drawing.Size(337, 57)
        Me.groupBoxPaymentOptions.TabIndex = 8
        Me.groupBoxPaymentOptions.TabStop = False
        Me.groupBoxPaymentOptions.Text = "Payment Options"
        Me.groupBoxPaymentOptions.Visible = False
        '
        'rdoNegativePaymentOption
        '
        Me.rdoNegativePaymentOption.AutoSize = True
        Me.rdoNegativePaymentOption.Location = New System.Drawing.Point(171, 27)
        Me.rdoNegativePaymentOption.Name = "rdoNegativePaymentOption"
        Me.rdoNegativePaymentOption.Size = New System.Drawing.Size(134, 18)
        Me.rdoNegativePaymentOption.TabIndex = 1
        Me.rdoNegativePaymentOption.TabStop = True
        Me.rdoNegativePaymentOption.Text = "-ve as Payment Option"
        Me.rdoNegativePaymentOption.UseVisualStyleBackColor = True
        '
        'rdoPositivePaymentOption
        '
        Me.rdoPositivePaymentOption.AutoSize = True
        Me.rdoPositivePaymentOption.Location = New System.Drawing.Point(9, 27)
        Me.rdoPositivePaymentOption.Name = "rdoPositivePaymentOption"
        Me.rdoPositivePaymentOption.Size = New System.Drawing.Size(136, 18)
        Me.rdoPositivePaymentOption.TabIndex = 0
        Me.rdoPositivePaymentOption.TabStop = True
        Me.rdoPositivePaymentOption.Text = "+ve as Payment Option"
        Me.rdoPositivePaymentOption.UseVisualStyleBackColor = True
        '
        'AdviceRecordGroupBox
        '
        Me.AdviceRecordGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AdviceRecordGroupBox.Controls.Add(Me.chkSumUpRemittanceAmount)
        Me.AdviceRecordGroupBox.Location = New System.Drawing.Point(364, 8)
        Me.AdviceRecordGroupBox.Name = "AdviceRecordGroupBox"
        Me.AdviceRecordGroupBox.Size = New System.Drawing.Size(254, 57)
        Me.AdviceRecordGroupBox.TabIndex = 7
        Me.AdviceRecordGroupBox.TabStop = False
        Me.AdviceRecordGroupBox.Text = "Advice Record"
        Me.AdviceRecordGroupBox.Visible = False
        '
        'chkSumUpRemittanceAmount
        '
        Me.chkSumUpRemittanceAmount.AutoSize = True
        Me.chkSumUpRemittanceAmount.Location = New System.Drawing.Point(13, 27)
        Me.chkSumUpRemittanceAmount.Name = "chkSumUpRemittanceAmount"
        Me.chkSumUpRemittanceAmount.Size = New System.Drawing.Size(158, 18)
        Me.chkSumUpRemittanceAmount.TabIndex = 0
        Me.chkSumUpRemittanceAmount.Text = "Sum up Remittance Amount"
        Me.chkSumUpRemittanceAmount.UseVisualStyleBackColor = True
        '
        'ucCommonTemplateConsolidate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.Controls.Add(Me.groupBoxConsolidate)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucCommonTemplateConsolidate"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.Size = New System.Drawing.Size(798, 393)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBoxConsolidate.ResumeLayout(False)
        Me.panelMain.ResumeLayout(False)
        Me.groupBoxGroupBy.ResumeLayout(False)
        CType(Me.dgvGroupBy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBoxReferenceFields.ResumeLayout(False)
        CType(Me.dgvReferenceFields, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelBottom.ResumeLayout(False)
        Me.groupBoxPaymentOptions.ResumeLayout(False)
        Me.groupBoxPaymentOptions.PerformLayout()
        Me.AdviceRecordGroupBox.ResumeLayout(False)
        Me.AdviceRecordGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnConsolidate As System.Windows.Forms.Button
    Friend WithEvents groupBoxConsolidate As System.Windows.Forms.GroupBox
    Friend WithEvents groupBoxGroupBy As System.Windows.Forms.GroupBox
    Friend WithEvents groupBoxPaymentOptions As System.Windows.Forms.GroupBox
    Friend WithEvents AdviceRecordGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents groupBoxReferenceFields As System.Windows.Forms.GroupBox
    Friend WithEvents rdoNegativePaymentOption As System.Windows.Forms.RadioButton
    Friend WithEvents rdoPositivePaymentOption As System.Windows.Forms.RadioButton
    Friend WithEvents chkSumUpRemittanceAmount As System.Windows.Forms.CheckBox
    Friend WithEvents dgvGroupBy As System.Windows.Forms.DataGridView
    Friend WithEvents dgvReferenceFields As System.Windows.Forms.DataGridView
    Friend WithEvents dgvReferenceCellDeleteBtn As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvReferenceCellAppendFieldText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvReferenceCellFieldCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgvGroupByCellDeleteBtn As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvGroupByCellFieldCombo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents panelBottom As System.Windows.Forms.Panel
    Friend WithEvents panelMain As System.Windows.Forms.Panel

End Class
