��    �        Q  �?      0U  -   1U  #   _U  )   �U  %   �U  �   �U    YV     bW  1   zW  0   �W      �W  >   �W  =   =X  �   {X  ;   AY     }Y     �Y     �Y     �Y     �Y     Z     #Z  D   @Z  .   �Z  I   �Z  8   �Z     7[     T[     n[     �[     �[     �[     �[  "   �[     \  #   4\     X\     t\     �\     �\  $   �\  &   �\  ,   ]     C]     `]     {]     �]     �]     �]     �]     �]     ^     "^     <^  
   V^     a^     v^     �^     �^     �^  %   �^     �^  
    _     _     _     <_  +   J_  #   v_     �_  -   �_  %   �_  ]   `  Z   e`  H   �`      	a     *a     Ba  /   Ua  (   �a  .   �a  3   �a  "   b  )   4b     ^b     yb     �b     �b     �b     �b      �b     �b     c     *c     Fc     _c  "   xc  )   �c     �c     �c     �c     �c     d     4d     Kd     ad     vd     �d     �d  "   �d  %   �d  &   	e  !   0e  %   Re  "   xe  #   �e  '   �e      �e     f     %f     :f     Wf     lf     �f  (   �f  $   �f     �f     �f  #   g     0g     Og     ng  #   �g     �g  &   �g  %   �g  ,   h  4   Ch     xh     �h     �h     �h     �h     �h     i  '   i     >i     Zi     xi     �i     �i     �i  "   �i     �i     j  -   j  (   Gj  0   pj  H   �j  	  �j     �k     l     l  �   ,l  �   �l  A   �m  /   �m  0   �m  \   0n  1   �n     �n  C   �n  )   o  -   Bo  0   po  .   �o  J   �o  '   p     Cp     Xp     rp     �p     �p  3   �p  4   �p  -   q  �   Dq  .   �q  .   -r     \r  	   er  	   or     yr  .   �r  :   �r     �r     s     s  !   ;s  *   ]s  1   �s     �s  #   �s  &   �s  "   t  &   At     ht     t     �t     �t  <   �t  0   �t  '   #u     Ku  0   ku  '   �u  6   �u  G   �u  @   Cv  >   �v  +   �v  =   �v     -w     Bw     Rw  "   kw  :   �w     �w     �w     �w      x  /   "x  �   Rx     #y  H   =y  -   �y  ,   �y     �y  
   �y     z  8   z  #   Qz     uz     �z  "   �z  �   �z  E   N{  �   �{  9   *|  ;   d|  �   �|     T}     r}     �}  ;   �}  $   �}     ~     !~     9~     H~  �   h~  �   	     �     �     �     %�     ;�     N�     ^�     p�  %   ��     ��  S   ��  �   	�      ��  G   ��  !   	�  3   +�  <   _�     ��  "   ��  ,   ڂ  %   �  ,   -�  %   Z�  2   ��     ��      σ  (   ��      �  
   :�  *   E�     p�     ��     ��     ��     ф  ,   �     �     *�  
   E�  �   P�  "   ߅     �     �     1�  d   Q�     ��     Ȇ  �   �  E   ��  o   ̇      <�      ]�  7   ~�  '   ��  c   ވ  /   B�  E   r�  ,   ��  )   �  #   �  -   3�  )   a�  �   ��    n�  '   u�  �   ��     0�     9�     R�     [�     t�     ��  1   ��     ȍ  (   ؍  %   �     '�  %   D�     j�     ��     ��  9   ��     �     
�     %�     8�     L�      j�  }  ��     	�     �  /   2�    b�  "   ��     ��     ��     Ȕ     ޔ  *   ��     &�     ;�  J   N�  j   ��  �   �  9   ��  O   ˖  �   �  5   �  .   �  '   N�    v�  �   ��     K�     c�     ~�  !   ��     ��  "   Қ  '   ��  �   �     �  -   1�     _�  �   r�     B�     _�  *   ~�  +   ��     ՝     �     �  %   �     E�  7   _�     ��  r   �  1   ��  =   ��  I   ��  4   D�     y�  #   ��  =   ��     ��  ?   �  D   Z�  1   ��  %   ѡ  5   ��  A   -�  E   o�  H   ��  -   ��  H   ,�     u�  <   ��  )   ͣ  C   ��  =   ;�  .   y�  F   ��  H   �  2   8�  1   k�  9   ��  ;   ץ  %   �  '   9�      a�  2   ��     ��  !   ��  h  ا  *   A�  &   l�  1   ��  #   ŭ  2   �  >   �  �   [�      �  =   .�  0   l�  M   ��  "   �  &   �  3   5�  f   i�  %   а  D  ��  ,   ;�  5   h�     ��     ��  _   ��     �  
   3�  
   >�     I�  
   V�     a�     r�     ~�  %   ��     ��  S   ݴ  5   1�  4   g�     ��     ��     ��     Ե  (   �  #   �     /�     6�  
   ?�     J�     Q�  1   q�     ��  
   ��     ��     ٶ     ��  E   �     X�     v�     ~�     ��     ��     ��     ��     ��     ˷     ٷ     �     �     �     &�     (�     >�     Y�     n�      ��     ��     ĸ  +   �  !   �  '   .�  (   V�  %   �  2   ��  '   ع      �     �     $�     9�     N�     k�  :   ��  >   ��  ?   ��  ;   <�  "   x�     ��     ��  #   ̻     �  5   �     ;�     Q�  ;   d�  <   ��     ݼ     ��     
�      �     &�  A   ;�  &   }�     ��     ��     ѽ  -   �  ;   �  %   J�  (   p�  +   ��     ž     ۾     ��  1   �     H�     _�     e�     |�     ��     ��     ��     Ϳ     ܿ     ��     �     
�     �  3   �     L�     T�     b�     }�     ��     ��  %   ��     ��     ��     �     #�      0�  3   Q�  $   ��  =   ��  %   ��     �      '�     H�     g�     ��     ��     ��  '   ��     ��     �  &   5�  &   \�     ��     ��     ��     ��     ��     ��      �  K   �  "   i�  %   ��  $   ��     ��     ��     ��     �     �     (�     8�     J�     [�     z�     ��     ��  E   ��  @   �  @   E�     ��     ��     ��  !   ��  D   ��  +   �  /   C�     s�     x�     ��     ��     ��  %   ��  %   ��  $   �     ;�     L�     Z�  .   q�  '   ��     ��     ��     ��  3   �     N�     f�     ��     ��     ��     ��  $   ��  &   ��  $   �  '   >�     f�  ,   ��  '   ��     ��     ��  M   ��  N   9�     ��  '   ��  /   ��  "   ��     �      9�      Z�     {�     ��     ��  -   ��  0   �  *   4�  '   _�  #   ��  1   ��  %   ��  .   �  ,   2�  &   _�  "   ��  0   ��  9   ��  8   �  2   M�  "   ��  %   ��  !   ��     ��  >   	�  3   H�  %   |�  (   ��  2   ��     ��  $   �     :�  ;   X�  '   ��  #   ��  $   ��  $   �  +   *�  .   V�  ;   ��     ��  $   ��  7   �  3   >�     r�     ��  $   ��  5   ��  S   �  9   Y�  ,   ��  <   ��     ��  G   �  H   ]�     ��  B   ��  "   ��     �     2�     B�     b�  0   ��  ;   ��  2   ��     !�     #�     2�     7�  	   M�     W�     r�  !   ��     ��     ��     ��     ��     �     	�  '   #�     K�     j�  .   y�     ��  (   ��  )   ��  ,   �     :�     =�     [�  
   i�     t�  $   ��     ��     ��  )   ��      �     $�  %   C�     i�  &   ��  "   ��     ��  4   ��     �     $�  !   4�     V�     t�     ��  %   ��  %   ��     ��     �     �     /�     =�      K�  #   l�     ��     ��  &   ��  )   ��     ��  <   �     L�     b�     }�  ,   ��     ��     ��     ��  ;   �  >   ?�  G   ~�     ��     ��     ��     �     "�     :�     V�     n�  "   v�  -   ��  ,   ��     ��  +   �  *   :�  8   e�  9   ��     ��  !   ��  &   �  $   A�     f�     {�  5   ��     ��     ��     ��     ��  I   ��     )�     9�     P�     c�     ~�     ��  #   ��  #   ��     ��     �  !   �     @�  %   ]�     ��     ��     ��     ��     ��     ��     ��     ��     ��      �     �     �  &   3�     Z�     y�  #   ��     ��     ��  3   ��  %   �  %   8�  =   ^�  =   ��     ��     ��     ��     �     �  %   $�  	   J�     T�     ]�  
   b�     m�  $   ��     ��     ��     ��     ��  &   ��     �  "   5�     X�     l�     u�     ��  V   ��      ��  5   �  $   O�     t�  +   ��  #   ��  4   ��  %   �  
   :�  *   E�  $   p�  0   ��     ��     ��     ��  &   ��  .   �  *   G�  �   r�  :   �  +   >�  E   j�  /   ��  %   ��  A   �  &   H�     o�     ��  ,   ��     ��  (   ��     ��      �  -   �  /   C�  ,   s�  "   ��  )   ��     ��     �      '�     H�     b�     f�     ��  #   ��  -   ��     ��  I   �      ]�     ~�     ��     ��     ��     ��     ��     ��     �     :�     S�     g�     ��     ��     ��  #   ��     ��     ��     �     (�      <�     ]�     p�     ��     ��     ��     ��     ��     ��  6   �  (   8�     a�  #   s�      ��  
   ��  7   ��     ��     �     �     �  -   <�     j�     ��     ��     ��     ��     ��     �     �     -�     0�  D   4�  F   y�  E   ��  D   �  A   K�  7   ��     ��  $   ��  F   �      N�      o�  '   ��      ��     ��  "   ��  #   �  $   7�     \�  /   |�  '   ��  7   ��  $   �     1�     M�  #   f�     ��  !   ��     ��     ��  #   ��  =  �     U�     u�  (   ��  "   ��  m   ��    N�     ^�  $   ~�      ��     ��  ,   ��  *   �  �   7�  E    �     F�     T�     o�     ��     ��     ��     ��  3   ��  %   /�  ;   U�  /   ��     ��     ��     ��          "     9     E     ]     z     �     �     �     �      "       @ &   _    �    � !   � #   �            2    >    Q    g        �    �    �    �    �    �    �     
   $    /    D    ` 0   n (   � %   � )   � %    c   > C   � @   �    '    G    b ,   r %   � %   � )   �      *   6    a    p    x    �    �    � #   �    �     "    &   ?    f    | #   �    �    �    �    � #       8    P    h    �    �     � #   � +   � )   %	 /   O	 +   	 %   �	 '   �	 0   �	 '   *
 !   R
    t
 !   �
    �
 -   �
    �
 #    #   , !   P    r ,   � (   �    �    � "       ;    O !   m    � 1   �    �    �         .    E    \    u %   �    �    �    �            0 "   I    l    � $   � $   � (   � H    �   P    4    D    Q �   n �   � A   � /   � +    a   F :   �    � /       3     P &   q (   � /   � &   �        '    A    [ 
   w    � A   � :   � q       �    �    � 
   � 
   �    � *   � 0       L    a    z     �    � 9   � !    %   4 %   Z    � %   �    �    �    �    	 7        H    i    �     �    � $   � 2   � ,   1 -   ^     � *   �    �    �     .   #    R "   q    �    �     � 4   � �       � <   � *   * 0   U    �    �     � *   � /   �     
   ) )   4 �   ^ A   � �   , <   � >   � �   >    �            :   >  *   y     �     �     �      �  �   ! �   �!    ?"    ["    w"    �"    �"    �"    �"    �" #   �"    ## C   (# k   l#    �# ?   �# "   3$ 1   V$ 0   �$    �$ 3   �$ ?   % $   D% /   i% "   �% 1   �% )   �%    & $   8&    ]& 
   }& #   �&    �&    �&    �&    �&    ' ,   )'    V' )   t'    �' �   �' +   &(    R(    o( "   �( s   �(    )    5) �   S) 5   �) n   * &   �*    �* <   �* #   	+ n   -+    �+ 6   �+ ;   �+ "   /, -   R,    �, "   �, �   �, �   l- '   . l   @. 
   �.    �.    �.    �.    �.    �. $   /    */ '   =/ *   e/ $   �/ *   �/ $   �/ "   0    (0 .   =0    l0    �0    �0    �0    �0    �0 b  �0    S2    h2    �2 �  �2 #   P5    t5    �5    �5    �5    �5 
   �5    �5 9   6 Z   ;6 �   �6 4   !7 W   V7 �   �7 ,   {8 1   �8 &   �8   9 �   
:    �: "   �:    �:    ;     ; )   >;    h; �   �;    f< (   �<    �< �   �<    �= '   �= 1   �= 2   �=    ,> &   I>     p> /   �>    �> 2   �> n   
? i   y? ,   �? 2   @ 8   C@ .   |@ $   �@    �@ =   �@    .A %   LA 6   rA 0   �A !   �A 7   �A 9   4B 7   nB ;   �B -   �B 1   C    BC ;   `C 8   �C 9   �C 8   D +   HD 2   tD 9   �D 3   �D 1   E 4   GE 2   |E "   �E     �E &   �E )   F �   DF    G �   G &   �K %    L    FL "   fL 7   �L L   �L �   M "   �M <   �M 7   !N A   YN '   �N /   �N 0   �N n   $O (   �O 7  �O 2   �Q 1   'R    YR    jR Z   {R    �R    �R    �R    S    S 
   S    "S    +S ,   CS %   pS H   �S )   �S 2   	T    <T    KT 
   XT    cT    tT    �T    �T    �T 
   �T    �T    �T .   �T    U 	    U !   *U "   LU    oU B   �U )   �U    �U    V    V    %V    2V    IV    ZV    kV    zV    �V 
   �V     �V    �V    �V    �V     W    W %   0W #   VW !   zW *   �W %   �W 1   �W 1   X '   QX -   yX    �X    �X    �X    �X    Y !   Y    AY 4   RY 6   �Y 5   �Y 3   �Y /   (Z    XZ    pZ .   �Z #   �Z -   �Z    [    )[ .   ?[ >   n[    �[    �[    �[    �[    �[ <   �[    2\    R\    n\    �\ )   �\ 5   �\    �\ #   ] 3   6]    j]    |]    �] R   �]     ^    ^    "^    6^    N^ 
   Z^ 
   e^    p^ #   �^    �^    �^    �^    �^ 3   �^    �^    �^    _    %_    8_    ?_     L_    m_    t_    �_    �_ !   �_ .   �_     �_ 8    `    Y`    x`    �`    �`    �`    �`    a    a '   8a    `a    xa    �a !   �a    �a    �a '   
b    2b    9b    Fb    _b `   |b $   �b +   c )   .c    Xc    qc    �c    �c    �c    �c    �c    �c    d    /d    Dd    Sd <   bd <   �d @   �d    e    !e    0e    Ae 7   Re *   �e 8   �e    �e    �e    �e    f    4f +   Df #   pf -   �f    �f    �f    �f +   �f !   !g    Cg $   ]g    �g :   �g    �g %   �g    h    -h    <h    Oh    fh    ~h    �h    �h    �h /   �h %   i 	   Bi    Li B   Pi B   �i    �i 0   �i (   j     Aj     bj &   �j    �j    �j %   �j    k     )k *   Jk    uk )   �k &   �k -   �k     l 0   3l ,   dl    �l "   �l )   �l %   �l )   m 0   Im    zm    �m    �m "   �m 9   �m 4   /n &   dn *   �n 2   �n     �n .   
o &   9o 2   `o /   �o (   �o &   �o $   p $   8p &   ]p 7   �p     �p $   �p -   q +   0q    \q !   wq    �q 3   �q Z   �q 0   Fr 2   wr D   �r    �r ;   s ;   Is    �s [   �s '   �s    t    -t !   @t    bt #   �t ,   �t &   �t    �t    �t    
u    u    $u    -u    <u    Ku    `u    zu    �u 
   �u    �u    �u    �u &   �u    �u /   v "   Jv *   mv 3   �v !   �v    �v    �v    w 
   w    %w #   Ew '   iw    �w .   �w    �w    �w 1   x #   5x -   Yx    �x    �x 3   �x    �x    y #   (y &   Ly    sy %   �y )   �y )   �y    	z    z    4z    Ez    Lz #   az +   �z    �z    �z    �z *   �z    { 7   "{    Z{    z{    �{ <   �{    �{    | "   | D   2| 2   w| =   �| !   �|    
}    }     } %   ?} 0   e} +   �}    �}    �} )   �} .   ~    A~ +   P~ %   |~ !   �~ !   �~     �~        ' *   F    q    � 0   �    �    �    �    � W   �    A� !   S�    u� #   ��    �� 
   ɀ    Ԁ    �    �    �    (�    E�    b�    y�    �� 
   ��    �� 
   ��    ��    ��        ց    ؁    ݁    �    � !   "�    D�    Y�    q�    }� -   �� %   �� '   � :   � :   G�    ��    �� 
   ��    ��    ��    ƃ 	   ߃    �    �    ��     �     �    2�    K�    \�    e�     v�    ��    ��        ڄ    ��    �� [   �    j� 1   ��    �� #   ܅ )    � '   *� 3   R�    ��    �� 3   ��    ۆ -   ��    '�    )�    D� !   S� (   u� &   �� w   Ň J   =� ,   �� Y   ��    � 3   +� K   _� #   ��    ω    �    �    � 0   �    M�    S� .   p� @   �� 4   �� 7   � 6   M� 1   �� +   �� *   � )   �    7�    ;�    P�    l� )   ��    �� 5   ό $   �    *�    8�    K�    h�    ��    ��    �� #   č    �    �    �    :�    W�    l�    ��    �� "   ��    �    ��    �    /�    G�    g�    ~�    ��    ��    ��    ȏ A   �    %�    B� !   X� )   z�    �� !   ��    Ԑ 
   ِ 
   �    � &   �    3�    K�    i�    ��    ��    ��    ϑ    �    �    	� :   � :   H� <   �� 7   �� -   �� +   &� #   R� %   v� D   �� 3   � -   � &   C�    j�    �� .   �� '   є '   �� '   !� 5   I� 1   � ?   �� "   � '   �    <�    [�    x� -   ��    ��     ܖ -   ��        �  S              �  Z       �   a   �      B   Y      &    �  �   1   �  �  �  B  �  J    �     F  |      e      �      �  �   �  �  �  �  �  �      c   �       w      �    �  �  �  =  �   �  �   �      �  �  ^          1     �  �      
  �  X   �  *  )   1  g          `  �      �      �  n  �  �   /       �  #          h  �      |  B      �      9  �  �  �  h   �      ;      �   o   h  �       �  �      A  �  �  �  �  �  �   �         f  �   �             c  �      #  8  a  ^  �   �   9  w  �  w  �  �         �   �  �  �    =  �         �  *  3      �          
  �   �  �                D       y      +         �  	   V  l  n  �  {  �  �  &   "   0      <      V  �                  �  -       �            �  �  �   �   {  �   �  �  c      �  J   �  <      �     �  �  ;  N  T      ?  v      2   �  �   �  �      �       g  �   �  !  �   �      ,                  {   (      v  �  �          �  j              �   �           �     �  �   �  )    �      �       ?    �   �      �  �   �      �  �  E  �  �  �  �  z   i  }   �  7      �   |   �             _     �  [      �   �   �  �      L   �     �      �  �   S    �           >      �       #  �   �    �  �  �  �  K   �  �  Y      I  �  t      B  2  �  �  �  W              �   �     W    k      U  �   �      �   �  �  �  F  �  ?   <     �      T      �  U          �  �      L                  �      �  �       �  �       �       �  �       �  �       �  %  �  :  H  �    �   �      �  �         ^  �  �   �    �  �      �  �  �         �   U   �       u   �              R  f  �  A    �       I        =      �  Q  .   �  A   �  �  }  �       Q  C  z     �   >       	  �    �  �      ^  V     ;   C  �  k  $  �  K          L      g   C  �              �      X  �            �   L  �  
  �  "  �  ]  �  3          �        F   ~       5  %  \           �  O   f  �    �  �   �      �               :           �  �      G  �      q          3  p   d       U          �  9  N  �  �    �  �  �   \  �      �  �      �  :      �  �       "  �      �   u  �  �  �    �   �  2     �  K  �  H        c  8   <  �       �  }  �   J      4  �  4       x  G   �  �  �   m   x  v  �  0   �      �  N  �               �          �  l  �  i  @  �  �      _  4  @   �   S  �   b      �   r      Q  )  �  R  �   �  �    J  �   R  �  u  �   �  �   i  R   o    l   @  �  �   �      �      ]              a  �   ,    ,  �  -        s      0  �      y       b           I   �    �  +      G  �      �  �      5  O  X  W   %   �      _      �      *  i   j   D  >       �      N   E  �   �  �  C   �  :  �  e  �     �           �      9   �       '      '  �      8  6  �  Z  M      �  �  �  T       /      �  o  �  �    r  �  (  �   Y  p              �   �      O  �  3       �  �  �  D  .  z  �  ,           �   �  d  �  �  [   �          �  �      X        �  �   =       �  n   �     �  �  h      �  $  �  y      m  v       �      �        j      �   �   �      Z  �     8      �  �  �   �   @  ]   �             �            �          �  !  �  �   �     .  �  m  }  �  a  �        �      w   7       �  k  �      �  �          `  �   M  7      �  G  �   �  �                   u      )  d  �           O  �  �        s      �  �  �  �  �  �      �     #   �    �   �   �     �           j      �    S       _       �     �  P   �   H   M            s   +  \      �  �    �     �  �   �  �      �  �  Q       �  p      �      �     �      �  �  �  �  �  �          �       P        �  t   �           
       �  �  �   \  �  r   �  �  |  �  {  �  o  �  �      t  %      �  �  �  0          �  .                 �  �      6  �  F      l  �         Z    E                      �  $   ?  D      �    z  p  V   �   `  d  b  b  �               �            s        �  �          �   (  �          ~  q          *   �  �  Y   �  '      '      	  y      �     g            n    4      �  H  �  �                  `       �  ]  t          1  r  �  �  I  �  ;          �   &  �  �      �  �  �  �      �  [         �       �  �  �             /  /     �     6   �  e   �          �   P  �      �  �  �  �  q          P    (   5  �  �  �   k   >  �  �  $  �  "  �          q  �   �               �   �  �   �      �  �      	  �  m  �        6  �   !  �  K  7   �      �       �          ~          �  �   �             -  �  �   �  �   �      �           �  �  5       E      �                      �          ~  +   T      �  e      M   x    �  -     W  f   �  �  �       �      !     2  &      �          �    �  �  [  �  �      �        �       x                           A              �  �        
Enter the user ID.  End with an empty line:  
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Key fingerprint =      Subkey fingerprint:    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
    signed by %08lX at %s%s
    signed by %08lX at %s%s%s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  Primary key fingerprint:  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" is not a JPEG file
 "%s" was already locally signed by key %08lX
 "%s" was already signed by key %08lX
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d keys processed (%d validity counts cleared)
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s ...
 %s does not expire at all
 %s encrypted data
 %s encryption will be used
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s signature from: "%s"
 %s signature, digest algorithm %s
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't make lock
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  ASCII armored output forced.
 About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to appoint this key as a designated revoker? (y/N):  Are you sure you want to use it (y/N)?  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Cipher:  Command>  Comment:  Compression:  Create a revocation certificate for this key?  Create a revocation certificate for this signature? (y/N)  Create anyway?  Critical signature notation:  Critical signature policy:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Displaying %s photo ID of size %ld for key 0x%08lX (uid %d)
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Experimental algorithms should not be used!
 Expired signature from " Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid passphrase; please try again Invalid selection.
 Is this correct (y/n)?  Is this okay?  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: Elgamal primary key detected - this may take some time to import
 NOTE: This key is not protected!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Policy:  Primary key fingerprint: Pubkey:  Public key is disabled.
 Quit without saving?  Real name:  Really create the revocation certificates? (y/N)  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really revoke all selected user IDs?  Really revoke this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Really update the preferences?  Reason for revocation: %s
 Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature expired %s
 Signature expires %s
 Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Signature notation:  Signature policy:  Syntax: gpg [options] [files]
Check signatures against known trusted keys
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The use of this algorithm is only supported by GnuPG.  You will not be
able to use this key to communicate with PGP users.  This algorithm is also
very slow, and may not be as secure as the other choices.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key  This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To be revoked by:
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 Unable to open photo "%s": %s
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) Use this key anyway?  User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: appointing a key as a designated revoker cannot be undone!
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: secret key %08lX does not have a simple SK checksum
 WARNING: signature digest conflict in message
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy url (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe enclosing directory ownership on %s "%s"
 WARNING: unsafe enclosing directory permissions on %s "%s"
 WARNING: unsafe ownership on %s "%s"
 WARNING: unsafe permissions on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [expired]  [filename] [revocation] [revoked]  [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid long keyID
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption anonymous recipient; trying secret key %08lX ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %08lX due to an unknown critical bit
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet binary build_packet failed: %s
 c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't get server read FD for the agent
 can't get server write FD for the agent
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open file: %s
 can't open signed data `%s'
 can't open the keyring can't put a policy URL into v3 (PGP 2.x style) signatures
 can't put a policy URL into v3 key (PGP 2.x style) signatures
 can't put notation data into v3 (PGP 2.x style) key signatures
 can't put notation data into v3 (PGP 2.x style) signatures
 can't query password in batchmode
 can't read `%s': %s
 can't search keyserver: %s
 can't set client pid for the agent
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking at depth %d signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 checking created signature failed: %s
 checking keyring `%s'
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 cipher extension "%s" not loaded due to unsafe permissions
 communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID deleting keyblock failed: %s
 delkey delphoto delsig deluid digest algorithm `%s' is read-only in this release
 disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error finding trust record: %s
 error in trailer line
 error reading `%s': %s
 error reading keyblock: %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 error: invalid fingerprint
 error: missing colon
 error: no ownertrust value
 expire export keys export keys to a key server export the ownertrust values external program calls are disabled due to unsafe options file permissions
 failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing digest algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 help iImMqQsS import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid symkey algorithm detected (%d)
 invalid value
 key key %08lX has been created %lu second in future (time warp or clock problem)
 key %08lX has been created %lu seconds in future (time warp or clock problem)
 key %08lX incomplete
 key %08lX marked as ultimately trusted
 key %08lX occurs more than once in the trustdb
 key %08lX: "%s" %d new signatures
 key %08lX: "%s" %d new subkeys
 key %08lX: "%s" %d new user IDs
 key %08lX: "%s" 1 new signature
 key %08lX: "%s" 1 new subkey
 key %08lX: "%s" 1 new user ID
 key %08lX: "%s" not changed
 key %08lX: "%s" revocation certificate added
 key %08lX: "%s" revocation certificate imported
 key %08lX: HKP subkey corruption repaired
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted as trusted key
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: direct key signature added
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid self-signature on user id "%s"
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey binding
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: this is a PGP generated ElGamal key which is NOT secure for signatures!
 key %08lX: unexpected signature class (0x%02X) - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key `%s' not found: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key incomplete
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l line too long
 list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never      never use a MDC for encryption new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition "%s"
 no corresponding public key: %s
 no default secret keyring: %s
 no entropy gathering module detected
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for `%s'
 no secret key
 no secret subkey for public subkey %08lX - ignoring
 no signed data
 no such user id no ultimately trusted keys found
 no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again passphrase too long
 passwd please do a --check-trustdb
 please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 please use "%s%s" instead
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent - disabling agent use
 problem with the agent: agent returns 0x%lx
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key does not match secret key!
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read error: %s
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring requesting key %08lX from %s
 resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke a user ID revoke signatures revsig revuid rounded up to %u bits
 s save save and quit search for keys on a key server searching for "%s" from HKP server %s
 secret key `%s' not found: %s
 secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key locally and non-revocably sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user id "%s"
 sorry, can't do this in batch mode
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 system error while calling external program: %s
 t take the keys from this keyring textmode the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temp files when calling external programs
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to display photo ID!
 unable to execute %s "%s": %s
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown protection algorithm
 unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg 1.2.1
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-05-07 22:57+0900
Last-Translator: IIDA Yosiaki <iida@gnu.org>
Language-Team: Japanese <translation-team-ja@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=EUC-JP
Content-Transfer-Encoding: 8bit
 
�桼����ID�����ϡ����Ԥǽ�λ:  
���θ��ϰ�����������ޤ�����
 
���θ��ϡ����ʤ����դ��Ƹ������ޤ�����
 
���θ����������������Ƥ��ޤ���
 
��ʬ��Ĺ��������������ޤ���OS����ä��𻨤������
�Ǥ���褦���������Ƥ�������! (����%d�Х��Ȥ���ޤ�)
 
���ʤ��Υե���ID�˻Ȥ���������Ƥ���������������JPEG�ե�����Ǥ���ɬ
�פ�����ޤ��������ϸ������Ȥ��ä���˳�Ǽ����롢�Ȥ������Ȥ�ǰƬ�ˤ�
���Ƥ����ޤ��礦���⤷�礭�ʼ̿���Ȥ��ȡ����ʤ��θ���Ʊ�ͤ��礭���ʤ�
�ޤ�! ���餤�ˤ����ޤ��礭���β����ϡ��Ȥ��褤�Ǥ��礦��
 
���ݡ��Ȥ��Ƥ��륢�르�ꥺ��:
 
��̾�ϡ���Ф��ԲĤ����ꤵ��ޤ���
 
��̾�ϼ����ԲĤ����ꤵ��ޤ���
 
���ʽ�̾�ˤʤ�Ǥ��礦��
 
�ٹ�: ��̾�ϡ���Ф��ԲĤ����ꤵ��ޤ���
 
�ٹ�: ��̾�ϡ������ԲĤ����ꤵ��ޤ���
 
���ʤ��θ���Ʊ�ꤹ�뤿��˥桼����ID��ɬ�פǤ���
���Υ��եȤ���̾�������ȡ��Żҥ᡼�롦���ɥ쥹����
���ν񼰤ǥ桼����ID�������ޤ�:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
���Υ桼��������̩���Υ��å���������ˤ�
�ѥ��ե졼��������ޤ�: "        ��̾ "                �ɹ���: %lu              �ѹ��ʤ�: %lu
            ����������: %lu
      �������桼����ID: %lu
              ̤�ɹ���: %lu
        �桼����ID�ʤ�: %lu
       ���ν�̾���ܿͤΤ�Τ��ɤ����ο��Ǥ��ޤ���
       ���ν�̾�Ϥ����餯 ��ʪ �Ǥ���
       ���ν�̾���ܿͤΤ�Τ��ɤ����θ��ڼ��ʤ�����ޤ���
       ��̾����ʪ�ʤ��Ȥ⤢�롢�Ȥ������ȤǤ���
            ��������̾: %lu
  �����λ���:        ��̩�����ɽФ�: %lu
  �����åפ�����������: %lu
                 ���� = �����λ���:    (%d) DSA (��̾�Τ�)
    (%d) DSA��ElGamal (����)
    (%d) ElGamal (�Ź沽�Τ�)
    (%d) ElGamal (��̾�ȰŹ沽)
    (%d) RSA (�Ź沽�Τ�)
    (%d) RSA (��̾�ȰŹ沽)
    (%d) RSA (��̾�Τ�)
    (0) �����ޤ���%s
    (1) �������������Ƥ��ޤ���%s
    (2) ������������ޤ�����%s
    (3) ���ʤ����դ��Ƹ������ޤ�����%s
        ���������μ���: %lu
    %08lX��%s�˼�������Ƥ��ޤ�
    %08lX��%s%s�˽�̾����Ƥ��ޤ�
    %08lX��%s%s%s�˽�̾����Ƥ��ޤ�
   ��̾��ǽ��
        ��̩�����ɹ���: %lu
  %d = ̤��
  %d = ���Ѥ� �ʤ�
  %d = �����˿��Ѥ���
  %d = �������ٿ��Ѥ���
  %d = ����Ū�˿��Ѥ���
  (����)  (�縰ID %08lX)  (��Ф��Բ�)  (�ǥꥱ����)  �縰�λ���:  [ͭ������: %s]  i = ���ܤ��������ɽ������
  m = �᡼�󡦥�˥塼�����
  q = ��λ
  s = ���θ��ϤȤФ�
        ̵�ѹ�����̩��: %lu
  trust: %c/%c "
���ʤ��θ�%08lX��%s������Ū�˽�̾����Ƥ��ޤ�
 "
���ʤ��θ�%08lX��%s�˽�̾����Ƥ��ޤ�
 ��%s�פϡ�JPEG�ե�����ǤϤ���ޤ���
 "%s" �ϸ�%08lX�Ǥ⤦������̾���Ƥ���ޤ�
 "%s" �ϸ�%08lX�Ǥ⤦��̾���Ƥ���ޤ�
 # Sitei sareta sin'you no atai itiran %s
# ("gpg --import-ownertrust" wo tukatte hukkyuu dekimasu)
 %08lX: ���θ��ϼºݤ��ܿͤΤ�Τ��������Ǥ�����
���������������ޤ�
 %08lX: ���θ����������ܿͤΤ�ΤǤ��롢�Ȥ���������������ޤ���
 %08lX: ���θ��Ͽ��ѤǤ��� ����
 ��%08lX: ���ϴ����ڤ�Ǥ�
 ̵���ʽ�̾%d��
 %d�ܤθ������ (����%d�ܤ�ͭ�������򥯥ꥢ)
 ���顼�Τ���%d�Ĥν�̾�򸡺����ޤ���
 �����ʤ�����%d�Ĥν�̾�򸡺����ޤ���
 ͭ���ʼ��ʽ�̾�Τʤ��桼����ID��%d�ĸ���
 %lu�Ĥθ��ޤǸ��� (%lu�Ĥν�̾)
 ����ޤǤ�%lu�Ĥθ��ޤǸ��� (%lu�Ĥν�̾)
 %lu���ޤǽ���
 %s ...
 %s��̵���¤Ǥ�
 %s�Ź沽�Ѥߥǡ���
 %s�Ź沽����Ѥ��ޤ�
 %s��%s�ˤƴ����ڤ�ˤʤ�ޤ�
 %s�ϡ�ͭ����ʸ������ǤϤ���ޤ���
 %s�Ͽ��������Ǥ�
 %s���ѹ��Τʤ����Ǥ�
 %s��%s�ȤȤ���Ѥ��Ƥ�̵��̣�Ǥ�!
 %s��%s�ȤȤ���Ѥ��뤳�ȤϤǤ��ޤ���!
 %s��̾����̾��: "%s"
 %s��̾�����󥢥르�ꥺ�� %s
 %s%c %4u%c/%08lX  ����: %s ����: %s %s.
 %s/%s�Ź沽 ������: "%s"
 %s: �ٹ�: ���Υե�����Ǥ�
 %s: ���������Ǥ��ޤ���: %s
 %s: �ǥ��쥯�ȥ꡼���Ǥ��ޤ���: %s
 %s: ���å����Ǥ��ޤ���
 %s: �����Ǥ��ޤ���: %s
 %s: ���å����Ǥ��ޤ���
 %s: �����ޤ���: %s
 %s: �ǥ��쥯�ȥ꡼���Ǥ��ޤ���
 %s: �ǥ��쥯�ȥ꡼������ޤ���!
 %s: �����쥳���ɤ��ɽФ����顼: %s
 %s: �С�����󡦥쥳���ɤ��ɽФ����顼: %s
 %s: �С�����󡦥쥳���ɤι������顼: %s
 %s: �ǥ��쥯�ȥ꡼���쥳���ɤν���ߥ��顼: %s
 %s: �С�����󡦥쥳���ɤν���ߥ��顼: %s
 %s: �쥳���ɤ��ɲä˼��Ԥ��ޤ���: %s
 %s: �ϥå���ɽ�κ����˼��Ԥ��ޤ���: %s
 %s: �С�����󡦥쥳���ɤκ����˼��Ԥ��ޤ���: %s %s: �쥳���ɤν�����˼��Ԥ��ޤ���: %s
 %s: ̵���ʥե����롦�С������%d
 %s: ̵���ʿ��ѥǡ����١���
 %s: ̵���ʿ��ѥǡ����١��������
 %s: ���ؤ��Ǥ��ޤ���
 %s: ���ѥǡ����١������ե�����ǤϤ���ޤ���
 %s: �����å�: %s
 %s: �����å�: �������ϴ��ˤ���ޤ�
 %s: �����å�: �������ϻ��ѶػߤǤ�
 %s: ���ѥǡ����١������Ǥ��ޤ���
 %s: ̤�Τγ�ĥ��
 %s: �쥳�����ֹ�%lu�֤ΥС�����󡦥쥳����
 %s:%d: ����뤵��Ƥ��륪�ץ�����%s��
 %s:%d: ̵���ʽ�Ф����ץ����
 %s:%d: ̵�����ɹ��ߥ��ץ����
 %u�ӥå�%s��, ID %08lX�������դ�%s (�����Ϥ���ޤ���)
 (�����ǤϤ��ä�%d�����Ӥޤ�)
 (����ϡ��ǥꥱ���Ȥʼ������Ǥ�)
 (���뤤�ϡ�����Ǹ������)
 (������Ū�ˤϸ��ä��ץ��������Ѥ����ΤǤ��礦)
 --clearsign [�ե�����̾] --decrypt [�ե�����̾] --edit-key �桼����id [���ޥ��] --encrypt [�ե�����̾] --lsign-key �桼����id --nrlsign-key �桼����id --nrsign-key �桼����id ���Υ��ޥ�ɤ�--output�ϵ�ǽ���ޤ���
 --sign --encrypt [�ե�����̾] --sign --symmetric [�ե�����̾] --sign [�ե�����̾] --sign-key �桼����id --store [�ե�����̾] --symmetric [�ե�����̾] -k[v][v][v][c] [�桼����id] [����] ... �Х��Ǥ� (%s:%d:%s)
 ̵���ʽ�̾1��
 �����ʤ�����1�Ĥν�̾�򸡺����ޤ���
 ���顼�Τ���1�Ĥν�̾�򸡺����ޤ���
 ͭ���ʼ��ʽ�̾�Τʤ��桼����ID��1�ĸ���
 @
(���ޥ�ɤȥ��ץ���������ΰ����ϡ�
�ޥ˥奢�롦�ڡ���������������)
 @
��:

 -se -r Bob [�ե�����]      ��̾�ȥ桼����Bob�ؤΰŹ沽
 --clearsign [�ե�����]     ���ꥢ��̾�����
 --detach-sign [�ե�����]   ʬΥ��̾�����
 --list-keys [̾��]         ����ɽ��
 --fingerprint [̾��]       �����ɽ��
 @
���ץ����:
  @���ޥ��:
  ASCII�������Ϥ������ޤ���
 ������%s���Ф��������ޤ���
             �Ǿ��θ�Ĺ��  768 �ӥå�
             ����θ�Ĺ�� 1024 �ӥå�
         ����ο侩��Ĺ�� 2048 �ӥå�
 �����θ���RFC2440���������Ƥ��ޤ������侩���ޤ���
�ʤ��ʤ顢�����򥵥ݡ��Ȥ��Ƥ��ʤ��ץ�����ब���ꡢ
����������̾������Ĺ���ơ����ڤˤ���֤������뤿��Ǥ��� ���������������硢��yes��(�ޤ��ϡ�ñ�ˡ�y��) �������Ƥ��������� �����������������硢��yes�פ������Ƥ������� ��񤭤��Ƥ褱��С���yes�פ������Ƥ������� �����Υ桼����ID�������˺����������С���yes�פ������Ƥ���������
�����������Ʊ���˺�����ޤ�! ���Ƥ� �桼����ID�˽�̾��������С���yes�פ������Ƥ������� ��yes�פ���no�פ������Ƥ������� �����ˤ��θ��ˤ��ʤ��θ��ǽ�̾���Ƥ褤�Ǥ���: " ���θ�Ĺ�������ˤ����Ǥ���?  ����Ǥ��ɲä������Ǥ���? (y/N)  ����Ǥ������˼����������Ǥ���? (y/N)  ����Ǥ⤳�θ��˽�̾�������Ǥ���? (y/N)  �����ˤ��θ����̾�����Ԥ����ꤷ�ޤ���? (y/N):  ����Ǥ������˻��Ѥ������Ǥ��� (y/N)?  ������ ��̾: " CRC���顼��%06lx - %06lx
 ��̾�򸡺��Ǥ��ޤ���: %s
 ���θ����Խ��Ǥ��ޤ���: %s
 ����󥻥� ����Ū�˿��Ѥ������ؤξ�����:
 ̾��(N)��������(C)���Żҥ᡼��(E)���ѹ����ޤ���OK(O)����λ(Q)?  ̾��(N)��������(C)���Żҥ᡼��(E)���ѹ����ޤ��Ͻ�λ(Q)?  ���� (�ޤ������򤷤�) �桼����ID�������򡢸��ߤ������������ѹ�
���ޤ����ط����뼫�ʽ�̾�������ϡ�1�ä����ߤޤ���
 ������ͭ�����¤��ѹ����ޤ���
 �縰��ͭ�����¤��ѹ����ޤ���
 �Ź�:  ���ޥ��>  ������:  ����:  ���ν�̾�ˤ������뼺�����������ޤ���?  ���ν�̾�ˤ������뼺�����������ޤ���? (y/N)  ����Ǥ���ޤ���?  ����ƥ�����ʽ�̾����:  ����ƥ�����ʽ�̾�ݥꥷ��:  DSA���Ф�1024�ӥåȤˤʤ�ޤ���
 DSA�θ�Ĺ��512����1024�ޤǤǤ�
 DSA�Ǥ�160�ӥåȤΥϥå��塦���르�ꥺ��λ��Ѥ�ɬ�פǤ�
 �ե�����ޤ���ɸ�����Ϥ�
�������� ������������̾�������ޤ���? (y/N/q) ����̵���ʽ�̾�������ޤ���? (y/N/q) ���θ����ؤ��������ޤ���?  ����̤�Τν�̾�������ޤ���? (y/N/q) %d�Ĥν�̾�������ޤ�����
 %d�Ĥν�̾�������ޤ�����
 ʬΥ��̾��
 ����:  %s�򥵥���%ld�θ�0x%08lX (uid %d) �Υե���ID�Ȥ���ɽ��
 ���򤷤����������˺�����ޤ���?  ���θ��������˺�����ޤ���?  �����˼¹Ԥ��ޤ���?  ���򤷤����������˼������ޤ���?  ���θ��������˼������ޤ���?  �����ˤ��θ�������Ū�˿��Ѥ��ޤ���?  �����ڤ��̾�򿷤�����̾�ȸ򴹤������Ǥ���? (y/N)  ��Ф���ǽ�ʽ�̾�˳ʾ夲�������Ǥ���? (y/N)  OpenPGP�μ��ʽ�̾�˳ʾ夲�������Ǥ���? (y/N)  ����Ǥ��̾�������Ǥ���? (y/N)  Ʊ���˽�̾������ڤ�ˤ������Ǥ���? (Y/n)  �ե���ID��ɽ�����ʤ� �Żҥ᡼�롦���ɥ쥹:  �ե�����ޤ���ɸ�����Ϥ������� �ե���ID�Ѥ�JPEG�ե�����̾�����Ϥ��Ƥ�������:  ͽ�������������ϡ����Ԥǽ�λ:
 �������ե�����̾�����Ϥ��Ƥ������� �ѥ��ե졼��������
 �ѥ��ե졼��������:  ���ۥ����̾�������Ϥ��Ƥ������� ������̩���ο������ѥ��ե졼�������Ϥ��Ƥ���������

 �ץ���ץȤ˼����񼰤��ͤ����Ϥ��Ƥ���������
ISO�����դν� (YYYY-MM-DD) �Ǥ����ϤǤ��ޤ��������������顼��
ɽ������ʤ��Ǥ��礦��������ꡢ�����ƥ�������ͤ���֤��Ѵ�����
�褦�˻�ߤޤ��� ����Ĺ�������Ϥ��Ƥ������� ��å�������������ꥢ�ɥ쥹�Υ桼����ID�����Ϥ��Ƥ��������� ��̾�����ԤΥ桼����ID�����Ϥ��Ƥ�������:  �¸���Υ��르�ꥺ��ϻ��Ѥ��٤��ǤϤ���ޤ���!
 �����ڤ�ν�̾: " ��ǽ:  �ե������%s�פϴ���¸�ߤ��ޤ��� ��̾��Ԥ��ե������̾������ꤷ�Ƥ������� ���Ϥ��ޤ�����å������򥿥��פ��Ƥ������� ...
 ��������̾: " �ϥå���:  ����: �ޤ���̾����桼����ID�����򤷤ޤ�
 ��̾���褦�Ȥ��Ƥ��븰���ºݤ˾嵭��̾���οͤΤ�Τ��ɤ������ɤ�����
���դ��Ƹ��ڤ��ޤ�����? �����狼��ʤ���С���0�פ����Ϥ��Ƥ���������
 IDEA�Ź��������ǽ�ʤΤǡ���ŷŪ�Ǥ���%s�����Ѥ��褦�Ȥ��Ƥ��ޤ�
 �⤷������������С��ʤ������������ȯ�Ԥ���Τ������������
�����Ȥ����Ϥ��뤳�Ȥ��Ǥ��ޤ����ƥ����Ȥϴʷ�ˤ��Ƥ���������
���Ԥǽ����ˤʤ�ޤ���
 ���μ������줿����Ȥ������ʤ���С���no�פ������Ƥ��������� ���ο��ѤǤ��ʤ�����Ȥ������ʤ���С���no�פ������Ƥ��������� ���̤ˡ���̾�ȰŹ��Ʊ������Ȥ��Τϡ������������ޤ���
���Υ��르�ꥺ��ϡ�������ϰ�������ǻȤ���٤��Ǥ���
�ޤ����������ƥ���������Ȥˤ����̤��������� �����Ȥ�̵����ʸ��������ޤ�
 ̾����̵����ʸ��������ޤ�
 ̵���ʥ��ޥ�� (��help�פ򻲾�)
 --allow-non-selfsigned-uid��ͭ���ˤ��줿̵���ʸ�%08lX�Ǥ�
 ̵���ʥѥ��ե졼���Ǥ��������Ϥ��Ƥ������� ̵��������Ǥ���
 ����Ǥ����Ǥ��� (y/n)?  ��������Ǥ���?  ���μ̿����������Ǥ��� (y/N/q)?  ���θ��ϡ����Υ桼����ID��ʤΤ��ܿͤΤ�Τ��ɤ����ο��Ǥ���
���󡣺�����Ԥ����Ȥ� *�μ¤�* ���򤷤Ƥ��ʤ����ˤϡ�
���μ���ˤ�no�������Ƥ���������

 �����ͤ�����ϡ����ʤ�����Ǥ��������ͤϡ��軰�Ԥ˷褷
���󶡤���ޤ��󡣤���ϡ�web-of-trust �μ�����ɬ�פǡ�
(����Ū�ˤǤ���) web-of-certificates �Ȥ�̵�ط��Ǥ��� ������������ߤ���ޤ�����
 ���������˼��Ԥ��ޤ���: %s
 �����ѥ����ޤ��� ���Ϥ⤦�Ȥ��Ƥ��ޤ��� �����ݸ��Ƥ��ޤ���
 ���ϡ���������Ƥ��ޤ��� �����Ȥ꤫��äƤ��ޤ� ����ͭ�����֤�? (0) ����̵�ѹ��ʤΤǹ����Ϥ���ޤ���
 ���� 2048����礭�ʸ�Ĺ�ϡ��׻����֤� ���� Ĺ���ʤ�Τ�
�侩���ޤ���!
 N  ̾�����ѹ���
C  �����Ȥ��ѹ���
E  �Żҥ᡼��Υ��ɥ쥹���ѹ���
O  ����������³�ԡ�
Q  ������������ߡ� ����: ����%s���Ѥ��ޤ���!
 ����: ElGamal�μ縰�򸡽� - �����ˤ����ɹ��ߤ˻��֤�������ޤ�
 ����: ���θ����ݸ��Ƥ��ޤ���!
 ����: �Ź楢�르�ꥺ��%d��ͥ�������äƤ��ޤ���
 ����: v3���������κ����ϡ�OpenPGP��Ŭ�礷�ޤ���
 ����: ���ϼ����ѤߤǤ� ����: ����Υ��ץ���󡦥ե������%s�פ�����ޤ���
 ����: �Ρ�������ä����ץ���󡦥ե������%s�פϡ�̵�뤵��ޤ�
 ����: ��̩��%08lX��%s�Ǵ����ڤ�Ǥ�
 ����: �����Ԥϡ����ˤ���פ褦�˵��Ƥ��ޤ�
 ����: ��̾��%08lX�ϴ����ڤ�Ǥ�%s
 ����: ñ���S2K�⡼��(0)�λ��Ѥˤ϶���ȿ�Ф��ޤ�
 �ٹ�: ���ѥǡ����١��������������ǽ�Ǥ�
 ̾��������ǻϤ�ƤϤ����ޤ���
 ̾����5ʸ���ʾ�Ǥʤ���Фʤ�ޤ���
 ���μ¹Ԥˤ���̩��������ޤ���
 NnCcEeOoQq ��̩���ؤ��б������̾������ޤ���
 �إ�פϤ���ޤ��� ��%s�פΥإ�פϤ���ޤ��� ��ͳ�ϻ��ꤵ��Ƥ��ޤ��� %d�֤������Ϥ���ޤ���
 ���Υ桼����ID�Ϥ���ޤ���
 �����٤����ꤵ��Ƥ��ޤ���:
%4u%c/%08lX %s " %d�֤Υ桼����ID�Ϥ���ޤ���
 ͭ�����Żҥ᡼�롦���ɥ쥹�ǤϤ���ޤ���
 ����:  ���θ��ϰŹ沽�ˤϻ��ѤǤ��ʤ����Ȥ����դ��Ƥ����������Ź沽��Ԥ��ˤϡ�
��--edit-key�ץ��ޥ�ɤ��Ѥ����������������Ƥ���������
 ����: ���θ��ϻ��Ѷػߤ����ꤵ��Ƥ��ޤ���
 ����: ���θ��ϴ����ڤ�Ǥ�!
 ���������Ƥ��ޤ���
 ��%08lX�ǽ�̾�����ΤϤ���ޤ���
 �狼��ޤ����������������ʤ��Υ�˥����䥭���ܡ������Ϥϡ�
����ˤ��������ȼ�Ǥ��뤳�Ȥ򿴤�α��Ƥ����Ƥ�������!
 ��񤭤��ޤ��� (y/N)?  �ޤ����顼�������Ƥ�������
 ¾�Υ桼�����θ������������ڤ��뤿��ˡ����Υ桼�����ο����٤����
��������(�ѥ��ݡ��Ȥ򸫤��Ƥ��ä��ꡢ¾������������򸡺�������...)?

 �Żҥ᡼��Υ��ɥ쥹����̾�䥳���Ȥ�����ʤ��褦��
 �������ե�����̾�����Ϥ��Ƥ���������ñ�˥꥿���󡦥����򲡤��ȡ�
(��̤Ǽ�����) ����Υե�����̾����Ѥ��ޤ��� ���ץ����Υ����Ȥ����Ϥ��Ƥ������� �ǡ������ե������̾��������:  �ѥ��ե졼�������Ϥ��Ƥ����������������̩��ʸ�ϤΤ��ȤǤ� 
 ���ΰ�����η�٤������Ƥ�������
 �ץ�������Ƶ�ư����ޤǡ�ɽ�����줿����ͭ�������������ʤ����⤷��ʤ���
�Ȥ������Ȥ�ǰƬ���֤��Ƥ���������
 ��̩���������Ȥ��Ƥ���������
 ���Ϥ����ѥ��ե졼���γ�ǧ�Τ��ᡢ�����Ϥ��Ƥ��������� �Х��򸫤Ĥ����� <gnupg-bugs@gnu.org> �ޤǤ���𤯤�������
 �⡹1�Ĥ����������򤷤Ƥ���������
 �桼����ID�򤭤ä���ҤȤ����򤷤Ƥ���������
 ��������ͳ�����򤷤Ƥ�������:
 �����ʸ��μ�������򤷤Ƥ�������:
 ����ͭ�����¤���Ƥ���������
         0 = ̵����
      <n>  = ͭ������ n ����
      <n>w = ͭ������ n ����
      <n>m = ͭ������ n �����
      <n>y = ͭ������ n ǯ��
 ��̾��ͭ�����¤���Ƥ���������
         0 = ̵����
      <n>  = ͭ������ n ����
      <n>w = ͭ������ n ����
      <n>m = ͭ������ n �����
      <n>y = ͭ������ n ǯ��
 �ޤ���toggle�ץ��ޥ�ɤ�ȤäƤ�������
 �𻨤���������Ƥ��ޤ��Τǡ����Ԥ������������𻨤��μ�������
���ޤ��Τǡ��⤷˰�����鲿����Ȥ��Ƥ���������
 �ݥꥷ��:  �縰�λ���: ������:  �������ϻ��ѶػߤǤ���
 ��¸�����˽�λ���ޤ���?  ��̾:  ����������������˺��ޤ���? (y/N)  �����˺��ޤ���?  ���μ��ʽ�̾�������˺�����ޤ���? (y/N) ���򤷤����桼����ID�������˺�����ޤ���?  ���Υ桼����ID�������˺�����ޤ���?  ���򤷤����桼����ID�������˼������ޤ���?  ���Υ桼����ID�������˼������ޤ���?  ���������桼����ID�˽�̾���ޤ���?  �����˽�̾���ޤ���?  ���򤷤��桼����ID�������������˹������ޤ���?  �����������˹������ޤ���?  ������ͳ: %s
 �ѥ��ե졼���������
 �ѥ��ե졼���������:  �׵ᤵ�줿��Ĺ��%u�ӥå�
 ����������������
 �����������������ޤ�����

���Ĥ���ʤ��褦�����Τ˰�ư���Ƥ����������⤷��뤬���ξ�����ؤΥ�
������������ȡ������Ĥ����ʤ��ξ������Ȥ��ʤ����뤳�Ȥ��Ǥ��ޤ���
���Τ��ɽФ���ǽ�ˤʤä����������ơ����ξ��������������ݴɤ����
�������Ǥ��������������դ��Ƥ������������ʤ��Υޥ���ΰ��������ƥ�ϡ�
����Ǥ⸫������˥ǡ����򤪤����Ȥ�����ޤ�!
 �ѹ�����¸���ޤ���?  ��̩�������ѤǤ��ޤ���
 �縰����̩��ʬ�������ޤ���
 ���Ѥ��륢�르�ꥺ������򤷤Ƥ���������

DSA (��̾DSS) �ϡ���̾�ˤΤ��Ѥ��뤳�Ȥ��Ǥ����Żҽ�̾���르��
����Ǥ���DSA��̾��ElGamalˡ�����®�˸��ڤǤ���Τǡ������
�侩���륢�르�ꥺ��Ǥ���

ElGamal�ϡ���̾�ȰŹ沽���Ѥ��뤳�Ȥ��Ǥ��륢�르�ꥺ��Ǥ���
OpenPGP�Ǥϡ����Υ��르�ꥺ��ΡְŹ沽�Τߡפȡֽ�̾+�Ź沽��
��2�Ĥ���ˡ����̤��Ƥ��ޤ������ºݤˤ�Ʊ���Ǥ�������������̾��
�ΰ����ʸ�����ˤϰ������ѿ������̤���ˡ�����򤷤ʤ���Фʤ�
�ޤ��󡣤��Υץ������ǤϤ��줬��ǽ�Ǥ�����¾��OpenPGP�μ�����
�ϡ��ֽ�̾+�Ź沽�פ���ˡ���ᤷ�ʤ����⤷��ޤ���

�ǽ�θ�(�縰)�ϡ���̾�˻��ѤǤ��븰�Ǥʤ���Фʤ�ޤ��󡣤��줬
���Υ�˥塼�ˡְŹ沽�Τߡפ�ElGamal���Τʤ���ͳ�Ǥ��� �ե���ID��������륳�ޥ�ɹ�
������ �ե���ID��ɽ�� �����ڤ�ν�̾ %s
 ���ν�̾��%s�Ǵ����ڤ�Ǥ�
 ��̾��ͭ�����֤�? (0) %.*s ��%s��ID %08lX�ˤ���̾
 ��̾����:  ��̾�ݥꥷ��:  ��ʸ: gpg [���ץ����] [�ե�����]
���Ѥ������ǽ�̾�򸡺�
 ��: gpg [���ץ����] [�ե�����]
��̾���������Ź沽������
�����ư��ϡ����ϥǡ����˰�¸
 ��ܤ���Ƥ�����������Ҥϡ��Ź��ѤȤ��ƤϤ����ޤĤǡ�
����������Ǥ��ޤ���!

���Υץ����������������ǡ�������ڻ��Ѥ��ƤϤ����ޤ���!!

 "%s" �ˤ������뼫�ʽ�̾�ϡ�
PGP 2.x�����ν�̾�Ǥ���
 ���ν�̾��ͭ���ǤϤ���ޤ��󡣤��Τ��Ȥϡ����ʤ��θ��ؤ��������٤���
�Ȥ�����̣�Ǥ��� ���Υ��르�ꥺ������Ѥϡ�GnuPG�����ǥ��ݡ��Ȥ���Ƥ��ޤ���
PGP�桼�����Ȥ��̿��ˡ����θ���Ȥ����Ȥϡ��Ǥ��ʤ��Ǥ��礦��
�ޤ������Υ��르�ꥺ���������®�ǡ�¾���������������
�ʤ����⤷��ޤ���
 PGP 2.x�����桼����ID��������������ޤ���
 %s�⡼�ɤǤ��Υ��ޥ�ɤ��Ѥ��뤳�ȤϤǤ��ޤ���
 �������̩���Ǥ�! �����˺�����ޤ���?  ����ϡ����Υ桼����ID���б�������̾�Ǥ����̾���ν�̾��������Τ�
�����ͤ��ǤϤ���ޤ��󡣼ºݤˤϡ�GnuPG�Ϥ�Ϥ䤳�θ���Ȥ��ʤ��Τ���
����ޤ��󡣤�äơ����μ��ʽ�̾�����餫����ͳ�ˤ��ͭ���ǤϤʤ��ơ�
���ؤȤʤ븰��������ˤΤߡ������¹Ԥ��Ƥ��������� ����ϸ��ˤ�������ͭ���ʽ�̾�Ǥ������̤��ν�̾��������٤���
�Ϥʤ��Ǥ��礦���ʤ��ʤ顢���ν�̾�ϡ����ؤο��Ѥ��ؤκ����䡢
���θ��ˤ������ˤȤäƽ��פ�����Ǥ��� ���θ��ϼ�ʬ�Τ�ΤǤ�
 ���θ��ϻ��Ѷػߤ����ꤵ��Ƥ��ޤ� ���θ��ϴ����ڤ�Ǥ�! ���θ���%s�Ǵ��¤��ڤ�ޤ���
 ���θ����ݸ��Ƥ��ޤ���
 ���θ��ϡ�%s���ˤ�äƼ������줿�褦�Ǥ�  ���θ��Ϥ��֤��ܿͤΤ�ΤǤ�
 ���ν�̾�Ȱ��פ��븰����ͭ���Ƥ��ʤ��Τǡ����ν�̾�ϸ����Ǥ��ޤ���
���θ������Ѥ����ޤǤϡ����ʤ��Ϥ��ν�̾�κ������α����٤��Ǥ���
�ʤ��ʤ顢���ν�̾�θ��ϡ�¾�ξ������줿���ǿ��Ѥ��ؤ�������뤫��
����ʤ�����Ǥ��� ���ν�̾��%s�Ǵ����ڤ�Ǥ���
 ���θ���PGP 2.x�ǻ��ѤǤ��ʤ��ʤ�ޤ���
 ������:
 Web-of-Trust���ۤ��뤿��GnuPG�ϡ��ɤθ�������Ū�˿��Ѥ���
�Τ����Τ�ɬ�פ�����ޤ�������ϤդĤ�����̩���˥��������Ǥ�
�븰�Τ��ȤǤ������θ�������Ū�˿��Ѥ��뤳�Ȥˤ���ʤ顢
��yes�פ������Ƥ���������
          �������ι��: %lu
 �̿����%s�פ򳫤����Ȥ��Ǥ��ޤ���: %s
 �Ȥ���: gpg [���ץ����] [�ե�����] (�إ�פ� -h) �Ȥ���: gpgv [���ץ����] [�ե�����] (�إ�פ� -h) ����Ǥ⤳�θ���Ȥ��ޤ���?  �桼����ID��%s�פϡ���������Ƥ��ޤ��� �桼����ID���⤦ͭ���Ǥ���ޤ��� �ٹ�: ��%s�פϡ�����뤵��Ƥ��륪�ץ����Ǥ�
 �ٹ�: %s��%s���ͥ��
 �ٹ�: ���Ѿ�����ä�2�ĤΥե����뤬¸�ߤ��ޤ���
 �ٹ�: �����PGP 2.x�����θ��Ǥ�����̾�����Ԥ��ɲäǡ��������Ǥ�PGP�ϡ�
      ���θ�����ݤ��뤫�⤷��ޤ���
 �ٹ�: �����PGP2�����θ��Ǥ����ե���ID���ɲäǡ��������Ǥ�PGP�ϡ�
      ���θ�����ݤ��뤫�⤷��ޤ���
 �ٹ�: ���θ����ܿͤˤ�äƼ�������Ƥ��ޤ�!
 �ٹ�: ���θ��Ͽ��ѤǤ����̾�Ǿ�������Ƥ��ޤ���!
 �ٹ�: ���θ��Ͻ�ʬ�˿��ѤǤ����̾�Ǿ�������Ƥ��ޤ���!
 �ٹ�: �����������ܿͤˤ�äƼ�������Ƥ��ޤ�!
 �ٹ�: ���ѤǤ��ʤ������Ѥ��Ƥ��ޤ�!
 �ٹ�: ���θ��Ͽ��ѤǤ��� ����!
 �ٹ�: �夤���򸡽Ф��ޤ������ѥ��ե졼�����ѹ����Ƥ���������
 �ٹ�: `%s'�϶��Υե�����Ǥ�
 �ٹ�: �桼����ID��̾����%d��̤��Ǥ�
 �ٹ�: ���븰���̾�����Ԥ����ꤹ��ȡ������᤻�ޤ���!
 �ٹ�: �Ź沽���줿��å������ϲ��⤵��Ƥ��ޤ�!
 �ٹ�: ̵��������ǡ���������ޤ�
 �ٹ�: ̵���ʥ������� random_seed �ե����� - �Ȥ��ޤ���
 �ٹ�: ��%08lX�ϼ������줿�褦�Ǥ�: ������%08lX�ΰ����Ф�
 �ٹ�: ��%08lX�ϼ������줿�褦�Ǥ�: ������%08lX���Ժߡ�
 �ٹ�: ��å��������оΰŹ�ˡ�μ夤���ǰŹ沽����Ƥ��ޤ���
 �ٹ�: ��å������δ��������ݸ��Ƥ��ޤ���
 �ٹ�: ¿�Ž�̾�θ��С��ǽ�Τ�Τ����������ޤ���
 �ٹ�: ����񤭽Ф��Ƥ��ޤ���
 �ٹ�: ��%s�פΥ��ץ����ϵ�ư���Ƥ���֡�ͭ���ˤʤ�ޤ���
 �ٹ�: �ץ������Υ������ե����뤬�Ǥ��뤳�Ȥ�����ޤ�!
 �ٹ�: �������Ź��Ȥ鷺�ˡ������ (-r) ����ꤷ�Ƥ��ޤ�
 �ٹ�: ��̩��%08lX�ˤϡ�ñ���SK�����å����ब����ޤ���
 �ٹ�: ��̾�����󤬡���å�������̷�⤷�ޤ�
 �ٹ�: ɽ����%%��ĥ��ǽ (�礭����)�����ĥ����ѡ�
 �ٹ�: �ݥꥷ��url��%%��ĥ��ǽ (�礭����)�����ĥ����ѡ�
 �ٹ�: ����ǥ��쥯�ȥ꡼��%s�פ����Ǥ��ޤ���: %s
 �ٹ�: ����ե���������Ǥ��ޤ��� (%s) `%s': %s
 �ٹ�: %s ��%s�פΰ����Ǥʤ���̥ǥ��쥯�ȥ꡼��ͭ��
 �ٹ�: %s ��%s�פΰ����Ǥʤ���̥ǥ��쥯�ȥ꡼����
 �ٹ�: %s ��%s�פΰ����Ǥʤ���ͭ��
 �ٹ�: %s ��%s�פΰ����Ǥʤ�����
 �ٹ�: ���ݤʥ��꡼����Ѥ��Ƥ��ޤ�!
 �ٹ�: ���ݤ���������Ҥ��Ȥ��Ƥ��ޤ�!!
 ������Ĺ��������������ޤ��������ܡ��ɤ��ǤĤȤ����ޥ�����ư����
�Ȥ����ǥ������˥�����������Ȥ���¾�Τ��Ȥ򤹤�ȡ���������Ҥ�
�𻨤����礭�ʤ���������������䤹���ʤ�Τǡ������ᤷ�ޤ���
 �ɤθ�Ĺ�ˤ��ޤ���? (1024)  ���Υ桼����ID�˽�̾����Ȥ��ϡ����Υ桼����ID��̾���οͤ���
���ͭ���Ƥ��롢�Ȥ������Ȥ��ǧ����٤��Ǥ������ʤ����ɤ���
�����տ�����ǧ�����Τ���¾�οͤ������Τ餻��ȡ������Ǥ���

"0" �ϡ����ʤ����ɤ��������տ�����ǧ�����Τ����ä˼�ĥ����
    ���Ȥ�����̣�Ǥ���

"1" �ϡ����ν�ͭ�Ԥȼ�ĥ���Ƥ���ͤ����θ����ͭ���Ƥ��롢��
    ���ʤ��Ͽ����ƤϤ����ΤΡ���ǧ���������Ƥʤ��ä����ޤ�
    �ϤǤ��ʤ��ä����Ȥ�����̣�Ǥ�������ϡ��ڥ�͡����Ȥ�
    �桼�����θ��˽�̾������Ρ��֥ڥ륽�ʡ׳�ǧ�������Ǥ���

"2" �ϡ����ʤ���������γ�ǧ�򤷤����Ȥ�����̣�Ǥ������Ȥ���
    ���λ���򸡾ڤ����ե���ID�ˤ������Ƹ��Υ桼����ID�򸡺�
    ������礬�����Ǥ���

"3" �ϡ����ʤ������ϰϤˤ錄�기�򸡾ڤ������Ȥ�����̣�Ǥ���
    ���Ȥ��С����ν�ͭ���ܿͤȸ��λ���򸡾ڤ���(�ѥ��ݡ���
    �Τ褦��) ��¤�Τ�Ĥ��������ե���ID�Ĥ��μ��ʤǡ����ν�
    ͭ�Ԥ�̾���������Υ桼����ID��̾���Ȱ��פ��뤳�Ȥ򸡺���
    �ơ�(�Żҥ᡼��θ򴹤ʤɤ�) �����Żҥ᡼�롦���ɥ쥹��
    ���ν�ͭ�Ԥ�°���Ƥ��뤳�Ȥ򸡾ڤ�����礬�����Ǥ���

�嵭2��3����ϡ�*ñ�ʤ�*��ˤ����ʤ����Ȥ������Ȥ�ǰƬ���֤�
�Ƥ���������¾�θ��˽�̾����ݡ��ְ���פ�ֹ��ϰϡפ������
̣���뤫��ǽ�Ū�˷���Τϡ����ʤ��Ǥ���

���������Τ狼��ʤ����ϡ���0�פ������Ƥ��������� �����ν�̾�򼺸����褦�Ȥ��Ƥ��ޤ�:
 ���ʤ���ʸ������`%s'��ȤäƤ��ޤ���
 v3����ͭ�����¤��ѹ��Ǥ��ޤ���
 �Ǹ�Υ桼����ID�Ϻ���Ǥ��ޤ���!
 �桼����ID����ꤷ�Ƥ��ޤ��� (��-r�פ��Ѥ��ޤ��礦) ��
 �ѥ��ե졼������ɬ�פʤ褦�Ǥ�����
�����餯����Ϥ����ʹͤ��ǤϤ���ޤ���!

 �ѥ��ե졼������ɬ�פʤ褦�Ǥ����������餯����Ϥ����ʹͤ��Ǥ�
����ޤ���! ��������³�Ԥ��ޤ����ѥ��ե졼���ϡ����Υץ������
�Ρ�--edit-key�ץ��ץ����Ǥ��ĤǤ��ѹ��Ǥ��ޤ���

 �����Υ桼����ID�˽�̾���ޤ���:
 PGP 2.x�����θ��ˤϻ�̾�����Ԥ��ɲäǤ��ʤ����⤷��ޤ���
 PGP2�����θ��ˤϥե���ID���ɲäǤ��ʤ����⤷��ޤ���
 --pgp2�⡼�ɤǤ�PGP 2.x����OpenPGP��̾���Ǥ��ʤ����⤷��ޤ���
 ���򾯤ʤ��Ȥ�ҤȤ����򤷤Ƥ���������
 �桼����ID�򾯤ʤ��Ȥ�ҤȤ����򤷤Ƥ���������
 ��̩�����ݸ�뤿��˥ѥ��ե졼��������ޤ���

 ���Υ桼��������̩���Υ��å���������ˤϥѥ��ե졼��������ޤ�:
"%.*s"
%u�ӥå�%s��, ID %08lX�������դ�%s%s
 ���Υ桼����ID�����򤷤ޤ���:
    "%s"

 ���������ͳ����ꤹ��ɬ�פ�����ޤ�������ط��ˤ���ޤ�����
�ʲ��������֤��Ȥ��Ǥ��ޤ���
  �ָ����ѥ����ޤ�����
      ǧ�ڤ���Ƥ��ʤ��ͤ���̩���ؤΥ����������������ȿ�����
      ��­����ͳ�Τ���Ȥ��ˡ������Ȥ��ޤ���
  �ָ����Ȥ꤫��äƤ��ޤ���
      ���θ���ʬ�ǿ��������ȤȤ꤫�����Ȥ��˻Ȥ��ޤ���
  �ָ��Ϥ⤦�Ȥ��Ƥ��ޤ����
      ���θ���Ȥ��Τ��᤿�Ȥ��˻Ȥ��ޤ���
  �֥桼����ID���⤦ͭ���Ǥ���ޤ����
      �⤦���Υ桼����ID�ϻȤ��٤��Ǥʤ����Ȥ����Ȥ��˻Ȥ�
      �ޤ�����������̡��Żҥ᡼��Υ��ɥ쥹��̵���ˤʤä���
      ���˻Ȥ��ޤ���
 ��%s�פˤ������뺣�Τ��ʤ��ν�̾��
�����ڤ�Ǥ���
 "%s" �ˤ������뺣�Τ��ʤ��ν�̾��
������̾�Ǥ���
 ���ʤ��η����?  �ɤ�ˤ��ޤ���?  ���Υ����ƥ�Ǥϡ�2038ǯ�ʹߤ����դ�ɽ���Ǥ��ޤ��󤬡�
2106ǯ�ޤǤʤ������������Ǥ��ޤ���
 [�桼����id�����Ĥ���ޤ���] [��λ]  [�ե�����̾] [����] [����]  [���ʽ�̾] [�Գ���] `%s'�ϴ��˰��̺ѤߤǤ�
 ��%s�פ����̤Υե�����ǤϤ���ޤ��� - ̵��
 ��%s�פϡ�ͭ�����緿��ID�Ǥ���ޤ���
 ����̾�ˤϰ�����ǽ��ʸ��������Τߤ��Ѥ���'='�ǽ����ʤ���Фʤ�ޤ���
 ����̾���ͤ�����ʸ�����Ѥ��ƤϤ����ޤ���
 �桼��������̾�ϡ�'@'ʸ����ޤޤʤ���Фʤ�ޤ���
 �ե���ID���ɲ� ���������ɲ� �������ɲ� �桼����ID���ɲ� ���ؤΰ����ˤ��θ��ؤ��ɲ� �����ˤ�����̩���ؤ��ɲ� addkey addphoto addrevoker adduid �Ź沽�ˤϾ��MDC����� ƿ̾�μ�����ѤǤ�����̩��%08lX���Ѥ��ޤ� ...
 �����إå���:  ����: %s
 �����Ƥ��μ����������no
�Ȥߤʤ� �����Ƥ��μ����������yes
�Ȥߤʤ� %s�Ź沽���줿�ǡ�������
 ̤�ΤΥ���ƥ����롦�ӥåȤˤ�ꡢ��%08lX�ν�̾�������Ȥߤʤ��ޤ�
 ��̾���줿�ǡ�����`%s'�ˤ�������ꤷ�ޤ�
 ������MPI�Ǥ� URI�������Ǥ� �����������Ǥ� ���������Ǥ� �ѥ��ե졼���������Ǥ� �������������Ǥ� ��̩���������Ǥ� ��̾�������Ǥ� �Хå����⡼��: ��ǧ��ά ����Ť� �Х��ʥ꡼ build_packet �˼��Ԥ��ޤ���: %s
 c ��%s�פ��Ĥ����ޤ���: %s
 `%s'����³�Ǥ��ޤ���: %s
 %s������Ǥ��ޤ���: %s
 ��%s�פ��Ǥ��ޤ���: %s
 �ǥ��쥯�ȥ꡼��%s�פ��Ǥ��ޤ���: %s
 ����������פ�̵���ˤǤ��ޤ���: %s
 ����ϥХå��⡼�ɤǤϤǤ��ޤ���
 ��--yes�פΤʤ��Хå��⡼�ɤǤϤǤ��ޤ���
 �������С����鸰���ɤ߹���ޤ���: %s
 ������������ѤΥ����С��ɽФ�FD������Ǥ��ޤ���
 ������������ѤΥ����С������FD������Ǥ��ޤ���
 �������Υ��르�ꥺ��%d�ϻ��ѤǤ��ޤ���
 %dʸ���ʾ��Ĺ���Υƥ����ȹԤϻ��ѤǤ��ޤ���
 ����¿�Ž�̾�ϼ�갷���ޤ���
 %s�������ޤ���: %s
 ��%s�פ������ޤ���
 ��%s�פ������ޤ���: %s
 �ե����뤬�����ޤ���: %s
 ��̾���줿�ǡ���`%s'�������ޤ���
 ���ؤ������ޤ��� �ݥꥷ��URL�ϡ�(PGP 2.x������) v3��̾�ˤ�����ޤ���
 �ݥꥷ��URL�ϡ�(PGP 2.x������) v3����̾�ˤ�����ޤ���
 ����ǡ����ϡ�(PGP 2.x������) v3����̾�ˤ�����ޤ���
 ����ǡ����ϡ�(PGP 2.x������) v3��̾�ˤ�����ޤ���
 �Хå��⡼�ɤǤϥѥ���ɤ���礻���Ǥ��ޤ���
 ��%s�פ��ɤ�ޤ���: %s
 �������С��򸡺��Ǥ��ޤ���: %s
 ����������ȤΥ��饤�����pid������Ǥ��ޤ���
 ��%s�פ�Ĵ�٤뤳�Ȥ��Ǥ��ޤ���: %s
 S2K�⡼�ɤΤ��ᡢ�о�ESK�ѥ��åȤ�Ȥ��ޤ���
 ��%s�פ˽񤱤ޤ���: %s
 �桼�����ˤ���ä�
 PGP 2.x�����θ��ϡ���̾�����Ԥ�Ǥ̿�Ǥ��ޤ���
 �оΰŹ�ˡ�μ夤������򤹤뤳�Ȥ��Ǥ��ޤ���%d���ߤޤ���!
 ͭ�����¤��ѹ� ��ͭ�Կ��Ѥ��ѹ� �ѥ��ե졼�����ѹ� check ���ν�̾�򸡺� ����%d�Ǹ�������̾�Ѥ�=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 �������줿��̾�θ����˼���: %s
 ���ء�%s�פ򸡺����Ƥ��ޤ�
 ���ѥǡ����١����θ���
 �����å����ࡦ���顼 �Ź楢�르�ꥺ��%d%s��̤�Τ������ԲĤǤ�
 �����Ǥʤ����ĤΤ��ᡢ�Ź��ĥ��%s�פ�����ɤ��ޤ���
 gpg-agent�Ȥ��̿��㳲
 completes-needed�������ͤ�ɬ�פǤ�
 ���̥��르�ꥺ���%d..%d���ϰϤǤʤ���Фʤ�ޤ���
 ��Ω���륳�ޥ��
 �������С���URI�������ǽ
 ASCII��������������� �ǡ�������¸����Ƥ��ޤ���
��¸����ˤϡ�--output�ץ��ץ�������Ѥ��Ƥ�������
 ��������˼��Ԥ��ޤ���: %s
 debug �ǡ��������� (����) ����˼��Ԥ��ޤ���: %s
 ���������
 �����κ�� ��̾�κ�� �桼����ID�κ�� ���֥��å��κ���˼��Ԥ��ޤ���: %s
 delkey delphoto delsig deluid ���󥢥르�ꥺ���%s�פϡ������Ǥ����ɽФ������Ǥ�
 disable ���λ��Ѥ�ػߤ��� v3��̾�������ʤ� v4��̾�������ʤ� ̵�ѹ� ü�����Ի��� RFC1991�˵��Ҥ��줿�⡼�ɤ�
���� enable ���λ��Ѥ���Ĥ��� �����˼��Ԥ��ޤ���: %s
 �ǡ�����Ź沽 %s��, ID %08lX�ǰŹ沽����ޤ���
 %u-�ӥå�%s��, ID %08lX�ǰŹ沽%s�ˤǤ��ޤ���
 ̤�ΤΥ��르�ꥺ��ˤ��Ź� %d
 --pgp2�⡼�ɤΥ�å������Ź沽�Ǥϡ�IDEA�Ź椬ɬ�פǤ� 
 �Ź沽�ˤ��оΰŹ�ˡ�Τߤ���� `%s'�κ������顼: %s
 ���ء�%s�פκ������顼: %s
 �ѥ��ե졼���κ������顼: %s
 ���ѥ쥳���ɤθ������顼: %s
 �����ιԤ˥��顼������ޤ�
 `%s'���ɹ��ߥ��顼: %s
 ���֥��å����ɹ��ߥ��顼: %s
 ��̩���֥��å���%s�פ��ɹ��ߥ��顼: %s
 `%s'�ؤ��������顼: %s
 ���ء�%s�פν���ߥ��顼: %s
 ��������`%s'�ν���ߥ��顼: %s
 ��̩���ء�%s�פν���ߥ��顼: %s
 ���顼: ̵���ʻ���
 ���顼: ������ȴ���Ƥ��ޤ�
 ���顼: ��ͭ�Ԥ��Ѥ����ͤ�����ޤ���
 expire ����񤭽Ф� �������С��˸���񤭽Ф� ��ͭ�Ԥ��Ѥ����ͤ�񤭽Ф� ���ץ���󡦥ե�����ε��ĥ⡼�ɤ��������ǤϤʤ��Τǡ�
�����ץ������θƽФ��ϡ����ѶػߤǤ���
 `%s'�ؤ������˼��Ԥ��ޤ���: ����=%u
 ���ѥǡ����١����ν�����˼��Ԥ��ޤ���: %s
 ���إ���å���κƹ��ۤ˼��Ԥ��ޤ���: %s
 �ե����뤬�Ĥ����ޤ��� �ե�����κ������顼 �ե�����κ�����顼 �ե����뤬����¸�ߤ��Ƥ��ޤ� �ե����뤬�����ޤ��� �ե�������ɽФ����顼 �ե�����̾���ѹ����顼 �ե�����ν���ߥ��顼 ���줿���ѥǡ����١������� �桼����ID���ˤ��� ����Ū��v3��̾ ����Ū��v4��̾ ���̥��르�ꥺ�� %s (%d) �ζ�����������ͤ���������Ω���ޤ�
 ���󥢥르�ꥺ�� %s (%d) �ζ�����������ͤ���������Ω���ޤ�
 �оΰŹ楢�르�ꥺ�� %s (%d) �ζ�����������ͤ���������Ω���ޤ�
 fpr ����Ū�ʥ��顼 ���������Ф����� ��������������� ����뤵�줿16�ӥåȤΥ����å��������̩�����ݸ������
 ���Υ��å�����gpg����������Ȥ�̵���Ǥ�
 gpg-agent�ץ��ȥ��롦�С������%d�ϥ��ݡ��Ȥ��Ƥ��ޤ���
 help iImMqQsS �������С����鸰���ɤ߹��� ��ͭ�Ԥ��Ѥ����ͤ��ɤ߹��� �����ɹ���/ʻ�� ���Ϥ�%u���ܤ�Ĺ�����뤫��LF���ʤ��褦�Ǥ�
 ���ϹԤ�Ĺ����%dʸ����Ķ���Ƥ��ޤ�
 ̵����S2K�⡼�ɡ�0��1��3�Ǥʤ���Фʤ�ޤ���
 ̵���ʻ���Ǥ� ̵���������Ǥ� ̵���������إå���:  ̵��������: �Ԥ�Ĺ����%dʸ����Ķ���Ƥ��ޤ�
 ����ʸ�����̵����ʸ��������ޤ�
 ̵���ʥ��ꥢ��̾�إå���
 ̵���ʥ��å���ǥ��������פ��줿��:  ̵���ʴ��������
 ̵����default-check-level��0��1��2��3�Ǥʤ���Фʤ�ޤ���
 ̵���ʽ�Ф����ץ����Ǥ�
 ̵���ʥϥå��塦���르�ꥺ��`%s'�Ǥ�
 ̵�����ɹ��ߥ��ץ����Ǥ�
 ���ؤ�̵���Ǥ� ̵���ʥѥ��åȤǤ� �ѥ��ե졼����̵���Ǥ� ̵���ʸĿ��ѰŹ������
 ̵���ʸĿ��Ѱ��̤�����
 ̵���ʸĿ������������
 ̵����64��ʸ��%02x��ȤФ��ޤ�
 ����������Ȥ����̵���ʱ���
 proc_tree() �����̵���ʥѥ��åȤ򸡽Ф��ޤ���
 ̵�����оθ����르�ꥺ�� (%d) �θ���
 ̵������
 key ��%08lX�ϡ�%lu��̤��ˤǤ��ޤ��� (����ι�Ԥ����פΤ��뤤�Ǥ��礦)
 ��%08lX�ϡ�%lu��̤��ˤǤ��ޤ��� (����ι�Ԥ����פΤ��뤤�Ǥ��礦)
 ��%08lX: �Դ���
 ����Ū�˿��Ѥ������Ȥ��Ƹ� %08lX ��Ͽ���ޤ���
 ��%08lX�����ѥǡ����١�����ʣ������ޤ�
 ��%08lX: ��%s�׿�������̾��%d��
 ��%08lX: ��%s�׿�����������%d��
 ��%08lX: ��%s�׿������桼����ID��%d��
 ��%08lX: ��%s�׿�������̾��1��
 ��%08lX: ��%s�׿�����������1��
 ��%08lX: ��%s�׿������桼����ID��1��
 ��%08lX: ��%s���ѹ��ʤ�
 ��%08lX: ��%s�׼�����������ɲ�
 ��%08lX: ��%s�׼�����������ɤ߹��ߤޤ���
 ��%08lX: HKP��������¤����
 �� %08lX: PGP 2.x�����θ��Ǥ� - �����å�
 ��%08lX: ���Ѥ��븰�Ȥ��Ƽ������ޤ���
 ��%08lX: ��������̤���ʽ�̾�Υ桼����ID '%s'
 ��%08lX: ������̩���ؤˤ���ޤ�
 ��%08lX: ���θ��֥��å��˰��֤Ť��Ǥ��ޤ���: %s
 ��%08lX: ���θ��֥��å����ɤ߹���ޤ���: %s
 ��%08lX: ľ�ܸ���̾���ɲ�
 ��%08lX: �������ʣ���ȹ礤�ޤ���
 ��%08lX: ��ʣ�����桼����ID�θ��� - ʻ��
 ��%08lX: ̵���ʼ���������: %s - ����
 ��%08lX: ̵���ʼ���������: %s - �����å�
 ��%08lX: �桼����id��%s�פμ��ʽ�̾����̵���Ǥ�
 ��%08lX: ̵�����������б��Ǥ�
 ��%08lX: ̵�������������Ǥ�
 ��%08lX: ���ϼ����ѤߤǤ�!
 �� %08lX: ���������Ǥ� - �����å�
 ��%08lX: ������������ޤ��� - �����������Ŭ�ѤǤ��ޤ���
 �� %08lX: ���Ѥ��븰�θ�����������ޤ��� - �����å�
 ��%08lX: �����б���������������ޤ���
 ��%08lX: �������ˤ�����������������ޤ���
 ��%08lX: �������ѥ��åȤˤ�����������������ޤ���
 ��%08lX: �桼����ID������ޤ���
 ��%08lX: ��̾���б�����桼����ID������ޤ���
 ��%08lX: ͭ���ʥ桼����ID������ޤ���
 ��%08lX: ��Ф��ԲĤʽ�̾ (���饹%02x) - �����å�
 �� %08lX: rfc2440�θ��ǤϤ���ޤ��� - �����å�
 �� %08lX: �ݸ��Ƥ��ޤ��� - �����å�
 ��%08lX: ��������%s�פ��ɤ߹��ߤޤ���
 ��%08lX: �����������Ĥ���ޤ���: %s
 ��%08lX: ¿���������б��������ޤ�
 ��%08lX: ̵���������μ����������ޤ�
 ��%08lX: ���������񤬸��ä����ꤵ��Ƥ��ޤ� - �����å�
 ��%08lX: ��̩�����ɤ߹��ߤޤ���
 ��%08lX: ��̩�������Ĥ���ޤ���: %s
 ��%08lX: ̵���ʰŹ�%d����̩���Ǥ� - �����å�
 ��%08lX: �������Τʤ���̩���Ǥ� - �����å�
 ��%08lX: �����åפ�������
 ��%08lX: �����åפ����桼����ID ' ��%08lX: �����ϼ����ѤߤǤ�!
 ��%08lX: ������̾�ξ�꤬�����äƤ��ޤ� - �����å�
 ��%08lX: ����ϡ�PGP����������ElGamal�Ǥ���
��̾���Ѥ���ˤϡ����θ��ϰ����Ǥ� �ʤ� �Ǥ�!
 ��%08lX: ͽ�����̽�̾���饹 (0x%02X) - �����å�
 ��%08lX: ���ݡ��Ȥ��Ƥ��ʤ����������르�ꥺ��Ǥ�
 ��%08lX: �桼����id��%s�פΥ��ݡ��Ȥ��Ƥ��ʤ����������르�ꥺ��Ǥ�
 ����%s�פ����Ĥ���ޤ���: %s
 ����%lu��̤��ˤǤ��ޤ��� (����ι�Ԥ����פΤ��뤤�Ǥ��礦)
 ����%lu��̤��ˤǤ��ޤ��� (����ι�Ԥ����פΤ��뤤�Ǥ��礦)
 �Դ����ʸ�
 ���ݤǤ���Ȥ����ե饰�����ˤ����ꤵ��Ƥ��ޤ���
��ʪ��������ҤȤϤ��ä���˻Ȥ��ޤ���!
 ����Ū�˿��Ѥ������Ȥ��Ƶ�Ͽ���ޤ�����
 ���ء�%s�פ��Ǥ��ޤ���
 �������С��Υ��顼 ̵���ʸ���������%u�ӥåȤˤ��ޤ�
 ����������%u�ӥåȤ˴ݤ�ޤ�
 ��Ĺ���礭�����ޤ���%d������Ǥ���
 ��Ĺ�����������ޤ���RSA�ϺǾ��Ǥ�1024�Ǥ���
 ��Ĺ�����������ޤ����Ǿ��Ǥ�768�Ǥ���
 l �Ԥ�Ĺ�᤮�ޤ�
 list ���ȥ桼����ID�ΰ��� ���ΰ��� ���Ȼ���ΰ��� ���Ƚ�̾�ΰ��� �ѥ��å���Τߤΰ��� �����ΰ��� (�������ѡ���) �����ΰ��� (��Ĺ) ��̩���ΰ��� ��̾�ΰ��� lsign ʬΥ��̾����� ������̷���ٹ�����ˤ��ޤ� make_keysig_packet �˼��Ԥ��ޤ���: %s
 CRC�ν񼰤�����������ޤ���
 GPG_AGENT_INFO�Ķ��ѿ��ν񼰤�����������ޤ���
 �桼����ID�ν񼰤�����������ޤ��� marginals-needed��1����礭���ͤ�ɬ�פǤ�
 max-cert-depth��1����255���ϰϤǤʤ���Фʤ�ޤ���
 ���ν�̾�����������˰�ư���ޤ�
 nN ����ҤΥ��ꥢ��̾
 �ͥåȥ�������顼 ̵����     �Ź沽�ˤ����Ф�MDC����Ѥ��ʤ� �����������ե�����`%s'���Ǥ��ޤ���
 ���ѥǡ����١����μ���θ����ϡ�%s�Ǥ�
 no =���椬�����롼�������%s����˸��Ĥ���ޤ���
 �б����������������ޤ���: %s
 �������̩���ؤ�����ޤ���: %s
 ����ȥ��ԡ������⥸�塼�뤬�����Ф���Ƥ��ޤ���
 ���ѥǡ����١����θ����ϡ����פǤ�
 ��֥ץ������μ¹Ԥϡ����ݡ��Ȥ��Ƥ��ޤ���
 ��%s���Ѥμ������ϡ�����ޤ���
 ��̩��������ޤ���
 ��������%08lX�ˤ���������̩����������ޤ��� - ̵��
 ��̾���줿�ǡ���������ޤ���
 ���Υ桼����ID�Ϥ���ޤ��� ����Ū�˿��Ѥ��븰�����Ĥ���ޤ���
 ͭ����OpenPGP�ǡ��������Ĥ���ޤ���
 ͭ���ʥ��ɥ쥹������ޤ���
 ����߲�ǽ�ʸ��ؤ����Ĥ���ޤ���: %s
 ����߲�ǽ�ʸ������ؤ����Ĥ���ޤ���: %s
 ����߲�ǽ����̩���ؤ����Ĥ���ޤ���: %s
 ʬΥ��̾�Ǥ���ޤ���
 �Ź沽����Ƥ��ޤ��� �ͤˤ��ɤ�ޤ��� ̤���� ���ݡ��Ȥ��Ƥ��ޤ��� ����: random_seed �ե�����϶��Ǥ�
 ����: random_seed �ե�����ι����򤷤ޤ���
 nrlsign nrsign ��λ��ƿ̾�μ�����ѤǤ���
 DEK�ˤ��켰�ΰŹ�ϥ��ݡ��Ȥ��Ƥ��ޤ���
 �Ť����� (PGP 2.x) �ν�̾
 ������Ѥߤΰ����ʥ��꡼���ʤ����ˤϼ¹ԤǤ��ޤ���
 ���ץ���󡦥ե������%s��: %s
 ���Υե�����̾='%.*s'
 ��ͭ�Կ��Ѿ���򥯥ꥢ���ޤ���
 �ѥ��ե졼��������ȷ����֤��Ƥ��ޤ��󡣺����Ϥ��Ƥ������� �ѥ��ե졼����Ĺ�᤮�ޤ�
 passwd --check-trustdb��¹Ԥ��Ƥ�������
 ���ץ����Ǥ������Żҥ᡼��Υ��ɥ쥹�����Ϥ��뤳�Ȥ򶯤��侩���ޤ� �ܺ٤�http://www.gnupg.org/faq.html������������
 �ܺ٤ϡ�http://www.gnupg.org/why-not-idea.html������������
 ��%s%s�פ�����˻ȤäƤ�������
 pref ����%c%lu�ν�ʣ
 ����%c%lu��ͭ���ǤϤ���ޤ���
 �ե������������᤹���ޤ� (CRC������)
 �ե������������᤹���ޤ� (����������ˤ���ޤ�)
 �ե������������᤹���ޤ� (CRC������ޤ���)
 primary �Ź�ѥ��åȤμ�갷���Ǿ㳲
 ����������Ȥ˾㳲: ��������������Ѷػ�
 ����������Ȥ˾㳲: ����������Ȥ�0x%lx���ֵ�
 ������˳�ǧ �ݸ�르�ꥺ��%d%s�ϥ��ݡ��Ȥ��Ƥ��ޤ���
 ����������̩�����ꡢ��̾���ޤ�����
 ������%08lX�ϡ���̾��%lu�����Ǥ�
 ������%08lX�ϡ���̾��%lu�����Ǥ�
 ������%08lX�����Ĥ���ޤ���: %s
 ������������˼��Ԥ��ޤ���: %s
 ����������̩�����ȹ礷�ޤ���!
 �������ǰŹ沽���줿�ǡ���: ������DEK�Ǥ�
 ��������%08lX�Ǥ�
 �����������Ĥ���ޤ��� ����Ū�˿��Ѥ��븰%08lX�θ����������Ĥ���ޤ���
 q qQ quit ���Υ�˥塼��λ ��������� quoted printable ʸ��������ޤ��������餯�Х��Τ���
MTA����Ѥ����ΤǤ��礦
 �ɹ��ߥ��顼: %s
 �ե����뤫�饪�ץ�����
�ɤ߹��� `%s'�����ɤ߹��ߤޤ�
 ��%s�פ��饪�ץ������ɤ߹��ߤޤ�
 ɸ�����Ϥ���ɹ����� ...
 ������ͳ:  �������ؤ��鸰������ ��̩���ؤ��鸰������ ��%08lX��%s���׵�
 �꥽�������³��Ǥ� rev! �����ϼ����ѤߤǤ�: %s
 rev- �Ǥä������μ�����ȯ��
 rev? ���������㳲: %s
 revkey ������Ŭ��:  �����μ��� �桼����ID�μ��� ��̾�μ��� revsig revuid %u�ӥåȤ˴ݤ�ޤ�
 s save ��¸���ƽ�λ �������С��θ��򸡺����� ��%s�פ�HKP�����С�%s���鸡��
 ��̩����%s�פ����Ĥ���ޤ���: %s
 ��̩���������ޤ��� ��̩��ʬ�������ޤ���
 ����N������ �桼����ID N������ ���򤵤줿���������󥢥르�ꥺ��ϡ�̵���Ǥ�
 ���򤵤줿�Ź楢�르�ꥺ���̵���Ǥ�
 ���򤵤줿���󥢥르�ꥺ��ϡ�̵���Ǥ�
 �ѥ��åȤȰŹ������Υ��ץ���
�������OpenPGP�ο�������� �ѥ��åȤȰŹ������Υ��ץ���
�������PGP 2.x�ο�������� �����ΰ��������� setpref �����ɽ�� �ե���ID��ɽ�� ���Υإ�פ�ɽ�� �����θ������븰�ؤ�ɽ�� showphoto showpref sign ���˽�̾ ��������Ū�˽�̾ �����Ǥ��ʤ��褦��������Ū�˽�̾ �����Ǥ��ʤ��褦���˽�̾ ���ؤν�̾���Խ� ���ؽ�̾ ��������Ū�˽�̾ �����Ǥ��ʤ��褦��������Ū�˽�̾ �����Ǥ��ʤ��褦���ؽ�̾ ��̾�θ��ڤ��ά
 ��̾�˼��Ԥ��ޤ���: %s
 ��̾: ��%s�פ򥹥��å�: %s
 ��%s�פ򥹥��å�: ��ʣ
 ��%s�פ򥹥��å�: �����PGP����������ElGamal���Ǥ���
��̾���Ѥ���ˤϡ������ǤϤ���ޤ���!
 �����å�: ������������ѤߤǤ�
 �����å�: �������ϴ���μ���ͤȤ�������ѤߤǤ�
 �����å�: ��̩���ϴ��ˤ���ޤ�
 ������%d�Υ֥��å��򥹥��åפ��ޤ�
 �桼����id��%s�פ�v3���ʽ�̾��ȤФ��ޤ�
 ��ǰ�ʤ��顢�Хå��⡼�ɤǤϤǤ��ޤ���
 ��Ω��������gpg --import�פ�Ȥä�Ŭ�Ѥ��Ƥ�������
 ���饹0x%02x����Ω��̾
 ��¸�Τ� ������%d�β��̥ѥ��åȤ˥���ƥ����롦�ӥåȤ�ȯ��
 `%s'�ؤ����������� (����=%u)
 �����ץ������θƽФ��ǥ����ƥࡦ���顼: %s
 t ���θ����ؤ��������ޤ� �ƥ����ȥ⡼�� IDEA�Ź�Υץ饰���󤬤���ޤ���
 ��������줿������ݥꥷ��URL��̵���Ǥ�
 ��������줿��̾�ݥꥷ��URL��̵���Ǥ�
 ��̾�򸡾ڤǤ��ޤ���Ǥ�������̾�ե�����
(.sig��.asc)�����ޥ�ɹԤκǽ�Ǥʤ����
�ʤ�ʤ����Ȥ�ǰƬ�ˤ����Ƥ���������
 ���ѥǡ����١���������Ƥ��ޤ�����gpg --fix-trustdb�פ�¹Ԥ��Ƥ���������
 ���θ������ˤ���������̩����%s�פ�����ޤ�!
 ���ΰŹ楢�르�ꥺ��ϡ�����뤵��Ƥ��ޤ���
��ä�ɸ��Ū�ʥ��르�ꥺ����Ѥ��Ƥ�������!
 ���ʽ�̾�Τʤ������Ǥ��礦
 ���Υ�å������ϡ�%s�Ǥϻ��ѤǤ��ʤ����⤷��ޤ���
 ���Υץ�åȥۡ�����ȡ������ץ������θƽФ��ˤϡ�����ե����뤬ɬ�פǤ�
 �Ź�ѥ��åȤθ�ID�ե������
������ ������̷�⤷�Ƥ��ޤ� toggle ��̩���ȸ������ΰ��������ؤ� ¿�������%c������
 pk����å���Υ���ȥ꡼��¿�����ޤ� - ���Ѷػ�
 trust ���ѥǡ����١����Υ��顼�Ǥ� ���ѥ쥳����%lu���׵ᤵ�줿��%d�ǤϤ���ޤ���
 ���ѥ쥳����%lu, �ꥯ�����ȡ�������%d: �ɽФ��˼��Ԥ��ޤ���: %s
 ���ѥ쥳����%lu, ������%d: ����ߤ˼��Ԥ��ޤ���: %s
 ���ѥǡ����١��� �쥳����%lu: �������˼��Ԥ��ޤ���: %s
 ���ѥǡ����١��� �쥳����%lu: ����ߤ˼��� (n=%d): %s
 ���ѥǡ����١����Υȥ�󥶥�������礭�����ޤ�
 ���ѥǡ����١���: �������˼��Ԥ��ޤ���: %s
 ���ѥǡ����١���: �ɽФ��˼��� (n=%d): %s
 ���ѥǡ����١���: Ʊ���˼��Ԥ��ޤ���: %s
 uid �ե���ID��ɽ����ǽ!
 �¹ԤǤ��ޤ��� %s "%s": %s
 �����ץ�������¹ԤǤ��ޤ���
 �����ץ������α������ɤ߹���ޤ���: %s
 exec-path��%s��������ǽ
 �Ź沽���褦�Ȥ��Ƥ��븰������IDEA�Ź��Ȥ��ޤ���
 �����Ƥ��Ƥʤ����ѥǡ����١����򹹿� ͽ����������: ͽ�����̥ǡ����Ǥ� ̤�����ΰŹ楢�르�ꥺ��Ǥ� ̤�����θ��������르�ꥺ��Ǥ� ̤�Τ� ̤�ΤΰŹ楢�르�ꥺ��Ǥ� ̤�Τΰ��̥��르�ꥺ��Ǥ� ����μ���͡�%s�פ����Ĥ���ޤ���
 ̤�Τ����󥢥르�ꥺ��Ǥ� ̤�ΤΥѥ��åȡ������פǤ� ̤�Τ��ݸ�르�ꥺ��Ǥ�
 ̤�Τθ��������르�ꥺ��Ǥ� ̤�Τν�̾���饹�Ǥ� ̤�ΤΥС������Ǥ� �����ץ�����ब���Լ����˽�λ
 ����URI�ϥ��ݡ��Ȥ��Ƥ��ޤ��� ���ѤǤ��ʤ����������르�ꥺ��Ǥ� ���ѤǤ��ʤ��������Ǥ� ���ѤǤ��ʤ���̩���Ǥ� �������С����鸰�������������� �����˼��Ԥ��ޤ���: %s
 ��̩���ι����˼��Ԥ��ޤ���: %s
 ���ѥǡ����١����򹹿� �����ΰ����򹹿� updpref �Ȥ���: gpg [���ץ����]  ���ϥե�����Ȥ��ƻ��� ����ƥ����ȡ��⡼�ɤ���� �ޤ���--delete-secret-keys�ץ��ץ����Ǥ���������Ƥ���������
 ����μ���ͤ˴���θ������ gpg����������Ȥ���� ��̾������ˤ��Υ桼����id
����� �桼����ID��%s�פϡ��⤦��������Ƥ��ޤ�
 �桼����ID: " ����%08lX��縰%08lX�����Ѥ��ޤ�
 ��Ĺ ��̾�򸡾� �夤���Ǥ� �夤�����Ǥ��ޤ��� - �Ƽ¹�
 �Ѥ�Ĺ���ΰŹ沽�Ѥߥ��å���� (%d)
 ľ�ܽ�̾��񤭹��ߤޤ�
 ���б��ؤν�̾��񤭹��ߤޤ�
 ��%s�פظ�������񤭹��ߤޤ�
 ��%s�פ���̩����񤭹��ߤޤ�
 ���ʽ�̾��񤭹��ߤޤ�
 ��%s�פؤν�Ф�
 ɸ����Ϥ˽񤭽Ф��ޤ�
 ���ä���̩�����Ѥ����Ƥ��ޤ� yY yes --pgp2�⡼�ɤǤ�PGP 2.x�����θ��ǥ��ꥢ��̾�����Ǥ��ޤ���
 --pgp2�⡼�ɤǤϡ�PGP 2.x�����θ���ʬΥ��̾�Ǥ�������Ǥ�
 --pgp2�⡼�ɤǤ�2048�ӥåȰʲ���RSA���ǰŹ沽�����Ǥ��ޤ���
 --pgp2�⡼�ɤǤ�ʬΥ��̾�����ꥢ��̾���������Ǥ��ޤ���
 --pgp2�⡼�ɤǤϽ�̾�ȰŹ��Ʊ���ˤǤ��ޤ���
 ��̾�����Ԥˤϡ����θ����Τ�Ǥ̿�Ǥ��ޤ���
 �Х��򸫤Ĥ����褦�Ǥ� ... (%s:%d)
 %s��%s�⡼�ɤ��Ѥ��뤳�ȤϤǤ��ޤ���
 --pgp2����ꤷ���顢(�ѥ��פǤʤ�) �ե��������ꤻ�ͤФʤ�ޤ���
 |�ե����뵭�һ�|���Υե����뵭�һҤ˾��֤�
�񤭽Ф� |�ե�����|��ĥ�⥸�塼��Υե������
�ɤ߹��� |�ۥ���|���θ����ˤ��θ������С������ |��ID|���θ�������Ū�˿��Ѥ��� |̾��|��̾�����Ѥ˰Ź沽 |CHARSET̾|ü����charset���CHARSET̾��
������ |̾��|����μ���ͤȤ���
��̾���פ���� |̾��|�������̩���Ȥ���
��̾���פ���� |̾��|��̾���פΰŹ楢�르�ꥺ���
���� |̾��|�ѥ��ե졼���ˡ�̾���פΰŹ�
���르�ꥺ������ |̾��|��̾���פΥ�å���������
���르�ꥺ������ |̾��|�ѥ��ե졼���ˡ�̾���פΥ��
���������󥢥르�ꥺ������ |N|���̥�٥��N������
(0���󰵽�) |̾��|��̾���פΰ��̥��르�ꥺ���
���� |N|�ѥ��ե졼�����⡼��N����� |[�ե�����]|���ꥢ��̾����� |[�ե�����]|��̾����� |�ե�����|���ơ����������ե������
�񤭽Ф� |[�ե����뷲]|�ե����뷲������ |[�ե����뷲]|�ե����뷲��Ź沽 |���르�ꥺ�� [�ե�����]|��å������������� 