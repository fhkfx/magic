��    �     d  Q  �>      �S  -   �S  #   T  )   CT  %   mT  �   �T    U     "V  1   :V  0   lV      �V  >   �V  =   �V  �   ;W  ;   X     =X     SX     oX     �X     �X     �X     �X  D    Y  .   EY  I   tY  8   �Y     �Y     Z     .Z     KZ     hZ     Z     �Z  "   �Z     �Z  #   �Z     [     4[     T[     m[  $   �[  &   �[  ,   �[     \      \     ;\     W\     u\     �\     �\     �\     �\     �\     �\  
   ]     !]     6]     H]     U]     o]  %   ~]     �]  
   �]     �]     �]     �]  +   
^  #   6^     Z^  -   s^  %   �^  ]   �^  Z   %_  H   �_      �_     �_     `  (   `  .   >`  3   m`  "   �`  )   �`     �`     	a     a     ,a     ?a     [a      ma     �a     �a     �a     �a     �a  )   b     2b     7b     Rb     kb     �b     �b     �b     �b     �b     �b     c  "   -c  %   Pc  &   vc  !   �c  %   �c  "   �c  #   d  '   ,d      Td     ud     �d     �d     �d     �d     �d  (   e  $   +e     Pe     ee  #   ye     �e     �e     �e  #   �e     f  &   6f  %   ]f  ,   �f  4   �f     �f     �f     g     /g     Dg     Xg     ng  '   �g     �g     �g     �g     �g     
h     h  "   4h     Wh     uh  -   �h  (   �h  0   �h  H   i  	  Wi     aj     nj     {j  �   �j  �   ?k  A   �k  /   <l  0   ll  \   �l  1   �l     ,m  C   Am  )   �m  -   �m  0   �m  .   n  '   =n     en     zn     �n     �n     �n  3   �n  4   o  -   8o  �   fo  .    p  .   Op     ~p  	   �p  	   �p     �p  .   �p  :   �p     q     #q     Aq  !   ]q  *   q  1   �q     �q  #   �q  &   r  "   @r  &   cr     �r     �r     �r     �r  <   �r  0   s  '   Es     ms  0   �s  '   �s  6   �s  G   t  @   et  >   �t  +   �t  =   u     Ou     du     tu  "   �u  :   �u     �u     �u     v      #v  /   Dv  �   tv     Ew  H   _w  -   �w  ,   �w     x  
   x     'x  8   :x  #   sx     �x     �x  "   �x  �   �x  E   py  �   �y  9   Lz  ;   �z  �   �z     v{     �{     �{  ;   �{  $   
|     /|     C|     [|     j|  �   �|  �   +}     �}     ~     .~     G~     ]~     p~     �~     �~  %   �~     �~  S   �~  �   +      �  G   �  !   +�  3   M�  <   ��     ��  "   ـ  ,   ��  %   )�  ,   O�  %   |�  2   ��     Ձ      �  (   �      ;�  
   \�  *   g�     ��     ��     ��     ӂ     �  ,   �     2�     L�  
   g�  �   r�  "   �     $�     A�     S�  d   s�     ؄     �  �   
�  E   ��  o   �      ^�      �  7   ��  '   ؆  c    �  /   d�  E   ��  ,   ڇ  )   �  #   1�  -   U�  )   ��  �   ��    ��  '   ��  �   ��     R�     [�     t�     }�     ��     ��  1   ��     �  (   ��  %   #�     I�  %   f�     ��     ��     Č  9   Ҍ     �     ,�     G�     Z�     n�      ��  }  ��     +�     :�  /   T�    ��  "   ��     Œ     Ԓ  *   �     �     1�  J   D�  j   ��  �   ��  9   ��  O   ��  �   �  5   ߕ  .   �  '   D�    l�  �   }�     A�     Y�     t�  !   ��     ��  "   Ș  '   �  �   �     �  -   '�     U�  �   h�     8�     U�  *   t�  +   ��     ˛     �     ��  %   �     ;�  7   U�     ��  r   �  1   ��  =   ��  I   �  4   :�     o�  #   ��  =   ��     �  ?   �  1   P�  %   ��  5   ��  A   ޟ  E    �  H   f�  -   ��  H   ݠ     &�  <   A�  )   ~�  C   ��  .   �  F   �  H   b�  2   ��  1   ޢ  9   �  ;   J�  %   ��  '   ��      ԣ  2   ��     (�  !   )�  h  K�  *   ��  &   ߪ  1   �  #   8�  2   \�  >   ��  �   Ϋ      ��  =   ��  0   ߬  M   �  "   ^�  &   ��  3   ��  f   ܭ  %   C�  D  i�  ,   ��  5   ۰     �     !�  _   2�     ��  
   ��     ��     ��     ϱ     ۱  %   ��     �  S   :�  5   ��  4   Ĳ     ��     �     �     1�  (   ?�  #   h�     ��     ��  
   ��     ��     ��  1   γ      �  
   �     �     6�     S�  E   o�     ��     Ӵ     ۴     �     �     ��     
�     �     (�     6�     L�     c�     |�     ~�     ��     ��     ĵ      ۵     ��     �  +   6�  !   b�  '   ��  (   ��  %   ն  2   ��  '   .�     V�     i�     z�     ��     ��     ��  "   ط     ��     �  #   ,�     P�  5   e�     ��     ��  ;   ĸ  <    �     =�     T�     j�     ��     ��  A   ��  &   ݹ     �     �     1�  -   @�  ;   n�  %   ��  (   к  +   ��     %�     ;�     Z�  1   v�     ��     ��     Ż     ܻ     �     �     �     -�     <�     Z�     a�     j�     q�  3   x�     ��     ��     ¼     ݼ     ��     �  %   2�     X�     _�     l�     ��      ��  3   ��  $   �  =   
�  %   H�     n�      ��     ��     Ǿ     �     ��     �  '   2�     Z�     u�  &   ��  &   ��     �     ��     �     1�     8�     D�     `�  K   }�  "   ��  %   ��  $   �     7�     H�     Z�     l�     x�     ��     ��     ��     ��     ��     ��     �  E   �  @   d�  @   ��     ��     ��     ��  !   �  D   2�  +   w�  /   ��     ��     ��     ��     ��     �  %   *�  %   P�  $   v�     ��     ��     ��  .   ��  '    �     (�     A�     ]�  3   z�     ��     ��     ��     ��     �     �  $   -�  &   R�  $   y�  '   ��     ��  ,   ��  '   �     8�     G�  M   K�  N   ��     ��  '   ��  /   &�  "   V�     y�      ��      ��     ��     ��     �  -   5�  0   c�  *   ��  '   ��  #   ��  1   �  %   =�  .   c�  ,   ��  &   ��  "   ��  0   	�  9   :�  8   t�  2   ��  "   ��  %   �  !   )�     K�  >   i�  3   ��  %   ��  (   �  2   +�     ^�  $   u�     ��  ;   ��  '   ��  #   �  $   @�  $   e�  +   ��  .   ��  ;   ��     !�  $   A�  7   f�  3   ��     ��     ��  $   
�  5   /�  S   e�  9   ��  ,   ��  <    �     ]�  G   u�  H   ��     �  B   �  "   Y�     |�     ��     ��     ��  0   ��  ;   �  2   N�     ��     ��     ��     ��  	   ��     ��     ��  !   ��     �     '�     B�     S�     c�     i�  '   ��     ��     ��  .   ��     �  (   �  )   C�  ,   m�     ��     ��     ��  
   ��     ��  $   ��     �     6�  )   9�      c�     ��  %   ��     ��  &   ��  "   �     0�     ?�     O�     _�     }�     ��  %   ��  %   ��     ��     �     %�     8�     F�      T�  #   u�     ��     ��  &   ��  )   ��     ��  <   �     U�     k�     ��  ,   ��     ��     ��  ;   ��  >   +�  G   j�     ��     ��     ��     ��     �     &�     B�     Z�  "   b�  -   ��  ,   ��     ��  +   ��  *   &�  8   Q�  9   ��     ��  !   ��  &   �  $   -�     R�     g�  5   |�     ��     ��     ��     ��  I   ��     �     %�     <�     O�     j�     }�  #   ��  #   ��     ��     ��  !   
�     ,�  %   I�     o�     v�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��  &   �     F�     e�  #   ~�     ��     ��  3   ��  %   ��  %   $�  =   J�  =   ��     ��     ��     ��     ��     �  %   �  	   6�     @�     I�  
   N�     Y�  $   l�     ��     ��     ��     ��  &   ��     �  "   !�     D�     X�     a�     s�  V   ��      ��  5   �  $   ;�     `�  +   {�  #   ��  4   ��  %    �  
   &�  *   1�  $   \�  0   ��     ��     ��  &   ��  .   ��  *   *�  �   U�  :   ��  +   !�  E   M�  /   ��  %   ��  A   ��  &   +�     R�     e�  ,   l�     ��  (   ��     ��     ��  -   ��  /   &�  ,   V�  "   ��  )   ��     ��     ��      
�     +�     E�     I�  #   h�  -   ��     ��  I   ��      #�     D�     V�     f�     ��     ��     ��     ��     ��     �     %�     C�     \�     t�  #   ��     ��     ��     ��     ��      ��     �     .�     H�     b�     v�     ~�     ��     ��  6   ��  (   ��     �  #   1�      U�  
   v�  7   ��     ��     ��     ��     ��  -   ��     (�     B�     a�     }�     ��     ��     ��     ��     ��     ��  D   ��  F   7�  E   ~�  D   ��  A   	�  7   K�     ��  $   ��  F   ��      �      -�  '   N�      v�     ��  "   ��  #   ��  $   ��     �  /   :�  '   j�  7   ��  $   ��     ��     �  #   $�     H�  !   a�     ��     ��  #   ��  -  ��  7   �  1   ;�  -   m�  )   ��  �   ��     ��     ��  +   ��  .   ��  "   $�  =   G�  @   ��  �   ��  S   ��     ��     �     &�     D�     d�     ��     ��  @   ��  /   ��  C   .�  D   r�     ��     ��  '   ��  )   �     C�     ]�      y�  )   ��  )   ��  -   ��  %   �  )   B�      l�     ��  "   ��  &   ��  &   ��  !   �      6�  #   W�  %   {�     ��  "   ��     ��     ��          $     B     ^     p     �     �     �     �  )   �     �      "   * !   M    o 4   � -   �    � )    )   , y   V g   � F   8 )       �    � +   � 2    5   9 +   o 6   � *   �    �         #   8    \ &   q    �    � ,   �     �     )   &    P     U     v    � '   � ,   �     ,   &    S    s    � 0   � 2   � 3    .   D 1   s -   � '   � 2   � .   .	     ]	    ~	 $   �	    �	    �	    �	 1   
 :   >
    y
    �
 "   �
 $   �
 $   �
 %    )   ?    i +   � ,   � 3   � <       K    g    �    �    �    �    � ,   �     $ "   E    h        �    � %   �    �     /    -   L 1   z A   � R  �    A    O *   [ �   � �   g L   9 ;   � :   � i   � 7   g     � ?   � )     -   * ,   X -   � 2   �    �    � #    ,   9    f ?   n A   � 6   � �   ' 4   � 2       J    \    e 
   n =   y E   �    �    
    ( +   F 7   r 4   � '   � +    0   3 /   d .   �    �    �    � 
    C    4   ] ,   �    � 4   � ,    D   A J   � =   � :    1   J 0   |    �    � '   � 2    E   5 $   {    �     � )   � A    �   M &   - K   T 4   � 8   �         %     2  ;   O  .   �     �     �  +   �  �   ! P   �! �   �! H   �" N   	# �   X# !   6$     X$ %   y$ F   �$ &   �$    %    !%    :% '   O% �   w% �   '& "   ' "   &'    I' "   `'    �'    �'    �'     �' 5   �' 
   $( [   /( �   �( .   >) i   m) ,   �) A   * R   F* "   �* 1   �* B   �* 0   1+ 6   b+ 2   �+ 9   �+ (   , 7   /, 1   g, 0   �, 
   �, 7   �,    - &   +-    R- +   i-    �- 2   �- !   �-    . 
   #. �   .. /   �. %   /    B/ ,   [/ d   �/    �/ $   �/ �   $0 D   �0 �   1 .   �1 %   �1 @   �1 5   42 p   j2 8   �2 =   3 1   R3 1   �3 )   �3 .   �3 4   4   D4   G5 4   J6 �   6 
   7    7    67 (   C7    l7    �7 :   �7    �7 0   �7 8   !8 -   Z8 8   �8 -   �8 &   �8    9 G   -9 %   u9    �9    �9    �9 /   �9 '   : �  ?:    < #   < 9   B< �  |< 7   E@    }@ !   �@ /   �@    �@    �@ ^   A �   pA �   B ;   �B N   �B �   C 7   D 4   ED 7   zD 9  �D �   �E    �F #   �F    G *   +G "   VG 1   yG -   �G   �G    �H /   I    CI �   WI *   NJ ,   yJ /   �J 0   �J %   K    -K !   IK 6   kK '   �K ?   �K �   
L �   �L D   KM N   �M W   �M G   7N 1   N 2   �N D   �N +   )O J   UO 9   �O 5   �O J   P O   [P \   �P Z   Q <   cQ I   �Q (   �Q K   R ;   _R T   �R 8   �R h   )S o   �S >   T 9   AT D   {T B   �T 1   U .   5U )   dU >   �U   �U &   �V (  W .   8\ '   g\ ?   �\ 1   �\ 4   ] 9   6] �   p] !   "^ L   D^ B   �^ Z   �^ ,   /_ -   \_ E   �_ �   �_     U` ~  v` 1   �b @   'c    hc    xc l   �c    �c    d 
   d    &d 	   6d    @d +   Vd "   �d e   �d >   e >   Je    �e     �e $   �e    �e 5   �e 1   2f    df    kf 
   tf    f #   �f 8   �f    �f    �f (   	g (   2g &   [g D   �g *   �g    �g    �g    h    h    #h    5h    Ih    ]h )   kh    �h    �h    �h "   �h    �h     i "   "i (   Ei +   ni 9   �i >   �i 2   j :   Fj 9   �j 7   �j B   �j 1   6k    hk    �k     �k #   �k 1   �k !   l 7   =l     ul #   �l 1   �l +   �l E   m    ^m    ~m G   �m X   �m    8n !   Un    wn    �n    �n E   �n 6   �n    .o    Ho    ]o D   mo T   �o &   p 2   .p 5   ap    �p 5   �p $   �p A   	q    Kq    dq +   jq    �q    �q $   �q    �q    r $   r    <r    Cr    Lr    Sr G   Zr    �r    �r $   �r $   �r    s    's 5   ;s    qs    xs    �s    �s $   �s @   �s (   #t K   Lt 2   �t '   �t .   �t ,   "u 1   Ou    �u &   �u 0   �u :   �u %   /v +   Uv 0   �v 0   �v    �v %    w +   &w    Rw    Yw +   jw    �w S   �w 2   	x '   <x 0   dx    �x    �x    �x    �x    �x    y    (y    Dy 4   \y !   �y     �y     �y ]   �y Y   Sz T   �z    {    { %   { (   :{ W   c{ 5   �{ 8   �{    *|    /| -   8|    f|    �| 7   �| ,   �| 2   }    ;}    L} !   _} :   �} 1   �}    �}    ~    .~ <   F~    �~ !   �~    �~    �~    �~     2    7   R 7   � -   � !   � :   � .   M�    |�    �� a   �� a   �    T� 6   k� 7   �� %   ځ %    � "   &� "   I� #   l� !   ��    �� 6   т 5   � 4   >� 3   s� 0   �� 7   ؃ )   � D   :� C   � 1   Ä 0   �� 3   &� ?   Z� C   �� 7   ޅ -   � -   D� (   r� )   �� O   ņ M   � =   c� >   �� C   �� *   $� 3   O� &   �� @   �� :   � 5   &� 3   \� 0   �� :   �� :   �� H   7� '   �� /   �� B   ؊ :   � %   V� $   |� +   �� H   ͋ w   � E   �� >   Ԍ O   �    c� [   �� [   ލ    :� Z   K� -   ��    Ԏ    � *   � 1   9� P   k� J   �� N   �    V�    X�    t� '   y�    �� >   �� ,   �� &   ,�     S�    t� '   ��    ��    ё *   ב ?   �    B�    b� 7   s� *   �� 2   ֒ 0   	� -   :�    h� *   k�    �� 	   ��     �� '   Г .   ��    '� 5   +� $   a� *   �� +   �� *   ݔ 7   � *   @�    k� $   �� "   �� %   ʕ    � %   	� ,   /� ,   \�    ��    ��    ��    ɖ    ږ ,   � 3   �    M�    U� (   \� .   ��    �� @   ԗ    �    /� .   L� 5   {� !   ��    Ә ;   ژ D   � M   [� +   ��    ՙ     ڙ $   �� !    � #   B� #   f�    �� /   �� <   Ú .    �    /� /   J� A   z� H   �� H   � (   N� ,   w� /   �� 5   Ԝ    
�    &� F   D�    ��    ��    ��    �� Y   ��    �    (�    E�    X�    t�    �� /   �� /   О      � 
   !�    ,� #   K� (   o�    ��    �� $   ��    ֟    �    ��    � !   �    /�    1�    6� -   L� ,   z� &   �� %   Π 2   ��     '�    H� E   Z� :   �� 5   ۡ G   � K   Y�    ��    ¢    ʢ    �    �� 9   � 	   Q�    [�    d�    i�    �� 0   �� %   ϣ $   ��    �    0� /   M� $   }�    ��    �� 	   ٤    �     �� h   � .   �� H   �� '   ��     $� 1   E� B   w� B   �� "   ��     � 0   0� /   a� =   ��    ϧ +   ѧ .   �� <   ,� 7   i� �   �� C   6� 7   z� `   �� 3   � 8   G� K   �� 6   ̪    �    � <   %�    b� 9   ��    �� '   �� 6   � 2   � /   R� $   �� +   ��    Ӭ    � !   �    0�    K� #   O� /   s� A   �� %   � W   � 8   c�    ��    �� )   ͮ ,   �� "   $�    G� )   e�    ��    ��    į %   �    
�    "� +   1�    ]� 0   s� !   �� !   ư 6   �    � !   8� )   Z�    ��    ��    ��    �� #   б J   �� ?   ?�    � D   �� #   ز 
   �� B   � 	   J�    T�    n� 5   }� @   ��    �� %   � '   4� '   \�    ��    ��    ��    Ĵ    �    � K   � T   5� N   �� L   ٵ P   &� L   w�    Ķ .   � D   � -   U� /   �� >   �� ,   �    � 8   >� 6   w� 7   �� ,   � @   � 2   T� I   �� 4   ѹ    � '   &� 7   N� #   �� 7   �� "   �    � 4   %�        �  E              �  Y       �   _   7      B   H          �  �   1   �  �  �  ?  �  9  �   �  0  8  n      �      �      �  �   �  �  �  �  |  �      a   �       f      �  �  �  �  �  /  �   �  �           �  �  ]                �  �        |  W   �    )   .  Y          ]  �      �      �  i  �  �   /       �            Z  �      w  1      �          �  �  �  f   �      *      �   m   c  �       �  �  
        �  �  �  �  �  ~   �             �   �             `  �         '  ^  P  �   �   +  r  �  i  �  �         �   �  �  �    :  �         �  '  %      �              �   �  �                D       t               �  	   H  [  `      v  �  q  &   "   -      .      E  �                  �  -       �            �  �  �   �   j  �   �  x  R      �  J   �  +      �       �  �  -  =  F      .  h      2   �  �   �  �      �       b  �   �    �   }                �  (      y         q  �  �          �  Y              �   �           �     �  �   �  &  �        �       1  �  �           �  �   �      �  �  B  �  �  �  �  x   X  {   �  &      �   z   �           �  N     �  M         �   �  �      L   �   q  �      �  �   B    �           -      �         �   �    w  �  �  �  K   �  �  K      8  �  f      4  !      {  �  T              �   �     I    ]      R  �   �      �   �  u  r  5  t  ?   <     �      C      �  G          �  �      >                  �      �  �       �  �       �        �  �       �  �       �  "  �  ,  7  �    �   �      z  �         M  �  �   �        �      �  �  �         �   T   �       s   y              D  X  �  3  �  �       F        ,      �  N  .   �  A   �  �  o  �       C  @  i  �  �   0       �  �     �  �      [  S     ;   2  �  f    �  H          I      e   5  �              �      U  �          �  �   ;  �  �  �    �  O  �  0          �        F   |       2    [           �  N   U  �    �  �           �    }   
        :           �  �      D  �      c          "  n   b       D          �  6  K    �  �  �  �  �   N  �      �  �      �  7      �  �         �      �   g  �  �  �  n  �   �  /     �  :  �  :        U  8   9  �       p  l  �   G      1  �  4       g  G   �  �  �   k   j  e  �  0   �      �  @  �         	      �          �  g  v  d  2  �  �      Q  #  @   �   P  �   _      �   d      @    �  A  �   s  �    <  �   O  �  p  �       �       Q   j    j   /  �  �   �      u      Z              P  �       )  �        �  e      "  �      w       `           I   �    �        9  �      �  �      $  >  J  V   %   �      \      �        g   h   3  >       o      �  [  �   �  �  C   �  )  �  W  �     �           {      9   �       '      $  �      5  %  �  W  ?      �  �  �  S       ,      z  a  �  �  z  a  �    �   V  _              �   r      L  y  3       �  �  t  A  +  l  �  ,           �   �  V  |  �  Z   �          �  �      G        �  �   =       �  l   �     �  �  W      �    �  k      _  t       �      �      �  \      �   �   �      L  �     *      �  �  �   �   =  \   �              �          �  �          �    �  �   �       �  \  x  �  S  �      �  }      u   )       �  Z  �      �  �          R  �   J  4      �  6  �   �  �                   d        S  �           A  x  �        n      �  �  �  �  �  �      �     #   �    �   �   �     �           e            R       ^       �   �    O       H   <            q   (  K      s  �  �  �     �  �   �  �      �  �  P       �  k      �      �       �      �  �  v  �  �  �          �       M  	      �  r   �           
       {  ~  �   Y  �  p   �  �  k  �  m  �  ^  �  �      o        �  �  �            �                    �  �      3  �  C      ^  �         I  	  E              �               $   <  6      �    u  b  U   �   O  a  Q  T  �               �  �          b      �  �  �          �   %  �          y  o          *   �    X   �              �  h      �   �  V      �      ]  �  &      �  E  �  �                  �      �  L  c          #  m  �  �  ;  �  8          �     ~  �      �  �  �  �      ~  X         �       �  �  �             !      �  �  6   �  c   �          �   ?  �      �  �  �  �  `          B  
  (   '  �      �   i   ;  �  �  !  �    }          l  �   �               �   �  �   �      �  �        w  h  �      �  (  �     �  =  7   �      �       �          m          �  �   �  �             �  �   �  �   �      �           �  �  5       4      �                      �          p  +   Q      �  T  �    M   s    �  *    F  d   �  �  �       �      !     $  #      �          �    �  �  J  �          �        �       v                           >              �  �        
Enter the user ID.  End with an empty line:  
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Key fingerprint =      Subkey fingerprint:    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
    signed by %08lX at %s%s
    signed by %08lX at %s%s%s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  Primary key fingerprint:  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" is not a JPEG file
 "%s" was already locally signed by key %08lX
 "%s" was already signed by key %08lX
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s ...
 %s does not expire at all
 %s encrypted data
 %s encryption will be used
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s signature from: "%s"
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't make lock
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  ASCII armored output forced.
 About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to use it (y/N)?  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Cipher:  Command>  Comment:  Compression:  Create a revocation certificate for this key?  Create a revocation certificate for this signature? (y/N)  Create anyway?  Critical signature notation:  Critical signature policy:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Displaying %s photo ID of size %ld for key 0x%08lX (uid %d)
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Experimental algorithms should not be used!
 Expired signature from " Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid passphrase; please try again Invalid selection.
 Is this correct (y/n)?  Is this okay?  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: Elgamal primary key detected - this may take some time to import
 NOTE: This key is not protected!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Policy:  Primary key fingerprint: Pubkey:  Public key is disabled.
 Quit without saving?  Real name:  Really create the revocation certificates? (y/N)  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really revoke all selected user IDs?  Really revoke this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Really update the preferences?  Reason for revocation: %s
 Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Signature notation:  Signature policy:  Syntax: gpg [options] [files]
Check signatures against known trusted keys
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The use of this algorithm is only supported by GnuPG.  You will not be
able to use this key to communicate with PGP users.  This algorithm is also
very slow, and may not be as secure as the other choices.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key  This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To be revoked by:
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 Unable to open photo "%s": %s
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) Use this key anyway?  User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: signature digest conflict in message
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy url (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe enclosing directory ownership on %s "%s"
 WARNING: unsafe enclosing directory permissions on %s "%s"
 WARNING: unsafe ownership on %s "%s"
 WARNING: unsafe permissions on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [filename] [revocation] [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid long keyID
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption anonymous recipient; trying secret key %08lX ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %08lX due to an unknown critical bit
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet build_packet failed: %s
 c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't get server read FD for the agent
 can't get server write FD for the agent
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open file: %s
 can't open signed data `%s'
 can't open the keyring can't query password in batchmode
 can't read `%s': %s
 can't search keyserver: %s
 can't set client pid for the agent
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking at depth %d signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 checking created signature failed: %s
 checking keyring `%s'
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 cipher extension "%s" not loaded due to unsafe permissions
 communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID deleting keyblock failed: %s
 delkey delphoto delsig deluid digest algorithm `%s' is read-only in this release
 disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error finding trust record: %s
 error in trailer line
 error reading `%s': %s
 error reading keyblock: %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 error: invalid fingerprint
 error: missing colon
 error: no ownertrust value
 expire export keys export keys to a key server export the ownertrust values external program calls are disabled due to unsafe options file permissions
 failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing digest algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 help iImMqQsS import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid symkey algorithm detected (%d)
 invalid value
 key key %08lX has been created %lu second in future (time warp or clock problem)
 key %08lX has been created %lu seconds in future (time warp or clock problem)
 key %08lX incomplete
 key %08lX marked as ultimately trusted
 key %08lX occurs more than once in the trustdb
 key %08lX: "%s" %d new signatures
 key %08lX: "%s" %d new subkeys
 key %08lX: "%s" %d new user IDs
 key %08lX: "%s" 1 new signature
 key %08lX: "%s" 1 new subkey
 key %08lX: "%s" 1 new user ID
 key %08lX: "%s" not changed
 key %08lX: "%s" revocation certificate added
 key %08lX: "%s" revocation certificate imported
 key %08lX: HKP subkey corruption repaired
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted as trusted key
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: direct key signature added
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid self-signature on user id "%s"
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey binding
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: this is a PGP generated ElGamal key which is NOT secure for signatures!
 key %08lX: unexpected signature class (0x%02X) - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key `%s' not found: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key incomplete
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l line too long
 list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never      never use a MDC for encryption new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition "%s"
 no corresponding public key: %s
 no default secret keyring: %s
 no entropy gathering module detected
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for `%s'
 no secret key
 no signed data
 no such user id no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again passphrase too long
 passwd please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 please use "%s%s" instead
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent - disabling agent use
 problem with the agent: agent returns 0x%lx
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key does not match secret key!
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read error: %s
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring requesting key %08lX from %s
 resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke a user ID revoke signatures revsig revuid rounded up to %u bits
 s save save and quit search for keys on a key server searching for "%s" from HKP server %s
 secret key `%s' not found: %s
 secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key locally and non-revocably sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user id "%s"
 sorry, can't do this in batch mode
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 system error while calling external program: %s
 t take the keys from this keyring the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temp files when calling external programs
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to execute %s "%s": %s
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown protection algorithm
 unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg-1.1.92
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-04-25 18:03+0200
Last-Translator: Dokianakis Theofanis <madf@hellug.gr>
Language-Team: Greek <nls@tux.hellug.gr>
MIME-Version: 1.0
Content-Type: text/plain; charset=CP1253
Content-Transfer-Encoding: 8bit
 
�������������� �� user ID. ����� �� ��� ����� ������:  
��� ����� ����������� ������ �� ���� �� ������.
 
��� ������� ���� ���������� ���� �� ������.
 
��� ��� ������� ������� ���� �� ������.
 
��� �������� ������ ��������� ������ bytes.  ����������� �� ���������� �
�� ����������� �� ����������� ������� ����� ���� �� ������������
����������� ��������! (����������� %d ����������� bytes)
 
�������� ��� ������ ��� �� photo ID.  � ������ ���� ������ �� ����� ������
JPEG. ��������� ��� � ������ ������������ ���� ��� ������� ������ ���.  ���
�������������� ��� ������ ������ �� ������ ��� ���������� �� ����� ������!
������� ������� ��� ��� ������ ����� ���� ����� ��� 240x288.
 
��������������� ����������:
 
� �������� �� ��������� ��� ��-���������.
 
� �������� �� ��������� ��� ��-�����������.

 
���� �� ����� ��� ����-��������.
 
�������������: � �������� �� �� ��������� ��� ��-���������.
 
�������������: � �������� �� �� ��������� ��� ��-�����������.

 
���������� ��� User-ID ��� �� ������������ ��� ������. �� ��������� ������������
�� user-id ��� �� ������� �����, ������ ��� ��������� Email ����� ����:
    "Nikolaoy Nikos (toy Ioanni) <nikoln@athens.gr>"

 
���������� ��� ����� ������ ��� �� ������������ �� ������� ������
��� �� ������: "                 ������ ��� "               ����������: %lu              ����������: %lu
            ��� ����������: %lu
           ��� user ID: %lu
           ��  ����������: %lu
           ����� user ID: %lu
          ��� ����� ������ ��� � �������� ������ ���� ���������.
          � �������� ������ ����� ������������.
          ��� ������� ������� ��� � �������� ������ ���� ���������.
          ���� ������ �� �������� ��� � �������� ����� ������������.
         ���� ���������: %lu
       ��������� �����������:       ����������� ������� �������: %lu
       ��� ������� ��� �������������: %lu
      ��������� �������� =      ��������� �����������:    (%d) DSA (��� �������� ����)
    (%d) DSA ��� ElGamal (��������������)
    (%d) ElGamal (��� ������������� ����)
    (%d) ElGamal (�������� ��� �������������)
    (%d) RSA (��� ������������� ����)
    (%d) RSA (�������� ��� �������������)
    (%d) RSA (��� �������� ����)
    (0) ��� ������.%s
    (1) ��� ��� ������� �������.%s
    (2) ��� ����� ��� ������ ������.%s
    (3) ��� ����� ���������� ������.%s
    ���� ���������� ��������: %lu
    ���������� ��� %08lX ���� %s
    ����������� ��� %08lX ���� %s%s
    ����������� ��� %08lX ���� %s%s%s
   �������� ���������.
   ���������� ������� �������: %lu
  %d = ��� ����
  %d =  ��� ��� �����������
  %d = ��� ����� �����������
  %d = ��� ������ �����������
  %d = ������������ �������
  (��������������)  (����� ������, ID %08lX)  (��-���������)  (���������)  ��������� �������� ��������:  [�����: %s]  i = ���������� ������������ �����������
  m = ���� ��� ������ �����
  q = �����������
  s = ��������� ����� ��� ��������
  ���������� ������� �������: %lu
  �����������: %c/%c "
����������� ������ �� �� ������ ��� %08lX ���� %s
 "
����������� �� �� ������ ��� %08lX ���� %s
 "%s" ��� ����� JPEG ������
 "%s" ��� ����������� ��� �� ������ %08lX
 "%s" ��� ����������� ��� �� ������ %08lX
 # ����� ��� ������������ ����� ������������, ������������� %s
# (����� ��� "gpg --import-ownertrust" ��� ��������� ����)
 %08lX: ��� ����� ������ ��� ���� �� ������ ������ ���� ���������
���� �� ���� ���������� ������� �����
 %08lX: ��� ������� ������� ��� � �������� ���� ������ ���� ���������.
 %08lX: ��� �������������� ���� �� ������
 %08lX: �� ������ ���� �����
 %d ����� ���������
 %d ��������� ��� ���������� ���� ���������
 %d ��������� ��� ���������� ���� ������� ��������
 %d user ID ������������ ����� ������� ����-���������
 %lu ������� ����� �������� (%lu ���������)
 %lu ������� ����� ����� ���� �������� (%lu ���������)
 %lu ������� ����� ����� ���� ������������
 %s ...
 �� %s ��� ����� ����
 %s ��������������� ��������
 %s ������������� �� ��������������
 �� %s ����� ���� %s
 �� %s ��� ����� ������ ��� ����������
 %s ����� �� ���
 %s ����� �� ����������
 �� %s ��� ���� ������ ������ ���� �� �� %s!
 �� %s ��� ����������� �� �� %s!
 %s �������� ���: "%s"
 %s%c %4u%c/%08lX  ����������: %s ����: %s %s.
 %s/%s ��������������� ���: "%s"
 %s: �������������: ����� ������
 %s: �������� ���������: %s
 %s: �������� ����������� ���������: %s
 %s: �������� ����������� ����������� (lock)
 %s: �������� �����������: %s
 %s: �������� ����������� ����������� (lock)
 %s: �������� ��������� ���: %s
 %s: ��������� �������������
 %s: � ������� ��� �������!
 %s: ������ ���� �������� ��� �������� free : %s
 %s: ������ ���� �������� ��� �������� �������: %s
 %s: ������ ���� ��������� ��� �������� �������: %s
 %s: ������ ���� ������� ��� �������� dir : %s
 %s: ������ ���� ������� ��� �������� �������: %s
 %s: �������� ���� �������� ���� ��������: %s
 %s: �������� ����������� hashtable: %s
 %s: �������� ����������� ���� �������� �������: %s %s: �������� ���� ��������� ���� ��������: %s
 %s: �� ������ ������ ������� %d
 %s: �� ������ trustdb
 %s: ������������� �� ������ trustdb
 %s: ���������� �������������
 %s: ��� ����� trustdb ������
 %s: ������������: %s
 %s: ������������: ������� ������ ����� ��� �����
 %s: ������������: �� ������� ������ ���� ���������������.
 %s: ������������� � trustdb
 %s: ������� ��������
 %s: ������� ������� �� recnum %lu
 %s:%d: �� ������������ ������� "%s"
 %s:%d: �� ������� �������� ��������
 %s:%d: �� ������� �������� ���������
 %u-bit %s ������, ID %08lX, ���������� %s (��� ������ ���������)
 (������� �� ������ �� ��������� �� %d ���)
 (���� ����� ��� ��������� ������ ���������)
 (����� ��� ������������� ��� ������ ��� ���������)
 (���� ��������������� ����� ��������� ��� ���� ��� �������)
 --clearsign [����� �������] --decrypt [����� �������] --edit-key user-id [�������] --encrypt [����� �������] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output ��� ���������� ��� ���� ��� ������
 --sign --encrypt [����� �������] --sign --symmetric [����� �������] --sign [����� �������] --sign-key user-id --store [����� �������] --symmetric [����� �������] -k[v][v][v][c] [user-id] [����������] ... ���� ����� bug (%s:%d:%s)
 1 ���� ��������
 1 �������� ��� ��������� ���� ������� ��������
 1 �������� ��� ��������� ���� ���� ���������
 1 user ID ����������� ����� ������ ����-��������
 @
(����� �� ������ man ��� ��� ����� ����� ������� ��� ��������)
 @
������������:

 -se -r Bob [������]          �������� ��� ������������� ��� �� Bob
 --clearsign [������]         ���������� �� ���������������� ���������
 --detach-sign [������]       ���������� ����������� ���������
 --list-keys [�������]        ���������� ��������
 --fingerprint [�������]      ���������� ������������ (fingerprints)
 @
��������:
  @�������:
  ������������ ������ �� ����������� ASCII.
 �� ���������� ����������� ���� ���� %s keypair.
              �������� ������� �������� �����  768 bits
              �������������� ������� �������� ����� 1024 bits
    ������� ������������ ������� �������� ����� 2048 bits
 �� ��� ���� �� ������� ������������� ��� RFC2440 ��� ������������
������ ��� �������������� ��� ��� �� ����������� ��� �� ��������� ���
����� ������������ ��� ���� ����� ������� ��� ���� ����� ���� ����������. ��������� "yes" (� ���� "y") ��� ����� ������� �� ������������ �� ���������. ��������� "yes" ��� ����� ������� �� ��������� �� ��������� ��������� "yes" ��� ����� ������� �� ����������� �� ������ ��������� "yes" ��� ���������� ������ �� ��������� ����
�� user ID.  ��� �� ������������� �� ������ ����! ��������� "yes" ��� ������ �� ���������� ��� �� user ID ��������� "yes"(���) � "no"(���) ������� ������ �� ���������� ���� �� ������
�� �� ������ ���: " ������� ������ ���� �� ������� ��������;  ������� ����� ������ �� �� ����������; (y/N)  ������� ������ �� ��������� ���� �� ������;  ������� ������ ����� �� �� ����������; (y/N)  ������� ������ ����� �� �� ���������������; (y/N)  ���� �������� ��� " ������ CRC: %06lx - %06lx
 �������� ������� ��� ���������: %s
 ������� � ����������� ����� ��� ��������:%s
 ������� ������������� ��� ������� �� ��� ������ �������� ������������:
 ������ (N)�����, (C)������, (E)mail � (O)�������/(Q)�����������;  ������ (N)�����, (C)������, (E)mail � (Q)�����������;  ������� ��� ����������� ���� ��� user ID (� ���� ��� �����������)
���� ������������ ����� �����������.  � ���������� ���� ��� ������������
����-��������� �� ������� ���� 1 ������������.
 ������ ����������� ����� ��� ��� ���������� ������.
 ������ ����������� ����� ��� ��� �������� ������.
 ���������������:  ������>  ������:  ��������:  ���������� ���� �������������� ��������� ��� ���� �� ������;  ���������� ���� �������������� ��������� ��� ���� ��� ��������; (y/N) ����������;  �������� �������� ���������:  �������� �������� ���������:  �� ������ �������� DSA �� ����� 1024 bits.
 � DSA ��������� ���� ������� �������� ��� 512 ��� 1024
 � DSA ������� �� ����� ���� 160 bit ���������� hash
 ��������-��������� ���� ������� � stdin �������� ����� ��� ����� ���������; (y/N/q) �������� ����� ��� �� ������� ���������; (y/N/q) �������� ����� ��� �������� ��� �� ����������;  �������� ����� ��� �������� ���������; (y/N/q) ����������� %d ��������.
 ����������� %d ���������.
 ���������� ��������.
 ��������:  ���������� %s photo ID �������� %ld ��� �� ������ 0x%08lX (uid %d)
 ������� ������ �� ���������� �� ���������� �������;  ������� ������ �� ��������� ���� �� ������;  ������� ������ �� ������ ����;  ������� ������ �� ���������� �� ���������� �������;  ������� ������ �� ��������� ���� �� ������;  ������� ������ ���� �� ������ �� ������� ��� �������� ������������;  ������ �� ������� ��� ��� �������� ���� ������������� ��� ��������; (y/N)  ������ �� �� �������� �� ��� ����� ��������� ��������; (y/N)  ������ �� �� �������� �� ��� OpenPGP ����-��������; (y/N)  ������� ������ ����� �� �� ��������������; (y/N)  ������ � ���������� �� ����� ����������;  (Y/n)  �� ���������� ��� Photo ID ��������� Email:  ��������-��������� ���� ������� � stdin �������������� ��� ����� ������� ��� �� photo ID:  �������������� ��� ����������� ��������޷ ����� �� ��� ����� ������:
 �������������� ��� ��� ����� ������� �������������� �� ����� ������
 �������������� �� ����� ������:  �������������� �� ����� ��� ������������� �������������� ��� ��� ����� ������ ��� ���� �� ������� ������.

 �������������� ��� ����������� ���� ���� ������������.
�������� �� �������� ��� ���������� ISO (YYYY-MM-DD) ����
��� �� ������ ��� ���� �������� ��������� - �������� �� �������
��������� �� ���������� ��� ���� ��� ��������. �������������� �� ������� ��� �������� �������������� �� user ID ��� �������� ����� ��� ������ �� �������� ������. �������������� �� user ID ��� ����������� ��������:  ������������ ���������� ��� ������ �� ����������������!
 ������� �������� ��� " ����������:  �� ������ `%s' ������� ���.  ����� �� ����� ��� ������� ��� ����� ����������� � �������� �������� ���� �� ���������� �� ������ ��� ...
 ���� �������� ��� " Hash:  ��������: �������� �� user ID ��� ��������
 ����� �������� ��� �������� ���������� ��� �� ������ ��� ����� ������� ��
������ �������� ��� ����������� �����; ��� ��� ������ �� �� ���������� �������"0".
 ��������������� IDEA �� ����������, ��������� ���������� ������ ���
%s ��������
 ��� ����������, �������� �� ��������������� ��� ������� ���
���������� ��� ���� ��� ��������� ���� �� ������������� ���������.
�������� �������� ���� �� ������� ���������. ��� ����� ������
����� �� �������.
 ��� ������ �� ��������������� ���� �� ��������� ������, ��������� "yes". ��� ������ �� ��������������� ���� �� �� ������������ ������, ��������� "yes". ������, ��� ����� ���� ���� �� ��������������� �� ���� ������ ��� ��������
��� �������������. ����� � ���������� ������ �� ��������������� ���� ��
���������� ������. � �������� ���� ������� �� ������ ��������� ����������. �� ������� ���������� ��� ������
 �� ������� ���������� ��� �����
 �� ������ ������  (��������� "help")
 �� ������ ������ %08lX ����� ������ ��� �� --allow-non-selfsigned-uid
 �� ������ ����� ������, ��������� ���� �� ������ �������.
 ����� ���� ����� (y/n);  ����� ���� �������;  ����� ���� � ���������� ����� (y/N/q);  ��� ����� ������ ��� ���� �� ������ ������ ��� ����� �� �����
��������� �� user ID. ��� *����������* ������ �� ������, ��������
�� ���������� ���� ������� ������� ����������

 ����� ������� ��� �� ������� ���� ���. ���� � ���� ��� �� �������
���� �� ����� �������.  �� ������������ ��� �������� ��� �����-������������,
��� ���� ������ �� ����� �� ��� (������� �������������) ����-��������������. � ���������� �������� ����������.
 � ���������� �������� �������: %s
 �� ������ ���� ������� �� ������ �� ��������������� ����� �� ������ �������������.
 �� ������ ����������. �� ������ ���� ���������� �� ������ ����� ������ ���; (0)  �� ������ ��� ������ ����� ��� ���������� ���������.
 ���������� ������ �������� ���������� ��� 2048 ��� ����������� �����
�� ����������� ����� ����������!
 N  ��� ������ ��� ��������.
C  ��� ������ ��� �������.
E  ��� ������ ��� ���������� email.
O  ��� �������� ��� ����������� ��������.
Q  ��� �� ����������� �� ���������� ��������. ��������: �� %s ��� ����� ��� �������� �����!
 ��������: ���������� ����������� �������� Elgamal - ���� ����� ���� �����
          ��� ��� �������� ���
 ��������: ���� �� ������ ��� �������������!
 ��������: � ���������� �������������� %d ��� ����� ���� ��������
 ��������: � ���������� ����������� ��� ������� v3 ��� ����� �������
�� �� OpenPGP
 ��������: �� ������ ���� ��������� ��������: �� �������������� ������ �������� `%s'
 ��������: ��������� �� ����� ������ ��������������� �������� `%s'
 ��������: �� ������� ������ %08lX ����� ���� %s
 ��������: � ���������� ������ "���-��-�����-���-����"
 ��������: �� ������ ��������� %08lX ����� ���� %s
 ��������: � ���� S2K ��������� (0) ������ �� �����������
 ��������: � trustdb ��� ����� ���������
 �� ����� ��� ����������� �� ������ �� ���������� �����
 �� ����� ������ �� ���� ����������� 5 ����������
 ���������� �� ������� ������ ��� �� ����� ����.
 NnCcEeOoQq ��� ������� ���������� �������� ��� ������� ����������
 ��� ������� ��������� ������� ��� ������� ��������� ������� ��� `%s' ��� ���� ������� ����� ��� ������� ���������� ������ �� ������ %d
 ��� ������� ���� �� user ID.
 ��� ������ ���� ������������ ���:
%4u%c/%08lX %s " ��� ������� user ID �� ������ %d
 �� ������ ��������� Email
 ��������:  ��������� ��� ���� �� ������ ��� ������ �� �������������� ��� �������������.
�������� �� ��������������� ��� ������ "--edit-key" ��� �� ������������
��� ���������� ������ ��� ���� �� ����.
 ��������: ���� �� ������ ���� ���������������.
 ��������: ���� �� ������ ���� �����!
 ������ ��� �����������.
 ������ ��� �� ��������� �� �� ������  %08lX
 �������, ���� �� ����� ��'��� ��� � ����� ��� �� ������������ �����
������ ���� ����� �� ���������!
 ��������� (y/N);  ��������, ��������� ����� �� ������
 �������� ���������� ���� ���� ������������ ���� ��
������, ���� �� ������ �� ���������� ������� ����� (�� ��
�� �������� passports ��� fingerprints ��� �������� �����...);

 �������� ��� ����������� ��� ��������� email ��� ����� � ��� ������
 �������� �������� ��� ��� ����� �������. ��� ���� �������� RETURN
�� ��'������� ������ (������������� ���� �������) �� ��������������. �������� ����������� �������������� ��� ������ �������� �� ����� ������� ���������:  �������������� �� ����� �����߷ ���� ����� ��� ������� ������� 
 �������� ��������� ���� ��� ������ "�����" ���������
 � ���������� ��� ��������������� �������� ��� ����� ���������� �����
����� ��� ��� �������������� �� ���������.
 �������� ��������� ��� �������� ��� �� ������� �������.
 �������� ����������� �� ��������� ����� ������, ��� ��������. ��������� �� ���������� ��� <gnupg-bugs@gnu.org>
 �������� �������� �� ���� ��� ���������� ������.
 ������ �� ��������� ������� ��� user ID.
 �������� �������� ��� ����� ��� ��� ��������:
 �������� �������� ��� ���� ��� �������� ��� ������:
 �������� ������ ��� ���� ����� �� ������ �� ����� ������.
         0 = �� ������ ��� ����� ����
      <n>  = �� ������ ����� �� n �����
      <n>w = �� ������ ����� �� n ���������
      <n>m = �� ������ ����� �� n �����
      <n>y = �� ������ ����� �� n ���
 �������� ������ ��� ���� ����� �� ������ �� ����� ������.
         0 = �� ������ ��� ����� ����
      <n>  = �� ������ ����� �� n �����
      <n>w = �� ������ ����� �� n ���������
      <n>m = �� ������ ����� �� n �����
      <n>y = �� ������ ����� �� n ���
 �������� ��������������� ��� ������ "toggle" �����.
 ������ ����������, � �������� ��������������. ����� ��� ���� �������
��� �� ��� ���������, ����� ���� �� ��������� ��� �������� ��� ��������.
 ��������:  ��������� �������� ��������: �����������: �� ������� ������ ���� ���������������.
 ����������� ����� ����������;  ������� �����:  ������� �� ������������� �� ������������� ���������; (y/N) ������� �� ������������;  ������� �� ��������� ���� � ����-��������; (y/N) ������� ������ �� ���������� ��� �� ���������� user ID;  ������� ������ �� ��������� ���� �� user ID;  ������� ������ �� ���������� ��� �� ���������� user ID;  ������� ������ �� ��������� ���� �� user ID;  ������� �� ���������� ��� �� user ID;  ������� �� ���������;  ������� ������ �� ���������� �� ����������� ��� �� ���������� user ID;  ������� �� ���������� �� �����������; ����� ��� ��������: %s
 ����������� �� �����
 ����������� �� ����� ������:  �� ������� �������� ��� �������� ����� %u bits
 ������������� ��������� �������������.
 �� ������������� ��������� �������������.

�������� ������������ �� �� ��� ���� ��� ������ �� ������� ������ ��� �
Mallory ��������� �������� �� ���� �� ������������� ������ �� �����������
�� ������ ���. ����� ������ �� �������� ���� �� ������������� ��� �� ��
�������� ������, ��� ��� ��������� ��� �� ���� ��� ��������� ���.  ����
������� �� ������� ��������� ��� �������� ��� ������ �� ����������� ���
�������� ��� �� ��� ����� ��������� �� ������!
 ���������� ��� �������;  �� ������� ������ ����� ���������.
 ������� ������� ��� ������ �������� ��� ����� ���������.
 �������� ��� ��������� ��� ������ �� ���������������.

DSA (������ ������� ��� DSS) ����� � ���������� �������� ���������
��� ������ �� �������������� ���� ��� ���������.  ����� � �������������
���������� ������ � ���������� ��� DSA ��������� ����� �������� ���
��� ��������� ����� ElGamal.

ElGamal ����� ���� ���������� ��� ����� ��� �� ��������� ��� ��� ������-
�������. �� OpenPGP ��������� ��� ��� "�������" ����� ��� ����������:
��� ��������������-���� ��� ��� ���������-���-��������������, ����
�������������� ����� � �����, ���� ������� ���������� ������ �� ���������
�� ��� ������ ����� ��� �� ������������ ��� ������� ������ ��� ���������.
���� �� ��������� �� �����, ���� �� ����� OpenPGP ����������� ���
����� ���������� �� ������������� ���� �� ��������� (�����).

�� ����� (��������) ������ ������ �� ����� ������� ��� ������ ����� ���
��������. ����� ����� � ����� ��� ��� ����� � ��������������-����
������ ElGamal ��� ����� ��������� �� ���� �� �����. ������� ��� ������� ������� ��� ���������� ��� Photo ID ���������� ��� Photo ID � �������� ����� ������ ���; (0)  �������� %.*s �� ����� ��� ��������%s ID %08lX
 �������� ���������:  �������� ���������:  �������: gpg [��������] [������]
������� ��������� �� �������� �� ������ ������������ �������
 �������: gpg [��������] [������]
��������, �������, ������������� � ����������������
� �������������� ���������� ��������� ��� �� �������� �������
 � ��������� ������� ������� ����� ���� ������� ��� ���� �� �����

�� ��������������� �������� ��� ����� �������� ��� ���� �� ���������!!

 � ����-�������� ��� "%s"
����� ��� �������� ����� PGP 2.x.
 � �������� ��� ����� ������.  ����� ������ �� ��������� ��� ��
���������� ���. � ����� ����� ��� ���������� ������������� ���� ��� �� GnuOG. ��� ��������
�� ��������������� ���� �� ������ ��� ����������� �� ������� PGP. ����� �
���������� ������ ����� ���� �����, ��� ������ �� ��� ����� ���� �������
��� ����� ��������.
 ��� �������� ����������� �� ��� user ID ����� PGP 2.x.
 ���� � ������ ����������� �� ���� ��� ��������� %s.
 ���� ����� ��� ������� ������! - ������� �� ���������;  ���� ����� ��� �������� ��� ������� �� user ID ��� ������. ���
����� ������� ���� ���� �� ��������� ��� ������ ��������.  ����
�������������� �� GnuPG ���� �� �� ������ �� �������������� ����
�� ������ ���.  ����� �� ���������� ���� ��� ���� � ����-�������� ���
������ ���� ��� ����� ������ � ������� ��� �������. ���� ����� ��� ������ �������� ��� ������. �������� ��� �� ������
�� ��������� ���� � �������� ������ ������ �� ����� ���������� ���
��������� ��� �������� ������������ ��� ������ � �� ��� ���� ������
������������� ��� ����. ���� �� ������ ������ �� ����
 ���� �� ������ ���� ��������������� ���� �� ������ ���� �����! ���� �� ������ �������� �� ����� ���� %s.
 ���� �� ������ ��� �������������.
 ���� �� ������ ������ �� ��������� ��� %s ������  ���� �� ������ ������� ������ ���� ���������
 ���� � �������� ��� ������ �� �������� ������ ��� ����� �� ����������
������.  ������ �� ���������� �� �������� ���, ����� �� ������ ��� ������
��������������� ����� ���� �� ������ ��������� ������ �� ����������
��� ������� ������������ ���� ���� ����� ��� �������������� ��������. ���� � �������� ����� ���� %s.
 ���� �� ����� �� ������ ������� �� �� PGP 2.x.
 ���� �������� ���:
 ��� �� ������ ���� �����-������������, �� GnuPG ������ �� ����� ���� �������
����� ������ ������������ - ���� ����� ������� ������� ��� ����� �����
�������� ��� ������� ������. ��������� "yes" (���) ��� �� ����� ��� ������
���� ����� �����������
 ��������� ������� ��� ��������������: %lu
 �������� ��������� ��� ����������� "%s": %s
 �����: gpg [��������] [������] (-h ��� �������) �����: gpgv [��������] [������] (-h ��� �������) ����� ���������� ����� ��� ��������;  �� user ID "%s" ����������. �� User ID ��� ����� ����� ������ �������������: "%s" ����� ��� �� ������������ �������
 �������������: �� %s ����������� �� %s
 �������������: 2 ������ �� ������������� ����������� ��������.
 �������������: ���� ����� ��� ������ ����� PGP 2.x. � �������� ����
               ������������ �������� ������ �� ����� ������� �������� PGP
               �� �� ����������.
 �������������: ���� ����� ��� ������ ����� PGP2. � �������� ���� photo ID
               ������ �� ����� ������� �������� PGP �� �� ����������.
 �������������: ���� �� ������ ���� ��������� ��� ��� ��������� ���!
 �������������: ���� �� ������ ��� ���� ������������ �� ������������ ��������!
 �������������: ���� �� ������ ��� ���� ������������ �� ���������
������� ������������!
 �������������: ���� �� ��������� ���� ��������� ��� ��� ��������� ���!
 �������������: ����� �������� ����� �����������!
 �������������: ��� �������������� ���� �� ������!
 �������������: ����������� ������� ������ - ������� �� ����� ������
 �������������: `%s' ����� ��� ����� ������
 �������������: ��� �������� user ID ���� ���������� %d ������� ��� ������
 �������������: �� ��������������� ������ ���� ���������!
 �������������: �������� �� ������ �������� ���������
 �������������: �� ������ ������� ������� random_seed - ���
��������������
 �������������: ������ %08lX ������ �� ���������: ���� �������� ��������� %08lX
 �������������: ������ %08lX ������ �� ���������: �� ������ ��������� %08lX
��� ����� �����.
 �������������: �� ������ ��������������� �� ������� ������ ���
���������� ��������������.
 �������������: ��� ������������� � ����������� ��� ��������
 �������������: ���������� ��������� ���������. ���� � ����� �� ��������.
 �������������: ��� ����� ������ �������
 �������������: �� �������� ��� `%s' ��� ����� ������� �� ���� ��� ��������
 �������������: �� ��������� ���� ������������ ������ core!
 �������������: ������� ���������� (-r) ����� ����� ��������������
�������� ��������
 �������������: ��������� ��������� ��������� ��� ������
 �������������: �������� ���� %%-�������� ��������� (���� ������).
               ����� �� ������������.
 �������������: �������� ��� %%-�������� ��� url ��������� (���� ������).
               ����� �� ������������.
 �������������: �������� ��������� ���������� ������� `%s': %s
 �������������: �������� ��������� tempfile (%s) `%s': %s
 �������������: �� ������� ��������������������� ������� ��� %s "%s"
 �������������: �� �������� ������ ����������� ������� ��� %s "%s"
 �������������: �� ������� ���������� ��� %s "%s"
 �������������: �� �������� ������ ��� %s "%s"
 �������������: ����� �� �������� ������!
 �������������: ����� �� �������� ���������� ������� �������!!
 ������ �� ������������� ����� ������ bytes. ����� ���� ���� �� ������
������ ������� (��������������, ����������� �� �������, ��������������
���� �������) ���� �� �������� ����������� ������ �������. ���� �����
��� ��������� ������� ������� ��� �������� �� ������� ������ ��������.
 �� ������� �������� �� ������; (1024)  ���� ���������� ��� user ID �� ��� ������, ������ ����� �� ����������� ���
�� ������ ������ ��� ������� ��� ��������� ��� user ID. ����� ������� ������� ������, �� ������ ���� ���������� ������ ���� �� �����������.

"0" �������� ��� ��� �������� ���� ���� ���������� ������������� �� ������.
"1" �������� ��� ��������� ��� �� ������ ������ ��� ����� ��� ���� ��� ���
    ������, ���� ��� �������� � ��� ������������� ������� �� ������. ����
    ����� ������� ���� ���������� �� ������ ���� "����������" ������.

"2" �������� ��� ������ ��� ������ ����������� ��� ��������. ��� ����������
    ���� ������ �� �������� ��� ������������� �� ������ ��� �������� ��
    user ID ��� ������ �� ��� photo ID.

"3" �������� ��� ������ ���������� ������ ��� ������. ��� ����������, ����
    ���� ������ �� �������� ��� �������� �� ��������� ��� �������� �� ���
    ��������� ��� �������� "������" ����� ��� �������� ��� �� photo ID ���
    �������� ����� ����� �� ���� �� ��� ������� �� ������������ ������� �.�.
    ���������, ����������, ������� ��������.

����� ����� ��� �� ������������ ��� ������� ��� "�������" 2 ��� 3 �����
*����* ������������. ��� ����� �������� ���� ��� ���� �� ����������� ��
�������� "�������" ��� �� "�����������" �� ���� ���� ���������� �������.

��� ��� ������ ���� ����� � ����� ��������, ����� "0". ��������� �� ����������� ����� ��� ���������:
 �������������� �� `%s' ��� ����������.
 ��� �������� �� �������� ��� ���������� ����� �� ��� v3 ������
 ��� �������� �� ���������� �� ��������� user ID!
 ��� �������� ��� user ID. (��������������� �� "-r")
 ��� ������ ����� ������ - ���� ����� ������ *����* ����!
 ��� ���������� ��� ����� ������ - ���� ����� ������ ��� *����* ����!
�� �������� ���� ��� �� ����. �������� �� �������� �� ����� ���
����� ������, �� ��� ������� "--edit-key".

 ����� ��������� ���� �� user ID:
 ��� �������� �� ���������� ��� ����������� �������� �� ������ ����� PGP2.x.
 ��� �������� �� ���������� ��� photo ID �� ��� ������ ����� PGP2.
 �������� �� ��������������� ��� OpenPGP �������� �� ��� PGP 2.x ���� �� ��������� --pgp2.
 ������ �� ��������� ����������� ��� ������.
 ������ �� ��������� �� �������� ��� user ID.
 ���������� ��� ����� ������ ��� �� ������������� �� ������� ������.

 ���������� ��� ����� ������ ��� �� ������������ �� ������� ������ ��� �� ������:
"%.*s"
%u-bit %s ������, ID %08lX, ���������� %s%s
 ��������� �� USER-ID:
    "%s"

 ������ �� ������� ��� ���� ��� ��� �����������.  ������� �� ��
������������, ����� �� ���������� �� ��������� ��� �� �����:
  "�� ������ ���� �������"
      ��������������� ��� ��������� ��� �� ��������������� �������
      ����� �������� ��� ������� ������ ���.
  "�� ������ ���� ����������"
      ��������������� ��� ����� �������������� �� ������ �� ��� ����.
  "�� ������ ��� �������������� �����"
      ��������������� ��� ����� �������� ���� �� ������.
  "�� user ID ��� ����� ����� ������"
      ��������������� ���� ��� �� �������� ��� �� user ID ��� ������
      �� ��������������� ���. ��� �� ������� ����� ��� ��������� email.
 � ������������ �������� ��� ��� "%s"
���� �����.
 � ������������ �������� ��� ��� "%s"
����� ��� ������ ��������.
 � ������� ���;  � ������� ���;  �� ������� ��� ��� ������ �� ����������� ����������� ���� ��� 2038.
����, �� ����������� ����� ��� �� 2106.
 [User id ��� �������] [����� �������] [��������] [����-��������] [�������] `%s' ��� �����������
 `%s' ��� ����� �������� ������ - ���������
 `%s' ��� ����� ������ ����� keyID
 ��� ����� ��������� ������ �� �������� ���� ������������ ���������� ��� ���� ��� �� ����� �� ��� '='
 � ���� ��������� ������ �� �� ������������ ���������� control
 �� ����� ��������� ������ ������ �� �������� �� '@' ���������
 �������� ���� photo ID �������� ���� �������� ��������� �������� ���� ������������� �������� �������� ���� user ID �������� ����� ��� �������� ��� ����� ��� ����������� �������� ����� ��� �������� ����������� ��� ����� addkey addphoto addrevoker adduid ����� ������� MDC ��� ������������� �������� ���������� ������ �������� �������� %08lX ...
 ����������� ���������:  ��������: %s
 �������� ��� ���� ������������ ��������� �������� ��� ���� ������������ ��������� ������� %s ���������������� ���������
 ������� ����� ��������� ��� ������ %08lX ���� �������� �������� bit
 ������� �������������� ��������� ��� `%s'
 ���� MPI ���� URI ���� ������������� ���� ������ ���� ����� ������ ���� ������� ������ ���� ������� ������ ���� �������� ��������� batch: �� �� �������� ��������� ����� ������ build_packet �������: %s
 c �������� ����������� ��� `%s': %s
 �������� �������� ��� `%s': %s
 �������� ����������� ��� %s: %s
 �������� ����������� ��� `%s': %s
 �������� ����������� ��������� `%s': %s
 �������� ��������������� ��� core dump: %s
 ��� ������ �� ����� ���� �� ��������� ������ (batchmode)
 ��� ������ �� ����� ���� �� ��������� ������ ����� �� "--yes"
 �������� ����� ��� �������� ��� �� ����������: %s
 �������� ��������� ��� FD ��� �� ���������� ��� ��� agent
 �������� �������� ��� FD ��� �� ���������� ��� ��� agent
 �������� ��������� ��� ���������� �������� �������� %d
 �������� ��������� ������� �������� ����������� ��� %d ����������
 �������� ��������� ����� ��� ��������� ���������
 �������� ��������� ��� %s: %s
 �������� ��������� ��� `%s'
 �������� ��������� ��� `%s': %s
 �������� ��������� ��� �������: %s
 �������� ��������� �������������� ��������� `%s'
 �������� ��������� ��� ���������� �������� �������� ��� ����� ������ �� ��������� ������
 �������� ��������� ��� `%s': %s
 �������� ���������� ����������: %s
 �������� ������� ��� pid ��� ������ ��� �� agent
 �������� ����� ����������� ��� �� `%s': %s
 �������� ������ ���� ����������� ������� ESK ���� ��� ���������� S2K
 �������� �������� ��� `%s': %s
 ��������� ��� �� ������
 �������� ������� ���� �������� ����� PGP 2.x, ��� ����������� ��������
 �������� �������� �������� �������� ��� ���������� ��������������, ������ ��� %d �����!
 ������ ��� ����������� ����� ������ ��� ������������ ��������� ������ ��� ������ ������ check ������� ��������� �������� ������� �� ����� %d �����������=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 ������� � ������� ��� ��������� ��� �������������: %s
 ������� ����������� `%s'
 ������� ��� trustdb
 ������ checksum � ���������� �������������� %d%s ����� �������� � �����������������
 � �������� ��� ��������������� "%s" ��� ��������� ������ ��������
���������� ������
 �������� ������������ �� �� gpg-agent
 completes-needed ������ �� ����� ���������� ��� 0
 � ���������� ��������� ������ �� ����� ������ %d..%d
 ������������� �������
 �������� ������������ ��� URI ��� ��������� ��������
 ���������� ascii ������������ ������ �������� ��� �������������. ���������� �� ��� ������� "--output"
 ����������� �������: %s
 debug ���������������� ��������� (��������������) ���������������� �������: %s
 ���������������� OK
 �������� ���� ������������� �������� �������� ��������� �������� ���� user ID �������� block �������� �������: %s
 delkey delphoto delsig deluid � ���������� ��������� `%s' ����� �� ���� ��� ������ ��� �������� ����
 disable ������������� ��� ������ ��� ������������ ������ v3 ��������� ��� ������������ ������ v4 ��������� �� �� ����� ������ ������ �� ����� ���������� ��������� ��� ���������� ��� ������������ ��� RFC1991 enable ����������� ��� ������ �������� �������: %s
 ������������� ��������� ��������������� �� %s key, ID %08lX
 ��������������� �� %u-bit %s ������, ID %08lX, ������������� %s
 ��������������� �� ������� ��������� %d
 � ������������� ���� ��������� �� --pgp2 ��������� ������� ��� �����. IDEA
 ������������� �� ����� ���� ����������� ���������� ������ ���� �� ���������� ��� `%s': %s
 �������� ����������� ��� ����������� `%s': %s
 ������ ��� ���������� ��� ������ ������: %s
 ������ ���� ������ ��� �������� ������������: %s
 ������ ��� ������ trailer
 ������ ���� ��� �������� ��� `%s': %s
 ������ ���� ��� �������� ��� ����� ��������: %s
 ������ ���� ��� �������� �������� ����� �������� `%s': %s
 ������ ��� �������� ���� �� `%s': %s
 �������� �������� ��� ����������� `%s': %s
 �������� �������� �������� ����������� `%s': %s
 �������� �������� �������� ����������� `%s': %s
 ������: �� ������ ���������
 ������: ������ � ��� ��� ���� ������
 ������: ������ ���� ������������ ���������
 expire ������� �������� ������� �������� �� ��� ���������� �������� ������� ��� ����� ������������ �� ������� ���������� ������������ ����������������� ���� ��������� ������
�������
 �������� ���� �������� ���� �� `%s': ���������=%u
 �������� ������������� ��� TrustDB: %s
 �������� ������������ ��� cache �����������: %s
 ������ ����������� ������� ������ ����������� ������� ������ ��������� ������� �� ������ ������� ������ ��������� ������� ������ ��������� ������� ������ ������������ ������� ������ �������� ������� �������� ���� ��������� ����� ��������� ������������ �������� ��� user ID ��� �������� ������������ ������ v3 ��������� ������������ ������ v4 ��������� � ������������ ������ ��� ���������� ��������� %s (%d) ���������� ���
�������� ��� ���������
 � ������������ ��� ���������� ��������� %s (%d) ���������� ���
����������� ��� ���������
 � ������������ ����������� ���������� %s (%d) ���������� ���
�������� ��� ���������
 fpr ������ ������ ���������� ���� ���� ������� �������� ���������� ���� �������������� ��������� ���������� ��� �� ������������� 16-bit checksum ��� �� ��������� ���
�������� ��������
 � gpg-agent ��� ����� ���������� �� ���� �� ��������
 ��� ������������� � ������ ����������� %d ��� gpg-agent
 help iImMqQsS �������� �������� ��� ��� ���������� �������� �������� ��� ����� ������������ ��������/���������� �������� ������ ������� %u ����� ���� ������ � ��� ������ �� LF
 ������ ������� ���������� ��� %d ����������
 �� ������ ��������� S2K; ������ �� ����� 0, 1 � 3
 �� ������ ������ �� ������ �������� �� ������ ����������� ���������:  �� ������ ��������: � ������ ����� ���� ��� %d ����������
 �� ������� ���������� ��� "�������" ��� ��������
 �� ������ ����������� clearsig
 �� ������ dash escaped ������:  �� ������� �����������
 �� ������ default-check-level� ������ �� ����� 0, 1, 2, � 3
 �� ������� �������� ��������
 �� ������� ����������  hash `%s'
 �� ������� �������� ���������
 �� ������ ���������� �� ������ ������ �� ������ ����� ������ �� ������� ����������� ���������� ���������������
 �� ������� ����������� ���������� ���������� ���������
 �� ������� ����������� ���������� ���������� ���������
 �� ������� radix64 ���������� %02x ���������
 �� ������ �������� ��� ��� agent
 �� ������ ������(root) ������ ����������� ��� proc_tree()
 ����������� �� ������� ���������� symkey (%d)
 �� ������ ����
 key �� ������ %08lX ������������� %lu ������������ ��� ������ (��������� �
����� �������� ��� �����)
 �� ������ %08lX ������������� %lu ������������ ��� ������ (��������� �
����� �������� ��� �����)
 ������ %08lX ��������
 �� ������ %08lX ���������� ��� �������� ������������.
 �� ������ %08lX ������� ���� ��� ��� ���� ���� trustdb
 ������ %08lX: "%s" %d ���� ���������
 ������ %08lX: "%s" %d ��� ����������
 ������ %08lX: "%s" %d ��� user ID
 ������ %08lX: "%s" 1 ��� ��������
 ������ %08lX: "%s" 1 ��� ���������
 ������ %08lX: "%s" 1 ��� user ID
 ������ %08lX: "%s" ����������
 ������ %08lX: "%s" ������������� ��������� ����������
 ������ %08lX: "%s" ������������� ��������� ���������
 ������ %08lX: ����������� ��������� ����������� HKP
 ������ %08lX:  ������ ����� PGP 2.x - ������������
 ������ %08lX: ������� ��� ������ �� �����������
 ������ %08lX: ����� �� ����-������������� user ID '%s'
 ������ %08lX: ��� ��� ������� ����������
 ������ %08lX: �������� ���������� ��� ������� �������� ��������: %s
 ������ %08lX: �������� ��������� ��� ������� �������� ��������: %s
 ������ %08lX: ����� �������� �������� ����������
 ������ %08lX: ��� ��������� �� �� ��������� ���
 ������ %08lX: ����������� ����� user ID - ��������
 ������ %08lX: �� ������ ������������� ���������: %s - ��������
 ������ %08lX: �� ������ ������������� ���������: %s - ������������
 ������ %08lX: �� ������ ����-�������� ��� user id "%s"
 ������ %08lX: �� ������ �������� �����������
 ������ %08lX: �� ������ �������� �����������
 ������ %08lX: �� ������ ���� ���������!
 ������ %08lX:  ��� ������ - ������������
 ������ %08lX: ��� ������� ������ - �������� ��������� �������������� ���������
 ������ %08lX: ������ ������� ������ ��� �� ������ �� ����������� - ���������
 ������ %08lX: ��� ������� ��������� ��� �� �������� ��������
 ������ %08lX: ��� ������� ��������� ��� ��� �������� ��������
 ������ %08lX: ������ ��������� ��� �� ������ ��������� �����������
 ������ %08lX: ��� ������� ���� �� user ID
 ������ %08lX: ��� ������� user ID ��� ��� ��������
 ������ %08lX: ��� ���� ������ user ID
 ������ %08lX: �� ��������� �������� (����� %02x) - ������������
 ������ %08lX: ��� ����� ��� rfc2440 ������ - ������������
 ������ %08lX: ��� ����� ������������� - ������������
 ������ %08lX: �� ������� ������ "%s" ���� ��������
 ������ %08lX: ������� ������ ��� �� �������: %s
 ������ %08lX: ���������� � �������� ��������� �����������
 ������ %08lX: ���������� � �������� ��������� �����������
 ������ %08lX: �� ������������� ��������� �� ����� ������ - ������������
 ������ %08lX: ������� ������ ���������
 ������ %08lX: �� ������� �� ������� ������: %s
 ������ %08lX: ������� ������ �� ����� ��������. %d - ������������
 ������ %08lX: ������� ������ ����� ������� - ������������
 ������ %08lX: ������������ ���������
 ������ %08lX: ������������ user ID ' ������ %08lX: �� ��������� ���� ���������!
 ������ %08lX: � �������� ��� ����������� �� ����� ������ - ������������
 ������ %08lX: ���� ����� ��� ���������� ��� PGP ElGamal ������ �� �����
              ��� ����� ������� ��� ���������!
 ������ %08lX: �� ����������� ����� ��������� (0x%02x) - ������������
 ������ %08lX: �� ��������������� ���������� �������� ��������
 ������ %08lX: �� ��������������� ���������� �������� �������� ��� user id "%s"
 �� ������ '%s' �� �������: %s
 �� ������ ������������� %lu ������������ ��� ������ (��������� �
����� �������� ��� �����)
 �� ������ ������������� %lu ������������ ��� ������ (��������� �
����� �������� ��� �����)
 ������ ��������
 �� ������ ��� ���� ��������� ��� ��������� - ��� ������ �� �������������� �� ������� RNG!
 �������� �������� ��� �������� ������������.
 ���������� `%s' �������������
 ������ ���������� �������� �� ������ ������� ��������, ����� %u bits
 ��������������� ��� ������� �������� ��� %u bits
 �� ������� �������� ����� ���� ������, %d ����� � ������� ���� ��� �����������.
 �� ������� �������� ����� ���� �����, 1024 ����� � �������� ���� ��� RSA.
 �� ������� �������� ����� ���� �����, 768 ����� � �������� ������������ ����.
 l � ������ ����� ���� ������
 list ���������� ��� �������� ��� ��� user ID ���������� ��� ������ �������� ���������� ��� ������ �������� ��� ������������ (fingerprints) ���������� ��� ������ �������� ��� ��������� ���������� ���� ��� ���������� ������� ���������� ����������� (�������) ���������� �������� (���������) ���������� ��� ������ �������� �������� ���������� ��������� lsign ���������� ���� �� ������������� ��������� ������� ��� ����������� ���� (timestamp) ���� ��� ������������� make_keysig_packet �������: %s
 ����� ����� CRC
 ��������������� ��������� ������������� GPG_AGENT_INFO
 ��������������� ��������� ������ (user id) marginals-needed ������ �� ����� ���������� ��� 1
 max-cert-depth ������ �� ����� ������ 1 ��� 255
 ���������� ��������� �������� ��� ����� ����
 nN ����������� �� ���������������� ���������
 ������ ������� ����      ���� ����� MDC ��� ������������� ������������� ��� ������ �������� `%s'
 �������� ������� ��� trustdb �� ����� ���� %s
 ��� ��� ������� �� ������� = ���� ������ ��� ������ "%s"
 ������ ��������� ������� ������: %s
 ��� ������� �������������� ����������: %s
 ��� ����������� ������� �������� ���������
 ��� ������� ������ ��� ������ ��� trustdb
 ��� ������������ � ������������� �������� ������������
 �� �������� ������� ��������� ��� �� `%s'
 ������ ������� ������
 ��� �������� ������������� ��������
 ������� ��������� ������ (user id) �� �������� ������ OpenPGP ��������.
 ������ ������ ���������
 ��� ������� ��������� ����������: %s
 �� ������� ��������� ������� ����������: %s
 �� ������� ��������� ������� ����������: %s
 ��� ����� ���������� ��������
 �� ��������������� �� ���������� �� ������������� ��� ������������� ��������: �� ������ random_seed ����� �����
 ��������: ��� ���� ��������� �� ������ random_seed
 nrlsign nrsign �������, ������� � �������� ����������.
 ������ ������������ ��� DEK ��� �������������
 �������� ������ ���� (PGP 2.x)
 � ���������� ��� ����� ������ ����� �������������� ������ �����
 ������ �������� `%s': %s
 ������ ����� �������='%.*s'
 ���������� ����������� ������������-���������
 � ����� ������ ��� ������������ �����. ��������� ���� � ����� ������ ����� ���� ������
 passwd �������������� ����������� ��� ��������� email (����������) ����� �� http://www.gnupg.org/faq.html ��� ������������ �����������
 ����� �� http://www.gnupg.org/why-not-idea.html ��� ������������ �����������
 �������� �������������� �� "%s%s" ��������
 pref � ���������� %c%lu ������������
 � ���������� %c%lu ��� ����� ������
 ������ ����� ������� (����� CRC)
 ������ ����� ������� (��� �railer)
 ������ ����� ������� (������� CRC)
 �������� �������� ��� �������� ���������������� �������
 �������� �� ��� agent - �������������� ��� ������ ��� agent
 �������� �� ��� agent: agent ���������� 0x%lx
 ������� ���� ��� ��������� ��� ������������� � ���������� ���������� %d%s
 �� ������� ��� �� ������� ������ �������������� ��� �����������.
 �� ������� ������ %08lX ����� %lu ������������ ������� ��� ��� ��������
 �� ������� ������ %08lX ����� %lu ������������ ������� ��� ��� ��������
 �� ������� ������ %08lX ��� �������: %s
 ������������� �� ������� ������ �������: %s
 �� ������� ������ ��� ��������� �� �� �������!
 ��������������� �������� �� ������� ������: ���� DEK
 ������� ������ ����� %08lX
 ��� ������� �� ������� ������ �� ������� �� ������� ������ ��� ������� ������������� �������� %08lX
 q qQ ����������� ����������� ����� ��� ����� �����������, �� ����������, ���������� ��� �������� - ���� ����� ����� ������������� MTA
 ������ ���������: %s
 �������� �������� ��� ������ �������� ��� `%s'
 �������� �������� ��� `%s'
 �������� ��� stdin ...
 ����� ��� ��������: �������� ��� �������� ��� �� ������� ���������� �������� ��� �������� ��� �� ������� ���������� ������ �������� %08lX ��� �� %s
 ���� ����� rev! �� ������ ����������: %s
 rev- ������� ������������ ��������
 rev? �������� ���� ������ ���������: %s
 revkey ������ ���������: �������� ���� ������������� �������� �������� ���� user ID �������� ��������� revsig revuid ����������������� ��� �� %u bits
 s save ���������� ��� ������ ��������� �������� �� ��� ���������� �������� ��������� ��� "%s" ��� �� HKP ���������� %s
 �� ������� ������ `%s' �� �������: %s
 �� ������� ������ ��� ����� ��������� ������� ��� �������� �������� ��� ����� ���������
 ������� ������������� �������� N ������� user ID N � ����������� ���������� ��������� ��� �����������
��� ����� �������
 � ����������� ���������� �������������� ��� ����� �������
 � ����������� ���������� ��������� ��� ����� �������
 ����������� ���� ��� �������� ������,���������� ��� �������� �� OPENPGP ������� ���� ��� �������� ������,����������,�������� �� PGP 2.x ����������� ������� ����������� �������� setpref ���������� ��� fingerprint ���������� photo ID ���������� ����� ��� �������� ���������� ��� ����������� ���� ����� ��������� �� ������ showphoto showpref sign �������� ���� �������� �������� ���� �������� ������ �������� ���� �������� ������ ��� ��-����������� �������� ���� �������� ��-����������� �������� � ����������� ���� �������� �������� ��� �������� �������� ��� �������� ������ �������� ��� �������� ������ ��� ��-����������� �������� ��� �������� ��-����������� ��������� ��������� ���������
 � �������� �������: %s
 ��������: ������������ `%s': %s
 ������������ `%s': ������������
 ������������ `%s': ���� ����� ������������� ��� PGP ������ ElGamal ��� ��� ����� ������� ��� ���������!
 ������������: ������� ������ ���� ��� �������
 ������������: ������� ������ ���� ��� ������� ��� �� ������� ����������
 ������������: ������� ������ ��� �����
 ��������� �������� ��� ����� %d
 ������������ � v3 ����-�������� ��� user id "%s"
 �������, ��� ������ �� ����� ���� �� ��������� ������ (batchmode)
 ���������� �������� - ��������������� "gpg --import" ��� ��������
 ���������� �������� ������ 0x%02x
 ���������� ���� ��������� ����� %d ���� �������� �� ������� bit
 �������� ��� ��������� ��� `%s' (���������=%u)
 ������ ���������� ���� ��� ����� ���������� ������������: %s
 t ������� ��� �������� ��� ���� �� ���������� �� ����� ��� ��������������� IDEA ��� �������
 �� URL ��������� �������������� ��� ������ ��� ����� ������
 �� URL ��������� ��������� ��� ������ ��� ����� ������
 �������� ����������� ��� ���������.
�������� ��� ������� ��� �� ������ ��������� (.sig or .asc)
������ �� ����� �� ����� ������ ��� ������ �������.
 � trustdb ����� �������� - ��������������� �� "gpg --fix-trustdb".
 ������� ��� ������� ������ ��� �� ������� ������ "%s"!
 � ����� ����� ��� ���������� �������������� ��� ����������. �������������� ��� ��� �����������!
 ���� ������ �� ������� ��� ��� ������ ������������
 ���� �� ������ ���� ��� ������ �� �������������� ��� %s
 ���� � ��������� ������� ������. ������ ���� ����� ���������� ������������
 �������� ��� ������ keyid ��� ���������������� ������� ��������� ���� (timestamp) toggle ������ ������ ��� ����������� �������� ��� �������� �������� ���� ������ `%c' �����������
 ���� ������ ������������ ��� pk cache - ����������������
 trust ������ ��� ����� ��������� ������������ � ������� trust %lu ��� ����� ��� ���������� ����� %d
 ������� trust %lu, req ����� %d: read �������: %s
 ������� trust %lu, ����� %d: write �������: %s
 trustdb rec %lu: �������� lseek: %s
 trustdb rec %lu: �������� write (n=%d): %s
 ���� ������ ��������� trustdb
 trustdb: ������� lseek: %s
 trustdb: read ������� (n=%d): %s
 trustdb: sync �������: %s
 uid �������� ��������� ��� %s "%s": %s
 �������� ��������� ��� ���������� ������������
 �������� ��������� ��� ��������� ��� ���������� ������������: %s
 �������� ������� ��� exec-path �� %s
 �������� ������ ��� ��������������� IDEA ��� ��� �� ������� �� �� �������������������.
 �������� ��� �������������� ����� ��������� ������������ �� ����������� ��������: �� ����������� �������� �� ������������ ���������� �������������� �� ������������ ���������� �������� �������� �������� ���������� �������������� �������� ���������� ��������� �������� ��������������� ���������� `%s'
 �������� ���������� ��������� �������� ����� ������� �������� ���������� ����������
 �������� ���������� �������� �������� ������� ����� ��������� ������� ������ ������� ������ ��� ���������� ������������
 �� �������������� URI �� ���������������� ���������� �������� �������� �� ��������������� ������� ������ �� ��������������� ������� ������ �������� ���� ��� �������� ��� ��� ���������� �������� � ��������� �������: %s
 � ��������� �������� �������: %s
 �������� ��� ����� ��������� ������������ ����������� �������� updpref �����: gpg [��������]  ����� �� ������� ������ ����� ��������� ���������� �������� ��������������� ����� ��� ������� "--delete-secret-key" ��� �������� ���.
 ����� ��� ��������������� �������� �� ��������������� ��������� ����� ��� gpg-agent ����� ����� ��� ���������� (user id) ��� �������� � ���������������� �� user ID "%s" ���� ��� ���������
 user ID: " ����� ��� ������������� �������� %08lX ���� ��� ����������� %08lX
 ��������� ���������� ���� ��������� ������� ������ ������������� ������� ������ - ��������� �����������
 �������� ������� ��� ��� ������ ���������������� ��������� (%d)
 ������� ������ ���������
 ������� ��������� "�������" ��������
 ������� ��� �������� �������� ��� `%s'
 ������� ��� �������� �������� ��� `%s'
 ������� ����-���������
 ������� ���  `%s'
 ������� ���� stdout
 ����� ����� �������� �������� yY ��� �������� �� ���������� ������ �� ������� ����� PGP 2.x �� ��������� --pgp2
 �������� �� ����������-���������� �� ������� ����� PGP 2.x ���� ��
--pgp2 ���������
 ������������� �� ������� RSA ��� 2048 bit � ��� ���� ���� �� ��������� --pgp2
 �������� �� ������ ����������� � ������� ��������� ���� �� --pgp2 ���������
 ��� �������� �� ���������� ��� �� �������������� ���������� �� --pgp2 ���������
 �� �������� �� ������� ��� ������ ��� �� ���������� �������� ��� ������ ���
 ������� ��� bug ... (%s:%d)
 ����������� � ����� ��� %s ���� ��������� %s.
 ���� ������ ������������ (��� ��� pipes) ���� ��� ��������� --pgp2.
 |FD|������� ��� ����������� ���������� ��� FD |������|������� ��� ���������� ��������� ������ |�����������|����� ����� ��� ���������� �������� ��� ��������� |KEYID|������� ����������� �� ���� �� ������ |�����|������������� ��� ����� |�����|���������� ��� ��� ���������� ���������� �� ����� |�����|����� ��� �������� �� ��������������� ��������� |�����|����� �������� ��� �������������� ������� ������ |�����|����� ���������� �������������� ����� |�����|����� ���������� �������������� ����� ��� ������� ������� |�����|����� ���������� ��������� ��������� �����  |ONOMA|����� ��� ���������� ��������� ��������� ����� ��� ������� ������� |N|���������� �������� ��������� N (0 �������������) |N|����� ���������� ��������� N |N|����� ��� ���������� ������ ������ N |[������]|���������� ���� �� ���������������� ��������� |[������]|���������� ���� ��������� |[������]|������� ��� ����������� ���������� ��� ������ |[������]|���������������� ������� |[������]|������������� ������� |����� [������]| ���������� ���������� ��� ��������� 