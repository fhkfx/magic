��    �     d  Q  �>      �S  -   �S  #   T  )   CT  %   mT  �   �T    U     "V  1   :V  0   lV      �V  >   �V  =   �V  �   ;W  ;   X     =X     SX     oX     �X     �X     �X     �X  D    Y  .   EY  I   tY  8   �Y     �Y     Z     .Z     KZ     hZ     Z     �Z  "   �Z     �Z  #   �Z     [     4[     T[     m[  $   �[  &   �[  ,   �[     \      \     ;\     W\     u\     �\     �\     �\     �\     �\     �\  
   ]     !]     6]     H]     U]     o]  %   ~]     �]  
   �]     �]     �]     �]  +   
^  #   6^     Z^  -   s^  %   �^  ]   �^  Z   %_  H   �_      �_     �_     `  (   `  .   >`  3   m`  "   �`  )   �`     �`     	a     a     ,a     ?a     [a      ma     �a     �a     �a     �a     �a  )   b     2b     7b     Rb     kb     �b     �b     �b     �b     �b     �b     c  "   -c  %   Pc  &   vc  !   �c  %   �c  "   �c  #   d  '   ,d      Td     ud     �d     �d     �d     �d     �d  (   e  $   +e     Pe     ee  #   ye     �e     �e     �e  #   �e     f  &   6f  %   ]f  ,   �f  4   �f     �f     �f     g     /g     Dg     Xg     ng  '   �g     �g     �g     �g     �g     
h     h  "   4h     Wh     uh  -   �h  (   �h  0   �h  H   i  	  Wi     aj     nj     {j  �   �j  �   ?k  A   �k  /   <l  0   ll  \   �l  1   �l     ,m  C   Am  )   �m  -   �m  0   �m  .   n  '   =n     en     zn     �n     �n     �n  3   �n  4   o  -   8o  �   fo  .    p  .   Op     ~p  	   �p  	   �p     �p  .   �p  :   �p     q     #q     Aq  !   ]q  *   q  1   �q     �q  #   �q  &   r  "   @r  &   cr     �r     �r     �r     �r  <   �r  0   s  '   Es     ms  0   �s  '   �s  6   �s  G   t  @   et  >   �t  +   �t  =   u     Ou     du     tu  "   �u  :   �u     �u     �u     v      #v  /   Dv  �   tv     Ew  H   _w  -   �w  ,   �w     x  
   x     'x  8   :x  #   sx     �x     �x  "   �x  �   �x  E   py  �   �y  9   Lz  ;   �z  �   �z     v{     �{     �{  ;   �{  $   
|     /|     C|     [|     j|  �   �|  �   +}     �}     ~     .~     G~     ]~     p~     �~     �~  %   �~     �~  S   �~  �   +      �  G   �  !   +�  3   M�  <   ��     ��  "   ـ  ,   ��  %   )�  ,   O�  %   |�  2   ��     Ձ      �  (   �      ;�  
   \�  *   g�     ��     ��     ��     ӂ     �  ,   �     2�     L�  
   g�  �   r�  "   �     $�     A�     S�  d   s�     ؄     �  �   
�  E   ��  o   �      ^�      �  7   ��  '   ؆  c    �  /   d�  E   ��  ,   ڇ  )   �  #   1�  -   U�  )   ��  �   ��    ��  '   ��  �   ��     R�     [�     t�     }�     ��     ��  1   ��     �  (   ��  %   #�     I�  %   f�     ��     ��     Č  9   Ҍ     �     ,�     G�     Z�     n�      ��  }  ��     +�     :�  /   T�    ��  "   ��     Œ     Ԓ  *   �     �     1�  J   D�  j   ��  �   ��  9   ��  O   ��  �   �  5   ߕ  .   �  '   D�    l�  �   }�     A�     Y�     t�  !   ��     ��  "   Ș  '   �  �   �     �  -   '�     U�  �   h�     8�     U�  *   t�  +   ��     ˛     �     ��  %   �     ;�  7   U�     ��  r   �  1   ��  =   ��  I   �  4   :�     o�  #   ��  =   ��     �  ?   �  1   P�  %   ��  5   ��  A   ޟ  E    �  H   f�  -   ��  H   ݠ     &�  <   A�  )   ~�  C   ��  .   �  F   �  H   b�  2   ��  1   ޢ  9   �  ;   J�  %   ��  '   ��      ԣ  2   ��     (�  !   )�  h  K�  *   ��  &   ߪ  1   �  #   8�  2   \�  >   ��  �   Ϋ      ��  =   ��  0   ߬  M   �  "   ^�  &   ��  3   ��  f   ܭ  %   C�  D  i�  ,   ��  5   ۰     �     !�  _   2�     ��  
   ��     ��     ��     ϱ     ۱  %   ��     �  S   :�  5   ��  4   Ĳ     ��     �     �     1�  (   ?�  #   h�     ��     ��  
   ��     ��     ��  1   γ      �  
   �     �     6�     S�  E   o�     ��     Ӵ     ۴     �     �     ��     
�     �     (�     6�     L�     c�     |�     ~�     ��     ��     ĵ      ۵     ��     �  +   6�  !   b�  '   ��  (   ��  %   ն  2   ��  '   .�     V�     i�     z�     ��     ��     ��  "   ط     ��     �  #   ,�     P�  5   e�     ��     ��  ;   ĸ  <    �     =�     T�     j�     ��     ��  A   ��  &   ݹ     �     �     1�  -   @�  ;   n�  %   ��  (   к  +   ��     %�     ;�     Z�  1   v�     ��     ��     Ż     ܻ     �     �     �     -�     <�     Z�     a�     j�     q�  3   x�     ��     ��     ¼     ݼ     ��     �  %   2�     X�     _�     l�     ��      ��  3   ��  $   �  =   
�  %   H�     n�      ��     ��     Ǿ     �     ��     �  '   2�     Z�     u�  &   ��  &   ��     �     ��     �     1�     8�     D�     `�  K   }�  "   ��  %   ��  $   �     7�     H�     Z�     l�     x�     ��     ��     ��     ��     ��     ��     �  E   �  @   d�  @   ��     ��     ��     ��  !   �  D   2�  +   w�  /   ��     ��     ��     ��     ��     �  %   *�  %   P�  $   v�     ��     ��     ��  .   ��  '    �     (�     A�     ]�  3   z�     ��     ��     ��     ��     �     �  $   -�  &   R�  $   y�  '   ��     ��  ,   ��  '   �     8�     G�  M   K�  N   ��     ��  '   ��  /   &�  "   V�     y�      ��      ��     ��     ��     �  -   5�  0   c�  *   ��  '   ��  #   ��  1   �  %   =�  .   c�  ,   ��  &   ��  "   ��  0   	�  9   :�  8   t�  2   ��  "   ��  %   �  !   )�     K�  >   i�  3   ��  %   ��  (   �  2   +�     ^�  $   u�     ��  ;   ��  '   ��  #   �  $   @�  $   e�  +   ��  .   ��  ;   ��     !�  $   A�  7   f�  3   ��     ��     ��  $   
�  5   /�  S   e�  9   ��  ,   ��  <    �     ]�  G   u�  H   ��     �  B   �  "   Y�     |�     ��     ��     ��  0   ��  ;   �  2   N�     ��     ��     ��     ��  	   ��     ��     ��  !   ��     �     '�     B�     S�     c�     i�  '   ��     ��     ��  .   ��     �  (   �  )   C�  ,   m�     ��     ��     ��  
   ��     ��  $   ��     �     6�  )   9�      c�     ��  %   ��     ��  &   ��  "   �     0�     ?�     O�     _�     }�     ��  %   ��  %   ��     ��     �     %�     8�     F�      T�  #   u�     ��     ��  &   ��  )   ��     ��  <   �     U�     k�     ��  ,   ��     ��     ��  ;   ��  >   +�  G   j�     ��     ��     ��     ��     �     &�     B�     Z�  "   b�  -   ��  ,   ��     ��  +   ��  *   &�  8   Q�  9   ��     ��  !   ��  &   �  $   -�     R�     g�  5   |�     ��     ��     ��     ��  I   ��     �     %�     <�     O�     j�     }�  #   ��  #   ��     ��     ��  !   
�     ,�  %   I�     o�     v�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��  &   �     F�     e�  #   ~�     ��     ��  3   ��  %   ��  %   $�  =   J�  =   ��     ��     ��     ��     ��     �  %   �  	   6�     @�     I�  
   N�     Y�  $   l�     ��     ��     ��     ��  &   ��     �  "   !�     D�     X�     a�     s�  V   ��      ��  5   �  $   ;�     `�  +   {�  #   ��  4   ��  %    �  
   &�  *   1�  $   \�  0   ��     ��     ��  &   ��  .   ��  *   *�  �   U�  :   ��  +   !�  E   M�  /   ��  %   ��  A   ��  &   +�     R�     e�  ,   l�     ��  (   ��     ��     ��  -   ��  /   &�  ,   V�  "   ��  )   ��     ��     ��      
�     +�     E�     I�  #   h�  -   ��     ��  I   ��      #�     D�     V�     f�     ��     ��     ��     ��     ��     �     %�     C�     \�     t�  #   ��     ��     ��     ��     ��      ��     �     .�     H�     b�     v�     ~�     ��     ��  6   ��  (   ��     �  #   1�      U�  
   v�  7   ��     ��     ��     ��     ��  -   ��     (�     B�     a�     }�     ��     ��     ��     ��     ��     ��  D   ��  F   7�  E   ~�  D   ��  A   	�  7   K�     ��  $   ��  F   ��      �      -�  '   N�      v�     ��  "   ��  #   ��  $   ��     �  /   :�  '   j�  7   ��  $   ��     ��     �  #   $�     H�  !   a�     ��     ��  #   ��  $  ��  ?   ��      :�  )   [�  (   ��  �   ��    B�     _�  1   x�  0   ��     ��  G   ��  F   C�  �   ��  [   z�     ��  #   ��  $   �  $   5�  $   Z�  $   �  $   ��  M   ��  1   �  G   I�  @   ��  $   ��      ��  $   �  $   =�     b�     y�     ��  $   ��  #   ��  +   ��     *�  '   J�     r�     ��  &   ��     ��  *   ��  $   !�     F�     a�     |�     ��  $   ��     ��     ��  $   
     /  $   M     r     �     �     �      �     �  $   �         ,    : $   Q    v -   � "   � !   � 2   � 2   . t   a o   � C   F 1   �    �    � .   � 7    9   T $   � -   �    �    �            :    Z ,   k    �    �    �    �     &       A    F    `    } *   � (   �    � "       /    L !   a <   � A   � E    D   H A   � /   � 4   � 8   4	 /   m	 #   �	    �	 %   �	    
 1   
    N
 '   ^
 +   �
    �
    �
 1   �
      "   7 "   Z (   }    � ,   � '   � 0    B   I    �    � "   �    �            4 2   M !   � #   �    �    �    �     )   +    U    r 7   � 0   � 8   � L   , 7  y    �    � !   � �   � �   � ;   � 7   � 5   �    . A   �    � K    (   T +   } 6   � 4   � ,       B    Z (   x "   �    � 8   � C    >   I �   � 3   N 3   �    � 
   �    �    � 5   � A   !    c     w !   � %   � =   � C    5   b )   � ,   � "   � ,       ?    X    r 	   � I   � 8   � +    "   @ 7   c *   � >   � S    6   Y =   � 7   � =    $   D    i 5   z 8   � E   �     /    P    h '   � 7   � �   �    � P   � 3   ,  ?   `     �     �     �  8   �  #   !    9! 	   N! -   X! �   �! T   &" �   {" F   :# F   �# �   �# '   �$    �$ $   �$ I   �$ !   G%    i%    z%    �% (   �% �   �% �   m& $   `' "   �'    �'    �'    �'    �'    (    ( 7   ;( 
   s( m   ~( �   �( 2   �) T   �) %   * M   6* H   �*    �* /   �* >   + *   X+ 7   �+ /   �+ 7   �+ >   #, ,   b, 0   �, (   �, 
   �, :   �,    /-    E-    e- &   }-    �- 0   �- "   �- '   . 
   >. �   I. "   �.    /    /     5/ ~   V/    �/    �/ �   0 G   �0 ~   1    �1 %   �1 3   �1 )   2 o   .2 *   �2 G   �2 t   3 4   �3 3   �3 &   �3 $   4 �   ;4   :5 '   Z6 �   �6    (7    47    Q7     `7    �7 
   �7 7   �7    �7 7   �7 8   )8 %   b8 @   �8 +   �8 (   �8    9 C   39 1   w9    �9    �9    �9    �9    : �  5:    �;     < C    < �  d< E   �?    :@ #   X@ .   |@    �@    �@ ]   �@ }   :A �   �A /   {B P   �B �   �B K   �C ,   9D 5   fD 1  �D �   �E    �F    �F    �F    �F    G +   9G 5   eG    �G     �H -   �H    I   #I $   2J (   WJ 6   �J 7   �J     �J &   K &   7K 1   ^K    �K F   �K �   �K �   |L ;    M K   <M L   �M @   �M 2   N 7   IN A   �N (   �N T   �N 3   AO ?   uO ^   �O \   P \   qP [   �P ;   *Q \   fQ     �Q I   �Q 7   .R g   fR ?   �R |   S �   �S F   T H   ^T P   �T N   �T /   GU 0   wU <   �U H   �U   .V +   JW q  vW 3   �[ )   \ =   F\ 6   �\ L   �\ P   ] �   Y] )   4^ J   ^^ H   �^ b   �^ *   U_ 1   �_ E   �_ �   �_ 6   ~` �  �` +   Zc -   �c    �c    �c y   �c    Od    md    ~d    �d    �d    �d ,   �d 7   �d w   "e D   �e >   �e    f    ;f    Yf    tf     �f '   �f    �f    �f    �f    �f .   �f 7    g    Xg    kg '   wg '   �g )   �g V   �g 2   Hh *   {h    �h    �h    �h    �h    �h    i    i )   0i     Zi    {i    �i    �i &   �i    �i    �i +   j ;   Dj -   �j :   �j -   �j G   k G   _k 3   �k @   �k 9   l    Vl    rl    �l #   �l ,   �l !   �l ;   m    Ym 4   vm 3   �m     �m F    n    Gn    en I   n [   �n    %o    Bo    Wo    oo     xo K   �o .   �o     p    5p    Tp :   np W   �p )   q +   +q E   Wq    �q 2   �q "   �q X   	r )   br    �r    �r    �r    �r    �r    
s    #s -   :s 	   hs    rs 
   ~s    �s N   �s 
   �s    �s "   �s "   "t    Et #   Ut %   yt    �t    �t +   �t    �t    �t :   u %   Su L   yu     �u '   �u 2   v /   Bv A   rv !   �v &   �v .   �v :   ,w #   gw 0   �w 7   �w 7   �w    ,x %   Hx 3   nx    �x    �x )   �x !   �x p   y ,   xy 2   �y 7   �y    z    /z (   Mz    vz    �z    �z .   �z    �z '   �z ,   !{    N{    j{ `   �{ `   �{ ^   H|    �|    �| "   �| #   �| W   } 2   Z} 5   �}    �}    �} &   �} !   �}    ~ P   6~ 4   �~ *   �~    �~    �~    	 5   % 1   [ %   � !   � !   � 8   �    0� $   L�    q�    ��    ��    �� 2   ǀ 2   �� .   -� *   \�    �� 0   �� 9   Ձ    �     � e   $� e   ��    �� 4   � ?   ;� (   {� '   �� )   ̃ '   �� &   � &   E�    l� 1   �� 1   �� .   �� *   � ,   J� :   w� *   �� >   ݅ ;   � 0   X� *   �� 0   �� :   � ;    � =   \� -   �� +   ȇ #   � "   � S   ;� B   �� .   ҈ 0   � D   2� $   w� /   �� +   ̉ <   �� 2   5� "   h� &   �� (   �� @   ۊ >   � A   [�     �� '   �� M   � ?   4� (   t� ,   �� (   ʌ >   � ]   2� ?   �� 3   Ѝ R   �    W� _   q� _   ю    1� u   A� .   ��    �    � (   �    H� >   c� E   �� ;   �    $�    &�    9� (   @�    i� !   y� !   ��    ��    ב    ��    �    0�    F�    N� T   k�     ��    � ;   �    *� +   E� 2   q� 4   ��    ٓ %   ܓ    � 
   � /   � ,   N� B   {�    �� 9    '   �� !   $� 8   F� -   � 4   �� +   �    �    "�    :� .   V�    �� ?   �� H   � H   *�     s�    ��    �� 
   ��    × (   З 6   �� 	   0�    :� ,   C� B   p� &   �� N   ژ    )�     E� @   f� >   ��    �    �� @   � ?   G� H   ��    К    �    � %   � (   3� -   \� '   �� 
   �� )   �� 7   � .   � #   N� 3   r� 8   �� G   ߜ H   '� $   p� /   �� 5   ŝ C   ��    ?�    Z� C   s�    ��    ��    ��    Ğ |   Ԟ    Q�    h�    ��    ��     ��    П %   � %   �    2�    Q� $   o� *   �� 3   ��    �    ��    �    2�    P�    h�    o�    v�    ��    ��    �� )   �� $   ١ ,   �� #   +� -   O�     }� #   �� B   ¢ 5   � 1   ;� +   m� d   ��    ��    �    )�    >�    Z�    n�    �� 
   ��    ��    ��    �� ,   Ԥ    �     �    9�    G� .   `� $   �� $   ��    ٥ 
   ��    ��    � c   3� "   �� 9   �� "   ��     � <   8� 5   u� ?   �� $   �    � 2   � 7   P� 8   ��    �� !   è 0   � 8   � 4   O� �   �� B   '� 3   j� N   �� 2   �� )    � W   J� '   ��    ʫ    ۫ D   �    (� ,   B�    o�     v� @   �� M   ج B   &� <   i� K   �� 2   � *   %� 2   P� 3   ��    �� $   �� +   � 7   � 0   D� Z   u� .   Я    ��    � &   &� &   M� !   t� !   �� "   ��    ۰    �� !   � !   2�    T�    q� *   ��    �� )   ��    �    � (   �    E� .   b� "   ��    ��    Ͳ    ֲ     �     � A   4� (   v�    �� )   �� ,   ܳ    	� M   �    l�    s� 
   �� "   �� 7   �� !   � $   � &   7� %   ^�    ��    �� !   ��    ֵ    ��    �� i   �� b   e� T   ȶ V   � C   t� H   �� #   � +   %� M   Q� $   �� (   ĸ 0   �� .   �    M� 3   d� ,   �� .   Ź -   �� 3   "� (   V� 3   � (   �� )   ܺ ,   � -   3�    a� 9   ��     �� !   ۻ /   ��        �  E              �  Y       �   _   7      B   H          �  �   1   �  �  �  ?  �  9  �   �  0  8  n      �      �      �  �   �  �  �  �  |  �      a   �       f      �  �  �  �  �  /  �   �  �           �  �  ]                �  �        |  W   �    )   .  Y          ]  �      �      �  i  �  �   /       �            Z  �      w  1      �          �  �  �  f   �      *      �   m   c  �       �  �  
        �  �  �  �  �  ~   �             �   �             `  �         '  ^  P  �   �   +  r  �  i  �  �         �   �  �  �    :  �         �  '  %      �              �   �  �                D       t               �  	   H  [  `      v  �  q  &   "   -      .      E  �                  �  -       �            �  �  �   �   j  �   �  x  R      �  J   �  +      �       �  �  -  =  F      .  h      2   �  �   �  �      �       b  �   �    �   }                �  (      y         q  �  �          �  Y              �   �           �     �  �   �  &  �        �       1  �  �           �  �   �      �  �  B  �  �  �  �  x   X  {   �  &      �   z   �           �  N     �  M         �   �  �      L   �   q  �      �  �   B    �           -      �         �   �    w  �  �  �  K   �  �  K      8  �  f      4  !      {  �  T              �   �     I    ]      R  �   �      �   �  u  r  5  t  ?   <     �      C      �  G          �  �      >                  �      �  �       �  �       �        �  �       �  �       �  "  �  ,  7  �    �   �      z  �         M  �  �   �        �      �  �  �         �   T   �       s   y              D  X  �  3  �  �       F        ,      �  N  .   �  A   �  �  o  �       C  @  i  �  �   0       �  �     �  �      [  S     ;   2  �  f    �  H          I      e   5  �              �      U  �          �  �   ;  �  �  �    �  O  �  0          �        F   |       2    [           �  N   U  �    �  �           �    }   
        :           �  �      D  �      c          "  n   b       D          �  6  K    �  �  �  �  �   N  �      �  �      �  7      �  �         �      �   g  �  �  �  n  �   �  /     �  :  �  :        U  8   9  �       p  l  �   G      1  �  4       g  G   �  �  �   k   j  e  �  0   �      �  @  �         	      �          �  g  v  d  2  �  �      Q  #  @   �   P  �   _      �   d      @    �  A  �   s  �    <  �   O  �  p  �       �       Q   j    j   /  �  �   �      u      Z              P  �       )  �        �  e      "  �      w       `           I   �    �        9  �      �  �      $  >  J  V   %   �      \      �        g   h   3  >       o      �  [  �   �  �  C   �  )  �  W  �     �           {      9   �       '      $  �      5  %  �  W  ?      �  �  �  S       ,      z  a  �  �  z  a  �    �   V  _              �   r      L  y  3       �  �  t  A  +  l  �  ,           �   �  V  |  �  Z   �          �  �      G        �  �   =       �  l   �     �  �  W      �    �  k      _  t       �      �      �  \      �   �   �      L  �     *      �  �  �   �   =  \   �              �          �  �          �    �  �   �       �  \  x  �  S  �      �  }      u   )       �  Z  �      �  �          R  �   J  4      �  6  �   �  �                   d        S  �           A  x  �        n      �  �  �  �  �  �      �     #   �    �   �   �     �           e            R       ^       �   �    O       H   <            q   (  K      s  �  �  �     �  �   �  �      �  �  P       �  k      �      �       �      �  �  v  �  �  �          �       M  	      �  r   �           
       {  ~  �   Y  �  p   �  �  k  �  m  �  ^  �  �      o        �  �  �            �                    �  �      3  �  C      ^  �         I  	  E              �               $   <  6      �    u  b  U   �   O  a  Q  T  �               �  �          b      �  �  �          �   %  �          y  o          *   �    X   �              �  h      �   �  V      �      ]  �  &      �  E  �  �                  �      �  L  c          #  m  �  �  ;  �  8          �     ~  �      �  �  �  �      ~  X         �       �  �  �             !      �  �  6   �  c   �          �   ?  �      �  �  �  �  `          B  
  (   '  �      �   i   ;  �  �  !  �    }          l  �   �               �   �  �   �      �  �        w  h  �      �  (  �     �  =  7   �      �       �          m          �  �   �  �             �  �   �  �   �      �           �  �  5       4      �                      �          p  +   Q      �  T  �    M   s    �  *    F  d   �  �  �       �      !     $  #      �          �    �  �  J  �          �        �       v                           >              �  �        
Enter the user ID.  End with an empty line:  
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Key fingerprint =      Subkey fingerprint:    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
    signed by %08lX at %s%s
    signed by %08lX at %s%s%s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  Primary key fingerprint:  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" is not a JPEG file
 "%s" was already locally signed by key %08lX
 "%s" was already signed by key %08lX
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s ...
 %s does not expire at all
 %s encrypted data
 %s encryption will be used
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s signature from: "%s"
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't make lock
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  ASCII armored output forced.
 About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to use it (y/N)?  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Cipher:  Command>  Comment:  Compression:  Create a revocation certificate for this key?  Create a revocation certificate for this signature? (y/N)  Create anyway?  Critical signature notation:  Critical signature policy:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Displaying %s photo ID of size %ld for key 0x%08lX (uid %d)
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Experimental algorithms should not be used!
 Expired signature from " Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid passphrase; please try again Invalid selection.
 Is this correct (y/n)?  Is this okay?  Is this photo correct (y/N/q)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: Elgamal primary key detected - this may take some time to import
 NOTE: This key is not protected!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Policy:  Primary key fingerprint: Pubkey:  Public key is disabled.
 Quit without saving?  Real name:  Really create the revocation certificates? (y/N)  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really revoke all selected user IDs?  Really revoke this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Really update the preferences?  Reason for revocation: %s
 Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Signature notation:  Signature policy:  Syntax: gpg [options] [files]
Check signatures against known trusted keys
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The use of this algorithm is only supported by GnuPG.  You will not be
able to use this key to communicate with PGP users.  This algorithm is also
very slow, and may not be as secure as the other choices.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key  This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To be revoked by:
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 Unable to open photo "%s": %s
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) Use this key anyway?  User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: signature digest conflict in message
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to %%-expand policy url (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe enclosing directory ownership on %s "%s"
 WARNING: unsafe enclosing directory permissions on %s "%s"
 WARNING: unsafe ownership on %s "%s"
 WARNING: unsafe permissions on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [filename] [revocation] [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid long keyID
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption anonymous recipient; trying secret key %08lX ...
 armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 assuming bad signature from key %08lX due to an unknown critical bit
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet build_packet failed: %s
 c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't get server read FD for the agent
 can't get server write FD for the agent
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open file: %s
 can't open signed data `%s'
 can't open the keyring can't query password in batchmode
 can't read `%s': %s
 can't search keyserver: %s
 can't set client pid for the agent
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking at depth %d signed=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 checking created signature failed: %s
 checking keyring `%s'
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 cipher extension "%s" not loaded due to unsafe permissions
 communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID deleting keyblock failed: %s
 delkey delphoto delsig deluid digest algorithm `%s' is read-only in this release
 disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error finding trust record: %s
 error in trailer line
 error reading `%s': %s
 error reading keyblock: %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 error: invalid fingerprint
 error: missing colon
 error: no ownertrust value
 expire export keys export keys to a key server export the ownertrust values external program calls are disabled due to unsafe options file permissions
 failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing digest algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 help iImMqQsS import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid symkey algorithm detected (%d)
 invalid value
 key key %08lX has been created %lu second in future (time warp or clock problem)
 key %08lX has been created %lu seconds in future (time warp or clock problem)
 key %08lX incomplete
 key %08lX marked as ultimately trusted
 key %08lX occurs more than once in the trustdb
 key %08lX: "%s" %d new signatures
 key %08lX: "%s" %d new subkeys
 key %08lX: "%s" %d new user IDs
 key %08lX: "%s" 1 new signature
 key %08lX: "%s" 1 new subkey
 key %08lX: "%s" 1 new user ID
 key %08lX: "%s" not changed
 key %08lX: "%s" revocation certificate added
 key %08lX: "%s" revocation certificate imported
 key %08lX: HKP subkey corruption repaired
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted as trusted key
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: direct key signature added
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid self-signature on user id "%s"
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey binding
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: this is a PGP generated ElGamal key which is NOT secure for signatures!
 key %08lX: unexpected signature class (0x%02X) - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key `%s' not found: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key incomplete
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l line too long
 list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never      never use a MDC for encryption new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition "%s"
 no corresponding public key: %s
 no default secret keyring: %s
 no entropy gathering module detected
 no need for a trustdb check
 no remote program execution supported
 no revocation keys found for `%s'
 no secret key
 no signed data
 no such user id no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again passphrase too long
 passwd please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 please use "%s%s" instead
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent - disabling agent use
 problem with the agent: agent returns 0x%lx
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key does not match secret key!
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read error: %s
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring requesting key %08lX from %s
 resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke a user ID revoke signatures revsig revuid rounded up to %u bits
 s save save and quit search for keys on a key server searching for "%s" from HKP server %s
 secret key `%s' not found: %s
 secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key locally and non-revocably sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 skipping v3 self-signature on user id "%s"
 sorry, can't do this in batch mode
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 system error while calling external program: %s
 t take the keys from this keyring the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temp files when calling external programs
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to execute %s "%s": %s
 unable to execute external program
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown protection algorithm
 unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg 1.2.2
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-04-28 18:48+0300
Last-Translator: Ga�l Qu�ri <gael@lautre.net>
Language-Team: French <traduc@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=CP850
Content-Transfer-Encoding: 8-bit
 
Entrez le nom d'utilisateur, en terminant par une ligne vide:  
J'ai un peu v�rifi� cette cl�.
 
J'ai v�rifi� cette cl� avec grand soin.
 
Je n'ai pas du tout v�rifi� cette cl�.
 
Il n'y a pas assez d'octets al�atoires disponibles. Faites autre chose
pour que l'OS puisse amasser plus d'entropie ! (il faut %d octets de plus)
 
Choisissez une image � utiliser pour votre photo d'identit�. L'image
doit �tre un fichier JPEG. Rappelez-vous que cette image est stock�e
dans votre cl� publique. Si vous utilisez une image tr�s grosse, il
en sera de m�me pour votre cl� !
La meilleure taille � utiliser est 240x288.
 
Algorithmes support�s:
 
La signature sera marqu�e comme non-exportable.
 
La signature sera marqu�e comme non-r�vocable.
 
cela sera une auto-signature.
 
AVERTISSEMENT: la signature ne sera pas marqu�e comme non-exportable.
 
AVERTISSEMENT: La signature ne sera pas marqu�e comme
non-r�vocable.
 
Vous avez besoin d'un nom d'utilisateur pour identifier votre cl�; le
programme le construit � partir du nom r�el, d'un commentaire et d'une
adresse e-mail de cette mani�re:
   � Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de> �

 
Vous avez besoin d'un mot de passe pour d�verrouiller la cl� secr�te pour
l'utilisateur: "               alias "                       import�e: %lu                      inchang�e: %lu
            nouvelles sous-cl�s: %lu
   nouveaux noms d'utilisateurs: %lu
                   non import�e: %lu
         sans nom d'utilisateur: %lu
             Il n'est pas s�r que la signature appartient � son propri�taire.
            La signature est certainement FAUSSE.
            Rien ne dit que la signature appartient � son propri�taire.
            Cela pourrait signifier que la signature est fausse.
           nouvelles signatures: %lu
        Empreinte de la sous-cl�:             cl�s secr�tes lues: %lu
        nouvelles cl�s ignor�es: %lu
  Empreinte de la cl� =    Empreinte de la sous-cl�:    (%d) DSA (signature seule)
    (%d) DSA et ElGamal (par d�faut)
    (%d) ElGamal (chiffrement seul)
    (%d) ElGamal (signature et chiffrement)
    (%d) RSA (chiffrement seul)
    (%d) RSA (signature et chiffrement)
    (%d) RSA (signature seule)
    (0) Je ne r�pondrai pas.%s
    (1) Je n'ai pas v�rifi� du tout.%s
    (2) J'ai un peu v�rifi�.%s
    (3) J'ai v�rifi� tr�s soigneusement.%s
  nouvelles r�vocations de cl�s: %lu
    r�voqu� par %08lX � %s
    sign� par %08lX � %s%s
    sign� par %08lX � %s%s%s
   Impossible de signer.
        cl�s secr�tes import�es: %lu
  %d = ne sait pas
  %d = je ne fais PAS confiance
  %d = je fais enti�rement confiance
  %d = je crois marginalement
  %d = je donne une confiance ultime
  (par d�faut)  (ID cl� principale %08lX)  (non-exportable)  (sensible)  Empreinte de la cl� principale:  [expire: %s]  i = donnez-moi plus d'informations
  m = retour au menu principal
  q = quitter
  s = sauter cette cl�
       cl�s secr�tes inchang�es: %lu
  confiance: %c/%c "
sign� localement avec votre cl� %08lX � %s
 �
sign� avec votre cl� %08lX � %s
 � %s � n'est pas un fichier JPEG
 "%s" a d�j� �t� sign� localement par la cl� %08lX
 "%s" a d�j� �t� sign� localement par la cl� %08lX
 # Liste des valeurs de confiance assign�es, cr��e le %s
# (Utilisez � gpg --import-ownertrust � pour les restaurer)
 %08lX: Il n'est pas s�r que cette cl� appartient vraiment � son
propri�taire mais elle est quand m�me accept�e
 %08lX: Rien ne dit que la cl� appartient vraiment au propri�taire.
 %08lX: Nous ne faisons PAS confiance � cette cl�
 %08lX: la cl� a expir�
 %d mauvaises signatures
 %d signatures non v�rifi�es � cause d'erreurs
 %d signatures non v�rifi�es � cause de cl�s manquantes
 %d nom d'utilisateurs sans auto-signature valide d�tect�
 %lu cl�s v�rifi�es (%lu signatures)
 %lu cl�s trait�es jusqu'ici (%lu signatures)
 %lu cl�s trait�es jusqu'ici
 %s ...
 %s n'expire pas du tout
 donn�es chiffr�es avec %s
 le chiffrement %s sera utilis�
 %s expire le %s
 %s n'est pas une table de caract�res valide
 %s est le nouveau
 %s est le fichier original
 %s n'a aucun sens avec %s !
 %s n'est pas permis avec %s !
 Signature %s de: "%s"
 %s%c %4u%c/%08lX  cr��e: %s expire: %s %s.
 %s/%s chiffr� pour: "%s"
 %s: ATTENTION: fichier vide
 %s: impossible d'acc�der: %s
 %s: impossible de cr�er le r�pertoire: %s
 %s: impossible de cr�er le verrouillage
 %s: impossible de cr�er: %s
 %s: impossible de cr�er le verrou
 %s: impossible d'ouvrir: %s
 %s: r�pertoire cr��
 %s: le r�pertoire n'existe pas !
 %s: erreur pendant la lecture de l'enregistrement libre: %s
 %s: erreur pendant la lecture de l'enregistrement de version: %s
 %s: erreur pendant la mise � jour de l'enregistrement de version: %s
 %s: erreur pendant l'�criture de l'enregistrement de
r�pertoire: %s
 %s: erreur pendant l'�criture de l'enregistrement de version: %s
 %s: impossible d'ajouter un enregistrement: %s
 %s: la cr�ation de la table de hachage a �chou�: %s
 %s: impossible de cr�er un enregistrement de version: %s %s: n'a pu mettre un enregistrement � z�ro: %s
 %s: version %d du fichier invalide
 %s: base de confiance invalide
 %s: base de confiance invalide cr��e
 %s: porte-cl�s cr��
 %s: ce n'est pas un fichier de base de confiance
 %s: ignor�: %s
 %s: ignor�: cl� publique d�j� pr�sente
 %s: ignor�: la cl� publique est d�sactiv�e
 %s: base de confiance cr��e
 %s: suffixe inconnu
 %s: enregistrement de version avec un num�ro %lu
 %s:%d: option d�conseill�e "%s"
 %s:%d: options d'export invalides
 %s:%d: options d'import invalides
 cl� de %u bits %s, ID %08lX, cr��e le %s (Aucune description donn�e)
 (Vous devriez s�rement s�lectionner %d ici)
 (c'est une cl� de r�vocation sensible)
 (� moins de sp�cifier la cl� par son empreinte)
 (vous avez peut-�tre utilis� un programme non adapt� � cette fin)
 --clearsign [nom du fichier] --decrypt [nom du fichier] --edit-key utilisateur [commandes] --encrypt [nom du fichier] --lsign-key utilisateur --nrlsign-key utilisateur --nrsign-key utilisateur --output n'est pas compatible avec cette commande
 --sign --encrypt [nom du fichier] --sign --symmetric [nom du fichier] --sign [nom du fichier] --sign-key utilisateur --store [nom du fichier] --symmetric [nom du fichier] -k[v][v][v][c] [utilisateur] [porte-cl�s] ... c'est un bug (%s:%d:%s)
 une mauvaise signature
 une signature non v�rifi�e � cause d'une cl� manquante
 une signature non v�rifi�e � cause d'une erreur
 un nom d'utilisateur sans auto-signature valide d�tect�
 @
(Voir la page de manuel pour une liste compl�te des commandes et options)
 @
Exemples:

 -se -r Alice [fichier]       signer et chiffrer pour l'utilisateur Alice
 --clearsign [fichier]        faire une signature en texte clair
 --detach-sign [fichier]      faire une signature d�tach�e
 --list-keys [utilisateur]    montrer les cl�s
 --fingerprint [utilisateur]  montrer les empreintes
 @
Options:
  @Commandes:
  sortie avec armure ASCII forc�e.
 Pr�paration � la g�n�ration d'une nouvelle paire de cl�s %s.
            la taille minimale est  768 bits
          la taille par d�faut est 1024 bits
 la taille maximale conseill�e est 2048 bits
 Bien que ces cl�s soient d�finies dans la RFC2440 elles ne sont pas
conseill�es car tous les programmes ne les supportent pas et les
signatures cr��es avec elles sont plut�t longues et tr�s lentes � v�rifier. R�pondez �oui� (ou simplement �o�) pour g�n�rer la sous-cl� R�pondez �oui� s'il faut vraiment supprimer la sous-cl� R�pondez �oui� s'il faut vraiment r��crire le fichier R�pondez �oui� si vous voulez vraiment supprimer ce nom
d'utilisateur. Tous les certificats seront alors perdus en m�me temps ! R�pondez �oui� si vous voulez signer TOUS les noms d'utilisateurs R�pondez �oui� ou �non� Etes-vous vraiment s�r(e) que vous voulez signer cette cl�
avec la v�tre: " Etes-vous s�r de vouloir cette taille ?  Etes-vous s�r de vouloir l'ajouter ? (y/N)  Etes-vous s�r de vouloir toujours le r�voquer ? (y/N)  Etes-vous s�r de toujours vouloir le signer ? (o/N)  Etes-vous s�r de vouloir l'utiliser ? (o/N)  MAUVAISE signature de " Erreur de CRC; %06lx - %06lx
 Impossible de v�rifier la signature: %s
 Impossible d'�diter cette cl�: %s
 Annuler Certificats conduisant vers une cl� � confiance ultime:
 Changer le (N)om, le (C)ommentaire, l'(E)-mail ou (O)K/(Q)uitter ?  Changer le (N)om, le (C)ommentaire, l'(E)-mail ou (Q)uitter ?  Changer les pr�f�rences de tous les noms d'utilisateurs (ou juste
ceux qui sont s�lectionn�s) vers la liste actuelle. La date de toutes
les auto-signatures affect�es seront avanc�es d'une seconde.
 Changer la date d'expiration d'une cl� secondaire.
 Changer la date d'expiration de la cl� principale.
 Chiffrement:  Commande>  Commentaire:  Compression:  G�n�rer un certificat de r�vocation pour cette cl� ?  G�n�rer un certificat de r�vocation pour cette signature ? (o/N)  Cr�er quand m�me ?  Notation de signature critique:  Politique de signature critique:  La paire de cl�s DSA fera 1024 bits.
 DSA permet seulement des tailles comprises entre 512 et 1024
 DSA n�cessite l'utilisation d'un algorithme de hachage de 160 bits
 Enlever l'armure d'un fichier ou de
l'entr�e standard Supprimer cette bonne signature ? (o/N/q) Supprimer cette signature invalide ? (o/N/q) Enlever cette cl� du porte-cl�s ?  Supprimer cette signature inconnue ? (o/N/q) %d signature supprim�e.
 %d signatures supprim�es
 Signature d�tach�e.
 Hachage:  Affichage %s photo d'identit� de taille %ld pour la cl� 0x%08lX (uid %d)
 Voulez-vous vraiment supprimer les cl�s s�lectionn�es ?  Voulez-vous vraiment supprimer cette cl� ?  Voulez-vous vraiment faire cela ?  Voulez-vous vraiment r�voquer les cl�s s�lectionn�es ?  Voulez-vous vraiment r�voquer cette cl� ?  Voulez-vous vraiment donner une confiance ultime � cette cl� ? Voulez-vous cr�er une nouvelle signature pour remplacer celle qui a
expir� ? (o/N)  Voulez vous la rendre compl�tement exportable ? (o/N)  Voulez vous la changer en une auto-signature OpenPGP ? (o/N)  Voulez-vous vraiment le signer encore une fois ? (o/N)  Voulez-vous que votre signature expire en m�me temps ? (O/n)  Ne pas montrer les photos d'identit� Adresse e-mail:  Mettre une armure � un fichier ou �
l'entr�e standard Entrez le nom du fichier JPEG pour la photo d'identit�:  Entrez une description optionnelle ; terminez-l� par une ligne vide:
 Entrez le nouveau nom de fichier Entrez le mot de passe
 Entrez le mot de passe:  Entrez le nom du propri�taire de la cl� Entrez le nouveau mot de passe pour cette cl� secr�te.
 Entrez la valeur demand�e comme indiqu� dans la ligne de commande.
On peut entrer une date ISO (AAAA-MM-JJ) mais le r�sultat d'erreur sera
mauvais - le syst�me essaierait d'interpr�ter la valeur donn�e comme un
intervalle. Entrez la taille de la cl� Entrez le nom d'utilisateur de la personne � qui vous voulez envoyer
le message. Entrez le nom d'utilisateur du r�vocateur d�sign�:  Les algorithmes exp�rimentaux ne devraient pas �tre utilis�s !
 Signature expir�e de " Fonctions:  Le fichier `%s' existe.  Donnez le nom du fichier auquel la signature se rapporte Vous pouvez taper votre message...
 Bonne signature de " Hachage:  Aide: S�lectionner les utilisateurs � signer
 Avec quel soin avez-vous v�rifi� que la cl� que vous allez signer
appartient r�ellement � la personne sus-nomm�e ? Si vous ne savez
quoi r�pondre, entrez "0".
 L'algorithme IDEA n'est pas disponible, avec un peu de chance %s marchera
peut-�tre
 Si vous le d�sirez, vous pouvez entrer un texte qui explique pourquoi vous
avez �mis ce certificat de r�vocation. Essayez de garder ce texte concis.
Une ligne vide d�limite la fin du texte.
 Si vous voulez utiliser cette cl� r�voqu�e quand-m�me, r�pondez �oui�. Si vous voulez utiliser cette cl� peu s�re quand-m�me, r�pondez �oui�. En g�n�ral ce n'est pas une bonne id�e d'utiliser la m�me cl� pour
signer et pour chiffrer. Cet algorithme ne doit �tre utilis� que
pour certains domaines.
Consultez votre expert en s�curit� d'abord. Caract�re invalide dans le commentaire
 Caract�re invalide dans le nom
 Commande invalide  (essayez �help�)
 La cl� invalide %08lX a �t� rendue valide par --allow-non-selfsigned-uid
 Mot de passe invalide ; r�essayez Choix invalide.
 Est-ce correct (o/n) ?  Est-ce d'accord ?  Cette photo est-elle correcte (o/N/q) ?  Il n'est PAS certain que la cl� appartient � sos propri�taire.
Si vous savez *vraiment* ce que vous faites, vous pouvez r�pondre
oui � la prochaine question

 C'est � vous d'assigner une valeur ici; cette valeur ne sera jamais
envoy�e � une tierce personne. Nous en avons besoin pour cr�er le r�seau
de confiance (web-of-trust); cela n'a rien � voir avec le r�seau des
certificats (cr�� implicitement) La g�n�ration de cl� a �t� annul�e.
 La g�n�ration de cl� a �chou�: %s
 La cl� a �t� compromise La cl� n'est plus utilis�e La cl� est prot�g�e.
 La cl� est r�voqu�e. La cl� a �t� remplac�e La cl� est valide pour ? (0)  La cl� n'a pas chang� donc la mise � jour est inutile.
 Porte-cl�s Les tailles sup�rieures � 2048 ne sont pas conseill�es car
les calculs prennent VRAIMENT beaucoup de temps !
 N pour changer le nom.
C pour changer le commentaire.
E pour changer l'adresse e-mail.
O pour continuer � g�n�rer la cl�.
Q pour arr�ter de g�n�rer de cl�. NOTE: %s n'est pas pour une utilisation normale !
 NOTE: Cl� principale Elgamal d�tect�e - l'importation peut prendre un
certain temps
 NOTE: Cette cl� n'est pas prot�g�e !
 NOTE: l'algorithme de chiffrement %d n'a pas �t� trouv� dans les pr�f�rences
 NOTE: cr�er des sous-cl�s pour des cl�s v3 n'est pas conforme � OpenPGP
 NOTE: la cl� a �t� r�voqu�e NOTE: pas de fichier d'options par d�faut `%s'
 NOTE: l'ancien fichier d'options par d�faut `%s' a �t� ignor�
 NOTE: la cl� secr�te %08lX a expir� le %s
 NOTE: l'exp�diteur a demand� �pour vos yeux seulement�
 NOTE: la cl� de signature %08lX a expir� le %s
 NOTE: le mode S2K simple (0) est fortement d�conseill�
 NOTE: la base de confiance n'a pas les permissions d'�criture
 Le nom ne doit pas commencer par un chiffre
 Le nom doit faire au moins 5 caract�res de long
 Il faut la cl� secr�te pour faire cela.
 NnCcEeOoQq Pas de signature correspondante dans le porte-cl�s secret
 Pas d'aide disponible Pas d'aide disponible pour `%s' Aucune raison sp�cifi�e Pas de cl� secondaire avec l'index %d
 Pas d'utilisateur de ce nom.
 Pas de confiance d�finie pour :
%4u%c/%08lX %s " Pas d'utilisateur avec l'index %d
 Ce n'est pas une adresse e-mail valide
 Notation:  Notez que cette cl� ne peut �tre utilis�e pour chiffrer. Vous pouvez
utiliser la commande �--edit-key� pour g�n�rer une cl� secondaire �
cette fin.
 Note: cette cl� a �t� d�sactiv�e.
 Note: Cette cl� a expir� !
 Rien n'a �t� supprim�.
 Rien � signer avec la cl� %08lX
 D'accord, mais n'oubliez pas que les radiations de votre �cran et de votre
clavier sont aussi tr�s vuln�rables aux attaques !
 R��crire (o/N)?  Corrigez l'erreur d'abord
 � quel point avez-vous confiance en cet utilisateur pour la v�rification
des cl�s des autres utilisateurs (vous pouvez v�rifier son passeport,
v�rifier les empreintes de diverses sources...) ?
 Ne mettez pas d'adresse e-mail dans le nom r�el ou dans le commentaire
 Entrez le nouveau nom de fichier. Si vous tapez simplement ENTR�E le
fichier par d�faut (indiqu� entre crochets) sera utilis�. Entrez un commentaire optionnel Entrez le nom du fichier de donn�es:  Entrez le mot de passe ; c'est une phrase secr�te 
 R�parez ce probl�me de s�curit� possible
 Notez que la validit� affich�e pour la cl� n'est pas n�cessairement
correcte � moins de relancer le programme.
 Enlevez les s�lections des cl�s secr�tes.
 R�p�tez le dernier mot de passe pour �tre s�r de ce que vous avez tap�. Signaler toutes anomalies � <gnupg-bugs@gnu.org> (en anglais)
et tout probl�me de traduction � <traduc@traduc.org>.
 Vous devez s�lectionner au plus une cl� secondaire.
 Vous devez s�lectionner exactement un utilisateur.
 choisissez la cause de la r�vocation:
 S�lectionnez le type de cl� d�sir�:
 Sp�cifiez combien de temps cette cl� devrait �tre valide.
         0 = la cl� n'expire pas
      <n>  = la cl� expire dans n jours
      <n>w = la cl� expire dans n semaines
      <n>m = la cl� expire dans n mois
      <n>y = la cl� expire dans n ann�es
 Sp�cifiez combien de temps la signature devrait �tre valide.
         0 = la signature n'expire pas
      <n>  = la signature expire dans n jours
      <n>w = la signature expire dans n semaines
      <n>m = la signature expire dans n mois
      <n>y = la signature expire dans n ann�es
 Utilisez la commande �toggle� d'abord.
 Attendez s'il vous pla�t que l'entropie soit r�cup�r�e. Vous pouvez
faire autre chose pour ne pas vous ennuyer, car cela donnera une
meilleure qualit� � l'entropie.
 Politique:  Empreinte de cl� principale: Cl� publique:  La cl� publique est d�sactiv�e.
 Quitter sans enregistrer?  Nom r�el:  Cr�er r�ellement les certificats de r�vocation ? (o/N)  Cr�er vraiment ?  Faut-il vraiment supprimer cette auto-signature ? (o/N) Enlever r�ellement tous les utilisateurs s�lectionn�s ?  Enlever r�ellement cet utilisateur ?  R�voquer r�ellement tous les noms d'utilisateurs s�lectionn�s ?  R�voquer r�ellement ce nom d'utilisateur ?  Signer vraiment tous les utilisateurs ?  Signer r�ellement ?  Enlever r�ellement les pr�f�rences des utilisateurs s�lectionn�s ?  Faut-il vraiment mettre � jour les pr�f�rences ?  Cause de r�vocation: %s
 R�p�tez le mot de passe
 R�p�tez le mot de passe:  La taille demand�e est %u bits
 Certificat de r�vocation cr��.
 Certificat de r�vocation cr��.

D�placez-le dans un support que vous pouvez cacher ; si Mallory a
acc�s � ce certificat il peut l'utiliser pour rendre votre cl�
inutilisable.
Une bonne id�e consiste � imprimer ce certificat puis � le stocker
ailleurs, au cas o� le support devient illisible. Mais attention :
le syst�me d'impression de votre machine pourrait stocker ces
donn�es et les rendre accessibles � d'autres personnes !
 Enregistrer les changements?  La cl� secr�te est disponible.
 Les parties secr�tes de la cl� principale ne sont pas disponibles.
 S�lectionnez l'algorithme � utiliser.
DSA (alias DSS) est l'algorithme de signatures �lectroniques qui ne peut
�tre utilis� que pour les signatures. C'est l'algorithme recommand� car
la v�rification des signatures DSA est beaucoup plus rapide que celle des
signatures ElGamal.

ElGamal est un algorithme pouvant � la fois �tre utilis� pour les
signatures et le chiffrement. OpenPGP en distingue deux sortes:
l'une destin�e uniquement au chiffrement et l'autre pouvant aussi bien
servir aux signatures ; elles sont en fait identiques mais certains
param�tres doivent �tre sp�cialement choisis pour que la cl� g�n�re des
signatures s�res: ce programme est capable de le faire mais les autres
implantations de OpenPGP ne sont pas oblig�es d'accepter cette forme de
cl�.

La premi�re cl� (cl� principale) doit toujours �tre capable de signer ;
c'est pourquoi la cl� ElGamal de chiffrement seul est alors d�sactiv�e. Choisir la ligne de commande servant �
afficher les photos d'identit� Montrer les photos d'identit� La signature est valide pour ? (0)  Signature faite %.*s avec une cl� %s ID %08lX
 Notation de signature:  Politique de signature:  Syntaxe: gpg [options] [fichiers]
Verifier des signatures avec des cl�s de confiance connues
 Syntaxe: gpg [options] [fichiers]
signer, v�rifier, chiffrer ou d�chiffrer
l'op�ration par d�faut d�pend des donn�es entr�es
 Le g�n�rateur de nombres al�atoires n'est qu'un artifice visant � ex�cuter
GnuPG - ce n'est en aucune mani�re un g�n�rateur (RNG) fort!

N'UTILISEZ PAS LES DONN�ES G�N�R�ES PAR CE PROGRAMME !!

 L'auto-signature de "%s"
est de style PGP 2.x.
 Cette signature n'est pas valide. Vous devriez la supprimer de votre
porte-cl�s. L'utilisation de cet algorithme n'est support�e que par GnuPG. Vous ne
pourrez pas utiliser cette cl� pour communiquer avec les utilisateurs de
PGP. Cet algorighme est aussi tr�s lent, et n'est peut-�tre pas aussi
s�r que les autres choix.
 Il n'y a pas de pr�f�rences dans un nom d'utilisateur du style de
PGP 2.x.
 Cette commande n'est pas admise en mode %s.
 C'est une cl� secr�te - faut-il vraiment l'effacer ?  Cette signature relie le nom d'utilisateur � la cl�. Habituellement
enlever une telle signature n'est pas une bonne id�e. En fait GnuPG peut
ne plus �tre capable d'utiliser cette cl�. Donc faites ceci uniquement si
cette auto-signature est invalide pour une certaine raison et si une autre
est disponible. C'est une signature valide dans la cl�; vous n'avez pas normalement
int�r�t � supprimer cette signature car elle peut �tre importante pour
�tablir une connection de confiance vers la cl� ou une autre cl� certifi�e
par celle-l�. Cette cl� nous appartient
 Cette cl� a �t� d�sactiv�e Cette cl� a expir� ! Cette cl� va expirer le %s.
 Cette cl� n'est pas prot�g�e.
 Cette cl� peut �tre r�voqu�e par la cl� %s  Cette cl� appartient probablement � son propri�taire
 Cette signature ne peut pas �tre v�rifi�e parce que vous n'avez pas la
cl� correspondante. Vous devriez remettre sa supression jusqu'� ce que
vous soyez s�r de quelle cl� a �t� utilis�e car cette cl� de signature
peut �tablir une connection de confiance vers une autre cl� d�j� certifi�e. Cette signature a expir� le %s.
 Cela rendra la cl� inutilisable par PGP 2.x.
 Doit �tre r�voqu� par:
 Pour mettre en place le R�seau de confiance (Web of Trust), GnuPG a
besoin de savoir en quelles cl�s votre confiance est ultime - ce sont
en g�n�ral les cl�s dont vous avez acc�s � la cl� secr�te. R�pondez
"oui" pour indiquer que votre confiance en cette cl� est ultime
        Quantit� totale trait�e: %lu
 Impossible d'ouvrir la photo � %s �: %s
 Utilisation: gpg [options] [fichiers] (-h pour l'aide) Utilisation: gpgv [options] [fichiers] (-h pour l'aide) Utiliser cette cl� quand m�me ?  Le nom d'utilisateur "%s" est r�voqu�. Le nom d'utilisateur n'est plus valide AVERTISSEMENT: "%s" est une option d�conseill�e.
 ATTENTION: %s remplace %s
 ATTENTION: 2 fichiers avec des informations confidentielles existent.
 AVERTISSEMENT: C'est une cl� du style PGP2.  Ajouter un r�vocateur
d�sign� peut emp�cher certaines versions de PGP d'accepter
cette cl�.
 ATTENTION: C'est une cl� du style PGP2.  Ajouter une photo
d'identit� peut emp�cher certaines versions de PGP d'accepter
cette cl�
 ATTENTION: Cette cl� � �t� r�voqu�e par son propri�taire !
 ATTENTION: Cette cl� n'est pas certifi�e avec une signature de confiance !
 ATTENTION: Les signatures de cette cl� n'ont pas une confiance suffisante !
 ATTENTION: Cette sous-cl� � �t� r�voqu�e par son propri�taire !
 ATTENTION: Utilisation d'une cl� sans confiance !
 ATTENTION: Nous ne faisons PAS confiance � cette cl� !
 ATTENTION: Cl� faible d�tect�e - changez encore le mot de passe.
 AVERTISSEMENT: `%s' est un fichier vide
 AVERTISSEMENT: une signature de nom d'utilisateur date de %d secondes
dans le futur
 AVERTISSEMENT: le message chiffr� a �t� manipul� !
 ATTENTION: des donn�es de notation invalides ont �t� d�tect�es
 AVERTISSEMENT: la taille du fichier `random_seed' est invalide.
Celui-ci ne sera pas utilis�.
 AVERTISSEMENT: la cl� %08lX est peut-�tre r�voqu�e: recherche de la cl� de
r�vocation %08lX
 AVERTISSEMENT: la cl� %08lX est peut-�tre r�voqu�e: la cl� de r�vocation
%08lX est absente.
 ATTENTION: Le message a �t� chiffr� avec une cl� faible pendant le
chiffrement sym�trique.
 AVERTISSEMENT: l'int�grit� du message n'�tait pas prot�g�e
 AVERTISSEMENT: plusieurs signatures ont �t� d�t�ct�es. Seulement la premi�re
sera v�rifi�e.
 ATTENTION: rien n'a �t� export�
 AVERTISSEMENT: les options de `%s' ne sont pas encore actives cette fois
 ATTENTION: Le programme peut cr�er un fichier �core� !
 AVERTISSEMENT: des r�cipients (-r) ont donn�s alors que le chiffrement
ne se fait pas par cl� publique
 AVERTISSEMENT: conflit de hachage de signature dans le message
 AVERTISSEMENT: impossible de faire une expansion � base de %%
(cha�ne trop grande). Utilisation de la version non expans�e.
 AVERTISSEMENT: impossible de faire une expansion � base de %% de l'url
de politique (trop grande). Utilisation de la version non expans�e.
 AVERTISSEMENT: impossible d'effacer le r�pertoire temporaire `%s':
%s
 AVERTISSEMENT: impossible d'enlever le fichier temporaire
(%s) `%s': %s
 AVERTISSEMENT: le propri�taire du r�pertoire contenant est peu
s�r pour %s "%s"
 AVERTISSEMENT: les permissions du r�pertoire contenant %s "%s"
sont peu s�res
 AVERTISSEMENT: propri�taire de %s "%s" peu s�r
 AVERTISSEMENT: permissions de %s "%s" peu s�res
 AVERTISSEMENT: l'utilisation de la m�moire n'est pas s�re !
 ATTENTION: utilisation d'un g�n�rateur de nombres al�atoires peu s�r !!
 Un grand nombre d'octets al�atoires doit �tre g�n�r�. Vous devriez faire
autre-chose (taper au clavier, d�placer la souris, utiliser les disques)
pendant la g�n�ration de nombres premiers; cela donne au g�n�rateur de
nombres al�atoires une meilleure chance d'avoir assez d'entropie.
 Quelle taille de cl� d�sirez-vous ? (1024)  Quand vous signez un nom d'utilisateur d'une cl�, vous devriez d'abord
v�rifier que la cl� appartient � la personne nomm�e. Il est utile que
les autres personnes sachent avec quel soin vous l'avez v�rifi�.

"0" signifie que vous n'avez pas d'opinon.

"1" signifie que vous croyez que la cl� appartient � la personne qui
dit la poss�der mais vous n'avez pas pu v�rifier du tout la cl�.
C'est utile lorsque vous signez la cl� d'un pseudonyme.

"2" signifie que vous avez un peu v�rifi� la cl�. Par exemple, cela
pourrait �tre un v�rification de l'empreinte et du nom de
l'utilisateur avec la photo.

"3" signifie que vous avez compl�tement v�rifi� la cl�. Par exemple,
cela pourrait �tre une v�rification de l'empreinte, du nom de
l'utilisateur avec un document difficile � contrefaire (comme un
passeport) et de son adresse e-mail (v�rifi� par un �change de
courrier �lectronique).

Notez bien que les exemples donn�s ci-dessus pour les niveaux 2 et
3 ne sont *que* des exemples.
C'est � vous de d�cider quelle valeur mettre quand vous signez
les cl�s des autres personnes.

Si vous ne savez pas quelle r�ponse est la bonne, r�pondez "0". Vous �tes sur le point de r�voquer ces signatures:
 Vous utilisez le jeu de caract�res '%s'.
 Vous ne pouvez pas changer la date d'expiration d'une cl� v3
 Vous ne pouvez pas supprimer le dernier utilisateur !
 Vous n'avez pas sp�cifi� de nom d'utilisateur. (vous pouvez
utiliser �-r�)

 Vous ne voulez pas de mot de passe - cela est certainement une
*mauvaise* id�e

 Vous ne voulez pas de mot de passe - c'est s�rement une *mauvaise* id�e !
Je l'accepte quand-m�me. Vous pouvez changer votre mot de passe quand vous
le d�sirez, en utilisant ce programme avec l'option � --edit-key �.

 Vous avez sign� ces noms d'utilisateurs:
 Vous ne pouvez pas ajouter de r�vocateur d�sign� � une cl� de style PGP2.
 Vous ne pouvez pas ajouter de photo d'identit� � une cl� du style PGP2.
 il n'est pas possible de g�n�rer une signature OpenPGP d'une cl� de style
PGP 2.x en mode --pgp2.
 Vous devez s�lectionner au moins une cl�.
 Vous devez s�lectionner au moins un utilisateur.
 Vous avez besoin d'un mot de passe pour prot�ger votre cl� secr�te.

 Vous avez besoin d'un mot de passe pour d�verrouiller la cl� secr�te pour
l'utilisateur:
"%.*s"
cl� %u bits %s, ID %08lX, cr��e %s%s
 Vous avez s�lectionn� ce nom d'utilisateur:
    "%s"

 Vous devriez donner une raison pour la certification. Selon le contexte
vous pouvez choisir dans cette liste:
  �La cl� a �t� compromise�
      Utilisez cette option si vous avez une raison de croire que des
      personnes ont pu acc�der � votre cl� secr�te sans autorisation.
  �La cl� a �t� remplac�e�
      Utilisez cette option si vous avez remplac� la cl� par une nouvelle.
  �La cl� n'est plus utilis�e�
      Utilisez cette option si cette cl� n'a plus d'utilit�.
  �Le nom d'utilisateur n'est plus valide�
      Utilisez cette option si le nom d'utilisateur ne doit plus �tre
      utilis�. Cela sert g�n�ralement � indiquer qu'une adresse e-mail
      est invalide.
 Votre signature actuelle de "%s"
a expir�.
 Votre signature actuelle de "%s"
est locale.
 Votre d�cision ?  Votre choix ?  Votre syst�me ne sait pas afficher les dates au-del� de 2038.
Cependant la gestion des dates sera correcte jusqu'� 2106.
 [Nom utilisateur introuvable] [nom du fichier] [r�vocation] [auto-signature] [incertain] `%s' d�j� compress�
 `%s' n'est pas un fichier r�gulier - ignor�
 `%s' n'est pas une identification de cl� longue valide
 le nom d'une notation ne doit comporter que des caract�res imprimables
ou des espaces, et se terminer par un signe '='
 une valeur de notation ne doit utiliser aucun caract�re de contr�le
 un nom de notation utilisateur doit contenir le caract�re '@'
 ajouter une photo d'identit� ajouter une cl� de r�vocation ajouter une cl� secondaire ajouter un utilisateur ajouter ce porte-cl�s � la liste ajouter ce porte-cl�s secret � la liste aj.cl� aj.photo aj.rev aj.ut toujours utiliser un sceau pour le chiffrement destinataire anonyme; essai de la cl� secr�te %08lX...
 en-t�te d'armure:  armure: %s
 r�pondre non � la plupart des questions r�pondre oui � la plupart des questions on suppose des donn�es chiffr�es avec %s
 la signature de la cl� %08lX est suppos�e �tre fausse car un bit
critique est inconnu
 les donn�es sign�es sont suppos�es �tre dans `%s'
 mauvais entier en pr�cision multiple (MPI) mauvaise adresse (URI) mauvais certificat mauvaise cl� mauvais mot de passe mauvaise cl� publique mauvaise cl� secr�te mauvaise signature mode automatique: ne jamais rien demander devenir beaucoup plus silencieux build_packet a �chou�: %s
 c impossible de fermer `%s': %s
 impossible de se connecter � `%s': %s
 impossible de cr�er %s: %s
 impossible de cr�er `%s': %s
 impossible de cr�er le r�pertoire `%s': %s
 impossible d'emp�cher la g�n�ration de fichiers �core�: %s
 impossible de faire cela en mode automatique
 impossible de faire cela en mode automatique sans �--yes�
 impossible d'obtenir les cl�s du serveur: %s
 impossible d'obtenir le descripteur de lecture du serveur
pour l'agent
 impossible d'obtenir le descripteur d'�criture du serveur pour l'agent
 impossible de g�rer l'algorithme � cl� publique %d
 impossible de traiter les lignes plus longues que %d caract�res
 le traitement de ces signatures multiples est impossible
 impossible d'ouvrir %s: %s
 impossible d'ouvrir `%s'
 impossible d'ouvrir `%s': %s
 impossible d'ouvrir le fichier: %s
 impossible d'ouvir les donn�es sign�es `%s'
 impossible d'ouvrir le porte-cl�s impossible de demander un mot de passe en mode automatique
 impossible de lire `%s': %s
 impossible de chercher une cl� dans le serveur : %s
 impossible d'obtenir le pid du client pour l'agent
 impossible d'acc�der � `%s': %s
 il n'est pas possible d'utiliser un paquet ESK sym�trique en mode S2K
 impossible d'�crire `%s': %s
 annul� par l'utilisateur
 impossible d'utiliser une cl� de style PGP 2.x comme r�vocateur
d�sign�.
 impossible d'�viter une cl� faible pour le chiffrement sym�trique:
%d essais ont eu lieu !
 changer la date d'expiration changer la confiance changer le mot de passe v�rifier v�rifier les signatures des cl�s v�rification � la profondeur %d sign�=%d ot(-/q/n/m/f/u)=%d/%d/%d/%d/%d/%d
 Impossible de v�rifier la signature cr��e: %s
 v�rification du porte-cl�s `%s'
 v�rifier la base de confiance
 somme de contr�le erron�e l'algorithme de chiffrement %d%s est inconnu ou d�sactiv�
 l'extension de chiffrement "%s" n'a pas �t� charg�e car ses
permissions sont peu s�res
 probl�me de communication avec ssh-agent
 �completes-needed� doit �tre sup�rieur � 0
 l'algorithme de compression doit faire partie de l'intervalle %d..%d
 commandes en conflit
 impossible d'interpr�ter l'URI du serveur de cl�s
 cr�er une sortie ascii avec armure les donn�es ne sont pas enregistr�es; utilisez l'option �--output� pour
les enregistrer
 la suppression d'une armure a �chou�: %s
 d�boguer d�chiffrer les donn�es (d�faut) le d�chiffrement a �chou�: %s
 le d�chiffrement a r�ussi
 enlever une cl� secondaire supprimer les signatures enlever un utilisateur la suppression du bloc de cl�s a �chou� : %s
 suppr.cl� suppr.photo suppr.sign suppr.ut l'algorithme de hachage `%s' ne marche que pour la lecture dans cette
version
 d�sactiver d�sactiver une cl� ne pas forcer les signatures en v3 ne pas forcer les signatures en v4 ne rien changer ne pas utiliser du tout le terminal imiter le mode d�crit dans la RFC1991 activer activer une cl� la construction d'une armure a �chou�: %s 
 chiffrer les donn�es chiffr� avec une cl� %s, %08lX
 chiffr� avec une cl� de %u bits %s, ID %08lX, cr��e le %s
 chiffr� avec l'algorithme inconnu %d
 chiffrer un message en mode --pgp2 n�cessite l'algorithme de chiffrage IDEA
 chiffrement sym�trique seulement erreur pendant la cr�ation de `%s': %s
 erreur durant la cr�ation du porte-cl�s `%s' : %s
 erreur pendant la cr�ation du mot de passe: %s
 erreur pendant la recherche de l'enregistrement de confiance: %s
 erreur dans la ligne de remorque
 erreur pendant la lecture de `%s': %s
 erreur pendant la lecture du bloc de cl� : %s
 erreur pendant la lecture du bloc de cl� secr�te `%s': %s
 erreur pendant l'envoi de `%s': %s
 erreur durant l'�criture du porte-cl�s `%s': %s
 erreur durant l'�criture du porte-cl�s public `%s': %s
 erreur durant l'�criture du porte-cl�s secret `%s': %s
 erreur: empreinte invalide
 erreur: symbole deux-points manquant
 erreur: pas de valeur de confiance au propri�taire
 expire exporter les cl�s exporter les cl�s vers un serveur de cl�s exporter les indices de confiance les appels aux programmes externes sont d�sactiv�s car les permissions
du fichier d'options sont trop peu s�res
 l'envoi � `%s' a �chou�: le r�sultat est %u
 impossible d'initialiser la base de confiance: %s
 la reconstruction du cache de porte-cl�s a �chou� : %s
 erreur de fermeture de fichier erreur de cr�ation de fichier erreur pendant la suppression du fichier le fichier existe erreur d'ouverture de fichier erreur de lecture erreur pendant le changement de nom du fichier erreur d'�criture r�parer une base de confiance corrompue marquer le nom d'utilisateur comme principal forcer les signatures en v3 forcer les signatures en v4 forcer l'algorithme de compression %s (%d) entre en d�saccord
avec les pr�f�rences du r�cipient
 forcer l'algorithme de chiffrement %s (%d) entre en d�saccord avec
les pr�f�rences du r�cipient
 forcer le chiffrement sym�trique %s (%d) entre en d�saccord
avec les pr�ferences du r�cipient
 fpr erreur g�n�rale g�n�rer une nouvelle paire de cl�s g�n�rer un certificat de r�vocation g�n�ration de la somme de contr�le de 16 bits (d�pr�ci�e) pour prot�ger
la cl� secr�te
 gpg-agent n'est pas disponible dans cette session
 le protocole gpg-agent version %d n'est pas support�
 help iImMqQsS importer les cl�s d'un serveur de cl�s importer les indices de confiance importer/fusionner les cl�s la ligne d'entr�e %u est trop longue ou il manque un caract�re de saut
de ligne
 la ligne d'entr�e est plus longue que %d caract�res
 mode S2K invalide; ce doit �tre 0, 1 ou 3
 argument invalide armure invalide en-t�te d'armure invalide:  armure invalide: ligne plus longue que %d caract�res
 Caract�re invalide dans la cha�ne de pr�f�rences
 en-t�te de signature claire invalide
 ligne �chapp�e par `-' invalide:  pr�f�rences par d�faut invalides
 default-check-level invalide; ce doit �tre 0, 1, 2 ou 3
 options d'export invalides
 algorithme de hachage `%s' invalide
 options d'import invalides
 porte-cl�s invalide paquet invalide mot de passe invalide pr�f�rences de chiffrement personnelles invalides
 pr�f�rences de compression personnelles invalides
 pr�f�rences de hachage personnelles invalides
 caract�re %02x invalide en base 64 ignor�
 r�ponse de l'agent invalide
 paquet racine invalide d�tect� dans proc_tree()
 l'algorithme � cl� sym�trique d�tect� est invalide (%d).
 valeur invalide
 cl� la cl� %08lX a �t� cr��e %lu seconde dans le futur (rupture
spatio-temporelle ou probl�me d'horloge)
 la cl� %08lX a �t� cr��e %lu secondes dans le futur (rupture
spatio-temporelle ou probl�me d'horloge
 cl� %08lX incompl�te
 cl� %08lX marqu�e comme ayant une confiance ultime.
 la cl� %08lX appara�t plusieurs fois dans la base de confiance
 cl� %08lX: "%s" %d nouvelles signatures
 cl� %08lX: "%s" %d nouvelles sous-cl�s
 cl� %08lX: "%s" %d nouveaux utilisateurs
 cl� %08lX: "%s" une nouvelle signature
 cl� %08lX: "%s" une nouvelle sous-cl�
 cl� %08lX: "%s" un nouvel utilisateur
 cl� %08lX: "%s" n'a pas chang�
 cl�: %08lX: "%s" certificat de r�vocation ajout�
 cl� %08lX: "%s" certificat de r�vocation import�
 cl� %08lX: corruption de sous-cl� HKP r�par�e
 cl� %08lX: cl� de style PGP 2.x - ignor�e
 cl� %08lX: accept�e comme cl� de confiance.
 cl� %08lX: nom d'utilisateur non auto-sign� accept� '%s':
 cl� %08lX: d�j� dans le porte-cl�s secret
 cl� %08lX: impossible de trouver le bloc de cl�s original: %s
 cl� %08lX: impossible de lire le bloc de cl�s original: %s
 cl� %08lX: ajout de la signature de cl� directe
 cl� %08lX: ne ressemble pas � notre copie
 cl� %08lX: nom d'utilisateur en double fusionn�
 cl� %08lX: certificat de r�vocation invalide: %s - rejet�
 cl� %08lX: certificat de r�vocation invalide: %s - ignor�e
 cl� %08lX: auto-signature du nom d'utilisateur "%s" invalide
 cl� %08lX: liaison avec la sous-cl� invalide
 cl� %08lX: r�vocation de sous-cl� invalide
 cl� %08lX: la cl� a �t� r�voqu�e !
 cl� %08lX: nouvelle cl� - ignor�e
 cl� %08lX: pas de cl� publique - le certificat de r�vocation ne peut
�tre appliqu�
 cl� %08lX: pas de cl� publique pour la cl� de confiance - ignor�e
 cl� %08lX: pas de sous-cl� pour relier la cl�
 cl� %08lX: pas de sous-cl� pour r�voquer la cl�
 cl� %08lX: pas de sous-cl� pour le paquet de r�vocation de sous-cl�
 cl� %08lX: pas de nom d'utilisateur
 cl� %08lX: pas d'utilisateur pour la signature
 cl� %08lX: pas de nom d'utilisateur valide
 cl� %08lX: signature non exportable (classe %02x) - ignor�e
 cl� %08lX: ce n'est pas une cl� rfc2440 - ignor�e
 cl� %08lX: non prot�g�e - ignor�e
 cl� %08lX: cl� publique "%s" import�e
 cl� %08lX: cl� publique pas trouv�e: %s
 cl� %08lX: supressions de liaisons multiples avec des sous-cl�s
 cl� %08lX: suppression de la r�vocation de sous-cl� multiples
 cl� %08lX: certificat de r�vocation au mauvais endroit - ignor�e
 cl� %08lX: cl� secr�te import�e
 cl� %08lX: cl� secr�te pas trouv�e: %s
 cl� %08lX: cl� secr�te avec le chiffrement invalide %d - non prise
en compte
 cl� %08lX: cl� secr�te sans cl� publique - non prise en compte
 cl� %08lX: sous-cl� non prise en compte
 cl� %08lX: utilisateur non pris en compte: ' cl� %08lX: la sous-cl� a �t� r�voqu�e !
 cl� %08lX: signature de sous-cl� au mauvais endroit - ignor�e
 cl� %08lX: ceci est une cl� ElGamal g�n�r�e par PGP qui n'est PAS
s�re pour les signatures !
 cl� %08lX: classe de signature non attendue (0x%02X) - ignor�e
 cl� %08lX: algorithme de cl� publique non support�
 cl� %08lX: algorithme de cl� publique non support� avec le nom
d'utilisateur "%s"
 cl� '%s' introuvable: %s
 la cl� a �t� cr��e %lu seconde dans le futur (rupture spatio-temporelle ou
probl�me d'horloge)
 la cl� a �t� cr��e %lu secondes dans le futur (rupture spatio-temporelle ou
probl�me d'horloge
 cl� incompl�te
 la cl� n'est pas marqu�e comme non-s�re; on ne peut pas l'utiliser avec le
pseudo-g�n�rateur de nombres al�atiores !
 cl� marqu�e comme ayant une confiance ultime.
 le porte-cl�s `%s` a �t� cr��
 erreur du serveur de cl�s Taille invalide; utilisation de %u bits
 taille arrondie � %u bits
 taille trop importante; %d est la plus grande valeur permise.
 taille trop petite; 1024 est la plus petite valeur permise pour RSA.
 taille trop petite; 768 est la plus petite valeur permise.
 l ligne trop longue
 lister lister la cl� et les noms d'utilisateurs lister les cl�s lister les cl�s et les empreintes lister les cl�s et les signatures ne lister que les paquets lister les pr�f�rences (expert) lister les pr�f�rences (bavard) lister les cl�s secr�tes lister les signatures lsigner faire une signature d�tach�e faire en sorte que les conflits d'horodatage ne soient qu'un
avertissement non fatal make_keysig_packet a �chou�: %s
 CRC d�form�
 la variable d'environnement GPG_AGENT_INFO est mal d�finie
 nom d'utilisateur malform� �marginals-needed� doit �tre sup�rieur � 1
 �max-cert-depth� doit �tre compris entre 1 et 255
 replacer la signature d'une cl� � l'endroit correct
 nN signatures en texte clair imbriqu�es
 erreur de r�seau jamais     ne jamais utiliser de sceau pour le
chiffrement  nouveau fichier de configuration `%s' cr��
 la prochaine v�rification de la base de confiance aura lieu le %s
 non no = signature trouv�e dans la d�finition du groupe "%s"
 pas de cl� publique correspondante: %s
 pas de porte-cl�s par d�faut: %s
 aucun module de r�cup�ration d'entropie n'a �t� trouv�.
 v�rification de la base de confiance inutile
 aucun programme d'ex�cution distante n'est support�
 aucune cl� de r�vocation trouv�e pour `%s'
 pas de cl� secr�te
 pas de donn�es sign�es
 pas d'utilisateur de ce nom aucune donn�e OpenPGP valide n'a �t� trouv�e.
 pas de destinataire valide
 aucun porte-cl� n'a �t� trouv� avec des droits d'�criture : %s
 aucun portes-cl�s public n'a �t� trouv� avec des droits d'�criture : %s
 aucun portes-cl�s secret n'a �t� trouv� avec des droits d'�criture : %s
 la signature n'est pas d�tach�e
 non chiffr� illisible par un humain non trait� non support� note: le fichier `random_seed' est vide
 note: le fichier `random_seed' n'a pas �t� mis � jour
 nrlsigner nrsigner d'accord, nous sommes le r�cipient anonyme.
 l'ancien codage de la cl� de chiffrement (DEK) n'est pas support�
 signature d'un ancien style (PGP 2.x)
 l'op�ration n'est pas possible tant que la m�moire s�re n'est pas
initialis�e
 fichier d'options `%s': %s
 nom de fichier original: '%.*s'
 les informations de confiance au propri�taires ont �t� effac�es
 le mot de passe n'a pas �t� correctement r�p�t� ; recommencez. mot de passe trop long
 mot.pas entrez une adresse e-mail optionnelle mais hautement recommand�e voir http://www.gnupg.org/fr/faq.html pour plus d'informations
 voir http://www.gnupg.org/fr/why-not-idea.html pour plus d'informations
 utilisez "%s%s" � la place
 pr�f pr�f�rence %c%lu dupliqu�e
 la pr�f�rence %c%lu n'est pas valide
 fin de fichier pr�matur�e (dans le CRC)
 fin de fichier pr�matur�e (dans la remorque)
 fin de fichier pr�matur�e (pas de CRC)
 principale probl�me de gestion des paquets chiffr�s
 probl�me avec l'agent - arr�t d'utilisation de l'agent
 probl�me avec l'agent : l'agent renvoie 0x%lx
 demander avant d'�craser un fichier l'algorithme de protection %d%s n'est pas support�
 les cl�s publique et secr�te ont �t� cr��es et sign�es.
 la cl� publique %08lX est plus r�cente de %lu seconde que la signature
 la cl� publique %08lX est plus r�cente de %lu secondes que la signature
 cl� publique %08lX non trouv�e : %s
 le d�chiffrement par cl� publique a �chou�: %s
 la cl� publique ne correspond pas � la cl� secr�te !
 donn�es chiffr�es par cl� publique: bonne cl� de chiffrement (DEK)
 la cl� publique est %08lX
 cl� publique non trouv�e la cl� publique de la cl� de confiace ultime %08lX est introuvable
 q qQ quitter quitter ce menu caract�re cit�-imprimable (quoted-printable) dans l'armure provenant
certainement d'un agent de transfert de messages bogu�
 erreur de lecture: %s
 lire les options du fichier lecture de `%s'
 lire les options de `%s'
 lecture de l'entr�e standard...
 cause de r�vocation:  enlever les cl�s du porte-cl�s public enlever les cl�s du porte-cl�s secret requ�te de la cl� %08lX de %s
 limite de ressources atteinte rev! la sous-cl� a �t� r�voqu�e: %s
 rev- une r�vocation truqu�e a �t� trouv�e
 rev? probl�me de v�rification de la r�vocation: %s
 revcl� commentaire de r�vocation:  r�voquer une cl� secondaire r�voquer un nom d'utilisateur r�voquer les signatures revsig revuid arrondie � %u bits
 s enregistrer enregistrer et quitter chercher les cl�s avec un serveur de cl�s recherche de "%s" du serveur HKP %s
 la cl� secr�te `%s' n'a pas �t� trouv�e: %s
 la cl� secr�te n'est pas disponible les parties secr�tes ne sont pas disponibles
 s�lectionner la cl� secondaire N s�lectionner le nom d'utilisateur N la fonction de hachage de certification s�lectionn�e est invalide
 l'algorithme de chiffrement s�lectionn� est invalide
 la fonction de hachage s�lectionn�e est invalide
 utiliser le comportement d�fini par OpenPGP utiliser le comportement de PGP 2.x
pour toutes les options de paquets,
de hachage et de chiffrement donner la liste de pr�f�rences mettre.pr�f afficher l'empreinte montrer la photo d'identit� afficher cette aide indiquer o� est une cl� list�e montr.photo montr.pr�f signer signer une cl� signer une cl� localement signer une cl� localement et irr�vocablement signer une cl� irr�vocablement signer ou �diter une cl� signer la cl� signer la cl� localement signer la cl� de fa�on locale et non-r�vocable signer la cl� de fa�on non-r�vocable v�rification de signature supprim�e
 la signature a �chou�: %s
 signature: `%s' a �t� ignor�: %s
 `%s' a �t� ignor�: dupliqu�
 `%s' a �t� ignor�e: c'est une cl� ElGamal g�n�r�e par PGP qui n'est pas
s�re pour les signatures !
 ignor�: cl� publique d�j� activ�e
 ignor�: la cl� publique est d�j� le r�cipient par d�faut
 ignor�: cl� secr�te d�j� pr�sente
 un bloc de type %d a �t� ignor�
 l'auto-signature v3 du nom d'utilisateur "%s" a �t� ignor�e
 d�sol�, impossible de faire cela en mode automatique
 r�vocation autonome - utilisez �gpg --import� pour l'appliquer
 signature autonome de classe 0x%02x
 pas d'action un sous-paquet de type %d poss�de un bit critique
 l'envoi � `%s' s'est d�roul� avec succ�s (r�sultat=%u)
 erreur syst�me pendant l'appel du programme externe: %s
 t enlever les cl�s de ce porte-cl�s le module de chiffrement IDEA n'est pas pr�sent
 l'URL de politique de certification donn�e est invalide
 l'URL de politique de signature donn�e est invalide
 impossible de v�rifier la signature.
Rappelez-vous bien que le fichier de signature (.sig ou .asc)
doit �tre le premier fichier indiqu� sur la ligne de commande.
 la base de confiance est corrompue; ex�cutez �gpg --fix-trustdb�.
 il y a une cl� secr�te pour la cl� publique "%s" !
 Cet algorithme de chiffrement est d�conseill�; utilisez-en un
plus standard !
 cela peut provenir d'une auto-signature manquante
 ce message ne sera pas utilisable par %s
 cette plateforme a besoin de fichiers temporaires pour appeler des
programmes externes
 supprimer l'ident. des paquets chiffr�s conflit de dates changer passer de la liste des cl�s secr�tes aux cl�s priv�es et inversement trop de pr�f�rences `%c'
 trop d'entr�es dans le cache pk - d�sactiv�
 confi. erreur dans la base de confiance l'enregistrement de confiance %lu: n'est pas du type demand� %d
 enregistrement de confiance %lu, type de requ�te %d: la lecture a �chou�: %s
 enregistrement de confiance %lu, type %d: l'�criture a �chou�: %s
 enregistrement de base de confiance %lu: lseek a �chou�: %s
 enregistrement de la base de confiance %lu: l'�criture a �chou� (n=%d): %s
 transaction de base de confiance trop volumineuse
 base de confiance: �lseek()� a �chou�: %s
 base de confiance: la lecture a �chou� (n=%d): %s
 base de confiance: la synchronisation a �chou�: %s
 uid impossible d'ex�cuter %s � %s �: %s
 impossible d'ex�cuter le programme externe
 impossible de lire la r�ponse du programme externe: %s
 impossible de mettre le chemin d'ex�cution � %s
 impossible d'utiliser le chiffre IDEA pour toutes les cl�s vers
lesquelles vous chiffrez.
 mise � jour inattendue de la base de confiance armure inattendue: donn�es inattendues algorithme de chiffrement non implant� algorithme � cl� publique non implant� algorithme de chiffrement inconnu algorithme de compression inconnu r�cipient par d�faut `%s' inconnu
 algorithme de hachage inconnu type de paquet inconnu algorithme de protection inconnu
 algorithme � cl� publique inconnu classe de signature inconnue version inconnue sortie non naturelle du programme externe
 URI non support�e algorithme de cl�s publiques inutilisable cl� publique inutilisable cl� secr�te inutilisable mettre � jour les cl�s depuis un serveur la mise � jour a �chou�: %s
 la mise � jour de la cl� secr�te a �chou�: %s
 mettre la base de confiance � jour pr�f�rences mises � jour pr�f.m�j utilisation: gpg [options]  utiliser comme fichier de sortie utiliser le mode texte canonique utiliser l'option �--delete-secret-keys� pour l'effacer d'abord.
 utiliser la cl� par d�f. comme r�cipient utiliser gpg-agent utiliser ce nom pour signer ou d�chiffrer Le nom d'utilisateur "%s" est d�j� r�voqu�.
 nom d'utilisateur: � utilisation de la cl� secondaire %08lX � la place de la cl�
principale %08lX
 bavard v�rifier une signature cl� faible cl� faible g�n�r�e - nouvel essai
 taille �tonnante pour une cl� de session chiffr�e (%d)
 �criture de la signature directe
 �criture de la signature de liaison
 �criture de la cl� publique vers `%s'
 �criture de la cl� secr�te vers `%s'
 �criture de l'auto-signature
 �criture de `%s'
 �criture vers la sortie standard
 mauvaise cl� secr�te utilis�e oO oui il n'est possible de faire une signature en texte clair avec des cl�s
de style PGP 2.x qu'en mode --pgp2
 il n'est possible g�n�rer une signature d�tach�e avec des cl�s de
style PGP 2.x qu'en mode --pgp2
 le chiffrement RSA ne se fait qu'avec des cl�s de moins de 2048 bits
en mode --pgp2
 il n'est possible de faire une signature d�tach�e ou en texte clair
qu'en mode --pgp2
 vous ne pouvez pas signer et chiffrer en m�me temps en mode --pgp2
 vous ne pouvez pas utiliser une cl� comme son propre r�vocateur
d�sign�
 vous avez trouv� un bug... (%s:%d)
 vous ne pouvez pas utiliser %s en mode %s.
 vous devez utiliser des fichiers (et pas un tube) lorsque --pgp2
est activ�.
 |FD|�crire l'�tat sur ce descripteur |FICH|charger le module d'extension FICH |H�TE|utiliser ce serveur pour chercher des cl�s |IDCL�|donner une confiance ultime � cette cl� |NOM|chiffrer pour NOM |NOM|le terminal utilise la table de caract�res NOM |NOM|utiliser NOM comme r�cipient par d�faut |NOM|utiliser NOM comme cl� secr�te par d�faut |NOM|utiliser l'algorithme de chiffrement NOM |NOM|utiliser le chiffre NOM pour les mots de passe |NOM|utiliser la fonction de hachage NOM |NOM|utiliser le hachage NOM pour les mots de passe |N|niveau de compression N (0 d�sactive) |N|utiliser l'algorithme de compression N |N|coder les mots de passe suivant le mode N |[fichier]|faire une signature en texte clair |[fichier]|faire une signature |[fichier]|�crire les informations d'�tat vers ce fichier |[fich.]|d�chiffrer les fichiers |[fichiers]|chiffrer les fichiers |alg. [fich.]|indiquer les fonctions de hachage 