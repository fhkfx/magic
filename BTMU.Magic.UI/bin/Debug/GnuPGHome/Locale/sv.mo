��    5     �  I  l3      �D  #   �D  )   �D  %   �D  �   E     �E  1   �E  0   �E      F  >   'F  =   fF  �   �F  ;   jG     �G     �G     �G     �G     H     /H     LH  D   iH  .   �H  I   �H  8   'I     `I     }I     �I     �I  "   �I     �I  #   J     7J     SJ     sJ     �J  $   �J  &   �J  ,   �J     "K     ?K     ZK     mK     �K     �K     �K     �K     �K  
   �K     L     L     -L     :L  %   IL     oL  
   �L     �L     �L     �L  +   �L  #   M  -   %M  Z   SM  H   �M      �M     N     0N  (   CN  .   lN  3   �N  "   �N  )   �N     O     7O     RO     eO      wO     �O     �O     �O     �O  )   �O     #P     (P     CP     \P     rP     �P     �P     �P     �P     �P  "   	Q  %   ,Q  &   RQ  !   yQ  %   �Q  "   �Q  #   �Q  '   R      0R     QR     nR     �R     �R     �R     �R  (   �R  $   S     ,S     AS  #   US     yS     �S  #   �S  ,   �S  4   T     =T     TT     iT     �T     �T     �T     �T  '   �T     U     U     =U     OU     bU     uU  "   �U     �U     �U  -   �U  (   V  0   5V  H   fV  	  �V     �W     �W  �   �W  �   yX  A   4Y  /   vY  0   �Y  \   �Y  1   4Z     fZ  C   {Z  )   �Z  0   �Z     [     /[     I[     d[  3   }[  4   �[  -   �[  �   \  .   �\  .   �\  	   ,]  	   6]  !   @]  *   b]  1   �]     �]  #   �]  &   �]  "   #^  &   F^     m^     �^     �^  0   �^  '   �^     
_  0   *_  '   [_  6   �_  @   �_  >   �_  =   :`     x`     �`     �`     �`     �`     �`      �`  /   a  �   ?a     b  H   *b  ,   sb     �b     �b  8   �b  #   c     )c  "   ?c  �   bc  E   �c  �   Ad  9   �d  ;   e     Me     ke     �e  ;   �e     �e     �e  �   f  �   �f     |g     �g     �g     �g     �g     �g     h     h  %   ,h     Rh  S   Zh  �   �h      Ei  3   fi  <   �i  "   �i  ,   �i  %   'j  ,   Mj  %   zj  2   �j     �j      �j  (   k      9k  
   Zk  *   ek     �k     �k     �k     �k     �k  ,   l     0l     Jl  
   el  �   pl  "   �l     "m     ?m     Qm  d   qm     �m     �m  �   n  E   �n  o   �n      \o      }o  7   �o  '   �o  /   �o  E   .p  ,   tp  )   �p  )   �p  �   �p    �q  '   �r     s     s     )s     ?s     Ks  (   [s  %   �s     �s     �s     �s  9   �s     *t     =t     Qt     ot     ~t  /   �t    �t  "   �w     	x     x  *   5x  j   `x  �   �x  9   Xy  O   �y  5   �y  .   z  '   Gz    oz  �   �{     D|     \|     w|  !   �|     �|  '   �|  �   �|  -   �}     ~  *   3~     ^~     t~     �~     �~  7   �~     �~  1   z  =   �  I   �  4   4�     i�  #   ��  =   ��     �  1   
�  %   <�  5   b�  A   ��  E   ځ  H    �     i�  )   ��  C   ��  %   �      �  2   9�     l�  !   m�  h  ��  *   ��  &   #�  1   J�  #   |�  2   ��  >   ӊ  �   �      ċ  =   �  0   #�  M   T�  "   ��  &   Ō  3   �  f    �  %   ��  D  ��     �     �  _   �     s�  
   ��     ��     ��     ��     ��  %   Ր  S   ��  5   O�     ��     ��     ��     ��  (   ˑ  #   ��     �     �  
   (�     3�     :�     Z�  
   i�     t�     ��     ��     ˒     Ӓ     ے     �     �     �     �      �     .�     D�     [�     ]�     s�     ��     ��     ��     ؓ  +   ��  !    �  %   B�  2   h�  '   ��     Ô     ֔     �     ��     �  "   0�     S�     h�  5   }�     ��     ɕ  ;   ܕ  <   �     U�     l�     ��     ��     ��  &   ��     ږ     �  %    �  (   &�  +   O�     {�     ��     ��  1   ̗     ��     �     �     2�     I�     Z�     q�     ��     ��     ��     ��     ��     ��     ��     Ƙ     �      �     �  %   6�     \�     c�     p�     ��      ��  3   ��  $   �  =   �  %   L�      r�     ��     ��     ɚ  '   �     	�     $�  &   D�  &   k�     ��     ��     ��     ��  "   ޛ  %   �  $   '�     L�     ]�     o�     ��     ��     ��     ��     ��     М     �     �     �  E   3�  @   y�     ��     ��     ̝  !   �  +   �     2�     7�     U�     n�  %   ��  %   ��  $   ̞     �     �     �  .   '�  '   V�     ~�     ��     ��  3   П     �     �     9�     Q�     a�     p�  $   ��  &   ��  $   Ϡ  '   ��     �  ,   9�     f�     u�     y�  0   ��  '   ɡ  1   �  %   #�  .   I�  ,   x�  "   ��  0   Ȣ  9   ��  8   3�  "   l�  %   ��  !   ��     ף  >   ��  3   4�  %   h�  (   ��  2   ��     �  $   �     &�  ;   D�  '   ��  #   ��  $   ̥  $   �  .   �  ;   E�     ��  $   ��  7   Ʀ  3   ��     2�     M�  $   j�  5   ��  ,   ŧ  <   �  G   /�  H   w�  B   ��  "   �     &�     <�     L�     l�  0   ��  ;   ��  2   ��     +�     -�     2�  	   H�     R�     m�  !   ��     ��     ª     ݪ     �     ��     �     �  .   -�     \�  (   n�  )   ��  ,   ��     �     �     �     �     <�     ?�     N�     ^�     n�     ��     ��  %   ��  %   �     �     &�     4�     G�     U�      c�  #   ��     ��     ��  &   ��  )   ޭ     �  <   '�     d�     z�  ,   ��     ®     ׮  ;   ޮ  >   �  G   Y�     ��     ��     ï     �     ��     �     .�  "   6�  ,   Y�  *   ��  8   ��  9   �     $�  !   D�  $   f�     ��     ��  5   ��     �     ��     �     ��  I   �     N�     e�     x�     ��     ��  #   ��  #   �     �  !   �     7�  %   T�     z�     ��     ��     ��     ��     Ƴ     ݳ     ߳     �     �     �  #   +�     O�     f�  3   w�  %   ��  %   Ѵ  =   ��  =   5�     s�     ��     ��     ��     ��  %   ��  	   �     ��     ��  
   ��     �  $   �     >�     W�     j�     w�     ��  "   ��     ʶ     ޶     �     ��  V   �      j�  5   ��  $   ��     �  4   �  %   6�  
   \�  *   g�  $   ��     ��  &   ��  .   �  *   �  �   :�  :   ˹  +   �  E   2�  /   x�  %   ��  &   κ     ��     �  ,   �     <�  (   W�     ��     ��  /   ��  ,   ˻  "   ��  )   �     E�     d�      �     ��     ��     ��  I   ݼ      '�     H�     Z�     j�     ��     ��     ��     ܽ     ��     �     )�     B�     Z�     j�     z�     ��     ��      ��     ݾ     �     
�     $�     8�     @�     V�     i�  (   ��     ��  #   ��  
   �  7   �     #�     +�     >�     G�     d�     ��     ��     ��     ��     ��     ��     �     �  E   �  D   Z�  A   ��  7   ��     �  $   6�  F   [�      ��      ��  '   ��      �     -�  "   D�  #   g�  $   ��     ��  /   ��  '    �  7   (�  $   `�     ��     ��  #   ��     ��  !   ��     �     0�  #   G�     k�  -   ��  4   ��  -   ��  �   �     ��  7   ��  @   	�  &   J�  @   q�  I   ��  �   ��  Q   ��     /�  &   E�  '   l�  '   ��  '   ��  )   ��  '   �  ;   6�  2   r�  F   ��  =   ��  '   *�  '   R�  '   z�     ��  (   ��  $   ��  +   �      <�  '   ]�     ��  I   ��  7   ��  7   '�  >   _�     ��     ��     ��  '   ��     �  (   "�  0   K�  -   |�  3   ��     ��      ��     �      �     ,�     9�  !   T�     v�     ��  '   ��     ��  .   ��  #   �  0   ,�  c   ]�  N   ��  %   �  %   6�     \�  5   u�  :   ��  9   ��  1    �  5   R�  #   ��  %   ��     ��  8   ��      �     ?�     N�  6   e�  (   ��  '   ��     ��     ��     �     #�     :�     Z�     r�     ��     ��     ��  &   ��  (   ��  ,   !�  '   N�  *   v�  0   ��  /   ��  /   �  0   2�     c�     ~�  #   ��     ��  '   ��     ��  8   �  B   J�     ��     ��  $   ��  "   ��  "   �  '   %�  8   M�  1   ��     ��     ��  (   ��     �     �     =�     ]�  .   |�     ��     ��     ��     ��     �     #�  /   9�  -   i�     ��  7   ��  6   ��  6   �  G   R�    ��     ��     ��  �   ��  �   ��  B   v�  /   ��  +   ��  i   �  6   �     ��  K   ��  3   �  '   M�     u�     ��  "   ��  #   ��  .   ��  9   �  4   V�  �   ��  -   ^�  .   ��  
   ��     ��  *   ��  5   ��  7   3�  !   k�  /   ��  /   ��  0   ��  -   �     L�     c�     |�  )   ��  (   ��     ��  -   �  '   2�  5   Z�  C   ��  ?   ��  L   �     a�     ~�  +   ��     ��     ��     ��     ��  8   �  �   F�     4�  @   N�  -   ��  /   ��     ��  (    �     )�     H�  *   `�  �   ��  N   �  �   k�  :   !�  A   \�     ��     ��  &   ��  D   ��     C�     S�  �   i�  �   ��     ��  $   ��     �     9�     U�     j�     ~�  (   ��  7   ��  
   ��  b   ��  �   _�  -   �  A   6�  D   x�  )   ��  /   ��  0   �  :   H�  1   ��  2   ��  3   ��  $   �  ,   A�  /   n�  
   ��  @   ��  !   ��  *   �     7�  #   T�  "   x�  3   ��  %   ��     ��  
   �  �   �  '   ��  "   ��     �  .   #�  m   R�     ��     ��  n  ��  1   U�  q   ��     ��     �  /   -�  '   ]�  0   ��  9   ��  z   ��  "   k�  &   ��    ��  )  ��  !   ��     �  0   %�     V�     o�     v�  2   ��  :   ��  3   ��  4   2�     g�  L   ��     ��     ��  -   �     1�  '   E�  @   m�  �  ��  0   R    � +   � 5   � �   � �   � 6   : R   q I   � 5    <   D J  � �   �    �    � 5   � +   	    D	 &   c	 �   �	 0   �
 '   �
 0   �
 #       4 )   Q #   { :   � w   � 1   R D   � a   � 6   + 1   b (   � :   �    � 1    (   G E   p A   � C   � T   <    � ;   � I   � !   7 !   Y 1   {   � (   � �  � 2   � %   � 3   � 3    J   S F   � �   � .   � 5   � @    R   Q %   � 0   � =   � �   9 /   � �  �    �    � m   � "    	   8 	   B    L 	   [    e '   � }   � :   &  '   a     �     �     �  6   �  .   !    D!    K!    Z!    k! #   r!    �! 	   �! *   �! )   �! *   "    /"    <"    I"    ^"    n" "   �"    �"    �"    �"    �"    �"    �"    #    6#    M# )   f# !   �# 3   �# 0   �# D   $ 4   \$ +   �$    �$    �$    �$ "   %    %% .   A%    p%    �% 3   �%    �%    �% 9   & O   M&    �&    �&    �&    �&    �& 1   �&    -' "   M' (   p' C   �' 7   �'    ( "   ,(    O( @   o( (   �(    �(    �(     �(    )    6)    Q)    d)    �)    �)    �)    �)    �)    �)    �)    �)    �)    * $   )*    N*    U* $   h*    �* "   �* 7   �* #   �* F   +    a+ *   }+ %   �+    �+    �+ )   ,    +, +   K, @   w, 3   �,    �,    �, &   - 0   ,- (   ]- 7   �- 4   �-    �-    .    $.    ?.    K.    c.    z.    �. "   �. !   �.    �.    / S    / Q   t/    �/    �/    �/    �/ <   0    L0 &   Q0 -   x0    �0 0   �0 #   �0 +   1    B1    U1    d1 -   �1 '   �1 &   �1     �1     2 @   @2    �2    �2    �2    �2    �2    �2 /   3 0   ?3 .   p3 +   �3    �3 *   �3    4    #4 *   '4 .   R4 2   �4 L   �4 5   5 @   75 >   x5 ,   �5 @   �5 5   %6 T   [6 *   �6 +   �6 %   7 &   -7 x   T7 O   �7 3   8 5   Q8 9   �8 &   �8 5   �8 /   9 D   N9 :   �9 +   �9 ;   �9 :   6: 4   q: S   �: 0   �: 3   +; N   _; J   �; '   �; .   !< ,   P< B   }< B   �< I   = p   M= r   �= G   1> 6   y>    �>    �> )   �> -   ? ?   /? J   o? A   �?    �?    �? )   @    -@ #   ?@     c@    �@ #   �@ "   �@    �@    A    A "   A    BA /   [A    �A 4   �A ?   �A 1    B    RB +   UB    �B #   �B    �B    �B    �B    �B #   �B    C 6   2C 0   iC 6   �C    �C    �C "   �C    D 
   .D !   9D &   [D    �D    �D "   �D 0   �D &   �D ;   E    HE    bE 3   E    �E    �E >   �E 5   F A   JF    �F !   �F #   �F "   �F ,   �F %   'G    MG +   TG .   �G ?   �G I   �G I   9H 8   �H <   �H H   �H *   BI &   mI ]   �I    �I    �I    �I    �I �   J    �J    �J    �J    �J    K 9   K -   XK    �K )   �K )   �K )   �K    L    L    .L    HL    ]L    dL    �L    �L    �L %   �L '   �L -   �L    M    /M -   HM +   vM -   �M b   �M `   3N    �N    �N    �N    �N    �N 3   �N 	   O    "O    +O    0O    BO 9   [O 2   �O     �O    �O    �O 7   P    HP    fP 	   �P    �P    �P h   �P 6   (Q S   _Q (   �Q    �Q E   �Q &   AR    hR E   wR %   �R    �R 8   �R >   S 8   ]S �   �S 5   T O   NT O   �T 1   �T 0    U 7   QU    �U    �U A   �U    �U +   V    9V    ?V 1   VV 0   �V 1   �V <   �V #   (W '   LW 0   tW 0   �W    �W '   �W E   X 1   HX    zX    �X $   �X +   �X    �X     Y &   Y *   CY    nY     }Y    �Y    �Y    �Y '   �Y $   �Y    "Z 3   ;Z    oZ /   �Z    �Z    �Z    �Z    �Z    [    0[ ,   P[    }[ ?   �[    �[ B   �[    (\    1\    G\ '   S\ %   {\ ,   �\     �\    �\    ]    ]    2]    P]    S] U   V] Y   �] G   ^ 4   N^ /   �^ +   �^ =   �^ )   _    G_ 7   d_ ,   �_    �_ 2   �_ 2   ` +   G` '   s` 9   �` )   �` ;   �` ?   ;a #   {a    �a !   �a    �a '   �a     b    :b %   Rb    B           �  n   �   D  �       �  +           x          �  �  2   �        D   �      3    �  �  f  �       �      t      c      �  �  m             a   �       P  \  �  �   Q      �         \   �  �      �      \  �  �  P   �           �  n              �       u  �  7  �  �      �  �                   �    t   �  V      x    q     Q  �      :      C      �  �  I  �   �  �      �  �    �              c       �  )  }  �        �       [  ^  �           �   �   I   �  T              ,  �  ?       �  ,   �    h  >       R  �      �       w       *  &  �  z          �  �  �   5  �   �  "    A  L   F  A  �      �  �  �   4  �  �  y          �   1   e  �            �  �  �             �     	  �   ~   
  J    G  z  �  �  1          �  z              �       �      �   j  �  1  �                   �   �  j   -  l  �  �          <   0   �      �  `  �   �   ,  �     )  �          �  �         #  O  �  _  v  {          K  �      4   *   �         O  �      S          s       "   _       �  �       �   �  �    Q   i      ;   <      �   �   :   �  !  �  9  �  �  �   [         �      �  �   �  �  �  �  �  �       �      
      d  �    �  �  �          �  s  �       �  �  s  �   *  =           |          �   �                 �     h           '  �          �  Y      �        �  Z          I  |  i   �      �  �      �       N                 �      �  E   #  �  �       (   �  u   U                   �       B      �      �   �   �   q              �      r           �  &        X   �  �     o  �  �  ]  �   �  �  �  +  �  G      �  N    �   �  �  �  �   �  B        �  p          -  �  �   [  �  �   �  !              �     �       �  �   f   X  {      �              C  �  �    8          �   k  �          *      K  �  %               ?    �       M  �  �   	       �   �       q     �  �  �  3  
  �      (  o       g    >      3   �   �         ]  ~            �  -  �  �  �          �  �   `   �       �  �   p          �   �  �      S      V  2  	  �       /          }  H      #   "        �   @  �   �   #      �  3  �                T  �  /  @   �  b  �   �   �            �  /    0     �      &  +  =   m  �  k   �      &        k      �  6       �        W   �      6  w    �       �   ]           �     �   �     �  �  �           o          f  2    �   �          �  �  �   8    ~  ^   v           h      r                }   7   /         =  $  E  �              K   w  �  %    �  F   .  5  �      �  �         �      m  l  �  �  �  V   R   �      E  �   '   �  %  �   j  (  �  �         M      �  r       �  �  �   u  �  �     �    �  �   �           :      �   �   %  0  �       �  '                �  �             �  �     J  �  �       G   �   �       .   �  �  �  �        F  �  L  �           Y  a             W  �      �   +  �   �            	  �   �      b   �   1          �       9   Z  �      �      .  l       �  �  x   4      �  �            �  5       �       P  !   Z       �   �   �  �   -   �  �   4  �   �  �  �  �      <  �       �  �  >      �   e  �           )   H   �     �       @  �       �  �  C                         n  $  �               �  �   `  �    �  !      �  �  p   9  �  �      �  �      �   D  �  _  "                �  y   �  �   �           �   t    �     )  �  d   5  �  �  �      i  H  J   $   �  v  �          �   �      g      U  �   �        �      �       6  �         X  �          0      �   ^  y  �      �      W      �  Y   �  �  �     .      ,  �  ;  �  �  d  �  b    �  �   M     $  �     S   �  �      �  �      �  U  ?  '  N                 A   �  O   a    �  �         T   7  L  �  �   �     �  �   ;          �      �  
          |      e       �       c          g   {      2  �   R  8  �      (      �        
I have checked this key casually.
 
I have checked this key very carefully.
 
I have not checked this key at all.
 
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Supported algorithms:
 
The signature will be marked as non-exportable.
 
The signature will be marked as non-revocable.
 
This will be a self-signature.
 
WARNING: the signature will not be marked as non-exportable.
 
WARNING: the signature will not be marked as non-revocable.
 
You need a User-ID to identify your key; the software constructs the user id
from Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

 
You need a passphrase to unlock the secret key for
user: "                 aka "               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
          This could mean that the signature is forgery.
         new signatures: %lu
       secret keys read: %lu
       skipped new keys: %lu
    (%d) DSA (sign only)
    (%d) DSA and ElGamal (default)
    (%d) ElGamal (encrypt only)
    (%d) ElGamal (sign and encrypt)
    (%d) RSA (encrypt only)
    (%d) RSA (sign and encrypt)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
    revoked by %08lX at %s
   Unable to sign.
   secret keys imported: %lu
  %d = Don't know
  %d = I do NOT trust
  %d = I trust fully
  %d = I trust marginally
  %d = I trust ultimately
  (default)  (main key ID %08lX)  (non-exportable)  (sensitive)  [expires: %s]  i = please show me more information
  m = back to the main menu
  q = quit
  s = skip this key
  secret keys unchanged: %lu
  trust: %c/%c "
locally signed with your key %08lX at %s
 "
signed with your key %08lX at %s
 "%s" was already locally signed by key %08lX
 %08lX: It is not sure that this key really belongs to the owner
but it is accepted anyway
 %08lX: There is no indication that this key really belongs to the owner
 %08lX: We do NOT trust this key
 %08lX: key has expired
 %d bad signatures
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %lu keys checked (%lu signatures)
 %lu keys so far checked (%lu signatures)
 %lu keys so far processed
 %s does not expire at all
 %s encrypted data
 %s expires at %s
 %s is not a valid character set
 %s is the new one
 %s is the unchanged one
 %s makes no sense with %s!
 %s not allowed with %s!
 %s%c %4u%c/%08lX  created: %s expires: %s %s.
 %s/%s encrypted for: "%s"
 %s: WARNING: empty file
 %s: can't access: %s
 %s: can't create directory: %s
 %s: can't create lock
 %s: can't create: %s
 %s: can't open: %s
 %s: directory created
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: invalid export options
 %s:%d: invalid import options
 %u-bit %s key, ID %08lX, created %s (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --nrlsign-key user-id --nrsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  About to generate a new %s keypair.
              minimum keysize is  768 bits
              default keysize is 1024 bits
    highest suggested keysize is 2048 bits
 Although these keys are defined in RFC2440 they are not suggested
because they are not supported by all programs and signatures created
with them are quite large and very slow to verify. Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" is you want to sign ALL the user IDs Answer "yes" or "no" Are you really sure that you want to sign this key
with your key: " Are you sure that you want this keysize?  Are you sure you still want to revoke it? (y/N)  BAD signature from " CRC error; %06lx - %06lx
 Can't check signature: %s
 Can't edit this key: %s
 Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for a secondary key.
 Changing expiration time for the primary key.
 Command>  Comment:  DSA keypair will have 1024 bits.
 DSA only allows keysizes from 512 to 1024
 DSA requires the use of a 160 bit hash algorithm
 De-Armor a file or stdin Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring?  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Do you really want to delete the selected keys?  Do you really want to delete this key?  Do you really want to do this?  Do you really want to revoke the selected keys?  Do you really want to revoke this key?  Do you really want to set this key to ultimate trust?  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Don't show Photo IDs Email address:  En-Armor a file or stdin Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Experimental algorithms should not be used!
 Expired signature from " File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from " Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this revoked key anyway, answer "yes". If you want to use this untrusted key anyway, answer "yes". Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid key %08lX made valid by --allow-non-selfsigned-uid
 Invalid selection.
 Is this correct (y/n)?  It is NOT certain that the key belongs to the person named
in the user ID.  If you *really* know what you are doing,
you may answer the next question with yes

 It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keysizes larger than 2048 are not suggested because
computations take REALLY long!
 N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to to quit the key generation. NOTE: %s is not for normal use!
 NOTE: cipher algorithm %d not found in preferences
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: secret key %08lX expired at %s
 NOTE: sender requested "for-your-eyes-only"
 NOTE: signature key %08lX expired %s
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No secondary key with index %d
 No such user ID.
 No trust value assigned to:
%4u%c/%08lX %s " No user ID with index %d
 Not a valid email address
 Notation:  Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a secondary key for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %08lX
 Okay, but keep in mind that your monitor and keyboard radiation is also very vulnerable to attacks!
 Overwrite (y/N)?  Please correct the error first
 Please decide how far you trust this user to correctly
verify other users' keys (by looking at passports,
checking fingerprints from different sources...)?

 Please don't put the email address into the real name or the comment
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the passhrase; this is a secret sentence 
 Please fix this possible security flaw
 Please remove selections from the secret keys.
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one secondary key.
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Policy:  Public key is disabled.
 Quit without saving?  Real name:  Really create?  Really delete this self-signature? (y/N) Really remove all selected user IDs?  Really remove this user ID?  Really sign all user IDs?  Really sign?  Really update the preferences for the selected user IDs?  Repeat passphrase
 Repeat passphrase:  Requested keysize is %u bits
 Save changes?  Secret key is available.
 Secret parts of primary key are not available.
 Select the algorithm to use.

DSA (aka DSS) is the digital signature algorithm which can only be used
for signatures.  This is the suggested algorithm because verification of
DSA signatures are much faster than those of ElGamal.

ElGamal is an algorithm which can be used for signatures and encryption.
OpenPGP distinguishs between two flavors of this algorithms: an encrypt only
and a sign+encrypt; actually it is the same, but some parameters must be
selected in a special way to create a safe key for signatures: this program
does this but other OpenPGP implementations are not required to understand
the signature+encryption flavor.

The first (primary) key must always be a key which is capable of signing;
this is the reason why the encryption only ElGamal key is not available in
this menu. Set command line to view Photo IDs Show Photo IDs Signature is valid for? (0)  Signature made %.*s using %s key ID %08lX
 Syntax: gpg [options] [files]
sign, check, encrypt or decrypt
default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete?  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key probably belongs to the owner
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This would make the key unusable in PGP 2.x.
 Total number processed: %lu
 Usage: gpg [options] [files] (-h for help) Use this key anyway?  User ID "%s" is revoked. User ID is no longer valid WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: invalid size of random_seed file - not used
 WARNING: key %08lX may be revoked: fetching revocation key %08lX
 WARNING: key %08lX may be revoked: revocation key %08lX not present.
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: nothing exported
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: unsafe ownership on %s "%s"
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 What keysize do you want? (1024)  When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You need a Passphrase to protect your secret key.

 You need a passphrase to unlock the secret key for user:
"%.*s"
%u-bit %s key, ID %08lX, created %s%s
 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User id not found] [filename] [revocation] [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 add a photo ID add a revocation key add a secondary key add a user ID add this keyring to the list of keyrings add this secret keyring to the list addkey addphoto addrevoker adduid always use a MDC for encryption armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature batch mode: never ask be somewhat more quiet c can't close `%s': %s
 can't connect to `%s': %s
 can't create %s: %s
 can't create `%s': %s
 can't disable core dumps: %s
 can't do that in batchmode
 can't do that in batchmode without "--yes"
 can't get key from keyserver: %s
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't handle these multiple signatures
 can't open %s: %s
 can't open `%s'
 can't open `%s': %s
 can't open signed data `%s'
 can't open the keyring can't query password in batchmode
 can't read `%s': %s
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the expire date change the ownertrust change the passphrase check check key signatures checking created signature failed: %s
 checking keyring `%s'
 checksum error communication problem with gpg-agent
 completes-needed must be greater than 0
 compress algorithm must be in range %d..%d
 conflicting commands
 could not parse keyserver URI
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 debug decrypt data (default) decryption failed: %s
 decryption okay
 delete a secondary key delete signatures delete user ID delkey delphoto delsig deluid disable disable a key do not force v3 signatures do not force v4 key signatures do not make any changes don't use the terminal at all emulate the mode described in RFC1991 enable enable a key enarmoring failed: %s
 encrypt data encrypted with %s key, ID %08lX
 encrypted with %u-bit %s key, ID %08lX, created %s
 encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating keyring `%s': %s
 error creating passphrase: %s
 error in trailer line
 error reading `%s': %s
 error reading secret keyblock `%s': %s
 error sending to `%s': %s
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 expire export keys export keys to a key server export the ownertrust values failed sending to `%s': status=%u
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 file close error file create error file delete error file exists file open error file read error file rename error file write error fix a corrupted trust database flag user ID as primary force v3 signatures force v4 key signatures forcing compression algorithm %s (%d) violates recipient preferences
 forcing symmetric cipher %s (%d) violates recipient preferences
 fpr general error generate a new key pair generate a revocation certificate gpg-agent is not available in this session
 help import keys from a key server import ownertrust values import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid character in preference string
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-check-level; must be 0, 1, 2, or 3
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02x skipped
 invalid response from agent
 invalid root packet detected in proc_tree()
 invalid value
 key key %08lX: "%s" 1 new user ID
 key %08lX: "%s" revocation certificate imported
 key %08lX: PGP 2.x style key - skipped
 key %08lX: accepted non self-signed user ID '%s'
 key %08lX: already in secret keyring
 key %08lX: can't locate original keyblock: %s
 key %08lX: can't read original keyblock: %s
 key %08lX: doesn't match our copy
 key %08lX: duplicated user ID detected - merged
 key %08lX: invalid revocation certificate: %s - rejected
 key %08lX: invalid revocation certificate: %s - skipped
 key %08lX: invalid subkey binding
 key %08lX: invalid subkey revocation
 key %08lX: key has been revoked!
 key %08lX: new key - skipped
 key %08lX: no public key - can't apply revocation certificate
 key %08lX: no public key for trusted key - skipped
 key %08lX: no subkey for key binding
 key %08lX: no subkey for key revocation
 key %08lX: no subkey for subkey revocation packet
 key %08lX: no user ID
 key %08lX: no user ID for signature
 key %08lX: no valid user IDs
 key %08lX: non exportable signature (class %02x) - skipped
 key %08lX: not a rfc2440 key - skipped
 key %08lX: not protected - skipped
 key %08lX: public key "%s" imported
 key %08lX: public key not found: %s
 key %08lX: removed multiple subkey revocation
 key %08lX: revocation certificate at wrong place - skipped
 key %08lX: secret key imported
 key %08lX: secret key not found: %s
 key %08lX: secret key with invalid cipher %d - skipped
 key %08lX: secret key without public key - skipped
 key %08lX: skipped subkey
 key %08lX: skipped user ID ' key %08lX: subkey has been revoked!
 key %08lX: subkey signature in wrong place - skipped
 key %08lX: unsupported public key algorithm
 key %08lX: unsupported public key algorithm on user id "%s"
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key is not flagged as insecure - can't use it with the faked RNG!
 key marked as ultimately trusted.
 keyring `%s' created
 keyserver error keysize invalid; using %u bits
 keysize rounded up to %u bits
 keysize too large; %d is largest value allowed.
 keysize too small; 1024 is smallest value allowed for RSA.
 keysize too small; 768 is smallest value allowed.
 l list list key and user IDs list keys list keys and fingerprints list keys and signatures list only the sequence of packets list preferences (expert) list preferences (verbose) list secret keys list signatures lsign make a detached signature malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginals-needed must be greater than 1
 max-cert-depth must be in range 1 to 255
 moving a key signature to the correct place
 nN nested clear text signatures
 network error never use a MDC for encryption no no secret key
 no signed data
 no such user id no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not encrypted not human readable not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 nrlsign nrsign okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 passphrase not correctly repeated; try again passphrase too long
 passwd please enter an optional but highly suggested email address please see http://www.gnupg.org/faq.html for more information
 please see http://www.gnupg.org/why-not-idea.html for more information
 pref preference %c%lu duplicated
 preference %c%lu is not valid
 premature eof (in CRC)
 premature eof (in Trailer)
 premature eof (no CRC)
 primary problem handling encrypted packet
 problem with the agent: agent returns 0x%lx
 public and secret key created and signed.
 public key %08lX is %lu second newer than the signature
 public key %08lX is %lu seconds newer than the signature
 public key %08lX not found: %s
 public key decryption failed: %s
 public key encrypted data: good DEK
 public key is %08lX
 public key not found public key of ultimately trusted key %08lX not found
 q qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 read options from file reading from `%s'
 reading options from `%s'
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring resource limit rev! subkey has been revoked: %s
 rev- faked revocation found
 rev? problem checking revocation: %s
 revkey revocation comment:  revoke a secondary key revoke signatures revsig rounded up to %u bits
 s save save and quit search for keys on a key server secret key not available secret key parts are not available
 select secondary key N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 set all packet, cipher and digest options to OpenPGP behavior set all packet, cipher and digest options to PGP 2.x behavior set preference list setpref show fingerprint show photo ID show this help show which keyring a listed key is on showphoto showpref sign sign a key sign a key locally sign a key locally and non-revocably sign a key non-revocably sign or edit a key sign the key sign the key locally sign the key non-revocably signature verification suppressed
 signing failed: %s
 signing: skipped `%s': %s
 skipped `%s': duplicated
 skipped `%s': this is a PGP generated ElGamal key which is not secure for signatures!
 skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 store only subpacket of type %d has critical bit set
 success sending to `%s' (status=%u)
 t the IDEA cipher plugin is not present
 the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 the trustdb is corrupted; please run "gpg --fix-trustdb".
 there is a secret key for public key "%s"!
 this cipher algorithm is deprecated; please use a more standard one!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 throw keyid field of encrypted packets timestamp conflict toggle toggle between secret and public key listing too many `%c' preferences
 too many entries in pk cache - disabled
 trust trust database error trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 uid unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unattended trust database update unexpected armor: unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown cipher algorithm unknown compress algorithm unknown default recipient `%s'
 unknown digest algorithm unknown packet type unknown pubkey algorithm unknown signature class unknown version unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database updated preferences updpref usage: gpg [options]  use as output file use canonical text mode use the default key as default recipient use the gpg-agent use this user-id to sign or decrypt user ID: " using secondary key %08lX instead of primary key %08lX
 verbose verify a signature weak key weak key created - retrying
 writing key binding signature
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY yes you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |FD|write status info to this FD |FILE|load extension module FILE |HOST|use this keyserver to lookup keys |KEYID|ultimately trust this key |NAME|encrypt for NAME |NAME|set terminal charset to NAME |NAME|use NAME as default recipient |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use cipher algorithm NAME for passphrases |NAME|use message digest algorithm NAME |NAME|use message digest algorithm NAME for passphrases |N|set compress level N (0 disables) |N|use compress algorithm N |N|use passphrase mode N |[file]|make a clear text signature |[file]|make a signature |[file]|write status info to file |[files]|decrypt files |[files]|encrypt files |algo [files]|print message digests Project-Id-Version: gnupg 1.2.1
POT-Creation-Date: 2003-08-21 18:22+0200
PO-Revision-Date: 2003-04-30 18:47+0200
Last-Translator: Per Tunedal <info@clipanish.com>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=CP850
Content-Transfer-Encoding: 8bit
 
Jag har gjort viss kontroll av identiteten.
 
Jag har gjort en noggrann kontroll av identiteten.
 
Jag har inte kontrollerat identiteten alls.
 
Otillr�cklig m�ngd slumpdata tillg�ngliga. Jobba med andra saker
en stund f�r att ge operativsystemet en chans att samla mer entropi!
(Beh�ver %d fler byte)
 
St�dda algoritmer:
 
Signaturen kommer att markeras som icke exporterbar.

 
Signaturen kommer att markeras som icke m�jlig att �terkalla.

 
Detta kommer att bli en egensignatur
 
VARNING: Signaturen kommer att markeras som icke exporterbar.

 
VARNING: Signaturen kommer att markeras som icke m�jlig att �terkalla.

 
Du beh�ver en anv�ndaridentitet f�r att identifiera din nyckel; mjukvaran
konstruerar en anv�ndaridentitet fr�n namn, kommentar och epostadress
enligt f�ljande form: 
     "Gustav Vasa (Brutal kung) <gustav@trekronor.se>"

 
Du beh�ver en l�senmening f�r att l�sa upp den hemliga nyckeln f�r
anv�ndaren: "       �ven k�nd som "                       importerade: %lu                       of�r�ndrade: %lu
                  nya undernycklar: %lu
           nya anv�ndaridentiteter: %lu
                    inte importerade: %lu
          utan anv�ndaridentiteter: %lu
          Det �r inte s�kert att signaturen tillh�r �garen.
          Signaturen �r sannolikt en F�RFALSKNING.
          Det finns inget som indikerar att signaturen tillh�r �garen.
          Detta kan betyda att signaturen �r en f�rfalskning.
                    nya signaturer: %lu
       antal l�sta hemliga nycklar: %lu
           �verhoppade nya nycklar: %lu
    (%d) DSA (endast signering)
    (%d) DSA och ElGamal (standardvalet)
    (%d) ElGamal (endast kryptering)
    (%d) ElGamal (signering och kryptering)
    (%d) RSA (endast kryptering)
    (%d) RSA (signering och kryptering)
    (%d) RSA (endast signering)
      (0) Jag vill inte ange hur noga jag har kontrollerat identiteten.%s
      (1) Jag har inte kontrollerat identiteten alls.%s
      (2) Jag har gjort viss kontroll av identiteten.%s
      (3) Jag har gjort en noggrann kontroll av identiteten.%s
   nya sp�rrar av nycklar: %lu
    sp�rrad av %08lX %s
   Kan inte signera.
 antal importerade hemliga nycklar: %lu
  %d = Vet inte
  %d = Jag litar INTE p� denna anv�ndare
  %d = Jag litar fullst�ndigt p� denna anv�ndare
  %d = Jag litar maginellt p� denna anv�ndare
  %d = Jag litar helt och h�llet p� denna anv�ndare
  (standard)  (huvudnyckelns identitet %08lX)   (icke exporterbar)  (k�nsligt) [g�r ut: %s]  i = visa mer information
  m = g� tillbaka till huvudmenyn
  q = avsluta
  s = hoppa �ver denna nyckel
 antal of�r�ndrade hemliga nycklar: %lu
  tillit: %c/%c "
lokalt signerad med din nyckel %08lX vid %s
 "
signerad med din nyckel %08lX %s
 "%s" var redan signerad lokalt av nyckeln %08lX
 %08lX: Det �r inte s�kert att denna nyckel verkligen tillh�r �garen
men den accepteras trots detta
 %08lX: Det finns inget som indikerar att signaturen verkligen tillh�r �garen.
 %08lX: Vi litar INTE p� denna nyckel
 %08lX: nyckeln har blivit f�r gammal
 %d felaktiga signaturer
 %d signaturer verifierades inte eftersom fel uppstod
 %d signaturer verifierades inte eftersom nycklar saknades
 %d anv�ndaridentiteter utan giltig egensignatur hittades
 %lu nycklar och %lu signaturer har kontrollerats
 har behandlat %lu nycklar hittills (% lu signaturer)
 har behandlat %lu nycklar hittills
 Giltighetstiden f�r %s g�r aldrig ut
 %s krypterad data
 Giltighetstiden f�r %s g�r ut vid f�ljande tidpunkt: %s
 %s �r ingen giltig teckentabell
 %s �r den nya
 %s �r den of�r�ndrade
 det �r ingen po�ng att anv�nda %s tillsammans med %s!
 %s �r inte till�ten tillsammans med %s!
 %s%c %4u%c/%08lX  skapad: %s g�r ut: %s %s.
 %s/%s krypterad f�r: "%s"
 %s: VARNING: tom fil
 %s: ingen �tkomst: %s
 %s: kan inte skapa katalog: %s
 %s: kan inte skapa l�s
 %s: kan inte skapa: %s
 %s: kan inte �ppna: %s
 %s: katalog skapad
 %s: katalogen finns inte!
 %s: fel vid l�sning av ledig post: %s
 %s: fel vid l�sning av versionspost: %s
 %s: fel vid uppdatering av versionspost: %s
 %s: fel vid l�sning av katalogpost: %s
 %s: fel vid skrivning av versionspost: %s
 %s: misslyckades med att l�gga till en post: %s
 %s: misslyckades med att skapa hash-tabell: %s
 %s: misslyckades med att skapa versionspost: %s %s: misslyckades med att nollst�lla en post: %s
 %s: ogiltig filversion %d
 %s: felaktig tillitsdatabas
 %s: felaktig tillitsdatabas skapad
 %s: nyckelring skapad
 %s: detta �r inte en tillitsdatabasfil
 %s: %s �verhoppad
 %s: hoppade �ver: offentlig (publik) nyckel finns redan
 %s: hoppade �ver: den offentliga (publika) nyckeln �r deaktiverad
 %s: tillitsdatabas skapad
 %s: ok�nt suffix
 %s: versionspost med postnummer %lu
 %s: felaktiga exportalternativ %d
 %s: felaktiga importalternativ %d
 %u-bits  %s-nyckel, ID %08lX, skapad %s (om du inte anger nyckeln med hj�lp av fingeravtrycket)
 (du kan ha anv�nt fel program f�r denna uppgift)
 --clearsign [filnamn] --decrypt [filnamn] --edit-key anv�ndaridentitet [kommandon] --encrypt [filnamn] --lsign-key anv�ndaridentitet --nrlsign-key anv�ndaridentitet --nrsign-key anv�ndaridentitet --output kan inte anv�ndas f�r detta kommando
 --sign --encrypt [filnamn] --sign --symmetric [filnamn] --sign [filnamn] --sign-key anv�ndaridentitet --store [filnamn] --symmetric [filnamn] -k[v][v][v][c] [anv�ndaridentitet] [nyckelring] ... detta �r ett fel i programmet (%s:%d:%s)
 1 felaktig signatur
 1 signatur verifierades inte eftersom nyckeln saknades
 1 signatur verifierades inte eftersom ett fel uppstod
 1 anv�ndaridentitet utan giltig egensignatur hittades
 @
(Se manualsidan f�r en komplett lista p� alla kommandon och flaggor)
 @
Exempel:

-se -r Bo [fil]             signera och kryptera f�r anv�ndaren Bo
--clearsign [fil]           skapa en klartextsignatur
--detach-sign [fil]         skapa en signatur i en separat fil
--list-keys [namn]          visa nycklar
--fingerprint [namn]        visa fingeravtryck
 @
Flaggor:
  @Kommandon:
  Kommer att generera ett nytt %s nyckelpar.
              den minimala nyckelstorleken �r  768 bitar
              den f�rvalda nyckelstorleken �r 1024 bitar
    den st�rsta f�reslagna nyckelstorleken �r 2048 bitar
 Trots att dessa nycklar �r definierade i RFC2440 �r det inte rekommenderat
att anv�nda dem eftersom de inte st�ds i alla program och signaturer
skapade med dem �r stora och mycket l�ngsamma att verifiera. Svara "ja" (eller bara "j") om du vill generera denna undernyckel. Svara "ja" om du vill ta bort denna undernyckel Svara "ja" om det du vill skriva �ver filen Svara "ja" om du verkligen vill ta bort denna anv�ndaridentitet.
Alla certifikat kommer att g� f�rlorade! Svara "ja" om du vill signera ALLA anv�ndaridentiteter Svara "ja" eller "nej" �r du verkligen s�ker p� att du vill signera denna nyckel
med din nyckel: " �r du s�ker p� att du vill ha denna nyckelstorlek?  Vill du verkligen sp�rra denna nyckel?  FELAKTIG signatur fr�n " CRC-fel; %06lx - %06lx
 Kan inte verifiera signaturen: %s
 Kan inte redigera denna nyckel: %s
 Certifikat som leder till en p�litlig nyckel:
 �ndra (N)amn, (K)ommentar, (E)post eller (O)k/(A)vsluta?  �ndra (N)amn, (K)ommentar, (E)post eller (A)vsluta?  �ndra dina inst�llningar f�r alla anv�ndar-ID:n (eller enbart f�r en av de valda)
till den aktuella listan p� inst�llningar. Tidsst�mplingen f�r alla p�verkade
egensignaturer kommer att flyttas fram en sekund.
 �ndrar giltighetstid f�r en sekund�r nyckel.
 �ndrar giltighetstid f�r den prim�ra nyckeln.
 Kommando>  Kommentar:  DSA-nyckelparet kommer att ha 1024 bitar.
 DSA till�ter bara nyckelstorlekar fr�n 512 till 1024
 DSA kr�ver att du anv�nder en 160-bitars hash-algoritm
 Skala av en fil eller standard in Vill du radera denna korrekta signatur? (j/N/a) Vill du radera denna ogiltiga signatur? (j/N/a) Vill du ta bort denna nyckel fr�n nyckelringen?  Vill du radera denna ok�nda signatur? (j/N/a) Raderade %d signatur.
 Raderade %d signaturer.
 L�skopplad signatur.
 Vill du verkligen ta bort valda nycklar?  Vill du verkligen ta bort denna nyckel?  Vill du verkligen g�ra detta?  Vill du verkligen sp�rra de valda nycklarna?  Vill du verkligen sp�rra denna nyckel?  Vill du verkligen ge denna nyckel slutglitig tillit?  Vill du �ndra den till en fullst�ndigt exporterbar signatur? (j/N)  Will du g�ra om den till en egensignatur av OpenPGP-typ? (j/N)  Vill du att giltighetstiden f�r signaturen ska upph�ra vid samma tid? (J/n)  Visa inte fotoidentifikation Epostadress:  Skapa ett skal f�r en fil eller standard in Ange nytt filnamn Ange l�senmening
 Ange l�senmening:  Ange namnet p� nyckelns �gare Skriv in den nya l�senmening f�r denna hemliga nyckel.

 Ange v�rdet som kr�vs som det visas vid prompten.
Det �r m�jligt att ange ett ISO-datum (����-MM-DD) men du kommer
inte att f� n�got vettigt felmeddelande - ist�llet kommer systemet
att f�rs�ka tolka det angivna v�rdet som ett intervall. Ange storleken p� nyckeln Ange anv�ndaridentiteten till vilken du vill skicka meddelandet. Experimentella algoritmer b�r inte anv�ndas!
 Giltighetstiden har upph�rt f�r signatur fr�n " Filen "%s" finns.  Ange namnet p� den fil signaturen g�ller Skriv ditt meddelande h�r ...
 Korrekt signatur fr�n " Tips: V�lj det anv�ndarid du vill signera
 Hur noga har du kontrollerat att nyckeln du ska signera verkligen tillh�r
personen som n�mns ovan?  Om du inte vet vad du ska svara, svara "0".
 IDEA-kryptering inte installerad. Optimistiskt f�rs�k att anv�nda %s ist�llet
 Om du vill kan du ange en text som beskriver varf�r du utf�rdar
detta sp�rrcertifikat (revocation certificate). F�rs�k att h�lla texten kort och koncis.
En tom rad avslutar texten.
 Om du vill anv�nda denna sp�rrade nyckel �nd�, svara "ja". Om du vill anv�nda denna otillf�rlitliga nyckel �nd�, svara "ja". Ogiltigt tecken i kommentaren
 Ogiltigt tecken i namnet
 Ogiltigt kommando (f�rs�k med "help")
 Ogiltig nyckel %08lX  tvingat giltig med --allow-non-selfsigned-uid
 Felaktigt val.
 St�mmer detta (j/n)?  Det �r INTE s�kert att nyckeln tillh�r den uppgivna personen. Om du
*verkligen* vet vad du g�r, kan du svara ja p� n�sta fr�ga

 Det �r upp till dig att ange ett v�rde h�r. Detta v�rde kommer aldrig att
exporteras till n�gon tredje part. Vi beh�ver det f�r att implementera
"n�tet av tillit". Det har inget att g�ra med det (implicit skapade)
n�tet av certifikat. Skapandet av nycklar avbr�ts.
 Nyckelgenereringen misslyckades: %s
 Nyckeln har tappat sin s�kerhet Nyckeln anv�nds inte l�ngre Nyckeln �r skyddad.
 Nyckeln �r sp�rrad. Nyckeln �r ersatt F�r hur l�ng tid �r nyckeln giltig? (0)  Nyckeln �r of�r�ndrad s� det beh�vs ingen uppdatering.
 Nyckelring Nyckelstorlekar st�rre �n 2048 �r inte att rekommendera
eftersom ber�kningar tar MYCKET l�ng tid!
 N  f�r att �ndra namnet.
C  f�r att �ndra kommentaren.
E  f�r att �ndra epostadressen.
O  f�r att forts�tta med nyckelgenerering.
Q  f�r att avsluta nyckelgenereringen. OBSERVERA: %s �r inte f�r normal anv�ndning!
 OBSERVERA: krypteringsalgoritmen %d finns inte i inst�llningarna
 OSERVERA: att skapa undernycklar till v3-nycklar bryter mot OpenPGP
 OBSERVERA: inst�llningsfilen "%s" saknas
 OBSERVERA: inst�llningsfilen "%s" anv�nds inte
 OBSERVERA: den hemliga nyckeln %08lX gick ut %s
 OBSERVERA: avs�ndaren efterfr�gade "endast-f�r-dina-�gon"
 OBSERVERA: signaturnyckeln %08lX, gick ut vid %s
 OBSERVERA: enkelt S2K-l�ge (0) rekommenderas inte
 OBS: det g�r inte att skriva till tillitsdatabasen
 Namnet f�r inte b�rja med en siffra
 Namnet m�ste vara �tminstone 5 tecken l�ngt
 Den hemliga nyckeln beh�vs f�r att g�ra detta.
 NnKkEeOoAa Det finns ingen motsvarande signatur i den hemliga nyckelringen
 Det finns ingen hj�lp tillg�nglig Det finns ingen hj�lp tillg�nglig f�r "%s" Ingen anledning har angivits Ingen sekund�r nyckel med index %d
 Hittade inte anv�ndaridentiteten.
 Inget tillitsv�rde tilldelat till:
%4u%c/%08lX %s " Ingen anv�ndaridentitet med index %d
 Epostadressen �r ogiltig
 Notation:  Notera att denna nyckel inte kan anv�ndas f�r kryptering. Du kommer kanske
att vilja anv�nda kommandot "--edit-key" f�r att generera en sekund�r
nyckel f�r detta syfte.
 Notera: Denna nyckel har deaktiverats.
 Notera: Denna nyckel har g�tt ut!
 Ingenting raderat.
 Det finns inget att signera med nyckeln %08lX
 Ok, men kom ih�g att din bildsk�rm och ditt tangentbord ocks� s�nder
avsl�jande str�lning som kan avlyssnas!
 Skriv �ver (j/N)?  R�tta f�rst felet
 Var god best�m hur mycket du litar p� denna anv�ndare
n�r det g�ller att korrekt verifiera andra anv�ndares nycklar
(genom att unders�ka pass, unders�ka fingeravtryck fr�n olika
k�llor...)?

 1 = Vet inte
 2 = Jag litar INTE p� denna anv�ndare
 3 = Jag litar marginellt p� denna anv�ndare
 4 = Jag litar fullst�ndigt p� denna anv�ndare
 s = visa mig mer information
 Ange inte epostadressen som namn eller kommentar
 Ange ett nytt filnamn. Om du bara trycker RETUR kommer standarfilnamnet
(som anges i hakparenteser) att anv�ndas. Ange en valfri kommentar Ange namnet p� datafilen:  Ange en l�senmening. Detta �r en hemlig mening
 L�s detta potentiella s�kerhetsproblem
 Tag bort markeringar fr�n de hemliga nycklarna.
 Reptera l�senmeningen, s� du �r s�ker p� vad du skrev in. Rapportera g�rna fel till <gnupg-bugs@gnu.org>.
Rapportera g�rna fel eller synpunkter p� �vers�ttningen till <sv@li.org>.
 V�lj som mest en sekund�r nyckel.
 V�lj vilken typ av nyckel du vill ha:
 Specificera hur l�nge nyckeln skall vara giltig.
         0 = nyckeln blir aldrig ogiltig
      <n>  = nyckeln blir ogiltig efter n dagar
      <n>w = nyckeln blir ogiltig efter n veckor
      <n>m = nyckeln blir ogiltig efter n m�nader
      <n>y = nyckeln blir ogiltig efter n �r
 Specificera hur l�nge nyckeln skall vara giltig.
         0 = signaturen blir aldrig ogiltig
      <n>  = signaturen blir ogiltig efter n dagar
      <n>w = signaturen blir ogiltig efter n veckor
      <n>m = signaturen blir ogiltig efter n m�nader
      <n>y = signaturen blir ogiltig efter n �r
 Anv�nd kommandot "toggle" f�rst.
 Policy:  Den offentliga (publika) nyckeln �r deaktiverad
 Avsluta utan att spara?  Namn:  Vill du verkligen skapa?  Vill du verkligen radera denna egensignatur? (j/N) Vill du verkligen ta bort alla valda anv�ndaridentiteter?  Vill du verkligen ta bort denna anv�ndaridentitet?  Vill du verkligen signera alla anv�ndaridentiteter?  Vill du verkligen signera?  Vill du verkligen �ndra inst�llningarna f�r alla valda anv�ndaridentiteter?  Repetera l�senordsfrasen
 Repetera l�senmeningen:  Den efterfr�gade nyckelstorleken �r %u bitar
 Spara �ndringarna?  Den hemliga nyckeln finns tillg�nglig.
 De hemliga delarna av den prim�ra nyckeln �r inte tillg�ngliga.
 V�lj vilken algoritm du vill anv�nda.

DSA (�ven k�nd som DSS) �r den algoritm f�r digitala signaturer som bara
kan anv�ndas f�r just signaturer. Detta �r den rekommenderade algoritmen
eftersom verifiering av DSA-signaturer �r mycket snabbare �n
ElGamal-signaturer.

ElGamal �r en algoritm som kan anv�ndas f�r signaturer och kryptering.
OpenPGP-standarden skiljer p� tv� varianter av denna algoritm: en som bara
kan anv�ndas f�r kryptering och en som b�de kan signera och kryptera.
Egentligen �r det samma algoritm, men vissa parametrar m�ste v�ljas p�
ett speciellt s�tt f�r att skapa en s�ker nyckel f�r signaturer: detta program
g�r detta men andra OpenPGP-implementationer beh�ver inte f�rst�
signatur+kryptering varianten.

Den f�rsta (prim�ra) nyckeln m�ste alltid vara en nyckel som kan anv�ndas
f�r att skapa signaturer. Detta �r anledningen till att den ElGamal-variant
som bara krypterar inte �r tillg�nglig i denna meny S�tt komandoraden f�r att visa fotoientifikation Visa fotoidentifikation F�r hur l�ng tid �r signaturen giltig? (0)  Signerades %.*s med hj�lp av %s-nyckeln med ID %08lX
 Syntax: gpg [flaggor] [filer]
signera, kontrollera, kryptera eller dekryptera
vilken operation som utf�rs beror p� programmets indata
 Slumptalsgeneratorn �r bara ett lappverk f�r att
f� programmet att fungera - den �r inte p� n�got s�tt en
stark slumptalsgenerator!

ANV�ND INGEN DATA GENERERAD AV DETTA PROGRAM!!

 Egensignaturen p� "%s"
�r en signatur av PGP 2.x-typ.
 Denna signatur �r inte giltig. Det �r rimligt att ta bort den fr�n
din nyckelring. Du kan inte ange n�gra inst�llningar f�r ett anv�ndar-ID av PGP 2.x-typ.
 Detta kommando �r inte till�tet n�r du �r i %s-l�ge.
 Detta �r en hemlig nyckel! - vill du verkligen ta bort den?  Detta �r en signatur som knyter anv�ndaridentiteten till denna nyckel.
Det �r oftast inte en bra id� att ta bort en s�dan signatur. Till
och med kan det bli s� att GnuPG kanske inte kan anv�nda denna nyckel
mer. S� g�r bara detta om denna egensignatur av n�gon anledning �r
ogiltig och det finns en andra signatur som tillg�nglig. Detta �r en giltig signatur p� nyckeln. Normalt sett vill du inte
ta bort denna signatur eftersom den kan vara viktig f�r att skapa
en tillitskoppling till nyckeln eller en annan nyckel som �r
certifierad av denna nyckel. Denna nyckel tillh�r oss
 Denna nyckel har deaktiverats Notera: Giltighetstiden f�r denna nyckel har g�tt ut! Denna nyckels giltighetstid g�r ut vid %s.
 Denna nyckel �r inte skyddad.
 Denna nyckel tillh�r sannolikt �garen
 Denna signatur kan inte verifieras eftersom du inte har den
motsvarande nyckeln. Du b�r v�nta med att ta bort den tills du
vet vilken nyckel som anv�ndes eftersom den nyckeln kanske uppr�ttar
en tillitskoppling genom en annan redan certifierad nyckel. Detta skulle g�ra nyckeln oanv�ndbar i PGP 2.x.
   Totalt antal behandlade enheter: %lu
 Anv�ndning: gpg [flaggor] [filer] (-h f�r hj�lp) Vill du anv�nda nyckeln trots det?  Anv�ndar-ID "%s" �r sp�rrat. Anv�ndaridentiteten �r inte l�ngre giltig VARNING: %s g�ller ist�llet f�r %s
 VARNING: det finns 2 filer med konfidentiell information.
 VARNING: Detta �r en PGP 2-nyckel. Om du anger en sp�rrnyckel kan denna nyckel inte anv�ndas i vissa versioner av PGP.
 VARNING: Denna nyckel har sp�rrats av sin �gare!
 VARNING: Denna nyckel �r inte certifierad med en p�litlig signatur!
 VARNING: Denna nyckel �r inte certifierad med signaturer med ett
tillr�ckligt h�gt tillitsv�rde!
 VARNING: Denna undernyckel har sp�rrats av sin �gare!
 VARNING: Anv�nder en nyckel som inte �r betrodd!
 VARNING: Vi litar INTE p� denna nyckel!
 VARNING: Uppt�ckte en svag nyckel - byt l�senmening igen.
 VARNING: "%s" �r en tom fil
 VARNING: det krypterade meddelandet har �ndrats!
 VARNING: ogiltig notationsdata hittades
 varning: slumpk�rnan har en felaktig storlek och anv�nds d�rf�r inte
 VARNING: nyckeln %08lX kan ha sp�rrats: H�mtar sp�rrnyckel %08lX
 VARNING: nyckeln %08lX kan ha sp�rrats: Sp�rrnyckeln %08lX saknas.
 VARNING: meddelandet krypterades med en svag nyckel i den symmetriska
krypteringen.
 VARNING: exporterade ingenting
 VARNING: programmet kan komma att skapa en minnesutskrift!
 VARNING: Du har valt mottagare (-r) trots att symetrisk kryptering valts
 VARNING: %s os�ker �gare till %s
 Varning: anv�nder os�kert minne!
 VARNING: anv�nder en os�ker slumptalsgenerator!!
 Vi beh�ver generera ett stor m�ngd slumpm�ssig data. Det �r en bra id�
att g�ra n�got annat (skriva p� tangentbordet, r�ra musen, anv�nda
h�rddisken) under primtalsgenereringen; detta ger slumptalsgeneratorn
en st�rre chans att samla ihop en tillr�cklig m�ngd entropi.
 Vilken nyckelstorlek vill du ha? (1024)  N�r du signerar ett anv�ndar-ID p� en nyckel, m�ste du f�rst kontrollera att nyckeln
verkligen tillh�r den person som n�mns i anv�ndar-ID:t.  Det �r viktigt f�r andra att
f� veta hur noga du har kontrollerat detta.

"0" betyder att du inte p�st�r n�gonting om hur noga du kontrolleratnyckeln.

"1" betyder att du tror att nyckeln tillh�r den person som p�st�r sig g�ra det
    men du kunde inte, eller ville inte kontrollera nyckeln alls.  Detta �r anv�ndbartf�r
    en "persona" kontroll, d�r du signerar nyckeln f�r en anv�ndare med pseudonym.

"2" betyder att du gjorde viss kontroll av nyckeln. Det kan t.ex. betyda att
    du kontrollerade fingeravtrycket och kontrollerade anv�ndar-ID:t f�r nyckeln
    mot en fotolegitimation.

"3" betyder att du gjorde en noggrann och utt�mmande kontroll av nyckeln.  Detta kan t.ex.
    betyda att du kontrollerade nyckelns fingeravtryck direkt med nyckelinnehavaren
    och att du kontrollerade, med hj�lp av sv�rf�rfalskade identitetsdokument
a
    med foto (tex ett k�rkort) att namnet p� innehavaren st�mmer med
    namnet i anv�ndar-ID:t p� nyckeln, och slutligen att du kontrollerade att(genom att utv�xla
    e-postmeddelanden) att e-postadressen p� nyckeln tillh�r nyckelinnehavaren.

Obs! Ovanst�ende exempel f�r niv�erna 2 och 3 �r bara f�rslag.
Slutligen �r det bara du sj�lv som avg�r vad "viss" and "noggrann"
betyder n�r du signerar andras nycklar.

Om du inte vet vad du ska svara, s� svara "0". Du st�r i begrepp att �terkalla dessa signaturer:
 Du anv�nder teckenupps�ttningen "%s"
 Du kan inte �ndra giltighetsdatum f�r en v3-nyckel
 Du kan inte ta bort den sista anv�ndaridentiteten!
 Du specificerade ingen anv�ndaridentitet. (du kan anv�nda "-r") f�r detta
 Du vill inte ha n�gon l�senmening - detta �r sannolikt en d�lig id�!

 Du vill inte ha n�gon l�senmening - det �r sannolikt en *d�lig* id�!
Jag kommer att g�ra det �nd�. Du kan �ndra din l�senmening n�r som helst
om du anv�nder detta program med flaggan "--edit-key".
 Du har signerat f�ljande anv�ndaridentiteter:
 Du f�r inte ange en sp�rrnyckel f�r en PGP 2-nyckel.
 Du kan inte l�gga till ett bild-ID till en nyckel av PGP 2-typ.
 Du kan inte g�ra en OpenPGP-signatur p� en PGP 2.x-nyckel n�r du �r i --pgp2-l�ge
 Du m�ste v�lja �tminstone en nyckel.
 Du m�ste v�lja �tminstone en anv�ndaridentitet.
 Du beh�ver en l�senmening f�r att skydda din hemliga nyckel

 Du beh�ver en l�senmening f�r att l�sa upp den hemliga nyckeln f�r
anv�ndaren: "%.*s"
%u-bitars %s-nyckel, ID %08lX, skapad %s%s
 Du valde f�ljande anv�ndaridentitet:
    "%s"

 Du borde ange en anledning till certifikationen. Beroende p� sammanhanget
har du m�jligheten att v�lja fr�n f�ljande lista:
  "Nyckeln har tappat sin s�kerhet"
     Anv�nd denna om du har anledning att tro att icke auktoriserade personer
     har f�tt tillg�ng till din hemliga nyckel.
  "Nyckeln har ersatts"
     Anv�nd denna om du har ersatt denna nyckel med en nyare.
  "Nyckeln anv�nds inte l�ngre"
     Anv�nd denna om du har pensionerat denna nyckel.
  "Anv�ndaridentiteten �r inte l�ngre giltig"
     Anv�nd denna f�r att visa att denna anv�ndaridentitet inte l�ngre
     skall anv�ndas. Detta anv�nds normalt f�r att visa att en epostadress
     �r ogiltig.
 Vad v�ljer du?  Vad v�ljer du?  Ditt system kan inte visa datum senare �n �r 2038.
Datum fram till �r 2106 kommer dock att hanteras korrekt.
 [Hittade inte anv�ndaridentiteten] [filnamn] [sp�rrad] [egensignatur] [os�kert] `%s' �r redan komprimerad
 "%s" �r inte �n vanlig fil - ignorerad
 ett notationsnamn kan bara inneh�lla bokst�ver, siffror, punkter eller
understrykningstecken och sluta med ett likhetstecken
 ett notationsv�rde f�r inte ineh�lla n�gra kontrolltecken
 l�gg till en anv�ndaridentitet med foto l�gg till en sp�rrnyckel l�gg till en sekund�r nyckel l�gg till en anv�ndaridentitet l�gg till denna nyckelring till listan av nyckelringar l�gg till denna hemliga nyckelring till listan addkey l�gg till bild Ange sp�rrnyckel adduid anv�nd alltid en MDC f�r kryptering rad i skalet:  skal: %s
 anta att svaret �r nej p� de flesta fr�gor anta att svaret �r ja p� de flesta fr�gor antar att signera data finns i filen "%s"
 felaktig MPI felaktig URI felaktigt certifikat felaktig nyckel felaktig l�senmening felaktig offentlig (publik) nyckel felaktig hemlig nyckel felaktig signatur batch-l�ge: fr�ga aldrig var n�got tystare c kan inte st�nga "%s": %s
 kan inte ansluta till "%s": %s
 kan inte skapa %s: %s
 kan inte skapa "%s": %s
 kan inte deaktivera minnesutskrifter: %s
 kan inte g�ra detta i batch-l�ge
 kan inte g�ra s� i batch-l�ge utan flaggan "--yes"
 kan inte h�mta nyckeln fr�n en nyckelserver: %s
 kan inte hantera algoritm %d f�r offentlig (publik) nyckelhantering
 kan inte hantera text med rader l�ngre �n %d tecken
 kan inte hantera dessa multipla signaturer
 kan inte �ppna %s: %s
 kan inte �ppna "%s"
 kan inte �ppna "%s": %s
 kan inte �ppna signerad data "%s"
 kan inte �ppna nyckelringen kan inte fr�ga efter l�senmening i batch-l�ge
 kan inte l�sa "%s": %s
 kan inte ta status p� "%s": %s
 kan inte anv�nda symetriska ESK-paket pga S2K-l�ge
 kan inte skriva till "%s": %s
 avbruten av anv�ndaren
 Det g�r inte att anv�nda en PGP 2-nyckel som sp�rrnyckel
 kan inte undvika en svag nyckel f�r symmetrisk kryptering; f�rs�kte
%d g�nger!
 �ndra utg�ngsdatum �ndra �gartillitsv�rdet �ndra l�senmening check verifiera nyckelsignaturer f�rs�k att verifiera signaturen misslyckades: %s
 kontrollerar nyckelringen `%s'
 fel vid ber�kning av kontrollsumma problem vid kommunikation med GPG-Agent
 variabeln "completes-needed" m�ste ha ett v�rde som �r st�rre �n 0
 kompressionsalgoritmen m�ste vara i intervallet %d..%d
 motstridiga kommandon
 kunde inte tolka nyckelserver-URI
 skapa utdata med ett ascii-skal data sparades inte, anv�nd flaggan "--output" f�r att spara den
 misslyckades med att ta bort skalet: %s
 debug dekryptera data (normall�ge) dekrypteringen misslyckades: %s
 dekrypteringen lyckades
 ta bort en sekund�r nyckel ta bort signaturer ta bort en anv�ndaridentitet delkey ta bort bild delsig deluid disable deaktivera en nyckel anv�nd inte v3-signaturer anv�nd inte v4-nyckelsignaturer g�r inga �ndringar anv�nd inte terminalen alls imitera l�get som beskrivs i RFC1991 enable aktivera en nyckel misslyckades med att skapa skal: %s
 kryptera data krypterad med %s-nyckel, ID %08lX
 krypterad med %u-bitars %s-nyckel, ID %08lX, skapad %s
 krypterad med en ok�nd algoritm %d
 f�r att kryptera meddelanden med --pgp2 kr�vs IDEA-insticksprogrammet
 endast symetrisk kryptering fel vid skapande av nyckelringen "%s": %s
 fel vid skapandet av l�senmening: %s
 fel i avslutande rad
 fel vid l�sning av "%s": %s
 fel vid l�sning av hemlig nyckel"%s": %s
 fel vid s�ndning till "%s": %s
 fel vid skrivning av nyckelringen "%s": %s
 fel vid skrivning av offentliga (publika) nyckelringen "%s": %s
 fel vid skrivning av hemliga nyckelringen "%s": %s
 expire exportera nycklar exportera nycklar till en nyckelserver exportera de v�rden som representerar �gartillit misslyckades s�nda till "%s": status=%u
 misslyckades med att initialisera tillitsdatabasen: %s
 misslyckades med att �terskapa nyckelringscache: %s
 fel vid st�ngning av fil fel vid skapande av fil fel vid borttagande av fil filen finns fel vid �ppnande av fil fel vid l�sning av fil fel vid namnbyte av fil fel vid skrivning av fil reparera en korrupt tillitsdatabas markera anv�ndar-ID:t som prim�rt anv�nd v3-signaturer anv�nd v4-nyckelsignaturer att genomdriva komprimeringsalgoritm %s (%d) strider mot mottagarens inst�llningar
 att kr�va symetrisk kryptering med %s (%d) strider mot mottagarnas inst�llningar
 fpr allm�nt fel generera ett nytt nyckelpar generera ett sp�rrcertifikat kunde inte f� tillg�ng till n�gon gpg-agent i denna session
 help importera nycklar fr�n en nyckelserver importera v�rden som representerar �gartillit importera/sl� ihop nycklar raden %u �r f�r l�ng, eller saknar nyradstecken
 indataraden �r l�ngre �n %d tecken
 ogiltigt S2K-l�ge; m�ste vara 0, 1 eller 3
 felaktigt argument felaktigt skal felaktig rubrikrad i skalet:  felaktigt skal: raden �r l�ngre �n %d tecken
 Ogiltigt tecken i inst�llningsstr�ngen
 felaktig rubrikrad i klartextsignatur
 felaktig bindestreck-kodad rad:  ogiltiga standardinst�llningar
 ogiltig standardv�rde f�r test-niv�; m�ste vara 0, 1, 2 eller 3
 felaktiga exportalternativ
 felaktig hash-algoritm "%s"
 felaktiga importalternativ
 felaktig nyckelring felaktigt paket felaktig l�senmening felaktiga inst�llningar av krypteringsalgoritm
 felaktiga inst�llningar av kompressionsalgoritm
 felaktiga inst�llningar av checksummealgoritm
 ogiltigt radix64-tecken %02x hoppades �ver
 felaktigt svar fr�n agenten
 felaktigt rotpaket hittades i proc_tree()
 ogiltigt v�rde
 key nyckel %08lX: "%s" 1 ny anv�ndaridentitet
 nyckel %08lX: "%s" sp�rrcertifikat importerat
 nyckeln %08lX: nyckel av PGP 2.x-typ - �verhoppad
 nyckel %08lX: accepterade anv�ndaridentitet ID '%s' som saknar egensignatur
 nyckel %08lX: finns redan i den hemliga nyckelringen
 nyckel %08lX: kan inte hitta det ursprungliga nyckelblocket: %s
 nyckel %08lX: kan inte l�sa det ursprungliga nyckelblocket %s
 nyckel %08lX: matchar inte v�r lokala kopia
 nyckel %08lX: anv�ndaridentitet hittades tv� g�nger - slog ihop
 nyckel %08lX: ogiltigt sp�rrcertifikat: %s - avvisat
 nyckel %08lX: felaktigt sp�rrcertifikat (revocation certificate): %s - hoppade �ver
 nyckel %08lX: ogiltig undernyckelbindning
 nyckel %08lX: ogiltig sp�rr av undernyckel
 nyckeln %08lX: nyckeln har sp�rrats!
 nyckeln %08lX: ny nyckel - �verhoppad
 nyckel %08lX: offentlig (publik) nyckel saknas - kan inte sp�rra nyckeln med sp�rrcertifikatet (revocation certificate)
 nyckel %08lX: hittade ingen motsvarande offentlig (publik) nyckel - �verhoppad
 nyckel %08lX: ingen undernyckel f�r nyckelbindning
 nyckel %08lX: ingen undernyckel f�r sp�rr av nyckeln
 nyckel %08lX: ingen undernyckel f�r sp�rr av undernyckel
 nyckel %08lX: ingen anv�ndaridentitet
 nyckel %08lX: ingen anv�ndaridentitet f�r signaturen
 nyckel %08lX: inga giltiga anv�ndaridentiteter
 nyckel %08lX: icke exporterbar signatur (klass %02x) - hoppade �ver
 nyckeln %08lX f�ljer inte standarden RFC2440 - �verhoppad
 nyckeln %08lX �r inte skyddad - �verhoppad
 nyckel %08lX: importerade offentlig (publik) nyckel ("%s")
 nyckel %08lX: hittade ingen offentlig (publik) nyckel: %s
 nyckel %08lX: tog bort ogiltig sp�rr av undernyckel
 nyckel %08lX: sp�rrcertifikat (revocation certificate) p� fel plats - hoppade �ver
 nyckel %08lX: den hemliga nyckeln �r importerad
 nyckel %08lX: hittade inte den hemliga nyckeln: %s
 nyckel %08lX: hemlig nyckel med ogiltig krypteringsalgoritm %d - hoppade �ver
 nyckel %08lX: hemlig nyckel utan offentlig (publik) nyckel - hoppade �ver
 nyckel %08lX: hoppade �ver undernyckel
 nyckel %08lX: hoppade �ver anv�ndaridentitet ' nyckeln %08lX: en undernyckel har sp�rrats!
 nyckel %08lX: signatur p� undernyckel p� fel plats - hoppade �ver
 nyckel %08lX: algoritmen f�r offentlig (publik) nyckel st�ds inte
 nyckel %08lX: algoritmen f�r offentlig (publik) nyckel st�ds inte ("%s")
 nyckeln �r skapad %lu sekund in i framtiden (problemet �r
relaterat till tidsresande eller en felst�lld klocka)
 nyckeln �r skapad %lu sekunder in i framtiden (problemet �r
relaterat till tidsresande eller en felst�lld klocka)
 nyckeln �r inte markerad os�ker - g�r inte att anv�nda med fejkad RNG!
 Nyckeln har ultimat f�rtroende (som din egen nyckel).
 %s: nyckelring skapad
 nyckelserverfel ogiltig nyckelstorlek; anv�nder %u bitar
 nyckelstorleken avrundad upp�t till %u bitar
 nyckelstorleken �r f�r stor; %d �r det st�rsta till�tna v�rdet
 nyckelstorleken �r f�r liten; 1024 �r det minsta till�tna v�rdet f�r RSA.
 nyckelstorleken �r f�r liten; 768 �r det minsta till�tna v�rdet.
 l list r�kna upp nycklar och anv�ndaridentiteter r�kna upp nycklar r�kna upp nycklar och fingeravtryck r�kna upp nycklar och signaturer skriv endast ut paketsekvensen skriv ut inst�llningar (expertl�ge) skriv ut inst�llningar (utf�rligt) r�kna upp hemliga nycklar r�kna upp signaturer lsign skapa en signatur i en separat fil felformaterad CRC-summa
 milj�variabeln GPG_AGENT_INFO �r felformaterad
 felformaterad anv�ndaridentitet variabeln "marginals-needed" m�ste vara st�rre �n 1
 variabeln "max-cert-depth" m�ste ha ett v�rde mellan 1 och 255
 flyttar en nyckelsignatur till den r�tta platsen
 nN flera klartextsignaturer g�r in i varandra
 n�tverksfel anv�nd aldrig en MDC f�r kryptering nej ingen hemlig nyckel
 ingen signerad data
 ok�nd anv�ndaridentitet hittade ingen giltig OpenPGP-data.
 inga giltiga adresser
 hittade ingen nyckelring som gick att skriva till: %s
 hittade ingen offentlig (publik) nyckelring: %s
 hittade ingen nyckelring som gick att skriva till: %s
 ingen frikopplad signatur
 inte krypterad inte m�jlig att l�sa f�r m�nniskor inte behandlade inte st�dd notera: filen random_seed �r tom
 notera: random_seed uppdaterades inte
 nrlsign nrsign ok, vi �r den hemliga mottagaren.
 gammal kodning av krypteringsnyckeln st�ds inte
 signatur av den gamla (PGP 2.x) typen
 operationen �r inte m�jlig utan tillg�ng till s�kert minne
 inst�llningsfil "%s": %s
 ursprungligt filnamn="%.*s"
 l�senmeningen upprepades inte korrekt; f�rs�k igen. l�senmeningen �r f�r l�ng
 passwd ange en epostadress. Detta �r valfritt men rekommenderas varmt se http://www.gnupg.org/faq.html f�r mer information
 Mer information finns p�: http://www.gnupg.org/why-not-idea.html
 pref inst�llningen %c%lu �r dubblerad
 inst�llningen %c%lu �r inte giltig
 f�r tidigt filslut (i CRC-summan)
 f�r tidigt filslut (i den avslutande raden)
 f�r tidigt filslut (ingen CRC-summa)
 prim�r problem vid hanteringen av krypterat paket
 problem med agenten: agenten returnerar 0x%lx
 offentlig (publik) och hemlig nyckel �r skapade och signerade.
 den offentliga (publika) nyckeln %08lX �r %lu sekund nyare �n signaturen
 den offentlig (publik) nyckeln %08lX �r %lu sekunder nyare �n signaturen
 hittade inte den offentliga (publika) nyckeln %08lX :%s
 dekryptering med offentlig (publik) nyckel misslyckades: %s
 data krypterad med offentlig (publik) nyckel: korrekt krypteringsnyckel
 den offentliga (publika) nyckeln �r %08lX
 hittade inte offentlig (publik) nyckel Hittar inte den offentliga (publika) nyckeln tillh�rande den ultimat betrodda nyckeln %08lX 
 q aA avsluta avsluta denna meny tecken kodade enligt "quoted printable"-standarden funna i skalet - detta
beror sannolikt p� att en felaktig epostserver eller epostklient har anv�nts
 l�s flaggor fr�n fil l�ser fr�n "%s"
 l�ser flaggor fr�n "%s"
 l�ser fr�n standard in ...
 Anledning till sp�rren:  ta bort nycklar fr�n den offentliga (publika)nyckelringen ta bort nycklar fr�n den hemliga nyckelringen resursbegr�nsning revoked! en undernyckel har sp�rrats: %s
 rev- hittade f�rfalskad sp�rr av nyckeln
 rev? problem vid kontroll av sp�rren: %s
 revkey Sp�rrkommentar:  sp�rra en sekund�r nyckel �terkalla signaturer revsig avrundade upp�t till %u bitar
 s save spara och avsluta s�k efter nycklar hos en nyckelserver den hemliga nyckeln �r inte tillg�nglig de hemliga nyckeldelarna �r inte tillg�nliga
 v�lj sekund�r nyckel N v�lj anv�ndaridentitet N den valda kontrollsummealgoritmen �r ogiltig
 den valda krypteringsalgoritmen �r ogiltig
 den valda kontrollsummealgoritmen �r ogiltig
 �ndra inst�llningarna f�r paket, kryptering och kontrollsumma s� att gpg f�ljer OpenPGP-standarden �ndra inst�llningarna f�r paket, kryptering och kontrollsumma s� att gpg h�rmar PGP 2.x-beteende ange inst�llningslista setpref visa fingeravtryck visa bild-ID visa denna hj�lp visa vilken nyckelring den listade nyckeln h�r till visa bild showpref sign signera en nyckel signera en nyckel lokalt signera en nyckel lokalt utan m�jlighet till �terkallelse signera en nyckel utan m�jlighet till �terkallelse signera eller redigera en nyckel signera nyckeln signera nyckeln lokalt signera nyckeln utan m�jlighet att �terkalla signaturen signaturen verifierades inte
 signeringen misslyckades: %s
 signerar: hoppade �ver "%s": %s
 hoppade �ver "%s": kopia
 hoppade �ver "%s": detta �r en nyckel av ElGamal-typ genererad av PGP
som inte �r s�ker f�r signaturer!
 hoppade �ver: offentlig (publik) nyckel redan angiven
 hoppade �ver: den offentliga (publika) nyckeln �r redan satt som f�rvald mottagare
 hoppade �ver: hemlig nyckel finns redan
 hoppar �ver block av typen %d
 frist�ende sp�rrcertifikat - anv�nd "gpg --import" f�r
att applicera
 frist�ende signatur av klassen 0x%02x
 endast lagring underpaket av typen %d har den bit satt som markerar den som kritisk
 lyckades s�nda till "%s" (status=%u)
 t Insticksprogram f�r IDEA-kryptering �r inte installerat
 den angivna URL som beskriver certifikationspolicy �r ogiltig
 den angivna URL som beskriver signaturpolicy �r ogiltig
 signaturen kunde inte verifieras.
Kom ih�g att signaturfilen (.sig eller .asc)
ska vara den f�rst angivna filen p� kommandoraden
 tillitsdatabasen �r trasig, k�r "gpg --fix-trustdb".
 det finns en hemlig nyckel tillh�rande denna offentliga (publika) nyckel!"%s"!
 denna krypteringsalgoritm �r f�rlegad, anv�nd ist�llet en mer normal algoritm!
 detta kan bero p� att det saknas en egensignatur
 detta meddelande kanske inte kan anv�ndas av %s
 sl�ng bort nyckelidentitetsf�ltet fr�n krypterade paket konflikt mellan tidsst�mplar toggle hoppa mellan utskrift av hemliga och offentliga (publika) nycklar f�r m�nga `%c' inst�llningar
 f�r m�nga poster i pk-cachen - inaktiverad
 trust fel i tillitsdatabasen tillitspost %lu, posttyp %d: kunde inte l�sa: %s
 tillitspost: %lu, typ %d: kunde inte skriva: %s
 tillitsdatabasposten %lu: lseek misslyckades: %s
 tillitsdatabasposten %lu: skrivning misslyckades (n=%d): %s
 tillitsdatabastransaktion f�r stor
 tillitsdatabas: lseek misslyckades: %s
 tillitsdatabas: l�sning misslyckades (n=%d): %s
 tillitsdatabas: synkronisering misslyckades: %s
 uid kunde inte s�tta exec-s�kv�gen till %s
 kan inte anv�nda IDEA-kryptering f�r alla nycklar du krypterar till.
 uppdaterar tillitsdatabasen utan m�nsklig tillsyn ov�ntat skal: ov�ntad data krypteringsalgorimten �r inte inf�rd algoritmen f�r publik nyckel �r inte inf�rd ok�nd krypteringsalgoritm ok�nd komprimeringsalgoritm den f�rvalda mottagaren "%s" �r ok�nd
 ok�nd algoritm f�r ber�kning av sammandrag ok�nd pakettyp ok�nd algoritm f�r publik nyckel ok�nd signaturklass ok�nd version denna URI st�ds inte oanv�ndbar algoritm f�r publika nycklar oanv�ndbar offentlig (publik) nyckel oanv�ndbar hemlig nyckel uppdatera alla nycklar nycklar fr�n en nyckelserver uppdateringen misslyckades: %s
 misslyckades med att uppdatera hemligheten: %s
 uppdatera tillitsdatabasen uppdaterat inst�llningar updpref anv�ndning: gpg [flaggor]  anv�nd som fil f�r utdata anv�nd "ursprunglig text"-l�get anv�nd standardnyckeln som standardmottagare anv�nd gpg-agenten anv�nd denna anv�ndaridentitet f�r att signera eller dekryptera anv�ndaridentitet: " anv�nder sekund�ra nyckeln %08lX ist�llet f�r prim�rnyckeln %08lX
 utf�rlig verifiera en signatur svag nyckel skapade en svag nyckel - f�rs�ker igen
 skriver signatur knuten till nyckeln
 skriver offentlig (publik) nyckel till "%s"
 skriver hemlig nyckel till "%s"
 skriver egensignatur
 skriver till "%s"
 skriver till standard ut
 fel hemlig nyckel har anv�nts jJ ja du kan endast krypterar till RSA nycklar som �r h�gst 2048 bitar l�nga i --pgp2-l�ge
 du kan bara skapa fr�nkopplade signaturereller klartextsignaturer
tillsammans med --pgp2
 du kan inte b�de signera och kryptera samtidigt tillsammans med --pgp2
 Du kan inte ange en nyckel som sin egen sp�rrnyckel
 du har hittat ett fel i programmet ... (%s:%d)
 du kan inte anv�nda %s n�r du �r i %s l�ge
 du m�ste anv�nda filer (och inte r�r) tillsammans med --pgp2
 |FD|skriv statusinformation till denna FD |FIL|ladda till�ggsmodul FIL |V�RD|anv�nd denna nyckelserver f�r att sl� upp nycklar |NYCKELID|lita ovillkorligen p� denna nyckel |NAMN|kryptera f�r NAMN |NAMN|s�tt teckentabellen f�r terminalen till NAMN |NAMN|anv�nd NAMN som standardv�rdet f�r mottagare |NAMN|anv�nd NAMN som f�rvald hemlig nyckel |NAMN|anv�nd krypteringsalgoritmen NAMN |NAMN|anv�nd krypteringsalgoritmen NAMN f�r l�senmeningar |NAMN|anv�nd kontrollsummealgoritmen NAMN |NAMN|anv�nd kontrollsummealgoritmen NAMN f�r l�senmeningar |N|s�tt kompressionsniv�n till N (0 f�r att sl� av kompression) |N|anv�nd komprimeringsalgoritmen N |N|anv�nd l�senmeningsl�get N |[fil]|skapa en klartext-signatur |[fil]|skapa en signatur |[fil]|skriv statusinformation till fil |[files]|dekryptera filer |[filer]|kryptera filer |algo [filer]|skriv ut kontrollsummor 