<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucGCMSMTTransList
    Inherits BTMU.MAGIC.UI.ucGCMSMTBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TransListGroupBox = New System.Windows.Forms.GroupBox
        Me.TransListDatagridView = New System.Windows.Forms.DataGridView
        Me.colActionChecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CurrencyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ValueDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RemitAmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CustomerReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SettlementAccNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SettlementAccName1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryAccNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryName1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChargesAccName1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryMsg3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChargesAccName2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBranchDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankAddress2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SectorSelectionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankAddress1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryMsg1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryMsg2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SettlementAccName2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SettlementAccName4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChargesAccDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContractNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankNCCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ModifiedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SettlementAccName3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ExchangeMethodDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BankChargesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryName3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBranchDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OriginalModifiedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryName4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryMsg4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryName2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OriginalModifiedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CreatedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TemplateIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ModifiedByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CreatedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankNCCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChargesAccName3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChargesAccName4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GCMSMTTransactionDetailCollectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TransListButtonGroupBox = New System.Windows.Forms.GroupBox
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnUpload = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnModify = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.TransListPanel = New System.Windows.Forms.Panel
        Me.TransListGroupBox.SuspendLayout()
        CType(Me.TransListDatagridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GCMSMTTransactionDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TransListButtonGroupBox.SuspendLayout()
        Me.TransListPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'TransListGroupBox
        '
        Me.TransListGroupBox.Controls.Add(Me.TransListDatagridView)
        Me.TransListGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TransListGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TransListGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.TransListGroupBox.Name = "TransListGroupBox"
        Me.TransListGroupBox.Size = New System.Drawing.Size(964, 503)
        Me.TransListGroupBox.TabIndex = 0
        Me.TransListGroupBox.TabStop = False
        Me.TransListGroupBox.Text = "List of Transactions : "
        '
        'TransListDatagridView
        '
        Me.TransListDatagridView.AllowUserToAddRows = False
        Me.TransListDatagridView.AllowUserToDeleteRows = False
        Me.TransListDatagridView.AllowUserToResizeRows = False
        Me.TransListDatagridView.AutoGenerateColumns = False
        Me.TransListDatagridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.TransListDatagridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colActionChecked, Me.DataGridViewTextBoxColumn1, Me.CurrencyDataGridViewTextBoxColumn, Me.ValueDateDataGridViewTextBoxColumn, Me.RemitAmountDataGridViewTextBoxColumn, Me.CustomerReferenceDataGridViewTextBoxColumn, Me.SettlementAccNoDataGridViewTextBoxColumn, Me.SettlementAccName1DataGridViewTextBoxColumn, Me.BeneficiaryAccNoDataGridViewTextBoxColumn, Me.BeneficiaryName1DataGridViewTextBoxColumn, Me.ChargesAccName1DataGridViewTextBoxColumn, Me.BeneficiaryMsg3DataGridViewTextBoxColumn, Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn, Me.ChargesAccName2DataGridViewTextBoxColumn, Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn, Me.IntermediaryBranchDataGridViewTextBoxColumn, Me.IntermediaryBankAddress2DataGridViewTextBoxColumn, Me.SectorSelectionDataGridViewTextBoxColumn, Me.IntermediaryBankAddress1DataGridViewTextBoxColumn, Me.BeneficiaryMsg1DataGridViewTextBoxColumn, Me.BeneficiaryMsg2DataGridViewTextBoxColumn, Me.BeneficiaryBankDataGridViewTextBoxColumn, Me.SettlementAccName2DataGridViewTextBoxColumn, Me.SettlementAccName4DataGridViewTextBoxColumn, Me.ChargesAccDataGridViewTextBoxColumn, Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn, Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn, Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn, Me.ContractNoDataGridViewTextBoxColumn, Me.IntermediaryBankNCCDataGridViewTextBoxColumn, Me.ModifiedDateDataGridViewTextBoxColumn, Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn, Me.SettlementAccName3DataGridViewTextBoxColumn, Me.ExchangeMethodDataGridViewTextBoxColumn, Me.BankChargesDataGridViewTextBoxColumn, Me.BeneficiaryName3DataGridViewTextBoxColumn, Me.BeneficiaryBranchDataGridViewTextBoxColumn, Me.OriginalModifiedDateDataGridViewTextBoxColumn, Me.BeneficiaryName4DataGridViewTextBoxColumn, Me.BeneficiaryMsg4DataGridViewTextBoxColumn, Me.BeneficiaryName2DataGridViewTextBoxColumn, Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn, Me.OriginalModifiedByDataGridViewTextBoxColumn, Me.CreatedByDataGridViewTextBoxColumn, Me.TemplateIDDataGridViewTextBoxColumn, Me.IntermediaryBankDataGridViewTextBoxColumn, Me.ModifiedByDataGridViewTextBoxColumn, Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn, Me.CreatedDateDataGridViewTextBoxColumn, Me.BeneficiaryBankNCCDataGridViewTextBoxColumn, Me.ChargesAccName3DataGridViewTextBoxColumn, Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn, Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn, Me.ChargesAccName4DataGridViewTextBoxColumn})
        Me.TransListDatagridView.DataSource = Me.GCMSMTTransactionDetailCollectionBindingSource
        Me.TransListDatagridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TransListDatagridView.Location = New System.Drawing.Point(3, 16)
        Me.TransListDatagridView.MultiSelect = False
        Me.TransListDatagridView.Name = "TransListDatagridView"
        Me.TransListDatagridView.RowHeadersVisible = False
        Me.TransListDatagridView.Size = New System.Drawing.Size(958, 484)
        Me.TransListDatagridView.TabIndex = 0
        '
        'colActionChecked
        '
        Me.colActionChecked.DataPropertyName = "colAction"
        Me.colActionChecked.HeaderText = ""
        Me.colActionChecked.Name = "colActionChecked"
        Me.colActionChecked.Width = 50
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Sequenc"
        Me.DataGridViewTextBoxColumn1.HeaderText = "No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'CurrencyDataGridViewTextBoxColumn
        '
        Me.CurrencyDataGridViewTextBoxColumn.DataPropertyName = "Currency"
        Me.CurrencyDataGridViewTextBoxColumn.HeaderText = "Currency"
        Me.CurrencyDataGridViewTextBoxColumn.Name = "CurrencyDataGridViewTextBoxColumn"
        '
        'ValueDateDataGridViewTextBoxColumn
        '
        Me.ValueDateDataGridViewTextBoxColumn.DataPropertyName = "ValueDate"
        Me.ValueDateDataGridViewTextBoxColumn.HeaderText = "Value Date"
        Me.ValueDateDataGridViewTextBoxColumn.Name = "ValueDateDataGridViewTextBoxColumn"
        '
        'RemitAmountDataGridViewTextBoxColumn
        '
        Me.RemitAmountDataGridViewTextBoxColumn.DataPropertyName = "RemitAmount"
        Me.RemitAmountDataGridViewTextBoxColumn.HeaderText = "Remittance Amount"
        Me.RemitAmountDataGridViewTextBoxColumn.Name = "RemitAmountDataGridViewTextBoxColumn"
        Me.RemitAmountDataGridViewTextBoxColumn.Width = 150
        '
        'CustomerReferenceDataGridViewTextBoxColumn
        '
        Me.CustomerReferenceDataGridViewTextBoxColumn.DataPropertyName = "CustomerReference"
        Me.CustomerReferenceDataGridViewTextBoxColumn.HeaderText = "Customer Reference"
        Me.CustomerReferenceDataGridViewTextBoxColumn.Name = "CustomerReferenceDataGridViewTextBoxColumn"
        Me.CustomerReferenceDataGridViewTextBoxColumn.Width = 150
        '
        'SettlementAccNoDataGridViewTextBoxColumn
        '
        Me.SettlementAccNoDataGridViewTextBoxColumn.DataPropertyName = "SettlementAccNo"
        Me.SettlementAccNoDataGridViewTextBoxColumn.HeaderText = "Settlement Acc No"
        Me.SettlementAccNoDataGridViewTextBoxColumn.Name = "SettlementAccNoDataGridViewTextBoxColumn"
        Me.SettlementAccNoDataGridViewTextBoxColumn.Width = 150
        '
        'SettlementAccName1DataGridViewTextBoxColumn
        '
        Me.SettlementAccName1DataGridViewTextBoxColumn.DataPropertyName = "SettlementAccName1"
        Me.SettlementAccName1DataGridViewTextBoxColumn.HeaderText = "Settlement Acc Name"
        Me.SettlementAccName1DataGridViewTextBoxColumn.Name = "SettlementAccName1DataGridViewTextBoxColumn"
        Me.SettlementAccName1DataGridViewTextBoxColumn.Width = 150
        '
        'BeneficiaryAccNoDataGridViewTextBoxColumn
        '
        Me.BeneficiaryAccNoDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryAccNo"
        Me.BeneficiaryAccNoDataGridViewTextBoxColumn.HeaderText = "Beneficiary Acc No"
        Me.BeneficiaryAccNoDataGridViewTextBoxColumn.Name = "BeneficiaryAccNoDataGridViewTextBoxColumn"
        Me.BeneficiaryAccNoDataGridViewTextBoxColumn.Width = 150
        '
        'BeneficiaryName1DataGridViewTextBoxColumn
        '
        Me.BeneficiaryName1DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryName1"
        Me.BeneficiaryName1DataGridViewTextBoxColumn.HeaderText = "Beneficiary Name"
        Me.BeneficiaryName1DataGridViewTextBoxColumn.Name = "BeneficiaryName1DataGridViewTextBoxColumn"
        Me.BeneficiaryName1DataGridViewTextBoxColumn.Width = 150
        '
        'ChargesAccName1DataGridViewTextBoxColumn
        '
        Me.ChargesAccName1DataGridViewTextBoxColumn.DataPropertyName = "ChargesAccName1"
        Me.ChargesAccName1DataGridViewTextBoxColumn.HeaderText = "ChargesAccName1"
        Me.ChargesAccName1DataGridViewTextBoxColumn.Name = "ChargesAccName1DataGridViewTextBoxColumn"
        Me.ChargesAccName1DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryMsg3DataGridViewTextBoxColumn
        '
        Me.BeneficiaryMsg3DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryMsg3"
        Me.BeneficiaryMsg3DataGridViewTextBoxColumn.HeaderText = "BeneficiaryMsg3"
        Me.BeneficiaryMsg3DataGridViewTextBoxColumn.Name = "BeneficiaryMsg3DataGridViewTextBoxColumn"
        Me.BeneficiaryMsg3DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankAddress1DataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBankAddress1"
        Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn.HeaderText = "BeneficiaryBankAddress1"
        Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn.Name = "BeneficiaryBankAddress1DataGridViewTextBoxColumn"
        Me.BeneficiaryBankAddress1DataGridViewTextBoxColumn.Visible = False
        '
        'ChargesAccName2DataGridViewTextBoxColumn
        '
        Me.ChargesAccName2DataGridViewTextBoxColumn.DataPropertyName = "ChargesAccName2"
        Me.ChargesAccName2DataGridViewTextBoxColumn.HeaderText = "ChargesAccName2"
        Me.ChargesAccName2DataGridViewTextBoxColumn.Name = "ChargesAccName2DataGridViewTextBoxColumn"
        Me.ChargesAccName2DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankAddress2DataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBankAddress2"
        Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn.HeaderText = "BeneficiaryBankAddress2"
        Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn.Name = "BeneficiaryBankAddress2DataGridViewTextBoxColumn"
        Me.BeneficiaryBankAddress2DataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBranchDataGridViewTextBoxColumn
        '
        Me.IntermediaryBranchDataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBranch"
        Me.IntermediaryBranchDataGridViewTextBoxColumn.HeaderText = "IntermediaryBranch"
        Me.IntermediaryBranchDataGridViewTextBoxColumn.Name = "IntermediaryBranchDataGridViewTextBoxColumn"
        Me.IntermediaryBranchDataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankAddress2DataGridViewTextBoxColumn
        '
        Me.IntermediaryBankAddress2DataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBankAddress2"
        Me.IntermediaryBankAddress2DataGridViewTextBoxColumn.HeaderText = "IntermediaryBankAddress2"
        Me.IntermediaryBankAddress2DataGridViewTextBoxColumn.Name = "IntermediaryBankAddress2DataGridViewTextBoxColumn"
        Me.IntermediaryBankAddress2DataGridViewTextBoxColumn.Visible = False
        '
        'SectorSelectionDataGridViewTextBoxColumn
        '
        Me.SectorSelectionDataGridViewTextBoxColumn.DataPropertyName = "SectorSelection"
        Me.SectorSelectionDataGridViewTextBoxColumn.HeaderText = "SectorSelection"
        Me.SectorSelectionDataGridViewTextBoxColumn.Name = "SectorSelectionDataGridViewTextBoxColumn"
        Me.SectorSelectionDataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankAddress1DataGridViewTextBoxColumn
        '
        Me.IntermediaryBankAddress1DataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBankAddress1"
        Me.IntermediaryBankAddress1DataGridViewTextBoxColumn.HeaderText = "IntermediaryBankAddress1"
        Me.IntermediaryBankAddress1DataGridViewTextBoxColumn.Name = "IntermediaryBankAddress1DataGridViewTextBoxColumn"
        Me.IntermediaryBankAddress1DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryMsg1DataGridViewTextBoxColumn
        '
        Me.BeneficiaryMsg1DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryMsg1"
        Me.BeneficiaryMsg1DataGridViewTextBoxColumn.HeaderText = "BeneficiaryMsg1"
        Me.BeneficiaryMsg1DataGridViewTextBoxColumn.Name = "BeneficiaryMsg1DataGridViewTextBoxColumn"
        Me.BeneficiaryMsg1DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryMsg2DataGridViewTextBoxColumn
        '
        Me.BeneficiaryMsg2DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryMsg2"
        Me.BeneficiaryMsg2DataGridViewTextBoxColumn.HeaderText = "BeneficiaryMsg2"
        Me.BeneficiaryMsg2DataGridViewTextBoxColumn.Name = "BeneficiaryMsg2DataGridViewTextBoxColumn"
        Me.BeneficiaryMsg2DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankDataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBank"
        Me.BeneficiaryBankDataGridViewTextBoxColumn.HeaderText = "BeneficiaryBank"
        Me.BeneficiaryBankDataGridViewTextBoxColumn.Name = "BeneficiaryBankDataGridViewTextBoxColumn"
        Me.BeneficiaryBankDataGridViewTextBoxColumn.Visible = False
        '
        'SettlementAccName2DataGridViewTextBoxColumn
        '
        Me.SettlementAccName2DataGridViewTextBoxColumn.DataPropertyName = "SettlementAccName2"
        Me.SettlementAccName2DataGridViewTextBoxColumn.HeaderText = "SettlementAccName2"
        Me.SettlementAccName2DataGridViewTextBoxColumn.Name = "SettlementAccName2DataGridViewTextBoxColumn"
        Me.SettlementAccName2DataGridViewTextBoxColumn.Visible = False
        '
        'SettlementAccName4DataGridViewTextBoxColumn
        '
        Me.SettlementAccName4DataGridViewTextBoxColumn.DataPropertyName = "SettlementAccName4"
        Me.SettlementAccName4DataGridViewTextBoxColumn.HeaderText = "SettlementAccName4"
        Me.SettlementAccName4DataGridViewTextBoxColumn.Name = "SettlementAccName4DataGridViewTextBoxColumn"
        Me.SettlementAccName4DataGridViewTextBoxColumn.Visible = False
        '
        'ChargesAccDataGridViewTextBoxColumn
        '
        Me.ChargesAccDataGridViewTextBoxColumn.DataPropertyName = "ChargesAcc"
        Me.ChargesAccDataGridViewTextBoxColumn.HeaderText = "ChargesAcc"
        Me.ChargesAccDataGridViewTextBoxColumn.Name = "ChargesAccDataGridViewTextBoxColumn"
        Me.ChargesAccDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryRemitPurposeDataGridViewTextBoxColumn
        '
        Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryRemitPurpose"
        Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn.HeaderText = "BeneficiaryRemitPurpose"
        Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn.Name = "BeneficiaryRemitPurposeDataGridViewTextBoxColumn"
        Me.BeneficiaryRemitPurposeDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryRemitInfo2DataGridViewTextBoxColumn
        '
        Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryRemitInfo2"
        Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn.HeaderText = "BeneficiaryRemitInfo2"
        Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn.Name = "BeneficiaryRemitInfo2DataGridViewTextBoxColumn"
        Me.BeneficiaryRemitInfo2DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryRemitInfo3DataGridViewTextBoxColumn
        '
        Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryRemitInfo3"
        Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn.HeaderText = "BeneficiaryRemitInfo3"
        Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn.Name = "BeneficiaryRemitInfo3DataGridViewTextBoxColumn"
        Me.BeneficiaryRemitInfo3DataGridViewTextBoxColumn.Visible = False
        '
        'ContractNoDataGridViewTextBoxColumn
        '
        Me.ContractNoDataGridViewTextBoxColumn.DataPropertyName = "ContractNo"
        Me.ContractNoDataGridViewTextBoxColumn.HeaderText = "ContractNo"
        Me.ContractNoDataGridViewTextBoxColumn.Name = "ContractNoDataGridViewTextBoxColumn"
        Me.ContractNoDataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankNCCDataGridViewTextBoxColumn
        '
        Me.IntermediaryBankNCCDataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBankNCC"
        Me.IntermediaryBankNCCDataGridViewTextBoxColumn.HeaderText = "IntermediaryBankNCC"
        Me.IntermediaryBankNCCDataGridViewTextBoxColumn.Name = "IntermediaryBankNCCDataGridViewTextBoxColumn"
        Me.IntermediaryBankNCCDataGridViewTextBoxColumn.Visible = False
        '
        'ModifiedDateDataGridViewTextBoxColumn
        '
        Me.ModifiedDateDataGridViewTextBoxColumn.DataPropertyName = "ModifiedDate"
        Me.ModifiedDateDataGridViewTextBoxColumn.HeaderText = "ModifiedDate"
        Me.ModifiedDateDataGridViewTextBoxColumn.Name = "ModifiedDateDataGridViewTextBoxColumn"
        Me.ModifiedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModifiedDateDataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankSWIFTBICDataGridViewTextBoxColumn
        '
        Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBankSWIFTBIC"
        Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn.HeaderText = "IntermediaryBankSWIFTBIC"
        Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn.Name = "IntermediaryBankSWIFTBICDataGridViewTextBoxColumn"
        Me.IntermediaryBankSWIFTBICDataGridViewTextBoxColumn.Visible = False
        '
        'SettlementAccName3DataGridViewTextBoxColumn
        '
        Me.SettlementAccName3DataGridViewTextBoxColumn.DataPropertyName = "SettlementAccName3"
        Me.SettlementAccName3DataGridViewTextBoxColumn.HeaderText = "SettlementAccName3"
        Me.SettlementAccName3DataGridViewTextBoxColumn.Name = "SettlementAccName3DataGridViewTextBoxColumn"
        Me.SettlementAccName3DataGridViewTextBoxColumn.Visible = False
        '
        'ExchangeMethodDataGridViewTextBoxColumn
        '
        Me.ExchangeMethodDataGridViewTextBoxColumn.DataPropertyName = "ExchangeMethod"
        Me.ExchangeMethodDataGridViewTextBoxColumn.HeaderText = "ExchangeMethod"
        Me.ExchangeMethodDataGridViewTextBoxColumn.Name = "ExchangeMethodDataGridViewTextBoxColumn"
        Me.ExchangeMethodDataGridViewTextBoxColumn.Visible = False
        '
        'BankChargesDataGridViewTextBoxColumn
        '
        Me.BankChargesDataGridViewTextBoxColumn.DataPropertyName = "BankCharges"
        Me.BankChargesDataGridViewTextBoxColumn.HeaderText = "BankCharges"
        Me.BankChargesDataGridViewTextBoxColumn.Name = "BankChargesDataGridViewTextBoxColumn"
        Me.BankChargesDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryName3DataGridViewTextBoxColumn
        '
        Me.BeneficiaryName3DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryName3"
        Me.BeneficiaryName3DataGridViewTextBoxColumn.HeaderText = "BeneficiaryName3"
        Me.BeneficiaryName3DataGridViewTextBoxColumn.Name = "BeneficiaryName3DataGridViewTextBoxColumn"
        Me.BeneficiaryName3DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBranchDataGridViewTextBoxColumn
        '
        Me.BeneficiaryBranchDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBranch"
        Me.BeneficiaryBranchDataGridViewTextBoxColumn.HeaderText = "BeneficiaryBranch"
        Me.BeneficiaryBranchDataGridViewTextBoxColumn.Name = "BeneficiaryBranchDataGridViewTextBoxColumn"
        Me.BeneficiaryBranchDataGridViewTextBoxColumn.Visible = False
        '
        'OriginalModifiedDateDataGridViewTextBoxColumn
        '
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.DataPropertyName = "OriginalModifiedDate"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.HeaderText = "OriginalModifiedDate"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.Name = "OriginalModifiedDateDataGridViewTextBoxColumn"
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.OriginalModifiedDateDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryName4DataGridViewTextBoxColumn
        '
        Me.BeneficiaryName4DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryName4"
        Me.BeneficiaryName4DataGridViewTextBoxColumn.HeaderText = "BeneficiaryName4"
        Me.BeneficiaryName4DataGridViewTextBoxColumn.Name = "BeneficiaryName4DataGridViewTextBoxColumn"
        Me.BeneficiaryName4DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryMsg4DataGridViewTextBoxColumn
        '
        Me.BeneficiaryMsg4DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryMsg4"
        Me.BeneficiaryMsg4DataGridViewTextBoxColumn.HeaderText = "BeneficiaryMsg4"
        Me.BeneficiaryMsg4DataGridViewTextBoxColumn.Name = "BeneficiaryMsg4DataGridViewTextBoxColumn"
        Me.BeneficiaryMsg4DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryName2DataGridViewTextBoxColumn
        '
        Me.BeneficiaryName2DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryName2"
        Me.BeneficiaryName2DataGridViewTextBoxColumn.HeaderText = "BeneficiaryName2"
        Me.BeneficiaryName2DataGridViewTextBoxColumn.Name = "BeneficiaryName2DataGridViewTextBoxColumn"
        Me.BeneficiaryName2DataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBankSWIFTBIC"
        Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn.HeaderText = "BeneficiaryBankSWIFTBIC"
        Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn.Name = "BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn"
        Me.BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn.Visible = False
        '
        'OriginalModifiedByDataGridViewTextBoxColumn
        '
        Me.OriginalModifiedByDataGridViewTextBoxColumn.DataPropertyName = "OriginalModifiedBy"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.HeaderText = "OriginalModifiedBy"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.Name = "OriginalModifiedByDataGridViewTextBoxColumn"
        Me.OriginalModifiedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.OriginalModifiedByDataGridViewTextBoxColumn.Visible = False
        '
        'CreatedByDataGridViewTextBoxColumn
        '
        Me.CreatedByDataGridViewTextBoxColumn.DataPropertyName = "CreatedBy"
        Me.CreatedByDataGridViewTextBoxColumn.HeaderText = "CreatedBy"
        Me.CreatedByDataGridViewTextBoxColumn.Name = "CreatedByDataGridViewTextBoxColumn"
        Me.CreatedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.CreatedByDataGridViewTextBoxColumn.Visible = False
        '
        'TemplateIDDataGridViewTextBoxColumn
        '
        Me.TemplateIDDataGridViewTextBoxColumn.DataPropertyName = "TemplateID"
        Me.TemplateIDDataGridViewTextBoxColumn.HeaderText = "TemplateID"
        Me.TemplateIDDataGridViewTextBoxColumn.Name = "TemplateIDDataGridViewTextBoxColumn"
        Me.TemplateIDDataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankDataGridViewTextBoxColumn
        '
        Me.IntermediaryBankDataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBank"
        Me.IntermediaryBankDataGridViewTextBoxColumn.HeaderText = "IntermediaryBank"
        Me.IntermediaryBankDataGridViewTextBoxColumn.Name = "IntermediaryBankDataGridViewTextBoxColumn"
        Me.IntermediaryBankDataGridViewTextBoxColumn.Visible = False
        '
        'ModifiedByDataGridViewTextBoxColumn
        '
        Me.ModifiedByDataGridViewTextBoxColumn.DataPropertyName = "ModifiedBy"
        Me.ModifiedByDataGridViewTextBoxColumn.HeaderText = "ModifiedBy"
        Me.ModifiedByDataGridViewTextBoxColumn.Name = "ModifiedByDataGridViewTextBoxColumn"
        Me.ModifiedByDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModifiedByDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryRemitInfo1DataGridViewTextBoxColumn
        '
        Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryRemitInfo1"
        Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn.HeaderText = "BeneficiaryRemitInfo1"
        Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn.Name = "BeneficiaryRemitInfo1DataGridViewTextBoxColumn"
        Me.BeneficiaryRemitInfo1DataGridViewTextBoxColumn.Visible = False
        '
        'CreatedDateDataGridViewTextBoxColumn
        '
        Me.CreatedDateDataGridViewTextBoxColumn.DataPropertyName = "CreatedDate"
        Me.CreatedDateDataGridViewTextBoxColumn.HeaderText = "CreatedDate"
        Me.CreatedDateDataGridViewTextBoxColumn.Name = "CreatedDateDataGridViewTextBoxColumn"
        Me.CreatedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.CreatedDateDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankNCCDataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankNCCDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBankNCC"
        Me.BeneficiaryBankNCCDataGridViewTextBoxColumn.HeaderText = "BeneficiaryBankNCC"
        Me.BeneficiaryBankNCCDataGridViewTextBoxColumn.Name = "BeneficiaryBankNCCDataGridViewTextBoxColumn"
        Me.BeneficiaryBankNCCDataGridViewTextBoxColumn.Visible = False
        '
        'ChargesAccName3DataGridViewTextBoxColumn
        '
        Me.ChargesAccName3DataGridViewTextBoxColumn.DataPropertyName = "ChargesAccName3"
        Me.ChargesAccName3DataGridViewTextBoxColumn.HeaderText = "ChargesAccName3"
        Me.ChargesAccName3DataGridViewTextBoxColumn.Name = "ChargesAccName3DataGridViewTextBoxColumn"
        Me.ChargesAccName3DataGridViewTextBoxColumn.Visible = False
        '
        'IntermediaryBankMasterCodeDataGridViewTextBoxColumn
        '
        Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn.DataPropertyName = "IntermediaryBankMasterCode"
        Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn.HeaderText = "IntermediaryBankMasterCode"
        Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn.Name = "IntermediaryBankMasterCodeDataGridViewTextBoxColumn"
        Me.IntermediaryBankMasterCodeDataGridViewTextBoxColumn.Visible = False
        '
        'BeneficiaryBankMasterCodeDataGridViewTextBoxColumn
        '
        Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn.DataPropertyName = "BeneficiaryBankMasterCode"
        Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn.HeaderText = "BeneficiaryBankMasterCode"
        Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn.Name = "BeneficiaryBankMasterCodeDataGridViewTextBoxColumn"
        Me.BeneficiaryBankMasterCodeDataGridViewTextBoxColumn.Visible = False
        '
        'ChargesAccName4DataGridViewTextBoxColumn
        '
        Me.ChargesAccName4DataGridViewTextBoxColumn.DataPropertyName = "ChargesAccName4"
        Me.ChargesAccName4DataGridViewTextBoxColumn.HeaderText = "ChargesAccName4"
        Me.ChargesAccName4DataGridViewTextBoxColumn.Name = "ChargesAccName4DataGridViewTextBoxColumn"
        Me.ChargesAccName4DataGridViewTextBoxColumn.Visible = False
        '
        'GCMSMTTransactionDetailCollectionBindingSource
        '
        Me.GCMSMTTransactionDetailCollectionBindingSource.DataSource = GetType(BTMU.Magic.GCMS_MT.GCMSMTTransactionDetailCollection)
        '
        'TransListButtonGroupBox
        '
        Me.TransListButtonGroupBox.Controls.Add(Me.btnBack)
        Me.TransListButtonGroupBox.Controls.Add(Me.btnSave)
        Me.TransListButtonGroupBox.Controls.Add(Me.btnUpload)
        Me.TransListButtonGroupBox.Controls.Add(Me.btnDelete)
        Me.TransListButtonGroupBox.Controls.Add(Me.btnModify)
        Me.TransListButtonGroupBox.Controls.Add(Me.btnAdd)
        Me.TransListButtonGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TransListButtonGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TransListButtonGroupBox.Location = New System.Drawing.Point(0, 503)
        Me.TransListButtonGroupBox.Name = "TransListButtonGroupBox"
        Me.TransListButtonGroupBox.Size = New System.Drawing.Size(964, 50)
        Me.TransListButtonGroupBox.TabIndex = 1
        Me.TransListButtonGroupBox.TabStop = False
        '
        'btnBack
        '
        Me.btnBack.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(533, 9)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(87, 37)
        Me.btnBack.TabIndex = 5
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(287, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 37)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnUpload
        '
        Me.btnUpload.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.Location = New System.Drawing.Point(441, 9)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(87, 37)
        Me.btnUpload.TabIndex = 4
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(194, 9)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(87, 37)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnModify
        '
        Me.btnModify.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModify.Location = New System.Drawing.Point(101, 9)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(87, 37)
        Me.btnModify.TabIndex = 1
        Me.btnModify.Text = "Modify"
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Location = New System.Drawing.Point(8, 9)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(87, 37)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'TransListPanel
        '
        Me.TransListPanel.AutoScroll = True
        Me.TransListPanel.Controls.Add(Me.TransListGroupBox)
        Me.TransListPanel.Controls.Add(Me.TransListButtonGroupBox)
        Me.TransListPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TransListPanel.Location = New System.Drawing.Point(5, 5)
        Me.TransListPanel.Name = "TransListPanel"
        Me.TransListPanel.Size = New System.Drawing.Size(964, 553)
        Me.TransListPanel.TabIndex = 3
        '
        'ucGCMSMTTransList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TransListPanel)
        Me.Name = "ucGCMSMTTransList"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Size = New System.Drawing.Size(974, 563)
        Me.TransListGroupBox.ResumeLayout(False)
        CType(Me.TransListDatagridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GCMSMTTransactionDetailCollectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TransListButtonGroupBox.ResumeLayout(False)
        Me.TransListPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TransListGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents TransListButtonGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnModify As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents TransListPanel As System.Windows.Forms.Panel
    Friend WithEvents SequenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryMasterCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryAccNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RemitInformationToBankDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMasterCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryNameAddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerRefDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMessageDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RemittanceAmtDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransListDatagridView As System.Windows.Forms.DataGridView
    Friend WithEvents GCMSMTTransactionDetailCollectionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BeneficiaryBankOptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankOptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colActionChecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CurrencyDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValueDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RemitAmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccName1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryAccNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryName1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChargesAccName1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMsg3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankAddress1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChargesAccName2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankAddress2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBranchDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankAddress2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SectorSelectionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankAddress1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMsg1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMsg2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccName2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccName4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChargesAccDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryRemitPurposeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryRemitInfo2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryRemitInfo3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContractNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankNCCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModifiedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankSWIFTBICDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettlementAccName3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ExchangeMethodDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BankChargesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryName3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBranchDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OriginalModifiedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryName4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryMsg4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryName2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankSWIFTBICDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OriginalModifiedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CreatedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TemplateIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModifiedByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryRemitInfo1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CreatedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankNCCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChargesAccName3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntermediaryBankMasterCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeneficiaryBankMasterCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChargesAccName4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
