<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGCMSMTBase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGCMSMTBase))
        Me.UcGCMSMTTransList1 = New BTMU.Magic.UI.ucGCMSMTTransList
        Me.UcGCMSMTMainMenu1 = New BTMU.Magic.UI.ucGCMSMTMainMenu
        Me.UcGCMSMTTransDetail1 = New BTMU.Magic.UI.ucGCMSMTTransDetail
        Me.SuspendLayout()
        '
        'UcGCMSMTTransList1
        '
        Me.UcGCMSMTTransList1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UcGCMSMTTransList1.ContainerForm = Nothing
        Me.UcGCMSMTTransList1.Location = New System.Drawing.Point(-2, 134)
        Me.UcGCMSMTTransList1.Name = "UcGCMSMTTransList1"
        Me.UcGCMSMTTransList1.Padding = New System.Windows.Forms.Padding(5)
        Me.UcGCMSMTTransList1.PColDirty = False
        Me.UcGCMSMTTransList1.Size = New System.Drawing.Size(833, 463)
        Me.UcGCMSMTTransList1.TabIndex = 2
        '
        'UcGCMSMTMainMenu1
        '
        Me.UcGCMSMTMainMenu1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UcGCMSMTMainMenu1.ContainerForm = Nothing
        Me.UcGCMSMTMainMenu1.Location = New System.Drawing.Point(-2, -1)
        Me.UcGCMSMTMainMenu1.Name = "UcGCMSMTMainMenu1"
        Me.UcGCMSMTMainMenu1.Size = New System.Drawing.Size(268, 488)
        Me.UcGCMSMTMainMenu1.TabIndex = 3
        '
        'UcGCMSMTTransDetail1
        '
        Me.UcGCMSMTTransDetail1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UcGCMSMTTransDetail1.ContainerForm = Nothing
        Me.UcGCMSMTTransDetail1.Location = New System.Drawing.Point(-76, 13)
        Me.UcGCMSMTTransDetail1.Name = "UcGCMSMTTransDetail1"
        Me.UcGCMSMTTransDetail1.Padding = New System.Windows.Forms.Padding(5)
        Me.UcGCMSMTTransDetail1.Size = New System.Drawing.Size(826, 710)
        Me.UcGCMSMTTransDetail1.TabIndex = 1
        '
        'frmGCMSMTBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(916, 598)
        Me.Controls.Add(Me.UcGCMSMTTransList1)
        Me.Controls.Add(Me.UcGCMSMTMainMenu1)
        Me.Controls.Add(Me.UcGCMSMTTransDetail1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGCMSMTBase"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GCMS Money Transfer Data Entry (MA4000)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcGCMSMTTransDetail1 As BTMU.MAGIC.UI.ucGCMSMTTransDetail
    Friend WithEvents UcGCMSMTTransList1 As BTMU.MAGIC.UI.ucGCMSMTTransList
    Friend WithEvents UcGCMSMTMainMenu1 As BTMU.MAGIC.UI.ucGCMSMTMainMenu
    'Friend WithEvents UcGCMSMTConfig1 As BTMU.MAGIC.UI.ucGCMSMTConfig

End Class
