Imports System.Windows.Forms

Public Class ucGCMSMTBase
    Inherits System.Windows.Forms.UserControl

    Protected _parent As frmGCMSMTBase

    Public Property ContainerForm() As frmGCMSMTBase
        Get
            Return _parent
        End Get
        Set(ByVal value As frmGCMSMTBase)
            _parent = value
        End Set
    End Property

End Class
