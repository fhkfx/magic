<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucGCMSMTMainMenu
    Inherits BTMU.MAGIC.UI.ucGCMSMTBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMainCreate = New System.Windows.Forms.Button
        Me.btnMainEdit = New System.Windows.Forms.Button
        Me.btnMainExit = New System.Windows.Forms.Button
        Me.MainMenuPanel = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ButtonsGroupBox = New System.Windows.Forms.GroupBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.MagicVersionPanel = New System.Windows.Forms.Panel
        Me.MagicVersionLabel = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.MainMenuPanel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ButtonsGroupBox.SuspendLayout()
        Me.MagicVersionPanel.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMainCreate
        '
        Me.btnMainCreate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMainCreate.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMainCreate.Location = New System.Drawing.Point(212, 39)
        Me.btnMainCreate.Name = "btnMainCreate"
        Me.btnMainCreate.Size = New System.Drawing.Size(147, 52)
        Me.btnMainCreate.TabIndex = 0
        Me.btnMainCreate.Text = "Create"
        Me.btnMainCreate.UseVisualStyleBackColor = True
        '
        'btnMainEdit
        '
        Me.btnMainEdit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMainEdit.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMainEdit.Location = New System.Drawing.Point(212, 109)
        Me.btnMainEdit.Name = "btnMainEdit"
        Me.btnMainEdit.Size = New System.Drawing.Size(147, 52)
        Me.btnMainEdit.TabIndex = 1
        Me.btnMainEdit.Text = "Edit"
        Me.btnMainEdit.UseVisualStyleBackColor = True
        '
        'btnMainExit
        '
        Me.btnMainExit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMainExit.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMainExit.Location = New System.Drawing.Point(212, 179)
        Me.btnMainExit.Name = "btnMainExit"
        Me.btnMainExit.Size = New System.Drawing.Size(147, 52)
        Me.btnMainExit.TabIndex = 3
        Me.btnMainExit.Text = "Exit"
        Me.btnMainExit.UseVisualStyleBackColor = True
        '
        'MainMenuPanel
        '
        Me.MainMenuPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainMenuPanel.Controls.Add(Me.Panel1)
        Me.MainMenuPanel.Controls.Add(Me.Panel2)
        Me.MainMenuPanel.Controls.Add(Me.MagicVersionPanel)
        Me.MainMenuPanel.Controls.Add(Me.PictureBox1)
        Me.MainMenuPanel.Location = New System.Drawing.Point(0, 0)
        Me.MainMenuPanel.Name = "MainMenuPanel"
        Me.MainMenuPanel.Size = New System.Drawing.Size(610, 501)
        Me.MainMenuPanel.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ButtonsGroupBox)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 171)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(20)
        Me.Panel1.Size = New System.Drawing.Size(610, 310)
        Me.Panel1.TabIndex = 9
        '
        'ButtonsGroupBox
        '
        Me.ButtonsGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonsGroupBox.Controls.Add(Me.btnMainCreate)
        Me.ButtonsGroupBox.Controls.Add(Me.btnMainEdit)
        Me.ButtonsGroupBox.Controls.Add(Me.btnMainExit)
        Me.ButtonsGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonsGroupBox.Location = New System.Drawing.Point(20, 20)
        Me.ButtonsGroupBox.Name = "ButtonsGroupBox"
        Me.ButtonsGroupBox.Padding = New System.Windows.Forms.Padding(5)
        Me.ButtonsGroupBox.Size = New System.Drawing.Size(570, 270)
        Me.ButtonsGroupBox.TabIndex = 5
        Me.ButtonsGroupBox.TabStop = False
        Me.ButtonsGroupBox.Text = "GCMS Money Transfer Data Entry"
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 481)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(610, 20)
        Me.Panel2.TabIndex = 6
        '
        'MagicVersionPanel
        '
        Me.MagicVersionPanel.BackColor = System.Drawing.SystemColors.Window
        Me.MagicVersionPanel.Controls.Add(Me.MagicVersionLabel)
        Me.MagicVersionPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.MagicVersionPanel.Location = New System.Drawing.Point(0, 152)
        Me.MagicVersionPanel.Name = "MagicVersionPanel"
        Me.MagicVersionPanel.Size = New System.Drawing.Size(610, 19)
        Me.MagicVersionPanel.TabIndex = 8
        '
        'MagicVersionLabel
        '
        Me.MagicVersionLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MagicVersionLabel.AutoSize = True
        Me.MagicVersionLabel.BackColor = System.Drawing.SystemColors.Window
        Me.MagicVersionLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MagicVersionLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MagicVersionLabel.Location = New System.Drawing.Point(507, 2)
        Me.MagicVersionLabel.Name = "MagicVersionLabel"
        Me.MagicVersionLabel.Size = New System.Drawing.Size(86, 14)
        Me.MagicVersionLabel.TabIndex = 7
        Me.MagicVersionLabel.Text = "Magic Version"
        Me.MagicVersionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = Global.BTMU.Magic.UI.My.Resources.Resources.Main_GCMSMT
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(610, 152)
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'ucGCMSMTMainMenu
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Controls.Add(Me.MainMenuPanel)
        Me.Name = "ucGCMSMTMainMenu"
        Me.Size = New System.Drawing.Size(610, 501)
        Me.MainMenuPanel.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ButtonsGroupBox.ResumeLayout(False)
        Me.MagicVersionPanel.ResumeLayout(False)
        Me.MagicVersionPanel.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnMainCreate As System.Windows.Forms.Button
    Friend WithEvents btnMainEdit As System.Windows.Forms.Button
    Friend WithEvents btnMainExit As System.Windows.Forms.Button
    Friend WithEvents MainMenuPanel As System.Windows.Forms.Panel
    Friend WithEvents ButtonsGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents MagicVersionLabel As System.Windows.Forms.Label
    Friend WithEvents MagicVersionPanel As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel

End Class
