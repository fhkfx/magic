Imports System.IO
Imports BTMU.MAGIC.GCMS_MT
Imports BTMU.MAGIC.Common

''' <summary>
''' User control for GCMS-MT transaction list view
''' </summary>
''' <remarks></remarks>
Public Class ucGCMSMTTransList

#Region "Private Variables"

    Private msgReader As Global.System.Resources.ResourceManager
    Private lstMaster As New GCMSMTTransactionDetailCollection

    Private _colDirty As Boolean

    'List action: SAVE, EDIT, DELETE, UPLOAD
    Enum eListAction
        DELETE
        EDIT
        SAVE
        UPLOAD
    End Enum

#End Region

#Region "Properties"

    Public Property PAddListMaster(Optional ByVal index As Integer = -1) As GCMSMTTransactionDetail
        Get
            Return lstMaster(index)
        End Get
        Set(ByVal value As GCMSMTTransactionDetail)
            If Not value Is Nothing Then
                lstMaster.Add(value)
            End If
        End Set
    End Property

    Public ReadOnly Property PListMaster() As GCMSMTTransactionDetailCollection
        Get
            Return lstMaster
        End Get
    End Property

    Public ReadOnly Property PGetListMasterIndex() As Integer
        Get
            If lstMaster.Count = 0 Then
                Return lstMaster.Count + 1
            Else
                Return lstMaster.Count
            End If

        End Get
    End Property

    Public Property PColDirty() As Boolean
        Get
            Return _colDirty
        End Get
        Set(ByVal value As Boolean)
            If _colDirty <> value Then
                _colDirty = value
            End If
        End Set
    End Property

#End Region

    Private Sub ucGCMSMTTransList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SuspendLayout()

        msgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
        GCMSMTTransactionDetailCollectionBindingSource.DataSource = PListMaster

        PColDirty = False

        ResumeLayout()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Try
            GCMSMTTransactionDetailCollectionBindingSource.AddNew()
            ContainerForm.UcGCMSMTTransDetail1.PopulateTransactionDetailNew(GCMSMTTransactionDetailCollectionBindingSource.Current)
            ContainerForm.ShowTransactionDetail("New")
            ContainerForm.PRecordState = frmGCMSMTBase.eRecordState.ENTRY
        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click

        Dim boolChecked As Boolean = False
        Dim selectedItemSeq As New ArrayList()

        Try
            Me.IsListEmpty(eListAction.EDIT)
            Me.GetSelectedItemSequence(selectedItemSeq, boolChecked)
            Me.IsListItemSelected(selectedItemSeq, boolChecked, eListAction.EDIT)

            ContainerForm.UcGCMSMTTransDetail1.PopulateTransactionDetailEdit(PListMaster(selectedItemSeq(0) - 1))
            ContainerForm.ShowTransactionDetail("Edit")
            ContainerForm.PRecordState = frmGCMSMTBase.eRecordState.EDIT

            ContainerForm.UcGCMSMTTransDetail1.ValueDateTextBox.Focus()
        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim counter As Int32 = 0
        Dim selectedItemSeq As New ArrayList()
        Dim boolChecked As Boolean = False

        Dim dlgResult As DialogResult = DialogResult.None

        Try
            Me.IsListEmpty(eListAction.DELETE)
            Me.GetSelectedItemSequence(selectedItemSeq, boolChecked)
            Me.IsListItemSelected(selectedItemSeq, boolChecked, eListAction.DELETE)

            dlgResult = MessageBox.Show("Do you want to delete the selected records?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)

            If dlgResult = DialogResult.Yes Then
                GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = False
                Me.RemoveSelectedItemFromList(selectedItemSeq)
                Me.ResetListSequence()
                GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = True
                GCMSMTTransactionDetailCollectionBindingSource.ResetBindings(False)
                PColDirty = True
            End If

        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click

        Dim dlgResult As DialogResult = DialogResult.None

        Try
            If PColDirty Then
                If ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE AndAlso _
                    Me.GCMSMTTransactionDetailCollectionBindingSource.Count = 0 Then
                    ClearAndResetCollection()
                    ContainerForm.ShowMainMenu()
                Else
                    dlgResult = MessageBox.Show("Data has been changed. Do you want to save the data?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)
                    If dlgResult = DialogResult.Yes Then
                        Me.btnSave_Click(Nothing, Nothing)
                        If Not PColDirty Then
                            ClearAndResetCollection()
                            ContainerForm.ShowMainMenu()
                        End If
                    ElseIf dlgResult = DialogResult.No Then
                        ClearAndResetCollection()
                        ContainerForm.ShowMainMenu()
                        PColDirty = False
                    Else
                        'do nothing
                    End If
                End If
            Else
                ClearAndResetCollection()
                ContainerForm.ShowMainMenu()
            End If
        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            CheckSaveFoldersExistance(eListAction.SAVE)

            If ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE Then
                If Me.GCMSMTTransactionDetailCollectionBindingSource.Count > 0 Then
                    If SaveCollection(eListAction.SAVE) Then
                        PColDirty = False
                    End If
                Else
                    MessageBox.Show(msgReader.GetString("E04000230"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information) '--
                End If
            Else
                If SaveCollection(eListAction.SAVE) Then
                    PColDirty = False
                End If
            End If

        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        Try
            CheckSaveFoldersExistance(eListAction.UPLOAD)

            'If ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE Then

            'Else
            If Me.GCMSMTTransactionDetailCollectionBindingSource.Count > 0 Then
                If SaveCollection(eListAction.UPLOAD) Then
                    PColDirty = False
                End If
            Else
                MessageBox.Show(msgReader.GetString("E04000240"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information) '--
            End If
            'If SaveCollection(eListAction.UPLOAD) Then
            '    PColDirty = False
            'End If
            'End If
        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TransListDatagridView_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles TransListDatagridView.CellEnter
        If e.ColumnIndex <> 0 Then
            TransListDatagridView.Columns(e.ColumnIndex).ReadOnly = True
        End If
    End Sub

    ''' <summary>
    ''' Populate transactions data from selected file to a list view
    ''' </summary>
    ''' <param name="boolProceed">Flag: True = populated successfully, False = Cancel/Fail to populate</param>
    ''' <remarks></remarks>
    Public Sub PopulateTransactionList(ByRef boolProceed As Boolean)

        Dim objMaster As GCMSMTMaster = Nothing
        Dim objTransDet As GCMSMTTransactionDetail = Nothing
        Dim srTransDetail As StreamReader = Nothing
        Dim fsTransDetail As FileStream = Nothing
        Dim openFileDialog As New OpenFileDialog

        Dim drRow As DataRow = Nothing
        Dim strTransDetailLine As String
        Dim strTransDetailLineArr() As String
        Dim fileInfo As FileInfo
        Dim index As Integer = 1

        Try

            boolProceed = False

            openFileDialog.InitialDirectory = ContainerForm.PSavedFilePath
            openFileDialog.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv"

            If openFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

                objMaster = ContainerForm.PGCMSMTMaster()
                fsTransDetail = openFileDialog.OpenFile()

                If (fsTransDetail IsNot Nothing) Then

                    '--
                    'Dim allText As String = File.ReadAllText(openFileDialog.FileName)
                    'Dim allHashText As String = HelperModule.ComputeHash(allText, "SHA512", Nothing)
                    'Dim allHashText As String = HelperModule.ComputeHashAsHexString(allText, "SHA512")

                    srTransDetail = New StreamReader(fsTransDetail)

                    While srTransDetail.Peek <> -1

                        strTransDetailLine = srTransDetail.ReadLine()

                        If strTransDetailLine.Trim <> "" Then

                            objTransDet = New GCMSMTTransactionDetail
                            strTransDetailLineArr = Common.HelperModule.Split(strTransDetailLine, ",", """", False)

                            'Check format
                            If strTransDetailLineArr.Length <> objMaster.PMaxSavedTemplateCnt Then
                                Throw New MagicException(msgReader.GetString("E04000190") & vbCrLf & " (" & openFileDialog.FileName & ")") '--
                            End If

                            'Detail 
                            objTransDet.Sequenc = index
                            objTransDet.SettlementAccNo = strTransDetailLineArr(0)
                            objTransDet.SettlementAccNoAndName = ""
                            drRow = objMaster.GetSettlementRow(objTransDet.SettlementAccNo, "AccountNo") 'Retrieve from master
                            If Not drRow Is Nothing Then
                                objTransDet.SettlementAccName1 = drRow.Item("AccountName1")
                                objTransDet.SettlementAccName2 = drRow.Item("AccountName2")
                                objTransDet.SettlementAccName3 = drRow.Item("AccountName3")
                                objTransDet.SettlementAccName4 = drRow.Item("AccountName4")
                                objTransDet.SettlementAccNoAndName = drRow.Item("AccountNoAndName")
                                objTransDet.SettlementBank = drRow.Item("Bank")
                                objTransDet.SettlementBranch = drRow.Item("Branch")
                                objTransDet.SettlementBankAddress1 = drRow.Item("BankAddress1")
                                objTransDet.SettlementBankAddress2 = drRow.Item("BankAddress2")
                                objTransDet.SettlementAccCurrency = drRow.Item("CurrencyCode")
                            End If

                            objTransDet.ValueDate = strTransDetailLineArr(1)
                            objTransDet.CustomerReference = strTransDetailLineArr(2)
                            objTransDet.SectorSelection = strTransDetailLineArr(3)
                            objTransDet.Currency = strTransDetailLineArr(4)
                            objTransDet.RemitAmount = strTransDetailLineArr(5)
                            objTransDet.ExchangeMethod = strTransDetailLineArr(6)
                            objTransDet.ContractNo = strTransDetailLineArr(7)

                            strTransDetailLineArr(8) = strTransDetailLineArr(8) & Strings.Space(objMaster.PMaxIntermediaryLength)
                            strTransDetailLineArr(8) = strTransDetailLineArr(8).Substring(0, objMaster.PMaxIntermediaryLength)

                            If strTransDetailLineArr(8).StartsWith(ContainerForm.PSavedOption_A) Then
                                objTransDet.IntermediaryBankOptionA = True
                                objTransDet.IntermediaryBank = strTransDetailLineArr(8).Substring(2, strTransDetailLineArr(8).Length - 3).Trim
                            ElseIf strTransDetailLineArr(8).StartsWith(ContainerForm.PSavedOption_C) Then
                                objTransDet.IntermediaryBankOptionC = True
                                objTransDet.IntermediaryBank = strTransDetailLineArr(8).Substring(2, strTransDetailLineArr(8).Length - 3).Trim
                            Else
                                objTransDet.IntermediaryBankOptionD = True
                                objTransDet.IntermediaryBank = objMaster.GetDetailLine(strTransDetailLineArr(8), 1).Trim
                                objTransDet.IntermediaryBranch = objMaster.GetDetailLine(strTransDetailLineArr(8), 2).Trim
                                objTransDet.IntermediaryBankAddress1 = objMaster.GetDetailLine(strTransDetailLineArr(8), 3).Trim
                                objTransDet.IntermediaryBankAddress2 = objMaster.GetDetailLine(strTransDetailLineArr(8), 4).Trim
                            End If

                            objTransDet.IntermediaryBankAndBranch = ""
                            drRow = objMaster.GetBankAndBranchRow(objTransDet.IntermediaryBank, objTransDet.IntermediaryBranch)
                            If drRow IsNot Nothing Then
                                objTransDet.IntermediaryBankAndBranch = drRow.Item("BankAndBranchName")
                            End If

                            objTransDet.IntermediaryBankMasterCode = strTransDetailLineArr(9)

                            strTransDetailLineArr(10) = strTransDetailLineArr(10) & Strings.Space(objMaster.PMaxBeneficiaryLength)
                            strTransDetailLineArr(10) = strTransDetailLineArr(10).Substring(0, objMaster.PMaxBeneficiaryLength)

                            If strTransDetailLineArr(10).StartsWith(ContainerForm.PSavedOption_A) Then
                                objTransDet.BeneficiaryBankOptionA = True
                                objTransDet.BeneficiaryBank = strTransDetailLineArr(10).Substring(2, strTransDetailLineArr(10).Length - 3).Trim
                            ElseIf strTransDetailLineArr(10).StartsWith(ContainerForm.PSavedOption_C) Then
                                objTransDet.BeneficiaryBankOptionC = True
                                objTransDet.BeneficiaryBank = strTransDetailLineArr(10).Substring(2, strTransDetailLineArr(10).Length - 3).Trim
                            Else
                                objTransDet.BeneficiaryBankOptionD = True
                                objTransDet.BeneficiaryBank = (objMaster.GetDetailLine(strTransDetailLineArr(10), 1).Trim)
                                objTransDet.BeneficiaryBranch = (objMaster.GetDetailLine(strTransDetailLineArr(10), 2).Trim)
                                objTransDet.BeneficiaryBankAddress1 = (objMaster.GetDetailLine(strTransDetailLineArr(10), 3).Trim)
                                objTransDet.BeneficiaryBankAddress2 = (objMaster.GetDetailLine(strTransDetailLineArr(10), 4).Trim)
                            End If

                            objTransDet.BeneficiaryBankAndBranch = ""
                            drRow = objMaster.GetBankAndBranchRow(objTransDet.BeneficiaryBank, objTransDet.BeneficiaryBranch)
                            If drRow IsNot Nothing Then
                                objTransDet.BeneficiaryBankAndBranch = (drRow.Item("BankAndBranchName"))
                            End If

                            objTransDet.BeneficiaryBankMasterCode = (strTransDetailLineArr(11))

                            objTransDet.BeneficiaryAccNo = (strTransDetailLineArr(12))
                            strTransDetailLineArr(13) = strTransDetailLineArr(13) & Strings.Space(objMaster.PMaxBeneficiaryLength)
                            strTransDetailLineArr(13) = strTransDetailLineArr(13).Substring(0, objMaster.PMaxBeneficiaryLength)
                            objTransDet.BeneficiaryName1 = (objMaster.GetDetailLine(strTransDetailLineArr(13), 1).Trim)
                            objTransDet.BeneficiaryName2 = (objMaster.GetDetailLine(strTransDetailLineArr(13), 2).Trim)
                            objTransDet.BeneficiaryName3 = (objMaster.GetDetailLine(strTransDetailLineArr(13), 3).Trim)
                            objTransDet.BeneficiaryName4 = (objMaster.GetDetailLine(strTransDetailLineArr(13), 4).Trim)

                            objTransDet.BeneficiaryAccNoAndName = ""
                            drRow = objMaster.GetAccNoAndNameRow(objTransDet.BeneficiaryAccNo, objTransDet.BeneficiaryName1)
                            If drRow IsNot Nothing Then
                                objTransDet.BeneficiaryAccNoAndName = (drRow.Item("AccountNoAndName"))
                            End If

                            strTransDetailLineArr(14) = strTransDetailLineArr(14) & Strings.Space(objMaster.PMaxBeneficiaryMsgLength)
                            strTransDetailLineArr(14) = strTransDetailLineArr(14).Substring(0, objMaster.PMaxBeneficiaryMsgLength)
                            objTransDet.BeneficiaryMsg1 = (objMaster.GetDetailLine(strTransDetailLineArr(14), 1).Trim)
                            objTransDet.BeneficiaryMsg2 = (objMaster.GetDetailLine(strTransDetailLineArr(14), 2).Trim)
                            objTransDet.BeneficiaryMsg3 = (objMaster.GetDetailLine(strTransDetailLineArr(14), 3).Trim)
                            objTransDet.BeneficiaryMsg4 = (objMaster.GetDetailLine(strTransDetailLineArr(14), 4).Trim)

                            objTransDet.BeneficiaryRemitPurpose = (strTransDetailLineArr(15))

                            strTransDetailLineArr(16) = strTransDetailLineArr(16) & Strings.Space(objMaster.PMaxBeneficiaryInfoLength)
                            strTransDetailLineArr(16) = strTransDetailLineArr(16).Substring(0, objMaster.PMaxBeneficiaryInfoLength)
                            objTransDet.BeneficiaryRemitInfo1 = (objMaster.GetDetailLine(strTransDetailLineArr(16), 1).Trim)
                            objTransDet.BeneficiaryRemitInfo2 = (objMaster.GetDetailLine(strTransDetailLineArr(16), 2).Trim)
                            objTransDet.BeneficiaryRemitInfo3 = (objMaster.GetDetailLine(strTransDetailLineArr(16), 3).Trim)

                            objTransDet.BankCharges = strTransDetailLineArr(17)
                            drRow = objMaster.GetBankChargesDTRow(objTransDet.BankCharges) 'Retrieve from master
                            If drRow IsNot Nothing Then
                                objTransDet.BankCharges = drRow.Item("BankChargesCode")
                            End If

                            objTransDet.ChargesAcc = (strTransDetailLineArr(18))
                            objTransDet.ChargesAccNoAndName = ""
                            drRow = objMaster.GetSettlementRow(objTransDet.ChargesAcc, "AccountNo") 'Retrieve from master
                            If Not drRow Is Nothing Then
                                objTransDet.ChargesAccNoAndName = (drRow.Item("AccountNoAndName"))
                                objTransDet.ChargesAccName1 = drRow.Item("AccountName1")
                                objTransDet.ChargesAccName2 = drRow.Item("AccountName2")
                                objTransDet.ChargesAccName3 = drRow.Item("AccountName3")
                                objTransDet.ChargesAccName4 = drRow.Item("AccountName4")
                            End If

                            objTransDet.CopyTransactionDetail(objTransDet)
                            objTransDet.Validate()

                            PAddListMaster = objTransDet
                            index = index + 1

                        End If

                    End While

                    boolProceed = True

                    fileInfo = New FileInfo(openFileDialog.FileName)
                    ContainerForm.PEditFileName = fileInfo.Name
                    ContainerForm.PEditFilePath = fileInfo.DirectoryName

                End If

            End If

        Catch flEx As FileLoadException
            Throw New MagicException(flEx.Message)
        Catch MagicEx As MagicException
            Throw New MagicException(MagicEx.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        Finally
            If srTransDetail IsNot Nothing Then
                srTransDetail.Close()
            End If
            If fsTransDetail IsNot Nothing Then
                fsTransDetail.Close()
            End If
        End Try

    End Sub

    ''' <summary>
    ''' Get selected/checked row indexes
    ''' </summary>
    ''' <param name="selectedItemSeq">Array list to store selected row indexes</param>
    ''' <param name="boolChecked">Flag: True = At least 1 row is selected, False = No row selected</param>
    ''' <remarks></remarks>
    Public Sub GetSelectedItemSequence(ByRef selectedItemSeq As ArrayList, ByRef boolChecked As Boolean)

        Dim drTemp As DataGridViewRow = Nothing
        Dim cbTemp As DataGridViewCheckBoxCell

        Try
            For Each drTemp In TransListDatagridView.Rows
                cbTemp = CType(drTemp.Cells(0), DataGridViewCheckBoxCell)
                If cbTemp.FormattedValue.ToString = "True" Then
                    selectedItemSeq.Add(drTemp.Cells(1).Value.ToString)
                    boolChecked = True
                End If
            Next
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Check if list view is empty
    ''' </summary>
    ''' <param name="strActionType">Action type: Delete or Edit</param>
    ''' <remarks></remarks>
    Public Sub IsListEmpty(ByVal strActionType As String)

        If TransListDatagridView.Rows.Count = 0 Then
            If strActionType = eListAction.DELETE Then
                Throw New MagicException(msgReader.GetString("E04000220")) '--
            ElseIf strActionType = eListAction.EDIT Then
                Throw New MagicException(msgReader.GetString("E04000200")) '--
            End If
        End If

    End Sub

    ''' <summary>
    ''' Check list view for selected rows
    ''' </summary>
    ''' <param name="selectedItemSeq">Array list of selected row indexes</param>
    ''' <param name="boolChecked">Flag: True = At least 1 row is selected, False = No row selected</param>
    ''' <param name="strActionType">Action type: Delete or Edit</param>
    ''' <remarks></remarks>
    Public Sub IsListItemSelected(ByVal selectedItemSeq As ArrayList, _
                                    ByVal boolChecked As Boolean, _
                                    ByVal strActionType As String)

        If Not boolChecked Then
            If strActionType = eListAction.DELETE Then
                Throw New MagicException(msgReader.GetString("E04000220")) '--
            ElseIf strActionType = eListAction.EDIT Then
                Throw New MagicException(msgReader.GetString("E04000200")) '--
            End If
        Else
            If selectedItemSeq.Count <> 1 And strActionType = eListAction.EDIT Then
                Throw New MagicException(msgReader.GetString("E04000210")) '--
            End If
        End If

    End Sub

    ''' <summary>
    ''' Remove selected rows from list view
    ''' </summary>
    ''' <param name="selectedItemSeq">Array list of selected row indexes</param>
    ''' <remarks></remarks>
    Public Sub RemoveSelectedItemFromList(ByRef selectedItemSeq As ArrayList)

        Try
            For i As Integer = 0 To selectedItemSeq.Count - 1
                For Each objTransDetail As GCMSMTTransactionDetail In PListMaster
                    If objTransDetail.Sequenc = selectedItemSeq(i) Then
                        PListMaster.Remove(objTransDetail)
                        Exit For
                    End If
                Next
            Next i
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Reset sequence no in list view after deletion
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ResetListSequence()

        Dim sequence As Int32 = 1

        Try
            For Each objTransDet As GCMSMTTransactionDetail In PListMaster
                objTransDet.Sequenc = sequence
                sequence = sequence + 1
            Next
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Define new standard filename with timestamp for NEW action
    ''' Use existing filename for EDIT action
    ''' </summary>
    ''' <returns>File name</returns>
    ''' <remarks></remarks>
    Public Function ConstructFileName() As String

        Dim strFileName As String = ""

        Try
            If ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE Then
                strFileName = ContainerForm.PSaveFilePrefix
                strFileName = strFileName & DateTime.Now.ToString("yyyyMMddHHmmss")
                strFileName = strFileName & ContainerForm.PTxtFileExt
            ElseIf ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.EDIT Then
                strFileName = ContainerForm.PEditFileName
            End If

            Return strFileName

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Define hash filename 
    ''' </summary>
    ''' <param name="strFileName">Standard file name</param>
    ''' <returns>Standard file name with suffix "HASH" </returns>
    ''' <remarks></remarks>
    Public Function ConstructFileNameHash(ByVal strFileName As String) As String

        Dim strFileNameHash As String = ""

        Try
            If strFileName.Contains(".") Then
                strFileNameHash = strFileName.Replace(".", "HASH.")
            Else
                strFileNameHash = strFileName & "HASH."
            End If

            Return strFileNameHash

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Clear and reset collection of transaction data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearAndResetCollection()
        Try
            Me.GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = False
            GCMSMTTransactionDetailCollectionBindingSource.Clear()
            Me.GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = True
            Me.GCMSMTTransactionDetailCollectionBindingSource.ResetBindings(False)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' 1. Call funtion to define filename
    ''' 2. Call funtion to save transaction data to defined file
    ''' 3. Log action
    ''' 4. Call funtion to clear collection
    ''' </summary>
    ''' <param name="strSaveType">Save mode: Save or Upload</param>
    ''' <returns>Flag: True = Data is saved successfully, False = Fail to save</returns>
    ''' <remarks></remarks>
    Public Function SaveCollection(ByVal strSaveType As Integer) As Boolean

        Dim result As DialogResult
        Dim strFileName As String = ""
        Dim strFileNameHash As String = ""
        Dim boolProceed As Boolean = True
        Dim strLogFileName As String = ""
        Dim strLogSaveType As String = ""

        Try
            strFileName = ConstructFileName()                                       'Create a default filename.
            frmSaveFileDialog.FileNameTextBox.Text = strFileName                    'Assign default filename to textbox in file dialog.
            frmSaveFileDialog.LocationTextBox.Text = ContainerForm.PSavedFilePath   'Default output location
            frmSaveFileDialog.ShowDialog()

            If frmSaveFileDialog.DialogResult = DialogResult.OK Then

                strFileName = frmSaveFileDialog.FileNameTextBox.Text    'Re-read filename, in case of changes from user.

                'Check for file existance.
                If File.Exists(ContainerForm.PSavedFilePath & strFileName) Then
                    result = MessageBox.Show("The file " & strFileName & " already exists. Do you want to replace the existing file?", "MAGIC Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If result = Windows.Forms.DialogResult.No Then
                        boolProceed = False
                    End If
                End If

                'Proceed to save transaction to file if user confirm to overwrite existing file.
                If boolProceed Then

                    strFileNameHash = ConstructFileNameHash(strFileName)
                    SaveToFile(ContainerForm.PSavedFilePath, strFileName, strFileNameHash)

                    If strSaveType = eListAction.UPLOAD Then
                        SaveToFileAndUpload(ContainerForm.PUploadFilePath, strFileName, strFileNameHash)
                        strLogFileName = ContainerForm.PUploadFilePath & strFileName    '--Log
                        strLogSaveType = "Upload"                                       '--Log
                    Else
                        strLogFileName = ContainerForm.PSavedFilePath & strFileName     '--Log
                        strLogSaveType = "Save"                                         '--Log
                    End If

                    Dim strMessage As String = "Transaction has been saved succesfully!" & vbCrLf & vbCrLf

                    strMessage &= "Output location: " & ContainerForm.PSavedFilePath & vbCrLf
                    strMessage &= "Output file: " & strFileName & vbCrLf
                    strMessage &= "Output hash file: " & strFileNameHash & vbCrLf
                    MessageBox.Show(strMessage, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    '--Log
                    If ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE Then
                        Log_CreateMoneyTransfer(frmLogin.GsUserName, strLogSaveType, strLogFileName)
                    Else
                        Log_EditMoneyTransfer(frmLogin.GsUserName, strLogSaveType, strLogFileName)
                    End If

                    ClearAndResetCollection()
                    ContainerForm.ShowMainMenu()
                End If
            Else
                boolProceed = False
            End If

            Return boolProceed

        Catch MagicEx As MagicException
            Throw New MagicException(MagicEx.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Perform save to file action.
    ''' Format, write and save transaction data to defined standard and hash file
    ''' </summary>
    ''' <param name="strDirName">Directory name where the file is saved</param>
    ''' <param name="strFileName">Standard file name</param>
    ''' <param name="strFileNameHash">Hash file name</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal strDirName As String, ByVal strFileName As String, ByVal strFileNameHash As String)

        Dim objMaster As GCMSMTMaster
        Dim sWriter As StreamWriter = Nothing
        Dim strBuilder As System.Text.StringBuilder = Nothing

        Try
            objMaster = ContainerForm.PGCMSMTMaster()
            sWriter = New StreamWriter(strDirName & strFileName, False)

            For Each objTransDet As GCMSMTTransactionDetail In PListMaster
                If objTransDet IsNot Nothing Then
                    strBuilder = New System.Text.StringBuilder("")
                    strBuilder.Append("""" & objTransDet.SettlementAccNo & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & objTransDet.ValueDate & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & objTransDet.CustomerReference & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & objTransDet.SectorSelection & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & objTransDet.Currency & """")
                    strBuilder.Append(",")
                    'strBuilder.Append("""" & GetRemitAmountFormat(objTransDet.RemitAmount, objTransDet.Currency) & """")
                    strBuilder.Append("""" & objTransDet.RemitAmount & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.ExchangeMethod & " ").Trim & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.ContractNo & " ").Trim & """")
                    strBuilder.Append(",")

                    If objTransDet.IntermediaryBankOptionA = True Then
                        strBuilder.Append("""" & ContainerForm.PSavedOption_A)
                        strBuilder.Append(((objTransDet.IntermediaryBank & Strings.Space(35)).Substring(0, 35)).Trim & """")
                    ElseIf objTransDet.IntermediaryBankOptionC = True Then
                        strBuilder.Append("""" & ContainerForm.PSavedOption_C)
                        strBuilder.Append(((objTransDet.IntermediaryBank & Strings.Space(35)).Substring(0, 35)).Trim & """")
                    ElseIf objTransDet.IntermediaryBankOptionD = True Then
                        strBuilder.Append("""" & (objTransDet.IntermediaryBank & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.IntermediaryBranch & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.IntermediaryBankAddress1 & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.IntermediaryBankAddress2 & Strings.Space(35)).Substring(0, 35) & """")
                    Else
                        strBuilder.Append("""""")
                    End If

                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.IntermediaryBankMasterCode & " ").Trim & """")
                    strBuilder.Append(",")

                    If objTransDet.BeneficiaryBankOptionA = True Then
                        strBuilder.Append("""" & ContainerForm.PSavedOption_A)
                        strBuilder.Append(((objTransDet.BeneficiaryBank & Strings.Space(35)).Substring(0, 35)).Trim & """")
                    ElseIf objTransDet.BeneficiaryBankOptionC = True Then
                        strBuilder.Append("""" & ContainerForm.PSavedOption_C)
                        strBuilder.Append(((objTransDet.BeneficiaryBank & Strings.Space(35)).Substring(0, 35)).Trim & """")
                    ElseIf objTransDet.BeneficiaryBankOptionD = True Then
                        strBuilder.Append("""" & (objTransDet.BeneficiaryBank & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.BeneficiaryBranch & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.BeneficiaryBankAddress1 & Strings.Space(35)).Substring(0, 35))
                        strBuilder.Append((objTransDet.BeneficiaryBankAddress2 & Strings.Space(35)).Substring(0, 35) & """")
                    Else
                        strBuilder.Append("""""")
                    End If

                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryBankMasterCode & " ").Trim & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryAccNo & " ").Trim & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryName1 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryName2 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryName3 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryName4 & Strings.Space(35)).Substring(0, 35) & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryMsg1 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryMsg2 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryMsg3 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryMsg4 & Strings.Space(35)).Substring(0, 35) & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryRemitPurpose & " ").Trim & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.BeneficiaryRemitInfo1 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryRemitInfo2 & Strings.Space(35)).Substring(0, 35))
                    strBuilder.Append((objTransDet.BeneficiaryRemitInfo3 & Strings.Space(objMaster.PMaxNameLength)).Substring(0, 35) & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & objTransDet.BankCharges & """")
                    strBuilder.Append(",")
                    strBuilder.Append("""" & (objTransDet.ChargesAcc & " ").Trim & """")

                    sWriter.WriteLine(strBuilder)
                End If
            Next

            'Create hash file
            If strBuilder IsNot Nothing AndAlso strBuilder.Length > 0 Then
                If sWriter IsNot Nothing Then sWriter.Close()
                Dim allText As String = File.ReadAllText(strDirName & strFileName)
                Dim allHashText As String = HelperModule.ComputeHashAsHexString(allText, "SHA512")
                sWriter = New StreamWriter(strDirName & strFileNameHash, False)
                sWriter.WriteLine(allHashText)
            End If

        Catch fnfEx As FileNotFoundException
            Throw New MagicException(fnfEx.Message)
        Catch MagicEx As MagicException
            Throw New MagicException(MagicEx.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        Finally
            If sWriter IsNot Nothing Then
                sWriter.Close()
            End If
        End Try

    End Sub

    ''' <summary>
    ''' Perform upload action. 
    ''' Call save to file function to perform saving 
    ''' Call check and move old file function to check and move existing file to archive 
    ''' folder if new file name is the same as old file name
    ''' </summary>
    ''' <param name="strDirName">Directory name where the file is saved</param>
    ''' <param name="strFileName">Standard file name</param>
    ''' <param name="strFileNameHash">Hash file name</param>
    ''' <remarks></remarks>
    Public Sub SaveToFileAndUpload(ByVal strDirName As String, ByVal strFileName As String, ByVal strFileNameHash As String)

        Try

            CheckAndMoveOldFile(strDirName, strFileName, strFileNameHash) 'Move to archive
            SaveToFile(ContainerForm.PUploadFilePath, strFileName, strFileNameHash)

        Catch MagicEx As MagicException
            Throw New MagicException(MagicEx.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Check, rename and move existing file to archive folder
    ''' </summary>
    ''' <param name="strDirName">Directory name</param>
    ''' <param name="strFileName">Standard file name</param>
    ''' <param name="strFileNameHash">Hash file name</param>
    ''' <remarks></remarks>
    Public Sub CheckAndMoveOldFile(ByVal strDirName As String, ByVal strFileName As String, ByVal strFileNameHash As String)

        Dim fInfo As FileInfo = Nothing
        Dim strNewFileName As String
        Dim strNewFileNameHash As String
        Dim strFileNameArr As String()
        Dim strDateTime As String

        Try
            strDateTime = DateTime.Now.ToString("yyyyMMddHHmmss")

            strNewFileName = strFileName
            If strFileName.Contains(".") Then
                strFileNameArr = strFileName.Split(".")
                If strFileNameArr.Length > 0 Then strNewFileName = strFileNameArr(0)
            End If
            strNewFileName = strNewFileName & "_" & strDateTime & ContainerForm.PTxtFileExt

            strNewFileNameHash = strFileNameHash
            If strFileNameHash.Contains(".") Then
                strFileNameArr = strFileNameHash.Split(".")
                If strNewFileNameHash.Length > 0 Then strNewFileNameHash = strFileNameArr(0)
            End If
            strNewFileNameHash = strNewFileNameHash & "_" & strDateTime & ContainerForm.PTxtFileExt

            If File.Exists(strDirName & strFileName) Then
                fInfo = New FileInfo(strDirName & strFileName)
                fInfo.MoveTo(ContainerForm.PArchiveFilePath & strNewFileName)
            End If

            If File.Exists(strDirName & strFileNameHash) Then
                fInfo = New FileInfo(strDirName & strFileNameHash)
                fInfo.MoveTo(ContainerForm.PArchiveFilePath & strNewFileNameHash)
            End If

        Catch fnfEx As FileNotFoundException
            Throw New MagicException(fnfEx.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Check for edit/save and upload directories existance
    ''' Directory will be created if it is not found
    ''' </summary>
    ''' <param name="strActionType">Save type: Upload or Save</param>
    ''' <remarks></remarks>
    Public Sub CheckSaveFoldersExistance(ByVal strActionType As Integer)

        Dim foldersNotFoundMessage As String = ""

        Try

            If Not (Directory.Exists(ContainerForm.PSavedFilePath)) Then
                Directory.CreateDirectory(ContainerForm.PSavedFilePath)
                foldersNotFoundMessage &= msgReader.GetString("E04000010") & vbCrLf '--
            End If

            If strActionType = eListAction.UPLOAD Then
                If Not (Directory.Exists(ContainerForm.PUploadFilePath)) Then
                    Directory.CreateDirectory(ContainerForm.PUploadFilePath)
                    foldersNotFoundMessage &= msgReader.GetString("E04000020") & vbCrLf '--
                End If

                If Not (Directory.Exists(ContainerForm.PArchiveFilePath)) Then
                    Directory.CreateDirectory(ContainerForm.PArchiveFilePath)
                    foldersNotFoundMessage &= msgReader.GetString("E04000030") & vbCrLf '--
                End If
            End If

            If foldersNotFoundMessage.Trim <> "" Then
                MessageBox.Show(foldersNotFoundMessage, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Format remittance amount with decimal places based on currency
    ''' </summary>
    ''' <param name="strRemitAmount">Remittance amount</param>
    ''' <param name="strCurrencyCode">Currency code</param>
    ''' <returns>Formatted remittance amount</returns>
    ''' <remarks></remarks>
    Public Function GetRemitAmountFormat(ByVal strRemitAmount As String, ByVal strCurrencyCode As String) As String

        Dim tmpRemitAmount As String = strRemitAmount
        Dim tmpDecimalPoint As Short = HelperModule.GetCurrencyDecimalPoint(strCurrencyCode)

        Try
            If strRemitAmount.Trim = "" Then
                tmpRemitAmount = strRemitAmount
            Else
                tmpRemitAmount = (CDbl(tmpRemitAmount) - 10 ^ -(tmpDecimalPoint + 1)).ToString("F" & tmpDecimalPoint)
            End If

            Return tmpRemitAmount

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try
    End Function

End Class
