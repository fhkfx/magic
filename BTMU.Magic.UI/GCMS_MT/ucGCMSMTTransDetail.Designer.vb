<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucGCMSMTTransDetail
     Inherits BTMU.MAGIC.UI.ucGCMSMTBase
    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TemplateIDLabel As System.Windows.Forms.Label
        Dim SettlementAccNoAndNameLabel As System.Windows.Forms.Label
        Dim SettlementAccNoLabel As System.Windows.Forms.Label
        Dim SettlementAccName1Label As System.Windows.Forms.Label
        Dim SettlementBankLabel As System.Windows.Forms.Label
        Dim SettlementBranchLabel As System.Windows.Forms.Label
        Dim SettlementBankAddress1Label As System.Windows.Forms.Label
        Dim ValueDateLabel As System.Windows.Forms.Label
        Dim CustomerReferenceLabel As System.Windows.Forms.Label
        Dim SectorSelectionLabel As System.Windows.Forms.Label
        Dim CurrencyLabel As System.Windows.Forms.Label
        Dim RemitAmountLabel As System.Windows.Forms.Label
        Dim ExchangeMethodLabel As System.Windows.Forms.Label
        Dim ContractNoLabel As System.Windows.Forms.Label
        Dim IntermediaryBankAndBranchLabel As System.Windows.Forms.Label
        Dim IntermediaryBankLabel As System.Windows.Forms.Label
        Dim IntermediaryBankMasterCodeLabel As System.Windows.Forms.Label
        Dim BeneficiaryBankAndBranchLabel As System.Windows.Forms.Label
        Dim BeneficiaryBankLabel As System.Windows.Forms.Label
        Dim BeneficiaryBankMasterCodeLabel As System.Windows.Forms.Label
        Dim BeneficiaryAccNoAndNameLabel As System.Windows.Forms.Label
        Dim BeneficiaryAccNoLabel As System.Windows.Forms.Label
        Dim BeneficiaryName1Label As System.Windows.Forms.Label
        Dim BeneficiaryMsg1Label As System.Windows.Forms.Label
        Dim BeneficiaryRemitPurposeLabel As System.Windows.Forms.Label
        Dim BeneficiaryRemitInfo1Label As System.Windows.Forms.Label
        Dim BankChargesLabel As System.Windows.Forms.Label
        Dim ChargesAccNoAndNameLabel As System.Windows.Forms.Label
        Dim ChargesAccName1Label As System.Windows.Forms.Label
        Dim ChargesAccLabel As System.Windows.Forms.Label
        Me.TransDetailPanel = New System.Windows.Forms.Panel
        Me.OthersDetGroupBox = New System.Windows.Forms.GroupBox
        Me.ChargesAccName4ValueLabel = New System.Windows.Forms.TextBox
        Me.GCMSMTTransactionDetailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChargesAccName3ValueLabel = New System.Windows.Forms.TextBox
        Me.ChargesAccName2ValueLabel = New System.Windows.Forms.TextBox
        Me.ChargesAccName1ValueLabel = New System.Windows.Forms.TextBox
        Me.ChargesAccValueLabel = New System.Windows.Forms.TextBox
        Me.ChargesAccNoAndNameComboBox = New System.Windows.Forms.ComboBox
        Me.PChargesAccountDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PSettlementDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GCMSMTMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BankChargesComboBox = New System.Windows.Forms.ComboBox
        Me.PBankChargesDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BeneficiaryRemitInfo3TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryRemitInfo2TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryRemitInfo1TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryRemitPurposeTextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryDetGroupBox = New System.Windows.Forms.GroupBox
        Me.BeneficiaryBankMasterCodeValueLabel = New System.Windows.Forms.TextBox
        Me.BeneficiaryMsg4TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryMsg3TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryMsg2TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryMsg1TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryName4TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryName3TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryName2TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryName1TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryAccNoTextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryAccNoAndNameComboBox = New System.Windows.Forms.ComboBox
        Me.PBeneficiaryCustDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BeneficiaryOptionGroupBox = New System.Windows.Forms.GroupBox
        Me.BeneficiaryBankOptionCRadioButton = New System.Windows.Forms.RadioButton
        Me.BeneficiaryBankOptionARadioButton = New System.Windows.Forms.RadioButton
        Me.BeneficiaryBankOptionDRadioButton = New System.Windows.Forms.RadioButton
        Me.BeneficiaryBankAddress2TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryBankAddress1TextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryBranchTextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryBankTextBox = New System.Windows.Forms.TextBox
        Me.BeneficiaryBankAndBranchComboBox = New System.Windows.Forms.ComboBox
        Me.PBeneficiaryBankDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IntermediaryDetGroupBox = New System.Windows.Forms.GroupBox
        Me.IntermediaryBankMasterCodeValueLabel = New System.Windows.Forms.TextBox
        Me.IntermediaryOptionGroupBox = New System.Windows.Forms.GroupBox
        Me.IntermediaryBankOptionDRadioButton = New System.Windows.Forms.RadioButton
        Me.IntermediaryBankOptionCRadioButton = New System.Windows.Forms.RadioButton
        Me.IntermediaryBankOptionARadioButton = New System.Windows.Forms.RadioButton
        Me.IntermediaryBankAddress2TextBox = New System.Windows.Forms.TextBox
        Me.IntermediaryBankAddress1TextBox = New System.Windows.Forms.TextBox
        Me.IntermediaryBranchTextBox = New System.Windows.Forms.TextBox
        Me.IntermediaryBankTextBox = New System.Windows.Forms.TextBox
        Me.IntermediaryBankAndBranchComboBox = New System.Windows.Forms.ComboBox
        Me.PIntermediaryDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RemittanceDetGroupBox = New System.Windows.Forms.GroupBox
        Me.ContractNoTextBox = New System.Windows.Forms.TextBox
        Me.ExchangeMethodComboBox = New System.Windows.Forms.ComboBox
        Me.PExchangeMethodArrBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RemitAmountTextBox = New System.Windows.Forms.TextBox
        Me.CurrencyComboBox = New System.Windows.Forms.ComboBox
        Me.PCurrencyDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SectorSelectionComboBox = New System.Windows.Forms.ComboBox
        Me.PSectorSelectionArrBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerReferenceTextBox = New System.Windows.Forms.TextBox
        Me.ValueDateInfoLabel = New System.Windows.Forms.Label
        Me.ValueDateTextBox = New System.Windows.Forms.TextBox
        Me.SettlementDetGroupBox = New System.Windows.Forms.GroupBox
        Me.SettlementBankAddress2ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementBankAddress1ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementBranchValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementBankValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccName4ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccName3ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccName2ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccName1ValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccNoValueLabel = New System.Windows.Forms.TextBox
        Me.SettlementAccNoAndNameComboBox = New System.Windows.Forms.ComboBox
        Me.TemplateIDComboBox = New System.Windows.Forms.ComboBox
        Me.PTransactionTemplateDTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.TransDetailError = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.ButtonGroupBox = New System.Windows.Forms.GroupBox
        Me.btnPanel = New System.Windows.Forms.Panel
        TemplateIDLabel = New System.Windows.Forms.Label
        SettlementAccNoAndNameLabel = New System.Windows.Forms.Label
        SettlementAccNoLabel = New System.Windows.Forms.Label
        SettlementAccName1Label = New System.Windows.Forms.Label
        SettlementBankLabel = New System.Windows.Forms.Label
        SettlementBranchLabel = New System.Windows.Forms.Label
        SettlementBankAddress1Label = New System.Windows.Forms.Label
        ValueDateLabel = New System.Windows.Forms.Label
        CustomerReferenceLabel = New System.Windows.Forms.Label
        SectorSelectionLabel = New System.Windows.Forms.Label
        CurrencyLabel = New System.Windows.Forms.Label
        RemitAmountLabel = New System.Windows.Forms.Label
        ExchangeMethodLabel = New System.Windows.Forms.Label
        ContractNoLabel = New System.Windows.Forms.Label
        IntermediaryBankAndBranchLabel = New System.Windows.Forms.Label
        IntermediaryBankLabel = New System.Windows.Forms.Label
        IntermediaryBankMasterCodeLabel = New System.Windows.Forms.Label
        BeneficiaryBankAndBranchLabel = New System.Windows.Forms.Label
        BeneficiaryBankLabel = New System.Windows.Forms.Label
        BeneficiaryBankMasterCodeLabel = New System.Windows.Forms.Label
        BeneficiaryAccNoAndNameLabel = New System.Windows.Forms.Label
        BeneficiaryAccNoLabel = New System.Windows.Forms.Label
        BeneficiaryName1Label = New System.Windows.Forms.Label
        BeneficiaryMsg1Label = New System.Windows.Forms.Label
        BeneficiaryRemitPurposeLabel = New System.Windows.Forms.Label
        BeneficiaryRemitInfo1Label = New System.Windows.Forms.Label
        BankChargesLabel = New System.Windows.Forms.Label
        ChargesAccNoAndNameLabel = New System.Windows.Forms.Label
        ChargesAccName1Label = New System.Windows.Forms.Label
        ChargesAccLabel = New System.Windows.Forms.Label
        Me.TransDetailPanel.SuspendLayout()
        Me.OthersDetGroupBox.SuspendLayout()
        CType(Me.GCMSMTTransactionDetailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PChargesAccountDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSettlementDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GCMSMTMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBankChargesDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryDetGroupBox.SuspendLayout()
        CType(Me.PBeneficiaryCustDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryOptionGroupBox.SuspendLayout()
        CType(Me.PBeneficiaryBankDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.IntermediaryDetGroupBox.SuspendLayout()
        Me.IntermediaryOptionGroupBox.SuspendLayout()
        CType(Me.PIntermediaryDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RemittanceDetGroupBox.SuspendLayout()
        CType(Me.PExchangeMethodArrBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCurrencyDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSectorSelectionArrBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SettlementDetGroupBox.SuspendLayout()
        CType(Me.PTransactionTemplateDTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransDetailError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ButtonGroupBox.SuspendLayout()
        Me.btnPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'TemplateIDLabel
        '
        TemplateIDLabel.AutoSize = True
        TemplateIDLabel.Location = New System.Drawing.Point(36, 25)
        TemplateIDLabel.Name = "TemplateIDLabel"
        TemplateIDLabel.Size = New System.Drawing.Size(65, 14)
        TemplateIDLabel.TabIndex = 7
        TemplateIDLabel.Text = "Template ID:"
        '
        'SettlementAccNoAndNameLabel
        '
        SettlementAccNoAndNameLabel.AutoSize = True
        SettlementAccNoAndNameLabel.Location = New System.Drawing.Point(72, 27)
        SettlementAccNoAndNameLabel.Name = "SettlementAccNoAndNameLabel"
        SettlementAccNoAndNameLabel.Size = New System.Drawing.Size(145, 14)
        SettlementAccNoAndNameLabel.TabIndex = 0
        SettlementAccNoAndNameLabel.Text = "* Settlement Acc No / Name :"
        '
        'SettlementAccNoLabel
        '
        SettlementAccNoLabel.AutoSize = True
        SettlementAccNoLabel.Location = New System.Drawing.Point(118, 50)
        SettlementAccNoLabel.Name = "SettlementAccNoLabel"
        SettlementAccNoLabel.Size = New System.Drawing.Size(102, 14)
        SettlementAccNoLabel.TabIndex = 2
        SettlementAccNoLabel.Text = "Settlement Acc No :"
        '
        'SettlementAccName1Label
        '
        SettlementAccName1Label.AutoSize = True
        SettlementAccName1Label.Location = New System.Drawing.Point(179, 73)
        SettlementAccName1Label.Name = "SettlementAccName1Label"
        SettlementAccName1Label.Size = New System.Drawing.Size(40, 14)
        SettlementAccName1Label.TabIndex = 4
        SettlementAccName1Label.Text = "Name :"
        '
        'SettlementBankLabel
        '
        SettlementBankLabel.AutoSize = True
        SettlementBankLabel.Location = New System.Drawing.Point(129, 165)
        SettlementBankLabel.Name = "SettlementBankLabel"
        SettlementBankLabel.Size = New System.Drawing.Size(90, 14)
        SettlementBankLabel.TabIndex = 12
        SettlementBankLabel.Text = "Settlement Bank :"
        '
        'SettlementBranchLabel
        '
        SettlementBranchLabel.AutoSize = True
        SettlementBranchLabel.Location = New System.Drawing.Point(172, 188)
        SettlementBranchLabel.Name = "SettlementBranchLabel"
        SettlementBranchLabel.Size = New System.Drawing.Size(48, 14)
        SettlementBranchLabel.TabIndex = 14
        SettlementBranchLabel.Text = "Branch :"
        '
        'SettlementBankAddress1Label
        '
        SettlementBankAddress1Label.AutoSize = True
        SettlementBankAddress1Label.Location = New System.Drawing.Point(169, 211)
        SettlementBankAddress1Label.Name = "SettlementBankAddress1Label"
        SettlementBankAddress1Label.Size = New System.Drawing.Size(55, 14)
        SettlementBankAddress1Label.TabIndex = 16
        SettlementBankAddress1Label.Text = "Address :"
        '
        'ValueDateLabel
        '
        ValueDateLabel.AutoSize = True
        ValueDateLabel.Location = New System.Drawing.Point(147, 29)
        ValueDateLabel.Name = "ValueDateLabel"
        ValueDateLabel.Size = New System.Drawing.Size(73, 14)
        ValueDateLabel.TabIndex = 0
        ValueDateLabel.Text = "* Value Date :"
        '
        'CustomerReferenceLabel
        '
        CustomerReferenceLabel.AutoSize = True
        CustomerReferenceLabel.Location = New System.Drawing.Point(103, 54)
        CustomerReferenceLabel.Name = "CustomerReferenceLabel"
        CustomerReferenceLabel.Size = New System.Drawing.Size(120, 14)
        CustomerReferenceLabel.TabIndex = 3
        CustomerReferenceLabel.Text = "* Customer Reference :"
        '
        'SectorSelectionLabel
        '
        SectorSelectionLabel.AutoSize = True
        SectorSelectionLabel.Location = New System.Drawing.Point(122, 81)
        SectorSelectionLabel.Name = "SectorSelectionLabel"
        SectorSelectionLabel.Size = New System.Drawing.Size(99, 14)
        SectorSelectionLabel.TabIndex = 5
        SectorSelectionLabel.Text = "* Sector Selection :"
        '
        'CurrencyLabel
        '
        CurrencyLabel.AutoSize = True
        CurrencyLabel.Location = New System.Drawing.Point(158, 108)
        CurrencyLabel.Name = "CurrencyLabel"
        CurrencyLabel.Size = New System.Drawing.Size(65, 14)
        CurrencyLabel.TabIndex = 7
        CurrencyLabel.Text = "* Currency :"
        '
        'RemitAmountLabel
        '
        RemitAmountLabel.AutoSize = True
        RemitAmountLabel.Location = New System.Drawing.Point(107, 134)
        RemitAmountLabel.Name = "RemitAmountLabel"
        RemitAmountLabel.Size = New System.Drawing.Size(113, 14)
        RemitAmountLabel.TabIndex = 9
        RemitAmountLabel.Text = "* Remittance Amount :"
        '
        'ExchangeMethodLabel
        '
        ExchangeMethodLabel.AutoSize = True
        ExchangeMethodLabel.Location = New System.Drawing.Point(120, 161)
        ExchangeMethodLabel.Name = "ExchangeMethodLabel"
        ExchangeMethodLabel.Size = New System.Drawing.Size(99, 14)
        ExchangeMethodLabel.TabIndex = 11
        ExchangeMethodLabel.Text = "Exchange Method :"
        '
        'ContractNoLabel
        '
        ContractNoLabel.AutoSize = True
        ContractNoLabel.Location = New System.Drawing.Point(150, 188)
        ContractNoLabel.Name = "ContractNoLabel"
        ContractNoLabel.Size = New System.Drawing.Size(70, 14)
        ContractNoLabel.TabIndex = 13
        ContractNoLabel.Text = "Contract No :"
        '
        'IntermediaryBankAndBranchLabel
        '
        IntermediaryBankAndBranchLabel.AutoSize = True
        IntermediaryBankAndBranchLabel.Location = New System.Drawing.Point(80, 24)
        IntermediaryBankAndBranchLabel.Name = "IntermediaryBankAndBranchLabel"
        IntermediaryBankAndBranchLabel.Size = New System.Drawing.Size(146, 14)
        IntermediaryBankAndBranchLabel.TabIndex = 0
        IntermediaryBankAndBranchLabel.Text = "Intermediary Bank / Branch : "
        '
        'IntermediaryBankLabel
        '
        IntermediaryBankLabel.AutoSize = True
        IntermediaryBankLabel.Location = New System.Drawing.Point(31, 50)
        IntermediaryBankLabel.Name = "IntermediaryBankLabel"
        IntermediaryBankLabel.Size = New System.Drawing.Size(197, 14)
        IntermediaryBankLabel.TabIndex = 2
        IntermediaryBankLabel.Text = "Intermediary Bank / Branch / Address : "
        '
        'IntermediaryBankMasterCodeLabel
        '
        IntermediaryBankMasterCodeLabel.AutoSize = True
        IntermediaryBankMasterCodeLabel.Location = New System.Drawing.Point(62, 170)
        IntermediaryBankMasterCodeLabel.Name = "IntermediaryBankMasterCodeLabel"
        IntermediaryBankMasterCodeLabel.Size = New System.Drawing.Size(163, 14)
        IntermediaryBankMasterCodeLabel.TabIndex = 10
        IntermediaryBankMasterCodeLabel.Text = "Intermediary Bank Master Code :"
        '
        'BeneficiaryBankAndBranchLabel
        '
        BeneficiaryBankAndBranchLabel.AutoSize = True
        BeneficiaryBankAndBranchLabel.Location = New System.Drawing.Point(82, 27)
        BeneficiaryBankAndBranchLabel.Name = "BeneficiaryBankAndBranchLabel"
        BeneficiaryBankAndBranchLabel.Size = New System.Drawing.Size(139, 14)
        BeneficiaryBankAndBranchLabel.TabIndex = 0
        BeneficiaryBankAndBranchLabel.Text = "Beneficiary Bank / Branch :"
        '
        'BeneficiaryBankLabel
        '
        BeneficiaryBankLabel.AutoSize = True
        BeneficiaryBankLabel.Location = New System.Drawing.Point(33, 53)
        BeneficiaryBankLabel.Name = "BeneficiaryBankLabel"
        BeneficiaryBankLabel.Size = New System.Drawing.Size(190, 14)
        BeneficiaryBankLabel.TabIndex = 2
        BeneficiaryBankLabel.Text = "Beneficiary Bank / Branch / Address :"
        '
        'BeneficiaryBankMasterCodeLabel
        '
        BeneficiaryBankMasterCodeLabel.AutoSize = True
        BeneficiaryBankMasterCodeLabel.Location = New System.Drawing.Point(64, 175)
        BeneficiaryBankMasterCodeLabel.Name = "BeneficiaryBankMasterCodeLabel"
        BeneficiaryBankMasterCodeLabel.Size = New System.Drawing.Size(159, 14)
        BeneficiaryBankMasterCodeLabel.TabIndex = 10
        BeneficiaryBankMasterCodeLabel.Text = "Beneficiary Bank Master Code :"
        '
        'BeneficiaryAccNoAndNameLabel
        '
        BeneficiaryAccNoAndNameLabel.AutoSize = True
        BeneficiaryAccNoAndNameLabel.Location = New System.Drawing.Point(77, 202)
        BeneficiaryAccNoAndNameLabel.Name = "BeneficiaryAccNoAndNameLabel"
        BeneficiaryAccNoAndNameLabel.Size = New System.Drawing.Size(143, 14)
        BeneficiaryAccNoAndNameLabel.TabIndex = 11
        BeneficiaryAccNoAndNameLabel.Text = "Beneficiary Acc No / Name :"
        '
        'BeneficiaryAccNoLabel
        '
        BeneficiaryAccNoLabel.AutoSize = True
        BeneficiaryAccNoLabel.Location = New System.Drawing.Point(115, 228)
        BeneficiaryAccNoLabel.Name = "BeneficiaryAccNoLabel"
        BeneficiaryAccNoLabel.Size = New System.Drawing.Size(107, 14)
        BeneficiaryAccNoLabel.TabIndex = 12
        BeneficiaryAccNoLabel.Text = "Beneficiary Acc No :"
        '
        'BeneficiaryName1Label
        '
        BeneficiaryName1Label.AutoSize = True
        BeneficiaryName1Label.Location = New System.Drawing.Point(67, 256)
        BeneficiaryName1Label.Name = "BeneficiaryName1Label"
        BeneficiaryName1Label.Size = New System.Drawing.Size(156, 14)
        BeneficiaryName1Label.TabIndex = 14
        BeneficiaryName1Label.Text = "* Beneficiary Name / Address :"
        '
        'BeneficiaryMsg1Label
        '
        BeneficiaryMsg1Label.AutoSize = True
        BeneficiaryMsg1Label.Location = New System.Drawing.Point(92, 346)
        BeneficiaryMsg1Label.Name = "BeneficiaryMsg1Label"
        BeneficiaryMsg1Label.Size = New System.Drawing.Size(127, 14)
        BeneficiaryMsg1Label.TabIndex = 21
        BeneficiaryMsg1Label.Text = "Message to Beneficiary :"
        '
        'BeneficiaryRemitPurposeLabel
        '
        BeneficiaryRemitPurposeLabel.AutoSize = True
        BeneficiaryRemitPurposeLabel.Location = New System.Drawing.Point(94, 24)
        BeneficiaryRemitPurposeLabel.Name = "BeneficiaryRemitPurposeLabel"
        BeneficiaryRemitPurposeLabel.Size = New System.Drawing.Size(122, 14)
        BeneficiaryRemitPurposeLabel.TabIndex = 0
        BeneficiaryRemitPurposeLabel.Text = "Purpose of Remittance :"
        '
        'BeneficiaryRemitInfo1Label
        '
        BeneficiaryRemitInfo1Label.AutoSize = True
        BeneficiaryRemitInfo1Label.Location = New System.Drawing.Point(53, 57)
        BeneficiaryRemitInfo1Label.Name = "BeneficiaryRemitInfo1Label"
        BeneficiaryRemitInfo1Label.Size = New System.Drawing.Size(161, 14)
        BeneficiaryRemitInfo1Label.TabIndex = 2
        BeneficiaryRemitInfo1Label.Text = "Information to Remittance Bank :"
        '
        'BankChargesLabel
        '
        BankChargesLabel.AutoSize = True
        BankChargesLabel.Location = New System.Drawing.Point(128, 122)
        BankChargesLabel.Name = "BankChargesLabel"
        BankChargesLabel.Size = New System.Drawing.Size(88, 14)
        BankChargesLabel.TabIndex = 6
        BankChargesLabel.Text = "* Bank Charges :"
        '
        'ChargesAccNoAndNameLabel
        '
        ChargesAccNoAndNameLabel.AutoSize = True
        ChargesAccNoAndNameLabel.Location = New System.Drawing.Point(85, 149)
        ChargesAccNoAndNameLabel.Name = "ChargesAccNoAndNameLabel"
        ChargesAccNoAndNameLabel.Size = New System.Drawing.Size(129, 14)
        ChargesAccNoAndNameLabel.TabIndex = 8
        ChargesAccNoAndNameLabel.Text = "Charges Acc No / Name :"
        '
        'ChargesAccName1Label
        '
        ChargesAccName1Label.AutoSize = True
        ChargesAccName1Label.Location = New System.Drawing.Point(110, 199)
        ChargesAccName1Label.Name = "ChargesAccName1Label"
        ChargesAccName1Label.Size = New System.Drawing.Size(107, 14)
        ChargesAccName1Label.TabIndex = 10
        ChargesAccName1Label.Text = "Charges Acc Name :"
        '
        'ChargesAccLabel
        '
        ChargesAccLabel.AutoSize = True
        ChargesAccLabel.Location = New System.Drawing.Point(124, 174)
        ChargesAccLabel.Name = "ChargesAccLabel"
        ChargesAccLabel.Size = New System.Drawing.Size(93, 14)
        ChargesAccLabel.TabIndex = 12
        ChargesAccLabel.Text = "Charges Acc No :"
        '
        'TransDetailPanel
        '
        Me.TransDetailPanel.AutoScroll = True
        Me.TransDetailPanel.Controls.Add(Me.OthersDetGroupBox)
        Me.TransDetailPanel.Controls.Add(Me.BeneficiaryDetGroupBox)
        Me.TransDetailPanel.Controls.Add(Me.IntermediaryDetGroupBox)
        Me.TransDetailPanel.Controls.Add(Me.RemittanceDetGroupBox)
        Me.TransDetailPanel.Controls.Add(Me.SettlementDetGroupBox)
        Me.TransDetailPanel.Controls.Add(TemplateIDLabel)
        Me.TransDetailPanel.Controls.Add(Me.TemplateIDComboBox)
        Me.TransDetailPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TransDetailPanel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TransDetailPanel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TransDetailPanel.Location = New System.Drawing.Point(5, 5)
        Me.TransDetailPanel.Name = "TransDetailPanel"
        Me.TransDetailPanel.Size = New System.Drawing.Size(766, 1523)
        Me.TransDetailPanel.TabIndex = 0
        '
        'OthersDetGroupBox
        '
        Me.OthersDetGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccName4ValueLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccName3ValueLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccName2ValueLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccName1ValueLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccValueLabel)
        Me.OthersDetGroupBox.Controls.Add(ChargesAccLabel)
        Me.OthersDetGroupBox.Controls.Add(ChargesAccName1Label)
        Me.OthersDetGroupBox.Controls.Add(ChargesAccNoAndNameLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.ChargesAccNoAndNameComboBox)
        Me.OthersDetGroupBox.Controls.Add(BankChargesLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.BankChargesComboBox)
        Me.OthersDetGroupBox.Controls.Add(Me.BeneficiaryRemitInfo3TextBox)
        Me.OthersDetGroupBox.Controls.Add(Me.BeneficiaryRemitInfo2TextBox)
        Me.OthersDetGroupBox.Controls.Add(BeneficiaryRemitInfo1Label)
        Me.OthersDetGroupBox.Controls.Add(Me.BeneficiaryRemitInfo1TextBox)
        Me.OthersDetGroupBox.Controls.Add(BeneficiaryRemitPurposeLabel)
        Me.OthersDetGroupBox.Controls.Add(Me.BeneficiaryRemitPurposeTextBox)
        Me.OthersDetGroupBox.Location = New System.Drawing.Point(36, 1200)
        Me.OthersDetGroupBox.Name = "OthersDetGroupBox"
        Me.OthersDetGroupBox.Size = New System.Drawing.Size(696, 298)
        Me.OthersDetGroupBox.TabIndex = 6
        Me.OthersDetGroupBox.TabStop = False
        Me.OthersDetGroupBox.Text = "Others : "
        '
        'ChargesAccName4ValueLabel
        '
        Me.ChargesAccName4ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ChargesAccName4", True))
        Me.ChargesAccName4ValueLabel.Enabled = False
        Me.ChargesAccName4ValueLabel.Location = New System.Drawing.Point(237, 261)
        Me.ChargesAccName4ValueLabel.Name = "ChargesAccName4ValueLabel"
        Me.ChargesAccName4ValueLabel.ReadOnly = True
        Me.ChargesAccName4ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.ChargesAccName4ValueLabel.TabIndex = 17
        '
        'GCMSMTTransactionDetailBindingSource
        '
        Me.GCMSMTTransactionDetailBindingSource.DataSource = GetType(BTMU.Magic.GCMS_MT.GCMSMTTransactionDetail)
        '
        'ChargesAccName3ValueLabel
        '
        Me.ChargesAccName3ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ChargesAccName3", True))
        Me.ChargesAccName3ValueLabel.Enabled = False
        Me.ChargesAccName3ValueLabel.Location = New System.Drawing.Point(237, 238)
        Me.ChargesAccName3ValueLabel.Name = "ChargesAccName3ValueLabel"
        Me.ChargesAccName3ValueLabel.ReadOnly = True
        Me.ChargesAccName3ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.ChargesAccName3ValueLabel.TabIndex = 16
        '
        'ChargesAccName2ValueLabel
        '
        Me.ChargesAccName2ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ChargesAccName2", True))
        Me.ChargesAccName2ValueLabel.Enabled = False
        Me.ChargesAccName2ValueLabel.Location = New System.Drawing.Point(237, 215)
        Me.ChargesAccName2ValueLabel.Name = "ChargesAccName2ValueLabel"
        Me.ChargesAccName2ValueLabel.ReadOnly = True
        Me.ChargesAccName2ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.ChargesAccName2ValueLabel.TabIndex = 15
        '
        'ChargesAccName1ValueLabel
        '
        Me.ChargesAccName1ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ChargesAccName1", True))
        Me.ChargesAccName1ValueLabel.Enabled = False
        Me.ChargesAccName1ValueLabel.Location = New System.Drawing.Point(237, 192)
        Me.ChargesAccName1ValueLabel.Name = "ChargesAccName1ValueLabel"
        Me.ChargesAccName1ValueLabel.ReadOnly = True
        Me.ChargesAccName1ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.ChargesAccName1ValueLabel.TabIndex = 11
        '
        'ChargesAccValueLabel
        '
        Me.ChargesAccValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ChargesAcc", True))
        Me.ChargesAccValueLabel.Enabled = False
        Me.ChargesAccValueLabel.Location = New System.Drawing.Point(237, 168)
        Me.ChargesAccValueLabel.Name = "ChargesAccValueLabel"
        Me.ChargesAccValueLabel.ReadOnly = True
        Me.ChargesAccValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.ChargesAccValueLabel.TabIndex = 13
        '
        'ChargesAccNoAndNameComboBox
        '
        Me.ChargesAccNoAndNameComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "ChargesAccNoAndName", True))
        Me.ChargesAccNoAndNameComboBox.DataSource = Me.PChargesAccountDTBindingSource
        Me.ChargesAccNoAndNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ChargesAccNoAndNameComboBox.FormattingEnabled = True
        Me.ChargesAccNoAndNameComboBox.Location = New System.Drawing.Point(237, 141)
        Me.ChargesAccNoAndNameComboBox.Name = "ChargesAccNoAndNameComboBox"
        Me.ChargesAccNoAndNameComboBox.Size = New System.Drawing.Size(410, 22)
        Me.ChargesAccNoAndNameComboBox.TabIndex = 9
        '
        'PChargesAccountDTBindingSource
        '
        Me.PChargesAccountDTBindingSource.DataSource = Me.PSettlementDTBindingSource
        '
        'PSettlementDTBindingSource
        '
        Me.PSettlementDTBindingSource.DataMember = "PSettlementDT"
        Me.PSettlementDTBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'GCMSMTMasterBindingSource
        '
        Me.GCMSMTMasterBindingSource.DataSource = GetType(BTMU.Magic.GCMS_MT.GCMSMTMaster)
        '
        'BankChargesComboBox
        '
        Me.BankChargesComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "BankCharges", True))
        Me.BankChargesComboBox.DataSource = Me.PBankChargesDTBindingSource
        Me.BankChargesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BankChargesComboBox.FormattingEnabled = True
        Me.BankChargesComboBox.Location = New System.Drawing.Point(237, 114)
        Me.BankChargesComboBox.Name = "BankChargesComboBox"
        Me.BankChargesComboBox.Size = New System.Drawing.Size(145, 22)
        Me.BankChargesComboBox.TabIndex = 7
        '
        'BeneficiaryRemitInfo3TextBox
        '
        Me.BeneficiaryRemitInfo3TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryRemitInfo3", True))
        Me.BeneficiaryRemitInfo3TextBox.Location = New System.Drawing.Point(237, 88)
        Me.BeneficiaryRemitInfo3TextBox.MaxLength = 35
        Me.BeneficiaryRemitInfo3TextBox.Name = "BeneficiaryRemitInfo3TextBox"
        Me.BeneficiaryRemitInfo3TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryRemitInfo3TextBox.TabIndex = 6
        '
        'BeneficiaryRemitInfo2TextBox
        '
        Me.BeneficiaryRemitInfo2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryRemitInfo2", True))
        Me.BeneficiaryRemitInfo2TextBox.Location = New System.Drawing.Point(237, 69)
        Me.BeneficiaryRemitInfo2TextBox.MaxLength = 35
        Me.BeneficiaryRemitInfo2TextBox.Name = "BeneficiaryRemitInfo2TextBox"
        Me.BeneficiaryRemitInfo2TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryRemitInfo2TextBox.TabIndex = 5
        '
        'BeneficiaryRemitInfo1TextBox
        '
        Me.BeneficiaryRemitInfo1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryRemitInfo1", True))
        Me.BeneficiaryRemitInfo1TextBox.Location = New System.Drawing.Point(237, 50)
        Me.BeneficiaryRemitInfo1TextBox.MaxLength = 35
        Me.BeneficiaryRemitInfo1TextBox.Name = "BeneficiaryRemitInfo1TextBox"
        Me.BeneficiaryRemitInfo1TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryRemitInfo1TextBox.TabIndex = 3
        '
        'BeneficiaryRemitPurposeTextBox
        '
        Me.BeneficiaryRemitPurposeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryRemitPurpose", True))
        Me.BeneficiaryRemitPurposeTextBox.Location = New System.Drawing.Point(237, 17)
        Me.BeneficiaryRemitPurposeTextBox.MaxLength = 22
        Me.BeneficiaryRemitPurposeTextBox.Name = "BeneficiaryRemitPurposeTextBox"
        Me.BeneficiaryRemitPurposeTextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryRemitPurposeTextBox.TabIndex = 1
        '
        'BeneficiaryDetGroupBox
        '
        Me.BeneficiaryDetGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBankMasterCodeValueLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryMsg4TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryMsg3TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryMsg2TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryMsg1Label)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryMsg1TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryName4TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryName3TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryName2TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryName1Label)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryName1TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryAccNoLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryAccNoTextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryAccNoAndNameLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryAccNoAndNameComboBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryBankMasterCodeLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryOptionGroupBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBankAddress2TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBankAddress1TextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBranchTextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryBankLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBankTextBox)
        Me.BeneficiaryDetGroupBox.Controls.Add(BeneficiaryBankAndBranchLabel)
        Me.BeneficiaryDetGroupBox.Controls.Add(Me.BeneficiaryBankAndBranchComboBox)
        Me.BeneficiaryDetGroupBox.Location = New System.Drawing.Point(36, 758)
        Me.BeneficiaryDetGroupBox.Name = "BeneficiaryDetGroupBox"
        Me.BeneficiaryDetGroupBox.Size = New System.Drawing.Size(696, 436)
        Me.BeneficiaryDetGroupBox.TabIndex = 5
        Me.BeneficiaryDetGroupBox.TabStop = False
        Me.BeneficiaryDetGroupBox.Text = "Beneficiary : "
        '
        'BeneficiaryBankMasterCodeValueLabel
        '
        Me.BeneficiaryBankMasterCodeValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBankMasterCode", True))
        Me.BeneficiaryBankMasterCodeValueLabel.Enabled = False
        Me.BeneficiaryBankMasterCodeValueLabel.Location = New System.Drawing.Point(237, 168)
        Me.BeneficiaryBankMasterCodeValueLabel.Name = "BeneficiaryBankMasterCodeValueLabel"
        Me.BeneficiaryBankMasterCodeValueLabel.ReadOnly = True
        Me.BeneficiaryBankMasterCodeValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryBankMasterCodeValueLabel.TabIndex = 27
        '
        'BeneficiaryMsg4TextBox
        '
        Me.BeneficiaryMsg4TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryMsg4", True))
        Me.BeneficiaryMsg4TextBox.Location = New System.Drawing.Point(237, 396)
        Me.BeneficiaryMsg4TextBox.MaxLength = 35
        Me.BeneficiaryMsg4TextBox.Name = "BeneficiaryMsg4TextBox"
        Me.BeneficiaryMsg4TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryMsg4TextBox.TabIndex = 26
        '
        'BeneficiaryMsg3TextBox
        '
        Me.BeneficiaryMsg3TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryMsg3", True))
        Me.BeneficiaryMsg3TextBox.Location = New System.Drawing.Point(237, 377)
        Me.BeneficiaryMsg3TextBox.MaxLength = 35
        Me.BeneficiaryMsg3TextBox.Name = "BeneficiaryMsg3TextBox"
        Me.BeneficiaryMsg3TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryMsg3TextBox.TabIndex = 24
        '
        'BeneficiaryMsg2TextBox
        '
        Me.BeneficiaryMsg2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryMsg2", True))
        Me.BeneficiaryMsg2TextBox.Location = New System.Drawing.Point(237, 358)
        Me.BeneficiaryMsg2TextBox.MaxLength = 35
        Me.BeneficiaryMsg2TextBox.Name = "BeneficiaryMsg2TextBox"
        Me.BeneficiaryMsg2TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryMsg2TextBox.TabIndex = 23
        '
        'BeneficiaryMsg1TextBox
        '
        Me.BeneficiaryMsg1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryMsg1", True))
        Me.BeneficiaryMsg1TextBox.Location = New System.Drawing.Point(237, 339)
        Me.BeneficiaryMsg1TextBox.MaxLength = 35
        Me.BeneficiaryMsg1TextBox.Name = "BeneficiaryMsg1TextBox"
        Me.BeneficiaryMsg1TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryMsg1TextBox.TabIndex = 22
        '
        'BeneficiaryName4TextBox
        '
        Me.BeneficiaryName4TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryName4", True))
        Me.BeneficiaryName4TextBox.Location = New System.Drawing.Point(237, 306)
        Me.BeneficiaryName4TextBox.MaxLength = 35
        Me.BeneficiaryName4TextBox.Name = "BeneficiaryName4TextBox"
        Me.BeneficiaryName4TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryName4TextBox.TabIndex = 21
        '
        'BeneficiaryName3TextBox
        '
        Me.BeneficiaryName3TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryName3", True))
        Me.BeneficiaryName3TextBox.Location = New System.Drawing.Point(237, 287)
        Me.BeneficiaryName3TextBox.MaxLength = 35
        Me.BeneficiaryName3TextBox.Name = "BeneficiaryName3TextBox"
        Me.BeneficiaryName3TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryName3TextBox.TabIndex = 19
        '
        'BeneficiaryName2TextBox
        '
        Me.BeneficiaryName2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryName2", True))
        Me.BeneficiaryName2TextBox.Location = New System.Drawing.Point(237, 268)
        Me.BeneficiaryName2TextBox.MaxLength = 35
        Me.BeneficiaryName2TextBox.Name = "BeneficiaryName2TextBox"
        Me.BeneficiaryName2TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryName2TextBox.TabIndex = 17
        '
        'BeneficiaryName1TextBox
        '
        Me.BeneficiaryName1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryName1", True))
        Me.BeneficiaryName1TextBox.Location = New System.Drawing.Point(237, 249)
        Me.BeneficiaryName1TextBox.MaxLength = 35
        Me.BeneficiaryName1TextBox.Name = "BeneficiaryName1TextBox"
        Me.BeneficiaryName1TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryName1TextBox.TabIndex = 15
        '
        'BeneficiaryAccNoTextBox
        '
        Me.BeneficiaryAccNoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryAccNo", True))
        Me.BeneficiaryAccNoTextBox.Location = New System.Drawing.Point(237, 221)
        Me.BeneficiaryAccNoTextBox.MaxLength = 34
        Me.BeneficiaryAccNoTextBox.Name = "BeneficiaryAccNoTextBox"
        Me.BeneficiaryAccNoTextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryAccNoTextBox.TabIndex = 13
        '
        'BeneficiaryAccNoAndNameComboBox
        '
        Me.BeneficiaryAccNoAndNameComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryAccNoAndName", True))
        Me.BeneficiaryAccNoAndNameComboBox.DataSource = Me.PBeneficiaryCustDTBindingSource
        Me.BeneficiaryAccNoAndNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BeneficiaryAccNoAndNameComboBox.FormattingEnabled = True
        Me.BeneficiaryAccNoAndNameComboBox.Location = New System.Drawing.Point(237, 194)
        Me.BeneficiaryAccNoAndNameComboBox.Name = "BeneficiaryAccNoAndNameComboBox"
        Me.BeneficiaryAccNoAndNameComboBox.Size = New System.Drawing.Size(410, 22)
        Me.BeneficiaryAccNoAndNameComboBox.TabIndex = 12
        '
        'PBeneficiaryCustDTBindingSource
        '
        Me.PBeneficiaryCustDTBindingSource.DataMember = "PBeneficiaryCustDT"
        Me.PBeneficiaryCustDTBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'BeneficiaryOptionGroupBox
        '
        Me.BeneficiaryOptionGroupBox.Controls.Add(Me.BeneficiaryBankOptionCRadioButton)
        Me.BeneficiaryOptionGroupBox.Controls.Add(Me.BeneficiaryBankOptionARadioButton)
        Me.BeneficiaryOptionGroupBox.Controls.Add(Me.BeneficiaryBankOptionDRadioButton)
        Me.BeneficiaryOptionGroupBox.Location = New System.Drawing.Point(9, 72)
        Me.BeneficiaryOptionGroupBox.Name = "BeneficiaryOptionGroupBox"
        Me.BeneficiaryOptionGroupBox.Size = New System.Drawing.Size(211, 91)
        Me.BeneficiaryOptionGroupBox.TabIndex = 10
        Me.BeneficiaryOptionGroupBox.TabStop = False
        Me.BeneficiaryOptionGroupBox.Text = "Options : "
        '
        'BeneficiaryBankOptionCRadioButton
        '
        Me.BeneficiaryBankOptionCRadioButton.Location = New System.Drawing.Point(11, 62)
        Me.BeneficiaryBankOptionCRadioButton.Name = "BeneficiaryBankOptionCRadioButton"
        Me.BeneficiaryBankOptionCRadioButton.Size = New System.Drawing.Size(195, 24)
        Me.BeneficiaryBankOptionCRadioButton.TabIndex = 3
        Me.BeneficiaryBankOptionCRadioButton.Text = "C National Clearing Code"
        '
        'BeneficiaryBankOptionARadioButton
        '
        Me.BeneficiaryBankOptionARadioButton.Location = New System.Drawing.Point(11, 39)
        Me.BeneficiaryBankOptionARadioButton.Name = "BeneficiaryBankOptionARadioButton"
        Me.BeneficiaryBankOptionARadioButton.Size = New System.Drawing.Size(195, 24)
        Me.BeneficiaryBankOptionARadioButton.TabIndex = 2
        Me.BeneficiaryBankOptionARadioButton.Text = "A Option SWIFT BIC"
        '
        'BeneficiaryBankOptionDRadioButton
        '
        Me.BeneficiaryBankOptionDRadioButton.Location = New System.Drawing.Point(11, 16)
        Me.BeneficiaryBankOptionDRadioButton.Name = "BeneficiaryBankOptionDRadioButton"
        Me.BeneficiaryBankOptionDRadioButton.Size = New System.Drawing.Size(158, 24)
        Me.BeneficiaryBankOptionDRadioButton.TabIndex = 1
        Me.BeneficiaryBankOptionDRadioButton.Text = "D Option Bank /Branch "
        '
        'BeneficiaryBankAddress2TextBox
        '
        Me.BeneficiaryBankAddress2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBankAddress2", True))
        Me.BeneficiaryBankAddress2TextBox.Location = New System.Drawing.Point(237, 110)
        Me.BeneficiaryBankAddress2TextBox.MaxLength = 35
        Me.BeneficiaryBankAddress2TextBox.Name = "BeneficiaryBankAddress2TextBox"
        Me.BeneficiaryBankAddress2TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryBankAddress2TextBox.TabIndex = 9
        '
        'BeneficiaryBankAddress1TextBox
        '
        Me.BeneficiaryBankAddress1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBankAddress1", True))
        Me.BeneficiaryBankAddress1TextBox.Location = New System.Drawing.Point(237, 91)
        Me.BeneficiaryBankAddress1TextBox.MaxLength = 35
        Me.BeneficiaryBankAddress1TextBox.Name = "BeneficiaryBankAddress1TextBox"
        Me.BeneficiaryBankAddress1TextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryBankAddress1TextBox.TabIndex = 7
        '
        'BeneficiaryBranchTextBox
        '
        Me.BeneficiaryBranchTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBranch", True))
        Me.BeneficiaryBranchTextBox.Location = New System.Drawing.Point(237, 72)
        Me.BeneficiaryBranchTextBox.MaxLength = 35
        Me.BeneficiaryBranchTextBox.Name = "BeneficiaryBranchTextBox"
        Me.BeneficiaryBranchTextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryBranchTextBox.TabIndex = 5
        '
        'BeneficiaryBankTextBox
        '
        Me.BeneficiaryBankTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBank", True))
        Me.BeneficiaryBankTextBox.Location = New System.Drawing.Point(237, 53)
        Me.BeneficiaryBankTextBox.MaxLength = 35
        Me.BeneficiaryBankTextBox.Name = "BeneficiaryBankTextBox"
        Me.BeneficiaryBankTextBox.Size = New System.Drawing.Size(410, 20)
        Me.BeneficiaryBankTextBox.TabIndex = 3
        '
        'BeneficiaryBankAndBranchComboBox
        '
        Me.BeneficiaryBankAndBranchComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "BeneficiaryBankAndBranch", True))
        Me.BeneficiaryBankAndBranchComboBox.DataSource = Me.PBeneficiaryBankDTBindingSource
        Me.BeneficiaryBankAndBranchComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BeneficiaryBankAndBranchComboBox.FormattingEnabled = True
        Me.BeneficiaryBankAndBranchComboBox.Location = New System.Drawing.Point(237, 19)
        Me.BeneficiaryBankAndBranchComboBox.Name = "BeneficiaryBankAndBranchComboBox"
        Me.BeneficiaryBankAndBranchComboBox.Size = New System.Drawing.Size(410, 22)
        Me.BeneficiaryBankAndBranchComboBox.TabIndex = 1
        '
        'PBeneficiaryBankDTBindingSource
        '
        Me.PBeneficiaryBankDTBindingSource.DataMember = "PBeneficiaryBankDT"
        Me.PBeneficiaryBankDTBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'IntermediaryDetGroupBox
        '
        Me.IntermediaryDetGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBankMasterCodeValueLabel)
        Me.IntermediaryDetGroupBox.Controls.Add(IntermediaryBankMasterCodeLabel)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryOptionGroupBox)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBankAddress2TextBox)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBankAddress1TextBox)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBranchTextBox)
        Me.IntermediaryDetGroupBox.Controls.Add(IntermediaryBankLabel)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBankTextBox)
        Me.IntermediaryDetGroupBox.Controls.Add(IntermediaryBankAndBranchLabel)
        Me.IntermediaryDetGroupBox.Controls.Add(Me.IntermediaryBankAndBranchComboBox)
        Me.IntermediaryDetGroupBox.Location = New System.Drawing.Point(36, 545)
        Me.IntermediaryDetGroupBox.Name = "IntermediaryDetGroupBox"
        Me.IntermediaryDetGroupBox.Size = New System.Drawing.Size(696, 207)
        Me.IntermediaryDetGroupBox.TabIndex = 4
        Me.IntermediaryDetGroupBox.TabStop = False
        Me.IntermediaryDetGroupBox.Text = "Intermediary : "
        '
        'IntermediaryBankMasterCodeValueLabel
        '
        Me.IntermediaryBankMasterCodeValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBankMasterCode", True))
        Me.IntermediaryBankMasterCodeValueLabel.Enabled = False
        Me.IntermediaryBankMasterCodeValueLabel.Location = New System.Drawing.Point(237, 163)
        Me.IntermediaryBankMasterCodeValueLabel.Name = "IntermediaryBankMasterCodeValueLabel"
        Me.IntermediaryBankMasterCodeValueLabel.ReadOnly = True
        Me.IntermediaryBankMasterCodeValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.IntermediaryBankMasterCodeValueLabel.TabIndex = 11
        '
        'IntermediaryOptionGroupBox
        '
        Me.IntermediaryOptionGroupBox.Controls.Add(Me.IntermediaryBankOptionDRadioButton)
        Me.IntermediaryOptionGroupBox.Controls.Add(Me.IntermediaryBankOptionCRadioButton)
        Me.IntermediaryOptionGroupBox.Controls.Add(Me.IntermediaryBankOptionARadioButton)
        Me.IntermediaryOptionGroupBox.Location = New System.Drawing.Point(9, 66)
        Me.IntermediaryOptionGroupBox.Name = "IntermediaryOptionGroupBox"
        Me.IntermediaryOptionGroupBox.Size = New System.Drawing.Size(211, 91)
        Me.IntermediaryOptionGroupBox.TabIndex = 10
        Me.IntermediaryOptionGroupBox.TabStop = False
        Me.IntermediaryOptionGroupBox.Text = "Options : "
        '
        'IntermediaryBankOptionDRadioButton
        '
        Me.IntermediaryBankOptionDRadioButton.Location = New System.Drawing.Point(11, 18)
        Me.IntermediaryBankOptionDRadioButton.Name = "IntermediaryBankOptionDRadioButton"
        Me.IntermediaryBankOptionDRadioButton.Size = New System.Drawing.Size(158, 24)
        Me.IntermediaryBankOptionDRadioButton.TabIndex = 4
        Me.IntermediaryBankOptionDRadioButton.Text = "D Option Bank / Branch"
        '
        'IntermediaryBankOptionCRadioButton
        '
        Me.IntermediaryBankOptionCRadioButton.Location = New System.Drawing.Point(11, 61)
        Me.IntermediaryBankOptionCRadioButton.Name = "IntermediaryBankOptionCRadioButton"
        Me.IntermediaryBankOptionCRadioButton.Size = New System.Drawing.Size(169, 24)
        Me.IntermediaryBankOptionCRadioButton.TabIndex = 3
        Me.IntermediaryBankOptionCRadioButton.Text = "C National Clearing Code"
        '
        'IntermediaryBankOptionARadioButton
        '
        Me.IntermediaryBankOptionARadioButton.Location = New System.Drawing.Point(11, 40)
        Me.IntermediaryBankOptionARadioButton.Name = "IntermediaryBankOptionARadioButton"
        Me.IntermediaryBankOptionARadioButton.Size = New System.Drawing.Size(180, 24)
        Me.IntermediaryBankOptionARadioButton.TabIndex = 1
        Me.IntermediaryBankOptionARadioButton.Text = "A Option SWIFT BIC"
        '
        'IntermediaryBankAddress2TextBox
        '
        Me.IntermediaryBankAddress2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBankAddress2", True))
        Me.IntermediaryBankAddress2TextBox.Location = New System.Drawing.Point(237, 107)
        Me.IntermediaryBankAddress2TextBox.MaxLength = 35
        Me.IntermediaryBankAddress2TextBox.Name = "IntermediaryBankAddress2TextBox"
        Me.IntermediaryBankAddress2TextBox.Size = New System.Drawing.Size(410, 20)
        Me.IntermediaryBankAddress2TextBox.TabIndex = 9
        '
        'IntermediaryBankAddress1TextBox
        '
        Me.IntermediaryBankAddress1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBankAddress1", True))
        Me.IntermediaryBankAddress1TextBox.Location = New System.Drawing.Point(237, 88)
        Me.IntermediaryBankAddress1TextBox.MaxLength = 35
        Me.IntermediaryBankAddress1TextBox.Name = "IntermediaryBankAddress1TextBox"
        Me.IntermediaryBankAddress1TextBox.Size = New System.Drawing.Size(410, 20)
        Me.IntermediaryBankAddress1TextBox.TabIndex = 7
        '
        'IntermediaryBranchTextBox
        '
        Me.IntermediaryBranchTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBranch", True))
        Me.IntermediaryBranchTextBox.Location = New System.Drawing.Point(237, 69)
        Me.IntermediaryBranchTextBox.MaxLength = 35
        Me.IntermediaryBranchTextBox.Name = "IntermediaryBranchTextBox"
        Me.IntermediaryBranchTextBox.Size = New System.Drawing.Size(410, 20)
        Me.IntermediaryBranchTextBox.TabIndex = 5
        '
        'IntermediaryBankTextBox
        '
        Me.IntermediaryBankTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBank", True))
        Me.IntermediaryBankTextBox.Location = New System.Drawing.Point(237, 50)
        Me.IntermediaryBankTextBox.MaxLength = 35
        Me.IntermediaryBankTextBox.Name = "IntermediaryBankTextBox"
        Me.IntermediaryBankTextBox.Size = New System.Drawing.Size(410, 20)
        Me.IntermediaryBankTextBox.TabIndex = 3
        '
        'IntermediaryBankAndBranchComboBox
        '
        Me.IntermediaryBankAndBranchComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "IntermediaryBankAndBranch", True))
        Me.IntermediaryBankAndBranchComboBox.DataSource = Me.PIntermediaryDTBindingSource
        Me.IntermediaryBankAndBranchComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.IntermediaryBankAndBranchComboBox.FormattingEnabled = True
        Me.IntermediaryBankAndBranchComboBox.Location = New System.Drawing.Point(237, 16)
        Me.IntermediaryBankAndBranchComboBox.Name = "IntermediaryBankAndBranchComboBox"
        Me.IntermediaryBankAndBranchComboBox.Size = New System.Drawing.Size(410, 22)
        Me.IntermediaryBankAndBranchComboBox.TabIndex = 1
        '
        'PIntermediaryDTBindingSource
        '
        Me.PIntermediaryDTBindingSource.DataSource = Me.PBeneficiaryBankDTBindingSource
        '
        'RemittanceDetGroupBox
        '
        Me.RemittanceDetGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RemittanceDetGroupBox.Controls.Add(ContractNoLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.ContractNoTextBox)
        Me.RemittanceDetGroupBox.Controls.Add(ExchangeMethodLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.ExchangeMethodComboBox)
        Me.RemittanceDetGroupBox.Controls.Add(RemitAmountLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.RemitAmountTextBox)
        Me.RemittanceDetGroupBox.Controls.Add(CurrencyLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.CurrencyComboBox)
        Me.RemittanceDetGroupBox.Controls.Add(SectorSelectionLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.SectorSelectionComboBox)
        Me.RemittanceDetGroupBox.Controls.Add(CustomerReferenceLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.CustomerReferenceTextBox)
        Me.RemittanceDetGroupBox.Controls.Add(Me.ValueDateInfoLabel)
        Me.RemittanceDetGroupBox.Controls.Add(ValueDateLabel)
        Me.RemittanceDetGroupBox.Controls.Add(Me.ValueDateTextBox)
        Me.RemittanceDetGroupBox.Location = New System.Drawing.Point(36, 324)
        Me.RemittanceDetGroupBox.Name = "RemittanceDetGroupBox"
        Me.RemittanceDetGroupBox.Size = New System.Drawing.Size(696, 215)
        Me.RemittanceDetGroupBox.TabIndex = 3
        Me.RemittanceDetGroupBox.TabStop = False
        Me.RemittanceDetGroupBox.Text = "Remittance Detail :"
        '
        'ContractNoTextBox
        '
        Me.ContractNoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ContractNo", True))
        Me.ContractNoTextBox.Location = New System.Drawing.Point(237, 181)
        Me.ContractNoTextBox.MaxLength = 15
        Me.ContractNoTextBox.Name = "ContractNoTextBox"
        Me.ContractNoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContractNoTextBox.TabIndex = 14
        '
        'ExchangeMethodComboBox
        '
        Me.ExchangeMethodComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "ExchangeMethod", True))
        Me.ExchangeMethodComboBox.DataSource = Me.PExchangeMethodArrBindingSource
        Me.ExchangeMethodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ExchangeMethodComboBox.FormattingEnabled = True
        Me.ExchangeMethodComboBox.Location = New System.Drawing.Point(237, 153)
        Me.ExchangeMethodComboBox.Name = "ExchangeMethodComboBox"
        Me.ExchangeMethodComboBox.Size = New System.Drawing.Size(121, 22)
        Me.ExchangeMethodComboBox.TabIndex = 12
        '
        'PExchangeMethodArrBindingSource
        '
        Me.PExchangeMethodArrBindingSource.DataMember = "PExchangeMethodDT"
        Me.PExchangeMethodArrBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'RemitAmountTextBox
        '
        Me.RemitAmountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "RemitAmount", True))
        Me.RemitAmountTextBox.Location = New System.Drawing.Point(237, 127)
        Me.RemitAmountTextBox.MaxLength = 15
        Me.RemitAmountTextBox.Name = "RemitAmountTextBox"
        Me.RemitAmountTextBox.Size = New System.Drawing.Size(175, 20)
        Me.RemitAmountTextBox.TabIndex = 10
        '
        'CurrencyComboBox
        '
        Me.CurrencyComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "Currency", True))
        Me.CurrencyComboBox.DataSource = Me.PCurrencyDTBindingSource
        Me.CurrencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CurrencyComboBox.FormattingEnabled = True
        Me.CurrencyComboBox.Location = New System.Drawing.Point(237, 100)
        Me.CurrencyComboBox.Name = "CurrencyComboBox"
        Me.CurrencyComboBox.Size = New System.Drawing.Size(121, 22)
        Me.CurrencyComboBox.TabIndex = 8
        '
        'PCurrencyDTBindingSource
        '
        Me.PCurrencyDTBindingSource.DataMember = "PCurrencyDT"
        Me.PCurrencyDTBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'SectorSelectionComboBox
        '
        Me.SectorSelectionComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "SectorSelection", True))
        Me.SectorSelectionComboBox.DataSource = Me.PSectorSelectionArrBindingSource
        Me.SectorSelectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SectorSelectionComboBox.FormattingEnabled = True
        Me.SectorSelectionComboBox.Location = New System.Drawing.Point(237, 73)
        Me.SectorSelectionComboBox.Name = "SectorSelectionComboBox"
        Me.SectorSelectionComboBox.Size = New System.Drawing.Size(162, 22)
        Me.SectorSelectionComboBox.TabIndex = 6
        '
        'PSectorSelectionArrBindingSource
        '
        Me.PSectorSelectionArrBindingSource.DataMember = "PSectorSelectionDT"
        Me.PSectorSelectionArrBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'CustomerReferenceTextBox
        '
        Me.CustomerReferenceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "CustomerReference", True))
        Me.CustomerReferenceTextBox.Location = New System.Drawing.Point(237, 47)
        Me.CustomerReferenceTextBox.MaxLength = 16
        Me.CustomerReferenceTextBox.Name = "CustomerReferenceTextBox"
        Me.CustomerReferenceTextBox.Size = New System.Drawing.Size(175, 20)
        Me.CustomerReferenceTextBox.TabIndex = 4
        '
        'ValueDateInfoLabel
        '
        Me.ValueDateInfoLabel.AutoSize = True
        Me.ValueDateInfoLabel.Location = New System.Drawing.Point(338, 29)
        Me.ValueDateInfoLabel.Name = "ValueDateInfoLabel"
        Me.ValueDateInfoLabel.Size = New System.Drawing.Size(61, 14)
        Me.ValueDateInfoLabel.TabIndex = 2
        Me.ValueDateInfoLabel.Text = "(YYMMDD)"
        '
        'ValueDateTextBox
        '
        Me.ValueDateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "ValueDate", True))
        Me.ValueDateTextBox.Location = New System.Drawing.Point(237, 22)
        Me.ValueDateTextBox.MaxLength = 6
        Me.ValueDateTextBox.Name = "ValueDateTextBox"
        Me.ValueDateTextBox.Size = New System.Drawing.Size(82, 20)
        Me.ValueDateTextBox.TabIndex = 1
        '
        'SettlementDetGroupBox
        '
        Me.SettlementDetGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementBankAddress2ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementBankAddress1ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementBranchValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementBankValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccName4ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccName3ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccName2ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccName1ValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccNoValueLabel)
        Me.SettlementDetGroupBox.Controls.Add(SettlementBankAddress1Label)
        Me.SettlementDetGroupBox.Controls.Add(SettlementBranchLabel)
        Me.SettlementDetGroupBox.Controls.Add(SettlementBankLabel)
        Me.SettlementDetGroupBox.Controls.Add(SettlementAccName1Label)
        Me.SettlementDetGroupBox.Controls.Add(SettlementAccNoLabel)
        Me.SettlementDetGroupBox.Controls.Add(SettlementAccNoAndNameLabel)
        Me.SettlementDetGroupBox.Controls.Add(Me.SettlementAccNoAndNameComboBox)
        Me.SettlementDetGroupBox.Location = New System.Drawing.Point(36, 55)
        Me.SettlementDetGroupBox.Name = "SettlementDetGroupBox"
        Me.SettlementDetGroupBox.Size = New System.Drawing.Size(696, 263)
        Me.SettlementDetGroupBox.TabIndex = 2
        Me.SettlementDetGroupBox.TabStop = False
        Me.SettlementDetGroupBox.Text = "Settlement : "
        '
        'SettlementBankAddress2ValueLabel
        '
        Me.SettlementBankAddress2ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementBankAddress2", True))
        Me.SettlementBankAddress2ValueLabel.Enabled = False
        Me.SettlementBankAddress2ValueLabel.Location = New System.Drawing.Point(237, 227)
        Me.SettlementBankAddress2ValueLabel.Name = "SettlementBankAddress2ValueLabel"
        Me.SettlementBankAddress2ValueLabel.ReadOnly = True
        Me.SettlementBankAddress2ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementBankAddress2ValueLabel.TabIndex = 23
        '
        'SettlementBankAddress1ValueLabel
        '
        Me.SettlementBankAddress1ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementBankAddress1", True))
        Me.SettlementBankAddress1ValueLabel.Enabled = False
        Me.SettlementBankAddress1ValueLabel.Location = New System.Drawing.Point(237, 204)
        Me.SettlementBankAddress1ValueLabel.Name = "SettlementBankAddress1ValueLabel"
        Me.SettlementBankAddress1ValueLabel.ReadOnly = True
        Me.SettlementBankAddress1ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementBankAddress1ValueLabel.TabIndex = 22
        '
        'SettlementBranchValueLabel
        '
        Me.SettlementBranchValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementBranch", True))
        Me.SettlementBranchValueLabel.Enabled = False
        Me.SettlementBranchValueLabel.Location = New System.Drawing.Point(237, 181)
        Me.SettlementBranchValueLabel.Name = "SettlementBranchValueLabel"
        Me.SettlementBranchValueLabel.ReadOnly = True
        Me.SettlementBranchValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementBranchValueLabel.TabIndex = 21
        '
        'SettlementBankValueLabel
        '
        Me.SettlementBankValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementBank", True))
        Me.SettlementBankValueLabel.Enabled = False
        Me.SettlementBankValueLabel.Location = New System.Drawing.Point(237, 158)
        Me.SettlementBankValueLabel.Name = "SettlementBankValueLabel"
        Me.SettlementBankValueLabel.ReadOnly = True
        Me.SettlementBankValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementBankValueLabel.TabIndex = 20
        '
        'SettlementAccName4ValueLabel
        '
        Me.SettlementAccName4ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccName4", True))
        Me.SettlementAccName4ValueLabel.Enabled = False
        Me.SettlementAccName4ValueLabel.Location = New System.Drawing.Point(237, 135)
        Me.SettlementAccName4ValueLabel.Name = "SettlementAccName4ValueLabel"
        Me.SettlementAccName4ValueLabel.ReadOnly = True
        Me.SettlementAccName4ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementAccName4ValueLabel.TabIndex = 8
        '
        'SettlementAccName3ValueLabel
        '
        Me.SettlementAccName3ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccName3", True))
        Me.SettlementAccName3ValueLabel.Enabled = False
        Me.SettlementAccName3ValueLabel.Location = New System.Drawing.Point(237, 112)
        Me.SettlementAccName3ValueLabel.Name = "SettlementAccName3ValueLabel"
        Me.SettlementAccName3ValueLabel.ReadOnly = True
        Me.SettlementAccName3ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementAccName3ValueLabel.TabIndex = 7
        '
        'SettlementAccName2ValueLabel
        '
        Me.SettlementAccName2ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccName2", True))
        Me.SettlementAccName2ValueLabel.Enabled = False
        Me.SettlementAccName2ValueLabel.Location = New System.Drawing.Point(237, 89)
        Me.SettlementAccName2ValueLabel.Name = "SettlementAccName2ValueLabel"
        Me.SettlementAccName2ValueLabel.ReadOnly = True
        Me.SettlementAccName2ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementAccName2ValueLabel.TabIndex = 6
        '
        'SettlementAccName1ValueLabel
        '
        Me.SettlementAccName1ValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccName1", True))
        Me.SettlementAccName1ValueLabel.Enabled = False
        Me.SettlementAccName1ValueLabel.Location = New System.Drawing.Point(237, 66)
        Me.SettlementAccName1ValueLabel.Name = "SettlementAccName1ValueLabel"
        Me.SettlementAccName1ValueLabel.ReadOnly = True
        Me.SettlementAccName1ValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementAccName1ValueLabel.TabIndex = 5
        '
        'SettlementAccNoValueLabel
        '
        Me.SettlementAccNoValueLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccNo", True))
        Me.SettlementAccNoValueLabel.Enabled = False
        Me.SettlementAccNoValueLabel.Location = New System.Drawing.Point(237, 43)
        Me.SettlementAccNoValueLabel.Name = "SettlementAccNoValueLabel"
        Me.SettlementAccNoValueLabel.ReadOnly = True
        Me.SettlementAccNoValueLabel.Size = New System.Drawing.Size(410, 20)
        Me.SettlementAccNoValueLabel.TabIndex = 3
        '
        'SettlementAccNoAndNameComboBox
        '
        Me.SettlementAccNoAndNameComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccNoAndName", True))
        Me.SettlementAccNoAndNameComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "SettlementAccNoAndName", True))
        Me.SettlementAccNoAndNameComboBox.DataSource = Me.PSettlementDTBindingSource
        Me.SettlementAccNoAndNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SettlementAccNoAndNameComboBox.FormattingEnabled = True
        Me.SettlementAccNoAndNameComboBox.Location = New System.Drawing.Point(237, 19)
        Me.SettlementAccNoAndNameComboBox.Name = "SettlementAccNoAndNameComboBox"
        Me.SettlementAccNoAndNameComboBox.Size = New System.Drawing.Size(410, 22)
        Me.SettlementAccNoAndNameComboBox.TabIndex = 1
        '
        'TemplateIDComboBox
        '
        Me.TemplateIDComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.GCMSMTTransactionDetailBindingSource, "TemplateID", True))
        Me.TemplateIDComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.GCMSMTTransactionDetailBindingSource, "TemplateID", True))
        Me.TemplateIDComboBox.DataSource = Me.PTransactionTemplateDTBindingSource
        Me.TemplateIDComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TemplateIDComboBox.FormattingEnabled = True
        Me.TemplateIDComboBox.Location = New System.Drawing.Point(114, 17)
        Me.TemplateIDComboBox.Name = "TemplateIDComboBox"
        Me.TemplateIDComboBox.Size = New System.Drawing.Size(280, 22)
        Me.TemplateIDComboBox.TabIndex = 1
        '
        'PTransactionTemplateDTBindingSource
        '
        Me.PTransactionTemplateDTBindingSource.DataMember = "PTransactionTemplateDT"
        Me.PTransactionTemplateDTBindingSource.DataSource = Me.GCMSMTMasterBindingSource
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(110, 17)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(87, 37)
        Me.btnBack.TabIndex = 33
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(14, 17)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(82, 37)
        Me.btnOK.TabIndex = 32
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'TransDetailError
        '
        Me.TransDetailError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.TransDetailError.ContainerControl = Me
        '
        'ButtonGroupBox
        '
        Me.ButtonGroupBox.Controls.Add(Me.btnBack)
        Me.ButtonGroupBox.Controls.Add(Me.btnOK)
        Me.ButtonGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.ButtonGroupBox.Name = "ButtonGroupBox"
        Me.ButtonGroupBox.Size = New System.Drawing.Size(766, 68)
        Me.ButtonGroupBox.TabIndex = 34
        Me.ButtonGroupBox.TabStop = False
        '
        'btnPanel
        '
        Me.btnPanel.AutoScroll = True
        Me.btnPanel.Controls.Add(Me.ButtonGroupBox)
        Me.btnPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.btnPanel.Location = New System.Drawing.Point(5, 1528)
        Me.btnPanel.Name = "btnPanel"
        Me.btnPanel.Size = New System.Drawing.Size(766, 68)
        Me.btnPanel.TabIndex = 35
        '
        'ucGCMSMTTransDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TransDetailPanel)
        Me.Controls.Add(Me.btnPanel)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.GCMSMTMasterBindingSource, "PTemplateID", True))
        Me.Name = "ucGCMSMTTransDetail"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Size = New System.Drawing.Size(776, 1601)
        Me.TransDetailPanel.ResumeLayout(False)
        Me.TransDetailPanel.PerformLayout()
        Me.OthersDetGroupBox.ResumeLayout(False)
        Me.OthersDetGroupBox.PerformLayout()
        CType(Me.GCMSMTTransactionDetailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PChargesAccountDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSettlementDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GCMSMTMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBankChargesDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryDetGroupBox.ResumeLayout(False)
        Me.BeneficiaryDetGroupBox.PerformLayout()
        CType(Me.PBeneficiaryCustDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryOptionGroupBox.ResumeLayout(False)
        CType(Me.PBeneficiaryBankDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.IntermediaryDetGroupBox.ResumeLayout(False)
        Me.IntermediaryDetGroupBox.PerformLayout()
        Me.IntermediaryOptionGroupBox.ResumeLayout(False)
        CType(Me.PIntermediaryDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RemittanceDetGroupBox.ResumeLayout(False)
        Me.RemittanceDetGroupBox.PerformLayout()
        CType(Me.PExchangeMethodArrBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCurrencyDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSectorSelectionArrBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SettlementDetGroupBox.ResumeLayout(False)
        Me.SettlementDetGroupBox.PerformLayout()
        CType(Me.PTransactionTemplateDTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransDetailError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ButtonGroupBox.ResumeLayout(False)
        Me.btnPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TransDetailPanel As System.Windows.Forms.Panel
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents GCMSMTMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PCurrencyDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PExchangeMethodArrBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PSectorSelectionArrBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PTransactionTemplateDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PSettlementDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PBeneficiaryBankDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PBeneficiaryCustDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TransDetailError As BTMU.MAGIC.Common.ErrorProviderFixed
    'Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents PChargesAccountDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PIntermediaryDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ButtonGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnPanel As System.Windows.Forms.Panel
    Friend WithEvents GCMSMTTransactionDetailBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SettlementDetGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents SettlementAccNoAndNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TemplateIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents RemittanceDetGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents CustomerReferenceTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ValueDateInfoLabel As System.Windows.Forms.Label
    Friend WithEvents ValueDateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ExchangeMethodComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents RemitAmountTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CurrencyComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SectorSelectionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ContractNoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryDetGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents IntermediaryBankAndBranchComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents IntermediaryBankAddress2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryBankAddress1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryBranchTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryBankTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryOptionGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents IntermediaryBankOptionDRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents IntermediaryBankOptionCRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents IntermediaryBankOptionARadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BeneficiaryDetGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BeneficiaryBankTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryBankAndBranchComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents BeneficiaryOptionGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BeneficiaryBankAddress2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryBankAddress1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryBranchTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryBankOptionDRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BeneficiaryBankOptionCRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BeneficiaryBankOptionARadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BeneficiaryAccNoAndNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents BeneficiaryAccNoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryMsg1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryName4TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryName3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryName2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryName1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryMsg4TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryMsg3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryMsg2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents OthersDetGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BankChargesComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents BeneficiaryRemitInfo3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryRemitInfo2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryRemitInfo1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryRemitPurposeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccNoAndNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PBankChargesDTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SettlementAccNoValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementAccName1ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementAccName2ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementAccName3ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementAccName4ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementBankValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementBranchValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementBankAddress1ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents SettlementBankAddress2ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccName1ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccName3ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccName2ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents ChargesAccName4ValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents IntermediaryBankMasterCodeValueLabel As System.Windows.Forms.TextBox
    Friend WithEvents BeneficiaryBankMasterCodeValueLabel As System.Windows.Forms.TextBox

End Class
