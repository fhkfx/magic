Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.GCMS_MT
Imports System.IO
Imports System.Text

''' <summary>
''' Main form which display all user controls of GCMS-MT
''' </summary>
''' <remarks></remarks>
Public Class frmGCMSMTBase

#Region "Private Variables"

    Private objMaster As GCMSMTMaster
    Private msgReader As Global.System.Resources.ResourceManager

    Private _editFileName As String
    Private _editFilePath As String
    Private _recordState As String
    Private _modeState As String

    Private _masterFilePath As String
    Private _savedFilePath As String
    Private _uploadFilePath As String
    Private _archiveFilePath As String

    Private _chargesType As String
    Private _filenamePrefix As String

    Private _iniFilePath As String

    Private Const INI_FILE = "Offline.ini"
    Private Const MST_TEMPLATE = "Offline_template00001"
    Private Const MST_SETTLE = "Offline_settlement000"
    Private Const MST_CURR = "Offline_remitcurrency"
    Private Const MST_BENE_BANK = "Offline_benebank00001"
    Private Const MST_BENE_CUST = "Offline_beneficiary00"
    Private Const EXT_HTML = ".html"
    Private Const EXT_CSV = ".csv"
    Private Const EXT_TXT = ".txt"

    Private Const D_OPTION_TEMP = "D"
    Private Const C_OPTION_TEMP = "C"
    Private Const A_OPTION_TEMP = "A"

    Private Const D_OPTION_SAVED = "D:"
    Private Const C_OPTION_SAVED = "C:"
    Private Const A_OPTION_SAVED = "A:"

    'Main menu: CREATE or EDIT
    Enum eMainModeState
        CREATE
        EDIT
    End Enum

    'Record state: ADD NEW or MODIFY
    Enum eRecordState
        ENTRY
        EDIT
    End Enum

    Enum eMasterType
        CURRENCY
        BENE_BANK
        BENE_CUST
        SETTLEMENT
        TEMPLATE
    End Enum

#End Region

#Region "Properties"

    Public Property PRecordState() As String
        Get
            Return _recordState
        End Get
        Set(ByVal value As String)
            _recordState = value
        End Set
    End Property

    Public Property PModeState() As String
        Get
            Return _modeState
        End Get
        Set(ByVal value As String)
            _modeState = value
        End Set
    End Property

    Public Property PEditFilePath() As String
        Get
            Return _editFilePath
        End Get
        Set(ByVal value As String)
            _editFilePath = value
        End Set
    End Property

    Public Property PEditFileName() As String
        Get
            Return _editFileName
        End Get
        Set(ByVal value As String)
            _editFileName = value
        End Set
    End Property

    Public Property PSavedFilePath() As String
        Get
            Return _savedFilePath
        End Get
        Set(ByVal value As String)
            _savedFilePath = value
        End Set
    End Property

    Public Property PUploadFilePath() As String
        Get
            Return _uploadFilePath
        End Get
        Set(ByVal value As String)
            _uploadFilePath = value
        End Set
    End Property

    Public Property PArchiveFilePath() As String
        Get
            Return _archiveFilePath
        End Get
        Set(ByVal value As String)
            _archiveFilePath = value
        End Set
    End Property

    Public Property PMasterFilePath() As String
        Get
            Return _masterFilePath
        End Get
        Set(ByVal value As String)
            _masterFilePath = value
        End Set
    End Property

    Public Property PChargesType() As String
        Get
            Return _chargesType
        End Get
        Set(ByVal value As String)
            _chargesType = value
        End Set
    End Property

    Public Property PSaveFilePrefix() As String
        Get
            Return _filenamePrefix
        End Get
        Set(ByVal value As String)
            _filenamePrefix = value
        End Set
    End Property

    Public Property PINIFilePath() As String
        Get
            Return _iniFilePath
        End Get
        Set(ByVal value As String)
            _iniFilePath = value
        End Set
    End Property

    Public ReadOnly Property PINIFileName() As String
        Get
            Return INI_FILE
        End Get
    End Property

    Public ReadOnly Property PMasterTemplateFileNamePrefix() As String
        Get
            Return MST_TEMPLATE
        End Get
    End Property

    Public ReadOnly Property PMasterSettlementFileNamePrefix() As String
        Get
            Return MST_SETTLE
        End Get
    End Property

    Public ReadOnly Property PMasterCurrencyFileNamePrefix() As String
        Get
            Return MST_CURR
        End Get
    End Property

    Public ReadOnly Property PMasterBeneBankFileNamePrefix() As String
        Get
            Return MST_BENE_BANK
        End Get
    End Property

    Public ReadOnly Property PMasterBeneCustFileNamePrefix() As String
        Get
            Return MST_BENE_CUST
        End Get
    End Property

    Public ReadOnly Property PTxtFileExt() As String
        Get
            Return EXT_TXT
        End Get
    End Property

    Public ReadOnly Property PHtmlFileExt() As String
        Get
            Return EXT_HTML
        End Get
    End Property

    Public ReadOnly Property PCsvFileExt() As String
        Get
            Return EXT_CSV
        End Get
    End Property

    Public ReadOnly Property PTemplateOption_D() As String
        Get
            Return D_OPTION_TEMP
        End Get
    End Property

    Public ReadOnly Property PTemplateOption_C() As String
        Get
            Return C_OPTION_TEMP
        End Get
    End Property

    Public ReadOnly Property PTemplateOption_A() As String
        Get
            Return A_OPTION_TEMP
        End Get
    End Property

    Public ReadOnly Property PSavedOption_D() As String
        Get
            Return D_OPTION_SAVED
        End Get
    End Property

    Public ReadOnly Property PSavedOption_C() As String
        Get
            Return C_OPTION_SAVED
        End Get
    End Property

    Public ReadOnly Property PSavedOption_A() As String
        Get
            Return A_OPTION_SAVED
        End Get
    End Property
 
    Public ReadOnly Property PGCMSMTMaster()
        Get
            Return objMaster
        End Get
    End Property

    Public ReadOnly Property PMessageReader()
        Get
            Return msgReader
        End Get
    End Property

#End Region

    Private Sub frmGCMSMTBase_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SuspendLayout()

        Try
            msgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

            Me.PINIFilePath = Application.StartupPath & "\default\"
            Me.PMasterFilePath = Application.StartupPath & "\GCMS-MT\Master\"
            Me.PSavedFilePath = Application.StartupPath & "\GCMS-MT\Save\"
            Me.PUploadFilePath = Application.StartupPath & "\GCMS-MT\Upload\"
            Me.PArchiveFilePath = Application.StartupPath & "\GCMS-MT\Archive\"

            LoadINIFile()
            CheckFoldersExistance()
            LoadMasterFiles()

            UcGCMSMTMainMenu1.ContainerForm = Me
            UcGCMSMTTransList1.ContainerForm = Me
            UcGCMSMTTransDetail1.ContainerForm = Me

            ShowMainMenu()

            UcGCMSMTTransDetail1.SettingBinding(objMaster)

            Me.Left = (Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2
            Me.Top = 25

            UcGCMSMTMainMenu1.MagicVersionLabel.Text = "Version " & Application.ProductVersion

        Catch MAGICEx1 As MagicException
            MessageBox.Show(MAGICEx1.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.BeginInvoke(New MethodInvoker(AddressOf CloseForm))
        Finally
            '--
        End Try

        ResumeLayout()

    End Sub

    Private Sub CloseForm()
        Me.Close()
    End Sub

    Private Sub frmGCMSMTBase_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Try
            If Me.UcGCMSMTTransDetail1.Visible Then

                Dim objTransDet As GCMSMTTransactionDetail
                objTransDet = Me.UcGCMSMTTransDetail1.GCMSMTTransactionDetailBindingSource.Current

                If objTransDet IsNot Nothing Then
                    objTransDet.Validate()
                    If objTransDet.IsDirty AndAlso Not Me.ConfirmToClose() Then
                        e.Cancel = True
                    End If
                End If
            ElseIf Me.UcGCMSMTTransList1.Visible Then
                If Me.UcGCMSMTTransList1.PColDirty AndAlso Not Me.ConfirmToClose() Then
                    e.Cancel = True
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        Finally
            '--

        End Try

    End Sub

    ''' <summary>
    ''' Show main menu screen
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowMainMenu()

        UcGCMSMTMainMenu1.Visible = True
        UcGCMSMTTransList1.Visible = False
        UcGCMSMTTransDetail1.Visible = False

        UcGCMSMTMainMenu1.Dock = DockStyle.Fill

        UcGCMSMTTransList1.Dock = DockStyle.None
        UcGCMSMTTransDetail1.Dock = DockStyle.None

    End Sub

    ''' <summary>
    ''' Show transaction list view screen
    ''' </summary>
    ''' <param name="strMode"></param>
    ''' <remarks></remarks>
    Public Sub ShowTransactionList(ByVal strMode As String)

        UcGCMSMTTransList1.Visible = True
        UcGCMSMTMainMenu1.Visible = False
        UcGCMSMTTransDetail1.Visible = False

        UcGCMSMTTransList1.Dock = DockStyle.Fill
        UcGCMSMTMainMenu1.Dock = DockStyle.None
        UcGCMSMTTransDetail1.Dock = DockStyle.None

    End Sub

    ''' <summary>
    ''' Show transaction entry screen
    ''' </summary>
    ''' <param name="strMode"></param>
    ''' <remarks></remarks>
    Public Sub ShowTransactionDetail(ByVal strMode As String)

        UcGCMSMTTransDetail1.Visible = True
        UcGCMSMTMainMenu1.Visible = False
        UcGCMSMTTransList1.Visible = False

        UcGCMSMTTransDetail1.Dock = DockStyle.Fill
        UcGCMSMTMainMenu1.Dock = DockStyle.None
        UcGCMSMTTransList1.Dock = DockStyle.None

    End Sub

    ''' <summary>
    ''' Show configuration screen
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowConfig()

        UcGCMSMTMainMenu1.Visible = False
        UcGCMSMTTransList1.Visible = False
        UcGCMSMTTransDetail1.Visible = False

        UcGCMSMTMainMenu1.Dock = DockStyle.None
        UcGCMSMTTransList1.Dock = DockStyle.None
        UcGCMSMTTransDetail1.Dock = DockStyle.None

    End Sub

    ''' <summary>
    ''' Close module
    ''' </summary>
    ''' <remarks>This is called by method invoker in event load</remarks>
    Public Sub ExitApplication()
        Me.Close()
    End Sub

    ''' <summary>
    ''' Prompt confirmation to close module
    ''' </summary>
    ''' <returns>Flag: True: close, False = do not close</returns>
    ''' <remarks></remarks>
    Public Function ConfirmToClose() As Boolean

        Dim dlgResult As Windows.Forms.DialogResult = Windows.Forms.DialogResult.None

        dlgResult = MessageBox.Show("Data has not been saved. Do you want to close the module?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)
        If dlgResult = Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If

    End Function

#Region "Config and Master Files"

    ''' <summary>
    ''' Check and create all default folders if not found.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CheckFoldersExistance()

        Dim foldersNotFoundMessage As String = ""

        Try
            If Not (Directory.Exists(PSavedFilePath)) Then
                Directory.CreateDirectory(PSavedFilePath)
                foldersNotFoundMessage &= msgReader.GetString("E04000010") & vbCrLf '--
            End If

            If Not (Directory.Exists(PUploadFilePath)) Then
                Directory.CreateDirectory(PUploadFilePath)
                foldersNotFoundMessage &= msgReader.GetString("E04000020") & vbCrLf '--
            End If

            If Not (Directory.Exists(PArchiveFilePath)) Then
                Directory.CreateDirectory(PArchiveFilePath)
                foldersNotFoundMessage &= msgReader.GetString("E04000030") & vbCrLf '--
            End If

            If Not (Directory.Exists(PMasterFilePath)) Then
                Directory.CreateDirectory(PMasterFilePath)
                foldersNotFoundMessage &= msgReader.GetString("E04000040") & vbCrLf '--
            End If

            If foldersNotFoundMessage.Trim <> "" Then
                MessageBox.Show(foldersNotFoundMessage, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Read and get default setting from INI file
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadINIFile()

        Dim objINIFile As INIFile

        Try
            If Not Directory.Exists(Me.PINIFilePath) Then
                Throw New MagicException(msgReader.GetString("E04000060") & vbCrLf & " (" & Me.PINIFilePath & ")") '--
            End If

            If Not File.Exists(Me.PINIFilePath & Me.PINIFileName) Then
                Throw New MagicException(msgReader.GetString("E04000050") & vbCrLf & " (" & Me.PINIFilePath & Me.PINIFileName & ")") '--
            End If

            objINIFile = New INIFile(Me.PINIFilePath & Me.PINIFileName)
            PChargesType = objINIFile.GetString("SETUP", "chargeselection", "")
            PSaveFilePrefix = objINIFile.GetString("SETUP", "prefix", "")

        Catch dnfEX As DirectoryNotFoundException
            Throw New MagicException(msgReader.GetString("E04000060") & vbCrLf & " (" & Me.PINIFilePath & ")") '--
        Catch fnfEX As FileNotFoundException
            Throw New MagicException(msgReader.GetString("E04000050") & vbCrLf & " (" & Me.PINIFilePath & Me.PINIFileName & ")") '--
        Catch MagicEx2 As MagicException
            Throw New MagicException(MagicEx2.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Call funtions to load all masters data to datatable 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadMasterFiles()

        Dim masterFileNotFoundMessage As String = ""

        Try
            objMaster = New GCMSMTMaster(PChargesType)
            ReadMasterFile(objMaster, MST_CURR, eMasterType.CURRENCY, masterFileNotFoundMessage)
            ReadMasterFile(objMaster, MST_SETTLE, eMasterType.SETTLEMENT, masterFileNotFoundMessage)
            ReadMasterFile(objMaster, MST_BENE_BANK, eMasterType.BENE_BANK, masterFileNotFoundMessage)
            ReadMasterFile(objMaster, MST_BENE_CUST, eMasterType.BENE_CUST, masterFileNotFoundMessage)
            ReadMasterFile(objMaster, MST_TEMPLATE, eMasterType.TEMPLATE, masterFileNotFoundMessage)

            'Currency and settlement master must not be empty.
            If objMaster.PCurrencyDT.Rows.Count = 0 Then
                Throw New MagicException(msgReader.GetString("E04000140")) '--
            ElseIf objMaster.PSettlementDT.Rows.Count = 0 Then
                Throw New MagicException(msgReader.GetString("E04000110")) '--
            End If

            If Not IsNothingOrEmptyString(masterFileNotFoundMessage) Then
                MessageBox.Show(masterFileNotFoundMessage, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch MagicEx2 As MagicException
            Throw New MagicException(MagicEx2.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Read and load all masters data to datatable
    ''' </summary>
    ''' <param name="objMaster">Instance of master object</param>
    ''' <param name="masterFileNamePartial">Prefix of master file name</param>
    ''' <param name="masterFileType">Master file type</param>
    ''' <param name="masterFileNotFoundMessage">Error message</param>
    ''' <remarks>This is called by LoadMasterFiles</remarks>
    Public Sub ReadMasterFile(ByRef objMaster As GCMSMTMaster, _
                                ByVal masterFileNamePartial As String, _
                                ByVal masterFileType As String, _
                                ByRef masterFileNotFoundMessage As String)

        Dim arAllLines As String()
        Dim strLine As String = ""
        Dim masterFileFullName As String = ""
        Dim boolPopulateOnce As Boolean = True

        Try
            'Search and get full master filename
            For Each Item As String In Directory.GetFiles(PMasterFilePath, masterFileNamePartial & "*")
                If Item.Contains(masterFileNamePartial) Then
                    masterFileFullName = Item.Trim
                    Exit For
                End If
            Next

            'Currency and settlement master file must exist.
            If masterFileType = eMasterType.CURRENCY And masterFileFullName = "" Then
                Throw New MagicException(msgReader.GetString("E04000120")) '--
            ElseIf masterFileType = eMasterType.SETTLEMENT And masterFileFullName = "" Then
                Throw New MagicException(msgReader.GetString("E04000090")) '--
            ElseIf masterFileType = eMasterType.BENE_BANK And masterFileFullName = "" Then
                masterFileNotFoundMessage &= msgReader.GetString("E04000150") & vbCrLf '--
            ElseIf masterFileType = eMasterType.BENE_CUST And masterFileFullName = "" Then
                masterFileNotFoundMessage &= msgReader.GetString("E04000170") & vbCrLf '--
            ElseIf masterFileType = eMasterType.TEMPLATE And masterFileFullName = "" Then
                masterFileNotFoundMessage &= msgReader.GetString("E04000070") & vbCrLf '--
            ElseIf masterFileFullName = "" Then
                Exit Sub
            End If

            If Not IsNothingOrEmptyString(masterFileNotFoundMessage) Then
                Exit Sub
            End If

            arAllLines = File.ReadAllLines(masterFileFullName)

            For Each strLine In arAllLines
                If strLine.Trim <> "" Then
                    If masterFileType = eMasterType.CURRENCY Then
                        If boolPopulateOnce Then
                            objMaster.PopuplateCurrencyRow("", PMessageReader, boolPopulateOnce)
                            boolPopulateOnce = False
                        End If
                        objMaster.PopuplateCurrencyRow(strLine, PMessageReader, boolPopulateOnce)
                    ElseIf masterFileType = eMasterType.SETTLEMENT Then
                        If boolPopulateOnce Then
                            objMaster.PopuplateSettlementRow(",,,,,,,,,,,", PMessageReader)
                            boolPopulateOnce = False
                        End If
                        objMaster.PopuplateSettlementRow(strLine, PMessageReader)
                    ElseIf masterFileType = eMasterType.BENE_BANK Then
                        If boolPopulateOnce Then
                            objMaster.PopuplateBeneficiaryBankRow(",,,,,,,", PMessageReader)
                            boolPopulateOnce = False
                        End If
                        objMaster.PopuplateBeneficiaryBankRow(strLine, PMessageReader)
                    ElseIf masterFileType = eMasterType.BENE_CUST Then
                        If boolPopulateOnce Then
                            objMaster.PopuplateBeneficiaryCustRow(",,,,,,,,,,", PMessageReader)
                            boolPopulateOnce = False
                        End If
                        objMaster.PopuplateBeneficiaryCustRow(strLine, PMessageReader)
                    ElseIf masterFileType = eMasterType.TEMPLATE Then
                        If boolPopulateOnce Then
                            objMaster.PopuplateTemplateRow(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,", PMessageReader)
                            boolPopulateOnce = False
                        End If
                        objMaster.PopuplateTemplateRow(strLine, PMessageReader)
                    End If
                End If
            Next
        Catch dnfEx As DirectoryNotFoundException
            Throw New MagicException(dnfEx.Message)
        Catch fnfEx As FileNotFoundException
            Throw New MagicException(fnfEx.Message)
        Catch MagicEx3 As MagicException
            Throw New MagicException(MagicEx3.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        Finally
            '--
        End Try

    End Sub

#End Region

End Class
