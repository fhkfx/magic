Imports BTMU.MAGIC.GCMS_MT
Imports BTMU.MAGIC.Common

''' <summary>
''' User control for GCMT-MT transaction entry
''' </summary>
''' <remarks></remarks>
Public Class ucGCMSMTTransDetail

    Private objMaster As GCMSMTMaster
    Private msgReader As Global.System.Resources.ResourceManager
    'Dim remittanceAmount As Binding

    Private Sub ucGCMSMTTransDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'remittanceAmount = tbxRemitAmount.DataBindings("Text")
        'remittanceAmount.FormattingEnabled = True
        'remittanceAmount.FormatString = "#,##0.00"
        msgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = GCMSMTTransactionDetailBindingSource.Current
            objTransDet.Validate()

            If objTransDet.IsValid Then
                If ContainerForm.PRecordState = frmGCMSMTBase.eRecordState.ENTRY Then
                    objTransDet.Sequenc = ContainerForm.UcGCMSMTTransList1.PGetListMasterIndex
                End If
                'objTransDet.SettlementAccNo = Me.SettlementAccNoValueLabel.Text
                'objTransDet.SettlementAccName1 = Me.SettlementAccName1ValueLabel.Text
                'objTransDet.SettlementAccName2 = Me.SettlementAccName2ValueLabel.Text
                'objTransDet.SettlementAccName3 = Me.SettlementAccName3ValueLabel.Text
                'objTransDet.SettlementAccName4 = Me.SettlementAccName4ValueLabel.Text
                'objTransDet.SettlementBank = Me.SettlementBankValueLabel.Text
                'objTransDet.SettlementBranch = Me.SettlementBranchValueLabel.Text
                'objTransDet.SettlementBankAddress1 = Me.SettlementBankAddress1ValueLabel.Text
                'objTransDet.SettlementBankAddress2 = Me.SettlementBankAddress2ValueLabel.Text
                objTransDet.ApplyEdit()
                'ResetAllBindings(True)
                ContainerForm.ShowTransactionList("Detail")
                ContainerForm.UcGCMSMTTransList1.PColDirty = True
            Else
                ValidateAllControl(Me)
                DisplayDetailError(objTransDet)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click

        Dim objTransDet As GCMSMTTransactionDetail
        Dim dlgResult As DialogResult = DialogResult.None

        Try
            objTransDet = GCMSMTTransactionDetailBindingSource.Current

            If objTransDet IsNot Nothing Then
                objTransDet.Validate()

                If objTransDet.IsDirty Then
                    dlgResult = MessageBox.Show("Data has been changed. Do you want to save the data?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3)
                    If dlgResult = DialogResult.Yes Then
                        If objTransDet.IsValid Then
                            objTransDet.ApplyEdit()
                            'ResetAllBindings(True)
                            ContainerForm.ShowTransactionList("Detail")
                            ContainerForm.UcGCMSMTTransList1.PColDirty = True

                        Else
                            ValidateAllControl(Me)
                            DisplayDetailError(objTransDet)
                        End If
                    ElseIf dlgResult = DialogResult.No Then
                        If ContainerForm.PRecordState = frmGCMSMTBase.eRecordState.ENTRY Then
                            ContainerForm.UcGCMSMTTransList1.GCMSMTTransactionDetailCollectionBindingSource.RemoveCurrent()
                        Else
                            objTransDet.CancelEdit()
                            objTransDet.Validate()
                            GCMSMTTransactionDetailBindingSource.DataSource = New GCMSMTTransactionDetail
                        End If
                        ' ResetAllBindings(False)
                        ContainerForm.ShowTransactionList("Detail")
                        TransDetailError.Clear()
                        'GCMSMTTransactionDetailBindingSource.DataSource = Nothing
                    End If
                Else
                    If ContainerForm.PRecordState = frmGCMSMTBase.eRecordState.ENTRY Then
                        ContainerForm.UcGCMSMTTransList1.GCMSMTTransactionDetailCollectionBindingSource.RemoveCurrent()
                    End If
                    'ResetAllBindings(False)
                    ContainerForm.ShowTransactionList("Detail")
                    TransDetailError.Clear()
                End If
            End If

        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    ''' <summary>
    ''' Populate transaction data for edit. 
    ''' </summary>
    ''' <param name="objTransDet">Instance of the selected transaction data</param>
    ''' <remarks></remarks>
    Public Sub PopulateTransactionDetailEdit(ByRef objTransDet As GCMSMTTransactionDetail)

        Try
            objTransDet.UnmarkDirty()
            objTransDet.BeginEdit()

            DisableBankOptionsFields(objTransDet)
            GCMSMTTransactionDetailBindingSource.SuspendBinding()
            GCMSMTTransactionDetailBindingSource.DataSource = objTransDet
            GCMSMTTransactionDetailBindingSource.ResumeBinding()

            ValidateAllControl(Me)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Populate new instance for data entry
    ''' </summary>
    ''' <param name="objTransDet">New instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub PopulateTransactionDetailNew(ByRef objTransDet As GCMSMTTransactionDetail)

        Try
            objTransDet.TemplateID = String.Empty
            objTransDet.IntermediaryBankOptionD = True
            objTransDet.BeneficiaryBankOptionD = True
            objTransDet.UnmarkDirty()

            GCMSMTTransactionDetailBindingSource.DataSource = objTransDet

            If TemplateIDComboBox.Items.Count > 0 Then
                TemplateIDComboBox.SelectedIndex = 0
            End If

            cmbxTemplateID_SelectionChangeCommitted(TemplateIDComboBox, Nothing)
            IntermediaryBankOptionDRadioButton.Checked = True
            BeneficiaryBankOptionDRadioButton.Checked = True

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Bind master datatables to data sources
    ''' </summary>
    ''' <param name="objMaster">Instance of master object</param>
    ''' <remarks></remarks>
    Public Sub SettingBinding(ByVal objMaster As GCMSMTMaster)

        Me.objMaster = objMaster
        PBankChargesDTBindingSource.DataSource = objMaster.PBankChargesDT
        BankChargesComboBox.DisplayMember = "BankChargesDesc"
        BankChargesComboBox.ValueMember = "BankChargesCode"
        BankChargesComboBox.DataSource = PBankChargesDTBindingSource

        PSectorSelectionArrBindingSource.DataSource = objMaster.PSectorSelectionDT
        SectorSelectionComboBox.DisplayMember = "SectorSelection"
        SectorSelectionComboBox.ValueMember = "SectorSelection"
        SectorSelectionComboBox.DataSource = PSectorSelectionArrBindingSource

        PExchangeMethodArrBindingSource.DataSource = objMaster.PExchangeMethodDT
        ExchangeMethodComboBox.DisplayMember = "ExchangeMethod"
        ExchangeMethodComboBox.ValueMember = "ExchangeMethod"
        ExchangeMethodComboBox.DataSource = PExchangeMethodArrBindingSource

        PCurrencyDTBindingSource.DataSource = objMaster.PCurrencyDT
        CurrencyComboBox.DisplayMember = "CurrencyCode"
        CurrencyComboBox.ValueMember = "CurrencyCode"
        CurrencyComboBox.DataSource = PCurrencyDTBindingSource

        PTransactionTemplateDTBindingSource.DataSource = objMaster.PTransactionTemplateDT
        TemplateIDComboBox.DisplayMember = "TemplateID"
        TemplateIDComboBox.ValueMember = "TemplateID"
        TemplateIDComboBox.DataSource = PTransactionTemplateDTBindingSource

        PSettlementDTBindingSource.DataSource = objMaster.PSettlementDT
        SettlementAccNoAndNameComboBox.DisplayMember = "AccountNoAndName"
        SettlementAccNoAndNameComboBox.ValueMember = "AccountNoAndName"
        SettlementAccNoAndNameComboBox.DataSource = PSettlementDTBindingSource

        PChargesAccountDTBindingSource.DataSource = objMaster.PSettlementDT
        ChargesAccNoAndNameComboBox.DisplayMember = "AccountNoAndName"
        ChargesAccNoAndNameComboBox.ValueMember = "AccountNoAndName"
        ChargesAccNoAndNameComboBox.DataSource = PChargesAccountDTBindingSource

        PBeneficiaryBankDTBindingSource.DataSource = objMaster.PBeneficiaryBankDT
        BeneficiaryBankAndBranchComboBox.DisplayMember = "BankAndBranchName"
        BeneficiaryBankAndBranchComboBox.ValueMember = "BankAndBranchName"
        BeneficiaryBankAndBranchComboBox.DataSource = PBeneficiaryBankDTBindingSource

        PIntermediaryDTBindingSource.DataSource = objMaster.PBeneficiaryBankDT
        IntermediaryBankAndBranchComboBox.DisplayMember = "BankAndBranchName"
        IntermediaryBankAndBranchComboBox.ValueMember = "BankAndBranchName"
        IntermediaryBankAndBranchComboBox.DataSource = PIntermediaryDTBindingSource

        PBeneficiaryCustDTBindingSource.DataSource = objMaster.PBeneficiaryCustDT
        BeneficiaryAccNoAndNameComboBox.DisplayMember = "AccountNoAndName"
        BeneficiaryAccNoAndNameComboBox.ValueMember = "AccountNoAndName"
        BeneficiaryAccNoAndNameComboBox.DataSource = PBeneficiaryCustDTBindingSource
    End Sub

    'Private Sub tbxValueDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbxValueDate.KeyPress

    '    Dim KeyAscii As Short = Asc(e.KeyChar)

    '    Select Case KeyAscii
    '        Case Keys.D0 To Keys.D9
    '        Case Keys.Back
    '        Case Else
    '            e.Handled = True
    '    End Select
    'End Sub

    'Private Sub tbxRemitAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbxRemitAmount.KeyPress
    '    Dim KeyAscii As Short = Asc(e.KeyChar)

    '    Select Case KeyAscii
    '        Case Keys.D0 To Keys.D9
    '        Case 44
    '        Case 46
    '        Case Keys.Back
    '        Case Else
    '            e.Handled = True
    '    End Select

    'End Sub

    'Private Sub tbxBeneficiaryAccNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbxBeneficiaryAccNo.KeyPress

    '    Dim KeyAscii As Short = Asc(e.KeyChar)

    '    Select Case KeyAscii
    '        Case Keys.D0 To Keys.D9
    '        Case Keys.Back
    '        Case Else
    '            e.Handled = True
    '    End Select
    'End Sub

    Private Sub Control_KeyPress(ByVal sender As Object, _
                                ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                                        ValueDateTextBox.KeyPress, _
                                        RemitAmountTextBox.KeyPress, _
                                        CustomerReferenceTextBox.KeyPress, _
                                        ContractNoTextBox.KeyPress, _
                                        IntermediaryBankTextBox.KeyPress, _
                                        IntermediaryBranchTextBox.KeyPress, _
                                        IntermediaryBankAddress1TextBox.KeyPress, _
                                        IntermediaryBankAddress2TextBox.KeyPress, _
                                        BeneficiaryAccNoTextBox.KeyPress, _
                                        BeneficiaryBankTextBox.KeyPress, _
                                        BeneficiaryBranchTextBox.KeyPress, _
                                        BeneficiaryBankAddress1TextBox.KeyPress, _
                                        BeneficiaryBankAddress2TextBox.KeyPress, _
                                        BeneficiaryName1TextBox.KeyPress, _
                                        BeneficiaryName2TextBox.KeyPress, _
                                        BeneficiaryName3TextBox.KeyPress, _
                                        BeneficiaryName4TextBox.KeyPress, _
                                        BeneficiaryMsg1TextBox.KeyPress, _
                                        BeneficiaryMsg2TextBox.KeyPress, _
                                        BeneficiaryMsg3TextBox.KeyPress, _
                                        BeneficiaryMsg4TextBox.KeyPress, _
                                        BeneficiaryRemitPurposeTextBox.KeyPress, _
                                        BeneficiaryRemitInfo1TextBox.KeyPress, _
                                        BeneficiaryRemitInfo2TextBox.KeyPress, _
                                        BeneficiaryRemitInfo3TextBox.KeyPress

        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case 34
                MessageBox.Show(msgReader.GetString("E04000490"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                e.Handled = True
            Case Else
        End Select

    End Sub

    Private Sub rbBeneficiaryBankOptionA_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeneficiaryBankOptionARadioButton.CheckedChanged

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current

            If BeneficiaryBankOptionARadioButton.Checked Then
                Me.UnsetBeneficiaryBankDetail(objTransDet)
                objTransDet.BeneficiaryBankOptionA = True
                objTransDet.BeneficiaryBankOptionC = False
                objTransDet.BeneficiaryBankOptionD = False
                Me.DisableBeneficiaryBankFields()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub rbBeneficiaryBankOptionC_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeneficiaryBankOptionCRadioButton.CheckedChanged

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current

            If BeneficiaryBankOptionCRadioButton.Checked Then
                Me.UnsetBeneficiaryBankDetail(objTransDet)
                objTransDet.BeneficiaryBankOptionA = False
                objTransDet.BeneficiaryBankOptionC = True
                objTransDet.BeneficiaryBankOptionD = False
                Me.DisableBeneficiaryBankFields()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub rbBeneficiaryBankOptionD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeneficiaryBankOptionDRadioButton.CheckedChanged
        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current
            If objTransDet IsNot Nothing Then
                If BeneficiaryBankOptionDRadioButton.Checked Then
                    Me.UnsetBeneficiaryBankDetail(objTransDet)
                    objTransDet.BeneficiaryBankOptionA = False
                    objTransDet.BeneficiaryBankOptionC = False
                    objTransDet.BeneficiaryBankOptionD = True
                    Me.EnableBeneficiaryBankFields()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub rbIntermediaryBankOptionA_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles IntermediaryBankOptionARadioButton.CheckedChanged

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current
            If objTransDet IsNot Nothing Then
                If Me.IntermediaryBankOptionARadioButton.Checked Then
                    Me.UnsetIntermediaryBankDetail(objTransDet)
                    objTransDet.IntermediaryBankOptionA = True
                    objTransDet.IntermediaryBankOptionC = False
                    objTransDet.IntermediaryBankOptionD = False
                    Me.DisableIntermediaryBankFields()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub rbIntermediaryBankOptionC_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles IntermediaryBankOptionCRadioButton.CheckedChanged

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current
            If objTransDet IsNot Nothing Then
                If Me.IntermediaryBankOptionCRadioButton.Checked Then
                    Me.UnsetIntermediaryBankDetail(objTransDet)
                    objTransDet.IntermediaryBankOptionA = False
                    objTransDet.IntermediaryBankOptionC = True
                    objTransDet.IntermediaryBankOptionD = False
                    Me.DisableIntermediaryBankFields()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub rbIntermediaryBankOptionD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles IntermediaryBankOptionDRadioButton.CheckedChanged

        Dim objTransDet As GCMSMTTransactionDetail

        Try
            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current
            If objTransDet IsNot Nothing Then
                If Me.IntermediaryBankOptionDRadioButton.Checked Then
                    Me.UnsetIntermediaryBankDetail(objTransDet)
                    objTransDet.IntermediaryBankOptionA = False
                    objTransDet.IntermediaryBankOptionC = False
                    objTransDet.IntermediaryBankOptionD = True
                    Me.EnableIntermediaryBankFields()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbxChargesAccNoAndName_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChargesAccNoAndNameComboBox.SelectionChangeCommitted

        Dim objTransdet As GCMSMTTransactionDetail

        Try

            objTransdet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransdet IsNot Nothing Then

                Dim strSelectedValue As String = Nothing
                Dim drChargeAcc As DataRow = Nothing

                strSelectedValue = ChargesAccNoAndNameComboBox.SelectedValue

                If Not IsNothingOrEmptyString(strSelectedValue) Then
                    drChargeAcc = objMaster.GetSettlementRow(strSelectedValue, "AccountNoAndName")
                    If Not drChargeAcc Is Nothing Then
                        objTransdet.ChargesAcc = drChargeAcc.Item("AccountNo")
                        objTransdet.ChargesAccNoAndName = drChargeAcc.Item("AccountNoAndName")
                        objTransdet.ChargesAccName1 = drChargeAcc.Item("AccountName1")
                        objTransdet.ChargesAccName2 = drChargeAcc.Item("AccountName2")
                        objTransdet.ChargesAccName3 = drChargeAcc.Item("AccountName3")
                        objTransdet.ChargesAccName4 = drChargeAcc.Item("AccountName4")
                    Else
                        Me.ResetChargeAccDetail(objTransdet)
                    End If
                Else
                    Me.ResetChargeAccDetail(objTransdet)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbxBeneficiaryAccNoAndName_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeneficiaryAccNoAndNameComboBox.SelectionChangeCommitted

        Dim drBeneficiaryCust As DataRow = Nothing
        Dim objTransdet As GCMSMTTransactionDetail
        Dim strSelectedValue As String = Nothing

        Try

            objTransdet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransdet IsNot Nothing Then

                strSelectedValue = BeneficiaryAccNoAndNameComboBox.SelectedValue
                If Not IsNothingOrEmptyString(strSelectedValue) Then
                    drBeneficiaryCust = objMaster.GetBeneficiaryCustRow(strSelectedValue)
                    If Not drBeneficiaryCust Is Nothing Then
                        objTransdet.BeneficiaryAccNoAndName = drBeneficiaryCust.Item("AccountNoAndName")
                        objTransdet.BeneficiaryAccNo = drBeneficiaryCust.Item("AccountNo")
                        objTransdet.BeneficiaryName1 = drBeneficiaryCust.Item("AccountName1")
                        objTransdet.BeneficiaryName2 = drBeneficiaryCust.Item("AccountName2")
                        objTransdet.BeneficiaryName3 = drBeneficiaryCust.Item("AccountName3")
                        objTransdet.BeneficiaryName4 = drBeneficiaryCust.Item("AccountName4")
                        ValidateAllControl(Me)
                    Else
                        Me.ResetBeneficiaryAccNoDetail(objTransdet)
                    End If
                Else
                    Me.ResetBeneficiaryAccNoDetail(objTransdet)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbxBeneficiaryBankAndBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeneficiaryBankAndBranchComboBox.SelectionChangeCommitted

        Dim objTransdet As GCMSMTTransactionDetail

        Try

            objTransdet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransdet IsNot Nothing Then

                Dim strSelectedValue As String = Nothing
                Dim drBeneficiaryBank As DataRow = Nothing

                strSelectedValue = BeneficiaryBankAndBranchComboBox.SelectedValue

                If Not IsNothingOrEmptyString(strSelectedValue) Then
                    drBeneficiaryBank = objMaster.GetBeneficiaryBankRow(strSelectedValue)
                    If Not drBeneficiaryBank Is Nothing Then
                        objTransdet.BeneficiaryBankAndBranch = drBeneficiaryBank.Item("BankAndBranchName")
                        objTransdet.BeneficiaryBank = drBeneficiaryBank.Item("BankName")
                        objTransdet.BeneficiaryBranch = drBeneficiaryBank.Item("BranchName")
                        objTransdet.BeneficiaryBankAddress1 = drBeneficiaryBank.Item("BankAddress1")
                        objTransdet.BeneficiaryBankAddress2 = drBeneficiaryBank.Item("BankAddress2")
                        objTransdet.BeneficiaryBankMasterCode = drBeneficiaryBank.Item("BankMasterCode")
                    Else
                        Me.ResetBeneficiaryBankAndBranchDetail(objTransdet)
                    End If
                Else
                    Me.ResetBeneficiaryBankAndBranchDetail(objTransdet)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbxIntermediaryBankAndBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles IntermediaryBankAndBranchComboBox.SelectionChangeCommitted

        Dim drIntermediaryBank As DataRow = Nothing
        Dim objTransdet As GCMSMTTransactionDetail
        Dim strSelectedValue As String = Nothing

        Try

            objTransdet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransdet IsNot Nothing Then

                strSelectedValue = IntermediaryBankAndBranchComboBox.SelectedValue

                If Not IsNothingOrEmptyString(strSelectedValue) Then
                    drIntermediaryBank = objMaster.GetBeneficiaryBankRow(strSelectedValue)
                    If Not drIntermediaryBank Is Nothing Then
                        objTransdet.IntermediaryBankAndBranch = drIntermediaryBank.Item("BankAndBranchName")
                        objTransdet.IntermediaryBank = drIntermediaryBank.Item("BankName")
                        objTransdet.IntermediaryBranch = drIntermediaryBank.Item("BranchName")
                        objTransdet.IntermediaryBankAddress1 = drIntermediaryBank.Item("BankAddress1")
                        objTransdet.IntermediaryBankAddress2 = drIntermediaryBank.Item("BankAddress2")
                        objTransdet.IntermediaryBankMasterCode = drIntermediaryBank.Item("BankMasterCode")
                    Else
                        Me.ResetIntermediaryBankAndBranchDetail(objTransdet)
                    End If
                Else
                    Me.ResetIntermediaryBankAndBranchDetail(objTransdet)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbxTemplateID_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles TemplateIDComboBox.SelectionChangeCommitted

        Dim objTransdet As GCMSMTTransactionDetail

        Try
            objTransdet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransdet IsNot Nothing Then
                Dim strSelectedValue As String = Nothing
                Dim drTemplate As DataRow = Nothing
                Dim drRow As DataRow = Nothing
                Dim index As Integer = 0

                strSelectedValue = TemplateIDComboBox.SelectedValue
                drTemplate = objMaster.GetTemplateRow(strSelectedValue)

                If drTemplate IsNot Nothing Then

                    objTransdet.TemplateID = drTemplate.Item("TemplateID")
                    objTransdet.ValueDate = Nothing
                    objTransdet.SettlementAccNo = drTemplate.Item("SettlementAccNo")
                    drRow = objMaster.GetSettlementRow(drTemplate.Item("SettlementAccNo"), "AccountNo") 'Retrieve from master
                    If drRow IsNot Nothing Then
                        objTransdet.SettlementAccNoAndName = drRow.Item("AccountNoAndName")
                        objTransdet.SettlementBank = drRow.Item("Bank")
                        objTransdet.SettlementBranch = drRow.Item("Branch")
                        objTransdet.SettlementBankAddress1 = drRow.Item("BankAddress1")
                        objTransdet.SettlementBankAddress2 = drRow.Item("BankAddress2")
                        objTransdet.SettlementAccCurrency = drRow.Item("CurrencyCode")
                    Else
                        objTransdet.SettlementAccNoAndName = String.Empty
                        objTransdet.SettlementBank = String.Empty
                        objTransdet.SettlementBranch = String.Empty
                        objTransdet.SettlementBankAddress1 = String.Empty
                        objTransdet.SettlementBankAddress2 = String.Empty
                        objTransdet.SettlementAccCurrency = String.Empty
                    End If
                    objTransdet.SettlementAccName1 = drTemplate.Item("SettlementAccName1")
                    objTransdet.SettlementAccName2 = drTemplate.Item("SettlementAccName2")
                    objTransdet.SettlementAccName3 = drTemplate.Item("SettlementAccName3")
                    objTransdet.SettlementAccName4 = drTemplate.Item("SettlementAccName4")

                    objTransdet.CustomerReference = drTemplate.Item("CustomerRef")
                    objTransdet.SectorSelection = drTemplate.Item("SectorSelection")
                    objTransdet.Currency = drTemplate.Item("Currency")
                    objTransdet.RemitAmount = drTemplate.Item("RemitAmount")
                    objTransdet.ExchangeMethod = drTemplate.Item("ExchangeMethod")
                    objTransdet.ContractNo = drTemplate.Item("ContractNo")

                    drRow = objMaster.GetBankAndBranchRow(drTemplate.Item("IntermediaryBank"), drTemplate.Item("IntermediaryBranch")) 'Retrieve from master
                    If drRow IsNot Nothing Then
                        objTransdet.IntermediaryBankAndBranch = drRow.Item("BankAndBranchName")
                    End If

                    'Intermediary options
                    If drTemplate.Item("IntermediaryBankOption") IsNot Nothing Then
                        If drTemplate.Item("IntermediaryBankOption") = ContainerForm.PTemplateOption_D Then
                            objTransdet.IntermediaryBankOptionA = False
                            objTransdet.IntermediaryBankOptionC = False
                            objTransdet.IntermediaryBankOptionD = True
                            Me.IntermediaryBankOptionDRadioButton.Checked = True '--
                            objTransdet.IntermediaryBank = drTemplate.Item("IntermediaryBank")
                            objTransdet.IntermediaryBranch = drTemplate.Item("IntermediaryBranch")
                            objTransdet.IntermediaryBankAddress1 = drTemplate.Item("IntermediaryBankAddress1")
                            objTransdet.IntermediaryBankAddress2 = drTemplate.Item("IntermediaryBankAddress2")
                            Me.EnableIntermediaryBankFields()
                        ElseIf drTemplate.Item("IntermediaryBankOption") = ContainerForm.PTemplateOption_C Then
                            objTransdet.IntermediaryBankOptionA = False
                            objTransdet.IntermediaryBankOptionC = True
                            objTransdet.IntermediaryBankOptionD = False
                            Me.IntermediaryBankOptionCRadioButton.Checked = True '--
                            objTransdet.IntermediaryBank = drTemplate.Item("IntermediaryBankNCC")
                            objTransdet.IntermediaryBranch = String.Empty
                            objTransdet.IntermediaryBankAddress1 = String.Empty
                            objTransdet.IntermediaryBankAddress2 = String.Empty
                            Me.DisableIntermediaryBankFields()
                        ElseIf drTemplate.Item("IntermediaryBankOption") = ContainerForm.PTemplateOption_A Then
                            objTransdet.IntermediaryBankOptionA = True
                            objTransdet.IntermediaryBankOptionC = False
                            objTransdet.IntermediaryBankOptionD = False
                            Me.IntermediaryBankOptionARadioButton.Checked = True '--
                            objTransdet.IntermediaryBank = drTemplate.Item("IntermediaryBankSWIFTBIC")
                            objTransdet.IntermediaryBranch = String.Empty
                            objTransdet.IntermediaryBankAddress1 = String.Empty
                            objTransdet.IntermediaryBankAddress2 = String.Empty
                            Me.DisableIntermediaryBankFields()
                        Else
                            objTransdet.IntermediaryBankOptionA = False
                            objTransdet.IntermediaryBankOptionC = False
                            objTransdet.IntermediaryBankOptionD = True
                            Me.IntermediaryBankOptionDRadioButton.Checked = True '--
                            objTransdet.IntermediaryBank = String.Empty
                            objTransdet.IntermediaryBranch = String.Empty
                            objTransdet.IntermediaryBankAddress1 = String.Empty
                            objTransdet.IntermediaryBankAddress2 = String.Empty
                            Me.EnableIntermediaryBankFields()
                        End If
                    End If

                    objTransdet.IntermediaryBankMasterCode = drTemplate.Item("IntermediaryBankMasterCode")

                    drRow = objMaster.GetBankAndBranchRow(drTemplate.Item("BeneficiaryBank"), drTemplate.Item("BeneficiaryBranch")) 'Retrieve from master
                    If drRow IsNot Nothing Then
                        objTransdet.BeneficiaryBankAndBranch = drRow.Item("BankAndBranchName")
                    End If

                    objTransdet.BeneficiaryBankMasterCode = drTemplate.Item("BeneficiaryBankMasterCode")

                    'Beneficiary options 
                    If drTemplate.Item("BeneficiaryBankOption") IsNot Nothing Then
                        If drTemplate.Item("BeneficiaryBankOption") = ContainerForm.PTemplateOption_D Then
                            objTransdet.BeneficiaryBankOptionA = False
                            objTransdet.BeneficiaryBankOptionC = False
                            objTransdet.BeneficiaryBankOptionD = True
                            objTransdet.BeneficiaryBank = drTemplate.Item("BeneficiaryBank")
                            objTransdet.BeneficiaryBranch = drTemplate.Item("BeneficiaryBranch")
                            objTransdet.BeneficiaryBankAddress1 = drTemplate.Item("BeneficiaryBankAddress1")
                            objTransdet.BeneficiaryBankAddress2 = drTemplate.Item("BeneficiaryBankAddress2")
                            Me.EnableBeneficiaryBankFields()
                        ElseIf drTemplate.Item("BeneficiaryBankOption") = ContainerForm.PTemplateOption_C Then
                            objTransdet.BeneficiaryBankOptionA = False
                            objTransdet.BeneficiaryBankOptionC = True
                            objTransdet.BeneficiaryBankOptionD = False
                            objTransdet.BeneficiaryBank = drTemplate.Item("BeneficiaryBankNCC")
                            objTransdet.BeneficiaryBranch = String.Empty
                            objTransdet.BeneficiaryBankAddress1 = String.Empty
                            objTransdet.BeneficiaryBankAddress2 = String.Empty
                            Me.DisableBeneficiaryBankFields()
                        ElseIf drTemplate.Item("BeneficiaryBankOption") = ContainerForm.PTemplateOption_A Then
                            objTransdet.BeneficiaryBankOptionA = True
                            objTransdet.BeneficiaryBankOptionC = False
                            objTransdet.BeneficiaryBankOptionD = False
                            objTransdet.BeneficiaryBank = drTemplate.Item("BeneficiaryBankSWIFTBIC")
                            objTransdet.BeneficiaryBranch = String.Empty
                            objTransdet.BeneficiaryBankAddress1 = String.Empty
                            objTransdet.BeneficiaryBankAddress2 = String.Empty
                            Me.DisableBeneficiaryBankFields()
                        Else
                            objTransdet.BeneficiaryBankOptionA = False
                            objTransdet.BeneficiaryBankOptionC = False
                            objTransdet.BeneficiaryBankOptionD = True
                            objTransdet.BeneficiaryBank = String.Empty
                            objTransdet.BeneficiaryBranch = String.Empty
                            objTransdet.BeneficiaryBankAddress1 = String.Empty
                            objTransdet.BeneficiaryBankAddress2 = String.Empty
                            Me.EnableBeneficiaryBankFields()
                        End If
                    End If

                    drRow = objMaster.GetAccNoAndNameRow(drTemplate.Item("BeneficiaryAccNo"), drTemplate.Item("BeneficiaryName1")) 'Retrieve from master
                    If drRow IsNot Nothing Then
                        objTransdet.BeneficiaryBankAndBranch = drRow.Item("AccountNoAndName")
                    End If
                    objTransdet.BeneficiaryAccNo = drTemplate.Item("BeneficiaryAccNo")
                    objTransdet.BeneficiaryName1 = drTemplate.Item("BeneficiaryName1")
                    objTransdet.BeneficiaryName2 = drTemplate.Item("BeneficiaryName2")
                    objTransdet.BeneficiaryName3 = drTemplate.Item("BeneficiaryName3")
                    objTransdet.BeneficiaryName4 = drTemplate.Item("BeneficiaryName4")

                    objTransdet.BeneficiaryMsg1 = drTemplate.Item("BeneficiaryMsg1")
                    objTransdet.BeneficiaryMsg2 = drTemplate.Item("BeneficiaryMsg2")
                    objTransdet.BeneficiaryMsg3 = drTemplate.Item("BeneficiaryMsg3")
                    objTransdet.BeneficiaryMsg4 = drTemplate.Item("BeneficiaryMsg4")

                    objTransdet.BeneficiaryRemitPurpose = drTemplate.Item("BeneficiaryRemitPurpose")
                    objTransdet.BeneficiaryRemitInfo1 = drTemplate.Item("BeneficiaryRemitInfo1")
                    objTransdet.BeneficiaryRemitInfo2 = drTemplate.Item("BeneficiaryRemitInfo2")
                    objTransdet.BeneficiaryRemitInfo3 = drTemplate.Item("BeneficiaryRemitInfo3")

                    objTransdet.BankCharges = drTemplate.Item("BankCharges")

                    drRow = objMaster.GetSettlementRow(drTemplate.Item("ChargesAcc"), "AccountNo")
                    If drRow IsNot Nothing Then
                        objTransdet.ChargesAccNoAndName = drRow.Item("AccountNoAndName")
                    End If
                    objTransdet.ChargesAccName1 = drTemplate.Item("ChargesAccName1")
                    objTransdet.ChargesAccName2 = drTemplate.Item("ChargesAccName2")
                    objTransdet.ChargesAccName3 = drTemplate.Item("ChargesAccName3")
                    objTransdet.ChargesAccName4 = drTemplate.Item("ChargesAccName4")

                    ValidateAllControl(Me)

                End If
            End If

        Catch magicEx As MagicException
            MessageBox.Show(magicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmbxSettlementAccNoAndName_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles SettlementAccNoAndNameComboBox.SelectionChangeCommitted

        Dim objTransDet As GCMSMTTransactionDetail = Nothing

        Try

            objTransDet = Me.GCMSMTTransactionDetailBindingSource.Current

            If objTransDet IsNot Nothing Then

                Dim strSelectedValue As String = Nothing
                Dim drSettlementBank As DataRow = Nothing

                strSelectedValue = SettlementAccNoAndNameComboBox.SelectedValue

                If Not IsNothingOrEmptyString(strSelectedValue) Then
                    drSettlementBank = objMaster.GetSettlementRow(strSelectedValue, "AccountNoAndName")
                    If Not drSettlementBank Is Nothing Then
                        objTransDet.SettlementAccNo = drSettlementBank.Item("AccountNo")
                        objTransDet.SettlementAccNoAndName = drSettlementBank.Item("AccountNoAndName")
                        objTransDet.SettlementAccName1 = drSettlementBank.Item("AccountName1")
                        objTransDet.SettlementAccName2 = drSettlementBank.Item("AccountName2")
                        objTransDet.SettlementAccName3 = drSettlementBank.Item("AccountName3")
                        objTransDet.SettlementAccName4 = drSettlementBank.Item("AccountName4")
                        objTransDet.SettlementBank = drSettlementBank.Item("Bank")
                        objTransDet.SettlementBranch = drSettlementBank.Item("Branch")
                        objTransDet.SettlementBankAddress1 = drSettlementBank.Item("BankAddress1")
                        objTransDet.SettlementBankAddress2 = drSettlementBank.Item("BankAddress2")
                        objTransDet.SettlementAccCurrency = drSettlementBank.Item("CurrencyCode")
                    Else
                        Me.ResetSettlementAccNoAndNameDetail(objTransDet)
                    End If
                Else
                    Me.ResetSettlementAccNoAndNameDetail(objTransDet)
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Control_Validated(ByVal sender As Object, _
        ByVal e As System.EventArgs) Handles ValueDateTextBox.Validated, _
                                     SettlementAccNoAndNameComboBox.Validated, _
                                     CustomerReferenceTextBox.Validated, _
                                     SectorSelectionComboBox.Validated, _
                                     CurrencyComboBox.Validated, _
                                     RemitAmountTextBox.Validated, _
                                     BeneficiaryName1TextBox.Validated, _
                                     BankChargesComboBox.Validated, _
                                     ExchangeMethodComboBox.Validated, _
                                     ContractNoTextBox.Validated, _
                                     IntermediaryBankTextBox.Validated, _
                                     BeneficiaryBankTextBox.Validated, _
                                     ChargesAccNoAndNameComboBox.Validated

        Dim ctl As Control = CType(sender, Control)
        Dim bnd As Binding
        Dim objTransDetail As GCMSMTTransactionDetail

        objTransDetail = GCMSMTTransactionDetailBindingSource.Current


        'If cmbxCurrency.Name = ctl.Name Then
        '    Dim test As System.ComponentModel.IDataErrorInfo = _
        '          CType(objTransDetail, System.ComponentModel.IDataErrorInfo)
        '    ErrorProvider1.SetError(cmbxExchangeMethod, test.Item("ExchangeMethod"))
        'End If

        For Each bnd In ctl.DataBindings
            If bnd.IsBinding Then
                Dim obj As System.ComponentModel.IDataErrorInfo = _
                  CType(objTransDetail, System.ComponentModel.IDataErrorInfo)
                TransDetailError.SetError( _
                  ctl, obj.Item(bnd.BindingMemberInfo.BindingField))
            End If
        Next

    End Sub

    Private Sub ValidateAllControl(ByVal ctl As Control)
        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                ValidateAllControl(ctlDetail)
            Else
                Control_Validated(ctlDetail, Nothing)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Unset beneficiary values to blank
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub UnsetBeneficiaryBankDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.BeneficiaryBankAndBranch = String.Empty
        objTransDet.BeneficiaryBank = String.Empty
        objTransDet.BeneficiaryBranch = String.Empty
        objTransDet.BeneficiaryBankAddress1 = String.Empty
        objTransDet.BeneficiaryBankAddress2 = String.Empty
    End Sub

    ''' <summary>
    ''' Unset intermediary values to blank
    ''' </summary>
    ''' <param name="objTransDet"></param>
    ''' <remarks></remarks>
    Public Sub UnsetIntermediaryBankDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.IntermediaryBankAndBranch = String.Empty
        objTransDet.IntermediaryBank = String.Empty
        objTransDet.IntermediaryBranch = String.Empty
        objTransDet.IntermediaryBankAddress1 = String.Empty
        objTransDet.IntermediaryBankAddress2 = String.Empty
    End Sub

    ''' <summary>
    ''' Display consolidated error messages.
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub DisplayDetailError(ByRef objTransDet As GCMSMTTransactionDetail)

        Dim errorInfo As System.ComponentModel.IDataErrorInfo
        Dim errorMessage As String
        Dim fistError As New System.Text.StringBuilder
        Dim detailError As New System.Text.StringBuilder

        errorInfo = CType(objTransDet, System.ComponentModel.IDataErrorInfo)
        errorMessage = errorInfo.Error

        If Not IsNothingOrEmptyString(errorMessage) Then

            Dim errorList As String() = errorMessage.Split(Environment.NewLine)
            Dim temp As String

            For Each temp In errorList
                If temp.Trim <> String.Empty Then
                    If fistError.Length > 0 Then
                        If (temp.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", temp.Trim))
                    Else
                        If (temp.Trim.Length > 0) Then fistError.AppendLine(String.Format("{0}", temp.Trim))
                    End If
                End If
            Next
        End If

        BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(fistError.ToString, detailError.ToString)

    End Sub

    '    Public Sub ResetAllBindings(ByVal boolReset As Boolean)
    '        SetBindingSourceEvent()
    '        RemoveCheckedChangeHandler()
    '        GCMSMTTransactionDetailBindingSource.ResetBindings(False)
    '        ContainerForm.UcGCMSMTTransList1.GCMSMTTransactionDetailCollectionBindingSource.ResetBindings(False)
    '        AddCheckedChangeHandler()
    '        ContainerForm.ShowTransactionList("Detail")
    '    End Sub

    '    Public Sub SetBindingSourceEvent()
    '        GCMSMTTransactionDetailBindingSource.RaiseListChangedEvents = True
    '        ContainerForm.UcGCMSMTTransList1.GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = True
    '    End Sub

    '    Public Sub ResetBindingSourceEvent()
    '        GCMSMTTransactionDetailBindingSource.RaiseListChangedEvents = False
    '        ContainerForm.UcGCMSMTTransList1.GCMSMTTransactionDetailCollectionBindingSource.RaiseListChangedEvents = False
    '    End Sub

    '    Public Sub ResetBindingAfterUpdate()
    '        Me.GCMSMTTransactionDetailBindingSource.RaiseListChangedEvents = True
    '        Me.GCMSMTTransactionDetailBindingSource.ResetBindings(False)
    '        Me.GCMSMTTransactionDetailBindingSource.RaiseListChangedEvents = False
    '    End Sub

    '    Public Sub ResetTemplateMasterDetail(ByRef objTransDet As GCMSMTTransactionDetail)

    '        If objTransDet IsNot Nothing Then

    '            Dim propInfo As System.Reflection.PropertyInfo
    '            Dim propInfoCollection As System.Reflection.PropertyInfo()

    '            Try

    '                propInfoCollection = objTransDet.GetType.GetProperties()

    '                For Each propInfo In propInfoCollection
    '                    If propInfo.PropertyType Is GetType(Boolean) Then
    '                        propInfo.SetValue(objTransDet, False, Nothing)
    '                    ElseIf propInfo.PropertyType Is GetType(Double) Then
    '                        propInfo.SetValue(objTransDet, 0, Nothing)
    '                    ElseIf propInfo.PropertyType Is GetType(String) Then
    '                        propInfo.SetValue(objTransDet, String.Empty, Nothing)
    '                    End If
    '                Next

    '            Catch ex As Exception
    '                Throw New MagicException(ex.Message)
    '            End Try
    '        End If

    '    End Sub

    ''' <summary>
    ''' Disable and reset related controls when bank option is selected
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub DisableBankOptionsFields(ByVal objTransDet As GCMSMTTransactionDetail)
        If objTransDet IsNot Nothing Then
            'Intermediary
            If objTransDet.IntermediaryBankOptionA Then
                Me.DisableIntermediaryBankFields()
                IntermediaryBankOptionARadioButton.Checked = True
            ElseIf objTransDet.IntermediaryBankOptionC Then
                Me.DisableIntermediaryBankFields()
                IntermediaryBankOptionCRadioButton.Checked = True
            Else
                Me.EnableIntermediaryBankFields()
                IntermediaryBankOptionDRadioButton.Checked = True
            End If
            'Beneficiary
            If objTransDet.BeneficiaryBankOptionA Then
                Me.DisableBeneficiaryBankFields()
                BeneficiaryBankOptionARadioButton.Checked = True
            ElseIf objTransDet.BeneficiaryBankOptionC Then
                Me.DisableBeneficiaryBankFields()
                BeneficiaryBankOptionCRadioButton.Checked = True
            Else
                Me.EnableBeneficiaryBankFields()
                BeneficiaryBankOptionDRadioButton.Checked = True
            End If
        End If
    End Sub

    ''' <summary>
    ''' Reset settlement related controls values
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub ResetSettlementAccNoAndNameDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.SettlementAccNoAndName = String.Empty
        objTransDet.SettlementAccNo = String.Empty
        objTransDet.SettlementAccName1 = String.Empty
        objTransDet.SettlementAccName2 = String.Empty
        objTransDet.SettlementAccName3 = String.Empty
        objTransDet.SettlementAccName4 = String.Empty
        objTransDet.SettlementBank = String.Empty
        objTransDet.SettlementBranch = String.Empty
        objTransDet.SettlementBankAddress1 = String.Empty
        objTransDet.SettlementBankAddress2 = String.Empty
        objTransDet.SettlementAccCurrency = String.Empty
    End Sub

    ''' <summary>
    ''' Reset intermediary related controls values
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub ResetIntermediaryBankAndBranchDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.IntermediaryBankAndBranch = String.Empty
        objTransDet.IntermediaryBank = String.Empty
        objTransDet.IntermediaryBranch = String.Empty
        objTransDet.IntermediaryBankAddress1 = String.Empty
        objTransDet.IntermediaryBankAddress2 = String.Empty
    End Sub

    ''' <summary>
    ''' Reset beneficiary bank and branch related controls values
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub ResetBeneficiaryBankAndBranchDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.BeneficiaryBankAndBranch = String.Empty
        objTransDet.BeneficiaryBank = String.Empty
        objTransDet.BeneficiaryBranch = String.Empty
        objTransDet.BeneficiaryBankAddress1 = String.Empty
        objTransDet.BeneficiaryBankAddress2 = String.Empty
    End Sub

    ''' <summary>
    ''' Reset beneficiary account and name related controls values
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub ResetBeneficiaryAccNoDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.BeneficiaryAccNoAndName = String.Empty
        objTransDet.BeneficiaryAccNo = String.Empty
        objTransDet.BeneficiaryName1 = String.Empty
        objTransDet.BeneficiaryName2 = String.Empty
        objTransDet.BeneficiaryName3 = String.Empty
        objTransDet.BeneficiaryName4 = String.Empty
    End Sub

    ''' <summary>
    ''' Reset charges account related controls values
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub ResetChargeAccDetail(ByRef objTransDet As GCMSMTTransactionDetail)
        objTransDet.ChargesAccNoAndName = String.Empty
        objTransDet.ChargesAcc = String.Empty
        objTransDet.ChargesAccName1 = String.Empty
        objTransDet.ChargesAccName2 = String.Empty
        objTransDet.ChargesAccName3 = String.Empty
        objTransDet.ChargesAccName4 = String.Empty
    End Sub

    ''' <summary>
    ''' Disable intermediary bank and branch related controls
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub EnableIntermediaryBankFields()
        Me.IntermediaryBankAndBranchComboBox.Enabled = True
        Me.IntermediaryBranchTextBox.Enabled = True
        Me.IntermediaryBankAddress1TextBox.Enabled = True
        Me.IntermediaryBankAddress2TextBox.Enabled = True
    End Sub

    ''' <summary>
    ''' Disable intermediary bank and branch related controls
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DisableIntermediaryBankFields()
        Me.IntermediaryBankAndBranchComboBox.Enabled = False
        Me.IntermediaryBranchTextBox.Enabled = False
        Me.IntermediaryBankAddress1TextBox.Enabled = False
        Me.IntermediaryBankAddress2TextBox.Enabled = False
    End Sub

    ''' <summary>
    ''' Disable beneficiary bank and branch related controls
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub EnableBeneficiaryBankFields()
        Me.BeneficiaryBankAndBranchComboBox.Enabled = True
        Me.BeneficiaryBranchTextBox.Enabled = True
        Me.BeneficiaryBankAddress1TextBox.Enabled = True
        Me.BeneficiaryBankAddress2TextBox.Enabled = True
    End Sub

    ''' <summary>
    ''' Disable beneficiary bank and branch related controls
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DisableBeneficiaryBankFields()
        Me.BeneficiaryBankAndBranchComboBox.Enabled = False
        Me.BeneficiaryBranchTextBox.Enabled = False
        Me.BeneficiaryBankAddress1TextBox.Enabled = False
        Me.BeneficiaryBankAddress2TextBox.Enabled = False
    End Sub

    '    'Private Sub cmbxCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxCurrency.SelectedIndexChanged

    '    '    Dim objTransDet As GCMSMTTransactionDetail
    '    '    objTransDet = GCMSMTTransactionDetailBindingSource.Current

    '    '    If objTransDet IsNot Nothing Then
    '    '        Select Case objTransDet.Currency
    '    '            Case "IDR", "JPY"
    '    '                remittanceAmount.FormattingEnabled = True
    '    '                remittanceAmount.FormatString = "#,##0"
    '    '                'tbxRemitAmount.Text = Format(objTransDet.RemitAmount, "#,##0")
    '    '            Case Else
    '    '                remittanceAmount.FormattingEnabled = True
    '    '                remittanceAmount.FormatString = "#,##0.00"
    '    '                ' tbxRemitAmount.Text = Format(objTransDet.RemitAmount, "#,##0.00")
    '    '        End Select
    '    '    End If
    '    'End Sub

    '    Public Sub RemoveCheckedChangeHandler()
    '        RemoveHandler rbIntermediaryBankOptionA.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionA_CheckedChanged
    '        RemoveHandler rbIntermediaryBankOptionC.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionC_CheckedChanged
    '        RemoveHandler rbIntermediaryBankOptionD.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionD_CheckedChanged
    '        RemoveHandler rbBeneficiaryBankOptionA.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionA_CheckedChanged
    '        RemoveHandler rbBeneficiaryBankOptionC.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionC_CheckedChanged
    '        RemoveHandler rbBeneficiaryBankOptionD.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionD_CheckedChanged
    '    End Sub

    '    Public Sub AddCheckedChangeHandler()
    '        AddHandler rbIntermediaryBankOptionA.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionA_CheckedChanged
    '        AddHandler rbIntermediaryBankOptionC.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionC_CheckedChanged
    '        AddHandler rbIntermediaryBankOptionD.CheckedChanged, AddressOf Me.rbIntermediaryBankOptionD_CheckedChanged
    '        AddHandler rbBeneficiaryBankOptionA.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionA_CheckedChanged
    '        AddHandler rbBeneficiaryBankOptionC.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionC_CheckedChanged
    '        AddHandler rbBeneficiaryBankOptionD.CheckedChanged, AddressOf Me.rbBeneficiaryBankOptionD_CheckedChanged
    '    End Sub

    ''' <summary>
    ''' Validate all controls when settlement account and name value is selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbxSettlementAccNoAndName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles SettlementAccNoAndNameComboBox.Validated
        ValidateAllControl(Me)
    End Sub

    ''' <summary>
    ''' Validate all controls when currency value is selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbxCurrency_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CurrencyComboBox.Validated
        ValidateAllControl(Me)
    End Sub

    ''' <summary>
    ''' Validate all controls when exchange method value is selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbxExchangeMethod_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExchangeMethodComboBox.Validated
        ValidateAllControl(Me)
    End Sub

    ''' <summary>
    ''' Validate all controls when bank charges value is selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbxBankCharges_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BankChargesComboBox.Validated
        ValidateAllControl(Me)
    End Sub

    ''' <summary>
    ''' Validate all controls when charges account and name value is selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbxchargesAccNoAndName_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChargesAccNoAndNameComboBox.Validated
        ValidateAllControl(Me)
    End Sub

End Class