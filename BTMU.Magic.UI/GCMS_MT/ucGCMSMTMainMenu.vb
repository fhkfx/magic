Imports BTMU.MAGIC.Common

''' <summary>
''' User control for GCMS-MT main menu
''' </summary>
''' <remarks></remarks>
Public Class ucGCMSMTMainMenu

    Private Sub btnMainExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMainExit.Click
        'Garbage collection?
        ContainerForm.Close()
    End Sub

    Private Sub btnMainCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMainCreate.Click
        ContainerForm.ShowTransactionList("Create")
        ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.CREATE
    End Sub

    Private Sub btnMainConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ContainerForm.ShowConfig()
    End Sub

    Private Sub btnMainEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMainEdit.Click

        Dim boolProceed As Boolean

        Try
            ContainerForm.UcGCMSMTTransList1.PopulateTransactionList(boolProceed)
            If boolProceed Then
                ContainerForm.ShowTransactionList("Edit")
                ContainerForm.PModeState = frmGCMSMTBase.eMainModeState.EDIT
            End If
        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

End Class
