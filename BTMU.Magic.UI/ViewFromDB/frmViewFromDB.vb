Imports System.Data.SqlServerCe
Imports System.IO
Imports BTMU.Magic.Common

''' <summary>
''' View database content 
''' </summary>
''' <remarks></remarks>
Public Class frmViewFromDB

    Private Shared strDBPath As String = Path.Combine(My.Settings.MagicDB, "Excel2DB.sdf")
    Private Shared strConnectionString As String = String.Format("Data Source = ""{0}"";Password='btmu123$';", strDBPath)
    Private Shared MsgReader As Global.System.Resources.ResourceManager

    Private Sub frmViewFromDB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Resource object to retrieve messages stored in application resosurces.
        MsgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

        Me.Left = (Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2
        Me.Top = 50

    End Sub

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click

        Me.Close()

    End Sub

    Private Sub ViewButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewButton.Click

        Dim dbCEConnectionString As String = String.Empty
        Dim dbCEConnection As SqlCeConnection = Nothing
        Dim dbCECommand As SqlCeCommand = Nothing

        Try
            If Not File.Exists(strDBPath) Then
                Throw New MagicException(MsgReader.GetString("E00041001"))
            End If

            If Me.TableComboBox.SelectedItem = "" Then
                Throw New MagicException(MsgReader.GetString("E00041002"))
            End If

            dbCEConnectionString = strConnectionString
            dbCEConnection = New SqlCeConnection(dbCEConnectionString)
            dbCEConnection.Open()

            Dim SQLString As String = "SELECT * FROM " & Me.TableComboBox.SelectedItem
            Dim SqlCEDataAdapter As New SqlCeDataAdapter(SQLString, dbCEConnection)
            Dim DataSet As New DataSet()
            SqlCEDataAdapter.Fill(DataSet, Me.TableComboBox.SelectedItem)
            DataGridView.DataSource = DataSet.Tables(Me.TableComboBox.SelectedItem)

        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If Not dbCEConnection Is Nothing AndAlso dbCEConnection.State = ConnectionState.Open Then
                dbCEConnection.Close()
            End If
            dbCEConnection = Nothing
        End Try

    End Sub

    Private Sub DataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView.CellContentClick

    End Sub

    Private Sub TableComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TableComboBox.SelectedIndexChanged

    End Sub
End Class