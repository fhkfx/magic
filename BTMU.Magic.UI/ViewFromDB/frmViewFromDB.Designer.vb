<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewFromDB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableGroupBox = New System.Windows.Forms.GroupBox
        Me.TableComboBox = New System.Windows.Forms.ComboBox
        Me.ViewButton = New System.Windows.Forms.Button
        Me.DBTableLabel = New System.Windows.Forms.Label
        Me.GridViewGroupBox = New System.Windows.Forms.GroupBox
        Me.DataGridView = New System.Windows.Forms.DataGridView
        Me.ButtonGroupBox = New System.Windows.Forms.GroupBox
        Me.CloseButton = New System.Windows.Forms.Button
        Me.TableGroupBox.SuspendLayout()
        Me.GridViewGroupBox.SuspendLayout()
        CType(Me.DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ButtonGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableGroupBox
        '
        Me.TableGroupBox.Controls.Add(Me.TableComboBox)
        Me.TableGroupBox.Controls.Add(Me.ViewButton)
        Me.TableGroupBox.Controls.Add(Me.DBTableLabel)
        Me.TableGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableGroupBox.Location = New System.Drawing.Point(5, 5)
        Me.TableGroupBox.Name = "TableGroupBox"
        Me.TableGroupBox.Size = New System.Drawing.Size(782, 65)
        Me.TableGroupBox.TabIndex = 0
        Me.TableGroupBox.TabStop = False
        Me.TableGroupBox.Text = "Table"
        '
        'TableComboBox
        '
        Me.TableComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TableComboBox.FormattingEnabled = True
        Me.TableComboBox.Items.AddRange(New Object() {"", "Table1", "Table2", "Table3", "Table4", "Table5", "Table6", "Table7", "Table8", "Table9", "Table10", "CountryCode", "AccountCcyMapping"})
        Me.TableComboBox.Location = New System.Drawing.Point(95, 23)
        Me.TableComboBox.Name = "TableComboBox"
        Me.TableComboBox.Size = New System.Drawing.Size(259, 22)
        Me.TableComboBox.TabIndex = 1
        '
        'ViewButton
        '
        Me.ViewButton.Location = New System.Drawing.Point(372, 18)
        Me.ViewButton.Name = "ViewButton"
        Me.ViewButton.Size = New System.Drawing.Size(87, 29)
        Me.ViewButton.TabIndex = 2
        Me.ViewButton.Text = "Retrieve"
        Me.ViewButton.UseVisualStyleBackColor = True
        '
        'DBTableLabel
        '
        Me.DBTableLabel.AutoSize = True
        Me.DBTableLabel.Location = New System.Drawing.Point(15, 26)
        Me.DBTableLabel.Name = "DBTableLabel"
        Me.DBTableLabel.Size = New System.Drawing.Size(74, 14)
        Me.DBTableLabel.TabIndex = 0
        Me.DBTableLabel.Text = "Source Table:"
        Me.DBTableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GridViewGroupBox
        '
        Me.GridViewGroupBox.Controls.Add(Me.DataGridView)
        Me.GridViewGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridViewGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridViewGroupBox.Location = New System.Drawing.Point(5, 70)
        Me.GridViewGroupBox.Name = "GridViewGroupBox"
        Me.GridViewGroupBox.Size = New System.Drawing.Size(782, 365)
        Me.GridViewGroupBox.TabIndex = 1
        Me.GridViewGroupBox.TabStop = False
        '
        'DataGridView
        '
        Me.DataGridView.AllowUserToAddRows = False
        Me.DataGridView.AllowUserToDeleteRows = False
        Me.DataGridView.AllowUserToResizeRows = False
        Me.DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView.Location = New System.Drawing.Point(3, 16)
        Me.DataGridView.Name = "DataGridView"
        Me.DataGridView.RowHeadersVisible = False
        Me.DataGridView.RowTemplate.ReadOnly = True
        Me.DataGridView.Size = New System.Drawing.Size(776, 346)
        Me.DataGridView.TabIndex = 0
        '
        'ButtonGroupBox
        '
        Me.ButtonGroupBox.Controls.Add(Me.CloseButton)
        Me.ButtonGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonGroupBox.Location = New System.Drawing.Point(5, 435)
        Me.ButtonGroupBox.Name = "ButtonGroupBox"
        Me.ButtonGroupBox.Size = New System.Drawing.Size(782, 67)
        Me.ButtonGroupBox.TabIndex = 2
        Me.ButtonGroupBox.TabStop = False
        '
        'CloseButton
        '
        Me.CloseButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseButton.Location = New System.Drawing.Point(18, 14)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(87, 40)
        Me.CloseButton.TabIndex = 0
        Me.CloseButton.Text = "Close"
        Me.CloseButton.UseVisualStyleBackColor = True
        '
        'frmViewFromDB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 507)
        Me.Controls.Add(Me.GridViewGroupBox)
        Me.Controls.Add(Me.ButtonGroupBox)
        Me.Controls.Add(Me.TableGroupBox)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewFromDB"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "View From DB Tool  (MA0041)"
        Me.TableGroupBox.ResumeLayout(False)
        Me.TableGroupBox.PerformLayout()
        Me.GridViewGroupBox.ResumeLayout(False)
        CType(Me.DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ButtonGroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ViewButton As System.Windows.Forms.Button
    Friend WithEvents DBTableLabel As System.Windows.Forms.Label
    Friend WithEvents GridViewGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents TableComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ButtonGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents CloseButton As System.Windows.Forms.Button
End Class
