Imports BTMU.MAGIC.Common
Imports System.Text.RegularExpressions

Module GlobalModule
    Public GBLConfigurationMaster As ConfigurationMaster

    Public Function IsValidFileName(ByVal fileName As String) As Boolean
        Try
            Dim isFound As Integer
            isFound = fileName.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(fileName) AndAlso isFound <> -1 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function
End Module
