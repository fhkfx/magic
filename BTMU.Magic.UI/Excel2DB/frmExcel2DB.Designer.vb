<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExcel2DB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExcel2DB))
        Me.SourceGroupBox = New System.Windows.Forms.GroupBox
        Me.gbxSeparatorSettings = New System.Windows.Forms.GroupBox
        Me.lblOtherDelimiter = New System.Windows.Forms.Label
        Me.txtOtherDelimiter = New System.Windows.Forms.TextBox
        Me.cboEnclosureChar = New System.Windows.Forms.ComboBox
        Me.cboDelimiter = New System.Windows.Forms.ComboBox
        Me.lblDelimiter = New System.Windows.Forms.Label
        Me.lblEnclosureChar = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.WorksheetComboBox = New System.Windows.Forms.ComboBox
        Me.TransStartRowTextBox = New System.Windows.Forms.TextBox
        Me.EndColTextBox = New System.Windows.Forms.TextBox
        Me.StartColTextBox = New System.Windows.Forms.TextBox
        Me.SourceFileTextBox = New System.Windows.Forms.TextBox
        Me.TransStartRowLabel = New System.Windows.Forms.Label
        Me.EndColLabel = New System.Windows.Forms.Label
        Me.StartColLabel = New System.Windows.Forms.Label
        Me.WorksheetLabel = New System.Windows.Forms.Label
        Me.SourceFileLabel = New System.Windows.Forms.Label
        Me.DestinationGroupBox = New System.Windows.Forms.GroupBox
        Me.ToTableComboBox = New System.Windows.Forms.ComboBox
        Me.ToTableLabel = New System.Windows.Forms.Label
        Me.btnTransfer = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.ErrorProvider1 = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.SourceGroupBox.SuspendLayout()
        Me.gbxSeparatorSettings.SuspendLayout()
        Me.DestinationGroupBox.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SourceGroupBox
        '
        Me.SourceGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.SourceGroupBox.Controls.Add(Me.gbxSeparatorSettings)
        Me.SourceGroupBox.Controls.Add(Me.btnBrowse)
        Me.SourceGroupBox.Controls.Add(Me.WorksheetComboBox)
        Me.SourceGroupBox.Controls.Add(Me.TransStartRowTextBox)
        Me.SourceGroupBox.Controls.Add(Me.EndColTextBox)
        Me.SourceGroupBox.Controls.Add(Me.StartColTextBox)
        Me.SourceGroupBox.Controls.Add(Me.SourceFileTextBox)
        Me.SourceGroupBox.Controls.Add(Me.TransStartRowLabel)
        Me.SourceGroupBox.Controls.Add(Me.EndColLabel)
        Me.SourceGroupBox.Controls.Add(Me.StartColLabel)
        Me.SourceGroupBox.Controls.Add(Me.WorksheetLabel)
        Me.SourceGroupBox.Controls.Add(Me.SourceFileLabel)
        Me.SourceGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.SourceGroupBox.Location = New System.Drawing.Point(10, 8)
        Me.SourceGroupBox.Name = "SourceGroupBox"
        Me.SourceGroupBox.Size = New System.Drawing.Size(700, 177)
        Me.SourceGroupBox.TabIndex = 0
        Me.SourceGroupBox.TabStop = False
        Me.SourceGroupBox.Text = "Source File (Excel) "
        '
        'gbxSeparatorSettings
        '
        Me.gbxSeparatorSettings.Controls.Add(Me.lblOtherDelimiter)
        Me.gbxSeparatorSettings.Controls.Add(Me.txtOtherDelimiter)
        Me.gbxSeparatorSettings.Controls.Add(Me.cboEnclosureChar)
        Me.gbxSeparatorSettings.Controls.Add(Me.cboDelimiter)
        Me.gbxSeparatorSettings.Controls.Add(Me.lblDelimiter)
        Me.gbxSeparatorSettings.Controls.Add(Me.lblEnclosureChar)
        Me.gbxSeparatorSettings.Location = New System.Drawing.Point(425, 62)
        Me.gbxSeparatorSettings.Name = "gbxSeparatorSettings"
        Me.gbxSeparatorSettings.Size = New System.Drawing.Size(254, 104)
        Me.gbxSeparatorSettings.TabIndex = 13
        Me.gbxSeparatorSettings.TabStop = False
        Me.gbxSeparatorSettings.Text = "Separator Settings"
        Me.gbxSeparatorSettings.Visible = False
        '
        'lblOtherDelimiter
        '
        Me.lblOtherDelimiter.AutoSize = True
        Me.lblOtherDelimiter.Location = New System.Drawing.Point(4, 76)
        Me.lblOtherDelimiter.Name = "lblOtherDelimiter"
        Me.lblOtherDelimiter.Size = New System.Drawing.Size(123, 14)
        Me.lblOtherDelimiter.TabIndex = 4
        Me.lblOtherDelimiter.Text = "Others, please specify :"
        '
        'txtOtherDelimiter
        '
        Me.txtOtherDelimiter.Enabled = False
        Me.txtOtherDelimiter.Location = New System.Drawing.Point(129, 69)
        Me.txtOtherDelimiter.MaxLength = 1
        Me.txtOtherDelimiter.Name = "txtOtherDelimiter"
        Me.txtOtherDelimiter.Size = New System.Drawing.Size(19, 20)
        Me.txtOtherDelimiter.TabIndex = 2
        '
        'cboEnclosureChar
        '
        Me.cboEnclosureChar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEnclosureChar.FormattingEnabled = True
        Me.cboEnclosureChar.Items.AddRange(New Object() {"Double Quote("""")", "Single Quote(')", "None"})
        Me.cboEnclosureChar.Location = New System.Drawing.Point(129, 12)
        Me.cboEnclosureChar.Name = "cboEnclosureChar"
        Me.cboEnclosureChar.Size = New System.Drawing.Size(109, 22)
        Me.cboEnclosureChar.TabIndex = 0
        '
        'cboDelimiter
        '
        Me.cboDelimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDelimiter.FormattingEnabled = True
        Me.cboDelimiter.Items.AddRange(New Object() {";", ",", "@", "^", "<Tab>", "<Space>", "Other"})
        Me.cboDelimiter.Location = New System.Drawing.Point(129, 40)
        Me.cboDelimiter.Name = "cboDelimiter"
        Me.cboDelimiter.Size = New System.Drawing.Size(109, 22)
        Me.cboDelimiter.TabIndex = 1
        '
        'lblDelimiter
        '
        Me.lblDelimiter.AutoSize = True
        Me.lblDelimiter.Location = New System.Drawing.Point(4, 47)
        Me.lblDelimiter.Name = "lblDelimiter"
        Me.lblDelimiter.Size = New System.Drawing.Size(53, 14)
        Me.lblDelimiter.TabIndex = 2
        Me.lblDelimiter.Text = "Delimiter :"
        '
        'lblEnclosureChar
        '
        Me.lblEnclosureChar.AutoSize = True
        Me.lblEnclosureChar.Location = New System.Drawing.Point(4, 16)
        Me.lblEnclosureChar.Name = "lblEnclosureChar"
        Me.lblEnclosureChar.Size = New System.Drawing.Size(111, 14)
        Me.lblEnclosureChar.TabIndex = 0
        Me.lblEnclosureChar.Text = "Enclosure character :"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(592, 27)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(87, 29)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'WorksheetComboBox
        '
        Me.WorksheetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.WorksheetComboBox.FormattingEnabled = True
        Me.WorksheetComboBox.Location = New System.Drawing.Point(138, 66)
        Me.WorksheetComboBox.Name = "WorksheetComboBox"
        Me.WorksheetComboBox.Size = New System.Drawing.Size(259, 22)
        Me.WorksheetComboBox.TabIndex = 4
        '
        'TransStartRowTextBox
        '
        Me.TransStartRowTextBox.Location = New System.Drawing.Point(138, 136)
        Me.TransStartRowTextBox.MaxLength = 5
        Me.TransStartRowTextBox.Name = "TransStartRowTextBox"
        Me.TransStartRowTextBox.Size = New System.Drawing.Size(80, 20)
        Me.TransStartRowTextBox.TabIndex = 10
        '
        'EndColTextBox
        '
        Me.EndColTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.EndColTextBox.Location = New System.Drawing.Point(317, 101)
        Me.EndColTextBox.MaxLength = 2
        Me.EndColTextBox.Name = "EndColTextBox"
        Me.EndColTextBox.Size = New System.Drawing.Size(80, 20)
        Me.EndColTextBox.TabIndex = 8
        '
        'StartColTextBox
        '
        Me.StartColTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.StartColTextBox.Location = New System.Drawing.Point(138, 101)
        Me.StartColTextBox.MaxLength = 2
        Me.StartColTextBox.Name = "StartColTextBox"
        Me.StartColTextBox.Size = New System.Drawing.Size(80, 20)
        Me.StartColTextBox.TabIndex = 6
        '
        'SourceFileTextBox
        '
        Me.SourceFileTextBox.AllowDrop = True
        Me.SourceFileTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.SourceFileTextBox.Location = New System.Drawing.Point(138, 31)
        Me.SourceFileTextBox.MaxLength = 100
        Me.SourceFileTextBox.Name = "SourceFileTextBox"
        Me.SourceFileTextBox.ReadOnly = True
        Me.SourceFileTextBox.Size = New System.Drawing.Size(437, 20)
        Me.SourceFileTextBox.TabIndex = 1
        '
        'TransStartRowLabel
        '
        Me.TransStartRowLabel.AutoSize = True
        Me.TransStartRowLabel.Location = New System.Drawing.Point(15, 139)
        Me.TransStartRowLabel.Name = "TransStartRowLabel"
        Me.TransStartRowLabel.Size = New System.Drawing.Size(119, 14)
        Me.TransStartRowLabel.TabIndex = 9
        Me.TransStartRowLabel.Text = "Transaction Start Row:"
        Me.TransStartRowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EndColLabel
        '
        Me.EndColLabel.AutoSize = True
        Me.EndColLabel.Location = New System.Drawing.Point(246, 104)
        Me.EndColLabel.Name = "EndColLabel"
        Me.EndColLabel.Size = New System.Drawing.Size(66, 14)
        Me.EndColLabel.TabIndex = 7
        Me.EndColLabel.Text = "End Column:"
        '
        'StartColLabel
        '
        Me.StartColLabel.AutoSize = True
        Me.StartColLabel.Location = New System.Drawing.Point(15, 104)
        Me.StartColLabel.Name = "StartColLabel"
        Me.StartColLabel.Size = New System.Drawing.Size(71, 14)
        Me.StartColLabel.TabIndex = 5
        Me.StartColLabel.Text = "Start Column:"
        Me.StartColLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WorksheetLabel
        '
        Me.WorksheetLabel.AutoSize = True
        Me.WorksheetLabel.Location = New System.Drawing.Point(15, 69)
        Me.WorksheetLabel.Name = "WorksheetLabel"
        Me.WorksheetLabel.Size = New System.Drawing.Size(63, 14)
        Me.WorksheetLabel.TabIndex = 3
        Me.WorksheetLabel.Text = "WorkSheet:"
        Me.WorksheetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SourceFileLabel
        '
        Me.SourceFileLabel.AutoSize = True
        Me.SourceFileLabel.Location = New System.Drawing.Point(15, 34)
        Me.SourceFileLabel.Name = "SourceFileLabel"
        Me.SourceFileLabel.Size = New System.Drawing.Size(64, 14)
        Me.SourceFileLabel.TabIndex = 0
        Me.SourceFileLabel.Text = "Source File:"
        Me.SourceFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DestinationGroupBox
        '
        Me.DestinationGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.DestinationGroupBox.Controls.Add(Me.ToTableComboBox)
        Me.DestinationGroupBox.Controls.Add(Me.ToTableLabel)
        Me.DestinationGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DestinationGroupBox.Location = New System.Drawing.Point(10, 191)
        Me.DestinationGroupBox.Name = "DestinationGroupBox"
        Me.DestinationGroupBox.Size = New System.Drawing.Size(700, 65)
        Me.DestinationGroupBox.TabIndex = 1
        Me.DestinationGroupBox.TabStop = False
        Me.DestinationGroupBox.Text = "Destination Table "
        '
        'ToTableComboBox
        '
        Me.ToTableComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ToTableComboBox.FormattingEnabled = True
        Me.ToTableComboBox.Items.AddRange(New Object() {"", "Table1", "Table2", "Table3", "Table4", "Table5", "Table6", "Table7", "Table8", "Table9", "Table10", "CountryCode", "AccountCcyMapping"})
        Me.ToTableComboBox.Location = New System.Drawing.Point(138, 27)
        Me.ToTableComboBox.Name = "ToTableComboBox"
        Me.ToTableComboBox.Size = New System.Drawing.Size(259, 22)
        Me.ToTableComboBox.TabIndex = 12
        '
        'ToTableLabel
        '
        Me.ToTableLabel.AutoSize = True
        Me.ToTableLabel.Location = New System.Drawing.Point(15, 30)
        Me.ToTableLabel.Name = "ToTableLabel"
        Me.ToTableLabel.Size = New System.Drawing.Size(51, 14)
        Me.ToTableLabel.TabIndex = 11
        Me.ToTableLabel.Text = "To Table:"
        '
        'btnTransfer
        '
        Me.btnTransfer.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnTransfer.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransfer.Location = New System.Drawing.Point(266, 267)
        Me.btnTransfer.Name = "btnTransfer"
        Me.btnTransfer.Size = New System.Drawing.Size(87, 40)
        Me.btnTransfer.TabIndex = 11
        Me.btnTransfer.Text = "Transfer"
        Me.btnTransfer.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnClose.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(368, 267)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 40)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'frmExcel2DB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(721, 318)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnTransfer)
        Me.Controls.Add(Me.DestinationGroupBox)
        Me.Controls.Add(Me.SourceGroupBox)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExcel2DB"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Data from Excel Tool (MA0040)"
        Me.SourceGroupBox.ResumeLayout(False)
        Me.SourceGroupBox.PerformLayout()
        Me.gbxSeparatorSettings.ResumeLayout(False)
        Me.gbxSeparatorSettings.PerformLayout()
        Me.DestinationGroupBox.ResumeLayout(False)
        Me.DestinationGroupBox.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SourceGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents DestinationGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents TransStartRowLabel As System.Windows.Forms.Label
    Friend WithEvents EndColLabel As System.Windows.Forms.Label
    Friend WithEvents StartColLabel As System.Windows.Forms.Label
    Friend WithEvents WorksheetLabel As System.Windows.Forms.Label
    Friend WithEvents SourceFileLabel As System.Windows.Forms.Label
    Friend WithEvents StartColTextBox As System.Windows.Forms.TextBox
    Friend WithEvents WorksheetComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TransStartRowTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EndColTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents ToTableComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ToTableLabel As System.Windows.Forms.Label
    Friend WithEvents btnTransfer As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As New BTMU.Magic.Common.ErrorProviderFixed
    Friend WithEvents SourceFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents gbxSeparatorSettings As System.Windows.Forms.GroupBox
    Friend WithEvents lblOtherDelimiter As System.Windows.Forms.Label
    Friend WithEvents txtOtherDelimiter As System.Windows.Forms.TextBox
    Friend WithEvents cboEnclosureChar As System.Windows.Forms.ComboBox
    Friend WithEvents cboDelimiter As System.Windows.Forms.ComboBox
    Friend WithEvents lblDelimiter As System.Windows.Forms.Label
    Friend WithEvents lblEnclosureChar As System.Windows.Forms.Label
End Class
