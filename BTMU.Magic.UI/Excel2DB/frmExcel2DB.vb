Imports System.IO
Imports BTMU.Magic.Common
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Data.OleDb
Imports System.Data.SqlServerCe

''' <summary>
''' Export excel content to SQL CE database
''' </summary>
''' <remarks></remarks>
Public Class frmExcel2DB

#Region "Private Variables"

    Private Shared MsgReader As Global.System.Resources.ResourceManager
    Private _excelFileName As String
    Private _workSheetDataTable As DataTable
    Private _workSheetDataTableCpy As DataTable
    Private _worksheetColumnBindingSource As BindingSource
    Private _workSheetColumn As DataColumn
    Private _worksheetColumnDataTable As DataTable

#End Region
    Private Shared strDBPath As String = Path.Combine(My.Settings.MagicDB, "Excel2DB.sdf")
    Private Shared strConnectionString As String = String.Format("Data Source = ""{0}"";Password='btmu123$';default lock timeout='60000'", strDBPath)

#Region "Events"

    Private Sub frmExcel2DB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SuspendLayout()

        '***Set form default values.
        Me.SetDefaultValues()
        'Resource object to retrieve messages stored in application resosurces.
        MsgReader = New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
        '***Validate all form controls on loading.
        Me.ValidateAllControl(Me)

        Me.Left = (Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2
        Me.Top = 50

        ResumeLayout()

    End Sub

    Private Sub btnTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransfer.Click

        Dim boolProceed As Boolean = True

        Try
            If Not File.Exists(Me.SourceFileTextBox.Text.Trim) Then
                MsgReader.GetString("E00040010")
            End If

            '***Validate all controls before import.
            Me.ValidateAllControl(Me)
            '***Retrieve and display all error messages.
            Me.DisplayDetailError(boolProceed)
            'Continue with import if no error.
            If boolProceed Then
                Me.Cursor = Cursors.WaitCursor
                Me.ReadExcelToDataTable()
                Me.WriteDataTableToDB()
                Me.Cursor = Cursors.Default
            End If

        Catch magicEx As MagicException
            MessageBox.Show(magicEx.Message.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click

        Dim openFileDialog As New OpenFileDialog
        Dim fileInfo As FileInfo = Nothing

        Try
            openFileDialog.InitialDirectory = "C:\"
            openFileDialog.Filter = "Excel Files(*.xls)|*.xls|Excel 2007 Files(*.xlsx)|*.xlsx|CSV Files(*.csv)|*.csv"

            If openFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Me.SourceFileTextBox.Text = openFileDialog.FileName
                Me._excelFileName = openFileDialog.FileName

                If Path.GetExtension(Me._excelFileName).Equals(".csv") Then
                    WorksheetComboBox.Items.Add("")
                    WorksheetComboBox.SelectedIndex = 0
                    Me.Control_Validated(Me.WorksheetComboBox, Nothing)
                    cboEnclosureChar.SelectedIndex = 0
                    cboDelimiter.SelectedIndex = 0
                    gbxSeparatorSettings.Visible = True
                    WorksheetComboBox.Enabled = False
                Else
                    WorksheetComboBox.Enabled = True
                    Me.Cursor = Cursors.WaitCursor
                    Me.ReadWorksheetName(openFileDialog.FileName)
                    Me.Cursor = Cursors.Default

                    Me.WorksheetComboBox.Focus()
                End If
                Me.Control_Validated(Me.SourceFileTextBox, Nothing)
            End If

        Catch MagicEx As MagicException
            MessageBox.Show(MagicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub SourceFileTextBox_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragDrop
        Dim fileNames() As String = e.Data.GetData("FileDrop", False)
        Dim fileName As String = ""
        'Dim i As Integer

        Try

            Me.SourceFileTextBox.Focus()

            For Each file As String In fileNames
                If System.IO.File.Exists(file) Then
                    fileName = file
                    Exit For
                End If
            Next

            If Not File.Exists(fileName) Then
                MsgReader.GetString("E00040010")
            End If
            If Not (fileName.Trim.EndsWith(".xls") Or fileName.Trim.EndsWith(".xlsx")) Then Exit Sub

            Me.SourceFileTextBox.Text = String.Empty
            Me.SourceFileTextBox.Text = fileName
            Me._excelFileName = fileName
            'For i = 0 To fileNames.Length - 1
            '    SourceFileTextBox.Text += fileNames(i)
            'Next i

            Me.Cursor = Cursors.WaitCursor
            Me.ReadWorksheetName(SourceFileTextBox.Text)
            Me.Cursor = Cursors.Default

            Me.WorksheetComboBox.Focus()
            Me.Control_Validated(Me.SourceFileTextBox, Nothing)

        Catch magicEx As MagicException
            MessageBox.Show(magicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub SourceFileTextBox_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.All
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    Public Shared ReadOnly Property LookupDatabaseExists() As Boolean
        Get
            Return File.Exists(strDBPath)
        End Get
    End Property

    Public Shared ReadOnly Property DatabaseConnectionString() As String
        Get
            Return strConnectionString
        End Get
    End Property
    Public Shared ReadOnly Property DatabasePath() As String
        Get
            Return strDBPath
        End Get
    End Property

#Region "Import Functions"

    ''' <summary>
    ''' Read from selected excel file worksheet content to a temporary datatable
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReadExcelToDataTable()

        Const MAX_COLUMN_CNT = 30

        'Dim excelConnectionString As String = String.Empty
        'Dim excelConnection As OleDbConnection = Nothing
        'Dim worksheetCmd As OleDbCommand = Nothing
        ' Dim workSheetAdapter As OleDbDataAdapter

        Try
            Dim index As Integer = 0
            Dim columnNo As Integer = 1

            Dim strWorksheet As String = Me.WorksheetComboBox.SelectedItem.ToString
            Dim strStartCol As String = Me.StartColTextBox.Text.Trim
            Dim strEndCol As String = Me.EndColTextBox.Text.Trim
            Dim intStartRow As Integer = CInt(Me.TransStartRowTextBox.Text.Trim)
            Dim intOrginalCol As Integer
            Dim intStartCol As Integer = GetColumnIndex(strStartCol)
            Dim intEndCol As Integer = GetColumnIndex(strEndCol)

            Me._workSheetDataTable = New DataTable
            Me._workSheetDataTableCpy = New DataTable
            'Me._worksheetColumnBindingSource = New BindingSource

            '#1.   Get the Worksheet Data Table
            If Path.GetExtension(Me._excelFileName).Equals(".csv") Then

                Dim sr As New StreamReader(Me._excelFileName)
                Dim fullFileStr As String = sr.ReadToEnd()
                sr.Close()
                sr.Dispose()
                Dim lines As String() = fullFileStr.Split(ControlChars.Lf)

                Dim sDelimiter As String
                Select Case cboDelimiter.Text
                    Case "Other"
                        sDelimiter = txtOtherDelimiter.Text
                    Case "<Tab>"
                        sDelimiter = vbTab
                    Case "<Space>"
                        sDelimiter = " "
                    Case Else
                        sDelimiter = cboDelimiter.Text
                End Select

                If cboEnclosureChar.Text <> "None" Then
                    Dim sEnclosureCharacter As String
                    sEnclosureCharacter = IIf(cboEnclosureChar.Text = "Single Quote(')", "'", """")
                    Dim hasCreateCol = False
                    For Each line As String In lines
                        Dim columns As String() = HelperModule.Split(line, sDelimiter, sEnclosureCharacter, True)
                        If Not columns Is Nothing Then
                            If Not hasCreateCol Then
                                For temp As Integer = 1 To columns.Length
                                    Me._workSheetDataTableCpy.Columns.Add("Column" + temp.ToString(), GetType(String))
                                Next
                                hasCreateCol = True
                            End If

                            Dim row As DataRow = Me._workSheetDataTableCpy.NewRow()
                            For temp As Integer = 0 To columns.Length - 1
                                row(temp) = columns(temp)
                            Next
                            Me._workSheetDataTableCpy.Rows.Add(row)
                        End If
                    Next
                Else
                    Dim hasCreateCol = False
                    For Each line As String In lines
                        Dim columns As String() = HelperModule.Split(line, sDelimiter, Nothing, True)
                        If Not columns Is Nothing Then
                            If Not hasCreateCol Then
                                For temp As Integer = 1 To columns.Length
                                    Me._workSheetDataTableCpy.Columns.Add("Column" + temp.ToString(), GetType(String))
                                Next
                                hasCreateCol = True
                            End If

                            Dim row As DataRow = Me._workSheetDataTableCpy.NewRow()
                            For temp As Integer = 0 To columns.Length - 1
                                row(temp) = columns(temp)
                            Next
                            Me._workSheetDataTableCpy.Rows.Add(row)
                        End If
                    Next
                End If
            Else
                Me._workSheetDataTableCpy = ExcelReaderInterop.ExcelOpenSpreadsheets(Me._excelFileName, strWorksheet)
            End If

            ' Fixed by FHK 2013-3-7 in case _workSheetDataTableCpy is nothing
            'If Me._workSheetDataTableCpy Is Nothing Or Me._workSheetDataTableCpy.Columns.Count = 0 Then
            If Me._workSheetDataTableCpy Is Nothing OrElse Me._workSheetDataTableCpy.Columns.Count = 0 Then
                Throw New MagicException(MsgReader.GetString("E00040120")) '--
            End If

            '#2.    Check starting column
            If intStartCol > Me._workSheetDataTableCpy.Columns.Count Then
                Throw New MagicException(MsgReader.GetString("E00040121"))
            End If

            '#2.1    Check max row
            If intStartRow > Me._workSheetDataTableCpy.Rows.Count Then
                Throw New MagicException(MsgReader.GetString("E00040122"))
            End If

            '#3.   Add the extra columns to the worksheetDataTable
            If intEndCol > Me._workSheetDataTableCpy.Columns.Count Then
                intOrginalCol = Me._workSheetDataTableCpy.Columns.Count
                For intI As Integer = intOrginalCol To intEndCol - 1
                    Me._workSheetDataTableCpy.Columns.Add("Field" & (intI + 1).ToString, GetType(String))
                Next
                ' Assign an empty string to the newly created columns
                For intI As Integer = 0 To Me._workSheetDataTableCpy.Rows.Count - 1
                    For intJ As Integer = intOrginalCol To Me._workSheetDataTableCpy.Columns.Count - 1
                        Me._workSheetDataTableCpy.Rows(intI)(intJ) = ""
                    Next
                Next
            End If

            '#4.   Copy datatable
            Me._workSheetDataTable = Me._workSheetDataTableCpy.Copy

            '#5.   Get the columns from Start Column to End Column
            'Remove the columns at the end till End Column from the DataTable
            For index = Me._workSheetDataTableCpy.Columns.Count - 1 To intEndCol Step -1
                Me._workSheetDataTable.Columns.Remove(Me._workSheetDataTable.Columns(index))
            Next

            '#6. Remove the columns at the start till Start Column from the Datatable
            For index = 1 To intStartCol - 1
                Me._workSheetDataTable.Columns.Remove(Me._workSheetDataTable.Columns(0))
            Next

            '#7. Remove 
            If intStartRow > 1 Then
                For index = 0 To intStartRow - 2
                    Me._workSheetDataTable.Rows.RemoveAt(0)
                Next
            End If

            '********************************************************************************'
            '#10. Add remaining empty columns to match DB table columns (fixed to 30 columns)
            For index = Me._workSheetDataTable.Columns.Count To MAX_COLUMN_CNT - 1
                Me._workSheetDataTable.Columns.Add("")
            Next

            '#9.   Discard the original DataTable
            ' No need of this datatable, as the Datatable has been copied.
            Me._workSheetDataTableCpy = Nothing

            'excelConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", Me._excelFileName)
            'excelConnection = New OleDbConnection(excelConnectionString)
            'excelConnection.Open()

            'worksheetCmd = New OleDbCommand(String.Format("Select * From [{0}$]", strWorksheet), excelConnection)
            'workSheetAdapter = New OleDbDataAdapter(worksheetCmd)
            'workSheetAdapter.Fill(Me._workSheetDataTable)

            ''***********************************************************************************'
            '' Get the columns from Start Column to End Column

            '' Remove the columns at the end till End Column from the DataTable
            ''For index = workSheetDataTable.Columns.Count - 1 To GetColumnIndex((strEndCol)) Step -1
            'For index = Me._workSheetDataTable.Columns.Count - 1 To strEndCol Step -1
            '    Me._workSheetDataTable.Columns.Remove(Me._workSheetDataTable.Columns(index))
            'Next

            '' Remove the columns at the start till Start Column from the Datatable
            ''For index = 1 To GetColumnIndex(strStartCol) - 1
            'For index = 1 To strStartCol - 1
            '    Me._workSheetDataTable.Columns.Remove(Me._workSheetDataTable.Columns(0))
            'Next

            ''********************************************************************************'
            '' Add remaining empty columns to match DB table columns (fixed to 30 columns)
            'For index = Me._workSheetDataTable.Columns.Count To MAX_COLUMN_CNT - 1
            '    Me._workSheetDataTable.Columns.Add("")
            'Next

            ''************************************************************************************'
            '' Create a Datatable of SourceFieldNames
            'Me._worksheetColumnDataTable = New DataTable
            'Me._worksheetColumnDataTable.Columns.Add("SourceFieldName")
            'Me._worksheetColumnBindingSource.DataSource = Me._worksheetColumnDataTable
            'Me._worksheetColumnDataTable.Rows.Clear()
            'Me._worksheetColumnDataTable.Rows.Add("")

            ''***********************************************************************************'
            '' Generate the field names as Field001, Field002 etc
            'For Each Me._workSheetColumn In Me._workSheetDataTable.Columns
            '    Me._worksheetColumnDataTable.Rows.Add("Field" & String.Format("{000}", columnNo))
            '    Me._workSheetColumn.ColumnName = "Field" & String.Format("{000}", CStr(columnNo))
            '    columnNo += 1
            'Next

            ''********************************************************************************'
            '' Get the row from Transaction Start onwards
            'For index = 0 To strStartRow - 2
            '    Me._workSheetDataTable.Rows.RemoveAt(0)
            'Next

            ''********************************Data From Excel is Ready********************************'

        Catch MagicEx As MagicException
            Throw New MagicException(MagicEx.Message.ToString)
        Catch ex As Exception
            Throw New MagicException(ex.Message.ToString)
        Finally
            'If excelConnection.State = ConnectionState.Open Then
            '    excelConnection.Close()
            'End If
            'excelConnection = Nothing

            'If worksheetCmd IsNot Nothing Then
            '    worksheetCmd.Dispose()
            'End If
        End Try

    End Sub


    ''' <summary>
    ''' Populates first row of given table 
    ''' </summary>
    ''' <param name="strTableName">Name of the Table</param>
    ''' <returns>The very first row available</returns>
    ''' <remarks></remarks>
    Public Shared Function GetSampleRow(ByVal strTableName As String) As String()

        Dim dtable As New DataTable

        Dim result As New List(Of String)

        If Not File.Exists(strDBPath) Then

            SetupDB(strConnectionString)

        End If

        Dim dbCEConnection As New SqlCeConnection(strConnectionString)
        dbCEConnection.Open()

        Try

            Dim dbCECommand As SqlCeCommand = dbCEConnection.CreateCommand()
            Dim strCommand As String = String.Format(" SELECT * FROM {0} ", strTableName)

            dbCECommand.CommandText = strCommand

            dtable.Load(dbCECommand.ExecuteReader(CommandBehavior.SingleRow))

        Catch ex As Exception
            Throw ex
        Finally
            dbCEConnection.Close()
        End Try


        For Each _tblColumn As DataColumn In dtable.Columns
            If dtable.Rows.Count >= 1 Then
                result.Add(String.Format("{0}-{1}", _tblColumn.ColumnName, dtable.Rows(0)(_tblColumn.ColumnName).ToString()))
            Else
                result.Add(String.Format("{0}- ", _tblColumn.ColumnName))
            End If
        Next

        Return result.ToArray()

    End Function

    ''' <summary>
    ''' Create SQLCE database if it is not found
    ''' </summary>
    ''' <param name="strDatabaseConString">Database connection string</param>
    ''' <remarks></remarks>
    Private Shared Sub SetupDB(ByVal strDatabaseConString)

        If Not Directory.Exists(My.Settings.MagicDB) Then
            Directory.CreateDirectory(My.Settings.MagicDB)
        End If

        Dim dbEng As New SqlCeEngine(strDatabaseConString)
        dbEng.CreateDatabase()
        dbEng.Dispose()

        Dim dbCEConnection As New SqlCeConnection(strDatabaseConString)
        dbCEConnection.Open()

        Try

            Dim dbCECommand As SqlCeCommand = dbCEConnection.CreateCommand()
            Dim strCommand As String = ""

            ' 2017-2-28: add Account-ccy mapping table as Table 12
            ' for Hong Kong GCMS Plus format use since unlike TW/SG, account currency cannot be deduced from account number
            'For tblCounter As Int32 = 1 To 11
            For tblCounter As Int32 = 1 To 12

                If (tblCounter = 11) Then
                    strCommand = "CREATE TABLE CountryCode (" ' Table 11
                ElseIf (tblCounter = 12) Then
                    strCommand = "CREATE TABLE AccountCcyMapping (" ' Table 12
                Else
                    strCommand = String.Format("CREATE TABLE Table{0} (", tblCounter)
                End If


                For tblColumnCounter As Int32 = 1 To 30
                    'Extend field's length to 255 (originally- 100)
                    'Due to data loss when importing bank branches and names
                    'strCommand &= String.Format("C{0} NVARCHAR(255),", tblColumnCounter.ToString.PadLeft(2, "0"))

                    Dim columnName As String = ""
                    If (tblColumnCounter > 26) Then
                        Dim c As String = Convert.ToChar(tblColumnCounter - 26 + 64).ToString()
                        columnName = "A" + c
                    Else
                        Dim c As String = Convert.ToChar(tblColumnCounter + 64).ToString()
                        columnName = c
                    End If
                    strCommand &= String.Format("{0} NVARCHAR(255),", columnName)
                Next

                strCommand = strCommand.Substring(0, strCommand.Length - 1) & " ) "
                dbCECommand.CommandText = strCommand
                dbCECommand.ExecuteNonQuery()
            Next

        Catch ex As Exception
            Throw ex
        Finally
            dbCEConnection.Close()
        End Try

    End Sub

    ''' <summary>
    ''' Write datatable content to the selected database table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub WriteDataTableToDB()

        Dim dbCEConnectionString As String = String.Empty
        Dim dbCEConnection As SqlCeConnection = Nothing
        Dim dbCECommand As SqlCeCommand = Nothing
        Dim dbCETrans As SqlCeTransaction = Nothing
        Dim colIndex As Integer = 0
        Dim rowIndex As Integer = 0
        Dim colName As String

        Try
            If Me._workSheetDataTable.Rows.Count = 0 Then
                Throw New MagicException(MsgReader.GetString("E00040120"))
            End If

            Dim strDestTable As String = Me.ToTableComboBox.SelectedItem.ToString
            dbCEConnectionString = strConnectionString
            'Database is missing! Just Create it
            If Not File.Exists(strDBPath) Then

                SetupDB(dbCEConnectionString)

            End If

            dbCEConnection = New SqlCeConnection(dbCEConnectionString)

            dbCEConnection.Open()
            dbCETrans = dbCEConnection.BeginTransaction()

            dbCECommand = dbCEConnection.CreateCommand()
            'originally start trans here
            'dbCECommand.Transaction = dbCETrans

            ' Alert the user and confirm before overwriting the table
            dbCECommand.CommandText = String.Format(" SELECT COUNT(*) FROM {0} ", strDestTable)
            Dim recordsCount As Int32 = Convert.ToInt32(dbCECommand.ExecuteScalar())

            Dim skipTableInsert As Boolean = True
            ''Check Table is empty or not
            'If IsEmptyTable() Then
            '    If MessageBox.Show(String.Format("Table '{0}' has data and it will be overwritten! Are you sure you want to Import Data?", strDestTable), "Confirm", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
            '        skipTableInsert = False
            '    Else
            '        skipTableInsert = True
            '    End If
            'End If
            If recordsCount > 0 Then

                If MessageBox.Show(String.Format("Table '{0}' has data and it will be overwritten! Are you sure you want to Import Data?", strDestTable), "Magic Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    skipTableInsert = False
                Else
                    skipTableInsert = True
                End If

            End If
            

            If Not skipTableInsert Then
                Me.SetDefaultValues()
                'Not necessary bcoz moved tras begin to below.
                'dbCETrans.Rollback()
            Else

                'delete data
                DeleteTableData()

                'start transaction
                dbCECommand.Transaction = dbCETrans

                'dbCECommand.CommandText = "DELETE from " & strDestTable
                'dbCECommand.ExecuteNonQuery()

                dbCECommand = dbCEConnection.CreateCommand()
                dbCECommand.CommandText = "INSERT into " & strDestTable & _
                   "(A,B,C,D,E,F,G,H,I,J" & _
                   ",K,L,M,N,O,P,Q,R,S,T" & _
                   ",U,V,W,X,Y,Z,AA,AB,AC,AD)" & _
                   "VALUES" & _
                   "(@A,@B,@C,@D,@E,@F,@G,@H,@I,@J" & _
                   ",@K,@L,@M,@N,@O,@P,@Q,@R,@S,@T" & _
                   ",@U,@V,@W,@X,@Y,@Z,@AA,@AB,@AC,@AD);"

                For rowIndex = 0 To Me._workSheetDataTable.Rows.Count - 1
                    If rowIndex = 0 Then
                        For colIndex = 0 To Me._workSheetDataTable.Columns.Count - 1
                            colName = "@" & getLetter(colIndex + 1) '"@C" & String.Format("{0:00}", (colIndex + 1))
                            dbCECommand.Parameters.Add(colName, GetType(String))
                        Next
                        dbCECommand.Prepare()
                    End If
                    For colIndex = 0 To Me._workSheetDataTable.Columns.Count - 1
                        If (_workSheetDataTable.Rows(rowIndex)(colIndex).ToString.Length > 255) Then
                            If (vbYes = MessageBox.Show("Some of the data has exceeded the maximum length (255) of table fields. Would you like to truncate the data up to 255?", "Magic Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)) Then
                                _workSheetDataTable.Rows(rowIndex)(colIndex) = _workSheetDataTable.Rows(rowIndex)(colIndex).ToString.Substring(0, 255)
                            Else
                                MessageBox.Show("Abort the import process!", "Magic Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                dbCECommand.CommandText = "Delete from " & strDestTable & ""
                                dbCECommand.ExecuteNonQuery()
                                Exit Sub

                            End If

                        End If
                        dbCECommand.Parameters(colIndex).Value = _workSheetDataTable.Rows(rowIndex)(colIndex).ToString
                    Next
                    dbCECommand.ExecuteNonQuery()
                Next

                dbCETrans.Commit()

                MessageBox.Show("Import has been completed successfully!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Me.ClearAndDispose()

            End If

        Catch cex As SqlCeException
            dbCETrans.Rollback()
            Throw New MagicException(cex.Message.ToString)
        Catch ex As Exception
            dbCETrans.Rollback()
            Throw New MagicException(ex.Message.ToString)
        Finally
            If dbCEConnection.State = ConnectionState.Open Then
                dbCEConnection.Close()
            End If
            dbCEConnection = Nothing

            If dbCECommand IsNot Nothing Then
                dbCECommand.Dispose()
            End If
        End Try

    End Sub

#End Region

#Region "Validations"

    Private Sub ValidateAllControl(ByVal ctl As Control)
        Dim ctlDetail As Control
        For Each ctlDetail In ctl.Controls
            If ctlDetail.Controls.Count > 0 Then
                Me.ValidateAllControl(ctlDetail)
            Else
                Me.Control_Validated(ctlDetail, Nothing)
            End If
        Next
    End Sub

    Private Sub ColTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles StartColTextBox.KeyPress, EndColTextBox.KeyPress
        If ((Asc(e.KeyChar.ToString) >= Asc("A") And Asc(e.KeyChar.ToString) <= Asc("Z")) _
                   Or (Asc(e.KeyChar.ToString) >= Asc("a") And Asc(e.KeyChar.ToString) <= Asc("z"))) _
                   Or (e.KeyChar = vbBack) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TransStartRowTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TransStartRowTextBox.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case Keys.D0 To Keys.D9
            Case Keys.Back
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub Control_Validated(ByVal sender As Object, _
                                    ByVal e As System.EventArgs) Handles _
                                                        SourceFileTextBox.Validated, _
                                                        WorksheetComboBox.Validated, _
                                                        StartColTextBox.Validated, _
                                                        EndColTextBox.Validated, _
                                                        TransStartRowTextBox.Validated, _
                                                        ToTableComboBox.Validated
        Try
            Dim ctl As Control = CType(sender, Control)
            Dim boolChecked As Boolean = True

            ErrorProvider1.SetError(ctl, String.Empty)
            If Not Path.GetExtension(Me._excelFileName).Equals(".csv") Then
                If TypeOf (sender) Is ComboBox Then
                    Dim ctlCombo As ComboBox = CType(sender, ComboBox)
                    Me.Validate_IsListEmpty(ctlCombo, boolChecked)
                    Me.Validate_IsListSelected(ctlCombo, boolChecked)
                ElseIf TypeOf (sender) Is TextBox Then
                    Dim ctlText As TextBox = CType(sender, TextBox)
                    If Not ctlText.Name.Equals("txtOtherDelimiter") Then
                        Me.Validate_IsTextEmpty(ctlText, boolChecked)
                        Me.Validate_IsTextValidNumber(ctlText, boolChecked)
                        Me.Validate_IsTextColValidRangeNumber_1(ctlText, boolChecked)
                        Me.Validate_IsTextColValidRangeNumber_2(ctlText, boolChecked)
                        Me.Validate_TextStartTransactionRow(ctlText, boolChecked)
                    End If
                End If
            End If
        Catch magicEx As MagicException
            MessageBox.Show(magicEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub tbxStartCol_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles StartColTextBox.Validated
        Me.Control_Validated(EndColTextBox, Nothing)
    End Sub

    Private Sub tbxEndCol_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles EndColTextBox.Validated
        Me.Control_Validated(StartColTextBox, Nothing)
    End Sub

    ''' <summary>
    ''' Validate mandatory comboboxes (check if list is empty)
    ''' </summary>
    ''' <param name="ctlCombo">Combobox to be validated</param>
    ''' <param name="boolChecked">Flag: True = Not empty or False = List is empty</param>
    ''' <remarks></remarks>
    Public Sub Validate_IsListEmpty(ByVal ctlCombo As ComboBox, ByRef boolChecked As Boolean)
        If boolChecked AndAlso ctlCombo.Items.Count = 0 Then
            If ctlCombo.Name = Me.WorksheetComboBox.Name Then
                ErrorProvider1.SetError(ctlCombo, MsgReader.GetString("E00040030"))
            ElseIf ctlCombo.Name = Me.ToTableComboBox.Name Then
                ErrorProvider1.SetError(ctlCombo, MsgReader.GetString("E00040100")) '--
            End If
            boolChecked = False
        End If
    End Sub

    ''' <summary>
    ''' Validate mandatory comboboxes (check if a value is selected)
    ''' </summary>
    ''' <param name="ctlCombo">Combobox to be validated</param>
    ''' <param name="boolChecked">Flag: True = selected or False = not selected</param>
    ''' <remarks></remarks>
    Public Sub Validate_IsListSelected(ByVal ctlCombo As ComboBox, ByRef boolChecked As Boolean)
        If boolChecked AndAlso ctlCombo.SelectedIndex <= 0 Then
            If ctlCombo.Name = Me.WorksheetComboBox.Name Then
                ErrorProvider1.SetError(ctlCombo, MsgReader.GetString("E00040030"))
            ElseIf ctlCombo.Name = Me.ToTableComboBox.Name Then
                ErrorProvider1.SetError(ctlCombo, MsgReader.GetString("E00040100")) '--
            End If
            boolChecked = False
        End If
    End Sub

    ''' <summary>
    ''' Validate mandatory textbox (check if it is empty)
    ''' </summary>
    ''' <param name="ctlText">Textbox to be validated</param>
    ''' <param name="boolChecked">Flag: True = not empty, False = empty</param>
    ''' <remarks>This sub calls SetTextEmptyMessage to set error message </remarks>
    Public Sub Validate_IsTextEmpty(ByVal ctlText As TextBox, ByRef boolChecked As Boolean)
        If boolChecked AndAlso ctlText.Text.Trim = String.Empty Then
            SetTextEmptyMessage(ctlText)
            boolChecked = False
        End If
    End Sub

    ''' <summary>
    ''' Validate input value for a valid number
    ''' </summary>
    ''' <param name="ctlText">Textbox to be validated</param>
    ''' <param name="boolChecked">Flag: True = check, False = do not check</param>
    ''' <remarks></remarks>
    Public Sub Validate_IsTextValidNumber(ByVal ctlText As TextBox, ByRef boolChecked As Boolean)

        Dim reg As Regex = New Regex("^-?[0-9]+$")
        Dim intNumber As Integer

        Try
            If boolChecked AndAlso ctlText.Name = Me.TransStartRowTextBox.Name Then

                If reg.IsMatch(ctlText.Text) Then
                    Integer.TryParse(ctlText.Text, intNumber)
                    If intNumber < 1 Then
                        SetTextEmptyMessage(ctlText)
                    End If
                Else
                    ErrorProvider1.SetError(ctlText, String.Format(MsgReader.GetString("E00040090"), (GetLabelName(ctlText))))
                End If
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Validate input value for a valid range of column no 
    ''' </summary>
    ''' <param name="ctlText">Textbox to be validated</param>
    ''' <param name="boolChecked">Flag: True = check, False = do not check</param>
    ''' <remarks></remarks>
    Public Sub Validate_IsTextColValidRangeNumber_1(ByVal ctlText As TextBox, ByRef boolChecked As Boolean)

        Dim intStartCol As Integer
        Dim intEndCol As Integer

        Try

            If Me.StartColTextBox.Text.Trim <> "" AndAlso Me.EndColTextBox.Text.Trim <> "" Then
                intStartCol = GetColumnIndex(Me.StartColTextBox.Text.Trim)
                intEndCol = GetColumnIndex(Me.EndColTextBox.Text.Trim)
                If boolChecked AndAlso ctlText.Name <> Me.SourceFileTextBox.Name AndAlso ctlText.Name <> Me.TransStartRowTextBox.Name Then
                    If ctlText.Name = Me.StartColTextBox.Name Then
                        If intStartCol > intEndCol Then
                            ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040050"))
                        End If
                    ElseIf ctlText.Name = Me.EndColTextBox.Name Then
                        If intStartCol > intEndCol Then
                            ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040060"))
                        End If
                    End If
                End If
            End If
           
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Validate input value for a valid range of start column and end column (max 30 cols) 
    ''' </summary>
    ''' <param name="ctlText">Textbox to be validated</param>
    ''' <param name="boolChecked">Flag: True = check, Flase = do not check</param>
    ''' <remarks></remarks>
    Public Sub Validate_IsTextColValidRangeNumber_2(ByVal ctlText As TextBox, ByRef boolChecked As Boolean)

        Dim strStartCol As String
        Dim strEndCol As String

        Try
            If boolChecked AndAlso ctlText.Name <> Me.SourceFileTextBox.Name AndAlso ctlText.Name <> Me.TransStartRowTextBox.Name Then

                strStartCol = StartColTextBox.Text.Trim
                strEndCol = EndColTextBox.Text.Trim

                If strStartCol <> "" AndAlso strEndCol <> "" Then
                    If ((GetColumnIndex(strEndCol) - GetColumnIndex(strStartCol)) + 1) > 30 Then
                        ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040070"))
                    End If
                End If
               
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Validate Transaction Start Row for a valid range number
    ''' </summary>
    ''' <param name="ctlText">Textbox to be validated</param>
    ''' <param name="boolChecked">Flag: True = check, False = do not check</param>
    ''' <remarks></remarks>
    Public Sub Validate_TextStartTransactionRow(ByVal ctlText As TextBox, ByRef boolChecked As Boolean)

        Dim intStartTransactionRow As Integer

        Try
            If boolChecked AndAlso ctlText.Name = Me.TransStartRowTextBox.Name Then
                Integer.TryParse(ctlText.Text, intStartTransactionRow)
                If intStartTransactionRow > 65536 Then
                    ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040080")) '--
                End If
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Set error message to the validated control
    ''' </summary>
    ''' <param name="ctlText">Textbox to be assign for error message</param>
    ''' <remarks>This sub is called by Validate_IsTextEmpty</remarks>
    Public Sub SetTextEmptyMessage(ByVal ctlText As TextBox)
        If ctlText.Name = Me.SourceFileTextBox.Name Then
            ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040020"))
        ElseIf ctlText.Name = Me.StartColTextBox.Name Or ctlText.Name = Me.EndColTextBox.Name Then
            ErrorProvider1.SetError(ctlText, String.Format(MsgReader.GetString("E00040040"), (GetLabelName(ctlText))))
        Else
            ErrorProvider1.SetError(ctlText, MsgReader.GetString("E00040080")) '--
        End If
    End Sub

#End Region

#Region " Excel Functions "

    ''' <summary>
    ''' Retrieve worksheet name from the selected excel file and populate to combobox
    ''' </summary>
    ''' <param name="strFileName">Excel file name</param>
    ''' <remarks></remarks>
    Public Sub ReadWorksheetName(ByVal strFileName As String)

        Dim excelCon As String = String.Empty
        Dim excelstrCon As OleDb.OleDbConnection = Nothing
        Dim restrictions As String() = {Nothing, Nothing, Nothing, "TABLE"}

        Try
            Dim schemaTable As DataTable
            Try
                excelCon = ExcelReaderInterop.GetConnectionString(strFileName, "2003")
                excelstrCon = New OleDbConnection(excelCon)
                excelstrCon.Open()
            Catch ex As Exception
                Try
                    excelCon = ExcelReaderInterop.GetConnectionString(strFileName, "2007")
                    excelstrCon = New OleDbConnection(excelCon)
                    excelstrCon.Open()
                Catch ex1 As Exception
                    'Throw New MagicException("Your system doesnot support Excel 2007!")
                    Throw New MagicException(MsgReader.GetString("E00000006"))
                End Try
            End Try

            'excelCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
            'excelCon &= strFileName
            'excelCon &= ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"""

            'excelstrCon = New OleDb.OleDbConnection(excelCon)
            'excelstrCon.Open()

            schemaTable = excelstrCon.GetSchema("Tables", restrictions)
            WorksheetComboBox.Items.Clear()
            WorksheetComboBox.Items.Add("")

            For Each row As DataRow In schemaTable.Rows
                If (Not row("TABLE_NAME").ToString().Replace("'", "").EndsWith("$")) Then
                Else
                    If row("TABLE_NAME").ToString.StartsWith("'") Then
                        row("TABLE_NAME") = row("TABLE_NAME").ToString.Remove(0, 1)
                    End If

                    If row("TABLE_NAME").ToString.EndsWith("'") Then
                        row("TABLE_NAME") = row("TABLE_NAME").ToString.Remove(row("TABLE_NAME").ToString.Length - 1, 1)
                    End If

                    row("TABLE_NAME") = row("TABLE_NAME").ToString().Replace("''", "'")
                    row("TABLE_NAME") = row("TABLE_NAME").ToString().Remove(row("TABLE_NAME").ToString().Length - 1, 1)

                    WorksheetComboBox.Items.Add(row("TABLE_NAME"))
                End If
            Next

            WorksheetComboBox.SelectedIndex = 0

        Catch fnfEx As FileNotFoundException
            Throw New MagicException(MsgReader.GetString("E00040010"))
        Catch flEx As FileLoadException
            MessageBox.Show(flEx.Message)
        Catch oleEx As OleDbException
            WorksheetComboBox.Items.Clear()
            Throw New MagicException(oleEx.Message)
        Catch ex As Exception
            WorksheetComboBox.Items.Clear()
            Throw New MagicException(ex.Message)
        Finally

            If excelstrCon IsNot Nothing Then
                excelstrCon.Close()
            End If
            excelstrCon = Nothing
        End Try

    End Sub


    ''' <summary>
    ''' <para>This function is to get the Column Index for the Excel Column letter</para>
    ''' </summary>
    ''' <param name="columnLetter">Excel Column Letter(Eg:-A,B,C,AA etc)</param>
    ''' <returns>ColumnIndex as integer</returns>
    ''' <remarks></remarks>
    Public Function GetColumnIndex(ByVal columnLetter As String) As Integer

        Dim leftLetterAscii As String
        Dim leftLetterVal As Integer
        Dim rightLetterAscii As String
        Dim rightLetterVal As Integer
        Try
            Select Case Len(columnLetter)
                Case 1
                    Return Asc(UCase(columnLetter)) - 64
                Case 2 'AA,AB,AC, etc
                    leftLetterAscii = Asc(columnLetter.Trim.ToUpper.Substring(0, 1))
                    leftLetterVal = leftLetterAscii - 64
                    rightLetterAscii = Asc(columnLetter.Trim.ToUpper.Substring(columnLetter.Trim.Length - 1, 1))
                    rightLetterVal = rightLetterAscii - 64
                    Return (leftLetterVal * 26) + rightLetterVal
            End Select
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    Private Function CheckColumnExists(ByVal excelColumnDatatable As DataTable, ByVal columnName As String) As Boolean
        Dim returnValue As Boolean = False
        For Each columnDataRow As DataRow In excelColumnDatatable.Rows
            If columnDataRow.Item(0) = columnName Then
                returnValue = True
            End If
        Next
        Return returnValue
    End Function

#End Region

    ''' <summary>
    ''' Consolidate and determine if error message should be prompt or not
    ''' </summary>
    ''' <param name="boolProceed">Flag to display or not to display error message</param>
    ''' <remarks>This function call GetDetailError to consolidate error messages</remarks>
    Public Sub DisplayDetailError(ByRef boolProceed As Boolean)

        Dim fistError As New StringBuilder
        Dim detailError As New StringBuilder

        Me.GetDetailError(SourceGroupBox, fistError, detailError)
        Me.GetDetailError(DestinationGroupBox, fistError, detailError)

        If fistError.Length > 0 Then
            boolProceed = False
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(fistError.ToString, detailError.ToString)
        End If

    End Sub

    ''' <summary>
    ''' Consolidate error messages from error providers for all controls 
    ''' </summary>
    ''' <param name="groupCtl">Groupbox which all controls are in</param>
    ''' <param name="fistError">The first error message to appear</param>
    ''' <param name="detailError">The rest of error messages</param>
    ''' <remarks>This function is callled by DisplayDetailError</remarks>
    Public Sub GetDetailError(ByVal groupCtl As GroupBox, _
                                ByRef fistError As StringBuilder, _
                                ByRef detailError As StringBuilder)

        Dim strMessage As String

        For Each ctl As Control In groupCtl.Controls
            strMessage = ErrorProvider1.GetError(ctl).Trim
            If strMessage <> String.Empty Then
                If fistError.Length > 0 Then
                    If (strMessage.Trim.Length > 0) Then detailError.AppendLine(String.Format("{0}", strMessage.Trim))
                Else
                    If (strMessage.Trim.Length > 0) Then fistError.AppendLine(String.Format("{0}", strMessage.Trim))
                End If
            End If
        Next
    End Sub

    ''' <summary>
    ''' Set controls default values 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetDefaultValues()
        Me.SourceFileTextBox.Text = ""
        'changed for dropdown selection
        Me.ToTableComboBox.SelectedIndex = 1
        Me.StartColTextBox.Text = ""
        Me.EndColTextBox.Text = ""
        Me.TransStartRowTextBox.Text = 1
        Me._excelFileName = ""
    End Sub

    ''' <summary>
    ''' Clear datatable content
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearAndDispose()
        Me._workSheetDataTable.Clear()
        'Me._worksheetColumnBindingSource = Nothing
        'Me._workSheetColumn = Nothing
        'Me._worksheetColumnDataTable.Clear()
    End Sub

    ''' <summary>
    ''' Return textbox name
    ''' </summary>
    ''' <param name="ctlText">Target textbox</param>
    ''' <returns>Target textbox name</returns>
    ''' <remarks></remarks>
    Public Function GetLabelName(ByVal ctlText As TextBox) As String

        GetLabelName = ""

        If ctlText.Name = Me.StartColTextBox.Name Then
            Return "Start Column"
        ElseIf ctlText.Name = Me.EndColTextBox.Name Then
            Return "End Column"
        ElseIf ctlText.Name = Me.TransStartRowTextBox.Name Then
            Return "Transaction Start Row"
        End If

    End Function

    Private Sub DeleteTableData()

        Dim dbCEConnectionString As String = String.Empty
        Dim dbCEConnection As SqlCeConnection = Nothing
        Dim dbCECommand As SqlCeCommand = Nothing
        Dim strDestTable As String


        Try
            dbCEConnectionString = strConnectionString

            strDestTable = Me.ToTableComboBox.SelectedItem.ToString
            dbCEConnection = New SqlCeConnection(dbCEConnectionString)

            dbCEConnection.Open()
            dbCECommand = dbCEConnection.CreateCommand()

            dbCECommand.CommandText = "DELETE from " & strDestTable
            dbCECommand.ExecuteNonQuery()



        Catch cex As SqlCeException
            'dbCETrans.Rollback()
            Throw New MagicException(cex.Message.ToString)
        Finally
            If dbCEConnection.State = ConnectionState.Open Then
                dbCEConnection.Close()
            End If
            dbCEConnection = Nothing

            If dbCECommand IsNot Nothing Then
                dbCECommand.Dispose()
            End If
        End Try
    End Sub

    Private Sub cboDelimiter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDelimiter.SelectedIndexChanged
        txtOtherDelimiter.Enabled = IIf(cboDelimiter.Text = "Other", True, False)
    End Sub

    Public Function getLetter(ByVal number As Integer) As String
        If (number > 26) Then
            Dim c As String = Convert.ToChar(number - 26 + 64).ToString()
            Return "A" + c
        Else
            Dim c As String = Convert.ToChar(number + 64).ToString()
            Return c
        End If
    End Function

    Private Sub ToTableComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToTableComboBox.SelectedIndexChanged

    End Sub
End Class
