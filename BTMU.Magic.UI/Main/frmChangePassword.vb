Imports BTMU.MAGIC.Common
Public Class frmChangePassword

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim objINIFile As INIFile

        If Not (ValidatePassword()) Then
            Exit Sub
        End If

        If Not (ValidateCurrentPassword()) Then
            Exit Sub
        End If

        If Not (ValidateNewPassword()) Then
            Exit Sub
        End If

        If (UCase(Trim(frmLogin.GsUserName)) = "BKMASTER") Then
            'Change Password
            objINIFile = New INIFile(frmLogin.GsFilePasswordAdmin)
            objINIFile.WriteString("Password", "Password", ComputeHash(NewPasswordTextBox.Text, "SHA512", Nothing))
        End If
        If (UCase(Trim(frmLogin.GsUserName)) = "SUPER") Then
            'Change Password
            objINIFile = New INIFile(frmLogin.GsFilePasswordSuperUser)
            objINIFile.WriteString("Password", "Password", ComputeHash(NewPasswordTextBox.Text, "SHA512", Nothing))
            'objINIFile.WriteString("Password", "Password", NewPasswordTextBox.Text)
        End If
        If (UCase(Trim(frmLogin.GsUserName)) = "COMMON") Then
            'Change Password
            objINIFile = New INIFile(frmLogin.GsFilePasswordCommonUser)
            objINIFile.WriteString("Password", "Password", ComputeHash(NewPasswordTextBox.Text, "SHA512", Nothing))
        End If

        If (UCase(Trim(frmLogin.GsUserName)) = "COMMON2") Then
            'Change Password
            objINIFile = New INIFile(frmLogin.GsFilePasswordCommon2User)
            objINIFile.WriteString("Password", "Password", ComputeHash(NewPasswordTextBox.Text, "SHA512", Nothing))
        End If

        Log_ChangePassword(frmLogin.GsUserName)
        MsgBox("Your password has been changed.", vbInformation, "Change Password")
        'Me.Close()


        ' UAT Issue No : 1 Added on 08-Sep-09 
        ' After changing the password, system should show the login screen.
        Log_Logout(frmLogin.GsUserName)
        Dim frmIndex As Int32 = 0
        For Each frm As Form In frmMain.MdiChildren
            frm.Close()
        Next

        While Application.OpenForms.Count > 3
            If Not (Application.OpenForms(frmIndex) Is Me Or Application.OpenForms(frmIndex) Is frmLogin Or _
                    Application.OpenForms(frmIndex) Is frmMain) Then
                Application.OpenForms(frmIndex).Close()
                frmIndex = 0
            Else
                frmIndex += 1
            End If
        End While
        frmMain.Hide()
        frmLogin.ClearForm()
        frmLogin.Show()
        frmLogin.UsernameTextBox.Focus()

        Me.Close()

    End Sub

    ''' <summary>
    ''' Validate current password which is inputted by user based on User Name
    ''' </summary>
    ''' <returns>True/False</returns>
    Private Function ValidateCurrentPassword() As Boolean
        Dim isCorrectPassword As Boolean = False
        Dim objINIFile As INIFile

        If (UCase(Trim(frmLogin.GsUserName)) = "BKMASTER") Then
            objINIFile = New INIFile(frmLogin.GsFilePasswordAdmin)
            isCorrectPassword = VerifyHash(OldPasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
        End If
        If (UCase(Trim(frmLogin.GsUserName)) = "SUPER") Then
            objINIFile = New INIFile(frmLogin.GsFilePasswordSuperUser)
            isCorrectPassword = VerifyHash(OldPasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
        End If
        If (UCase(Trim(frmLogin.GsUserName)) = "COMMON") Then
            objINIFile = New INIFile(frmLogin.GsFilePasswordCommonUser)
            isCorrectPassword = VerifyHash(OldPasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
        End If

        If (UCase(Trim(frmLogin.GsUserName)) = "COMMON2") Then
            objINIFile = New INIFile(frmLogin.GsFilePasswordCommon2User)
            isCorrectPassword = VerifyHash(OldPasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
        End If

        'Password Check
        If isCorrectPassword Then
            ValidateCurrentPassword = True
        Else
            ValidateCurrentPassword = False
            MsgBox("'Current Password' is incorrect, please fill in the correct password.", vbCritical, "Change Password")
            OldPasswordTextBox.Focus()
            OldPasswordTextBox.SelectAll()
        End If
    End Function

    ''' <summary>
    ''' Validate minimum requirement for Current and New Password 
    ''' </summary>
    ''' <returns>True/False</returns>
    ''' <remarks></remarks>
    Private Function ValidatePassword() As Boolean
        If Trim(OldPasswordTextBox.Text) = "" Then
            ValidatePassword = False
            MsgBox("'Current Password' is empty, please fill in 'Current Password'.", vbExclamation, "Change Password")
            OldPasswordTextBox.Focus()
            Exit Function
        End If
        If Trim(NewPasswordTextBox.Text) = "" Then
            ValidatePassword = False
            MsgBox("'New password' is empty, please fill in 'New Password'.", vbExclamation, "Change Password")
            NewPasswordTextBox.Focus()
            Exit Function
        End If
        If Trim(ConfirmPasswordTextBox.Text) = "" Then
            ValidatePassword = False
            MsgBox("'Confirm New Password' is empty, please fill in the 'Confirm New Password' to confirm.", vbExclamation, "Change Password")
            ConfirmPasswordTextBox.Focus()
            Exit Function
        End If
        ValidatePassword = True
    End Function

    ''' <summary>
    ''' Validate new password
    ''' </summary>
    ''' <returns>True/False</returns>
    ''' <remarks></remarks>
    Private Function ValidateNewPassword() As Boolean
        If Len((Trim(NewPasswordTextBox.Text))) < 4 Then
            ValidateNewPassword = False
            MsgBox("'New Password' is less than 4 characters, please fill at least 4 characters.", vbExclamation, "Change Password")
            NewPasswordTextBox.Focus()
            NewPasswordTextBox.SelectAll()
            Exit Function
        End If

        If Trim(NewPasswordTextBox.Text) = Trim(OldPasswordTextBox.Text) Then
            ValidateNewPassword = False
            MsgBox("'New Password' and 'Current Password' are the same, please fill in different password.", vbExclamation, "Change Password")
            NewPasswordTextBox.Focus()
            NewPasswordTextBox.SelectAll()
            Exit Function
        End If

        If Trim(ConfirmPasswordTextBox.Text) <> Trim(NewPasswordTextBox.Text) Then
            ValidateNewPassword = False
            MsgBox("Password entered in the 'Confirm New Password' is different. Please fill in the same password in 'New Password'.", vbExclamation, "Change Password")
            ConfirmPasswordTextBox.Focus()
            ConfirmPasswordTextBox.SelectAll()
            Exit Function
        End If
        ValidateNewPassword = True
    End Function

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Close()
    End Sub
End Class