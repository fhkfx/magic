<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResetPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResetPassword))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.DefaultPasswordLabel = New System.Windows.Forms.Label
        Me.DefaultPasswordTextBox = New System.Windows.Forms.TextBox
        Me.BrowseButton = New System.Windows.Forms.Button
        Me.OKButton = New System.Windows.Forms.Button
        Me._CancelButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Password files (*.dat)|*.dat"
        Me.OpenFileDialog1.Title = "Open Password File"
        '
        'DefaultPasswordLabel
        '
        Me.DefaultPasswordLabel.AutoSize = True
        Me.DefaultPasswordLabel.Location = New System.Drawing.Point(12, 32)
        Me.DefaultPasswordLabel.Name = "DefaultPasswordLabel"
        Me.DefaultPasswordLabel.Size = New System.Drawing.Size(90, 13)
        Me.DefaultPasswordLabel.TabIndex = 0
        Me.DefaultPasswordLabel.Text = "Default Password"
        '
        'DefaultPasswordTextBox
        '
        Me.DefaultPasswordTextBox.Location = New System.Drawing.Point(120, 29)
        Me.DefaultPasswordTextBox.Name = "DefaultPasswordTextBox"
        Me.DefaultPasswordTextBox.ReadOnly = True
        Me.DefaultPasswordTextBox.Size = New System.Drawing.Size(379, 20)
        Me.DefaultPasswordTextBox.TabIndex = 1
        '
        'BrowseButton
        '
        Me.BrowseButton.Location = New System.Drawing.Point(505, 23)
        Me.BrowseButton.Name = "BrowseButton"
        Me.BrowseButton.Size = New System.Drawing.Size(75, 30)
        Me.BrowseButton.TabIndex = 2
        Me.BrowseButton.Text = "Browse.."
        Me.BrowseButton.UseVisualStyleBackColor = True
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(203, 68)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(101, 33)
        Me.OKButton.TabIndex = 3
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        '_CancelButton
        '
        Me._CancelButton.Location = New System.Drawing.Point(310, 68)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(101, 33)
        Me._CancelButton.TabIndex = 4
        Me._CancelButton.Text = "Cancel"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        'frmResetPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 113)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.BrowseButton)
        Me.Controls.Add(Me.DefaultPasswordTextBox)
        Me.Controls.Add(Me.DefaultPasswordLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmResetPassword"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reset Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DefaultPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents DefaultPasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BrowseButton As System.Windows.Forms.Button
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents _CancelButton As System.Windows.Forms.Button
End Class
