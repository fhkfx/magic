<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbout))
        Me.OKButton = New System.Windows.Forms.Button
        Me.AboutPictureBox = New System.Windows.Forms.PictureBox
        Me.WarningContentLabel = New System.Windows.Forms.Label
        Me.WarningTitleLabel = New System.Windows.Forms.Label
        Me.CopyrightLabel = New System.Windows.Forms.Label
        Me.AboutContentLabel = New System.Windows.Forms.Label
        Me.BottomPanel = New System.Windows.Forms.Panel
        Me.MagicVersionLabel = New System.Windows.Forms.Label
        Me.TopPanel = New System.Windows.Forms.Panel
        Me.BodyPanel = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.AboutPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BottomPanel.SuspendLayout()
        Me.TopPanel.SuspendLayout()
        Me.BodyPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(502, 3)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 27)
        Me.OKButton.TabIndex = 0
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'AboutPictureBox
        '
        Me.AboutPictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AboutPictureBox.Image = Global.BTMU.Magic.UI.My.Resources.Resources.Magic_About
        Me.AboutPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.AboutPictureBox.Name = "AboutPictureBox"
        Me.AboutPictureBox.Size = New System.Drawing.Size(590, 184)
        Me.AboutPictureBox.TabIndex = 1
        Me.AboutPictureBox.TabStop = False
        '
        'WarningContentLabel
        '
        Me.WarningContentLabel.Location = New System.Drawing.Point(7, 88)
        Me.WarningContentLabel.Name = "WarningContentLabel"
        Me.WarningContentLabel.Size = New System.Drawing.Size(577, 39)
        Me.WarningContentLabel.TabIndex = 1
        Me.WarningContentLabel.Text = "Warning Content"
        '
        'WarningTitleLabel
        '
        Me.WarningTitleLabel.AutoSize = True
        Me.WarningTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WarningTitleLabel.Location = New System.Drawing.Point(7, 72)
        Me.WarningTitleLabel.Name = "WarningTitleLabel"
        Me.WarningTitleLabel.Size = New System.Drawing.Size(62, 13)
        Me.WarningTitleLabel.TabIndex = 0
        Me.WarningTitleLabel.Text = "Warning :"
        '
        'CopyrightLabel
        '
        Me.CopyrightLabel.AutoSize = True
        Me.CopyrightLabel.Font = New System.Drawing.Font("Arial", 8.3!, System.Drawing.FontStyle.Bold)
        Me.CopyrightLabel.Location = New System.Drawing.Point(7, 8)
        Me.CopyrightLabel.Name = "CopyrightLabel"
        Me.CopyrightLabel.Size = New System.Drawing.Size(337, 15)
        Me.CopyrightLabel.TabIndex = 3
        Me.CopyrightLabel.Text = "Copyright � 2007-2018 MUFG Bank, Ltd. All rights reserved"
        '
        'AboutContentLabel
        '
        Me.AboutContentLabel.Location = New System.Drawing.Point(7, 38)
        Me.AboutContentLabel.Name = "AboutContentLabel"
        Me.AboutContentLabel.Size = New System.Drawing.Size(576, 31)
        Me.AboutContentLabel.TabIndex = 4
        Me.AboutContentLabel.Text = "About Content"
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.SystemColors.Control
        Me.BottomPanel.Controls.Add(Me.CopyrightLabel)
        Me.BottomPanel.Controls.Add(Me.OKButton)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 321)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(590, 34)
        Me.BottomPanel.TabIndex = 5
        '
        'MagicVersionLabel
        '
        Me.MagicVersionLabel.AutoSize = True
        Me.MagicVersionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MagicVersionLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MagicVersionLabel.Location = New System.Drawing.Point(498, 10)
        Me.MagicVersionLabel.Name = "MagicVersionLabel"
        Me.MagicVersionLabel.Size = New System.Drawing.Size(87, 13)
        Me.MagicVersionLabel.TabIndex = 7
        Me.MagicVersionLabel.Text = "Magic Version"
        Me.MagicVersionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TopPanel
        '
        Me.TopPanel.Controls.Add(Me.AboutPictureBox)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(590, 184)
        Me.TopPanel.TabIndex = 8
        '
        'BodyPanel
        '
        Me.BodyPanel.Controls.Add(Me.Label1)
        Me.BodyPanel.Controls.Add(Me.AboutContentLabel)
        Me.BodyPanel.Controls.Add(Me.MagicVersionLabel)
        Me.BodyPanel.Controls.Add(Me.WarningTitleLabel)
        Me.BodyPanel.Controls.Add(Me.WarningContentLabel)
        Me.BodyPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BodyPanel.Location = New System.Drawing.Point(0, 184)
        Me.BodyPanel.Name = "BodyPanel"
        Me.BodyPanel.Size = New System.Drawing.Size(590, 137)
        Me.BodyPanel.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(241, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "East Asia"
        '
        'frmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(590, 355)
        Me.Controls.Add(Me.BodyPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.BottomPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAbout"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About"
        CType(Me.AboutPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        Me.TopPanel.ResumeLayout(False)
        Me.BodyPanel.ResumeLayout(False)
        Me.BodyPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents AboutPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents CopyrightLabel As System.Windows.Forms.Label
    Friend WithEvents WarningTitleLabel As System.Windows.Forms.Label
    Friend WithEvents WarningContentLabel As System.Windows.Forms.Label
    Friend WithEvents AboutContentLabel As System.Windows.Forms.Label
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents MagicVersionLabel As System.Windows.Forms.Label
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents BodyPanel As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
