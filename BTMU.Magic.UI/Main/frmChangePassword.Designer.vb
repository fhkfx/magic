<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangePassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChangePassword))
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Cancel = New System.Windows.Forms.Button
        Me.OK = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ConfirmPasswordTextBox = New System.Windows.Forms.TextBox
        Me.ConfirmPasswordLabel = New System.Windows.Forms.Label
        Me.NewPasswordTextBox = New System.Windows.Forms.TextBox
        Me.OldPasswordTextBox = New System.Windows.Forms.TextBox
        Me.NewPasswordLabel = New System.Windows.Forms.Label
        Me.OldPasswordLabel = New System.Windows.Forms.Label
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Cancel)
        Me.Panel2.Controls.Add(Me.OK)
        Me.Panel2.Location = New System.Drawing.Point(169, 128)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(243, 48)
        Me.Panel2.TabIndex = 13
        '
        'Cancel
        '
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Location = New System.Drawing.Point(115, 9)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(101, 33)
        Me.Cancel.TabIndex = 4
        Me.Cancel.Text = "&Cancel"
        '
        'OK
        '
        Me.OK.Location = New System.Drawing.Point(12, 9)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(101, 33)
        Me.OK.TabIndex = 3
        Me.OK.Text = "&OK"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConfirmPasswordTextBox)
        Me.Panel1.Controls.Add(Me.ConfirmPasswordLabel)
        Me.Panel1.Controls.Add(Me.NewPasswordTextBox)
        Me.Panel1.Controls.Add(Me.OldPasswordTextBox)
        Me.Panel1.Controls.Add(Me.NewPasswordLabel)
        Me.Panel1.Controls.Add(Me.OldPasswordLabel)
        Me.Panel1.Location = New System.Drawing.Point(3, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(409, 110)
        Me.Panel1.TabIndex = 12
        '
        'ConfirmPasswordTextBox
        '
        Me.ConfirmPasswordTextBox.Location = New System.Drawing.Point(128, 68)
        Me.ConfirmPasswordTextBox.MaxLength = 50
        Me.ConfirmPasswordTextBox.Name = "ConfirmPasswordTextBox"
        Me.ConfirmPasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.ConfirmPasswordTextBox.Size = New System.Drawing.Size(254, 20)
        Me.ConfirmPasswordTextBox.TabIndex = 6
        '
        'ConfirmPasswordLabel
        '
        Me.ConfirmPasswordLabel.Location = New System.Drawing.Point(9, 68)
        Me.ConfirmPasswordLabel.Name = "ConfirmPasswordLabel"
        Me.ConfirmPasswordLabel.Size = New System.Drawing.Size(140, 20)
        Me.ConfirmPasswordLabel.TabIndex = 7
        Me.ConfirmPasswordLabel.Text = "Confirm Password"
        Me.ConfirmPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NewPasswordTextBox
        '
        Me.NewPasswordTextBox.Location = New System.Drawing.Point(128, 42)
        Me.NewPasswordTextBox.MaxLength = 50
        Me.NewPasswordTextBox.Name = "NewPasswordTextBox"
        Me.NewPasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.NewPasswordTextBox.Size = New System.Drawing.Size(254, 20)
        Me.NewPasswordTextBox.TabIndex = 2
        '
        'OldPasswordTextBox
        '
        Me.OldPasswordTextBox.Location = New System.Drawing.Point(128, 16)
        Me.OldPasswordTextBox.MaxLength = 50
        Me.OldPasswordTextBox.Name = "OldPasswordTextBox"
        Me.OldPasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.OldPasswordTextBox.Size = New System.Drawing.Size(254, 20)
        Me.OldPasswordTextBox.TabIndex = 1
        '
        'NewPasswordLabel
        '
        Me.NewPasswordLabel.Location = New System.Drawing.Point(9, 42)
        Me.NewPasswordLabel.Name = "NewPasswordLabel"
        Me.NewPasswordLabel.Size = New System.Drawing.Size(140, 20)
        Me.NewPasswordLabel.TabIndex = 5
        Me.NewPasswordLabel.Text = "New Password"
        Me.NewPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'OldPasswordLabel
        '
        Me.OldPasswordLabel.Location = New System.Drawing.Point(9, 15)
        Me.OldPasswordLabel.Name = "OldPasswordLabel"
        Me.OldPasswordLabel.Size = New System.Drawing.Size(140, 20)
        Me.OldPasswordLabel.TabIndex = 1
        Me.OldPasswordLabel.Text = "Current Password"
        Me.OldPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmChangePassword
        '
        Me.AcceptButton = Me.OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 190)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmChangePassword"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Password"
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConfirmPasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConfirmPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents NewPasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OldPasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents OldPasswordLabel As System.Windows.Forms.Label
End Class
