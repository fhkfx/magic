Imports System.IO
Imports BTMU.MAGIC.Common
Public Class frmResetPassword

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub BrowseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BrowseButton.Click
        Dim stFileName As String = ""
        Dim stFilePathAndName As String = ""

        openFileDialog1.InitialDirectory = System.Environment.CurrentDirectory
        OpenFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = DialogResult.OK Then
            stFilePathAndName = openFileDialog1.FileName
            Dim MyFile As FileInfo = New FileInfo(stFilePathAndName)
            stFileName = MyFile.Name
        End If

        If Not IsNothingOrEmptyString(stFilePathAndName) Then DefaultPasswordTextBox.Text = stFilePathAndName

    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Dim objINIFile As INIFile
        Dim stFileName As String = ""
        Dim stFilePathAndName As String = ""
        Dim NewPassword As String

        Try
            If Trim(DefaultPasswordTextBox.Text) = "" Then
                MsgBox("Please select the default password file." & Chr(13) & "If you don't have default password file, please contact the bank administrator.", vbExclamation, "Reset Password")
                BrowseButton.Focus()
                Exit Sub
            End If

            stFilePathAndName = OpenFileDialog1.FileName
            Dim MyFile As FileInfo = New FileInfo(stFilePathAndName)
            stFileName = MyFile.Name

            'Validate File Name (Only can accept: pSuper.dat and pCommon.dat)
            If Not (UCase(stFileName) = "PSUPER.DAT" Or UCase(stFileName) = "PCOMMON.DAT") Then
                MsgBox("This is not a correct default password. " & Chr(13) & "Please select another file.", vbExclamation, "Reset Password")
                BrowseButton.Focus()
                Exit Sub
            End If

            If UCase(Trim(frmLogin.GsUserName)) = "SUPER" And UCase(stFileName) = "PSUPER.DAT" Then
                MsgBox("You can reset only common user password.", vbExclamation, "Reset Password")
                BrowseButton.Focus()
                Exit Sub
            End If

            objINIFile = New INIFile(stFilePathAndName)
            NewPassword = objINIFile.GetString("Password", "Password", "")
            If Not IsNothingOrEmptyString(NewPassword) Then
                If MsgBox("Are you sure that you want to reset password?", vbQuestion & vbYesNo, "Reset Password") = vbYes Then
                    If UCase(stFileName) = "PSUPER.DAT" Then
                        MyFile.CopyTo(frmLogin.GsFilePasswordSuperUser, True)
                        Log_ResetPassword(frmLogin.GsUserName)
                        MsgBox("Password has been reset successfully for super user.", vbInformation, "Reset Password")
                    ElseIf UCase(stFileName) = "PCOMMON.DAT" Then
                        MyFile.CopyTo(frmLogin.GsFilePasswordCommonUser, True)
                        Log_ResetPassword(frmLogin.GsUserName)
                        MsgBox("Password has been reset successfully for common user.", vbInformation, "Reset Password")
                    End If
                End If
                Close()
            Else
                MsgBox("The default password file could not be read. " & Chr(13) & "Please select the correct default password file.", vbExclamation, "Reset Password")
                BrowseButton.Focus()
            End If
            

        Catch ex As Exception
            MsgBox("The default password file could not be read. " & Chr(13) & "Please select the correct default password file.", vbExclamation, "Reset Password")
            BrowseButton.Focus()

        End Try

    End Sub

    Private Sub CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Close()
    End Sub

End Class