Public Class frmAbout

    Private Sub frmAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SuspendLayout()

        Dim strWarning As String
        Dim strAboutContent As String
        Dim strDisclaimerContent As String

        strWarning = "This software program is protected by copyright law and intellectual property rights. "
        strWarning &= "Unauthorized reproduction or distribution of this program, or any portion of it, "
        strWarning &= "may result in civil and/or criminal actions, claims and proceedings."

        strAboutContent = """COMSUITE Conversion Tool"" is an intelligent data conversion tool, which enables you to convert "
        strAboutContent &= "payment data file in Text, Excel, SWIFT or Unstructured format to the Bank's "
        strAboutContent &= "payment file format."

        strDisclaimerContent = "Disclaimer" & vbCrLf & vbCrLf
        strDisclaimerContent &= "While every effort has been made to ensure high quality of COMSUITE Conversion Tool, "
        strDisclaimerContent &= "MUFG Bank, Ltd., (""MUFG Bank"") is not responsible for "
        strDisclaimerContent &= "any errors or omissions in COMSUITE Conversion Tool. No part of COMSUITE Conversion Tool may be reproduced, "
        strDisclaimerContent &= "transmitted, transcribed, stored in a retrieval system "
        strDisclaimerContent &= "in any form, by any means, without the prior permission of MUFG Bank. "

        strDisclaimerContent &= "MUFG Bank makes no warranty for damages resulting from corrupted or loss data due to "
        strDisclaimerContent &= "the operation or use of COMSUITE Conversion Tool. " & vbCrLf & vbCrLf

        strDisclaimerContent &= "MUFG Bank makes no guarantees of any kind with regards to the COMSUITE Conversion Tool, files or any "
        strDisclaimerContent &= "other materials to which COMSUITE Conversion Tool relates. COMSUITE Conversion Tool, files and any other "
        strDisclaimerContent &= "materials are supplied ""as is""." & vbCrLf & vbCrLf

        strDisclaimerContent &= "MUFG Bank disclaims all warranties expressed or implied, including without limitation, "
        strDisclaimerContent &= "implied warranties of merchantability, fitness for a particular purpose and "
        strDisclaimerContent &= "non-infringement. MUFG Bank shall not be held liable for errors contained in COMSUITE Conversion Tool "
        strDisclaimerContent &= "or for loss of profits, loss of business, loss of contract, loss of revenue, "
        strDisclaimerContent &= "loss of anticipated savings, loss of goodwill, loss of opportunities, consequential "
        strDisclaimerContent &= "or incidental damages of any nature whatsoever incurred as a result of acting on "
        strDisclaimerContent &= "the operation or use of COMSUITE Conversion Tool or any other matters whatsoever." & vbCrLf & vbCrLf

        Me.WarningContentLabel.Text = strWarning
        Me.AboutContentLabel.Text = strAboutContent
        'Me.DisclaimerTextBox.Text = strDisclaimerContent

        Me.MagicVersionLabel.Text = "Version " & Application.ProductVersion

        ResumeLayout()

    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

    Private Sub MagicVersionLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MagicVersionLabel.Click

    End Sub
End Class