Imports BTMU.MAGIC.Common
Imports system.IO

Public Class frmLogin

    'Modified by Swamy on 11 Aug 2009
    'Please do not make any changes here
    'change by kaler on 2009.08.25, password file's path is not configurable
    Public GsFilePasswordAdmin = GetFilePasswordAdmin() 'Path.Combine(Application.StartupPath, My.Settings.Item("FilePasswordAdmin"))
    Public GsFilePasswordSuperUser = GetFilePasswordSuperUser() 'Path.Combine(Application.StartupPath, My.Settings.Item("FilePasswordSuperUser"))
    Public GsFilePasswordCommonUser = GetFilePasswordCommonUser() 'Path.Combine(Application.StartupPath, My.Settings.Item("FilePasswordCommonUser"))
    '********************
    'New user credential is added to implement BTMU-BKK branch's request
    'new account is called common2, which can't import/view lookup data. (lowest user level)
    '********************
    Public GsFilePasswordCommon2User = GetFilePasswordCommon2User() 'Path.Combine(Application.StartupPath, My.Settings.Item("FilePasswordCommonUser"))

    Public Shared GsUserName As String
    Public Shared bOK As Boolean
    Public Shared isLogin As Boolean = True


    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    ''' <summary>
    ''' System will validate password based on User Name. If password is correct, Main form will be displayed.
    ''' </summary>
    ''' <param name="sender">OK Button</param>
    ''' <param name="e">Click event</param>
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim objINIFile As INIFile
        'Dim fMain As New frmMain

        If Not (ValidateLogin()) Then
            Exit Sub
        End If

        If ((UCase(Trim(UsernameTextBox.Text)) = "BKMASTER") Or _
        (UCase(Trim(UsernameTextBox.Text)) = "SUPER") Or _
        (UCase(Trim(UsernameTextBox.Text)) = "COMMON") Or _
        (UCase(Trim(UsernameTextBox.Text)) = "COMMON2")) Then

            Dim isCorrectPassword As Boolean = False

            If (UCase(Trim(UsernameTextBox.Text)) = "BKMASTER") Then
                'Read File
                objINIFile = New INIFile(GsFilePasswordAdmin)
                isCorrectPassword = VerifyHash(PasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
            End If
            If (UCase(Trim(UsernameTextBox.Text)) = "SUPER") Then
                'Read File
                objINIFile = New INIFile(GsFilePasswordSuperUser)
                isCorrectPassword = VerifyHash(PasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
            End If
            If (UCase(Trim(UsernameTextBox.Text)) = "COMMON") Then
                'Read File
                objINIFile = New INIFile(GsFilePasswordCommonUser)
                isCorrectPassword = VerifyHash(PasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
            End If

            'newly added user for BTMU-BKK
            If (UCase(Trim(UsernameTextBox.Text)) = "COMMON2") Then
                'Read File
                objINIFile = New INIFile(GsFilePasswordCommon2User)
                isCorrectPassword = VerifyHash(PasswordTextBox.Text.Trim, "SHA512", objINIFile.GetString("Password", "Password", ""))
            End If

            'Password Check
            If isCorrectPassword Then
                GsUserName = Trim(UsernameTextBox.Text)
                bOK = True
                Me.Hide()
            Else
                bOK = False
                MsgBox("The password is incorrect. Please try again.", vbCritical, "Login")
                PasswordTextBox.Focus()
                PasswordTextBox.SelectAll()
            End If
        Else
            bOK = False
            MsgBox("The user name is incorrect. Please try again.", vbCritical, "Login")
            UsernameTextBox.Focus()
            UsernameTextBox.SelectAll()
        End If

        If bOK Then
            isLogin = True
            Log_Login(frmLogin.GsUserName)
            frmMain.UserPriviledge(GsUserName)
            frmMain.Show()
        End If
    End Sub

    ''' <summary>
    ''' Handles Cancel Button. System will be ended.
    ''' </summary>
    ''' <param name="sender">Cancel Button</param>
    ''' <param name="e">Click Event</param>
    ''' <remarks></remarks>
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
        Application.Exit()
    End Sub

    ''' <summary>
    ''' When login screen is called, system will check setting in configuration file. 
    ''' If Login Mode is 0 (False) then system will be redirect to Main Form and logged in as Common User.
    ''' Otherwise user will be prompted with login screen. 
    ''' </summary>
    ''' <param name="sender">Login Form</param>
    ''' <param name="e">Load Event</param>
    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Enable / Disable Exception Log
        BTMUExceptionManager.EnableExceptionLog = My.Settings.EnableExceptionLog

        SuspendLayout()
        If GBLConfigurationMaster Is Nothing Then
            GBLConfigurationMaster = New ConfigurationMaster
            Try
                GBLConfigurationMaster = ConfigurationMaster.ReadConfigurationFromFile(AppDomain.CurrentDomain.BaseDirectory + "\magic.xml")
                If Not GBLConfigurationMaster.LoginMode Then
                    GsUserName = "COMMON"
                    bOK = True
                    isLogin = False
                    Log_Login(frmLogin.GsUserName)
                    frmMain.UserPriviledge(GsUserName)
                    frmMain.Show()
                    Me.BeginInvoke(New MethodInvoker(AddressOf HideForm))
                End If

                UsernameTextBox.Focus()
                Me.MagicVersionLabel.Text = "Version " & Application.ProductVersion

            Catch ex As Exception
                MsgBox("Cannot find file magic.xml", vbExclamation, "File Not Found")
                End
            End Try
        End If
        ResumeLayout()

    End Sub

    ''' <summary>
    ''' To check User Name and Password are not blank.
    ''' </summary>
    ''' <returns>True/False</returns>
    ''' <remarks></remarks>
    Private Function ValidateLogin() As Boolean
        If Trim(UsernameTextBox.Text) = "" Then
            ValidateLogin = False
            MsgBox("'User Name' is empty, please fill in 'User Name'.", vbExclamation, "Login")
            UsernameTextBox.Focus()
            Exit Function
        End If
        If Trim(PasswordTextBox.Text) = "" Then
            ValidateLogin = False
            MsgBox("'Password' is empty, please fill in 'Password'.", vbExclamation, "Login")
            PasswordTextBox.Focus()
            Exit Function
        End If
        ValidateLogin = True
    End Function

    ''' <summary>
    ''' Reset User Name and password value to blank.
    ''' </summary>
     Public Sub ClearForm()
        UsernameTextBox.Text = String.Empty
        PasswordTextBox.Text = String.Empty
    End Sub

    ''' <summary>
    ''' To make this screen invisible
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub HideForm()
        Me.Visible = False
        Me.Hide()
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub
End Class