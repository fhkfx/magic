Imports System.IO
Imports System.Xml.Serialization
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.Magic.Common
Imports System.Xml

 Public Class frmMain

    Private Sub GenerateMasterTemplateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GenerateMasterTemplateToolStripMenuItem.Click

        frmMasterTemplateNonFixedBase.MdiParent = Me
        frmMasterTemplateNonFixedBase.Show()
        frmMasterTemplateNonFixedBase.WindowState = FormWindowState.Maximized

    End Sub

    Private Sub ExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExcelToolStripMenuItem.Click
        frmCommonTemplateExcel.MdiParent = Me
        frmCommonTemplateExcel.Show()
        frmCommonTemplateExcel.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub TextToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextToolStripMenuItem.Click
        frmCommonTemplateText.MdiParent = Me
        frmCommonTemplateText.Show()
        frmCommonTemplateText.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub QuickConversionConfigurationEditorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuickConversionConfigurationEditorToolStripMenuItem.Click
        frmQuickConversionConfigurationEditor.MdiParent = Me
        frmQuickConversionConfigurationEditor.Show()
        frmQuickConversionConfigurationEditor.WindowState = FormWindowState.Maximized
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        'Dim objE As New MasterTemplateNonFixed

        'System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture

        'Dim serializer As New XmlSerializer(GetType(MasterTemplateNonFixed))
        'Dim stream As FileStream = File.Open("C:\mydata.txt", FileMode.Open)
        'objE = DirectCast(serializer.Deserialize(stream), MasterTemplateNonFixed)
        'stream.Close()
        UserPriviledge(frmLogin.GsUserName)


    End Sub

    Private Sub GCMCMTToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GCMCMTToolStripMenuItem.Click
        frmGCMSMTBase.MdiParent = Me
        frmGCMSMTBase.Show()
        frmGCMSMTBase.WindowState = FormWindowState.Normal

    End Sub

    Private Sub frmMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub ChangePasswordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangePasswordToolStripMenuItem.Click
        Dim f As New frmChangePassword
        'frmChangePassword.MdiParent = Me
        f.ShowDialog()
    End Sub


    Private Sub ResetPasswordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetPasswordToolStripMenuItem.Click
        'frmResetPassword.MdiParent = Me
        Dim f As New frmResetPassword
        f.ShowDialog()
    End Sub

    Private Sub GenerateMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GenerateMasterToolStripMenuItem.Click
        frmMasterTemplateFixedBase.MdiParent = Me
        frmMasterTemplateFixedBase.Show()
        frmMasterTemplateFixedBase.WindowState = FormWindowState.Maximized

    End Sub

    Private Sub ImportFromExcelToDBToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportFromExcelToDBToolStripMenuItem.Click
        frmExcel2DB.MdiParent = Me
        frmExcel2DB.Show()
        frmExcel2DB.WindowState = FormWindowState.Normal
    End Sub

    Private Sub ViewFromDBToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewFromDBToolStripMenuItem.Click
        frmViewFromDB.MdiParent = Me
        frmViewFromDB.Show()
        frmViewFromDB.WindowState = FormWindowState.Normal
    End Sub


    Private Sub ViewActivityLogToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewActivityLogToolStripMenuItem.Click
        If UCase(frmLogin.GsUserName) = "BKMASTER" Then
            frmLogActivityAdmin.MdiParent = Me
            frmLogActivityAdmin.Show()
            frmLogActivityAdmin.WindowState = FormWindowState.Maximized
        Else
            frmLogActivitySuper.MdiParent = Me
            frmLogActivitySuper.Show()
            frmLogActivitySuper.WindowState = FormWindowState.Maximized
        End If
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click

        If MessageBox.Show("Are you sure you want to log out?", "Confirm Log out", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Cancel Then Exit Sub

        'unload the form
        Log_Logout(frmLogin.GsUserName)

        Dim frmIndex As Int32 = 0

        For Each frm As Form In Me.MdiChildren
            frm.Close()
        Next

        While Application.OpenForms.Count > 2

            If Not (Application.OpenForms(frmIndex) Is Me Or Application.OpenForms(frmIndex) Is frmLogin) Then
                Application.OpenForms(frmIndex).Close()
                frmIndex = 0
            Else
                frmIndex += 1
            End If

        End While

        Me.Hide()

        frmLogin.ClearForm()
        frmLogin.Show()
        frmLogin.UsernameTextBox.Focus()
    End Sub

    Public Sub UserPriviledge(ByVal GsUserName As String)
        If UCase(GsUserName) = "BKMASTER" Then
            'Login/Logout
            'View Online Help
            'View Activity Log (Current and Archive log folder)
            'Change(Password)
            'Reset(Password)
            'Create Master Template
            'Create Common Template
            'Create Definition File for Unstructured File Conversion
            'Exit

            Me.ViewActivityLogToolStripMenuItem.Visible = True
            Me.ResetPasswordToolStripMenuItem.Visible = True
            'Me.GetNewMasterTemplateToolStripMenuItem.Visible = False
            Me.TemplateToolStripMenuItem.Visible = True
            Me.GenerateMasterTemplateToolStripMenuItem.Visible = True
            Me.GenerateMasterToolStripMenuItem.Visible = True
            Me.CommonTransactionTemplateToolStripMenuItem.Visible = True
            Me.GenerateSwiftToolStripMenuItem.Visible = True
            Me.QuickConversionConfigurationEditorToolStripMenuItem.Visible = True
            Me.DataEntryToolStripMenuItem.Visible = False
            Me.ConvertToolStripMenuItem.Visible = False
            Me.UtilityToolStripMenuItem.Visible = True
            Me.LogoutToolStripMenuItem.Visible = True
            Me.ToolStripMenuItem1.Visible = True
            Me.ToolStripMenuItem5.Visible = True
            'Me.mnuConvert.Visible = False

        ElseIf UCase(GsUserName) = "SUPER" Then
            'Login/Logout
            'View Online Help
            'View Activity Log (Current log folder)
            'Change(Password)
            'Reset(Password)
            'Create Common Template
            'GCMS Money Transfer Data Entry 
            'Create Definition File for Unstructured File Conversion
            'Convert Unstructured File
            'Exit

            Me.ViewActivityLogToolStripMenuItem.Visible = True
            Me.ResetPasswordToolStripMenuItem.Visible = True
            'Me.GetNewMasterTemplateToolStripMenuItem.Visible = True
            Me.TemplateToolStripMenuItem.Visible = True
            Me.GenerateMasterTemplateToolStripMenuItem.Visible = False
            Me.GenerateMasterToolStripMenuItem.Visible = False
            Me.CommonTransactionTemplateToolStripMenuItem.Visible = True
            Me.GenerateSwiftToolStripMenuItem.Visible = True
            Me.QuickConversionConfigurationEditorToolStripMenuItem.Visible = True
            Me.ConvertToolStripMenuItem.Visible = False
            Me.DataEntryToolStripMenuItem.Visible = True
            Me.UtilityToolStripMenuItem.Visible = True
            Me.LogoutToolStripMenuItem.Visible = True
            Me.ToolStripMenuItem1.Visible = True
            Me.ToolStripMenuItem5.Visible = False
            'Me.mnuConvert.Visible = False

        ElseIf UCase(GsUserName) = "COMMON" Then
            'Login/Logout
            'View Online Help
            'Change(Password)
            'File Conversion - Quick Conversion
            'File Conversion - Manual Conversion
            'GCMS Money Transfer Data Entry 
            'Exit
            'If Not frmLogin.isLogin Then
            '    Me.LogoutToolStripMenuItem.Visible = False
            '    Me.ToolStripMenuItem1.Visible = False
            'Else
            Me.LogoutToolStripMenuItem.Visible = True
            Me.ToolStripMenuItem1.Visible = True
            'End If
            Me.ViewActivityLogToolStripMenuItem.Visible = True
            Me.ResetPasswordToolStripMenuItem.Visible = False
            'Me.GetNewMasterTemplateToolStripMenuItem.Visible = False
            Me.TemplateToolStripMenuItem.Visible = False
            Me.QuickConversionConfigurationEditorToolStripMenuItem.Visible = False
            Me.ConvertToolStripMenuItem.Visible = True
            Me.DataEntryToolStripMenuItem.Visible = True
            Me.UtilityToolStripMenuItem.Visible = True
            Me.ToolStripMenuItem5.Visible = True

            frmQuickConversion.MdiParent = Me
            frmQuickConversion.WindowState = FormWindowState.Normal
            frmQuickConversion.StartPosition = FormStartPosition.CenterScreen
            frmQuickConversion.Show()

        ElseIf UCase(GsUserName) = "COMMON2" Then
            'added for BTMU-BKK branch

            Me.LogoutToolStripMenuItem.Visible = True
            Me.ToolStripMenuItem1.Visible = True

            Me.ViewActivityLogToolStripMenuItem.Visible = True
            Me.ResetPasswordToolStripMenuItem.Visible = False

            Me.TemplateToolStripMenuItem.Visible = False
            Me.QuickConversionConfigurationEditorToolStripMenuItem.Visible = False
            Me.ConvertToolStripMenuItem.Visible = True
            Me.DataEntryToolStripMenuItem.Visible = True
            'only the difference between COMMON and COMMON2
            Me.UtilityToolStripMenuItem.Visible = False
            Me.ToolStripMenuItem5.Visible = True

            frmQuickConversion.MdiParent = Me
            frmQuickConversion.WindowState = FormWindowState.Normal
            frmQuickConversion.StartPosition = FormStartPosition.CenterScreen
            frmQuickConversion.Show()

        End If

    End Sub









    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        If MsgBox("Are you sure you want to exit?", vbQuestion & vbOKCancel, "Exit") = vbOK Then
            'unload the form
            Log_Logout(frmLogin.GsUserName)

            If System.Windows.Forms.Application.OpenForms.Count > 2 Then 'frmMain and frmlogin
                For i As Integer = System.Windows.Forms.Application.OpenForms.Count - 1 To 2 Step -1
                    Dim form As Form = System.Windows.Forms.Application.OpenForms(i)
                    form.Close()
                Next i
            End If

            Me.Close()



        End If
    End Sub

    Private Sub GenerateSwiftToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GenerateSwiftToolStripMenuItem.Click
        frmSwiftTemplate.MdiParent = Me
        frmSwiftTemplate.Show()
        frmSwiftTemplate.WindowState = FormWindowState.Maximized
    End Sub



    Private Sub UnstructuredFileConverterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnstructuredFileConverterToolStripMenuItem.Click


        frmMA5000.MdiParent = Me
        frmMA5000.Show()
        'frmMA5000.WindowState = FormWindowState.Normal
        frmMA5000.Left = Convert.ToInt32((Me.Width - 848) / 2)
        frmMA5000.Top = Convert.ToInt32((Me.Height - 289) / 3)

    End Sub

    Private Sub UnstructureTextToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnstructureTextToolStripMenuItem.Click
        frmCommonTemplateUnstructuredText.MdiParent = Me
        frmCommonTemplateUnstructuredText.Show()
        frmCommonTemplateUnstructuredText.Left = Convert.ToInt32((Me.Width - 474) / 2)
        frmCommonTemplateUnstructuredText.Top = Convert.ToInt32((Me.Height - 222) / 3)
     End Sub

    Private Sub ExcelToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExcelToolStripMenuItem1.Click
        frmConvertCommonTransactionExcel.MdiParent = Me
        frmConvertCommonTransactionExcel.Show()


        frmConvertCommonTransactionExcel.Left = Convert.ToInt32((Me.Width - 716) / 2)
        frmConvertCommonTransactionExcel.Top = Convert.ToInt32((Me.Height - 340) / 3)
     End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        Dim f As New frmAbout
        'frmAbout.MdiParent = Me
        'frmAbout.Show()
        f.ShowDialog()
        'frmAbout.WindowState = FormWindowState.Normal
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileName As String = AppDomain.CurrentDomain.BaseDirectory + "magic.lic"
        Try
            If (Not HelperModule.VerifyLicense(fileName)) Then
                MessageBox.Show("System is not able to continue because license information does not match")
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Me.Close()
        End Try

        'Modified by Swamy
        'Please do not remove the following lines
        InitAppSettings()


        ArchiveLogFile()
        DeleteArchiveLogFile()

    End Sub

    Private Sub TextToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextToolStripMenuItem1.Click
        frmConvertCommonTransactionText.MdiParent = Me
        frmConvertCommonTransactionText.Show()

        frmConvertCommonTransactionText.Left = Convert.ToInt32((Me.Width - 716) / 2)
        frmConvertCommonTransactionText.Top = Convert.ToInt32((Me.Height - 340) / 3)


     End Sub

    Private Sub SWIFTTransactionTemplateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SWIFTTransactionTemplateToolStripMenuItem.Click
        frmConvertCommonTransactionSWIFT.MdiParent = Me
        frmConvertCommonTransactionSWIFT.Show()

        frmConvertCommonTransactionSWIFT.Left = Convert.ToInt32((Me.Width - 716) / 2)
        frmConvertCommonTransactionSWIFT.Top = Convert.ToInt32((Me.Height - 292) / 3)
    End Sub

    Private Sub InitAppSettings()

        MasterTemplateSharedFunction._masterTemplateFileExtension = MasterTemplateModule.MasterTemplateFileExtension
        MasterTemplateSharedFunction._masterTemplateViewDraftFolder = MasterTemplateModule.MasterTemplateViewDraftFolder
        MasterTemplateSharedFunction._masterTemplateViewFolder = MasterTemplateModule.MasterTemplateViewFolder
        BTMU.Magic.FileFormat.BaseFileFormat.GnuPG_Home = My.Settings.GnuPGHome
        BTMU.Magic.FileFormat.BaseFileFormat.GnuPG147 = My.Settings.Gnu147
        BTMU.Magic.FileFormat.BaseFileFormat.GnuPG147_PubKey = My.Settings.Gnu147PubKey
    End Sub

    Private Sub MenuStrip1_ItemAdded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemEventArgs) Handles MenuStrip1.ItemAdded
        If e.Item.Image IsNot Nothing AndAlso e.Item.GetType().ToString() = "System.Windows.Forms.MdiControlStrip+SystemMenuItem" Then
            e.Item.Visible = False
        End If
    End Sub

    Private Sub ContentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContentToolStripMenuItem.Click

        Try
            Dim file_path As String = GetHelpFile()

            If Not File.Exists(file_path) Then
                BTMUExceptionManager.LogAndShowError("File not found", String.Format("Help File '{0}' is not found.", file_path))
            Else
                System.Diagnostics.Process.Start(file_path)

            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Application Error", "Application error has been encountered please contact your Administrator/Application Support Engineer for help.")
            BTMU.Magic.Common.BTMUExceptionManager.HandleException(ex)
        End Try


    End Sub

End Class
