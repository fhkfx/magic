<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FunctionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewActivityLogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ChangePasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ResetPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.QuickConversionConfigurationEditorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TemplateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GenerateMasterTemplateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GenerateMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator
        Me.CommonTransactionTemplateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UnstructureTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator
        Me.GenerateSwiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator
        Me.UnstructuredFileConverterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ConvertToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CommonTransactionTemplateToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.TextToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ExcelToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.SWIFTTransactionTemplateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DataEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GCMCMTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UtilityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImportFromExcelToDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewFromDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ContentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FunctionToolStripMenuItem, Me.TemplateToolStripMenuItem, Me.ConvertToolStripMenuItem, Me.DataEntryToolStripMenuItem, Me.UtilityToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(669, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FunctionToolStripMenuItem
        '
        Me.FunctionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewActivityLogToolStripMenuItem, Me.ChangePasswordToolStripMenuItem, Me.ResetPasswordToolStripMenuItem, Me.QuickConversionConfigurationEditorToolStripMenuItem, Me.ToolStripMenuItem1, Me.LogoutToolStripMenuItem, Me.ToolStripMenuItem2, Me.ExitToolStripMenuItem})
        Me.FunctionToolStripMenuItem.Name = "FunctionToolStripMenuItem"
        Me.FunctionToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.FunctionToolStripMenuItem.Text = "&Function"
        '
        'ViewActivityLogToolStripMenuItem
        '
        Me.ViewActivityLogToolStripMenuItem.Name = "ViewActivityLogToolStripMenuItem"
        Me.ViewActivityLogToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ViewActivityLogToolStripMenuItem.Text = "View Activity Log"
        '
        'ChangePasswordToolStripMenuItem
        '
        Me.ChangePasswordToolStripMenuItem.Name = "ChangePasswordToolStripMenuItem"
        Me.ChangePasswordToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ChangePasswordToolStripMenuItem.Text = "Change Password"
        '
        'ResetPasswordToolStripMenuItem
        '
        Me.ResetPasswordToolStripMenuItem.Name = "ResetPasswordToolStripMenuItem"
        Me.ResetPasswordToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ResetPasswordToolStripMenuItem.Text = "Reset Password"
        '
        'QuickConversionConfigurationEditorToolStripMenuItem
        '
        Me.QuickConversionConfigurationEditorToolStripMenuItem.Name = "QuickConversionConfigurationEditorToolStripMenuItem"
        Me.QuickConversionConfigurationEditorToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.QuickConversionConfigurationEditorToolStripMenuItem.Text = "Quick Conversion Configuration Editor"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(276, 6)
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(276, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'TemplateToolStripMenuItem
        '
        Me.TemplateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerateMasterTemplateToolStripMenuItem, Me.GenerateMasterToolStripMenuItem, Me.ToolStripMenuItem5, Me.CommonTransactionTemplateToolStripMenuItem, Me.ToolStripMenuItem6, Me.GenerateSwiftToolStripMenuItem, Me.ToolStripMenuItem4, Me.UnstructuredFileConverterToolStripMenuItem})
        Me.TemplateToolStripMenuItem.Name = "TemplateToolStripMenuItem"
        Me.TemplateToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.TemplateToolStripMenuItem.Text = "&Template"
        '
        'GenerateMasterTemplateToolStripMenuItem
        '
        Me.GenerateMasterTemplateToolStripMenuItem.Name = "GenerateMasterTemplateToolStripMenuItem"
        Me.GenerateMasterTemplateToolStripMenuItem.Size = New System.Drawing.Size(340, 22)
        Me.GenerateMasterTemplateToolStripMenuItem.Text = "Generate Master Template"
        '
        'GenerateMasterToolStripMenuItem
        '
        Me.GenerateMasterToolStripMenuItem.Name = "GenerateMasterToolStripMenuItem"
        Me.GenerateMasterToolStripMenuItem.Size = New System.Drawing.Size(340, 22)
        Me.GenerateMasterToolStripMenuItem.Text = "Generate Master Template - Fixed Width"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(337, 6)
        '
        'CommonTransactionTemplateToolStripMenuItem
        '
        Me.CommonTransactionTemplateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TextToolStripMenuItem, Me.UnstructureTextToolStripMenuItem, Me.ExcelToolStripMenuItem})
        Me.CommonTransactionTemplateToolStripMenuItem.Name = "CommonTransactionTemplateToolStripMenuItem"
        Me.CommonTransactionTemplateToolStripMenuItem.Size = New System.Drawing.Size(340, 22)
        Me.CommonTransactionTemplateToolStripMenuItem.Text = "Generate Common Transaction Template"
        '
        'TextToolStripMenuItem
        '
        Me.TextToolStripMenuItem.Name = "TextToolStripMenuItem"
        Me.TextToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.TextToolStripMenuItem.Text = "Text"
        '
        'UnstructureTextToolStripMenuItem
        '
        Me.UnstructureTextToolStripMenuItem.Name = "UnstructureTextToolStripMenuItem"
        Me.UnstructureTextToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.UnstructureTextToolStripMenuItem.Text = "Unstructure Text"
        '
        'ExcelToolStripMenuItem
        '
        Me.ExcelToolStripMenuItem.Name = "ExcelToolStripMenuItem"
        Me.ExcelToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.ExcelToolStripMenuItem.Text = "Excel"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(337, 6)
        '
        'GenerateSwiftToolStripMenuItem
        '
        Me.GenerateSwiftToolStripMenuItem.Name = "GenerateSwiftToolStripMenuItem"
        Me.GenerateSwiftToolStripMenuItem.Size = New System.Drawing.Size(340, 22)
        Me.GenerateSwiftToolStripMenuItem.Text = "Generate Swift Transaction Template"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(337, 6)
        '
        'UnstructuredFileConverterToolStripMenuItem
        '
        Me.UnstructuredFileConverterToolStripMenuItem.Name = "UnstructuredFileConverterToolStripMenuItem"
        Me.UnstructuredFileConverterToolStripMenuItem.Size = New System.Drawing.Size(340, 22)
        Me.UnstructuredFileConverterToolStripMenuItem.Text = "Create Definition File (Unstructured File Converter)"
        '
        'ConvertToolStripMenuItem
        '
        Me.ConvertToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommonTransactionTemplateToolStripMenuItem1, Me.SWIFTTransactionTemplateToolStripMenuItem})
        Me.ConvertToolStripMenuItem.Name = "ConvertToolStripMenuItem"
        Me.ConvertToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ConvertToolStripMenuItem.Text = "Convert"
        '
        'CommonTransactionTemplateToolStripMenuItem1
        '
        Me.CommonTransactionTemplateToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TextToolStripMenuItem1, Me.ExcelToolStripMenuItem1})
        Me.CommonTransactionTemplateToolStripMenuItem1.Name = "CommonTransactionTemplateToolStripMenuItem1"
        Me.CommonTransactionTemplateToolStripMenuItem1.Size = New System.Drawing.Size(243, 22)
        Me.CommonTransactionTemplateToolStripMenuItem1.Text = "Common Transaction Template"
        '
        'TextToolStripMenuItem1
        '
        Me.TextToolStripMenuItem1.Name = "TextToolStripMenuItem1"
        Me.TextToolStripMenuItem1.Size = New System.Drawing.Size(100, 22)
        Me.TextToolStripMenuItem1.Text = "Text"
        '
        'ExcelToolStripMenuItem1
        '
        Me.ExcelToolStripMenuItem1.Name = "ExcelToolStripMenuItem1"
        Me.ExcelToolStripMenuItem1.Size = New System.Drawing.Size(100, 22)
        Me.ExcelToolStripMenuItem1.Text = "Excel"
        '
        'SWIFTTransactionTemplateToolStripMenuItem
        '
        Me.SWIFTTransactionTemplateToolStripMenuItem.Name = "SWIFTTransactionTemplateToolStripMenuItem"
        Me.SWIFTTransactionTemplateToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.SWIFTTransactionTemplateToolStripMenuItem.Text = "SWIFT Transaction Template"
        '
        'DataEntryToolStripMenuItem
        '
        Me.DataEntryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GCMCMTToolStripMenuItem})
        Me.DataEntryToolStripMenuItem.Name = "DataEntryToolStripMenuItem"
        Me.DataEntryToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.DataEntryToolStripMenuItem.Text = "Data Entry"
        '
        'GCMCMTToolStripMenuItem
        '
        Me.GCMCMTToolStripMenuItem.Name = "GCMCMTToolStripMenuItem"
        Me.GCMCMTToolStripMenuItem.Size = New System.Drawing.Size(193, 22)
        Me.GCMCMTToolStripMenuItem.Text = "GCMS Money Transfer"
        '
        'UtilityToolStripMenuItem
        '
        Me.UtilityToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImportFromExcelToDBToolStripMenuItem, Me.ViewFromDBToolStripMenuItem})
        Me.UtilityToolStripMenuItem.Name = "UtilityToolStripMenuItem"
        Me.UtilityToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.UtilityToolStripMenuItem.Text = "Utility"
        '
        'ImportFromExcelToDBToolStripMenuItem
        '
        Me.ImportFromExcelToDBToolStripMenuItem.Name = "ImportFromExcelToDBToolStripMenuItem"
        Me.ImportFromExcelToDBToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.ImportFromExcelToDBToolStripMenuItem.Text = "Import from Excel to DB"
        '
        'ViewFromDBToolStripMenuItem
        '
        Me.ViewFromDBToolStripMenuItem.Name = "ViewFromDBToolStripMenuItem"
        Me.ViewFromDBToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.ViewFromDBToolStripMenuItem.Text = "View from DB"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContentToolStripMenuItem, Me.ToolStripMenuItem3, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'ContentToolStripMenuItem
        '
        Me.ContentToolStripMenuItem.Name = "ContentToolStripMenuItem"
        Me.ContentToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.ContentToolStripMenuItem.Text = "Contents"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(119, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(669, 409)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "COMSUITE Conversion Tool"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FunctionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TemplateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerateMasterTemplateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommonTransactionTemplateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangePasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ViewActivityLogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerateMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnstructureTextToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerateSwiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GCMCMTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuickConversionConfigurationEditorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UtilityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportFromExcelToDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnstructuredFileConverterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConvertToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CommonTransactionTemplateToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcelToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SWIFTTransactionTemplateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ViewFromDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
