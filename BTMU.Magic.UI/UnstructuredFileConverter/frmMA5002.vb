Public Class frmMA5002
    Private _unstructuredFileConverter As New BTMU.MAGIC.UnstructuredFileConverter.UnstructuredFileConverter
    Private _defniationfilename As String
    Private _sourcefilename As String
    Private _UserID As String
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Public Property DefinitionFile() As String
        Get
            Return _defniationfilename
        End Get
        Set(ByVal value As String)
            _defniationfilename = value
        End Set
    End Property
    Public Property SourceFile() As String
        Get
            Return _sourcefilename
        End Get
        Set(ByVal value As String)
            _sourcefilename = value
        End Set
    End Property
 
    Private Function GetColumnNo(ByVal colname As String) As Integer
        Dim colNo As Integer = 0
        Dim colHeaderName As String = ""
        For d As Integer = 0 To OutputDataGirdView.Columns.Count - 1
            colHeaderName = OutputDataGirdView.Columns(d).HeaderText
            If colHeaderName = colname Then
                colNo = d
                Exit For
            End If
        Next
        Return colNo
    End Function
    Public lblConvertMessage As Label
    Private Sub frmMA5002_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim errlog As String = ""
        Dim dset As DataSet
        Dim errlogArr As String()
        Dim convertedDataTable As New DataTable
        Dim errlogDataArr As String()
        Dim tmpString As String = ""
        Dim lineno As String = ""
        Dim colName As String = ""
        Dim errlogtext As String = ""
        Dim colNoPosition As Integer = 0
        Dim errLineCount As Integer = 0
        Dim colNo As Integer = 0
        Dim rowno As Integer = 0

        dset = New DataSet
        dset.Tables.Add(convertedDataTable)
        OutputDataGirdView.DataSource = dset.Tables(0)
        TotalRecordsTextBox.Text = ""
        ErrorRecordTextBox.Text = ""
        Try

            btnClose.Visible = False
            convertedDataTable = _unstructuredFileConverter.Preview(_defniationfilename, _sourcefilename, errlog)
            OutputDataGirdView.DataSource = convertedDataTable
            If convertedDataTable.Rows.Count > 0 Then
                OutputDataGirdView.Columns(0).Width = 50
                OutputDataGirdView.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                OutputDataGirdView.Refresh()
            End If
            'No Sortable 
            For Each column As DataGridViewColumn In OutputDataGirdView.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next


            TotalRecordsTextBox.Text = convertedDataTable.Rows.Count
            If errlog.Trim = "" Then
                ErrogLogTextBox.Text = ""
                ErrorRecordTextBox.Text = "0"
                OutputDataGirdView.Focus()
                lblConvertMessage.Visible = False
                btnClose.Visible = True
                Exit Sub
            End If
            ErrogLogTextBox.Text = errlog
            errlogArr = Split(errlog, Chr(10))
            ErrorRecordTextBox.Text = errlogArr.Length - 1

            'Show fill color in datagrid
            ReDim errlogDataArr(0 To errlogArr.Length - 1)
            If errlogDataArr.Length > 0 Then
                For i As Integer = 0 To errlogDataArr.Length - 1
                    errlogDataArr(i) = ""
                Next
            End If
            errLineCount = -1
            For x As Integer = 0 To errlogArr.Length - 1
                errlogtext = errlogArr(x)
                tmpString = Mid(errlogtext, 1, 4)
                If tmpString = "Line" Then
                    lineno = ""
                    colNoPosition = 0
                    For k As Integer = 5 To errlogtext.Length - 1
                        If errlogtext(k) = " " Then
                            lineno = lineno + ","
                            colNoPosition = k + 1
                            Exit For
                        Else
                            lineno = lineno + errlogtext(k)
                        End If
                    Next
                    colNoPosition = colNoPosition + 1
                    tmpString = Mid(errlogtext, colNoPosition, 6)
                    colNoPosition = colNoPosition + 6
                    If tmpString = "Column" Then
                        colName = ""
                        For kk As Integer = colNoPosition + 1 To errlogtext.Length - 1
                            If errlogtext(kk) = "'" Then
                                Exit For
                            Else
                                colName = colName + errlogtext(kk)
                            End If
                        Next
                    End If
                    errLineCount = errLineCount + 1
                    errlogDataArr(errLineCount) = lineno + colName
                End If
            Next
            For x As Integer = 0 To errlogDataArr.Length - 1
                tmpString = errlogDataArr(x)
                If tmpString.Trim <> "" Then
                    errlogArr = Split(tmpString, ",")
                    lineno = errlogArr(0)
                    rowno = CType(lineno, Integer) - 1
                    colName = errlogArr(1)
                    colNo = GetColumnNo(colName)
                    Try
                        OutputDataGirdView.Rows(rowno).DefaultCellStyle.BackColor = Color.LightCyan
                        OutputDataGirdView.Rows(rowno).Cells(colNo).Style.BackColor = Color.Lime
                    Catch ex As Exception
                    End Try

                End If
            Next
            OutputDataGirdView.Focus()
            lblConvertMessage.Visible = False
            btnClose.Visible = True
        Catch ex As Exception
            UnstructureFileConverterCommonFunc.ShowErrorMsg("Can't convert data.", ex.Message)
            lblConvertMessage.Visible = False
            btnClose.Visible = True
        End Try
    End Sub
End Class