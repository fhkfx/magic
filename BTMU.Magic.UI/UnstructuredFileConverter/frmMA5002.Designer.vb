<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMA5002
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OutputDataGirdView = New System.Windows.Forms.DataGridView
        Me.btnClose = New System.Windows.Forms.Button
        Me.ErrogLogTextBox = New System.Windows.Forms.TextBox
        Me.OutputGroupBox = New System.Windows.Forms.GroupBox
        Me.ValidationErrorsGroupBox = New System.Windows.Forms.GroupBox
        Me.TotalRecordsLabel = New System.Windows.Forms.Label
        Me.TotalRecordsTextBox = New System.Windows.Forms.TextBox
        Me.ErrorRecordTextBox = New System.Windows.Forms.TextBox
        Me.ErrorLabel = New System.Windows.Forms.Label
        CType(Me.OutputDataGirdView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OutputGroupBox.SuspendLayout()
        Me.ValidationErrorsGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'OutputDataGirdView
        '
        Me.OutputDataGirdView.AllowUserToAddRows = False
        Me.OutputDataGirdView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OutputDataGirdView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.OutputDataGirdView.Location = New System.Drawing.Point(6, 20)
        Me.OutputDataGirdView.Name = "OutputDataGirdView"
        Me.OutputDataGirdView.ReadOnly = True
        Me.OutputDataGirdView.RowHeadersWidth = 4
        Me.OutputDataGirdView.Size = New System.Drawing.Size(765, 314)
        Me.OutputDataGirdView.TabIndex = 8
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(702, 514)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 40)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ErrogLogTextBox
        '
        Me.ErrogLogTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ErrogLogTextBox.Location = New System.Drawing.Point(6, 20)
        Me.ErrogLogTextBox.Multiline = True
        Me.ErrogLogTextBox.Name = "ErrogLogTextBox"
        Me.ErrogLogTextBox.ReadOnly = True
        Me.ErrogLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.ErrogLogTextBox.Size = New System.Drawing.Size(765, 130)
        Me.ErrogLogTextBox.TabIndex = 10
        '
        'OutputGroupBox
        '
        Me.OutputGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OutputGroupBox.Controls.Add(Me.OutputDataGirdView)
        Me.OutputGroupBox.Location = New System.Drawing.Point(12, 3)
        Me.OutputGroupBox.Name = "OutputGroupBox"
        Me.OutputGroupBox.Size = New System.Drawing.Size(777, 341)
        Me.OutputGroupBox.TabIndex = 11
        Me.OutputGroupBox.TabStop = False
        Me.OutputGroupBox.Text = "Output"
        '
        'ValidationErrorsGroupBox
        '
        Me.ValidationErrorsGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ValidationErrorsGroupBox.Controls.Add(Me.ErrogLogTextBox)
        Me.ValidationErrorsGroupBox.Location = New System.Drawing.Point(12, 351)
        Me.ValidationErrorsGroupBox.Name = "ValidationErrorsGroupBox"
        Me.ValidationErrorsGroupBox.Size = New System.Drawing.Size(777, 157)
        Me.ValidationErrorsGroupBox.TabIndex = 12
        Me.ValidationErrorsGroupBox.TabStop = False
        Me.ValidationErrorsGroupBox.Text = "Validation Errors"
        '
        'TotalRecordsLabel
        '
        Me.TotalRecordsLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TotalRecordsLabel.AutoSize = True
        Me.TotalRecordsLabel.Location = New System.Drawing.Point(12, 527)
        Me.TotalRecordsLabel.Name = "TotalRecordsLabel"
        Me.TotalRecordsLabel.Size = New System.Drawing.Size(77, 14)
        Me.TotalRecordsLabel.TabIndex = 13
        Me.TotalRecordsLabel.Text = "Total Records:"
        '
        'TotalRecordsTextBox
        '
        Me.TotalRecordsTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TotalRecordsTextBox.Location = New System.Drawing.Point(88, 524)
        Me.TotalRecordsTextBox.Name = "TotalRecordsTextBox"
        Me.TotalRecordsTextBox.ReadOnly = True
        Me.TotalRecordsTextBox.Size = New System.Drawing.Size(81, 20)
        Me.TotalRecordsTextBox.TabIndex = 14
        Me.TotalRecordsTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ErrorRecordTextBox
        '
        Me.ErrorRecordTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ErrorRecordTextBox.Location = New System.Drawing.Point(239, 524)
        Me.ErrorRecordTextBox.Name = "ErrorRecordTextBox"
        Me.ErrorRecordTextBox.ReadOnly = True
        Me.ErrorRecordTextBox.Size = New System.Drawing.Size(81, 20)
        Me.ErrorRecordTextBox.TabIndex = 16
        Me.ErrorRecordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ErrorLabel
        '
        Me.ErrorLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ErrorLabel.AutoSize = True
        Me.ErrorLabel.Location = New System.Drawing.Point(200, 527)
        Me.ErrorLabel.Name = "ErrorLabel"
        Me.ErrorLabel.Size = New System.Drawing.Size(40, 14)
        Me.ErrorLabel.TabIndex = 15
        Me.ErrorLabel.Text = "Errors:"
        '
        'frmMA5002
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(801, 557)
        Me.Controls.Add(Me.ErrorRecordTextBox)
        Me.Controls.Add(Me.ErrorLabel)
        Me.Controls.Add(Me.TotalRecordsTextBox)
        Me.Controls.Add(Me.TotalRecordsLabel)
        Me.Controls.Add(Me.ValidationErrorsGroupBox)
        Me.Controls.Add(Me.OutputGroupBox)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmMA5002"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BTMU-MAGIC  - Unstructured File Converter (MA5002)"
        CType(Me.OutputDataGirdView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OutputGroupBox.ResumeLayout(False)
        Me.ValidationErrorsGroupBox.ResumeLayout(False)
        Me.ValidationErrorsGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OutputDataGirdView As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents ErrogLogTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OutputGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ValidationErrorsGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents TotalRecordsLabel As System.Windows.Forms.Label
    Friend WithEvents TotalRecordsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ErrorRecordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ErrorLabel As System.Windows.Forms.Label
End Class
