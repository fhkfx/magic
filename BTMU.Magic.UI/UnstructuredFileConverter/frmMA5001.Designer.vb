<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMA5001
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.SourceFileTypeComboBox = New System.Windows.Forms.ComboBox
        Me.SourceFileTypeLabel = New System.Windows.Forms.Label
        Me.SourceFileTextBox = New System.Windows.Forms.TextBox
        Me.SourceFileLabel = New System.Windows.Forms.Label
        Me.DefinitionDescriptionTextBox = New System.Windows.Forms.TextBox
        Me.btnSourceFile = New System.Windows.Forms.Button
        Me.DefinitionDescriptionLabel = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.SourceOpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SourceFileTypeComboBox)
        Me.GroupBox1.Controls.Add(Me.SourceFileTypeLabel)
        Me.GroupBox1.Controls.Add(Me.SourceFileTextBox)
        Me.GroupBox1.Controls.Add(Me.SourceFileLabel)
        Me.GroupBox1.Controls.Add(Me.DefinitionDescriptionTextBox)
        Me.GroupBox1.Controls.Add(Me.btnSourceFile)
        Me.GroupBox1.Controls.Add(Me.DefinitionDescriptionLabel)
        Me.GroupBox1.Controls.Add(Me.btnCancel)
        Me.GroupBox1.Controls.Add(Me.btnNext)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(832, 253)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "New Definition Setting"
        '
        'SourceFileTypeComboBox
        '
        Me.SourceFileTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SourceFileTypeComboBox.FormattingEnabled = True
        Me.SourceFileTypeComboBox.Items.AddRange(New Object() {"Excel File", "Delimited File", "Fixed Width File"})
        Me.SourceFileTypeComboBox.Location = New System.Drawing.Point(144, 141)
        Me.SourceFileTypeComboBox.Name = "SourceFileTypeComboBox"
        Me.SourceFileTypeComboBox.Size = New System.Drawing.Size(184, 22)
        Me.SourceFileTypeComboBox.TabIndex = 12
        '
        'SourceFileTypeLabel
        '
        Me.SourceFileTypeLabel.AutoSize = True
        Me.SourceFileTypeLabel.Location = New System.Drawing.Point(29, 144)
        Me.SourceFileTypeLabel.Name = "SourceFileTypeLabel"
        Me.SourceFileTypeLabel.Size = New System.Drawing.Size(91, 14)
        Me.SourceFileTypeLabel.TabIndex = 13
        Me.SourceFileTypeLabel.Text = "Source File Type:"
        '
        'SourceFileTextBox
        '
        Me.SourceFileTextBox.AllowDrop = True
        Me.SourceFileTextBox.Location = New System.Drawing.Point(144, 30)
        Me.SourceFileTextBox.MaxLength = 100
        Me.SourceFileTextBox.Name = "SourceFileTextBox"
        Me.SourceFileTextBox.Size = New System.Drawing.Size(580, 20)
        Me.SourceFileTextBox.TabIndex = 8
        '
        'SourceFileLabel
        '
        Me.SourceFileLabel.AutoSize = True
        Me.SourceFileLabel.Location = New System.Drawing.Point(29, 33)
        Me.SourceFileLabel.Name = "SourceFileLabel"
        Me.SourceFileLabel.Size = New System.Drawing.Size(64, 14)
        Me.SourceFileLabel.TabIndex = 14
        Me.SourceFileLabel.Text = "Source File:"
        '
        'DefinitionDescriptionTextBox
        '
        Me.DefinitionDescriptionTextBox.Location = New System.Drawing.Point(144, 58)
        Me.DefinitionDescriptionTextBox.MaxLength = 500
        Me.DefinitionDescriptionTextBox.Multiline = True
        Me.DefinitionDescriptionTextBox.Name = "DefinitionDescriptionTextBox"
        Me.DefinitionDescriptionTextBox.Size = New System.Drawing.Size(580, 76)
        Me.DefinitionDescriptionTextBox.TabIndex = 10
        '
        'btnSourceFile
        '
        Me.btnSourceFile.Location = New System.Drawing.Point(730, 26)
        Me.btnSourceFile.Name = "btnSourceFile"
        Me.btnSourceFile.Size = New System.Drawing.Size(87, 29)
        Me.btnSourceFile.TabIndex = 9
        Me.btnSourceFile.Text = "Browse"
        Me.btnSourceFile.UseVisualStyleBackColor = True
        '
        'DefinitionDescriptionLabel
        '
        Me.DefinitionDescriptionLabel.AutoSize = True
        Me.DefinitionDescriptionLabel.Location = New System.Drawing.Point(29, 61)
        Me.DefinitionDescriptionLabel.Name = "DefinitionDescriptionLabel"
        Me.DefinitionDescriptionLabel.Size = New System.Drawing.Size(111, 14)
        Me.DefinitionDescriptionLabel.TabIndex = 11
        Me.DefinitionDescriptionLabel.Text = "Definition Description:"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(130, 201)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 40)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Enabled = False
        Me.btnNext.Location = New System.Drawing.Point(33, 201)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(87, 40)
        Me.btnNext.TabIndex = 4
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'SourceOpenFileDialog
        '
        Me.SourceOpenFileDialog.Filter = "All file (*.*)|*.*"
        Me.SourceOpenFileDialog.InitialDirectory = "C:\Program Files\BTMU MAGIC 2\Sample Source\"
        Me.SourceOpenFileDialog.Title = "Source File"
        '
        'frmMA5001
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 272)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmMA5001"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BTMU-MAGIC - Unstructured File Converter (MA5001)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents SourceOpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SourceFileTypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SourceFileTypeLabel As System.Windows.Forms.Label
    Friend WithEvents SourceFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SourceFileLabel As System.Windows.Forms.Label
    Friend WithEvents DefinitionDescriptionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnSourceFile As System.Windows.Forms.Button
    Friend WithEvents DefinitionDescriptionLabel As System.Windows.Forms.Label
End Class
