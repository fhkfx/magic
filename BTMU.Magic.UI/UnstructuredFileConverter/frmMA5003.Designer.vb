<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMA5003
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Identifiers")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Columns")
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMA5003))
        Me.DefinationGroupBox = New System.Windows.Forms.GroupBox
        Me.btnDown = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.DefinitionTreeView = New System.Windows.Forms.TreeView
        Me.TreeViewImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.PropertyGroupBox = New System.Windows.Forms.GroupBox
        Me.ucColumnsProperty = New BTMU.Magic.UI.ucColumnsProperty
        Me.ucIdentifierProperty = New BTMU.Magic.UI.ucIdentifierProperty
        Me.LegendsGroupBox = New System.Windows.Forms.GroupBox
        Me.HideTypeLegendsPanel = New System.Windows.Forms.Panel
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCopy = New System.Windows.Forms.Button
        Me.btnNewColumn = New System.Windows.Forms.Button
        Me.btnNewIdentifier = New System.Windows.Forms.Button
        Me.btnPriview = New System.Windows.Forms.Button
        Me.SourceFileGroupBox = New System.Windows.Forms.GroupBox
        Me.ucGridEditor = New BTMU.Magic.UI.ucGridEditor
        Me.ucTextEditor = New BTMU.Magic.UI.ucTextEditor
        Me.lblConvertMessage = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.DefinitionSaveFileDialog = New System.Windows.Forms.SaveFileDialog
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.pnlBody = New System.Windows.Forms.Panel
        Me.DefinationGroupBox.SuspendLayout()
        Me.PropertyGroupBox.SuspendLayout()
        Me.LegendsGroupBox.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SourceFileGroupBox.SuspendLayout()
        Me.pnlTop.SuspendLayout()
        Me.pnlBottom.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefinationGroupBox
        '
        Me.DefinationGroupBox.Controls.Add(Me.btnDown)
        Me.DefinationGroupBox.Controls.Add(Me.btnUp)
        Me.DefinationGroupBox.Controls.Add(Me.DefinitionTreeView)
        Me.DefinationGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DefinationGroupBox.ForeColor = System.Drawing.Color.Navy
        Me.DefinationGroupBox.Location = New System.Drawing.Point(6, 4)
        Me.DefinationGroupBox.Name = "DefinationGroupBox"
        Me.DefinationGroupBox.Size = New System.Drawing.Size(272, 284)
        Me.DefinationGroupBox.TabIndex = 0
        Me.DefinationGroupBox.TabStop = False
        Me.DefinationGroupBox.Text = "Definitions"
        '
        'btnDown
        '
        Me.btnDown.Enabled = False
        Me.btnDown.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnDown.Location = New System.Drawing.Point(30, 17)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(25, 25)
        Me.btnDown.TabIndex = 8
        Me.btnDown.Text = "▼"
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Enabled = False
        Me.btnUp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnUp.Location = New System.Drawing.Point(5, 17)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(25, 25)
        Me.btnUp.TabIndex = 7
        Me.btnUp.Text = "▲"
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'DefinitionTreeView
        '
        Me.DefinitionTreeView.BackColor = System.Drawing.Color.White
        Me.DefinitionTreeView.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DefinitionTreeView.HideSelection = False
        Me.DefinitionTreeView.ImageIndex = 0
        Me.DefinitionTreeView.ImageList = Me.TreeViewImageList
        Me.DefinitionTreeView.Location = New System.Drawing.Point(6, 43)
        Me.DefinitionTreeView.Name = "DefinitionTreeView"
        TreeNode1.Name = "IdentifiersRootNode"
        TreeNode1.NodeFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TreeNode1.Text = "Identifiers"
        TreeNode2.Name = "ColumnsRootNode"
        TreeNode2.NodeFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TreeNode2.Text = "Columns"
        Me.DefinitionTreeView.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2})
        Me.DefinitionTreeView.SelectedImageIndex = 2
        Me.DefinitionTreeView.Size = New System.Drawing.Size(260, 235)
        Me.DefinitionTreeView.StateImageList = Me.TreeViewImageList
        Me.DefinitionTreeView.TabIndex = 6
        '
        'TreeViewImageList
        '
        Me.TreeViewImageList.ImageStream = CType(resources.GetObject("TreeViewImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TreeViewImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.TreeViewImageList.Images.SetKeyName(0, "Root Icon.gif")
        Me.TreeViewImageList.Images.SetKeyName(1, "PrintPreviewHS.png")
        Me.TreeViewImageList.Images.SetKeyName(2, "blueTick.jpg")
        Me.TreeViewImageList.Images.SetKeyName(3, "openfolderHS.png")
        '
        'PropertyGroupBox
        '
        Me.PropertyGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PropertyGroupBox.Controls.Add(Me.ucColumnsProperty)
        Me.PropertyGroupBox.Controls.Add(Me.ucIdentifierProperty)
        Me.PropertyGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PropertyGroupBox.ForeColor = System.Drawing.Color.Navy
        Me.PropertyGroupBox.Location = New System.Drawing.Point(288, 4)
        Me.PropertyGroupBox.Name = "PropertyGroupBox"
        Me.PropertyGroupBox.Size = New System.Drawing.Size(446, 284)
        Me.PropertyGroupBox.TabIndex = 1
        Me.PropertyGroupBox.TabStop = False
        Me.PropertyGroupBox.Text = "Property"
        '
        'ucColumnsProperty
        '
        Me.ucColumnsProperty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucColumnsProperty.column = CType(resources.GetObject("ucColumnsProperty.column"), BTMU.Magic.UnstructuredFileConverter.Column)
        Me.ucColumnsProperty.fileType = Nothing
        Me.ucColumnsProperty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucColumnsProperty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ucColumnsProperty.Location = New System.Drawing.Point(13, 15)
        Me.ucColumnsProperty.Name = "ucColumnsProperty"
        Me.ucColumnsProperty.OriginalColumnName = Nothing
        Me.ucColumnsProperty.OriginalIdNo = Nothing
        Me.ucColumnsProperty.Size = New System.Drawing.Size(427, 263)
        Me.ucColumnsProperty.TabIndex = 0
        Me.ucColumnsProperty.Visible = False
        '
        'ucIdentifierProperty
        '
        Me.ucIdentifierProperty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ucIdentifierProperty.fileType = Nothing
        Me.ucIdentifierProperty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucIdentifierProperty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ucIdentifierProperty.identifier = CType(resources.GetObject("ucIdentifierProperty.identifier"), BTMU.Magic.UnstructuredFileConverter.Identifier)
        Me.ucIdentifierProperty.Location = New System.Drawing.Point(10, 14)
        Me.ucIdentifierProperty.Name = "ucIdentifierProperty"
        Me.ucIdentifierProperty.OriginalIDNo = Nothing
        Me.ucIdentifierProperty.Size = New System.Drawing.Size(427, 264)
        Me.ucIdentifierProperty.TabIndex = 1
        Me.ucIdentifierProperty.Visible = False
        '
        'LegendsGroupBox
        '
        Me.LegendsGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LegendsGroupBox.Controls.Add(Me.HideTypeLegendsPanel)
        Me.LegendsGroupBox.Controls.Add(Me.Label18)
        Me.LegendsGroupBox.Controls.Add(Me.Label17)
        Me.LegendsGroupBox.Controls.Add(Me.Label16)
        Me.LegendsGroupBox.Controls.Add(Me.Label15)
        Me.LegendsGroupBox.Controls.Add(Me.Label14)
        Me.LegendsGroupBox.Controls.Add(Me.Label13)
        Me.LegendsGroupBox.Controls.Add(Me.Label12)
        Me.LegendsGroupBox.Controls.Add(Me.Label11)
        Me.LegendsGroupBox.Controls.Add(Me.Label10)
        Me.LegendsGroupBox.Controls.Add(Me.Label9)
        Me.LegendsGroupBox.Controls.Add(Me.Label8)
        Me.LegendsGroupBox.Controls.Add(Me.Label7)
        Me.LegendsGroupBox.Controls.Add(Me.Label6)
        Me.LegendsGroupBox.Controls.Add(Me.Label5)
        Me.LegendsGroupBox.Controls.Add(Me.Label4)
        Me.LegendsGroupBox.Controls.Add(Me.Label3)
        Me.LegendsGroupBox.Controls.Add(Me.Label2)
        Me.LegendsGroupBox.Controls.Add(Me.Label1)
        Me.LegendsGroupBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LegendsGroupBox.ForeColor = System.Drawing.Color.Navy
        Me.LegendsGroupBox.Location = New System.Drawing.Point(749, 3)
        Me.LegendsGroupBox.Name = "LegendsGroupBox"
        Me.LegendsGroupBox.Size = New System.Drawing.Size(238, 284)
        Me.LegendsGroupBox.TabIndex = 2
        Me.LegendsGroupBox.TabStop = False
        Me.LegendsGroupBox.Text = "Identifier Type Legends"
        '
        'HideTypeLegendsPanel
        '
        Me.HideTypeLegendsPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HideTypeLegendsPanel.Location = New System.Drawing.Point(11, 148)
        Me.HideTypeLegendsPanel.Name = "HideTypeLegendsPanel"
        Me.HideTypeLegendsPanel.Size = New System.Drawing.Size(222, 115)
        Me.HideTypeLegendsPanel.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(95, 190)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(138, 26)
        Me.Label18.TabIndex = 17
        Me.Label18.Text = "Matches line n repeat every" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "m lines"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(95, 169)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(105, 13)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Matches a blank line"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(95, 148)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(138, 13)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Matches a page break char"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(95, 127)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(102, 14)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Matches same char"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(95, 106)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(70, 14)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Matches digit"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(95, 85)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(121, 14)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Matches alpha A-Z, a-z"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(95, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(94, 14)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Matches any char"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(95, 43)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(102, 14)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Matches non space"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(95, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 14)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Matches space"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(28, 190)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "<LINE n,m>"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(8, 169)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "<EMPTY LINE>"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Navy
        Me.Label7.Location = New System.Drawing.Point(14, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "<NEW PAGE>"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(58, 127)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 14)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Other"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(77, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 14)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "#"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(73, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 14)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "@"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Navy
        Me.Label3.Location = New System.Drawing.Point(53, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Space"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(78, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "?"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(78, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(13, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "_"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.btnDelete)
        Me.GroupBox3.Controls.Add(Me.btnCopy)
        Me.GroupBox3.Controls.Add(Me.btnNewColumn)
        Me.GroupBox3.Controls.Add(Me.btnNewIdentifier)
        Me.GroupBox3.Controls.Add(Me.btnPriview)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 291)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(981, 49)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'btnDelete
        '
        Me.btnDelete.Enabled = False
        Me.btnDelete.Location = New System.Drawing.Point(476, 9)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(87, 37)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCopy
        '
        Me.btnCopy.Enabled = False
        Me.btnCopy.Location = New System.Drawing.Point(379, 9)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(87, 37)
        Me.btnCopy.TabIndex = 3
        Me.btnCopy.Text = "Copy"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'btnNewColumn
        '
        Me.btnNewColumn.Location = New System.Drawing.Point(282, 9)
        Me.btnNewColumn.Name = "btnNewColumn"
        Me.btnNewColumn.Size = New System.Drawing.Size(87, 37)
        Me.btnNewColumn.TabIndex = 2
        Me.btnNewColumn.Text = "New Column"
        Me.btnNewColumn.UseVisualStyleBackColor = True
        '
        'btnNewIdentifier
        '
        Me.btnNewIdentifier.Location = New System.Drawing.Point(185, 9)
        Me.btnNewIdentifier.Name = "btnNewIdentifier"
        Me.btnNewIdentifier.Size = New System.Drawing.Size(87, 37)
        Me.btnNewIdentifier.TabIndex = 1
        Me.btnNewIdentifier.Text = "New Identifier"
        Me.btnNewIdentifier.UseVisualStyleBackColor = True
        '
        'btnPriview
        '
        Me.btnPriview.Location = New System.Drawing.Point(88, 9)
        Me.btnPriview.Name = "btnPriview"
        Me.btnPriview.Size = New System.Drawing.Size(87, 37)
        Me.btnPriview.TabIndex = 0
        Me.btnPriview.Text = "Preview"
        Me.btnPriview.UseVisualStyleBackColor = True
        '
        'SourceFileGroupBox
        '
        Me.SourceFileGroupBox.Controls.Add(Me.ucTextEditor)
        Me.SourceFileGroupBox.Controls.Add(Me.ucGridEditor)
        Me.SourceFileGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SourceFileGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SourceFileGroupBox.ForeColor = System.Drawing.Color.Navy
        Me.SourceFileGroupBox.Location = New System.Drawing.Point(6, 6)
        Me.SourceFileGroupBox.Name = "SourceFileGroupBox"
        Me.SourceFileGroupBox.Padding = New System.Windows.Forms.Padding(6)
        Me.SourceFileGroupBox.Size = New System.Drawing.Size(989, 318)
        Me.SourceFileGroupBox.TabIndex = 4
        Me.SourceFileGroupBox.TabStop = False
        Me.SourceFileGroupBox.Text = "Source File"
        '
        'ucGridEditor
        '
        Me.ucGridEditor.column = CType(resources.GetObject("ucGridEditor.column"), BTMU.Magic.UnstructuredFileConverter.Column)
        Me.ucGridEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucGridEditor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucGridEditor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ucGridEditor.identifier = CType(resources.GetObject("ucGridEditor.identifier"), BTMU.Magic.UnstructuredFileConverter.Identifier)
        Me.ucGridEditor.Location = New System.Drawing.Point(6, 19)
        Me.ucGridEditor.Name = "ucGridEditor"
        Me.ucGridEditor.Size = New System.Drawing.Size(977, 293)
        Me.ucGridEditor.sourceFilePath = Nothing
        Me.ucGridEditor.TabIndex = 2
        Me.ucGridEditor.unstructuredFileConverter = Nothing
        '
        'ucTextEditor
        '
        Me.ucTextEditor.column = CType(resources.GetObject("ucTextEditor.column"), BTMU.Magic.UnstructuredFileConverter.Column)
        Me.ucTextEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucTextEditor.errorMsg = Nothing
        Me.ucTextEditor.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucTextEditor.identifier = CType(resources.GetObject("ucTextEditor.identifier"), BTMU.Magic.UnstructuredFileConverter.Identifier)
        Me.ucTextEditor.Location = New System.Drawing.Point(6, 19)
        Me.ucTextEditor.Name = "ucTextEditor"
        Me.ucTextEditor.Size = New System.Drawing.Size(977, 293)
        Me.ucTextEditor.sourceFilePath = Nothing
        Me.ucTextEditor.TabIndex = 7
        '
        'lblConvertMessage
        '
        Me.lblConvertMessage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblConvertMessage.AutoSize = True
        Me.lblConvertMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConvertMessage.ForeColor = System.Drawing.Color.Red
        Me.lblConvertMessage.Location = New System.Drawing.Point(6, 17)
        Me.lblConvertMessage.Name = "lblConvertMessage"
        Me.lblConvertMessage.Size = New System.Drawing.Size(272, 16)
        Me.lblConvertMessage.TabIndex = 2
        Me.lblConvertMessage.Text = "Converting in progress. . .  Please wait"
        Me.lblConvertMessage.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Ignore
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancel.Location = New System.Drawing.Point(905, 7)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 37)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSave.Location = New System.Drawing.Point(808, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 37)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DefinitionSaveFileDialog
        '
        Me.DefinitionSaveFileDialog.AddExtension = False
        Me.DefinitionSaveFileDialog.DefaultExt = "mdt"
        Me.DefinitionSaveFileDialog.Filter = "Definition File (*.mdt)|*.mdt"
        Me.DefinitionSaveFileDialog.InitialDirectory = "C:\Program Files\BTMU MAGIC 2\Definition\"
        '
        'pnlTop
        '
        Me.pnlTop.Controls.Add(Me.DefinationGroupBox)
        Me.pnlTop.Controls.Add(Me.PropertyGroupBox)
        Me.pnlTop.Controls.Add(Me.LegendsGroupBox)
        Me.pnlTop.Controls.Add(Me.GroupBox3)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(1001, 343)
        Me.pnlTop.TabIndex = 7
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.lblConvertMessage)
        Me.pnlBottom.Controls.Add(Me.btnCancel)
        Me.pnlBottom.Controls.Add(Me.btnSave)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 673)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(1001, 49)
        Me.pnlBottom.TabIndex = 8
        '
        'pnlBody
        '
        Me.pnlBody.Controls.Add(Me.SourceFileGroupBox)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 343)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Padding = New System.Windows.Forms.Padding(6)
        Me.pnlBody.Size = New System.Drawing.Size(1001, 330)
        Me.pnlBody.TabIndex = 9
        '
        'frmMA5003
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1001, 722)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlBottom)
        Me.Controls.Add(Me.pnlTop)
        Me.Name = "frmMA5003"
        Me.ShowIcon = False
        Me.Text = "BTMU-MAGIC - Unstructured File Converter (MA5003)"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.DefinationGroupBox.ResumeLayout(False)
        Me.PropertyGroupBox.ResumeLayout(False)
        Me.LegendsGroupBox.ResumeLayout(False)
        Me.LegendsGroupBox.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.SourceFileGroupBox.ResumeLayout(False)
        Me.pnlTop.ResumeLayout(False)
        Me.pnlBottom.ResumeLayout(False)
        Me.pnlBottom.PerformLayout()
        Me.pnlBody.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefinationGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents DefinitionTreeView As System.Windows.Forms.TreeView
    Friend WithEvents PropertyGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents LegendsGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents btnNewColumn As System.Windows.Forms.Button
    Friend WithEvents btnNewIdentifier As System.Windows.Forms.Button
    Friend WithEvents btnPriview As System.Windows.Forms.Button
    Friend WithEvents SourceFileGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents ucColumnsProperty As BTMU.Magic.UI.ucColumnsProperty
    Friend WithEvents ucIdentifierProperty As BTMU.Magic.UI.ucIdentifierProperty
    Friend WithEvents TreeViewImageList As System.Windows.Forms.ImageList
    Friend WithEvents ucTextEditor As BTMU.Magic.UI.ucTextEditor
    Friend WithEvents ucGridEditor As BTMU.Magic.UI.ucGridEditor
    Friend WithEvents HideTypeLegendsPanel As System.Windows.Forms.Panel
    Friend WithEvents DefinitionSaveFileDialog As System.Windows.Forms.SaveFileDialog
    Public WithEvents lblConvertMessage As System.Windows.Forms.Label
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents pnlBody As System.Windows.Forms.Panel
End Class
