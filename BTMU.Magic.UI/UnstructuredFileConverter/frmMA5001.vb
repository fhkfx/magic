Imports BTMU.Magic.UnstructuredFileConverter
Imports System.IO

Public Class frmMA5001
    '''MA5001 - Define source file for new definition creation
#Region "Control Event"
    Private Sub SourceFileDragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragDrop
        Try
            Dim a As Array = e.Data.GetData(DataFormats.FileDrop)
            SourceFileTextBox.Text = a.GetValue(0).ToString()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub SourceFileDragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub SourceFileTextChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SourceFileTextBox.TextChanged
        btnNext.Enabled = (SourceFileTextBox.Text.Trim <> "")
        If SourceFileTextBox.Text.Trim = "" Then
            SourceFileTypeComboBox.SelectedIndex = 2
        ElseIf SourceFileTextBox.Text.ToLower.IndexOf(".xls") <> -1 Or SourceFileTextBox.Text.ToUpper.IndexOf(".XLS") <> -1 Then
            SourceFileTypeComboBox.SelectedIndex = 0
        Else
            SourceFileTypeComboBox.SelectedIndex = 2
        End If
    End Sub
    Private Sub btnSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSourceFile.Click
        If SourceOpenFileDialog.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
            SourceFileTextBox.Text = SourceOpenFileDialog.FileName

        End If
    End Sub
    Private Sub frmMA5001_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SourceFileTextBox.Focus()
        SourceFileTypeComboBox.SelectedIndex = 2
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If ValidateSourceFile(SourceFileTextBox.Text, "Source File") = False Then Exit Sub
        'Added by Hein 20090910
        If SourceFileTypeComboBox.Text = "Excel File" Then
            If CheckExcelFormat(SourceFileTextBox.Text) = False Then
                Dim errWindow As New BTMU.Magic.Common.ErrorWindow()
                errWindow.BriefErrorMessage = "Invalid excel format or the excel file is password-protected!"
                errWindow.DetailedErrorMessage = SourceFileTextBox.Text + vbNewLine + "Invalid excel format or the excel file is password-protected!"
                errWindow.ShowDialog()
                Exit Sub
            End If
        End If
        unstructuredFileConverter = New UnstructuredFileConverter.UnstructuredFileConverter
        With unstructuredFileConverter
            Select Case SourceFileTypeComboBox.Text
                Case "Excel File"
                    .SourceFileType = "E"
                Case "Delimited File"
                    .SourceFileType = "D"
                Case "Fixed Width File"
                    .SourceFileType = "F"
            End Select
            .SourceFile = SourceFileTextBox.Text
            .DefinitionDesc = DefinitionDescriptionTextBox.Text.Replace(vbNewLine, "^")
            Dim fileInfo As FileInfo = New FileInfo(SourceFileTextBox.Text)
            Dim DefinationFilePath As String = fileInfo.DirectoryName + "\" + fileInfo.Name.Replace(fileInfo.Extension, ".mdt.new")
            Try
                System.IO.File.Delete(DefinationFilePath)
            Catch ex As Exception
            End Try
            .CreatedDate = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")
            .DefinitionFile = DefinationFilePath
            .SaveToFile(DefinationFilePath)
            Dim hiddenfileInfo As FileInfo = New FileInfo(DefinationFilePath)
            hiddenfileInfo.Attributes = hiddenfileInfo.Attributes Or IO.FileAttributes.Hidden
            DialogResult = Windows.Forms.DialogResult.OK
            hiddenfileInfo = New FileInfo("C:\")
            Me.Close()
        End With
    End Sub
#End Region

#Region "Variable"
    Private _unstructuredFileConverter As New BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter()
    Private _UserID As String
#End Region

#Region " Public Property "
    Public Property unstructuredFileConverter() As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter
        Get
            Return _unstructuredFileConverter
        End Get
        Set(ByVal value As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter)
            _unstructuredFileConverter = value
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property

#End Region

End Class