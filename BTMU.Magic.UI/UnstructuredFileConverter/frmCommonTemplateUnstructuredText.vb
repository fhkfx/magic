Imports BTMU.Magic.Common
Imports System.IO

Public Class frmCommonTemplateUnstructuredText

    Private IsValid As Boolean
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Private Sub frmCommonTemplateUnstructuredText_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        textSourceFile.Text = ""
        textDefinitionFile.Text = ""

        If Not File.Exists(My.Settings.IntermediateFile) Then File.Create(My.Settings.IntermediateFile)

        textIntermediaryFile.Text = My.Settings.IntermediateFile

        Me.Left = Convert.ToInt32((Me.MdiParent.Width - 474) / 2)
        Me.Top = Convert.ToInt32((Me.MdiParent.Height - 222) / 3)

    End Sub

    Private Function IsInSupportedFormat(ByVal e As System.Windows.Forms.DragEventArgs) As Boolean
        Dim fmt As String
        Dim supported As Boolean = False

        For Each fmt In e.Data.GetFormats()
            If fmt = System.Windows.Forms.DataFormats.FileDrop Then
                supported = True
                Exit For
            End If
        Next
        Return supported
    End Function



    Private Sub buttonBrowseSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonBrowseSourceFile.Click

        Dim SrcFileDialog As New OpenFileDialog()
        ' SrcFileDialog.Filter = "Text Documents (*.txt)|*.txt|CSV Files (*.csv)|*.csv|XL Files (*.xls)|*.xls"
        SrcFileDialog.Filter = MsgReader.GetString("E10000023")

        SrcFileDialog.Multiselect = False
        SrcFileDialog.InitialDirectory = My.Settings.SampleSourceFolder
        SrcFileDialog.CheckFileExists = True
        SrcFileDialog.CheckPathExists = True
        If SrcFileDialog.ShowDialog(Me) = DialogResult.OK Then textSourceFile.Text = SrcFileDialog.FileName
        SrcFileDialog.Dispose()

    End Sub

    Private Sub textSourceFile_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textSourceFile.DragEnter

        e.Effect = IIf(IsInSupportedFormat(e), DragDropEffects.Copy, DragDropEffects.None)

    End Sub

    Private Sub textSourceFile_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textSourceFile.DragDrop

        Dim filename As String = ""
        Dim Filenames As String()

        textSourceFile.Focus()

        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)

        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase) Or _
                filename.EndsWith(".xls", StringComparison.InvariantCultureIgnoreCase) Or _
                filename.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase)) Then Exit Sub


        textSourceFile.Text = filename

    End Sub



    Private Sub buttonBrowseDefinitionFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonBrowseDefinitionFile.Click

        Dim SrcFileDialog As New OpenFileDialog()
        'SrcFileDialog.Filter = "Magic Definition Templates (*.MDT)|*.MDT"
        SrcFileDialog.Filter = MsgReader.GetString("E10000024")
        SrcFileDialog.Multiselect = False
        SrcFileDialog.InitialDirectory = My.Settings.SampleSourceFolder
        SrcFileDialog.CheckFileExists = True
        SrcFileDialog.CheckPathExists = True
        If SrcFileDialog.ShowDialog(Me) = DialogResult.OK Then textDefinitionFile.Text = SrcFileDialog.FileName
        SrcFileDialog.Dispose()

    End Sub

    Private Sub textDefinitionFile_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textDefinitionFile.DragEnter

        e.Effect = IIf(IsInSupportedFormat(e), DragDropEffects.Copy, DragDropEffects.None)

    End Sub

    Private Sub textDefinitionFile_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textDefinitionFile.DragDrop

        Dim filename As String = ""
        Dim Filenames As String()

        textDefinitionFile.Focus()

        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)

        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".MDT", StringComparison.InvariantCultureIgnoreCase)) Then Exit Sub

        textDefinitionFile.Text = filename

    End Sub


    Private Sub buttonIntermediaryFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonIntermediaryFile.Click

        Dim SrcFileDialog As New SaveFileDialog()
        'SrcFileDialog.Filter = "Text Files (*.txt)|*.txt"
        SrcFileDialog.Filter = MsgReader.GetString("E10000025")

        SrcFileDialog.InitialDirectory = My.Settings.SampleSourceFolder

        If textIntermediaryFile.Text <> "" AndAlso File.Exists(textIntermediaryFile.Text) Then SrcFileDialog.FileName = textIntermediaryFile.Text

        SrcFileDialog.CheckPathExists = True
        If SrcFileDialog.ShowDialog(Me) = DialogResult.OK Then textIntermediaryFile.Text = SrcFileDialog.FileName

        SrcFileDialog.Dispose()

        ValidateIntermediaryFile()

    End Sub

    Private Sub textIntermediaryFile_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textIntermediaryFile.DragEnter

        e.Effect = IIf(IsInSupportedFormat(e), DragDropEffects.Copy, DragDropEffects.None)

    End Sub

    Private Sub textIntermediaryFile_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles textIntermediaryFile.DragDrop

        Dim filename As String = ""
        Dim Filenames As String()

        textIntermediaryFile.Focus()

        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)

        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase)) Then Exit Sub

        textIntermediaryFile.Text = filename

    End Sub

    Private Function ValidateIntermediaryFile() As Boolean

        'Validate Intermediary File 
        If textIntermediaryFile.Text.Trim() = String.Empty Then
            'errorPvd.SetError(textIntermediaryFile, "Please key in Intermediary Filename.")
            errorPvd.SetError(textIntermediaryFile, MsgReader.GetString("E10000026"))
            Return False
        Else
            errorPvd.SetError(textIntermediaryFile, "")
        End If

        'Validate, if Intermediary File exists
        If File.Exists(textIntermediaryFile.Text.Trim()) Then
            Dim _fileObj As New FileInfo(textIntermediaryFile.Text.Trim())
            If _fileObj.Length > 0 Then
                'errorPvd.SetError(textIntermediaryFile, "Intermediary File exists!. Please key in a new Intermediary Filename.")
                errorPvd.SetError(textIntermediaryFile, MsgReader.GetString("E10000027"))

                Return False
            Else
                errorPvd.SetError(textIntermediaryFile, "")
            End If
        Else

            errorPvd.SetError(textIntermediaryFile, "")
        End If
        Return True

    End Function

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click

        IsValid = True
        'Validate Source File
        If textSourceFile.Text.Trim() = String.Empty Then
            'errorPvd.SetError(textSourceFile, "Please select Source File.")
            errorPvd.SetError(textSourceFile, MsgReader.GetString("E10000028"))
            IsValid = False
        Else
            errorPvd.SetError(textSourceFile, "")
        End If

        'Validate Definition File 
        If textDefinitionFile.Text.Trim() = String.Empty Then
            'errorPvd.SetError(textDefinitionFile, "Please select Definition File.")
            errorPvd.SetError(textDefinitionFile, MsgReader.GetString("E10000029"))
            IsValid = False
        Else
            errorPvd.SetError(textDefinitionFile, "")
        End If

     

        IsValid = ValidateIntermediaryFile()



        If IsValid Then

            Dim objUnstructFileCnv As New UnstructuredFileConverter.UnstructuredFileConverter()
            objUnstructFileCnv.DefinitionFile = textDefinitionFile.Text.Trim()

            Try

                Dim convertedSrcFilename As String = textIntermediaryFile.Text.Trim()

                If objUnstructFileCnv.UnstructuredFileConverter(frmLogin.GsUserName, textDefinitionFile.Text, textSourceFile.Text.Trim(), convertedSrcFilename, "D", ";", "{""}") Then

                    frmCommonTemplateText.MdiParent = Me.MdiParent
                    frmCommonTemplateText.Show()
                    frmCommonTemplateText.WindowState = FormWindowState.Maximized
                    frmCommonTemplateText.ShowDetail(convertedSrcFilename, textDefinitionFile.Text)
                    Me.Visible = False

                End If
            Catch ex As IOException
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error", ex.Message)
                BTMU.Magic.Common.BTMUExceptionManager.HandleException(ex)
            Catch ex As Exception
                BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError(MsgReader.GetString("E10000031"), MsgReader.GetString("E10000030"))
                BTMU.Magic.Common.BTMUExceptionManager.HandleException(ex)
            End Try

        End If
    End Sub

    Private Sub textIntermediaryFile_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles textIntermediaryFile.Leave
        ValidateIntermediaryFile()
    End Sub
End Class