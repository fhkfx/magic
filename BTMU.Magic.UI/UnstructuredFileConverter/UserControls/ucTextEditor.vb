Imports BTMU.Magic.UnstructuredFileConverter
Public Class ucTextEditor
    Private font_height As Single
    Private LastSelectPoint As Integer
    Private vMaxLength As Integer = 300
    Dim vOriLineAry() As Integer
    Private columnColor As Color = Color.Yellow
    Private identifierColor As Color = Color.LightSteelBlue
    Private clearColor As Color
    Private vSecondEditor As New RichTextBox
    Private Sub uxTextEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rTBHide.SendToBack()
        _parentForm = Me.ParentForm()
        clearColor = UnstructureFileConverterCommonFunc.EditorBackColor
        'Added by Hein 20090812
        vSecondEditor.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        'Added by Hein 20090831
        WorkShopTextBox.AutoWordSelection = False
    End Sub

#Region "Scroll Down Up"
    Dim currentVertical As Integer = -1
    Private Sub CheckVertical(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerticalTimer.Tick

        If currentVertical = -1 Then
            currentVertical = GetScrollPos(WorkShopTextBox.Handle, SB_VERT)
        ElseIf currentVertical <> GetScrollPos(WorkShopTextBox.Handle, SB_VERT) Then
            'Dim vScStartLineIndex As Integer = GetViewFirstLine()
            'Dim vScEndLineIndex As Integer = GetViewLastLine()
            'Dim vStartLineIndex As Integer = 0
            'Dim vEndLineIndex As Integer = 0

            'If vScStartLineIndex <> 0 Then
            '    vStartLineIndex = (vScStartLineIndex - 200) + IIf((vScStartLineIndex - 200) < 0, (vScStartLineIndex - 200), 0)
            '    vEndLineIndex = (vScStartLineIndex - 200) + IIf((vScStartLineIndex - 200) < 0, (vScStartLineIndex - 200), 0)
            'End If


            'Dim sReader As IO.StreamReader = New IO.StreamReader(sourceFilePath)
            'WorkShopTextBox.SelectionBackColor = Color.White
            'saveTextBox.Text = sReader.ReadToEnd()

            'For ind As Integer = vScStartLineIndex To vScEndLineIndex
            '    WorkShopTextBox.Lines.a()
            'Next


            'MessageBox.Show("")
            If _StillSroll = True Then Exit Sub
            LineNoPictureBox.Invalidate()
            SearchPattenTextInHideView()
            RepairLineNoArray()
            LineNoPictureBox.Invalidate()
            currentVertical = GetScrollPos(WorkShopTextBox.Handle, SB_VERT)
        End If

    End Sub
#End Region
#Region "Ruler And Offset"
    Public vStillProcessInPaint As Boolean = False
    Private Sub pbLineNo_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles LineNoPictureBox.Paint
        DrawRichTextBoxLineNumbers(e.Graphics)
    End Sub
    'Private Sub DrawRichTextBoxLineNumbers(ByRef g As Graphics)
    '    font_height = WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(2)).Y - WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(1)).Y
    '    If font_height = 0 Then Exit Sub
    '    Dim firstIndex As Integer = WorkShopTextBox.GetCharIndexFromPosition(New Point(0, g.VisibleClipBounds.Y + font_height / 3))
    '    Dim firstLine As Integer = WorkShopTextBox.GetLineFromCharIndex(firstIndex)
    '    Dim firstLineY As Integer = WorkShopTextBox.GetPositionFromCharIndex(firstIndex).Y
    '    g.Clear(Control.DefaultBackColor)
    '    Dim i As Integer = firstLine
    '    Dim y As Single
    '    Dim lastValue As Integer
    '    Do While y < g.VisibleClipBounds.Y + g.VisibleClipBounds.Height
    '        Dim test As String
    '        Try
    '            test = vOriLineAry(i - 1)
    '        Catch ex As Exception
    '            test = lastValue + 1
    '        End Try
    '        y = firstLineY + 2 + font_height * (i - firstLine - 1)
    '        If y < 16 Then
    '            Dim temp As Integer = y
    '        End If
    '        g.DrawString(test.Trim, WorkShopTextBox.Font, Brushes.Maroon, LineNoPictureBox.Width - g.MeasureString(test, WorkShopTextBox.Font).Width, y)

    '        Try
    '            lastValue = test
    '        Catch ex As Exception
    '        End Try
    '        i += 1
    '    Loop
    'End Sub

    Private Sub WorkShopTextBox_VScroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkShopTextBox.VScroll
        LineNoPictureBox.Invalidate()
    End Sub
    Private Sub DoHorizontalScroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkShopTextBox.HScroll
        If _StillSroll = True Then Exit Sub
        If vStillProcessInPaint = True Then Exit Sub
        RulerPictureBox.Invalidate()
        Dim vScrollPosition As Integer = GetScrollPos(WorkShopTextBox.Handle, SB_HORZ)
        SetScrollPos(patternTextBox.Handle, SB_HORZ, vScrollPosition, True)
        SendMessage(patternTextBox.Handle, WM_HSCROLL, CType((4 + &H10000 * vScrollPosition), IntPtr), IntPtr.Zero)
    End Sub
    Private Sub pbRuler_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles RulerPictureBox.Paint
        DrawRichRuler(e.Graphics)
    End Sub
    Private Sub DrawRichRuler(ByRef g As Graphics)
        font_height = WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(2)).Y - WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(1)).Y
        If font_height = 0 Then Exit Sub

        Dim firstIndex As Integer = WorkShopTextBox.GetCharIndexFromPosition(New Point(0, g.VisibleClipBounds.Y + font_height / 3))
        Dim firstLine As Integer = WorkShopTextBox.GetLineFromCharIndex(firstIndex)
        Dim firstLineX As Integer = WorkShopTextBox.GetPositionFromCharIndex(firstIndex).X

        Dim X As Single
        Dim i As Integer = 0
        Do While X < g.VisibleClipBounds.X + g.VisibleClipBounds.Width
            X = firstLineX + (i * 5)
            If i Mod 10 = 0 Then
                Dim FontSize As Double = g.MeasureString("A", WorkShopTextBox.Font).Width
                g.DrawString("|" + (i + 1).ToString(), WorkShopTextBox.Font, Brushes.Black, (8 * i) + firstLineX, 3)
            End If
            i += 1
        Loop
    End Sub
#End Region
#Region "Get Set Position Function"
    Private Const SB_HORZ As Integer = &H0
    Private Const SB_VERT As Integer = &H1
    Private Const WM_HSCROLL As Integer = &H114
    Private Const WM_VSCROLL As Integer = &H115
    Dim vCrrHorz As Integer
    Dim vCrrVer As Integer
    Private Declare Auto Function SetScrollPos Lib "user32" (ByVal hWnd As IntPtr, ByVal nBar As Integer, ByVal nPos As Integer, ByVal bRedraw As Boolean) As Integer
    Private Declare Auto Function GetScrollPos Lib "user32" (ByVal hWnd As IntPtr, ByVal nBar As Integer) As Integer
    Private Declare Auto Function SendMessage Lib "user32" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr

    Private Sub SaveCurrentScrollPosition(ByVal isVertical As Boolean, ByVal isHorizontal As Boolean)
        If isVertical Then vCrrHorz = GetScrollPos(WorkShopTextBox.Handle, SB_HORZ)
        If isHorizontal Then vCrrVer = GetScrollPos(WorkShopTextBox.Handle, SB_VERT)
    End Sub
    Private Sub BackCurrentScrollPosition(ByVal isVertical As Boolean, ByVal isHorizontal As Boolean)
        If isVertical Then
            SetScrollPos(WorkShopTextBox.Handle, SB_VERT, vCrrVer, True)
            SendMessage(WorkShopTextBox.Handle, WM_VSCROLL, CType((4 + &H10000 * vCrrVer), IntPtr), IntPtr.Zero)
        End If
        If isHorizontal Then
            SetScrollPos(WorkShopTextBox.Handle, SB_HORZ, vCrrHorz, True)
            SendMessage(WorkShopTextBox.Handle, WM_HSCROLL, CType((4 + &H10000 * vCrrHorz), IntPtr), IntPtr.Zero)
        End If
    End Sub
#End Region
#Region "Pattern Serach"
    Private IdentifierString As String
    Private Identifier_Decode As String
    Private Still_Process As Boolean = False
    Private Sub PatternTextChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles patternTextBox.TextChanged
        RefreshPaintScreen()
    End Sub
    Public Sub RepairLineNoArray()
        Dim isFirstZero As Boolean = True
        Dim running_no As Integer = 0
        For ind As Integer = 0 To vOriLineAry.Length - 1
            If vOriLineAry(ind) = 0 Then
                running_no = 0
                If isFirstZero Then
                    For ind2 As Integer = ind - 1 To 0 Step -1
                        vOriLineAry(ind2) = vOriLineAry(ind2) - ind - 1
                    Next
                    isFirstZero = False
                End If
            Else
                running_no += 1
                vOriLineAry(ind) = running_no
            End If
        Next

    End Sub
#End Region
#Region "Advance Mouse Select"
    Dim MoveClick As Boolean
    Dim vStartMouseMovePoint As Integer
    Private Sub MouseIsEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkShopTextBox.MouseEnter
        SaveCurrentScrollPosition(True, True)
        BackCurrentScrollPosition(True, True)
    End Sub
    Private Sub MouseIsMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles WorkShopTextBox.MouseMove
        If MoveClick Then
            'BackCurrentScrollPosition(True, True) 'Regina 04 Aug 09 - to prevent highlighting of column appears longer than selected

            Dim CharIndex As Integer = WorkShopTextBox.GetCharIndexFromPosition(New Point(e.X, e.Y))
            If vStartMouseMovePoint = 0 Then Exit Sub
            WorkShopTextBox.Select(CharIndex, vStartMouseMovePoint - CharIndex)
        End If
    End Sub

    Private Sub MouseIsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkShopTextBox.DoubleClick
        MoveClick = False
    End Sub
    Private _isStartIdentifier As Boolean
    'Updated By Hein 20090805
    Private Sub CreateNewIdentifierPoint(ByVal _CanBeNewIdentifier As Boolean, ByVal ViewLineZeroIndex As Integer)

        If _CanBeNewIdentifier Then
            If DialogResult.No = MessageBox.Show("Do you want to add new Identifier?", "Unstructured File Converter", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                'Rollback to original pattern
                patternTextBox.Text = identifier.IDString
                '_isStartIdentifier = True
            Else
                'Create new pattern and save
                _parentForm.CreateNewIdentifier()
                With WorkShopTextBox

                    If WorkShopTextBox.SelectionBackColor <> columnColor Then
                        ResetLineNo()
                        patternTextBox.Text = .Lines(ViewLineZeroIndex - 1)
                        identifier.IDString = patternTextBox.Text
                    End If
                End With
                BackCurrentScrollPosition(True, True)
            End If
        ElseIf WorkShopTextBox.SelectionBackColor = identifierColor And identifier.IDString.Trim = "" Then
            'For Button New Identifier
            _isStartIdentifier = True
            With WorkShopTextBox

                If WorkShopTextBox.SelectionBackColor <> columnColor Then
                    ResetLineNo()
                    patternTextBox.Text = .Lines(ViewLineZeroIndex - 1)
                End If
            End With
            BackCurrentScrollPosition(True, True)
        End If

    End Sub
    Private Sub MouseIsDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles WorkShopTextBox.MouseDown
        MoveClick = True
        vStartMouseMovePoint = WorkShopTextBox.GetCharIndexFromPosition(New Point(e.X, e.Y))

        Dim _CanBeNewIdentifier As Boolean = False
        If identifier Is Nothing Or identifier.IDNo Is Nothing Then
            _CanBeNewIdentifier = True
        ElseIf identifier.IDString Is Nothing Then
            _CanBeNewIdentifier = True
        End If
        'Updated By Hein 20090805
        CreateNewIdentifierPoint(_CanBeNewIdentifier, _
        WorkShopTextBox.GetLineFromCharIndex(WorkShopTextBox.GetCharIndexFromPosition(New Point(e.X, e.Y))) + 1)
    End Sub

    Private Sub MouseIsLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkShopTextBox.MouseLeave
        MoveClick = False
    End Sub
    Private Sub CreateNewColumnPoint()
        MoveClick = False 'For Mouse Select enhacement
        If _isStartIdentifier Then
            _isStartIdentifier = False
            Exit Sub
        End If
        If column Is Nothing And identifier.IDString.Trim <> "" Then
            If DialogResult.Yes = MessageBox.Show("Do you want to add new Column?", "Unstructured File Converter", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                FillColumnColor(True)
            End If
        ElseIf column IsNot Nothing And identifier.IDString.Trim <> "" Then
            FillColumnColor()
        End If
        _parentForm.RefreshPropertyBox()
    End Sub
    'Mouse Click Event is for HightLight Record Field
    Private Sub MouseIsUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles WorkShopTextBox.MouseUp
        If is_mouseClick = True Then
            is_mouseClick = False
        Else
            CreateNewColumnPoint()
        End If
    End Sub
    Dim is_mouseClick As Boolean = False
    Private Sub MouseIsClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles WorkShopTextBox.MouseClick
        is_mouseClick = True
        'Updated By Hein 20090805
        If WorkShopTextBox.SelectedText.Length > 0 Then
            CreateNewColumnPoint()
        Else
            If WorkShopTextBox.SelectionBackColor = clearColor Then
                If identifier.IDString IsNot Nothing Then
                    If identifier.IDString.Trim <> "" Then
                        Dim ViewLineZeroIndex As Integer = WorkShopTextBox.GetLineFromCharIndex(WorkShopTextBox.GetCharIndexFromPosition(New Point(e.X, e.Y))) + 1
                        If CheckCorrectLine(WorkShopTextBox.Lines(ViewLineZeroIndex - 1), patternTextBox.Text, ViewLineZeroIndex - 1) = False Then
                            CreateNewIdentifierPoint(True, _
                                            WorkShopTextBox.GetLineFromCharIndex(WorkShopTextBox.GetCharIndexFromPosition(New Point(e.X, e.Y))) + 1)
                        End If
                    End If
                End If
            End If
            MoveClick = False
        End If

    End Sub

#End Region
#Region "Common Function"
    Public Sub RepairSourceFile()
        'Dim sReader As IO.StreamReader = New IO.StreamReader(sourceFilePath)
        'Dim resulttext As String = ""
        'Dim LineString() As String = WorkShopTextBox.Lines
        'For ind As Integer = 0 To LineString.Length - 1
        '    If ind = 2000 Then Exit For
        '    If ind = LineString.Length - 1 Or ind = 2000 Then
        '        resulttext = resulttext + ReplaceStringDetail(LineString(ind)).Replace(vbTab, " ")
        '    Else
        '        resulttext = resulttext + ReplaceStringDetail(LineString(ind)).Replace(vbTab, " ") + vbNewLine
        '    End If
        'Next
        'WorkShopTextBox.Text = resulttext
        'WorkShopTextBox.Select(0, 0)
    End Sub
    Public Sub ResetLineNo()
        ReDim vOriLineAry(WorkShopTextBox.Lines.Length - 1)
        For ind As Integer = 0 To WorkShopTextBox.Lines.Length - 1
            vOriLineAry(ind) = ind + 1
        Next
    End Sub
    Private Function ReplaceStringDetail(ByVal vOriString As String) As String
        ' If vOriString.Length > vMaxLength Then Return vOriString
        Dim vOriLength As Integer = vOriString.Length - 1
        Dim vNewString As String = ""
        If (vOriString = "") Then
            vOriString = "^"
        End If

        For vInd As Integer = vOriLength To vMaxLength
            vNewString = vNewString + " "
        Next
        vNewString = vOriString + vNewString
        vNewString = vNewString.Substring(0, vMaxLength)
        Return vNewString
    End Function
    Private Function GetViewFirstLine() As Integer
        With WorkShopTextBox
            Return .GetLineFromCharIndex(.GetCharIndexFromPosition(New Point(0, 0))) '+ 1
        End With
    End Function
    Private Function GetViewLastLine() As Integer
        With WorkShopTextBox
            Return .GetLineFromCharIndex(.GetCharIndexFromPosition(New Point(0, .Height))) '- 1
        End With
    End Function
    Public Function isAnyIdentifier(ByVal vSourceLine As String, ByVal index As String) As Boolean
        For Each tempIdentifier As Identifier In identifierCollection
            If CheckCorrectLine(vSourceLine, tempIdentifier.IDString, index) Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Function inTheseIdentifier(ByVal vSourceLine As String, ByVal IdentiferNames As String, ByVal LineIndex As String) As Boolean
        If IdentiferNames.IndexOf("Identifier ") = -1 Then
            Return False
            Exit Function
        End If
        Dim cutIdentifier As String = IdentiferNames.Replace("Identifier ", "")
        If cutIdentifier.Trim = "" Then
            Return False
            Exit Function
        End If
        Dim identifierCheckName() As String = cutIdentifier.Split(",")
        For Each vCheckIdentifierName As String In identifierCheckName
            For Each tempIdentifier As Identifier In identifierCollection
                If tempIdentifier.IDNo = vCheckIdentifierName Then
                    If CheckCorrectLine(vSourceLine, tempIdentifier.IDString, LineIndex) Then
                        Return True
                    End If
                End If
            Next
        Next
        Return False
    End Function

    Public Sub PointZeroStart(ByVal line_no As Integer, ByVal count As Integer)
        Try
            vOriLineAry(line_no) = count
        Catch ex As Exception

        End Try

    End Sub
    Public Sub SearchPattenText()
        With WorkShopTextBox
            ResetLineNo()
            vStillProcessInPaint = True

            Dim vStartLineIndex As Integer = GetViewFirstLine()
            Dim vEndLineIndex As Integer = GetViewLastLine()
            vSecondEditor.Rtf = WorkShopTextBox.Rtf
            With vSecondEditor
                For vRowIndex As Integer = vStartLineIndex To vEndLineIndex
                    .Select(WorkShopTextBox.GetFirstCharIndexFromLine(vRowIndex), vMaxLength)
                    .SelectionBackColor = clearColor

                    Dim vCorrectLine As Boolean = CheckCorrectLine(.Lines(vRowIndex), patternTextBox.Text, vRowIndex)
                    If vCorrectLine Then
                        .Select(WorkShopTextBox.GetFirstCharIndexFromLine(vRowIndex), vMaxLength)
                        .SelectionBackColor = columnColor
                        PointZeroStart(vRowIndex, 0)
                    Else
                        PointZeroStart(vRowIndex, -1)
                    End If
                Next
                SetBackToWorkShop()
                vStillProcessInPaint = False
            End With
        End With
    End Sub
    'Added by Hein 20090812
    Public Sub RepairSourceFileHideView(ByVal vStartLineIndex As Integer, ByVal vEndLineIndex As Integer)
        Dim LineString() As String = WorkShopTextBox.Lines()
        For ind As Integer = vStartLineIndex To vEndLineIndex
            LineString.SetValue(ReplaceStringDetail(LineString(ind)).Replace(vbTab, " "), ind)
        Next

        vSecondEditor.Lines() = LineString
        SetBackToWorkShop()
    End Sub
    'Updated by Hein 20090812
    Public Sub SearchPattenTextInHideView()
        'Updated by Hein 20090831
        GetMaxLength()
        Dim vStartLineIndex As Integer = GetViewFirstLine()
        Dim vEndLineIndex As Integer = GetViewLastLine()

        Try

            RepairSourceFileHideView(vStartLineIndex, vEndLineIndex)
            vSecondEditor.Rtf = WorkShopTextBox.Rtf
            Dim LineString() As String = vSecondEditor.Lines()

            Dim _isSperateLoop As Boolean
            If column Is Nothing Then
                _isSperateLoop = False
            ElseIf column.ColLineNo = 0 And column.ColDataType <> "M" Then
                _isSperateLoop = False
            Else
                _isSperateLoop = True
            End If

            If _isSperateLoop = False Then
                'ColLineNo is Zero, so can do both identifier and column
                For ind As Integer = vStartLineIndex To vEndLineIndex
                    'On Error Resume Next
                    If CheckCorrectLine(LineString(ind), patternTextBox.Text, ind) Then
                        vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind), vMaxLength)
                        vSecondEditor.SelectionBackColor = identifierColor
                        If column IsNot Nothing Then
                            vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind) + column.Start, column.ColLength)
                            vSecondEditor.SelectionBackColor = columnColor
                        End If
                        PointZeroStart(ind, 0)
                    Else
                        vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind), vMaxLength)
                        vSecondEditor.SelectionBackColor = clearColor
                        PointZeroStart(ind, -1)
                    End If
                Next
            Else
                'ColLineNo is not Zero, so can't do both identifier and column, only can do sperately
                For ind As Integer = vStartLineIndex To vEndLineIndex
                    If CheckCorrectLine(LineString(ind), patternTextBox.Text, ind) Then
                        vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind), vMaxLength)
                        vSecondEditor.SelectionBackColor = identifierColor
                        PointZeroStart(ind, 0)
                    Else
                        vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind), vMaxLength)
                        vSecondEditor.SelectionBackColor = clearColor
                        PointZeroStart(ind, -1)
                    End If
                Next

                For ind As Integer = vStartLineIndex To vEndLineIndex
                    If CheckCorrectLine(LineString(ind), patternTextBox.Text, ind) Then
                        If WorkShopTextBox.GetFirstCharIndexFromLine(ind + column.ColLineNo) < 0 Then Exit For
                        Try
                            If column.ColDataType = "M" And column.DataFormat = "Line" Then
                                vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind + column.ColLineNo) + _
                                                                 column.Start, vMaxLength - column.Start)
                            Else
                                Dim vSelectLen As Integer
                                If (Integer.Parse(column.Start) + Integer.Parse(column.ColLength) > vMaxLength) Then
                                    vSelectLen = vMaxLength - column.Start
                                Else
                                    vSelectLen = column.ColLength
                                End If

                                vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(ind + column.ColLineNo) + column.Start, _
                               vSelectLen)
                            End If
                            vSecondEditor.SelectionBackColor = columnColor
                            If column.ColDataType = "M" Then
                                Select Case column.MultiLineEnd
                                    Case "Blank Line"
                                        Dim nextLine As Integer = ind + 1
                                        Try
                                            While (LineString(nextLine + column.ColLineNo).Trim <> "")
                                                vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(nextLine + column.ColLineNo) + _
                                                IIf(column.DataFormat = "Line", 0, column.Start), _
                                                IIf(column.DataFormat = "Line", Integer.Parse(column.Start) + Integer.Parse(column.ColLength), column.ColLength))
                                                vSecondEditor.SelectionBackColor = columnColor
                                                nextLine = nextLine + 1
                                            End While
                                            vSecondEditor.SelectionBackColor = clearColor
                                            vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(nextLine - 1 + column.ColLineNo) + _
                                                                 IIf(column.DataFormat = "Line", 0, column.Start), _
                                                                 IIf(column.DataFormat = "Line", IIf(Integer.Parse(column.Start) + Integer.Parse(column.ColLength) > vMaxLength, vMaxLength, Integer.Parse(column.Start) + Integer.Parse(column.ColLength)), column.ColLength))
                                            vSecondEditor.SelectionBackColor = columnColor
                                        Catch ex As Exception
                                            'Error can occur when nextLine index is last one
                                        End Try
                                    Case "Any Identifier"
                                        Dim nextLine As Integer = ind + 1
                                        Try
                                            While (isAnyIdentifier(WorkShopTextBox.Lines(nextLine + column.ColLineNo), nextLine) = False)
                                                vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(nextLine + column.ColLineNo) + _
                                                                     IIf(column.DataFormat = "Line", 0, column.Start), _
                                                                     IIf(column.DataFormat = "Line", vMaxLength, GetLineEnd(column.Start, column.ColLength)))

                                                vSecondEditor.SelectionBackColor = columnColor
                                                nextLine = nextLine + 1
                                            End While
                                            vSecondEditor.SelectionBackColor = clearColor
                                            vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(nextLine - 1 + column.ColLineNo) + _
                                                                 IIf(column.DataFormat = "Line", 0, column.Start), _
                                                                 IIf(column.DataFormat = "Line", IIf((Integer.Parse(column.Start) + Integer.Parse(column.ColLength)) > vMaxLength, vMaxLength, Integer.Parse(column.Start) + Integer.Parse(column.ColLength)), _
                                                     GetLineEnd(column.Start, column.ColLength)))
                                            vSecondEditor.SelectionBackColor = columnColor

                                        Catch ex As Exception
                                            'Error can occur when nextLine index is last one
                                        End Try
                                    Case Else
                                        Dim nextLine As Integer = ind + 1
                                        Try
                                            While (inTheseIdentifier(LineString(nextLine + column.ColLineNo), column.MultiLineEnd, nextLine) = False)
                                                vSecondEditor.Select(WorkShopTextBox.GetFirstCharIndexFromLine(nextLine + column.ColLineNo) + _
                                                                     IIf(column.DataFormat = "Line", 0, column.Start), _
                                                                     IIf(column.DataFormat = "Line", vMaxLength - column.Start, GetLineEnd(column.Start, column.ColLength)))

                                                vSecondEditor.SelectionBackColor = columnColor
                                                nextLine = nextLine + 1
                                            End While
                                        Catch ex As Exception
                                            'Error can occur when nextLine index is last one
                                        End Try
                                End Select
                                'MessageBox.Show("MultiLine")
                            End If
                        Catch ex As Exception
                            'can be exceed last line
                        End Try
                    End If
                Next
            End If
        Catch ex As IndexOutOfRangeException
            'Resume
        End Try
        SetBackToWorkShop()

    End Sub
    Public Function GetLineEnd(ByVal vStart As String, ByVal vLength As String) As Integer
        If Integer.Parse(vStart) + Integer.Parse(vLength) > vMaxLength Then
            Return vMaxLength
        Else
            Return Integer.Parse(vLength)
        End If
    End Function
    'Private Function GetFirstIndexOfLine(ByVal Textbox As RichTextBox, ByVal LineNo As Integer)

    'End Function

    Private Sub SetBackToWorkShop()
        _StillSroll = True
        SaveCurrentScrollPosition(True, True)
        WorkShopTextBox.SendToBack()
        WorkShopTextBox.Visible = False
        WorkShopTextBox.Rtf = vSecondEditor.Rtf
        BackCurrentScrollPosition(True, True)
        WorkShopTextBox.Visible = True
        rTBHide.SendToBack()
        _StillSroll = False
    End Sub
    Dim _StillSroll As Boolean = False
    'Updated by Hein 20090805
    Private Function GetLineNoFromIdentifier() As Integer
        Try
            vSecondEditor.Rtf = WorkShopTextBox.Rtf
            Dim _findIdentiferLineNo As Integer = 0
            Dim _currentLineIndex As Integer = WorkShopTextBox.GetLineFromCharIndex(WorkShopTextBox.SelectionStart)
            Dim _columnLineIndex As Integer = _currentLineIndex
            Dim _identifierSearcherIndex As Integer = WorkShopTextBox.GetFirstCharIndexFromLine(_currentLineIndex)
            vSecondEditor.Select(_identifierSearcherIndex, 1)
            While (vSecondEditor.SelectionBackColor <> identifierColor)
                _currentLineIndex = _currentLineIndex - 1
                _identifierSearcherIndex = vSecondEditor.GetFirstCharIndexFromLine(_currentLineIndex)
                vSecondEditor.Select(_identifierSearcherIndex, 1)
                If _currentLineIndex < 0 Then Exit While
            End While
            Return _columnLineIndex - _currentLineIndex
        Catch ex As Exception
            Return 0
        End Try

    End Function
    Private Sub FillColumnColor(Optional ByVal isNew As Boolean = False)

        If WorkShopTextBox.SelectedText.Length <= 0 Then Exit Sub
        vSecondEditor.Rtf = WorkShopTextBox.Rtf
        Dim OriSelectStart As Integer = WorkShopTextBox.SelectionStart
        Dim StartPoint As Integer = OriSelectStart - WorkShopTextBox.GetFirstCharIndexOfCurrentLine
        Dim SelectLength As Integer = WorkShopTextBox.SelectedText.Length
        If isNew Then
            _parentForm.CreateNewColumn(identifier.IDNo)
        End If
        'Update Column Property according to user select change
        column.Start = StartPoint
        If StartPoint + SelectLength > vMaxLength Then SelectLength = vMaxLength - StartPoint
        column.ColLength = SelectLength
        'Updated by Hein 20090805
        With WorkShopTextBox
            Try
                Dim _currentLineIndex As Integer = WorkShopTextBox.GetLineFromCharIndex(OriSelectStart)
                Dim _columnLineIndex As Integer = _currentLineIndex
                While (CheckCorrectLine(.Lines(_currentLineIndex), patternTextBox.Text, _currentLineIndex) = False)
                    _currentLineIndex = _currentLineIndex - 1
                    If _currentLineIndex < 0 Then
                        _currentLineIndex = _columnLineIndex
                        Exit While
                    End If

                End While
                column.ColLineNo = _columnLineIndex - _currentLineIndex
            Catch ex As Exception
                column.ColLineNo = 0
            End Try
        End With

        Dim vStartLineIndex As Integer = GetViewFirstLine()
        Dim vEndLineIndex As Integer = GetViewLastLine()
        'Start to read the viewable window line
        With vSecondEditor
            'Updated By Hein 20090805 Clean Screen
            For ind As Integer = vStartLineIndex To vEndLineIndex
                Dim FirstIndexOfLine As Integer = WorkShopTextBox.GetFirstCharIndexFromLine(ind)
                Dim ColumnStartIndex As Integer = WorkShopTextBox.GetFirstCharIndexFromLine(ind + column.ColLineNo)
                .Select(FirstIndexOfLine, vMaxLength)
                .SelectionBackColor = clearColor
            Next

            For ind As Integer = vStartLineIndex To vEndLineIndex
                Try
                    Dim FirstIndexOfLine As Integer = WorkShopTextBox.GetFirstCharIndexFromLine(ind)
                    Dim ColumnStartIndex As Integer = WorkShopTextBox.GetFirstCharIndexFromLine(ind + column.ColLineNo)
                    If CheckCorrectLine(.Lines(ind), patternTextBox.Text, ind) Then
                        'clear previous column select
                        .Select(FirstIndexOfLine, vMaxLength)
                        .SelectionBackColor = identifierColor
                        If FirstIndexOfLine <> ColumnStartIndex Then
                            .Select(ColumnStartIndex, vMaxLength)
                            .SelectionBackColor = clearColor
                        End If

                        .Select(ColumnStartIndex + StartPoint, SelectLength)
                        .SelectionBackColor = columnColor
                    End If
                Catch ex As Exception
                    'Can be exceed to Last Line
                End Try
            Next
        End With
        SetBackToWorkShop()
    End Sub

#End Region


#Region "Variable"
    Private _errorMsg As String
    Private _sourceFilePath As String
    Private _column As New Column
    Private _identifier As New Identifier
    Private _identifierCollection As New IdentifierCollection
    Private _parentForm As Object
#End Region
#Region " Public Property "
    Public Property errorMsg() As String
        Get
            Return _errorMsg
        End Get
        Set(ByVal value As String)
            _errorMsg = value
        End Set
    End Property
    Public Property sourceFilePath() As String
        Get
            Return _sourceFilePath
        End Get
        Set(ByVal value As String)
            _sourceFilePath = value
        End Set
    End Property
    Public Property column() As Column
        Get
            Return _column
        End Get
        Set(ByVal value As Column)
            _column = value
        End Set
    End Property
    Public Property identifier() As Identifier
        Get
            Return _identifier
        End Get
        Set(ByVal value As Identifier)
            _identifier = value
        End Set
    End Property
    Public Property identifierCollection() As IdentifierCollection
        Get
            Return _identifierCollection
        End Get
        Set(ByVal value As IdentifierCollection)
            _identifierCollection = value
        End Set
    End Property
#End Region
#Region "Joined with Property Box"
    'Updated by Hein 20090812
    Public Function ReadSourceFile() As Boolean
        Dim success As Boolean = True
        Try
            'Added by Hein 20090826
            Dim encoding As System.Text.Encoding = BTMU.Magic.Common.HelperModule.GetFileEncoding(sourceFilePath)
            Dim sReader As IO.StreamReader = New IO.StreamReader(sourceFilePath, encoding)
            'Dim sReader As IO.StreamReader = New IO.StreamReader(sourceFilePath)

            WorkShopTextBox.SelectionBackColor = Color.White
            WorkShopTextBox.Text = sReader.ReadToEnd().Replace(vbCr + vbCrLf, vbCr).Replace(vbTab, " ")
            If WorkShopTextBox.Lines.Length > 2000 Then Array.Resize(WorkShopTextBox.Lines, 2000)
            'Updated by Hein 20090831
            GetMaxLength()
          
            ResetLineNo()
            RepairSourceFile()
        Catch ex As Exception
            errorMsg = ex.Message
            success = False
        End Try
        Return success
    End Function
    'Updated by Hein 20090831
    Private Sub GetMaxLength()
        Dim vRawString() As String = WorkShopTextBox.Lines
        Dim vMaxRaw As Integer
        For ind As Integer = 0 To vRawString.Length - 1
            If ind = 0 Then vMaxRaw = vRawString(ind).Length
            If vMaxRaw < vRawString(ind).Length Then
                vMaxRaw = vRawString(ind).Length
            End If
        Next
        vMaxLength = vMaxRaw
    End Sub
    Public Sub SetObjectPoint(ByVal identifierParameter As Identifier, ByVal columnParameter As Column, ByVal identiferCollectionParameter As IdentifierCollection)
        identifier = identifierParameter
        column = columnParameter
        identifierCollection = identiferCollectionParameter
        If patternTextBox.Text = identifier.IDString Then
            RefreshPaintScreen() 'Only need to refresh paint screen
        Else
            patternTextBox.Text = identifier.IDString
        End If
        patternTextBox.Enabled = (columnParameter Is Nothing And identifier.IDNo IsNot Nothing)

    End Sub
    Public Sub CloseEdit()
        identifier.IDString = patternTextBox.Text
    End Sub
#End Region
#Region "Custom Function"
    Dim _StillRefreshPaintScreen As Boolean = False 'for performance
    Public Sub RefreshPaintScreen()
        If _StillRefreshPaintScreen Then Exit Sub 'for performance
        _StillRefreshPaintScreen = True 'for performance
        SaveCurrentScrollPosition(True, True)
        Dim vInd As Integer = patternTextBox.SelectionStart
        patternTextBox.Text = ReplaceStringDetail(patternTextBox.Text)
        identifier.IDString = patternTextBox.Text
        IdentifierString = patternTextBox.Text
        Identifier_Decode = patternTextBox.Text.Replace(" ", "~")
        SearchPattenTextInHideView()
        RepairLineNoArray()
        LineNoPictureBox.Invalidate()
        patternTextBox.Select(vInd, 0)
        BackCurrentScrollPosition(True, True)
        _StillRefreshPaintScreen = False 'for performance
    End Sub
#End Region
    Private Sub DrawRichTextBoxLineNumbers(ByRef g As Graphics)
        font_height = WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(2)).Y - WorkShopTextBox.GetPositionFromCharIndex(WorkShopTextBox.GetFirstCharIndexFromLine(1)).Y
        If font_height <= 0 Then font_height = 16 'Updated by Hein 20090909
        Dim firstIndex As Integer = WorkShopTextBox.GetCharIndexFromPosition(New Point(0, g.VisibleClipBounds.Y + font_height / 3))
        Dim firstLine As Integer = WorkShopTextBox.GetLineFromCharIndex(firstIndex)
        Dim firstLineY As Integer = WorkShopTextBox.GetPositionFromCharIndex(firstIndex).Y
        g.Clear(Control.DefaultBackColor)
        Dim i As Integer = firstLine
        Dim y As Single
        Dim lastValue As Integer
        Do While y < g.VisibleClipBounds.Y + g.VisibleClipBounds.Height
            Dim tmp As Integer = g.VisibleClipBounds.Y + g.VisibleClipBounds.Height
            Dim test As String
            Try
                test = vOriLineAry(i - 1)
            Catch ex As Exception
                test = lastValue + 1
            End Try
            y = firstLineY + 2 + font_height * (i - firstLine - 1)

            g.DrawString(test.Trim, WorkShopTextBox.Font, Brushes.Maroon, LineNoPictureBox.Width - g.MeasureString(test, WorkShopTextBox.Font).Width, y)

            Try
                lastValue = test
            Catch ex As Exception
            End Try
            i += 1
        Loop
    End Sub

End Class
