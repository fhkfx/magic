<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucColumnsProperty
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataFormatLabel = New System.Windows.Forms.Label
        Me.DataTypeComboBox = New System.Windows.Forms.ComboBox
        Me.DataTypeLabel = New System.Windows.Forms.Label
        Me.NameTextBox = New System.Windows.Forms.TextBox
        Me.NameLabel = New System.Windows.Forms.Label
        Me.DataFormatComboBox = New System.Windows.Forms.ComboBox
        Me.RepeatComboBox = New System.Windows.Forms.ComboBox
        Me.LineTextBox = New System.Windows.Forms.TextBox
        Me.LengthTextBox = New System.Windows.Forms.TextBox
        Me.StartTextBox = New System.Windows.Forms.TextBox
        Me.DelimColNoTextBox = New System.Windows.Forms.TextBox
        Me.MultiLineEndLabel = New System.Windows.Forms.Label
        Me.RepeatLabel = New System.Windows.Forms.Label
        Me.IdentifierNoLabel = New System.Windows.Forms.Label
        Me.LineLabel = New System.Windows.Forms.Label
        Me.StartLabel = New System.Windows.Forms.Label
        Me.LengthLabel = New System.Windows.Forms.Label
        Me.DelimColNoLabel = New System.Windows.Forms.Label
        Me.DelimiterLabel = New System.Windows.Forms.Label
        Me.TextQualifierLabel = New System.Windows.Forms.Label
        Me.MultiLineEndComboBox = New System.Windows.Forms.ComboBox
        Me.DelimiterComboBox = New System.Windows.Forms.ComboBox
        Me.TextQualifierComboBox = New System.Windows.Forms.ComboBox
        Me.erroProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.IdentiferComboBox = New System.Windows.Forms.ComboBox
        Me.ColumnNoTextbox = New System.Windows.Forms.TextBox
        CType(Me.erroProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataFormatLabel
        '
        Me.DataFormatLabel.AutoSize = True
        Me.DataFormatLabel.Location = New System.Drawing.Point(2, 47)
        Me.DataFormatLabel.Name = "DataFormatLabel"
        Me.DataFormatLabel.Size = New System.Drawing.Size(65, 14)
        Me.DataFormatLabel.TabIndex = 17
        Me.DataFormatLabel.Text = "Data Format"
        '
        'DataTypeComboBox
        '
        Me.DataTypeComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DataTypeComboBox.FormattingEnabled = True
        Me.DataTypeComboBox.Items.AddRange(New Object() {"Text", "Numeric", "Date", "Multiline"})
        Me.DataTypeComboBox.Location = New System.Drawing.Point(78, 22)
        Me.DataTypeComboBox.Name = "DataTypeComboBox"
        Me.DataTypeComboBox.Size = New System.Drawing.Size(199, 22)
        Me.DataTypeComboBox.TabIndex = 1
        '
        'DataTypeLabel
        '
        Me.DataTypeLabel.AutoSize = True
        Me.DataTypeLabel.Location = New System.Drawing.Point(2, 25)
        Me.DataTypeLabel.Name = "DataTypeLabel"
        Me.DataTypeLabel.Size = New System.Drawing.Size(56, 14)
        Me.DataTypeLabel.TabIndex = 14
        Me.DataTypeLabel.Text = "Data Type"
        '
        'NameTextBox
        '
        Me.NameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NameTextBox.Location = New System.Drawing.Point(78, 2)
        Me.NameTextBox.MaxLength = 50
        Me.NameTextBox.Name = "NameTextBox"
        Me.NameTextBox.Size = New System.Drawing.Size(199, 20)
        Me.NameTextBox.TabIndex = 0
        '
        'NameLabel
        '
        Me.NameLabel.AutoSize = True
        Me.NameLabel.Location = New System.Drawing.Point(2, 5)
        Me.NameLabel.Name = "NameLabel"
        Me.NameLabel.Size = New System.Drawing.Size(34, 14)
        Me.NameLabel.TabIndex = 12
        Me.NameLabel.Text = "Name"
        '
        'DataFormatComboBox
        '
        Me.DataFormatComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DataFormatComboBox.FormattingEnabled = True
        Me.DataFormatComboBox.Location = New System.Drawing.Point(78, 44)
        Me.DataFormatComboBox.Name = "DataFormatComboBox"
        Me.DataFormatComboBox.Size = New System.Drawing.Size(199, 22)
        Me.DataFormatComboBox.TabIndex = 2
        '
        'RepeatComboBox
        '
        Me.RepeatComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RepeatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RepeatComboBox.FormattingEnabled = True
        Me.RepeatComboBox.Items.AddRange(New Object() {"No", "Yes, repeat when imported value is blank", "Yes, repeat last imported value"})
        Me.RepeatComboBox.Location = New System.Drawing.Point(78, 88)
        Me.RepeatComboBox.Name = "RepeatComboBox"
        Me.RepeatComboBox.Size = New System.Drawing.Size(199, 22)
        Me.RepeatComboBox.TabIndex = 5
        '
        'LineTextBox
        '
        Me.LineTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineTextBox.Location = New System.Drawing.Point(78, 132)
        Me.LineTextBox.MaxLength = 4
        Me.LineTextBox.Name = "LineTextBox"
        Me.LineTextBox.Size = New System.Drawing.Size(199, 20)
        Me.LineTextBox.TabIndex = 7
        '
        'LengthTextBox
        '
        Me.LengthTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LengthTextBox.Location = New System.Drawing.Point(78, 172)
        Me.LengthTextBox.MaxLength = 4
        Me.LengthTextBox.Name = "LengthTextBox"
        Me.LengthTextBox.Size = New System.Drawing.Size(199, 20)
        Me.LengthTextBox.TabIndex = 9
        '
        'StartTextBox
        '
        Me.StartTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.StartTextBox.Location = New System.Drawing.Point(78, 152)
        Me.StartTextBox.MaxLength = 4
        Me.StartTextBox.Name = "StartTextBox"
        Me.StartTextBox.Size = New System.Drawing.Size(199, 20)
        Me.StartTextBox.TabIndex = 8
        '
        'DelimColNoTextBox
        '
        Me.DelimColNoTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DelimColNoTextBox.Location = New System.Drawing.Point(78, 192)
        Me.DelimColNoTextBox.MaxLength = 4
        Me.DelimColNoTextBox.Name = "DelimColNoTextBox"
        Me.DelimColNoTextBox.Size = New System.Drawing.Size(199, 20)
        Me.DelimColNoTextBox.TabIndex = 10
        '
        'MultiLineEndLabel
        '
        Me.MultiLineEndLabel.AutoSize = True
        Me.MultiLineEndLabel.Location = New System.Drawing.Point(2, 69)
        Me.MultiLineEndLabel.Name = "MultiLineEndLabel"
        Me.MultiLineEndLabel.Size = New System.Drawing.Size(72, 14)
        Me.MultiLineEndLabel.TabIndex = 27
        Me.MultiLineEndLabel.Text = "Multi Line End"
        '
        'RepeatLabel
        '
        Me.RepeatLabel.AutoSize = True
        Me.RepeatLabel.Location = New System.Drawing.Point(2, 91)
        Me.RepeatLabel.Name = "RepeatLabel"
        Me.RepeatLabel.Size = New System.Drawing.Size(41, 14)
        Me.RepeatLabel.TabIndex = 28
        Me.RepeatLabel.Text = "Repeat"
        '
        'IdentifierNoLabel
        '
        Me.IdentifierNoLabel.AutoSize = True
        Me.IdentifierNoLabel.Location = New System.Drawing.Point(3, 113)
        Me.IdentifierNoLabel.Name = "IdentifierNoLabel"
        Me.IdentifierNoLabel.Size = New System.Drawing.Size(48, 14)
        Me.IdentifierNoLabel.TabIndex = 29
        Me.IdentifierNoLabel.Text = "Identifier"
        '
        'LineLabel
        '
        Me.LineLabel.AutoSize = True
        Me.LineLabel.Location = New System.Drawing.Point(3, 135)
        Me.LineLabel.Name = "LineLabel"
        Me.LineLabel.Size = New System.Drawing.Size(27, 14)
        Me.LineLabel.TabIndex = 30
        Me.LineLabel.Text = "Line"
        '
        'StartLabel
        '
        Me.StartLabel.AutoSize = True
        Me.StartLabel.Location = New System.Drawing.Point(2, 155)
        Me.StartLabel.Name = "StartLabel"
        Me.StartLabel.Size = New System.Drawing.Size(30, 14)
        Me.StartLabel.TabIndex = 31
        Me.StartLabel.Text = "Start"
        '
        'LengthLabel
        '
        Me.LengthLabel.AutoSize = True
        Me.LengthLabel.Location = New System.Drawing.Point(3, 175)
        Me.LengthLabel.Name = "LengthLabel"
        Me.LengthLabel.Size = New System.Drawing.Size(40, 14)
        Me.LengthLabel.TabIndex = 32
        Me.LengthLabel.Text = "Length"
        '
        'DelimColNoLabel
        '
        Me.DelimColNoLabel.AutoSize = True
        Me.DelimColNoLabel.Location = New System.Drawing.Point(3, 195)
        Me.DelimColNoLabel.Name = "DelimColNoLabel"
        Me.DelimColNoLabel.Size = New System.Drawing.Size(66, 14)
        Me.DelimColNoLabel.TabIndex = 33
        Me.DelimColNoLabel.Text = "Delim Col No"
        '
        'DelimiterLabel
        '
        Me.DelimiterLabel.AutoSize = True
        Me.DelimiterLabel.Location = New System.Drawing.Point(2, 215)
        Me.DelimiterLabel.Name = "DelimiterLabel"
        Me.DelimiterLabel.Size = New System.Drawing.Size(47, 14)
        Me.DelimiterLabel.TabIndex = 34
        Me.DelimiterLabel.Text = "Delimiter"
        '
        'TextQualifierLabel
        '
        Me.TextQualifierLabel.AutoSize = True
        Me.TextQualifierLabel.Location = New System.Drawing.Point(2, 237)
        Me.TextQualifierLabel.Name = "TextQualifierLabel"
        Me.TextQualifierLabel.Size = New System.Drawing.Size(71, 14)
        Me.TextQualifierLabel.TabIndex = 35
        Me.TextQualifierLabel.Text = "Text Qualifier"
        '
        'MultiLineEndComboBox
        '
        Me.MultiLineEndComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MultiLineEndComboBox.FormattingEnabled = True
        Me.MultiLineEndComboBox.Location = New System.Drawing.Point(78, 66)
        Me.MultiLineEndComboBox.Name = "MultiLineEndComboBox"
        Me.MultiLineEndComboBox.Size = New System.Drawing.Size(199, 22)
        Me.MultiLineEndComboBox.TabIndex = 3
        '
        'DelimiterComboBox
        '
        Me.DelimiterComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DelimiterComboBox.AutoCompleteCustomSource.AddRange(New String() {"{tab}", "{space}", "{new line}", "{,}", "{other}"})
        Me.DelimiterComboBox.FormattingEnabled = True
        Me.DelimiterComboBox.ItemHeight = 14
        Me.DelimiterComboBox.Items.AddRange(New Object() {"{tab}", "{space}", "{new line}", "{,}", "{other}"})
        Me.DelimiterComboBox.Location = New System.Drawing.Point(78, 212)
        Me.DelimiterComboBox.Name = "DelimiterComboBox"
        Me.DelimiterComboBox.Size = New System.Drawing.Size(199, 22)
        Me.DelimiterComboBox.TabIndex = 37
        '
        'TextQualifierComboBox
        '
        Me.TextQualifierComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextQualifierComboBox.AutoCompleteCustomSource.AddRange(New String() {"{""}", "{'}", "{none}", "{other}"})
        Me.TextQualifierComboBox.FormattingEnabled = True
        Me.TextQualifierComboBox.ItemHeight = 14
        Me.TextQualifierComboBox.Items.AddRange(New Object() {"{""}", "{'}", "{none}", "{other}"})
        Me.TextQualifierComboBox.Location = New System.Drawing.Point(78, 234)
        Me.TextQualifierComboBox.Name = "TextQualifierComboBox"
        Me.TextQualifierComboBox.Size = New System.Drawing.Size(199, 22)
        Me.TextQualifierComboBox.TabIndex = 38
        '
        'erroProvider
        '
        Me.erroProvider.ContainerControl = Me
        '
        'IdentiferComboBox
        '
        Me.IdentiferComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IdentiferComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.IdentiferComboBox.FormattingEnabled = True
        Me.IdentiferComboBox.Items.AddRange(New Object() {"No", "Yes, repeat when imported value is blank", "Yes, repeat last imported value"})
        Me.IdentiferComboBox.Location = New System.Drawing.Point(78, 110)
        Me.IdentiferComboBox.Name = "IdentiferComboBox"
        Me.IdentiferComboBox.Size = New System.Drawing.Size(199, 22)
        Me.IdentiferComboBox.TabIndex = 6
        '
        'ColumnNoTextbox
        '
        Me.ColumnNoTextbox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ColumnNoTextbox.Location = New System.Drawing.Point(78, 66)
        Me.ColumnNoTextbox.Multiline = True
        Me.ColumnNoTextbox.Name = "ColumnNoTextbox"
        Me.ColumnNoTextbox.Size = New System.Drawing.Size(199, 22)
        Me.ColumnNoTextbox.TabIndex = 4
        '
        'ucColumnsProperty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ColumnNoTextbox)
        Me.Controls.Add(Me.IdentiferComboBox)
        Me.Controls.Add(Me.TextQualifierComboBox)
        Me.Controls.Add(Me.DelimiterComboBox)
        Me.Controls.Add(Me.MultiLineEndComboBox)
        Me.Controls.Add(Me.TextQualifierLabel)
        Me.Controls.Add(Me.DelimiterLabel)
        Me.Controls.Add(Me.DelimColNoLabel)
        Me.Controls.Add(Me.LengthLabel)
        Me.Controls.Add(Me.StartLabel)
        Me.Controls.Add(Me.LineLabel)
        Me.Controls.Add(Me.IdentifierNoLabel)
        Me.Controls.Add(Me.RepeatLabel)
        Me.Controls.Add(Me.MultiLineEndLabel)
        Me.Controls.Add(Me.DelimColNoTextBox)
        Me.Controls.Add(Me.LengthTextBox)
        Me.Controls.Add(Me.StartTextBox)
        Me.Controls.Add(Me.LineTextBox)
        Me.Controls.Add(Me.RepeatComboBox)
        Me.Controls.Add(Me.DataFormatComboBox)
        Me.Controls.Add(Me.DataFormatLabel)
        Me.Controls.Add(Me.DataTypeComboBox)
        Me.Controls.Add(Me.DataTypeLabel)
        Me.Controls.Add(Me.NameTextBox)
        Me.Controls.Add(Me.NameLabel)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucColumnsProperty"
        Me.Size = New System.Drawing.Size(303, 260)
        CType(Me.erroProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataFormatLabel As System.Windows.Forms.Label
    Friend WithEvents DataTypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DataTypeLabel As System.Windows.Forms.Label
    Friend WithEvents NameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NameLabel As System.Windows.Forms.Label
    Friend WithEvents DataFormatComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents RepeatComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents LineTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LengthTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StartTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DelimColNoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MultiLineEndLabel As System.Windows.Forms.Label
    Friend WithEvents RepeatLabel As System.Windows.Forms.Label
    Friend WithEvents IdentifierNoLabel As System.Windows.Forms.Label
    Friend WithEvents LineLabel As System.Windows.Forms.Label
    Friend WithEvents StartLabel As System.Windows.Forms.Label
    Friend WithEvents LengthLabel As System.Windows.Forms.Label
    Friend WithEvents DelimColNoLabel As System.Windows.Forms.Label
    Friend WithEvents DelimiterLabel As System.Windows.Forms.Label
    Friend WithEvents TextQualifierLabel As System.Windows.Forms.Label
    Friend WithEvents MultiLineEndComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DelimiterComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TextQualifierComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents erroProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents IdentiferComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnNoTextbox As System.Windows.Forms.TextBox

End Class
