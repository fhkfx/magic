Imports BTMU.Magic.UnstructuredFileConverter
Public Class ucColumnsProperty
#Region "Variable"
    Private _IdentifierCollection As New IdentifierCollection
    Private _Column As New Column
    Private _OriginalColumnName As String
    Private _OriginalIdNo As String
    Private _parentForm As Object
    Private _isStillFillData As Boolean = False
    Private _fileType As String
#End Region
#Region " Public Property "
    Public Property column() As Column
        Get
            Return _Column
        End Get
        Set(ByVal value As Column)
            _Column = value
        End Set
    End Property
    Public Property identifierCollection() As IdentifierCollection
        Get
            Return _IdentifierCollection
        End Get
        Set(ByVal value As IdentifierCollection)
            _IdentifierCollection = value
        End Set
    End Property
    Public Property fileType() As String
        Get
            Return _fileType
        End Get
        Set(ByVal value As String)
            _fileType = value
        End Set
    End Property
    Public Property OriginalIdNo() As String
        Get
            Return _OriginalIdNo
        End Get
        Set(ByVal value As String)
            _OriginalIdNo = value
        End Set
    End Property
    Public Property OriginalColumnName() As String
        Get
            Return _OriginalColumnName
        End Get
        Set(ByVal value As String)
            _OriginalColumnName = value
        End Set
    End Property
    Public ReadOnly Property UserControlType() As String
        Get
            Return "Column"
        End Get
    End Property

#End Region
#Region "Custon Function"
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub New(ByVal columnParameter As Column, ByVal identifierCollectionParameter As IdentifierCollection, ByVal fileTypeParameter As String)
        Me.New()
        FillData(columnParameter, identifierCollectionParameter, fileTypeParameter)
    End Sub
    Public Sub FillData(ByVal columnParameter As Column, ByVal identifierCollectionParameter As IdentifierCollection, ByVal fileTypeParameter As String)

        _isStillFillData = True
        column = columnParameter
        identifierCollection = identifierCollectionParameter
        fileType = fileTypeParameter
        With column
            Select Case fileType
                Case "F"
                    MultiLineEndLabel.Text = "Multi Line End"
                    DataTypeComboBox.Items.Clear()
                    DataTypeComboBox.Items.Add("Text")
                    DataTypeComboBox.Items.Add("Numeric")
                    DataTypeComboBox.Items.Add("Date")
                    DataTypeComboBox.Items.Add("Multiline")
                Case "D", "E"
                    MultiLineEndLabel.Text = "Column No"
                    DataTypeComboBox.Items.Clear()
                    DataTypeComboBox.Items.Add("Text")
                    DataTypeComboBox.Items.Add("Numeric")
                    DataTypeComboBox.Items.Add("Date")
            End Select

            NameTextBox.Text = .ColName
            OriginalColumnName = .ColName
            OriginalIdNo = .IDNo
            FillDataFormat(UnstructureFileConverterCommonFunc.GetColumnDataTypeName(.ColDataType))
            DataTypeComboBox.Text = UnstructureFileConverterCommonFunc.GetColumnDataTypeName(.ColDataType)

            DataFormatComboBox.Text = .DataFormat
            If .ColDataType = "M" And .MultiLineEnd <> "Any Identifier" And .MultiLineEnd <> "Blank Line" Then
                MultiLineEndComboBox.Items.Add(.MultiLineEnd)
                MultiLineEndComboBox.Items.Remove("Identifier n, m, �")
            End If
            MultiLineEndComboBox.Text = .MultiLineEnd
            Select Case .Repeat
                Case "0"
                    RepeatComboBox.Text = "No"
                Case "1"
                    RepeatComboBox.Text = "Yes, repeat when imported value is blank"
                Case "2"
                    RepeatComboBox.Text = "Yes, repeat last imported value"
            End Select
            IdentiferComboBox.Items.Clear()
            For Each tempIdentifier As Identifier In identifierCollection
                IdentiferComboBox.Items.Add(tempIdentifier.IDNo)
            Next

            IdentiferComboBox.Text = .IDNo
            LineTextBox.Text = .ColLineNo
            StartTextBox.Text = UnstructureFileConverterCommonFunc.IntegerParse(.Start) + 1
            LengthTextBox.Text = .ColLength
            LengthPointChange(LengthTextBox, New EventArgs) 'Added by Hein 20090903
            DelimColNoTextBox.Text = .DelimColNo
            DelimiterComboBox.Text = .Delimiter
            TextQualifierComboBox.Text = .TextQualifier
            ColumnNoTextbox.Text = .ColNo
        End With

        'Chnage UI design depend on source file type
        'Updated By Hein 20090824
        'LineTextBox.Visible = (fileType = "F")
        'LineLabel.Visible = (fileType = "F")
        StartTextBox.Visible = (fileType = "F")
        StartLabel.Visible = (fileType = "F")
        LengthTextBox.Visible = (fileType = "F")
        LengthLabel.Visible = (fileType = "F")
        DelimColNoTextBox.Visible = (fileType = "F")
        DelimColNoLabel.Visible = (fileType = "F")
        DelimiterComboBox.Visible = (fileType = "F")
        DelimiterLabel.Visible = (fileType = "F")
        TextQualifierComboBox.Visible = (fileType = "F")
        TextQualifierLabel.Visible = (fileType = "F")
        MultiLineEndComboBox.Visible = (fileType = "F")
        ColumnNoTextbox.Visible = (fileType <> "F")
        _isStillFillData = False
    End Sub
    Public Sub RefreshPropertyBox()
        FillData(column, identifierCollection, fileType)
    End Sub
    Public Function CloseEdit() As Boolean
        Dim errorString As String = erroProvider.GetError(NameTextBox)
        If errorString <> "" Then
            ShowErrorMsg(erroProvider.GetError(NameTextBox), erroProvider.GetError(NameTextBox))
            Return False
        End If
        'Updated by Hein 20090831
        errorString = erroProvider.GetError(LengthTextBox)
        If errorString <> "" Then
            ShowErrorMsg(erroProvider.GetError(LengthTextBox), erroProvider.GetError(LengthTextBox))
            Return False
        End If
        'Updated by Hein 20090831
        Try
            If Integer.Parse(DelimColNoTextBox.Text) > 0 And DelimiterComboBox.Text.Trim = "" Then
                ShowErrorMsg("Delimiter cannot be blank when Delim Col No is greater than zero!", "Delimiter cannot be blank when Delim Col No is greater than zero!")
                DelimiterComboBox.Focus()
                Return False
            End If
        Catch ex As Exception

        End Try

        


        With column
            .ColName = NameTextBox.Text
            Select Case DataTypeComboBox.Text
                Case "Text"
                    .ColDataType = "T"
                Case "Numeric"
                    .ColDataType = "N"
                Case "Date"
                    .ColDataType = "D"
                Case "Multiline"
                    .ColDataType = "M"
            End Select
            .DataFormat = DataFormatComboBox.Text
            .MultiLineEnd = MultiLineEndComboBox.Text
            Select Case RepeatComboBox.Text
                Case "No"
                    .Repeat = "0"
                Case "Yes, repeat when imported value is blank"
                    .Repeat = "1"
                Case "Yes, repeat last imported value"
                    .Repeat = "2"
            End Select
            .IDNo = IdentiferComboBox.Text
            .ColLineNo = LineTextBox.Text
            .Start = UnstructureFileConverterCommonFunc.IntegerParse(StartTextBox.Text) - 1
            .ColLength = LengthTextBox.Text
            .DelimColNo = DelimColNoTextBox.Text
            .Delimiter = DelimiterComboBox.Text
            .TextQualifier = TextQualifierComboBox.Text
        End With
        Return True
    End Function
#End Region
#Region "Form Event"
#Region "Form"
    Private Sub ucColumnsProperty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _parentForm = Me.ParentForm()
    End Sub
#End Region
#Region "TextBox Event"
    Private Sub NumericKeyPressEvent(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles LineTextBox.KeyPress, StartTextBox.KeyPress, LengthTextBox.KeyPress, DelimColNoTextBox.KeyPress, ColumnNoTextbox.KeyPress
        UnstructureFileConverterCommonFunc.NumericTextBox_Validate(sender, e, False, 10, 0, True)
    End Sub
    Private Sub DataFormatChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataFormatComboBox.SelectedIndexChanged
        If _isStillFillData Then Exit Sub
        If column.ColDataType <> "M" Then Exit Sub
        column.DataFormat = DataFormatComboBox.Text
        _parentForm.RefreshPaintScreen()
    End Sub
    Private Sub MultiLineEndChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MultiLineEndComboBox.TextChanged
        If _isStillFillData Then Exit Sub
        column.MultiLineEnd = MultiLineEndComboBox.Text
        _parentForm.RefreshPaintScreen()
    End Sub
    Private Sub StartPointChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartTextBox.TextChanged
        If _isStillFillData Then Exit Sub
        column.Start = UnstructureFileConverterCommonFunc.IntegerParse(StartTextBox.Text) - 1
        _parentForm.RefreshPaintScreen()
    End Sub
    Private Sub LengthPointChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LengthTextBox.TextChanged
        erroProvider.SetError(sender, "") 'Updated by Hein 20090903

        If _isStillFillData Then Exit Sub

        'Updated by Hein 20090831
        If LengthTextBox.Text = "" Then
            erroProvider.SetError(sender, "Length cannot be blank!")
            ' Exit Sub 'Updated by hein 20090303
        Else
            Try
                If Integer.Parse(LengthTextBox.Text) <= 0 Then
                    erroProvider.SetError(sender, "Length cannot be less than one!")
                    ' Exit Sub 'Updated by hein 20090303
                End If
            Catch ex As Exception
            End Try
        End If
        column.ColLength = UnstructureFileConverterCommonFunc.IntegerParse(LengthTextBox.Text)
        _parentForm.RefreshPaintScreen()
    End Sub

    Private Sub ColumnNameChnage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NameTextBox.TextChanged
        If _isStillFillData Then Exit Sub
        If NameTextBox.Text = "" Then
            erroProvider.SetError(sender, "Column name cannot be blank!")
            Exit Sub
        End If

        If _parentForm.CanUseNewName("Column", column, OriginalColumnName, OriginalIdNo, NameTextBox.Text, IdentiferComboBox.Text) Then
            erroProvider.SetError(sender, "")
        Else
            erroProvider.SetError(sender, "Column name cannot be repeated under the same identifer!")
        End If
    End Sub
   
    Private Sub IdentiferNoChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IdentiferComboBox.SelectedIndexChanged
        If _isStillFillData Then Exit Sub
        Dim tmpIDNo As String = IdentiferComboBox.Text
        If _parentForm.ColumnChangeIdentifier(IdentiferComboBox.Text) Then
            column.IDNo = tmpIDNo
            If _parentForm.CanUseNewName("Column", column, OriginalColumnName, OriginalIdNo, NameTextBox.Text, IdentiferComboBox.Text) Then
                erroProvider.SetError(NameTextBox, "")
            Else
                erroProvider.SetError(NameTextBox, "This column name can't be same in same identifier!")
            End If

        End If
    End Sub
    Private Sub LineNoChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LineTextBox.TextChanged
        If _isStillFillData Then Exit Sub
        column.ColLineNo = UnstructureFileConverterCommonFunc.IntegerParse(LineTextBox.Text)
        _parentForm.RefreshPaintScreen()
    End Sub

    Private Sub ColumnNoChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColumnNoTextbox.TextChanged
        If _isStillFillData Then Exit Sub
        column.ColNo = UnstructureFileConverterCommonFunc.IntegerParse(ColumnNoTextbox.Text)
        _parentForm.RefreshPaintScreen()
    End Sub
    Private Sub DelimColNoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DelimColNoTextBox.TextChanged
        Dim tempInteger As Integer = 0
        Integer.TryParse(DelimColNoTextBox.Text, tempInteger)
        DelimiterLabel.Enabled = (tempInteger > 0)
        DelimiterComboBox.Enabled = (tempInteger > 0)
        TextQualifierLabel.Enabled = (tempInteger > 0)
        TextQualifierComboBox.Enabled = (tempInteger > 0)
        'Updated by Hein 20090831
        If DelimColNoTextBox.Text = "0" Then
            DelimiterComboBox.Text = ""
            TextQualifierComboBox.Text = ""
        End If
    End Sub
    Private Sub NumericZeroValidate(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles LengthTextBox.Validating, DelimColNoTextBox.Validating, LineTextBox.Validating
        NumericTextBox_DefaultValue(sender)
    End Sub
    Private Sub NumericOneValidate(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles StartTextBox.Validating, ColumnNoTextbox.Validating
        NumericTextBox_DefaultValue(sender, 1)
    End Sub
    Private Sub NotAllowSpecialCharacter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NameTextBox.KeyPress
        If Not (Char.IsDigit(e.KeyChar) Or Char.IsLetter(e.KeyChar) Or Char.IsControl(e.KeyChar) Or Char.IsWhiteSpace(e.KeyChar)) Then e.Handled = True
    End Sub
#End Region
#Region "Combobox Event"
    Private Sub FillDataFormat(ByVal DataType As String)
        DataFormatComboBox.Items.Clear()
        MultiLineEndComboBox.Items.Clear()

        Select Case DataType
            Case "Text"
                DataFormatComboBox.Items.Add("Trim")
                DataFormatComboBox.Items.Add("Raw")
                DataFormatComboBox.SelectedIndex = 0
            Case "Numeric"
                DataFormatComboBox.Items.Add(".")
                DataFormatComboBox.Items.Add(",")
                DataFormatComboBox.SelectedIndex = 0
            Case "Multiline"
                DataFormatComboBox.Items.Add("Clip")
                DataFormatComboBox.Items.Add("Line")
                DataFormatComboBox.SelectedIndex = 0

                MultiLineEndComboBox.Items.Add("Any Identifier")
                MultiLineEndComboBox.Items.Add("Identifier n, m, �")
                MultiLineEndComboBox.Items.Add("Blank Line")
                MultiLineEndComboBox.SelectedIndex = 0

        End Select
        DataFormatLabel.Enabled = (DataType <> "Date")
        DataFormatComboBox.Enabled = (DataType <> "Date")


        MultiLineEndComboBox.Enabled = (DataType = "Multiline")
        Select Case fileType
            Case "F"
                MultiLineEndLabel.Enabled = (DataType = "Multiline")
            Case "D", "E"
                MultiLineEndLabel.Enabled = True
        End Select
    End Sub
    Private Sub DataTypeChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataTypeComboBox.SelectedIndexChanged
        If _isStillFillData Then Exit Sub
        FillDataFormat(DataTypeComboBox.Text)

        Select Case DataTypeComboBox.Text
            Case "Text"
                column.ColDataType = "T"
            Case "Numeric"
                column.ColDataType = "N"
            Case "Date"
                column.ColDataType = "D"
            Case "Multiline"
                column.ColDataType = "M"
        End Select
        _parentForm.RefreshPaintScreen()
    End Sub
#End Region
#End Region

   


  
End Class
