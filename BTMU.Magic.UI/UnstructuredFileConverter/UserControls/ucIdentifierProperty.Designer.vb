<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucIdentifierProperty
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.LineOffsetLabel = New System.Windows.Forms.Label
        Me.LineOffsetTextBox = New System.Windows.Forms.TextBox
        Me.TypeComboBox = New System.Windows.Forms.ComboBox
        Me.TypeLabel = New System.Windows.Forms.Label
        Me.IdNoTextBox = New System.Windows.Forms.TextBox
        Me.IdentifierNoLabel = New System.Windows.Forms.Label
        Me.erroProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.erroProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LineOffsetLabel
        '
        Me.LineOffsetLabel.AutoSize = True
        Me.LineOffsetLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LineOffsetLabel.Location = New System.Drawing.Point(3, 47)
        Me.LineOffsetLabel.Name = "LineOffsetLabel"
        Me.LineOffsetLabel.Size = New System.Drawing.Size(61, 14)
        Me.LineOffsetLabel.TabIndex = 11
        Me.LineOffsetLabel.Text = "Line Offset"
        '
        'LineOffsetTextBox
        '
        Me.LineOffsetTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineOffsetTextBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LineOffsetTextBox.Location = New System.Drawing.Point(78, 45)
        Me.LineOffsetTextBox.Name = "LineOffsetTextBox"
        Me.LineOffsetTextBox.Size = New System.Drawing.Size(199, 20)
        Me.LineOffsetTextBox.TabIndex = 2
        '
        'TypeComboBox
        '
        Me.TypeComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TypeComboBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.Items.AddRange(New Object() {"Header", "Detail", "Trailer", "Delete"})
        Me.TypeComboBox.Location = New System.Drawing.Point(78, 23)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(199, 22)
        Me.TypeComboBox.TabIndex = 1
        '
        'TypeLabel
        '
        Me.TypeLabel.AutoSize = True
        Me.TypeLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TypeLabel.Location = New System.Drawing.Point(3, 26)
        Me.TypeLabel.Name = "TypeLabel"
        Me.TypeLabel.Size = New System.Drawing.Size(31, 14)
        Me.TypeLabel.TabIndex = 8
        Me.TypeLabel.Text = "Type"
        '
        'IdNoTextBox
        '
        Me.IdNoTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IdNoTextBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdNoTextBox.Location = New System.Drawing.Point(78, 3)
        Me.IdNoTextBox.MaxLength = 100
        Me.IdNoTextBox.Name = "IdNoTextBox"
        Me.IdNoTextBox.Size = New System.Drawing.Size(199, 20)
        Me.IdNoTextBox.TabIndex = 0
        '
        'IdentifierNoLabel
        '
        Me.IdentifierNoLabel.AutoSize = True
        Me.IdentifierNoLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdentifierNoLabel.Location = New System.Drawing.Point(3, 6)
        Me.IdentifierNoLabel.Name = "IdentifierNoLabel"
        Me.IdentifierNoLabel.Size = New System.Drawing.Size(64, 14)
        Me.IdentifierNoLabel.TabIndex = 6
        Me.IdentifierNoLabel.Text = "Identifier No"
        '
        'erroProvider
        '
        Me.erroProvider.ContainerControl = Me
        '
        'ucIdentifierProperty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LineOffsetLabel)
        Me.Controls.Add(Me.LineOffsetTextBox)
        Me.Controls.Add(Me.TypeComboBox)
        Me.Controls.Add(Me.TypeLabel)
        Me.Controls.Add(Me.IdNoTextBox)
        Me.Controls.Add(Me.IdentifierNoLabel)
        Me.Name = "ucIdentifierProperty"
        Me.Size = New System.Drawing.Size(303, 249)
        CType(Me.erroProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LineOffsetLabel As System.Windows.Forms.Label
    Friend WithEvents LineOffsetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TypeLabel As System.Windows.Forms.Label
    Friend WithEvents IdNoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IdentifierNoLabel As System.Windows.Forms.Label
    Friend WithEvents erroProvider As System.Windows.Forms.ErrorProvider

End Class
