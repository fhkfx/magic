Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop.Excel
Imports excel = Microsoft.Office.Interop.Excel
Public Class ExcelReaderInterop
    '''Added by Regina on 18 Aug 2009 - help to speed up loading of Excel
    ''' <summary>
    ''' Open the file path received in Excel. Then, open the workbook
    ''' within the file. Send the workbook to the next function, the internal scan
    ''' function. Will throw an exception if a file cannot be found or opened.
    ''' </summary>
    Public Shared Function ExcelOpenSpreadsheets(ByVal fileName As String, ByVal worksheetName As String) As System.Data.DataTable

        Dim excelApp As Application = Nothing
        Dim wkBook As Workbook = Nothing
        Try

            excelApp = New Application()

            wkBook = excelApp.Workbooks.Open(fileName, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            Dim dt As System.Data.DataTable = ExcelScanIntenal(wkBook, worksheetName)

            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            If wkBook IsNot Nothing Then
                wkBook.Close(False, fileName, Nothing)
                Marshal.ReleaseComObject(wkBook)
            End If

            If excelApp IsNot Nothing Then
                Marshal.ReleaseComObject(excelApp)
                excelApp = Nothing
                GC.Collect()
                GC.WaitForPendingFinalizers()
            End If
        End Try
    End Function

    Private Shared Function ExcelScanIntenal(ByVal wkBook As Workbook, ByVal worksheetName As String) As System.Data.DataTable

        Dim numSheets As Integer = wkBook.Sheets.Count
        Dim sheetNum As Integer
        For sheetNum = 1 To numSheets


            Dim sheet As Worksheet = CType(wkBook.Sheets(sheetNum), Worksheet)
            If sheet.Name = worksheetName Then
                Dim valueArray As Object(,)
                Dim excelRange As Range = sheet.UsedRange
                'Update by Hein 20090821
                If (excelRange.Columns.Count = 1 And excelRange.Rows.Count = 1) Then ' For Only 1 Column 1 Row has data
                    ReDim valueArray(1, 1)
                    valueArray(1, 1) = excelRange.Value(Excel.XlRangeValueDataType.xlRangeValueDefault)
                Else
                    valueArray = CType(excelRange.Value(Excel.XlRangeValueDataType.xlRangeValueDefault), Object(,))
                End If
                Dim dt As System.Data.DataTable = New System.Data.DataTable()
                Dim colIndex As Integer
                Dim rowIndex As Integer
                'Added by Hein 20090821
                If valueArray Is Nothing Then Return dt 'For Blank Sheet

                For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                    dt.Columns.Add("Column" & colIndex, GetType(String))
                Next


                For rowIndex = 0 To valueArray.GetUpperBound(0) - 1
                    Dim dr As System.Data.DataRow = dt.NewRow()
                    For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                        Dim temp As Object = valueArray(rowIndex + 1, colIndex + 1)
                        If temp IsNot Nothing Then
                            dr(colIndex) = temp.ToString()
                        End If
                    Next
                    dt.Rows.Add(dr)
                Next
                Return dt
            End If

        Next sheetNum

        Return Nothing
    End Function
End Class
