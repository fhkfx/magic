<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucGridEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DelimiterLabel = New System.Windows.Forms.Label
        Me.DelimiterComboBox = New System.Windows.Forms.ComboBox
        Me.TextQualifierComboBox = New System.Windows.Forms.ComboBox
        Me.TextQualifierLabel = New System.Windows.Forms.Label
        Me.IncludeFirstLineCheckBox = New System.Windows.Forms.CheckBox
        Me.RawTextRickTextBox = New System.Windows.Forms.RichTextBox
        Me.PatternTextBox = New System.Windows.Forms.TextBox
        Me.PatternLineNoTextbox = New System.Windows.Forms.TextBox
        Me.EditWorkShopGirdView = New System.Windows.Forms.DataGridView
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.SheetComboBox = New System.Windows.Forms.ComboBox
        Me.SheetLabel = New System.Windows.Forms.Label
        CType(Me.EditWorkShopGirdView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DelimiterLabel
        '
        Me.DelimiterLabel.AutoSize = True
        Me.DelimiterLabel.Location = New System.Drawing.Point(58, 5)
        Me.DelimiterLabel.Name = "DelimiterLabel"
        Me.DelimiterLabel.Size = New System.Drawing.Size(47, 14)
        Me.DelimiterLabel.TabIndex = 0
        Me.DelimiterLabel.Text = "Delimiter"
        '
        'DelimiterComboBox
        '
        Me.DelimiterComboBox.FormattingEnabled = True
        Me.DelimiterComboBox.Items.AddRange(New Object() {"{tab}", "{space}", "{new line}", "{,}", "{other}"})
        Me.DelimiterComboBox.Location = New System.Drawing.Point(122, 2)
        Me.DelimiterComboBox.MaxLength = 3
        Me.DelimiterComboBox.Name = "DelimiterComboBox"
        Me.DelimiterComboBox.Size = New System.Drawing.Size(116, 22)
        Me.DelimiterComboBox.TabIndex = 1
        Me.DelimiterComboBox.Visible = False
        '
        'TextQualifierComboBox
        '
        Me.TextQualifierComboBox.FormattingEnabled = True
        Me.TextQualifierComboBox.Items.AddRange(New Object() {"{""}", "{'}", "{none}", "{other}"})
        Me.TextQualifierComboBox.Location = New System.Drawing.Point(412, 2)
        Me.TextQualifierComboBox.MaxLength = 3
        Me.TextQualifierComboBox.Name = "TextQualifierComboBox"
        Me.TextQualifierComboBox.Size = New System.Drawing.Size(121, 22)
        Me.TextQualifierComboBox.TabIndex = 3
        Me.TextQualifierComboBox.Visible = False
        '
        'TextQualifierLabel
        '
        Me.TextQualifierLabel.AutoSize = True
        Me.TextQualifierLabel.Location = New System.Drawing.Point(337, 5)
        Me.TextQualifierLabel.Name = "TextQualifierLabel"
        Me.TextQualifierLabel.Size = New System.Drawing.Size(71, 14)
        Me.TextQualifierLabel.TabIndex = 2
        Me.TextQualifierLabel.Text = "Text Qualifier"
        Me.TextQualifierLabel.Visible = False
        '
        'IncludeFirstLineCheckBox
        '
        Me.IncludeFirstLineCheckBox.AutoSize = True
        Me.IncludeFirstLineCheckBox.Location = New System.Drawing.Point(538, 4)
        Me.IncludeFirstLineCheckBox.Name = "IncludeFirstLineCheckBox"
        Me.IncludeFirstLineCheckBox.Size = New System.Drawing.Size(107, 18)
        Me.IncludeFirstLineCheckBox.TabIndex = 4
        Me.IncludeFirstLineCheckBox.Text = "Include First Line"
        Me.IncludeFirstLineCheckBox.UseVisualStyleBackColor = True
        '
        'RawTextRickTextBox
        '
        Me.RawTextRickTextBox.Location = New System.Drawing.Point(3, 381)
        Me.RawTextRickTextBox.Name = "RawTextRickTextBox"
        Me.RawTextRickTextBox.Size = New System.Drawing.Size(977, 136)
        Me.RawTextRickTextBox.TabIndex = 7
        Me.RawTextRickTextBox.Text = ""
        Me.RawTextRickTextBox.Visible = False
        '
        'PatternTextBox
        '
        Me.PatternTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PatternTextBox.Location = New System.Drawing.Point(61, 306)
        Me.PatternTextBox.Name = "PatternTextBox"
        Me.PatternTextBox.Size = New System.Drawing.Size(764, 20)
        Me.PatternTextBox.TabIndex = 8
        '
        'PatternLineNoTextbox
        '
        Me.PatternLineNoTextbox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PatternLineNoTextbox.Location = New System.Drawing.Point(905, 306)
        Me.PatternLineNoTextbox.Name = "PatternLineNoTextbox"
        Me.PatternLineNoTextbox.Size = New System.Drawing.Size(41, 20)
        Me.PatternLineNoTextbox.TabIndex = 9
        Me.PatternLineNoTextbox.Text = "0"
        Me.PatternLineNoTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'EditWorkShopGirdView
        '
        Me.EditWorkShopGirdView.AllowUserToAddRows = False
        Me.EditWorkShopGirdView.AllowUserToDeleteRows = False
        Me.EditWorkShopGirdView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EditWorkShopGirdView.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.EditWorkShopGirdView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.EditWorkShopGirdView.Location = New System.Drawing.Point(61, 43)
        Me.EditWorkShopGirdView.Name = "EditWorkShopGirdView"
        Me.EditWorkShopGirdView.ReadOnly = True
        Me.EditWorkShopGirdView.RowHeadersWidth = 4
        Me.EditWorkShopGirdView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.EditWorkShopGirdView.Size = New System.Drawing.Size(885, 261)
        Me.EditWorkShopGirdView.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Blue
        Me.Label19.Location = New System.Drawing.Point(1, 306)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(57, 26)
        Me.Label19.TabIndex = 25
        Me.Label19.Text = "Identifier" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Type" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(831, 309)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Column No"
        '
        'SheetComboBox
        '
        Me.SheetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SheetComboBox.FormattingEnabled = True
        Me.SheetComboBox.Location = New System.Drawing.Point(122, 2)
        Me.SheetComboBox.Name = "SheetComboBox"
        Me.SheetComboBox.Size = New System.Drawing.Size(209, 22)
        Me.SheetComboBox.TabIndex = 27
        Me.SheetComboBox.Visible = False
        '
        'SheetLabel
        '
        Me.SheetLabel.AutoSize = True
        Me.SheetLabel.Location = New System.Drawing.Point(58, 5)
        Me.SheetLabel.Name = "SheetLabel"
        Me.SheetLabel.Size = New System.Drawing.Size(59, 14)
        Me.SheetLabel.TabIndex = 28
        Me.SheetLabel.Text = "Worksheet"
        Me.SheetLabel.Visible = False
        '
        'ucGridEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SheetLabel)
        Me.Controls.Add(Me.SheetComboBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.PatternLineNoTextbox)
        Me.Controls.Add(Me.PatternTextBox)
        Me.Controls.Add(Me.RawTextRickTextBox)
        Me.Controls.Add(Me.EditWorkShopGirdView)
        Me.Controls.Add(Me.IncludeFirstLineCheckBox)
        Me.Controls.Add(Me.TextQualifierComboBox)
        Me.Controls.Add(Me.TextQualifierLabel)
        Me.Controls.Add(Me.DelimiterComboBox)
        Me.Controls.Add(Me.DelimiterLabel)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucGridEditor"
        Me.Size = New System.Drawing.Size(949, 345)
        CType(Me.EditWorkShopGirdView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DelimiterLabel As System.Windows.Forms.Label
    Friend WithEvents DelimiterComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TextQualifierComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TextQualifierLabel As System.Windows.Forms.Label
    Friend WithEvents IncludeFirstLineCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents RawTextRickTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents PatternTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PatternLineNoTextbox As System.Windows.Forms.TextBox
    Friend WithEvents EditWorkShopGirdView As System.Windows.Forms.DataGridView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SheetComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SheetLabel As System.Windows.Forms.Label

End Class
