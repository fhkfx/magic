Imports BTMU.Magic.UnstructuredFileConverter
Public Class ucIdentifierProperty
#Region "Variable"
    Private _Identifier As New Identifier
    Private _OriginalIDNo As String
    Private _isStillFillData As Boolean = False
    Private _parentForm As Object
    Private _fileType As String
#End Region
#Region " Public Property "
    Public Property fileType() As String
        Get
            Return _fileType
        End Get
        Set(ByVal value As String)
            _fileType = value
        End Set
    End Property
    Public Property identifier() As Identifier
        Get
            Return _Identifier
        End Get
        Set(ByVal value As Identifier)
            _Identifier = value
        End Set
    End Property
    Public Property OriginalIDNo() As String
        Get
            Return _OriginalIDNo
        End Get
        Set(ByVal value As String)
            _OriginalIDNo = value
        End Set
    End Property

    Public ReadOnly Property UserControlType() As String
        Get
            Return "Identifier"
        End Get
    End Property
#End Region

#Region "Custom Function"
    Public Sub RefreshPropertyBox()
        FillData(identifier, fileType)
    End Sub
    Public Sub FillData(ByVal identifierParameter As Identifier, ByVal fileTypeParameter As String)
        _isStillFillData = True
        Me.identifier = identifierParameter
        IdNoTextBox.Text = identifier.IDNo
        OriginalIDNo = identifier.IDNo
        TypeComboBox.Text = UnstructureFileConverterCommonFunc.GetIdentifierTypeName(identifier.IDType)
        fileType = fileTypeParameter
        ChangeGuiDependOnType()
        LineOffsetTextBox.Text = identifier.LineOffset

        _isStillFillData = False
    End Sub
    Public Function CloseEdit() As Boolean
        Dim errorString As String = erroProvider.GetError(IdNoTextBox)
        If errorString <> "" Then
            ShowErrorMsg(erroProvider.GetError(IdNoTextBox), erroProvider.GetError(IdNoTextBox))
            Return False
        End If
      
        _parentForm.ChangeIdentifierName(identifier.IDNo, IdNoTextBox.Text)
        identifier.IDNo = IdNoTextBox.Text
        Select Case TypeComboBox.Text
            Case "Header"
                identifier.IDType = "H"
            Case "Detail"
                identifier.IDType = "D"
            Case "Trailer"
                identifier.IDType = "T"
            Case "Delete"
                identifier.IDType = "X"
        End Select
        identifier.LineOffset = LineOffsetTextBox.Text
        Return True
    End Function
#End Region

#Region "Control Event"
#Region "Form"
    Private Sub ucIdentifierProperty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _parentForm = Me.ParentForm()
    End Sub
#End Region
#Region "Text Box"
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub New(ByVal identifierParameter As Identifier, ByVal fileTypeParameter As String)
        Me.New()
        FillData(identifierParameter, fileTypeParameter)
    End Sub
    Private Sub TypeChangeEvent(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TypeComboBox.SelectedIndexChanged
        ChangeGuiDependOnType()
    End Sub
    Private Sub ChangeGuiDependOnType()
        Select Case fileType
            Case "F"
                LineOffsetLabel.Text = "Line Offset"
                LineOffsetLabel.Enabled = (TypeComboBox.Text = "Delete")
                LineOffsetTextBox.Enabled = (TypeComboBox.Text = "Delete")
                LineOffsetTextBox.Text = 0
            Case "D", "E"
                LineOffsetLabel.Text = "Column No"
                LineOffsetLabel.Enabled = True
                LineOffsetTextBox.Enabled = True
        End Select

    End Sub



    Private Sub NumericKeyPressEvent(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles LineOffsetTextBox.KeyPress
        UnstructureFileConverterCommonFunc.NumericTextBox_Validate(sender, e, False, 10, 0, True)
    End Sub

    Private Sub NumericZeroValidate(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles LineOffsetTextBox.Validating
        NumericTextBox_DefaultValue(sender)
    End Sub

    Private Sub IdentifierIDChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IdNoTextBox.TextChanged
        If _isStillFillData Then Exit Sub
        If sender.Text = "" Then
            erroProvider.SetError(sender, "Identifier ID cannot be blank!")
            Exit Sub
        End If
        If _parentForm.CanUseNewName("Identifier", identifier, sender.Text, "", "", "") Then
            erroProvider.SetError(sender, "")
        Else
            erroProvider.SetError(sender, "Identifier ID is already been used!")
        End If
    End Sub
    Private Sub LineOffsetTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LineOffsetTextBox.TextChanged
        If _isStillFillData Then Exit Sub
        If fileType = "E" Or fileType = "D" Then
            identifier.LineOffset = UnstructureFileConverterCommonFunc.IntegerParse(LineOffsetTextBox.Text)
            _parentForm.RefreshPaintScreen()
        End If
    End Sub
    Private Sub NotAllowSpecialCharacter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles IdNoTextBox.KeyPress
        If Not (Char.IsDigit(e.KeyChar) Or Char.IsLetter(e.KeyChar) Or Char.IsControl(e.KeyChar) Or Char.IsWhiteSpace(e.KeyChar)) Then e.Handled = True
    End Sub
#End Region
#End Region

 
   
End Class
