<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucTextEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.RulerPictureBox = New System.Windows.Forms.PictureBox
        Me.patternTextBox = New System.Windows.Forms.TextBox
        Me.LineNoPictureBox = New System.Windows.Forms.PictureBox
        Me.WorkShopTextBox = New System.Windows.Forms.RichTextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.VerticalTimer = New System.Windows.Forms.Timer(Me.components)
        Me.saveTextBox = New System.Windows.Forms.RichTextBox
        Me.rTBHide = New System.Windows.Forms.RichTextBox
        CType(Me.RulerPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LineNoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RulerPictureBox
        '
        Me.RulerPictureBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RulerPictureBox.Location = New System.Drawing.Point(62, 3)
        Me.RulerPictureBox.Name = "RulerPictureBox"
        Me.RulerPictureBox.Size = New System.Drawing.Size(730, 20)
        Me.RulerPictureBox.TabIndex = 21
        Me.RulerPictureBox.TabStop = False
        '
        'patternTextBox
        '
        Me.patternTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.patternTextBox.BackColor = System.Drawing.SystemColors.Info
        Me.patternTextBox.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.patternTextBox.Location = New System.Drawing.Point(62, 264)
        Me.patternTextBox.Name = "patternTextBox"
        Me.patternTextBox.Size = New System.Drawing.Size(730, 22)
        Me.patternTextBox.TabIndex = 20
        Me.patternTextBox.WordWrap = False
        '
        'LineNoPictureBox
        '
        Me.LineNoPictureBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LineNoPictureBox.Location = New System.Drawing.Point(24, 26)
        Me.LineNoPictureBox.Name = "LineNoPictureBox"
        Me.LineNoPictureBox.Size = New System.Drawing.Size(36, 213)
        Me.LineNoPictureBox.TabIndex = 19
        Me.LineNoPictureBox.TabStop = False
        '
        'WorkShopTextBox
        '
        Me.WorkShopTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WorkShopTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.WorkShopTextBox.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WorkShopTextBox.ImeMode = System.Windows.Forms.ImeMode.Close
        Me.WorkShopTextBox.Location = New System.Drawing.Point(62, 26)
        Me.WorkShopTextBox.Name = "WorkShopTextBox"
        Me.WorkShopTextBox.ReadOnly = True
        Me.WorkShopTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth
        Me.WorkShopTextBox.Size = New System.Drawing.Size(749, 232)
        Me.WorkShopTextBox.TabIndex = 18
        Me.WorkShopTextBox.Text = ""
        Me.WorkShopTextBox.WordWrap = False
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Blue
        Me.Label19.Location = New System.Drawing.Point(3, 261)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(57, 26)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Identifier" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Type" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(8, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Position"
        '
        'VerticalTimer
        '
        Me.VerticalTimer.Enabled = True
        '
        'saveTextBox
        '
        Me.saveTextBox.Location = New System.Drawing.Point(721, 305)
        Me.saveTextBox.Name = "saveTextBox"
        Me.saveTextBox.Size = New System.Drawing.Size(100, 52)
        Me.saveTextBox.TabIndex = 26
        Me.saveTextBox.Text = ""
        Me.saveTextBox.Visible = False
        '
        'rTBHide
        '
        Me.rTBHide.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rTBHide.BackColor = System.Drawing.Color.Gainsboro
        Me.rTBHide.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rTBHide.ImeMode = System.Windows.Forms.ImeMode.Close
        Me.rTBHide.Location = New System.Drawing.Point(62, 25)
        Me.rTBHide.Name = "rTBHide"
        Me.rTBHide.ReadOnly = True
        Me.rTBHide.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.rTBHide.Size = New System.Drawing.Size(730, 214)
        Me.rTBHide.TabIndex = 27
        Me.rTBHide.Text = ""
        Me.rTBHide.WordWrap = False
        '
        'ucTextEditor
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.rTBHide)
        Me.Controls.Add(Me.saveTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.RulerPictureBox)
        Me.Controls.Add(Me.patternTextBox)
        Me.Controls.Add(Me.LineNoPictureBox)
        Me.Controls.Add(Me.WorkShopTextBox)
        Me.Name = "ucTextEditor"
        Me.Size = New System.Drawing.Size(824, 320)
        CType(Me.RulerPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LineNoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RulerPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents patternTextBox As System.Windows.Forms.TextBox
    Public WithEvents LineNoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents WorkShopTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VerticalTimer As System.Windows.Forms.Timer
    Friend WithEvents saveTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents rTBHide As System.Windows.Forms.RichTextBox

End Class
