Imports BTMU.Magic.UnstructuredFileConverter
Imports BTMU.Magic.Common
Imports System.Data
Imports System.Data.OleDb
Public Class ucGridEditor
    Dim delimiterName As String
    Dim ExcelConncetionString As String
    Dim ExcelConncetion As OleDbConnection
    Dim ExcelCommand As OleDbCommand
    Dim delimiterCode As String
    Dim textQualifierCode As String
    Dim textQualifierReplaceCode As String = "~"
    Dim columnColor As Color = Color.Yellow
    Dim identifierColor As Color = Color.LightSteelBlue
    Dim clearColor As Color
    Dim editDataTable As New DataTable()
    Dim isStillFillData As Boolean = False
    Dim columnCellStyle As New System.Windows.Forms.DataGridViewCellStyle()
    Dim identifierCellStyle As New System.Windows.Forms.DataGridViewCellStyle()
    Dim ClearCellStyle As New System.Windows.Forms.DataGridViewCellStyle()
#Region "Control event"
#Region "Form"
    Private Sub ucDelimiterWorkshop_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        columnCellStyle.BackColor = Color.Yellow
        identifierCellStyle.BackColor = Color.LightSteelBlue
        ClearCellStyle.BackColor = clearColor
        _parentForm = Me.ParentForm()
        clearColor = UnstructureFileConverterCommonFunc.EditorBackColor


    End Sub
#End Region
#Region "Combobox"
    Private Sub NumericKeyPressEvent(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PatternLineNoTextbox.KeyPress
        UnstructureFileConverterCommonFunc.NumericTextBox_Validate(sender, e, False, 10, 0, True)
    End Sub
    Private Sub PatternTextChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PatternTextBox.TextChanged, PatternLineNoTextbox.TextChanged
        If _stillFillData Then Exit Sub
        PaintIdentifier()
        Try
            _parentForm.RefreshPropertyBox()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub NumericOneValidate(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PatternLineNoTextbox.Validating
        NumericTextBox_DefaultValue(sender, 0)
    End Sub
    Private Sub TextQualifierChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextQualifierComboBox.TextChanged
        If isStillFillData Then Exit Sub
        Me.Cursor = Cursors.WaitCursor
        Me.Refresh()
        If sender.Text.ToString.IndexOf("{") = 0 And sender.Text.ToString.IndexOf("}") = sender.Text.ToString.Length - 1 Then

            BuildRawDataTable()
            FillDataIntoDataTable()
            DoingDelimiter()
            FillDataIntoGrid()
            is_Remove_First_Line = False
            FirstLineChange()
            RefreshIdentifierColumnCollection()

        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub DelimiterTextChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DelimiterComboBox.TextChanged
        If isStillFillData Then Exit Sub
        Me.Cursor = Cursors.WaitCursor
        Me.Refresh()
        If sender.Text.ToString.IndexOf("{") = 0 And sender.Text.ToString.IndexOf("}") = sender.Text.ToString.Length - 1 Then

            BuildRawDataTable()
            FillDataIntoDataTable()
            FillDataIntoGrid()
            is_Remove_First_Line = False
            FirstLineChange()
            RefreshIdentifierColumnCollection()

        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SheetNoChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SheetComboBox.SelectedIndexChanged
        If isStillFillData = True Then Exit Sub
        LoadDataFromSheet()
        FillDataIntoGrid()
        is_Remove_First_Line = False
        FirstLineChange()
    End Sub
#End Region
#Region "Check Box"
    Private Sub IncludeFirstLineChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncludeFirstLineCheckBox.CheckedChanged
        FirstLineChange()
    End Sub
#End Region
#End Region
#Region "Custom Function"
    Dim is_Remove_First_Line As Boolean = False
    Private Sub FirstLineChange()
        If notNeedToCheck Then Exit Sub
        With editDataTable
            Select Case IncludeFirstLineCheckBox.Checked
                Case False
                    If .Rows.Count = 0 Then Exit Sub
                    For columnIndex As Integer = 0 To .Columns.Count - 1
                        .Columns(columnIndex).ColumnName = (columnIndex + 1).ToString() + " : " + .Rows(0)(columnIndex).ToString
                    Next
                    .Rows.RemoveAt(0)
                    is_Remove_First_Line = True
                Case True

                    Dim firstRow As DataRow = .NewRow()
                    For columnIndex As Integer = 0 To .Columns.Count - 1
                        Dim headerName As String = .Columns(columnIndex).ColumnName
                        Dim colNo As Integer = UnstructureFileConverterCommonFunc.IntegerParse(headerName.Split(New Char() {":"})(0))
                        Dim temp As String = colNo.ToString
                        Try
                            firstRow(columnIndex) = headerName.Replace(colNo.ToString + " : ", "")
                        Catch ex As Exception

                        End Try

                        .Columns(columnIndex).ColumnName = "Column No:" + (columnIndex + 1).ToString()
                    Next
                    If is_Remove_First_Line = True Then
                        .Rows.InsertAt(firstRow, 0)
                    End If
            End Select
            FillDataIntoGrid()
            PaintIdentifier()
        End With
    End Sub
    Private Sub CleanScreen()
        With EditWorkShopGirdView
            For index As Integer = 0 To .Rows.Count - 1
                .Rows(index).DefaultCellStyle.BackColor = clearColor
            Next
        End With
    End Sub
    Public Sub PaintIdentifier()
        With EditWorkShopGirdView
            If _stillFillData Then Exit Sub
            Dim patternColumnNo As Integer = UnstructureFileConverterCommonFunc.IntegerParse(PatternLineNoTextbox.Text)
            identifier.IDString = PatternTextBox.Text
            identifier.LineOffset = patternColumnNo

            'If patternColumnNo < 1 Then
            '    CleanScreen()
            '    Exit Sub
            'End If
          
            Dim patternText As String = PatternTextBox.Text
            Try
                'Start identifier paint
                '.RowsDefaultCellStyle.BackColor = clearColor
                '.Refresh()
                For index As Integer = 0 To .Rows.Count - 1
                    For columnIndex As Integer = 0 To .ColumnCount - 1
                        .Rows(index).Cells(columnIndex).Style.BackColor = clearColor
                    Next

                Next
                For index As Integer = 0 To .Rows.Count - 1
                    If patternColumnNo > 0 Then
                        Dim tempColumnData As String
                        If .Rows(index).Cells(patternColumnNo - 1).Value Is DBNull.Value Then
                            tempColumnData = ""
                        Else
                            tempColumnData = .Rows(index).Cells(patternColumnNo - 1).Value
                        End If

                        If CheckCorrectLine(tempColumnData, patternText, index) Then
                            .Rows(index).DefaultCellStyle.BackColor = identifierColor
                            If column IsNot Nothing Then
                                For columnIndex As Integer = 0 To .ColumnCount - 1
                                    .Rows(index + column.ColLineNo).Cells(columnIndex).Style.BackColor = clearColor
                                    If column.ColNo - 1 = columnIndex Then
                                        If .Rows(index).Cells(columnIndex).Style.BackColor <> columnColor Then
                                            .Rows(index).Cells(columnIndex).Style.BackColor = identifierColor
                                        End If
                                        .Rows(index + column.ColLineNo).Cells(columnIndex).Style.BackColor = columnColor
                                    Else
                                        If .Rows(index).Cells(columnIndex).Style.BackColor <> columnColor Then
                                            .Rows(index).Cells(columnIndex).Style.BackColor = identifierColor
                                        End If

                                    End If
                                Next
                            Else
                                For columnIndex As Integer = 0 To .ColumnCount - 1
                                    .Rows(index).Cells(columnIndex).Style.BackColor = identifierColor
                                Next
                            End If
                        Else
                            For columnIndex As Integer = 0 To .ColumnCount - 1
                                If .Rows(index).Cells(columnIndex).Style.BackColor <> columnColor Then
                                    .Rows(index).Cells(columnIndex).Style.BackColor = clearColor
                                End If

                            Next
                        End If
                    Else
                        For columnIndex As Integer = 0 To .ColumnCount - 1
                            If column IsNot Nothing Then
                                If column.ColNo - 1 = columnIndex Then
                                    .Rows(index).Cells(columnIndex).Style.BackColor = columnColor
                                Else
                                    .Rows(index).Cells(columnIndex).Style.BackColor = identifierColor
                                End If
                            Else
                                .Rows(index).Cells(columnIndex).Style.BackColor = identifierColor
                            End If
                        Next
                    End If
                Next
                'end identifier paint
               
            Catch ex As Exception
            End Try
        End With
        EditWorkShopGirdView.ClearSelection()
        '_parentForm.RefreshPropertyBox()

    End Sub

    Public Function DoingLoadingSheetCombo() As Boolean

        Dim sheetNameDataTable As New DataTable
        Dim result As Boolean = False

        Try

            sheetNameDataTable = GetWorksheetNames(unstructuredFileConverter.SourceFile)
            SheetComboBox.Items.Clear()
            For Each sheetNameRow As DataRow In sheetNameDataTable.Rows
                'Updated By Hein 20090824
                SheetComboBox.Items.Add(sheetNameRow(0))
            Next
            If SheetComboBox.Items.Count > 0 Then SheetComboBox.SelectedIndex = 0
            result = True
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            result = False
        End Try


        Return result
    End Function

    Public Function GetWorksheetNames(ByVal excelFile As String) As DataTable
        'Dim excelConnectionString As String
        Dim excelConnection As OleDbConnection

        Dim tempWorksheetDataTable As DataTable
        Dim tempWorksheetDataRow As DataRow
        Dim worksheetDataTable As DataTable
        Dim worksheetDataRow As DataRow

        Dim restrictions As String() = {Nothing, Nothing, Nothing, "TABLE"}

        worksheetDataTable = New DataTable()
        worksheetDataTable.Columns.Add("WorksheetName")

        If Not System.IO.File.Exists(excelFile) Then
            Throw New MagicException(String.Format("Excel File '{0}' doesnot exists. Please check the file path.", excelFile))
        End If

        'Test Connection
        Try

            excelConnection = New System.Data.OleDb.OleDbConnection(IIf(excelFile.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase), _
                String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended properties=""Excel 12.0 Xml;HDR=No""", excelFile), _
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)))
            excelConnection.Open()
        Catch ex As OleDbException
            Throw New Exception("Your system doesnot support Excel 2007!")
        End Try

        'excelConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)
        'excelConnection = New OleDbConnection(excelConnectionString)
        'Try
        '    excelConnection.Open()

        'Catch ex As Exception
        '    If ex.Message = "External table is not in the expected format." Then
        '        MessageBox.Show("The source file is not valid excel file")
        '    End If
        '    Throw ex
        'End Try

        Try
            tempWorksheetDataTable = excelConnection.GetSchema("Tables", restrictions)
            worksheetDataTable.Rows.Clear()
            For Each tempWorksheetDataRow In tempWorksheetDataTable.Rows

                If (Not tempWorksheetDataRow("TABLE_NAME").ToString().Replace("'", "").EndsWith("$")) Then
                Else
                    worksheetDataRow = worksheetDataTable.NewRow()

                    If tempWorksheetDataRow("TABLE_NAME").ToString.StartsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(0, 1)
                    End If

                    If tempWorksheetDataRow("TABLE_NAME").ToString.EndsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(tempWorksheetDataRow("TABLE_NAME").ToString.Length - 1, 1)
                    End If

                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Replace("''", "'")
                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Remove(tempWorksheetDataRow("TABLE_NAME").ToString().Length - 1, 1)
                    worksheetDataRow("WorksheetName") = tempWorksheetDataRow("TABLE_NAME")
                    worksheetDataTable.Rows.Add(worksheetDataRow)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error on getting Worksheet Names(GetWorksheetNames) : {0}", ex.Message.ToString))
        Finally
            If excelConnection.State = ConnectionState.Open Then
                excelConnection.Close()
            End If
            excelConnection = Nothing
        End Try
        Return worksheetDataTable
    End Function
    'Private Function LoadDataFromSheet()
    '    Dim result As Boolean = False
    '    ExcelConncetionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", unstructuredFileConverter.SourceFile)
    '    ExcelConncetion = New OleDbConnection(ExcelConncetionString)
    '    Dim excelDataAdapter As New OleDbDataAdapter
    '    Try
    '        editDataTable = New DataTable()

    '        ExcelCommand = New OleDbCommand(String.Format("Select * From [{0}]", SheetComboBox.Text & "$"), ExcelConncetion)
    '        ExcelConncetion.Open()
    '        excelDataAdapter.SelectCommand = ExcelCommand
    '        excelDataAdapter.Fill(editDataTable)
    '        For Each vCol As DataColumn In editDataTable.Columns
    '            Dim myDC As DataColumn = New DataColumn(vCol.ColumnName, System.Type.GetType("System.String"))
    '        Next
    '    Catch ex As Exception
    '        result = False
    '    Finally
    '        If ExcelConncetion.State = ConnectionState.Open Then
    '            ExcelConncetion.Close()
    '        End If
    '    End Try
    '    Return result
    'End Function
    Private Function LoadDataFromSheet()
        Dim result As Boolean = False
        Try
            'Amended by Regina on 27 Aug 2009 - help to speed up loading of Excel using ExcelReaderInterop class
            'editDataTable = GetDataTableFromExcelWorkSheet(unstructuredFileConverter.SourceFile, SheetComboBox.Text)
            editDataTable = Common.ExcelReaderInterop.ExcelOpenSpreadsheets(unstructuredFileConverter.SourceFile, SheetComboBox.Text)
        Catch ex As Exception
            result = False
            editDataTable = New DataTable
        Finally
            If ExcelConncetion IsNot Nothing AndAlso ExcelConncetion.State = ConnectionState.Open Then
                ExcelConncetion.Close()
            End If
        End Try
        Return result
    End Function
    Dim notNeedToCheck As Boolean = False
    Public Function ReadSourceFile() As Boolean
        Me.Cursor = Cursors.WaitCursor
        'Design Change depent on Type
        DelimiterLabel.Visible = (unstructuredFileConverter.SourceFileType = "D")
        DelimiterComboBox.Visible = (unstructuredFileConverter.SourceFileType = "D")
        TextQualifierLabel.Visible = (unstructuredFileConverter.SourceFileType = "D")
        TextQualifierComboBox.Visible = (unstructuredFileConverter.SourceFileType = "D")
        SheetLabel.Visible = (unstructuredFileConverter.SourceFileType = "E")
        SheetComboBox.Visible = (unstructuredFileConverter.SourceFileType = "E")
        Me.Refresh()
        Select Case unstructuredFileConverter.SourceFileType
            Case "E"
                isStillFillData = True
                If DoingLoadingSheetCombo() = False Then Exit Function
                If unstructuredFileConverter.Delimiter IsNot Nothing Then
                    SheetComboBox.Text = unstructuredFileConverter.Delimiter
                End If
                LoadDataFromSheet()
                isStillFillData = False
            Case "D"
                isStillFillData = True
                If unstructuredFileConverter.Delimiter Is Nothing Then
                    DelimiterComboBox.SelectedIndex = 1
                Else
                    DelimiterComboBox.Text = unstructuredFileConverter.Delimiter
                End If
                If unstructuredFileConverter.TextQualifier Is Nothing Then
                    TextQualifierComboBox.SelectedIndex = 0
                Else
                    TextQualifierComboBox.Text = unstructuredFileConverter.TextQualifier
                End If

                isStillFillData = False
                Try
                    'Added by Hein 20090826
                    Dim encoding As System.Text.Encoding = HelperModule.GetFileEncoding(unstructuredFileConverter.SourceFile)
                    'Dim sReader As IO.StreamReader = New IO.StreamReader(unstructuredFileConverter.SourceFile)
                    Dim sReader As IO.StreamReader = New IO.StreamReader(unstructuredFileConverter.SourceFile, encoding)
                    RawTextRickTextBox.Text = sReader.ReadToEnd().Replace(vbCr + vbCrLf, vbCr)
                Catch ex As Exception
                    Return False
                End Try
                BuildRawDataTable()
                FillDataIntoDataTable()
        End Select
        FillDataIntoGrid()
        notNeedToCheck = True
        IncludeFirstLineCheckBox.Checked = (unstructuredFileConverter.ImportFirstLine = "Y")
        notNeedToCheck = False
        FirstLineChange()

        Me.Cursor = Cursors.Default
        Return True

    End Function
    Public Sub RefreshPaintScreen()
        SetObjectPoint(identifier, column, identifierCollection, columnCollection, unstructuredFileConverter)
    End Sub
    Public Sub DoingDelimiter()
        If RawTextRickTextBox.Lines.Length = 0 Then Exit Sub
        delimiterName = DelimiterComboBox.Text
        Select Case delimiterName
            Case "{space}"
                delimiterCode = " "
            Case "{tab}"
                delimiterCode = vbTab
            Case "{new line}"
                delimiterCode = vbNewLine
            Case "{,}"
                delimiterCode = ","
            Case delimiterName.IndexOf("{") <> -1 And delimiterName.IndexOf("}") <> -1
                delimiterCode = delimiterName.Replace("{", "").Replace("}", "")
            Case Else
                Exit Sub
        End Select
        If delimiterCode = "" Then Exit Sub
        BuildRawDataTable()
        FillDataIntoDataTable()
    End Sub
    Public Sub BuildRawDataTable()
        editDataTable.Columns.Clear()
        editDataTable.Rows.Clear()
        Dim firstLineText As String = RawTextRickTextBox.Lines(0)
        GetDelimiterCode()
        GetTextQualifierCode()
        Dim columnIdex() As String = unstructuredFileConverter.SplitString(firstLineText, delimiterCode, textQualifierCode, True)
        For index As Integer = 0 To columnIdex.Length - 1
            editDataTable.Columns.Add(index)
        Next
    End Sub
    Public Function GetDelimiterCode() As Boolean
        Select Case DelimiterComboBox.Text
            Case "{space}"
                delimiterCode = " "
            Case "{tab}"
                delimiterCode = vbTab
            Case "{new line}"
                delimiterCode = vbNewLine
            Case "{,}"
                delimiterCode = ","
            Case "{}"
                delimiterCode = "`"
            Case Else
                If DelimiterComboBox.Text.IndexOf("{") > -1 And DelimiterComboBox.Text.IndexOf("}") > -1 Then
                    delimiterCode = DelimiterComboBox.Text.Replace("{", "").Replace("}", "")
                Else
                    Return False
                End If
        End Select
        Return True
    End Function
    Public Function GetTextQualifierCode() As Boolean
        Select Case TextQualifierComboBox.Text
            Case "{""}"
                textQualifierCode = """"
            Case "{'}"
                textQualifierCode = "'"
            Case "{none}"
                textQualifierCode = ""
            Case Else
                If TextQualifierComboBox.Text.IndexOf("{") <> -1 And TextQualifierComboBox.Text.IndexOf("}") <> -1 Then
                    textQualifierCode = TextQualifierComboBox.Text.Replace("{", "").Replace("}", "")
                Else
                    Return False
                End If
        End Select
        Return True
    End Function
    Public Sub DoTextQualifier(ByRef sourceParamenter As String)
        If GetDelimiterCode() = False Then Exit Sub
        If GetTextQualifierCode() = False Then Exit Sub
        Dim rawData() As String = sourceParamenter.Split(textQualifierCode)
        For index As Integer = 1 To rawData.Length - 1 Step 2
            sourceParamenter = sourceParamenter.Replace(textQualifierCode + rawData(index) + textQualifierCode, rawData(index).Replace(delimiterCode, textQualifierReplaceCode))
        Next
    End Sub
    Public Sub FillDataIntoDataTable()
       
        GetDelimiterCode()
        GetTextQualifierCode()
        Dim LineString() As String = RawTextRickTextBox.Lines
        editDataTable.Rows.Clear()
        For lineIndex As Integer = 0 To LineString.Length - 1
            If lineIndex = 2000 Then Exit For
            Dim currentLineText As String = LineString(lineIndex)
            'If currentLineText.Trim <> "" Then
            editDataTable.Rows.Add()
            Dim newRowIndex As Integer = editDataTable.Rows.Count - 1
            Dim columnString() As String = unstructuredFileConverter.SplitString(currentLineText, delimiterCode, textQualifierCode, True)
            For Columnndex As Integer = 0 To columnString.Length - 1
                With editDataTable
                    Try
                        If Columnndex = editDataTable.Columns.Count Then
                            editDataTable.Columns.Add()
                        End If
                        .Rows(newRowIndex)(Columnndex) = columnString(Columnndex)
                    Catch ex As Exception
                    End Try
                End With
            Next
            ' End If
        Next
        For columnIndex As Integer = 0 To editDataTable.Columns.Count - 1
            editDataTable.Columns(columnIndex).ColumnName = "Column No:" + (columnIndex + 1).ToString()
        Next
    End Sub
    Public Sub FillDataIntoGrid()
        EditWorkShopGirdView.DataSource = editDataTable
        EditWorkShopGirdView.RowsDefaultCellStyle.BackColor = Color.Gainsboro
        For Each column As DataGridViewColumn In EditWorkShopGirdView.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub
    Dim _stillFillData As Boolean = True
    Public Sub SetObjectPoint(ByVal identifierParameter As Identifier, _
    ByVal columnParameter As Column, _
    ByVal identiferCollectionParameter As IdentifierCollection, _
   ByVal columnCollectionParameter As ColumnCollection, _
   ByVal unstructuredFileConverterParameter As UnstructuredFileConverter.UnstructuredFileConverter)
        identifier = identifierParameter
        column = columnParameter
        identifierCollection = identiferCollectionParameter
        columnCollection = columnCollectionParameter
        unstructuredFileConverter = unstructuredFileConverterParameter

        If identifier Is Nothing Then Exit Sub
        _stillFillData = True
        PatternTextBox.Text = identifier.IDString
        PatternLineNoTextbox.Text = identifier.LineOffset
        _stillFillData = False
        PaintIdentifier()
        PatternTextBox.Enabled = (columnParameter Is Nothing And identifier.IDNo IsNot Nothing)
        PatternLineNoTextbox.Enabled = (columnParameter Is Nothing And identifier.IDNo IsNot Nothing)

    End Sub
    Public Sub CloseEdit()
        identifier.IDString = PatternTextBox.Text
        identifier.LineOffset = PatternLineNoTextbox.Text
        Select Case unstructuredFileConverter.SourceFileType
            Case "D"
                unstructuredFileConverter.Delimiter = DelimiterComboBox.Text
                unstructuredFileConverter.TextQualifier = TextQualifierComboBox.Text
            Case "E"
                unstructuredFileConverter.Delimiter = SheetComboBox.Text
        End Select
        unstructuredFileConverter.ImportFirstLine = IIf(IncludeFirstLineCheckBox.Checked, "Y", "N")




    End Sub
    Private ctl As New TreeView
    Public Property TreeView1() As TreeView
        Get
            Return ctl
        End Get
        Set(ByVal value As TreeView)
            ctl = value
        End Set
    End Property

    Public Sub AutoCreateNew()
        Dim newIdentifier As Identifier = New Identifier()
        newIdentifier.IDString = ""
        newIdentifier.LineOffset = "1"
        newIdentifier.IDType = "D"
        newIdentifier.IDNo = _parentForm.GetNewIDNo("ID", "Identifier")
        identifierCollection.Add(newIdentifier)
        identifier = newIdentifier

        For index As Integer = 0 To EditWorkShopGirdView.ColumnCount - 1
            Dim tmpColumn As New Column()
            With tmpColumn
                .ColName = "Column" + (index + 1).ToString()
                .ColNo = index + 1
                .ColDataType = "T"
                .DataFormat = "Trim"
                .Repeat = "0"
                'Added by Hein 20090824
                .ColLineNo = "0"
                .IDNo = identifier.IDNo
            End With
            columnCollection.Add(tmpColumn)
        Next

    End Sub
    ''' <summary>
    ''' This function is used to automatically refresh the Identifier and Column Collection,if the delimiter/Text Qualifier is changed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub RefreshIdentifierColumnCollection()
        identifierCollection.Clear()
        columnCollection.Clear()
        TreeView1.Nodes(0).Nodes.Clear()
        TreeView1.Nodes(1).Nodes.Clear()

        Dim newIdentifier As Identifier = New Identifier()
        newIdentifier.IDString = ""
        newIdentifier.LineOffset = "1"
        newIdentifier.IDType = "D"
        newIdentifier.IDNo = _parentForm.GetNewIDNo("ID", "Identifier")

        identifierCollection.Add(newIdentifier)
        identifier = newIdentifier
        For index As Integer = 0 To EditWorkShopGirdView.ColumnCount - 1
            Dim tmpColumn As New Column()
            With tmpColumn
                .ColName = "Column" + (index + 1).ToString()
                .ColNo = index + 1
                .ColDataType = "T"
                .DataFormat = "Trim"
                .Repeat = "0"
                'Added by Hein 20090824
                .ColLineNo = "0"
                .IDNo = identifier.IDNo
            End With
            columnCollection.Add(tmpColumn)
        Next
        Dim IdentifierNode As TreeNode = TreeView1.Nodes("IdentifiersRootNode")
        For Each identifier As Identifier In unstructuredFileConverter.IdentifierCollection
            Dim childNode As TreeNode
            childNode = IdentifierNode.Nodes.Add(BuildTreeNodeString("Identifier", identifier))
        Next

        Dim ColumnNode As TreeNode = TreeView1.Nodes("ColumnsRootNode")
        For Each column As Column In unstructuredFileConverter.ColumnCollection
            Dim childNode As TreeNode
            childNode = ColumnNode.Nodes.Add(BuildTreeNodeString("Column", column))
        Next
    End Sub
    Private Function BuildTreeNodeString(ByVal nodeTypeParameter As String, ByVal nodeObject As Object, Optional ByVal newNameParameter As String = "") As String
        Dim _buildString As String = ""
        Dim _tmpName As String = ""
        Dim _treeNodeStringSperater As String = "||"
        Select Case nodeTypeParameter
            Case "Identifier"
                Dim tmpIdentifier As Identifier = nodeObject
                _tmpName = IIf(newNameParameter = "", tmpIdentifier.IDNo, newNameParameter)
                _buildString = tmpIdentifier.IDNo + " " + _treeNodeStringSperater + " [ " + tmpIdentifier.IDString.Trim() + " , " + _
                UnstructureFileConverterCommonFunc.GetIdentifierTypeName(tmpIdentifier.IDType) + " ]"
            Case "Column"
                Dim tmpColumn As Column = nodeObject
                _tmpName = IIf(newNameParameter = "", tmpColumn.ColName, newNameParameter)
                _buildString = tmpColumn.ColName + " " + _treeNodeStringSperater + " [ " + tmpColumn.IDNo + " , " + UnstructureFileConverterCommonFunc.GetColumnDataTypeName(tmpColumn.ColDataType) + " ] "
        End Select
        Return _buildString
    End Function
#End Region
#Region "Variable"
    Private _sourceFilePath As String
    Private _column As New Column
    Private _identifier As New Identifier
    Private _identifierCollection As New IdentifierCollection
    Private _columnCollection As New ColumnCollection
    Private _parentForm As Object
    Private _unstructuredFileConverter As UnstructuredFileConverter.UnstructuredFileConverter

#End Region
#Region " Public Property "
    Public Property unstructuredFileConverter() As UnstructuredFileConverter.UnstructuredFileConverter
        Get
            Return _unstructuredFileConverter
        End Get
        Set(ByVal value As UnstructuredFileConverter.UnstructuredFileConverter)
            _unstructuredFileConverter = value
        End Set
    End Property
    Public Property sourceFilePath() As String
        Get
            Return _sourceFilePath
        End Get
        Set(ByVal value As String)
            _sourceFilePath = value
        End Set
    End Property
    Public Property column() As Column
        Get
            Return _column
        End Get
        Set(ByVal value As Column)
            _column = value
        End Set
    End Property
    Public Property identifier() As Identifier
        Get
            Return _identifier
        End Get
        Set(ByVal value As Identifier)
            _identifier = value
        End Set
    End Property
    Public Property identifierCollection() As IdentifierCollection
        Get
            Return _identifierCollection
        End Get
        Set(ByVal value As IdentifierCollection)
            _identifierCollection = value
        End Set
    End Property
    Public Property columnCollection() As ColumnCollection
        Get
            Return _columnCollection
        End Get
        Set(ByVal value As ColumnCollection)
            _columnCollection = value
        End Set
    End Property
#End Region

    'ADDED BY HEIN 20090904
    Private Sub EditWorkShopGirdView_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles EditWorkShopGirdView.CellDoubleClick
        Try
            If column IsNot Nothing Then
                column.ColNo = e.ColumnIndex + 1
                Dim CurrentCell As DataGridViewCell = EditWorkShopGirdView.Rows(e.RowIndex).Cells(e.ColumnIndex)
                If CurrentCell.Style().BackColor <> identifierColor And CurrentCell.Style().BackColor <> columnColor Then
                    Dim vBackRowIndex As Integer = e.RowIndex - 1
                    Dim vLinePlus As Integer = 1
                    If vBackRowIndex >= 0 Then
                        Dim backCell As DataGridViewCell = EditWorkShopGirdView.Rows(vBackRowIndex).Cells(e.ColumnIndex)
                        While (backCell.Style().BackColor <> identifierColor And backCell.Style().BackColor <> columnColor And vBackRowIndex >= 0)
                            vLinePlus = vLinePlus + 1
                            vBackRowIndex = vBackRowIndex - 1
                            backCell = EditWorkShopGirdView.Rows(vBackRowIndex).Cells(e.ColumnIndex)
                        End While
                    End If
                    column.ColLineNo = vLinePlus
                Else
                    column.ColLineNo = 0
                End If
                _parentForm.RefreshPropertyBox()
                SetObjectPoint(identifier, column, identifierCollection, columnCollection, unstructuredFileConverter)
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
