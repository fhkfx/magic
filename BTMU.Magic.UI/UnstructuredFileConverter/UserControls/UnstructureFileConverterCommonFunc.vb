Imports BTMU.Magic.UnstructuredFileConverter
Imports System.IO
Imports System.Data
Imports BTMU.Magic.Common
Imports System.Data.OleDb

Public Module UnstructureFileConverterCommonFunc
    Public EditorBackColor As Color = Color.Gainsboro
    Dim IdentifierString As String
    Dim Identifier_Decode As String
    Public Sub NumericTextBox_Validate(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal AllowDecimal As Boolean, ByVal Precision As Integer, ByVal DecimalPoint As Integer, Optional ByVal AllowMinus As Boolean = False)
        Dim HandledAllow As Boolean = True
        Dim SenderObject As New System.Windows.Forms.TextBox
        SenderObject = CType(sender, System.Windows.Forms.TextBox)
        If SenderObject.SelectionLength = SenderObject.Text.Length Then
            SenderObject.Text = ""
        End If
        If IsNumeric(e.KeyChar) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 9 Or Asc(e.KeyChar) = 46 Then
            Dim _TempValue(2) As String
            _TempValue = SenderObject.Text.Split(".")

            If IsNumeric(e.KeyChar) = True Then
                If AllowDecimal = False Then
                    If _TempValue(0).Length = Precision Then
                        HandledAllow = False
                        GoTo ReturnValue
                    End If
                Else
                    If _TempValue(0).Length > Precision And SenderObject.Text.Contains(".") = False Then
                        HandledAllow = False
                        GoTo ReturnValue
                    End If
                    If _TempValue.Length = 2 Then
                        If _TempValue(1).Length > DecimalPoint Then
                            HandledAllow = False
                            GoTo ReturnValue
                        End If
                    End If
                End If
            End If
            If AllowDecimal = True And Asc(e.KeyChar) = 46 Then
                If SenderObject.Text.Contains(".") Then
                    HandledAllow = False
                    GoTo ReturnValue
                End If
            ElseIf Asc(e.KeyChar) = 46 Then
                HandledAllow = False
                GoTo ReturnValue
            End If
        ElseIf e.KeyChar = "-" And SenderObject.Text = "" And AllowMinus = True Then

        Else
            HandledAllow = False
            GoTo ReturnValue
        End If
        Exit Sub
ReturnValue:
        If HandledAllow = False Then
            e.Handled = False
            Beep()
            e.KeyChar = ""
        End If
    End Sub
    Public Sub NumericTextBox_DefaultValue(ByVal senderTextBoxParameter As TextBox, Optional ByVal defaultValueParameter As Integer = 0)
        Dim integerTemp As Integer
        If Not Integer.TryParse(senderTextBoxParameter.Text, integerTemp) Then
            integerTemp = defaultValueParameter
        ElseIf integerTemp < defaultValueParameter Then
            integerTemp = defaultValueParameter
        End If
        senderTextBoxParameter.Text = integerTemp
    End Sub
    Public Function IntegerParse(ByVal objectParameter As String) As Integer
        Dim tempInteger As Integer = 0
        Integer.TryParse(objectParameter, tempInteger)
        Return tempInteger
    End Function
    Public Function GetIdentifierTypeName(ByVal identifierTypeCodeParameter As String) As String
        Select Case identifierTypeCodeParameter
            Case "H"
                Return "Header"
            Case "D"
                Return "Detail"
            Case "T"
                Return "Trailer"
            Case "X"
                Return "Delete"
        End Select
        Return ""
    End Function
    Public Function GetColumnDataTypeName(ByVal columnDataTypeCodeParameter As String) As String
        Select Case columnDataTypeCodeParameter
            Case "T"
                Return "Text"
            Case "N"
                Return "Numeric"
            Case "D"
                Return "Date"
            Case "M"
                Return "Multiline"
        End Select
        Return ""
    End Function
    Public Function CheckCorrectLine(ByVal vSourceLine As String, ByVal patternTextParameter As String, ByVal lineIndexParameter As String) As Boolean
        Dim tempPatterntext As String = ""
        tempPatterntext = patternTextParameter
        If tempPatterntext.Trim = "" Then Return True

        Select Case tempPatterntext.Trim()
            Case "<EMPTY LINE>"
                Return (vSourceLine.Trim = "")
            Case "<NEW PAGE>"
                Return (vSourceLine.Trim = "^")
            Case Else
                If tempPatterntext.IndexOf("<LINE ") <> -1 Then
                    Try
                        Dim patternLineIndex() As String = tempPatterntext.Replace("<LINE ", "").Replace(">", "").Split(",")
                        If patternLineIndex(0).Trim = "" Then Return False
                        If lineIndexParameter + 1 = patternLineIndex(0) Then
                            Return True
                        ElseIf patternLineIndex.Length > 1 Then
                            Dim index As Integer = 1

                            Dim LineNo As Integer = Integer.Parse(lineIndexParameter) + 1
                            Dim FirstPoint As Integer = Integer.Parse(patternLineIndex(0))
                            Dim NextLevel As Integer = Integer.Parse(patternLineIndex(1))
                            If (Integer.Parse(lineIndexParameter) + 1 - Integer.Parse(patternLineIndex(0))) Mod Integer.Parse(patternLineIndex(1)) = 0 Then
                                Return True
                            Else
                                Return False
                            End If
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    'Updated By Hein 20090824
                    Dim pattern As String = tempPatterntext
                    If (pattern.Length > vSourceLine.Length) Then Return False

                    Dim strExpressionBuilder As New System.Text.StringBuilder
                    For Each c As Char In pattern.TrimEnd
                        Select Case c
                            Case "["
                                strExpressionBuilder.Append("[\[]")
                            Case "]"
                                strExpressionBuilder.Append("[\]]")
                            Case "\"
                                strExpressionBuilder.Append("[\\]")
                            Case "."
                                strExpressionBuilder.Append("[\.]")
                            Case " "
                                strExpressionBuilder.Append(".")
                            Case "^"
                                strExpressionBuilder.Append("[\^]")
                            Case "$"
                                strExpressionBuilder.Append("[\$]")
                            Case "|"
                                strExpressionBuilder.Append("[\|]")
                            Case "*"
                                strExpressionBuilder.Append("[\*]")
                            Case "+"
                                strExpressionBuilder.Append("[\+]")
                            Case "("
                                strExpressionBuilder.Append("[\(]")
                            Case ")"
                                strExpressionBuilder.Append("[\)]")
                            Case "#"
                                strExpressionBuilder.Append("[\d]")
                            Case "@"
                                strExpressionBuilder.Append("[\w]")
                            Case "_"
                                strExpressionBuilder.Append("[\s]")
                            Case "?"
                                strExpressionBuilder.Append("[\S]")
                            Case Else
                                strExpressionBuilder.Append(c)
                        End Select
                    Next
                    'IdentifierString = pattern.Trim.Replace("[", "[/[]").Replace("\", "[\\]").Replace(".", "[\.]").Replace(" ", ".").Replace("^", "[\^]"). _
                    ' Replace("$", "[\$]").Replace("|", "[\|]").Replace("*", "[\*]").Replace("+", "[\+]"). _
                    ' Replace("(", "[\(]").Replace(")", "[\)]").Replace("#", "[\d]").Replace("@", "[\w]").Replace("_", "[\s]").Replace("?", "\S").Replace("[/[]", "[\[]")
                    IdentifierString = strExpressionBuilder.ToString()
                    Dim tem As String = vSourceLine.Substring(0, pattern.TrimEnd.Length)
                    Return System.Text.RegularExpressions.Regex.IsMatch(vSourceLine.Substring(0, pattern.TrimEnd.Length), IdentifierString)
                  


                    'If vSourceLine.Trim = "" Then Return False
                    'IdentifierString = tempPatterntext
                    'Identifier_Decode = tempPatterntext.Replace(" ", "~")
                    'Dim SourceLine As String = vSourceLine.Replace(" ", "~")
                    'Dim Pattern As String = Identifier_Decode
                    'For vInd As Integer = 0 To Pattern.Length - 1
                    '    Select Case Pattern(vInd)
                    '        Case "~"
                    '            If vInd = Pattern.Length - 1 Then
                    '                SourceLine = SourceLine.Substring(0, vInd) + "~"
                    '            Else
                    '                SourceLine = SourceLine.Substring(0, vInd) + "~" + SourceLine.Substring(vInd + 1, SourceLine.Length - (vInd + 1))
                    '            End If
                    '        Case "#"
                    '            Dim vTestInteger As Integer
                    '            If SourceLine.Length - 1 < vInd Then
                    '                Return False
                    '            End If
                    '            If Integer.TryParse(SourceLine(vInd), vTestInteger) = False Then
                    '                Return False
                    '            Else
                    '                SourceLine = SourceLine.Substring(0, vInd) + "#" + SourceLine.Substring(vInd + 1, SourceLine.Length - (vInd + 1))
                    '            End If
                    '        Case "@"
                    '            Dim vTestInteger As Integer
                    '            If SourceLine.Length - 1 < vInd Then
                    '                Return False
                    '            End If
                    '            If (SourceLine(vInd) >= "A" And SourceLine(vInd) <= "Z") Or (SourceLine(vInd) >= "a" And SourceLine(vInd) <= "z") Then
                    '                SourceLine = SourceLine.Substring(0, vInd) + "@" + SourceLine.Substring(vInd + 1, SourceLine.Length - (vInd + 1))
                    '            Else
                    '                Return False
                    '            End If
                    '        Case "_"
                    '            If SourceLine.Length - 1 < vInd Then
                    '                Return False
                    '            End If
                    '            If SourceLine(vInd) = "~" Then
                    '                SourceLine = SourceLine.Substring(0, vInd) + "_" + SourceLine.Substring(vInd + 1, SourceLine.Length - (vInd + 1))
                    '            Else
                    '                Return False
                    '            End If
                    '        Case "?"
                    '            If SourceLine.Length - 1 < vInd Then
                    '                Return False
                    '            End If
                    '            If SourceLine(vInd) <> "~" Then
                    '                SourceLine = SourceLine.Substring(0, vInd) + "?" + SourceLine.Substring(vInd + 1, SourceLine.Length - (vInd + 1))
                    '            Else
                    '                Return False
                    '            End If
                    '        Case Else
                    '            If Pattern(vInd) <> SourceLine(vInd) Then
                    '                Return False
                    '            End If
                    '    End Select
                    'Next
                    'If Identifier_Decode = SourceLine.Substring(0, patternTextParameter.Length) Then
                    '    Return True
                    'Else
                    '    Return False
                    'End If
                End If

        End Select
    End Function
    Public Sub CloneUnstructuredFileConverter(ByVal BaseUnstructureFileConverter As UnstructuredFileConverter.UnstructuredFileConverter, _
 ByRef cloneUnstructureFileConverter As UnstructuredFileConverter.UnstructuredFileConverter)
        With cloneUnstructureFileConverter
        
            .SourceFileType = BaseUnstructureFileConverter.SourceFileType

            .CreatedDate = BaseUnstructureFileConverter.CreatedDate
            .DefinitionFile = BaseUnstructureFileConverter.DefinitionFile
            .Delimiter = BaseUnstructureFileConverter.Delimiter
            .DefinitionDesc = BaseUnstructureFileConverter.DefinitionDesc
            .ImportFirstLine = BaseUnstructureFileConverter.ImportFirstLine
            .SourceFile = BaseUnstructureFileConverter.SourceFile
            .TextQualifier = BaseUnstructureFileConverter.TextQualifier
            'Start to Clone Identifier
            For Each baseIdentifier As Identifier In BaseUnstructureFileConverter.IdentifierCollection
                Dim cloneIdentifer As New Identifier()
                With cloneIdentifer
                    .IDNo = baseIdentifier.IDNo
                    .IDString = baseIdentifier.IDString
                    .IDType = baseIdentifier.IDType
                    .LineOffset = baseIdentifier.LineOffset
                End With
                cloneUnstructureFileConverter.IdentifierCollection.Add(cloneIdentifer)
            Next
            'Start to Clone Column
            For Each baseColumn As Column In BaseUnstructureFileConverter.ColumnCollection
                Dim cloneColumn As New Column()
                With cloneColumn
                    .ColDataType = baseColumn.ColDataType
                    .ColLength = baseColumn.ColLength
                    .ColLineNo = baseColumn.ColLineNo
                    .ColName = baseColumn.ColName
                    .ColNo = baseColumn.ColNo
                    .DataFormat = baseColumn.DataFormat
                    .DelimColNo = baseColumn.DelimColNo
                    .Delimiter = baseColumn.Delimiter
                    .IDNo = baseColumn.IDNo
                    .MultiLineEnd = baseColumn.MultiLineEnd
                    .Repeat = baseColumn.Repeat
                    .Start = baseColumn.Start
                    .TextQualifier = baseColumn.TextQualifier
                End With
                cloneUnstructureFileConverter.ColumnCollection.Add(cloneColumn)
            Next


        End With
    End Sub
    Public Function CheckItChange(ByVal firstUnstructureFileConverter As UnstructuredFileConverter.UnstructuredFileConverter, _
 ByRef secondUnstructureFileConverter As UnstructuredFileConverter.UnstructuredFileConverter) As Boolean
        With firstUnstructureFileConverter
            If .DefinitionDesc <> secondUnstructureFileConverter.DefinitionDesc Then Return True
            If .Delimiter <> secondUnstructureFileConverter.Delimiter Then Return True
            If .ImportFirstLine <> secondUnstructureFileConverter.ImportFirstLine Then Return True
            If .SourceFile <> secondUnstructureFileConverter.SourceFile Then Return True
            If .TextQualifier <> secondUnstructureFileConverter.TextQualifier Then Return True
            'Check Identifier Collection
            If .IdentifierCollection.Count <> secondUnstructureFileConverter.IdentifierCollection.Count Then
                Return True
            Else
                For index As Integer = 0 To .IdentifierCollection.Count - 1
                    Dim firstIdentifier As Identifier = .IdentifierCollection(index)
                    Dim seconddentifier As Identifier = secondUnstructureFileConverter.IdentifierCollection(index)
                    With firstIdentifier
                        If .IDNo <> seconddentifier.IDNo Then Return True
                        If .IDString <> seconddentifier.IDString Then Return True
                        If .IDType <> seconddentifier.IDType Then Return True
                        If .LineOffset <> seconddentifier.LineOffset Then Return True
                    End With
                Next
            End If
            'Check Column Collection
            If .ColumnCollection.Count <> secondUnstructureFileConverter.ColumnCollection.Count Then
                Return True
            Else
                For index As Integer = 0 To .ColumnCollection.Count - 1
                    Dim firstColumn As Column = .ColumnCollection(index)
                    Dim secondColumn As Column = secondUnstructureFileConverter.ColumnCollection(index)
                    With firstColumn
                        If .ColDataType <> secondColumn.ColDataType Then Return True
                        If .ColLength <> secondColumn.ColLength Then Return True
                        If .ColLineNo <> secondColumn.ColLineNo Then Return True
                        If .ColName <> secondColumn.ColName Then Return True
                        If .ColNo <> secondColumn.ColNo Then Return True
                        If .DataFormat <> secondColumn.DataFormat Then Return True
                        If NothingToString(.DelimColNo) <> NothingToString(secondColumn.DelimColNo) Then Return True
                        If NothingToString(.Delimiter) <> NothingToString(secondColumn.Delimiter) Then Return True
                        If NothingToString(.IDNo) <> NothingToString(secondColumn.IDNo) Then Return True
                        If NothingToString(.MultiLineEnd) <> NothingToString(secondColumn.MultiLineEnd) Then Return True
                        If NothingToString(.Repeat) <> NothingToString(secondColumn.Repeat) Then Return True
                        If NothingToString(.Start) <> NothingToString(secondColumn.Start) Then Return True
                        If NothingToString(.TextQualifier) <> NothingToString(secondColumn.TextQualifier) Then Return True
                    End With
                Next
            End If
            Return False
        End With
    End Function
    Public Function NothingToString(ByVal parameter As String) As String
        Return IIf(parameter Is Nothing, "", parameter)
    End Function
    Public Function FileInUse(ByVal sFile As String) As Boolean

        Dim oRead As System.IO.StreamReader = Nothing

        Try
            oRead = File.OpenText(sFile)
            Return False
        Catch ex As Exception
            Return True
        Finally
            Try
                oRead.Close()
            Catch ex As Exception
            End Try
        End Try

    End Function

    'Amended by Regina on 27 Aug 2009: Change error message
    Public Function ValidateSourceFile(ByVal sourceFilePath As String, ByVal typeStringParameter As String) As Boolean
        Dim hasError As Boolean = False
        Dim errorMessage As String = ""
        Dim sourceFileInfo As FileInfo

        Try
            sourceFileInfo = New FileInfo(sourceFilePath)
            If sourceFileInfo.Exists = False And hasError = False Then
                errorMessage = vbNewLine + typeStringParameter + " does not exist !"
                hasError = True
            ElseIf FileInUse(sourceFileInfo.FullName) And hasError = False Then
                errorMessage = vbNewLine + typeStringParameter + " is used by another program !"
                hasError = True
            End If
            If hasError = False And typeStringParameter.StartsWith("D") Then
                Dim correctType As String = sourceFilePath.Substring(sourceFilePath.Length - 3, 3)
                If correctType.ToLower <> "mdt" Then
                    errorMessage = vbNewLine + "Definition File is not in correct file format !"
                    hasError = True
                End If
            End If
           
        Catch ex As Exception
            errorMessage = vbNewLine + typeStringParameter + " does not exist !"
            hasError = True
        
        End Try

        If hasError Then
            Dim errWindow As New BTMU.Magic.Common.ErrorWindow()
            errWindow.BriefErrorMessage = errorMessage
            errWindow.DetailedErrorMessage = sourceFilePath + errorMessage
            errWindow.ShowDialog()
            Return False
        End If

        Return True
    End Function

    Public Sub ShowErrorMsg(ByVal Header As String, ByVal Detail As String)
        Dim errWindow As New BTMU.Magic.Common.ErrorWindow()
        errWindow.BriefErrorMessage = Header
        errWindow.DetailedErrorMessage = Detail
        errWindow.ShowDialog()
    End Sub

    'Added by Hein 20090910
    Public Function CheckExcelFormat(ByVal excelFile As String) As Boolean

        Dim excelConnection As System.Data.OleDb.OleDbConnection

        'Test Connection
        Try
            excelConnection = New System.Data.OleDb.OleDbConnection(IIf(excelFile.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase), _
                String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended properties=""Excel 12.0 Xml;HDR=No""", excelFile), _
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)))
            excelConnection.Open()
            excelConnection.Close()
            Return True

        Catch ex As Exception

            Return False

        End Try

    End Function

    'Commented by Regina on 24 Aug 09 - Use Common.ExcelReaderInterop instead
    'Public Function GetDataTableFromExcelWorkSheet(ByVal ExcelPath As String, ByVal SheetName As String) As System.Data.DataTable
    '    Dim XlApp As New Excel.Application
    '    Dim XlWorkBook As Excel.Workbook
    '    Dim XlSheet As Excel.Sheets
    '    Dim XlWorkSheet As Excel.Worksheet
    '    Dim ResultTable As New System.Data.DataTable()
    '    XlWorkBook = XlApp.Workbooks.Open(ExcelPath)
    '    XlSheet = XlWorkBook.Worksheets

    '    Dim xlSheetIndex As Integer
    '    Dim xlIntRows As Integer
    '    Dim xlIntCols As Integer
    '    XlWorkBook = XlApp.Workbooks.Open(ExcelPath)
    '    XlSheet = XlWorkBook.Worksheets
    '    XlWorkSheet = CType(XlSheet(SheetName), Excel.Worksheet)
    '    xlSheetIndex = xlSheetIndex + 1
    '    xlIntRows = XlWorkSheet.UsedRange.Rows.Count
    '    xlIntCols = XlWorkSheet.UsedRange.Columns.Count


    '    'DataGridView2.ColumnCount = xlIntCols

    '    'Fetch values from the selected excel sheet

    '    Dim XlObj As Excel.Range

    '    Dim XlArray() As String

    '    ReDim XlArray(xlIntCols - 1)

    '    Dim TempCnt1 As Integer

    '    Dim TempCnt2 As Integer

    '    For TempCnt1 = 0 To xlIntCols - 1
    '        ResultTable.Columns.Add("Field" & (TempCnt1 + 1).ToString)
    '        'DataGridView2.Columns(TempCnt1).Name = "Field" & (TempCnt1 + 1).ToString
    '    Next

    '    For TempCnt1 = 1 To xlIntRows

    '        For TempCnt2 = 1 To xlIntCols
    '            XlObj = CType(XlWorkSheet.Cells(TempCnt1, TempCnt2), Excel.Range)
    '            XlArray(TempCnt2 - 1) = XlObj.Value
    '        Next
    '        ResultTable.Rows.Add(XlArray)
    '        'DataGridView2.Rows.Add(XlArray)
    '    Next
    '    XlWorkBook.Close()
    '    XlApp.Quit()
    '    'releaseObject(XlApp)
    '    'releaseObject(XlWorkBook)
    '    'releaseObject(XlWorkSheet)
    '    Return ResultTable
    'End Function
End Module


