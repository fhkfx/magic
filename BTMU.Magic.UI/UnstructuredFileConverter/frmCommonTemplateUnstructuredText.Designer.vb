<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonTemplateUnstructuredText
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonTemplateUnstructuredText))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.buttonIntermediaryFile = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.textIntermediaryFile = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.textDefinitionFile = New System.Windows.Forms.TextBox
        Me.textSourceFile = New System.Windows.Forms.TextBox
        Me.buttonBrowseSourceFile = New System.Windows.Forms.Button
        Me.buttonBrowseDefinitionFile = New System.Windows.Forms.Button
        Me.btnProceed = New System.Windows.Forms.Button
        Me.errorPvd = New BTMU.Magic.Common.ErrorProviderFixed(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.errorPvd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.buttonIntermediaryFile)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.textIntermediaryFile)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.textDefinitionFile)
        Me.GroupBox1.Controls.Add(Me.textSourceFile)
        Me.GroupBox1.Controls.Add(Me.buttonBrowseSourceFile)
        Me.GroupBox1.Controls.Add(Me.buttonBrowseDefinitionFile)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(449, 139)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Source File && Definition"
        '
        'buttonIntermediaryFile
        '
        Me.buttonIntermediaryFile.Location = New System.Drawing.Point(356, 93)
        Me.buttonIntermediaryFile.Name = "buttonIntermediaryFile"
        Me.buttonIntermediaryFile.Size = New System.Drawing.Size(87, 29)
        Me.buttonIntermediaryFile.TabIndex = 5
        Me.buttonIntermediaryFile.Text = "Browse"
        Me.buttonIntermediaryFile.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 14)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Intermediary File:"
        '
        'textIntermediaryFile
        '
        Me.textIntermediaryFile.Location = New System.Drawing.Point(101, 96)
        Me.textIntermediaryFile.Name = "textIntermediaryFile"
        Me.textIntermediaryFile.Size = New System.Drawing.Size(249, 20)
        Me.textIntermediaryFile.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 14)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Definition File:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 14)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Source File:"
        '
        'textDefinitionFile
        '
        Me.textDefinitionFile.AllowDrop = True
        Me.textDefinitionFile.Location = New System.Drawing.Point(101, 64)
        Me.textDefinitionFile.Name = "textDefinitionFile"
        Me.textDefinitionFile.ReadOnly = True
        Me.textDefinitionFile.Size = New System.Drawing.Size(249, 20)
        Me.textDefinitionFile.TabIndex = 2
        '
        'textSourceFile
        '
        Me.textSourceFile.AllowDrop = True
        Me.textSourceFile.Location = New System.Drawing.Point(101, 31)
        Me.textSourceFile.Name = "textSourceFile"
        Me.textSourceFile.ReadOnly = True
        Me.textSourceFile.Size = New System.Drawing.Size(249, 20)
        Me.textSourceFile.TabIndex = 0
        '
        'buttonBrowseSourceFile
        '
        Me.buttonBrowseSourceFile.Location = New System.Drawing.Point(356, 28)
        Me.buttonBrowseSourceFile.Name = "buttonBrowseSourceFile"
        Me.buttonBrowseSourceFile.Size = New System.Drawing.Size(87, 29)
        Me.buttonBrowseSourceFile.TabIndex = 1
        Me.buttonBrowseSourceFile.Text = "Browse"
        Me.buttonBrowseSourceFile.UseVisualStyleBackColor = True
        '
        'buttonBrowseDefinitionFile
        '
        Me.buttonBrowseDefinitionFile.Location = New System.Drawing.Point(356, 60)
        Me.buttonBrowseDefinitionFile.Name = "buttonBrowseDefinitionFile"
        Me.buttonBrowseDefinitionFile.Size = New System.Drawing.Size(87, 29)
        Me.buttonBrowseDefinitionFile.TabIndex = 3
        Me.buttonBrowseDefinitionFile.Text = "Browse"
        Me.buttonBrowseDefinitionFile.UseVisualStyleBackColor = True
        '
        'btnProceed
        '
        Me.btnProceed.Location = New System.Drawing.Point(193, 157)
        Me.btnProceed.Name = "btnProceed"
        Me.btnProceed.Size = New System.Drawing.Size(87, 40)
        Me.btnProceed.TabIndex = 1
        Me.btnProceed.Text = "&Proceed"
        Me.btnProceed.UseVisualStyleBackColor = True
        '
        'errorPvd
        '
        Me.errorPvd.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink
        Me.errorPvd.ContainerControl = Me
        '
        'frmCommonTemplateUnstructuredText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 202)
        Me.Controls.Add(Me.btnProceed)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonTemplateUnstructuredText"
        Me.ShowIcon = False
        Me.Text = "BTMU-MAGIC - Unstructured Text Definition (MA2030)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.errorPvd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents buttonBrowseSourceFile As System.Windows.Forms.Button
    Friend WithEvents buttonBrowseDefinitionFile As System.Windows.Forms.Button
    Friend WithEvents btnProceed As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents textDefinitionFile As System.Windows.Forms.TextBox
    Friend WithEvents textSourceFile As System.Windows.Forms.TextBox
    Friend WithEvents errorPvd As BTMU.Magic.Common.ErrorProviderFixed
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents textIntermediaryFile As System.Windows.Forms.TextBox
    Friend WithEvents buttonIntermediaryFile As System.Windows.Forms.Button
End Class
