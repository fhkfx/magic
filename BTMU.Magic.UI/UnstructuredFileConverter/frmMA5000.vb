Imports BTMU.Magic.UnstructuredFileConverter
Imports System.IO
Public Class frmMA5000
    '''MA5000 - Define existing definition file and source file for editing
#Region "Form Event"
#Region "Drag Drop"
    Private Sub DefinationFileDragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles DefinationFileTextBox.DragDrop
        Try
            Dim a As Array = e.Data.GetData(DataFormats.FileDrop)
            DefinationFileTextBox.Text = a.GetValue(0).ToString()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub DefinationFileDragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles DefinationFileTextBox.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub SourceFileDragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragDrop
        Try
            Dim a As Array = e.Data.GetData(DataFormats.FileDrop)
            SourceFileTextBox.Text = a.GetValue(0).ToString()
        Catch ex As Exception
        End Try

    End Sub
    Private Sub SourceFileDragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub DefinationFileChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefinationFileTextBox.TextChanged
        LoadFromTextBox()
    End Sub
    Private Sub SourceFileChnage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SourceFileTextBox.TextChanged
        btnPreview.Enabled = (DefinationFileTextBox.Text.Trim <> "" And SourceFileTextBox.Text.Trim <> "")
    End Sub
#End Region


#Region "Button Event"
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If ValidateSourceFile(DefinationFileTextBox.Text, "Definition File") = False Then Exit Sub
        If ValidateSourceFile(SourceFileTextBox.Text, "Source File") = False Then Exit Sub
        'Added by Hein 20090910
        If SourceFileTypeComboBox.Text = "Excel File" Then
            If CheckExcelFormat(SourceFileTextBox.Text) = False Then
                Dim errWindow As New BTMU.Magic.Common.ErrorWindow()
                errWindow.BriefErrorMessage = "Invalid excel format or the excel file is password-protected!"
                errWindow.DetailedErrorMessage = SourceFileTextBox.Text + vbNewLine + "Invalid excel format or the excel file is password-protected!"
                errWindow.ShowDialog()
                Exit Sub
            End If
        End If
        unstructuredFileConverter.DefinitionFile = DefinationFileTextBox.Text


        'Set Original One
        Dim originalUnstructureFileConverter As New UnstructuredFileConverter.UnstructuredFileConverter()
        CloneUnstructuredFileConverter(unstructuredFileConverter, originalUnstructureFileConverter)
        'Create editable xml 
        Dim editableXmlPath As String = unstructuredFileConverter.DefinitionFile + ".edit"
        Try
            System.IO.File.Delete(editableXmlPath)
        Catch ex As Exception
        End Try
        Try
            System.IO.File.Copy(unstructuredFileConverter.DefinitionFile, editableXmlPath)
        Catch ex As Exception
            ' Some time editableXmlPath is already exit
            System.IO.File.Delete(editableXmlPath)
            System.IO.File.Copy(unstructuredFileConverter.DefinitionFile, editableXmlPath)
        End Try
        unstructuredFileConverter.DefinitionFile = editableXmlPath
        Dim temp As String = DefinitionDescriptionTextBox.Text.Replace(vbNewLine, "^")
        unstructuredFileConverter.DefinitionDesc = temp
        unstructuredFileConverter.SourceFile = SourceFileTextBox.Text
        Dim newFrmMA5003 As New frmMA5003()
        newFrmMA5003.UserID = Me.UserID
        newFrmMA5003.definationFilePath = unstructuredFileConverter.DefinitionFile
        newFrmMA5003.unstructuredFileConverter = unstructuredFileConverter

        newFrmMA5003.originalUnstructuredFileConverter = originalUnstructureFileConverter
        Me.Visible = False
        Dim detailFormResult As DialogResult = newFrmMA5003.ShowDialog()
        Me.Visible = True
        Select Case detailFormResult
            Case Windows.Forms.DialogResult.Yes
                unstructuredFileConverter = newFrmMA5003.unstructuredFileConverter
                DefinationFileTextBox.Text = unstructuredFileConverter.DefinitionFile
                LoadFromTextBox()
            Case Windows.Forms.DialogResult.No
                LoadFromTextBox()
            Case Windows.Forms.DialogResult.OK
                unstructuredFileConverter = newFrmMA5003.unstructuredFileConverter
        End Select
        newFrmMA5003.Dispose()
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub btnDefinationFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefinationFile.Click
        If DefinationOpenFileDialog.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
            DefinationFileTextBox.Text = DefinationOpenFileDialog.FileName
        End If
    End Sub
    Private Sub btnSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSourceFile.Click
        If SourceOpenFileDialog.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
            SourceFileTextBox.Text = SourceOpenFileDialog.FileName
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        lblConvertMessage.Visible = True
        Me.Refresh()
        If ValidateSourceFile(DefinationFileTextBox.Text, "definition file") = False Then Exit Sub
        If ValidateSourceFile(SourceFileTextBox.Text, "source file") = False Then Exit Sub
        Dim _frmMA5002 As New frmMA5002()
        _frmMA5002.UserID = Me.UserID
        _frmMA5002.lblConvertMessage = lblConvertMessage
        _frmMA5002.DefinitionFile = unstructuredFileConverter.DefinitionFile
        _frmMA5002.SourceFile = unstructuredFileConverter.SourceFile
        _frmMA5002.ShowDialog()
    End Sub
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Me.Visible = False
        Dim _frmMA5001 As New frmMA5001()
        _frmMA5001.UserID = Me.UserID
        If _frmMA5001.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Dim newFrmMA5003 As New frmMA5003()
            newFrmMA5003.UserID = Me.UserID
            newFrmMA5003.definationFilePath = _frmMA5001.unstructuredFileConverter.DefinitionFile
            newFrmMA5003.unstructuredFileConverter = _frmMA5001.unstructuredFileConverter
            newFrmMA5003.originalUnstructuredFileConverter = Nothing
            Dim userResult As DialogResult = newFrmMA5003.ShowDialog()
            If DialogResult.Ignore = userResult Then
                unstructuredFileConverter = New UnstructuredFileConverter.UnstructuredFileConverter()
                DefinationFileTextBox.Text = ""
                DefinitionDescriptionTextBox.Text = ""
                SourceFileTextBox.Text = ""
            Else
                unstructuredFileConverter = newFrmMA5003.unstructuredFileConverter
                DefinationFileTextBox.Text = unstructuredFileConverter.DefinitionFile
            End If
        End If
        Me.Visible = True
    End Sub
#End Region
#End Region

#Region "Custom Function"

    Private Sub LoadFromTextBox()
        btnEdit.Enabled = (DefinationFileTextBox.Text.Trim <> "")
        If DefinationFileTextBox.Text.Trim = "" Then Exit Sub
        Try


            Dim DefinitionPath As String = DefinationFileTextBox.Text
            unstructuredFileConverter.LoadFromFile(DefinitionPath)
            unstructuredFileConverter.DefinitionFile = DefinitionPath
            DefinitionDescriptionTextBox.Text = unstructuredFileConverter.DefinitionDesc.Replace("^", vbNewLine)
            SourceFileTextBox.Text = unstructuredFileConverter.SourceFile
            Select Case unstructuredFileConverter.SourceFileType
                Case "E"
                    SourceFileTypeComboBox.Text = "Excel File"
                Case "D"
                    SourceFileTypeComboBox.Text = "Delimited File"
                Case "F"
                    SourceFileTypeComboBox.Text = "Fixed Width File"
            End Select
        Catch ex As Exception
            ' File Not found Error
            SourceFileTypeComboBox.Text = "Fixed Width File"
            DefinitionDescriptionTextBox.Text = ""
            SourceFileTextBox.Text = ""
        End Try
    End Sub
#End Region

#Region "Variable"
    Private _unstructuredFileConverter As New BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter()
    Private _UserID As String
#End Region

#Region "Public Property "
    Public Property unstructuredFileConverter() As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter
        Get
            Return _unstructuredFileConverter
        End Get
        Set(ByVal value As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter)
            _unstructuredFileConverter = value
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property
#End Region

    'Amended by Regina on 27 Aug 09 - Pass in user name from Login form to retrieve the user id
    Private Sub frmMA5000_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UserID = frmLogin.GsUserName 'For Actual Use
        'UserID = "SUPER" 'For Testing
    End Sub
End Class