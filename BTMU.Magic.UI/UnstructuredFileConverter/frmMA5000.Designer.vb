<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMA5000
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblConvertMessage = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnSourceFile = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.SourceFileTypeLabel = New System.Windows.Forms.Label
        Me.SourceFileTypeComboBox = New System.Windows.Forms.ComboBox
        Me.DefinitionDescriptionTextBox = New System.Windows.Forms.TextBox
        Me.DefinitionDescriptionLabel = New System.Windows.Forms.Label
        Me.btnDefinationFile = New System.Windows.Forms.Button
        Me.DefinationFileTextBox = New System.Windows.Forms.TextBox
        Me.DefinationFileLabel = New System.Windows.Forms.Label
        Me.SourceFileTextBox = New System.Windows.Forms.TextBox
        Me.SourceFileLabel = New System.Windows.Forms.Label
        Me.DefinationOpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.SourceOpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblConvertMessage)
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Controls.Add(Me.btnPreview)
        Me.GroupBox1.Controls.Add(Me.btnEdit)
        Me.GroupBox1.Controls.Add(Me.btnNew)
        Me.GroupBox1.Controls.Add(Me.btnSourceFile)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.SourceFileTextBox)
        Me.GroupBox1.Controls.Add(Me.SourceFileLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(832, 253)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Existing Definition Setting"
        '
        'lblConvertMessage
        '
        Me.lblConvertMessage.AutoSize = True
        Me.lblConvertMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConvertMessage.ForeColor = System.Drawing.Color.Red
        Me.lblConvertMessage.Location = New System.Drawing.Point(438, 219)
        Me.lblConvertMessage.Name = "lblConvertMessage"
        Me.lblConvertMessage.Size = New System.Drawing.Size(272, 16)
        Me.lblConvertMessage.TabIndex = 3
        Me.lblConvertMessage.Text = "Converting in progress. . .  Please wait"
        Me.lblConvertMessage.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(324, 208)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 40)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Enabled = False
        Me.btnPreview.Location = New System.Drawing.Point(227, 208)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 40)
        Me.btnPreview.TabIndex = 8
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Enabled = False
        Me.btnEdit.Location = New System.Drawing.Point(130, 208)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(87, 40)
        Me.btnEdit.TabIndex = 7
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(33, 208)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(87, 40)
        Me.btnNew.TabIndex = 6
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnSourceFile
        '
        Me.btnSourceFile.Location = New System.Drawing.Point(716, 176)
        Me.btnSourceFile.Name = "btnSourceFile"
        Me.btnSourceFile.Size = New System.Drawing.Size(87, 29)
        Me.btnSourceFile.TabIndex = 5
        Me.btnSourceFile.Text = "Browse"
        Me.btnSourceFile.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SourceFileTypeLabel)
        Me.GroupBox2.Controls.Add(Me.SourceFileTypeComboBox)
        Me.GroupBox2.Controls.Add(Me.DefinitionDescriptionTextBox)
        Me.GroupBox2.Controls.Add(Me.DefinitionDescriptionLabel)
        Me.GroupBox2.Controls.Add(Me.btnDefinationFile)
        Me.GroupBox2.Controls.Add(Me.DefinationFileTextBox)
        Me.GroupBox2.Controls.Add(Me.DefinationFileLabel)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 17)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(814, 156)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'SourceFileTypeLabel
        '
        Me.SourceFileTypeLabel.AutoSize = True
        Me.SourceFileTypeLabel.Enabled = False
        Me.SourceFileTypeLabel.Location = New System.Drawing.Point(6, 128)
        Me.SourceFileTypeLabel.Name = "SourceFileTypeLabel"
        Me.SourceFileTypeLabel.Size = New System.Drawing.Size(91, 14)
        Me.SourceFileTypeLabel.TabIndex = 6
        Me.SourceFileTypeLabel.Text = "Source File Type:"
        '
        'SourceFileTypeComboBox
        '
        Me.SourceFileTypeComboBox.Enabled = False
        Me.SourceFileTypeComboBox.FormattingEnabled = True
        Me.SourceFileTypeComboBox.Location = New System.Drawing.Point(121, 125)
        Me.SourceFileTypeComboBox.Name = "SourceFileTypeComboBox"
        Me.SourceFileTypeComboBox.Size = New System.Drawing.Size(184, 22)
        Me.SourceFileTypeComboBox.TabIndex = 3
        '
        'DefinitionDescriptionTextBox
        '
        Me.DefinitionDescriptionTextBox.Location = New System.Drawing.Point(121, 42)
        Me.DefinitionDescriptionTextBox.MaxLength = 500
        Me.DefinitionDescriptionTextBox.Multiline = True
        Me.DefinitionDescriptionTextBox.Name = "DefinitionDescriptionTextBox"
        Me.DefinitionDescriptionTextBox.Size = New System.Drawing.Size(580, 76)
        Me.DefinitionDescriptionTextBox.TabIndex = 2
        '
        'DefinitionDescriptionLabel
        '
        Me.DefinitionDescriptionLabel.AutoSize = True
        Me.DefinitionDescriptionLabel.Location = New System.Drawing.Point(6, 45)
        Me.DefinitionDescriptionLabel.Name = "DefinitionDescriptionLabel"
        Me.DefinitionDescriptionLabel.Size = New System.Drawing.Size(111, 14)
        Me.DefinitionDescriptionLabel.TabIndex = 3
        Me.DefinitionDescriptionLabel.Text = "Definition Description:"
        '
        'btnDefinationFile
        '
        Me.btnDefinationFile.Location = New System.Drawing.Point(707, 10)
        Me.btnDefinationFile.Name = "btnDefinationFile"
        Me.btnDefinationFile.Size = New System.Drawing.Size(87, 29)
        Me.btnDefinationFile.TabIndex = 1
        Me.btnDefinationFile.Text = "Browse"
        Me.btnDefinationFile.UseVisualStyleBackColor = True
        '
        'DefinationFileTextBox
        '
        Me.DefinationFileTextBox.AllowDrop = True
        Me.DefinationFileTextBox.Location = New System.Drawing.Point(121, 14)
        Me.DefinationFileTextBox.MaxLength = 100
        Me.DefinationFileTextBox.Name = "DefinationFileTextBox"
        Me.DefinationFileTextBox.Size = New System.Drawing.Size(580, 20)
        Me.DefinationFileTextBox.TabIndex = 0
        '
        'DefinationFileLabel
        '
        Me.DefinationFileLabel.AutoSize = True
        Me.DefinationFileLabel.Location = New System.Drawing.Point(6, 17)
        Me.DefinationFileLabel.Name = "DefinationFileLabel"
        Me.DefinationFileLabel.Size = New System.Drawing.Size(73, 14)
        Me.DefinationFileLabel.TabIndex = 0
        Me.DefinationFileLabel.Text = "Definition File:"
        '
        'SourceFileTextBox
        '
        Me.SourceFileTextBox.AllowDrop = True
        Me.SourceFileTextBox.Location = New System.Drawing.Point(130, 180)
        Me.SourceFileTextBox.MaxLength = 100
        Me.SourceFileTextBox.Name = "SourceFileTextBox"
        Me.SourceFileTextBox.Size = New System.Drawing.Size(580, 20)
        Me.SourceFileTextBox.TabIndex = 4
        '
        'SourceFileLabel
        '
        Me.SourceFileLabel.AutoSize = True
        Me.SourceFileLabel.Location = New System.Drawing.Point(15, 183)
        Me.SourceFileLabel.Name = "SourceFileLabel"
        Me.SourceFileLabel.Size = New System.Drawing.Size(64, 14)
        Me.SourceFileLabel.TabIndex = 7
        Me.SourceFileLabel.Text = "Source File:"
        '
        'DefinationOpenFileDialog
        '
        Me.DefinationOpenFileDialog.Filter = "Definition file (*.mdt)|*.mdt"
        Me.DefinationOpenFileDialog.InitialDirectory = "C:\Program Files\BTMU MAGIC 2\Definition\"
        Me.DefinationOpenFileDialog.Title = "Definition File"
        '
        'SourceOpenFileDialog
        '
        Me.SourceOpenFileDialog.Filter = "All file (*.*)|*.*"
        Me.SourceOpenFileDialog.InitialDirectory = "C:\Program Files\BTMU MAGIC 2\Sample Source\"
        Me.SourceOpenFileDialog.Title = "Source File"
        '
        'frmMA5000
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 272)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmMA5000"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BTMU-MAGIC - Unstructured File Converter (MA5000)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DefinitionDescriptionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DefinitionDescriptionLabel As System.Windows.Forms.Label
    Friend WithEvents btnDefinationFile As System.Windows.Forms.Button
    Friend WithEvents DefinationFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DefinationFileLabel As System.Windows.Forms.Label
    Friend WithEvents SourceFileTypeLabel As System.Windows.Forms.Label
    Friend WithEvents SourceFileTypeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents btnSourceFile As System.Windows.Forms.Button
    Friend WithEvents SourceFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SourceFileLabel As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents DefinationOpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SourceOpenFileDialog As System.Windows.Forms.OpenFileDialog
    Public WithEvents lblConvertMessage As System.Windows.Forms.Label
End Class
