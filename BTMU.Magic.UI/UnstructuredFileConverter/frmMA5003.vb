Imports BTMU.Magic.UnstructuredFileConverter
Imports BTMU.Magic.Common
Imports System.IO

Public Class frmMA5003
    '''MA5003 - Definition Design screen
#Region "Variable"
    Private _treeNodeStringSperater As String = "||"
    Private _definationFilePath As String
    Private _sourceFilePath As String
    Private _originalUnstructuredFileConverter As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter
    Private _unstructuredFileConverter As New BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter()
    Private _UserID As String
#End Region

#Region "Public Property "
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property
    Public Property definationFilePath() As String
        Get
            Return _definationFilePath
        End Get
        Set(ByVal value As String)
            _definationFilePath = value
        End Set
    End Property
    Public Property sourceFilePath() As String
        Get
            Return _sourceFilePath
        End Get
        Set(ByVal value As String)
            _sourceFilePath = value
        End Set
    End Property
    Public Property originalUnstructuredFileConverter() As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter
        Get
            Return _originalUnstructuredFileConverter
        End Get
        Set(ByVal value As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter)
            _originalUnstructuredFileConverter = value
        End Set
    End Property
    Public Property unstructuredFileConverter() As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter
        Get
            Return _unstructuredFileConverter
        End Get
        Set(ByVal value As BTMU.Magic.UnstructuredFileConverter.UnstructuredFileConverter)
            _unstructuredFileConverter = value
        End Set
    End Property
#End Region

#Region "Control Event"
#Region "Form Event"
    Dim is_FinishReadSourceFile As Boolean = False
    Private Sub frmMA5003_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ucTextEditor.Visible = False
        ucGridEditor.Visible = False
        _is_First_Time_Node_Select = True
        CursorBusy()
        Me.Refresh()
        If unstructuredFileConverter Is Nothing Then
            unstructuredFileConverter.LoadFromFile(definationFilePath)
        End If

        DefinitionTreeView.Nodes(0).Nodes.Clear()
        DefinitionTreeView.Nodes(1).Nodes.Clear()
        Select Case unstructuredFileConverter.SourceFileType
            Case "D", "E"
                If unstructuredFileConverter.IdentifierCollection.Count = 0 Then
                    ucGridEditor.sourceFilePath = unstructuredFileConverter.SourceFile
                    ucGridEditor.SetObjectPoint(Nothing, Nothing, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter)
                    is_FinishReadSourceFile = True
                    ucGridEditor.ReadSourceFile()
                    ucGridEditor.AutoCreateNew()

                End If
        End Select
        Dim IdentifierNode As TreeNode = DefinitionTreeView.Nodes("IdentifiersRootNode")
        For Each identifier As Identifier In unstructuredFileConverter.IdentifierCollection
            Dim childNode As TreeNode
            childNode = IdentifierNode.Nodes.Add(BuildTreeNodeString("Identifier", identifier))
        Next

        Dim ColumnNode As TreeNode = DefinitionTreeView.Nodes("ColumnsRootNode")
        For Each column As Column In unstructuredFileConverter.ColumnCollection
            Dim childNode As TreeNode
            childNode = ColumnNode.Nodes.Add(BuildTreeNodeString("Column", column))
        Next

        If unstructuredFileConverter.SourceFileType = "F" Then
            HideTypeLegendsPanel.Visible = False
            ucTextEditor.Visible = True
            ucGridEditor.Visible = False
            ucTextEditor.sourceFilePath = unstructuredFileConverter.SourceFile
            ucTextEditor.ReadSourceFile()
        Else
            HideTypeLegendsPanel.Visible = True
            ucGridEditor.Visible = False
            ucGridEditor.Visible = True
            ucGridEditor.sourceFilePath = unstructuredFileConverter.SourceFile
            ucGridEditor.unstructuredFileConverter = unstructuredFileConverter
            If is_FinishReadSourceFile = False Then
                ucGridEditor.ReadSourceFile()
            End If
        End If
        DefinitionTreeView.ExpandAll()
        EnablePreview()
        CursorDefault()
        ucGridEditor.TreeView1 = DefinitionTreeView ' Used to refresh the treeview upon delimiter/identifier change
    End Sub
    Dim _is_First_Time_Node_Select As Boolean = False
    Private Sub SaveFile2()
        Log_CreateDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile.Replace(".edit", ""))
        unstructuredFileConverter.SaveToFile(unstructuredFileConverter.DefinitionFile.Replace(".edit", ""))
    End Sub
    Private Sub SaveFile()
        If originalUnstructuredFileConverter IsNot Nothing Then
            System.IO.File.Delete(originalUnstructuredFileConverter.DefinitionFile)
            Dim editFile As New FileInfo(unstructuredFileConverter.DefinitionFile)
            'Restore original Defiantaion file Path
            unstructuredFileConverter.DefinitionFile = originalUnstructuredFileConverter.DefinitionFile
            'Write Log File
            Log_ModifyDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
            unstructuredFileConverter.SaveToFile(definationFilePath)
            'Rename File Name
            editFile.MoveTo(originalUnstructuredFileConverter.DefinitionFile)
        Else
            'Write Log File
            Log_CreateDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
            unstructuredFileConverter.SaveToFile(unstructuredFileConverter.DefinitionFile)
        End If

    End Sub
    Private Function CheckOverwrite(ByVal vFileName As String) As DialogResult
        If File.Exists(vFileName) Then
            Return MessageBox.Show("Do you want to overwrite existing definition file?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        Else
            Return DialogResult.Yes
        End If
    End Function
    Private Sub frmMA5003_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Me.DialogResult = DialogResult.Cancel Then Me.DialogResult = Windows.Forms.DialogResult.Ignore
        If UpdateCurrentProperty() = False Then
            e.Cancel = True
            Exit Sub
        End If
        If Me.DialogResult <> Windows.Forms.DialogResult.Ignore Then
            If CheckDetailIdentifier() = False Then
                e.Cancel = True
                Exit Sub
            End If
        End If
        Dim userClick As DialogResult
        If originalUnstructuredFileConverter IsNot Nothing Then
            unstructuredFileConverter.SaveToFile(definationFilePath)
            If CheckItChange(unstructuredFileConverter, originalUnstructuredFileConverter) And doingSaving = False Then
                userClick = MessageBox.Show("Data has been changed. Do you want to save the data?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

                Select Case userClick
                    Case Windows.Forms.DialogResult.Yes
                        If CheckDetailIdentifier() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                        'Need to ask Confirm Msg
                        Select Case CheckOverwrite(unstructuredFileConverter.DefinitionFile.Replace(".edit", ""))
                            Case Windows.Forms.DialogResult.Yes
                                SaveFile() 'Modified
                                Me.DialogResult = DialogResult.Yes
                            Case Windows.Forms.DialogResult.Cancel
                                e.Cancel = True
                            Case Windows.Forms.DialogResult.No
                                DefinitionSaveFileDialog.FileName = New FileInfo(unstructuredFileConverter.DefinitionFile).Name.Replace(".mdt.edit", "")
                                userClick = DefinitionSaveFileDialog.ShowDialog
                                Select Case userClick
                                    Case Windows.Forms.DialogResult.OK
                                        Dim tmpFilePath As String = unstructuredFileConverter.DefinitionFile

                                        unstructuredFileConverter.DefinitionFile = DefinitionSaveFileDialog.FileName + ".mdt"
                                        SaveFile2()
                                        System.IO.File.Delete(tmpFilePath)
                                        'Write Log File
                                        'Commited by Hein
                                        'Log_ModifyDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
                                        Me.DialogResult = Windows.Forms.DialogResult.Yes
                                    Case Else
                                        e.Cancel = True
                                End Select
                        End Select
                    Case Windows.Forms.DialogResult.No
                        System.IO.File.Delete(unstructuredFileConverter.DefinitionFile)
                        Me.DialogResult = userClick
                    Case Windows.Forms.DialogResult.Cancel
                        e.Cancel = True
                End Select
            ElseIf doingSaving = True Then
                'Need to ask Confirm Msg
                Select Case CheckOverwrite(unstructuredFileConverter.DefinitionFile.Replace(".edit", ""))
                    Case Windows.Forms.DialogResult.Yes
                        SaveFile()
                        Me.DialogResult = DialogResult.Yes
                    Case Windows.Forms.DialogResult.Cancel
                        e.Cancel = True
                    Case Windows.Forms.DialogResult.No
                        DefinitionSaveFileDialog.FileName = New FileInfo(unstructuredFileConverter.DefinitionFile).Name.Replace(".mdt.edit", "")
                        userClick = DefinitionSaveFileDialog.ShowDialog
                        Select Case userClick
                            Case Windows.Forms.DialogResult.OK
                                Dim tmpFilePath As String = unstructuredFileConverter.DefinitionFile
                                unstructuredFileConverter.DefinitionFile = DefinitionSaveFileDialog.FileName + ".mdt"
                                SaveFile2()
                                System.IO.File.Delete(tmpFilePath)
                                'Write Log File
                                'Commited by Hein 20090909
                                'Log_ModifyDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
                                Me.DialogResult = Windows.Forms.DialogResult.Yes
                            Case Else
                                e.Cancel = True
                        End Select
                End Select
            Else
                'Click Cancel after open exiting defination
                Me.DialogResult = Windows.Forms.DialogResult.OK
                System.IO.File.Delete(unstructuredFileConverter.DefinitionFile)
                'Need to ask Confirm Msg
                unstructuredFileConverter.DefinitionFile = originalUnstructuredFileConverter.DefinitionFile
                unstructuredFileConverter.SaveToFile(originalUnstructuredFileConverter.DefinitionFile)

            End If
        ElseIf originalUnstructuredFileConverter Is Nothing And doingSaving = False Then
            'for new file but cancel to save
            If Me.DialogResult = Windows.Forms.DialogResult.Ignore Or Me.DialogResult = Windows.Forms.DialogResult.Cancel Then
                System.IO.File.Delete(unstructuredFileConverter.DefinitionFile)
            End If
        ElseIf originalUnstructuredFileConverter Is Nothing And doingSaving = True Then
            DefinitionSaveFileDialog.FileName = New FileInfo(unstructuredFileConverter.DefinitionFile).FullName.Replace(".mdt.new", "").Replace("\Sample Source\", "\Definition\")
            'Select Case userClick
            Dim oldTempNewFile As String = unstructuredFileConverter.DefinitionFile
            unstructuredFileConverter.DefinitionFile = DefinitionSaveFileDialog.FileName + ".mdt"

            Select Case CheckOverwrite(unstructuredFileConverter.DefinitionFile)
                Case Windows.Forms.DialogResult.Yes
                    If File.Exists(unstructuredFileConverter.DefinitionFile) Then
                        Log_ModifyDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
                    Else
                        Log_CreateDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
                    End If
                    unstructuredFileConverter.SaveToFile(unstructuredFileConverter.DefinitionFile)
                    System.IO.File.Delete(oldTempNewFile)
                    'SaveFile()
                    Me.DialogResult = DialogResult.Yes
                Case Windows.Forms.DialogResult.Cancel
                    e.Cancel = True
                Case Windows.Forms.DialogResult.No
                    DefinitionSaveFileDialog.FileName = unstructuredFileConverter.DefinitionFile.Replace(".mdt", "")
                    Select Case DefinitionSaveFileDialog.ShowDialog
                        Case Windows.Forms.DialogResult.OK
                            Dim filName As String = DefinitionSaveFileDialog.FileName + ".mdt"
                            unstructuredFileConverter.DefinitionFile = filName
                            Log_CreateDefinitionFile(UserID, unstructuredFileConverter.SourceFile, unstructuredFileConverter.DefinitionFile)
                            unstructuredFileConverter.SaveToFile(unstructuredFileConverter.DefinitionFile)
                            System.IO.File.Delete(oldTempNewFile)
                            'SaveFile()
                            Me.DialogResult = DialogResult.Yes
                        Case Else
                            e.Cancel = True
                    End Select
            End Select
        End If
        If e.CloseReason = CloseReason.TaskManagerClosing Or e.CloseReason = CloseReason.UserClosing Or e.CloseReason = CloseReason.WindowsShutDown Then

        End If

    End Sub

#End Region
#Region "TreeView Event"
    Private LastIdentifer As TreeNode

    Private Sub NodeSelectionChange(ByVal eNode As TreeNode)
        If Still_RollBack_Select Then Exit Sub 'When Same ID Error Occur, Roll back to selection
        If UpdateCurrentProperty() = False Then Exit Sub
        btnCopy.Enabled = (eNode.Parent IsNot Nothing)
        btnDelete.Enabled = (eNode.Parent IsNot Nothing)

        btnUp.Enabled = (eNode.Parent IsNot Nothing)
        btnDown.Enabled = (eNode.Parent IsNot Nothing)
        If eNode.Parent Is Nothing Then
            SetBlankTextEditor()
            Exit Sub
        End If
        Select Case unstructuredFileConverter.SourceFileType
            Case "F"


                If eNode.Parent.Text = "Identifiers" Then
                    LastIdentifer = eNode
                    Dim showIdentifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(eNode.Text))
                    ucIdentifierProperty.FillData(showIdentifier, unstructuredFileConverter.SourceFileType)
                    PropertyGroupBox.Text = "Identifier Properties"
                    ucIdentifierProperty.Visible = True
                    ucColumnsProperty.Visible = False
                    ucTextEditor.SetObjectPoint(showIdentifier, Nothing, unstructuredFileConverter.IdentifierCollection) ' Refresh Text Work Shop Color
                Else
                    Dim finderColumn() As String = DestoryTreeNodeIDString(eNode.Text)
                    Dim showColumn As Column = FindColumnObject(finderColumn(0), finderColumn(1))
                    Dim showIdentifier As Identifier = FindIdentifierObject(showColumn.IDNo)
                    ucColumnsProperty.FillData(showColumn, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.SourceFileType)
                    PropertyGroupBox.Text = "Column Properties"
                    ucColumnsProperty.Visible = True
                    ucIdentifierProperty.Visible = False

                    ucTextEditor.SetObjectPoint(showIdentifier, showColumn, unstructuredFileConverter.IdentifierCollection) ' Refresh Text Work Shop Color
                End If

            Case "D", "E"

                If eNode.Parent.Text = "Identifiers" Then
                    Dim showIdentifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(eNode.Text))
                    ucIdentifierProperty.FillData(showIdentifier, unstructuredFileConverter.SourceFileType)
                    PropertyGroupBox.Text = "Identifier Properties"
                    ucIdentifierProperty.Visible = True
                    ucColumnsProperty.Visible = False
                    ucGridEditor.SetObjectPoint(showIdentifier, Nothing, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter) ' Refresh Text Work Shop Color
                Else
                    Dim finderColumn() As String = DestoryTreeNodeIDString(eNode.Text)
                    Dim showColumn As Column = FindColumnObject(finderColumn(0), finderColumn(1))
                    Dim showIdentifier As Identifier = FindIdentifierObject(showColumn.IDNo)
                    ucColumnsProperty.FillData(showColumn, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.SourceFileType)
                    PropertyGroupBox.Text = "Column Properties"
                    ucColumnsProperty.Visible = True
                    ucIdentifierProperty.Visible = False

                    ucGridEditor.SetObjectPoint(showIdentifier, showColumn, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter) ' Refresh Text Work Shop Color
                End If
        End Select
        eNode.SelectedImageIndex = 2
    End Sub
    Dim Already_Mouse_Click
    Private Sub CursorBusy()
        Me.Cursor = Cursors.WaitCursor
        Me.Refresh()
    End Sub
    Private Sub CursorDefault()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub DefinitionTreeView_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles DefinitionTreeView.AfterSelect
        If _is_First_Time_Node_Select = True Then
            _is_First_Time_Node_Select = False
            Exit Sub
        End If
        CursorBusy()
        NodeSelectionChange(e.Node)
        CursorDefault()
    End Sub
    Private Sub DefinitionTreeView_NodeMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles DefinitionTreeView.NodeMouseClick
        'NodeSelectionChange(e.Node)
    End Sub
#End Region
#Region "Button Event"
    Private Sub btnNewIdentifier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewIdentifier.Click
        CreateNewIdentifier()
        EnablePreview()
    End Sub
    Private Sub btnNewColumn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewColumn.Click

        Dim getIdentifierId As String
        If DefinitionTreeView.SelectedNode.Parent Is Nothing Then
            If DefinitionTreeView.Nodes(0).Nodes.Count = 0 Then
                ShowErrorMsg("Fail to add new Column.", "There is any Identifier declared.")
                Exit Sub
            Else
                getIdentifierId = DestoryTreeNodeString(DefinitionTreeView.Nodes(0).Nodes(0).Text)
            End If
        ElseIf DefinitionTreeView.SelectedNode.Parent.Index = 0 Then
            getIdentifierId = DestoryTreeNodeString(DefinitionTreeView.SelectedNode.Text)
        Else
            If DefinitionTreeView.Nodes(0).Nodes.Count = 0 Then
                ShowErrorMsg("Fail to add new Column.", "There is any Identifier declared.")
                Exit Sub
            Else
                If LastIdentifer Is Nothing Then
                    getIdentifierId = DestoryTreeNodeString(DefinitionTreeView.Nodes(0).Nodes(0).Text)
                Else
                    'Updated by Hein 20090831
                    getIdentifierId = DestoryTreeNodeString(LastIdentifer.Text)
                End If

            End If
        End If
        CreateNewColumn(getIdentifierId)
        EnablePreview()
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If UpdateCurrentProperty() = False Then Exit Sub
        Dim selectedNode As TreeNode = DefinitionTreeView.SelectedNode
        If selectedNode.Parent Is Nothing Then Exit Sub

        If selectedNode.Parent.Index = 0 Then ' For Identifier
            If selectedNode.Index - 1 < 0 Then Exit Sub
            Dim selectedIdenifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(selectedNode.Text))
            unstructuredFileConverter.IdentifierCollection.RemoveAt(selectedNode.Index)
            unstructuredFileConverter.IdentifierCollection.Insert(selectedNode.Index - 1, selectedIdenifier)
        Else ' For Column
            If selectedNode.Index - 1 < 0 Then Exit Sub
            Dim finderColumn() As String = DestoryTreeNodeIDString(selectedNode.Text)
            Dim selectedColumn As Column = FindColumnObject(finderColumn(0), finderColumn(1))
            unstructuredFileConverter.ColumnCollection.RemoveAt(selectedNode.Index)
            unstructuredFileConverter.ColumnCollection.Insert(selectedNode.Index - 1, selectedColumn)
        End If
        Still_RollBack_Select = True
        Dim parentIndex As Integer = selectedNode.Parent.Index
        DefinitionTreeView.Nodes(parentIndex).Nodes.RemoveAt(selectedNode.Index)
        DefinitionTreeView.Nodes(parentIndex).Nodes.Insert(selectedNode.Index - 1, selectedNode)
        Still_RollBack_Select = False
        DefinitionTreeView.SelectedNode = selectedNode
    End Sub
    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If UpdateCurrentProperty() = False Then Exit Sub
        Dim selectedNode As TreeNode = DefinitionTreeView.SelectedNode
        If selectedNode.Parent Is Nothing Then Exit Sub

        If selectedNode.Parent.Index = 0 Then ' For Identifier
            If selectedNode.Index + 1 > unstructuredFileConverter.IdentifierCollection.Count - 1 Then Exit Sub
            Dim selectedIdenifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(selectedNode.Text))
            unstructuredFileConverter.IdentifierCollection.RemoveAt(selectedNode.Index)
            unstructuredFileConverter.IdentifierCollection.Insert(selectedNode.Index + 1, selectedIdenifier)
        Else ' For Column
            If selectedNode.Index + 1 > unstructuredFileConverter.ColumnCollection.Count - 1 Then Exit Sub
            Dim finderColumn() As String = DestoryTreeNodeIDString(selectedNode.Text)
            Dim selectedColumn As Column = FindColumnObject(finderColumn(0), finderColumn(1))
            unstructuredFileConverter.ColumnCollection.RemoveAt(selectedNode.Index)
            unstructuredFileConverter.ColumnCollection.Insert(selectedNode.Index + 1, selectedColumn)
        End If

        Dim parentIndex As Integer = selectedNode.Parent.Index
        Still_RollBack_Select = True
        DefinitionTreeView.Nodes(parentIndex).Nodes.RemoveAt(selectedNode.Index)
        DefinitionTreeView.Nodes(parentIndex).Nodes.Insert(selectedNode.Index + 1, selectedNode)

        DefinitionTreeView.SelectedNode = selectedNode
        Still_RollBack_Select = False
    End Sub
    Private Function CheckDetailCount() As Boolean
        Dim iDenCount As Integer = 0
        For Each vIde As Identifier In unstructuredFileConverter.IdentifierCollection
            If vIde.IDType = "D" Then
                iDenCount = iDenCount + 1
            End If
        Next
        If iDenCount = 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function Check2Detail() As Boolean
        Dim iDenCount As Integer = 0
        For Each vIde As Identifier In unstructuredFileConverter.IdentifierCollection
            If vIde.IDType = "D" Then
                iDenCount = iDenCount + 1
            End If
        Next
        If iDenCount > 1 Then
            Return False
        End If
        Return True
    End Function
    Private Function CheckDetailIdentifier() As Boolean
        If CheckDetailCount() = False Then
            ShowErrorMsg("There is no detail identifier defined!", "There is no identifier defined!")
            Return False
        End If
        If Check2Detail() = False Then
            ShowErrorMsg("Only one Detail Identifier can be defined in one definition!", "Only one Detail Identifier can be defined in one definition!")
            Return False
        End If
        Return True
    End Function
    Private Sub btnPriview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPriview.Click

        Me.Refresh()

        If UpdateCurrentProperty() = False Then Exit Sub
        If CheckDetailIdentifier() = False Then Exit Sub
        lblConvertMessage.Visible = True
        Dim oriFilePath As String = unstructuredFileConverter.DefinitionFile
        Dim tempFilePath As String = oriFilePath + ".tmp"
        unstructuredFileConverter.DefinitionFile = tempFilePath
        Try
            System.IO.File.Delete(unstructuredFileConverter.DefinitionFile)
        Catch ex As Exception
        End Try
        unstructuredFileConverter.SaveToFile(unstructuredFileConverter.DefinitionFile)
        Dim _frmMA5002 As New frmMA5002()
        _frmMA5002.lblConvertMessage = lblConvertMessage
        _frmMA5002.UserID = Me.UserID
        _frmMA5002.DefinitionFile = unstructuredFileConverter.DefinitionFile
        _frmMA5002.SourceFile = unstructuredFileConverter.SourceFile
        _frmMA5002.ShowDialog()
        unstructuredFileConverter.DefinitionFile = oriFilePath
        File.Delete(tempFilePath)
            'Label19.Visible = False
    End Sub
    Dim doingSaving As Boolean = False
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        doingSaving = True
        Me.Close()
        doingSaving = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteNode()
        EnablePreview()
    End Sub
    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        If UpdateCurrentProperty() = False Then Exit Sub 'Added by Hein 20090904
        If DefinitionTreeView.SelectedNode.Parent.Index = 0 Then
            'Selected Node is Identifier
            CreateNewIdentifier("", FindIdentifierObject(DestoryTreeNodeString(DefinitionTreeView.SelectedNode.Text)))
        Else
            'Selected Node is Column

            Dim finderColumn() As String = DestoryTreeNodeIDString(DefinitionTreeView.SelectedNode.Text)
            CreateNewColumn("", FindColumnObject(finderColumn(0), finderColumn(1)))
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        doingSaving = False
        Me.Close()
    End Sub
#End Region
#Region "Custom Function"
    Public Sub EnablePreview()
        btnPriview.Enabled = (DefinitionTreeView.Nodes(1).Nodes.Count <> 0)
    End Sub
    Private Function BuildTreeNodeString(ByVal nodeTypeParameter As String, ByVal nodeObject As Object, Optional ByVal newNameParameter As String = "") As String
        Dim _buildString As String = ""
        Dim _tmpName As String = ""
        Select Case nodeTypeParameter
            Case "Identifier"
                Dim tmpIdentifier As Identifier = nodeObject
                _tmpName = IIf(newNameParameter = "", tmpIdentifier.IDNo, newNameParameter)
                _buildString = tmpIdentifier.IDNo + " " + _treeNodeStringSperater + " [ " + tmpIdentifier.IDString.Trim() + " , " + _
                UnstructureFileConverterCommonFunc.GetIdentifierTypeName(tmpIdentifier.IDType) + " ]"
            Case "Column"
                Dim tmpColumn As Column = nodeObject
                _tmpName = IIf(newNameParameter = "", tmpColumn.ColName, newNameParameter)
                _buildString = tmpColumn.ColName + " " + _treeNodeStringSperater + " [ " + tmpColumn.IDNo + " , " + UnstructureFileConverterCommonFunc.GetColumnDataTypeName(tmpColumn.ColDataType) + " ] "
        End Select
        Return _buildString
    End Function
    Private Function DestoryTreeNodeIDString(ByVal nodeText As String) As String()
        Dim rawIDNo As String = nodeText.Split(_treeNodeStringSperater)(2).Split(",")(0).Replace("[ ", "")
        Dim rawColName As String = nodeText.Split(_treeNodeStringSperater)(0)
        Dim _destoryString() As String = New String() {rawColName.Substring(0, rawColName.Length - 1), _
       rawIDNo.Substring(1, rawIDNo.Length - 2)}
        Return _destoryString
    End Function
    Private Function DestoryTreeNodeString(ByVal nodeText As String) As String
        Dim _destoryString As String = nodeText.Split(_treeNodeStringSperater)(0).Substring(0, nodeText.Split(_treeNodeStringSperater)(0).Length - 1)
        Return _destoryString
    End Function
    Private Sub DeleteNode()
        If UpdateCurrentProperty() = False Then Exit Sub
        'Same as Up/Down Button , but not need to reinsert , only delete and parent select
        Dim selectedNode As TreeNode = DefinitionTreeView.SelectedNode
        If selectedNode.Parent Is Nothing Then Exit Sub

        If selectedNode.Parent.Index = 0 Then ' For Identifier
            If selectedNode.Index < 0 Then Exit Sub

            Dim selectedIdenifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(selectedNode.Text))
            If MessageBox.Show("Are you sure you want to remove identifier: " + selectedIdenifier.IDNo + _
            " with BTMU?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
            'Delete child Column that use this defination
            Dim still_Delete As Boolean = True
            If unstructuredFileConverter.ColumnCollection.Count = 0 Then still_Delete = False
            While still_Delete
                For Each columnTemp As Column In unstructuredFileConverter.ColumnCollection
                    If columnTemp.ColName = unstructuredFileConverter.ColumnCollection(unstructuredFileConverter.ColumnCollection.Count - 1).ColName Then
                        still_Delete = False
                    Else
                        still_Delete = True
                    End If
                    If columnTemp.IDNo = selectedIdenifier.IDNo Then
                        For Each tempTreeNode As TreeNode In DefinitionTreeView.Nodes(1).Nodes
                            Dim tempCovertTreeName As String = BuildTreeNodeString("Column", columnTemp)
                            If tempTreeNode.Text = tempCovertTreeName Then
                                unstructuredFileConverter.ColumnCollection.RemoveAt(tempTreeNode.Index)
                                DefinitionTreeView.Nodes(1).Nodes.RemoveAt(tempTreeNode.Index)
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End While
            'Delete Identifier
            unstructuredFileConverter.IdentifierCollection.RemoveAt(selectedNode.Index)
            LastIdentifer = Nothing 'Added by Hein 20090909
        Else ' For Column
            If selectedNode.Index < 0 Then Exit Sub
            Dim finderColumn() As String = DestoryTreeNodeIDString(selectedNode.Text)
            Dim selectedColumn As Column = FindColumnObject(finderColumn(0), finderColumn(1))
            If MessageBox.Show("Are you sure you want to remove column: " + selectedColumn.ColName + _
          " with BTMU?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
            unstructuredFileConverter.ColumnCollection.RemoveAt(selectedNode.Index)
        End If

        Dim parentIndex As Integer = selectedNode.Parent.Index
        DefinitionTreeView.Nodes(parentIndex).Nodes.RemoveAt(selectedNode.Index)

        DefinitionTreeView.Nodes(parentIndex).Checked = True
        SetBlankTextEditor()
    End Sub
    Public Sub ChangeIdentifierName(ByVal oldIdNoParameter As String, ByVal newIdNoParameter As String)
        If oldIdNoParameter <> newIdNoParameter Then
            For Each columnTemp As Column In unstructuredFileConverter.ColumnCollection
                If columnTemp.IDNo = oldIdNoParameter Then
                    columnTemp.IDNo = newIdNoParameter
                End If
            Next
        End If
    End Sub
    Private Sub SetBlankTextEditor()
        ucIdentifierProperty.Visible = False
        ucColumnsProperty.Visible = False
        PropertyGroupBox.Text = "Properties"
        Dim tempIdentifier As Identifier = New Identifier
        tempIdentifier.IDString = ""
        Select Case unstructuredFileConverter.SourceFileType
            Case "F"
                ucTextEditor.SetObjectPoint(tempIdentifier, Nothing, unstructuredFileConverter.IdentifierCollection)
            Case "E", "D"
                ucGridEditor.SetObjectPoint(tempIdentifier, Nothing, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter)
        End Select

    End Sub
    Public Function CanUseNewName(ByVal typeParameter As String, _
    ByVal currentObjectParameter As Object, _
    ByVal newNameParameter As String, _
    ByVal IdentifierNameParameter As String, ByVal newCol As String, ByVal newIDNo As String) As Boolean
        Select Case typeParameter
            Case "Column"
                Dim tempColumn As Column = currentObjectParameter

                For Each tempTreeNode As TreeNode In DefinitionTreeView.Nodes(1).Nodes
                    Dim finderColumn() As String = DestoryTreeNodeIDString(tempTreeNode.Text)
                    Dim oriTreeNodeName As String = finderColumn(0)
                    Dim oriTreeNodeIdNo As String = finderColumn(1)
                    If oriTreeNodeName = newCol And oriTreeNodeIdNo = newIDNo And _
                   (oriTreeNodeName <> newNameParameter Or oriTreeNodeIdNo <> IdentifierNameParameter) Then
                        Return False
                    End If
                Next
            Case "Identifier"
                Dim tempIdentifer As Identifier = currentObjectParameter
                For Each tempTreeNode As TreeNode In DefinitionTreeView.Nodes(0).Nodes
                    Dim oriTreeNodeName As String = DestoryTreeNodeString(tempTreeNode.Text)
                    If oriTreeNodeName <> tempIdentifer.IDNo And newNameParameter = oriTreeNodeName Then
                        Return False
                    End If
                Next
        End Select
        Return True
    End Function
    Public Sub CreateNewIdentifier(Optional ByVal idStringParamenter As String = "", Optional ByVal copyIdentifier As Identifier = Nothing)
        Dim newIdNo As String = GetNewIDNo("ID", "Identifier")
        Dim newIdentifer As New Identifier

        'For Copy button Event
        If copyIdentifier IsNot Nothing Then
            If UpdateCurrentProperty() = False Then Exit Sub
            newIdentifer.IDString = copyIdentifier.IDString
            newIdentifer.LineOffset = copyIdentifier.LineOffset
            newIdentifer.IDType = copyIdentifier.IDType
        Else
            'it is new 
            newIdentifer.IDString = idStringParamenter
            newIdentifer.IDType = "H"
            newIdentifer.LineOffset = "0"
        End If
        newIdentifer.IDNo = newIdNo

        unstructuredFileConverter.IdentifierCollection.Add(newIdentifer)
        Dim newTreeNode As TreeNode = New TreeNode(BuildTreeNodeString("Identifier", newIdentifer))
        DefinitionTreeView.Nodes(0).Nodes.Add(newTreeNode)
        DefinitionTreeView.SelectedNode = newTreeNode
        'Select and show in property box
        'Commited by Hein 20090727
        'Dim e As TreeNodeMouseClickEventArgs = New TreeNodeMouseClickEventArgs(newTreeNode, Windows.Forms.MouseButtons.Left, 0, 0, 0)
        ''DefinitionTreeView_NodeMouseClick(DefinitionTreeView, e)
        'NodeSelectionChange(newTreeNode)
        'Select Case unstructuredFileConverter.SourceFileType
        '    Case "F"
        '        ucTextEditor.SetObjectPoint(newIdentifer, Nothing, unstructuredFileConverter.IdentifierCollection) ' Refresh Text Work Shop Color
        '    Case "E", "D"
        '        ucGridEditor.SetObjectPoint(newIdentifer, Nothing, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter) ' Refresh Text Work Shop Color
        'End Select

    End Sub
    Public Sub CreateNewColumn(ByVal idNoParamenter As String, Optional ByVal copyColumn As Column = Nothing)
        Dim newColumName As String = GetNewIDNo("Column", "Column")
        Dim newColumn As New Column
        newColumn.ColName = newColumName


        'For Copy button Event
        If copyColumn IsNot Nothing Then
            If UpdateCurrentProperty() = False Then Exit Sub
            newColumn.Start = copyColumn.Start
            newColumn.ColLineNo = copyColumn.ColLineNo
            newColumn.Repeat = copyColumn.Repeat
            newColumn.DelimColNo = copyColumn.DelimColNo
            newColumn.ColLength = copyColumn.ColLength
            newColumn.ColDataType = copyColumn.ColDataType
            newColumn.DataFormat = copyColumn.DataFormat
            newColumn.IDNo = copyColumn.IDNo
            newColumn.ColNo = copyColumn.ColNo
            newColumn.Delimiter = copyColumn.Delimiter
            newColumn.TextQualifier = copyColumn.TextQualifier
            newColumn.MultiLineEnd = copyColumn.MultiLineEnd 'added by hein 20090909
        Else
            'it is new 
            newColumn.ColNo = 1
            newColumn.Start = 0
            newColumn.ColLineNo = 0
            newColumn.Repeat = 0
            newColumn.DelimColNo = 0
            newColumn.ColLength = 0
            newColumn.ColDataType = "T"
            newColumn.DataFormat = "Trim"
            newColumn.IDNo = idNoParamenter
            newColumn.Delimiter = ""
            newColumn.TextQualifier = ""
            newColumn.MultiLineEnd = ""
        End If


        unstructuredFileConverter.ColumnCollection.Add(newColumn)
        Dim newTreeNode As TreeNode = New TreeNode(BuildTreeNodeString("Column", newColumn))
        DefinitionTreeView.Nodes(1).Nodes.Add(newTreeNode)
        DefinitionTreeView.SelectedNode = newTreeNode

        'Commited by Hein 20090727
        'Dim showIdentifier As Identifier = FindIdentifierObject(DestoryTreeNodeString(newColumn.IDNo))
        'Select Case unstructuredFileConverter.SourceFileType
        '    Case "F"
        '        ucTextEditor.SetObjectPoint(showIdentifier, newColumn, unstructuredFileConverter.IdentifierCollection) ' Refresh Text Work Shop Color
        '    Case "E", "D"
        '        ucGridEditor.SetObjectPoint(showIdentifier, newColumn, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter) ' Refresh Text Work Shop Color
        'End Select
        'Commited by Hein 20090727

        'Select and show in property box
        'Dim e As TreeNodeMouseClickEventArgs = New TreeNodeMouseClickEventArgs(newTreeNode, Windows.Forms.MouseButtons.Left, 0, 0, 0)
        'DefinitionTreeView_NodeMouseClick(DefinitionTreeView, e)
        'NodeSelectionChange(newTreeNode)
    End Sub
    Dim Still_RollBack_Select As Boolean = False
    Private Function UpdateCurrentProperty() As Boolean
        If PropertyGroupBox.Controls.Count <> 0 Then
            If ucIdentifierProperty.Visible = True Then 'it is Identifier Control
                If ucIdentifierProperty.identifier Is Nothing Then Exit Function
                If ucIdentifierProperty.CloseEdit() = False Then
                    If ucIdentifierProperty.OriginalIDNo <> DefinitionTreeView.SelectedNode.Text Then
                        For Each tempTreeNode As TreeNode In DefinitionTreeView.Nodes(0).Nodes
                            If DestoryTreeNodeString(tempTreeNode.Text) = ucIdentifierProperty.OriginalIDNo Then
                                Still_RollBack_Select = True
                                DefinitionTreeView.SelectedNode = tempTreeNode
                                Still_RollBack_Select = False
                                Exit For
                            End If
                        Next
                    End If
                    Return False
                End If
                'UpdateIDNoTreeNode(ucIdentifierProperty.OriginalIDNo, ucIdentifierProperty.identifier.IDNo, ucIdentifierProperty.identifier, "Identifier") 'Update Tree View Text When we change ID name
                RefreshTreeNode()
            Else 'it is Column Control
                If ucColumnsProperty.column Is Nothing Then Exit Function
                If ucColumnsProperty.CloseEdit() = False Then

                    Dim TreeNodeColumnName As String = DestoryTreeNodeIDString(DefinitionTreeView.SelectedNode.Text)(0)
                    Dim TreeNodeIDNo As String = DestoryTreeNodeIDString(DefinitionTreeView.SelectedNode.Text)(1)
                    If ucColumnsProperty.OriginalColumnName <> TreeNodeColumnName Or _
                    ucColumnsProperty.OriginalIdNo <> TreeNodeIDNo Then

                        For Each tempTreeNode As TreeNode In DefinitionTreeView.Nodes(1).Nodes
                            TreeNodeColumnName = DestoryTreeNodeIDString(tempTreeNode.Text)(0)
                            TreeNodeIDNo = DestoryTreeNodeIDString(tempTreeNode.Text)(1)
                            If TreeNodeColumnName = ucColumnsProperty.OriginalColumnName And TreeNodeIDNo = ucColumnsProperty.OriginalIdNo Then
                                Still_RollBack_Select = True
                                DefinitionTreeView.SelectedNode = tempTreeNode
                                Still_RollBack_Select = False
                                Exit For
                            End If
                        Next
                    End If
                    Return False
                End If
                'UpdateIDNoTreeNode(ucColumnsProperty.OriginalColumnName, ucColumnsProperty.column.ColName, ucColumnsProperty.column, "Column") 'Update Tree View Text When we change ID name
                RefreshTreeNode()
            End If
            Select Case unstructuredFileConverter.SourceFileType
                Case "F"
                    ucTextEditor.CloseEdit() 'Update From Workshop Textbox change
                Case "E", "D"
                    ucGridEditor.CloseEdit()
            End Select
        End If
        Return True
    End Function
    Private Sub RefreshTreeNode()
        Dim tmpIndex As Integer = 0
        For Each identifier As Identifier In unstructuredFileConverter.IdentifierCollection
            DefinitionTreeView.Nodes(0).Nodes(tmpIndex).Text = BuildTreeNodeString("Identifier", identifier)
            tmpIndex += 1
        Next
        tmpIndex = 0
        For Each column As Column In unstructuredFileConverter.ColumnCollection
            DefinitionTreeView.Nodes(1).Nodes(tmpIndex).Text = BuildTreeNodeString("Column", column)
            tmpIndex += 1
        Next
    End Sub
    Private Sub UpdateIDNoTreeNode(ByVal OriginalTreeNodeName As String, ByVal NewTreeNodeName As String, _
    ByVal newUpdateObject As Object, ByVal objectType As String)
        If OriginalTreeNodeName = NewTreeNodeName Then Exit Sub
        For Each parentNode As TreeNode In DefinitionTreeView.Nodes
            For Each ChildNode As TreeNode In parentNode.Nodes
                If DestoryTreeNodeString(ChildNode.Text) = OriginalTreeNodeName Then
                    ChildNode.Text = BuildTreeNodeString(objectType, newUpdateObject)
                    Exit Sub
                End If
            Next
        Next
    End Sub

    Public Function FindIdentifierObject(ByVal IDNoParameter As String, Optional ByRef IndexParameter As Integer = 0) As Identifier
        For index As Integer = 0 To unstructuredFileConverter.IdentifierCollection.Count - 1
            Dim identifier As Identifier = unstructuredFileConverter.IdentifierCollection(index)
            If identifier.IDNo = IDNoParameter Then
                IndexParameter = index
                Return identifier
            End If
        Next
        Return Nothing
    End Function
    Private Function FindColumnObject(ByVal columnNameParameter As String, ByVal IDNoParameter As String, Optional ByRef IndexParameter As Integer = 0) As Column
        For index As Integer = 0 To unstructuredFileConverter.ColumnCollection.Count - 1
            Dim column As Column = unstructuredFileConverter.ColumnCollection(index)
            If column.ColName = columnNameParameter And column.IDNo = IDNoParameter Then
                IndexParameter = index
                Return column
            End If
        Next
        Return Nothing
    End Function
    Public Function GetNewIDNo(ByVal FormatStringParameter As String, ByVal NodeTypeParameter As String) As String
        Dim tryInteger As Integer
        Dim MaxNo As Integer = 0

        Dim parentNodeIndex As Integer
        If NodeTypeParameter = "Identifier" Then
            parentNodeIndex = 0
        Else
            parentNodeIndex = 1
        End If
        For Each ChildNode As TreeNode In DefinitionTreeView.Nodes(parentNodeIndex).Nodes
            Dim _childNodeText As String = DestoryTreeNodeString(ChildNode.Text)
            If _childNodeText.IndexOf(FormatStringParameter) <> -1 And Integer.TryParse(_childNodeText.Replace(FormatStringParameter, "").Trim, tryInteger) Then
                If MaxNo < tryInteger Then
                    MaxNo = tryInteger
                End If
            End If
        Next
        Return FormatStringParameter + (MaxNo + 1).ToString()
    End Function
    Public Sub RefreshPaintScreen()
        Select Case unstructuredFileConverter.SourceFileType
            Case "F"
                ucTextEditor.RefreshPaintScreen()
            Case "E", "D"
                ucGridEditor.RefreshPaintScreen()
        End Select

    End Sub
    Public Function ColumnChangeIdentifier(ByVal IDNoParameter As String) As Boolean
        Dim findIdentifier As Identifier = FindIdentifierObject(IDNoParameter)
        If findIdentifier Is Nothing Then
            Return False
        Else
            If unstructuredFileConverter.SourceFileType = "F" Then
                ucTextEditor.SetObjectPoint(findIdentifier, ucColumnsProperty.column, unstructuredFileConverter.IdentifierCollection)
            Else
                ucGridEditor.SetObjectPoint(findIdentifier, ucColumnsProperty.column, unstructuredFileConverter.IdentifierCollection, unstructuredFileConverter.ColumnCollection, unstructuredFileConverter)
            End If
            Return True
        End If
    End Function
    Public Sub RefreshPropertyBox()
        If ucIdentifierProperty.Visible Then
            ucIdentifierProperty.RefreshPropertyBox()
        ElseIf ucColumnsProperty.Visible Then
            ucColumnsProperty.RefreshPropertyBox()
        End If
        EnablePreview()
    End Sub
#End Region
#End Region
End Class