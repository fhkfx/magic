Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat
''' <summary>
''' Form for Convert Common Transaction - Swift
''' </summary>
''' <remarks></remarks>
Public Class frmConvertCommonTransactionSWIFT
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Private _datamodel As SwiftTemplate
    Private _masterTemplateList As MasterTemplateList
    Private _objPreview As BaseFileFormat

#Region " On Load "
    Private Sub frmConvertCommonTransactionSWIFT_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SWIFTTemplateBindingSource.DataSource = CommonTemplateModule.ListAllSWIFTCommonTemplates
            SWIFTTemplateComboBox.SelectedIndex = -1
            MasterTemplateTextBox.Clear()
            SaveLocationTextBox.Text = My.Settings.ConvertSWIFTFolder
            ' Check the UseValueDate checkbox and enable the ValueDatePicker
            ValueDateCheckBox.Checked = True
            ValueDatePicker.Enabled = True
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading SWIFT Template : ", ex.Message.ToString)
        End Try
    End Sub
    ''' <summary>
    ''' Initializes the Form with the Swift Template Data
    ''' </summary>
    ''' <param name="objPreview">Preview Object containing the Swift Template Data</param>
    ''' <remarks></remarks>
    Public Sub SetData(ByRef objPreview As BaseFileFormat)
        Try
            ' Assign the values to each fields
            _objPreview = objPreview
            SWIFTTemplateComboBox.SelectedValue = _objPreview.CommonTemplateName
            MasterTemplateTextBox.Text = _objPreview.MasterTemplateName
            SWIFTFileTextBox.Text = _objPreview.SourceFileName
            SaveLocationTextBox.Text = _objPreview.SaveLocation
            ValueDatePicker.Value = _objPreview.ValueDate
            If _objPreview.UseValueDate Then
                ValueDateCheckBox.Checked = True
            Else
                ValueDateCheckBox.Checked = False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading SWIFT Template : ", ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " Source File Drag and Drop "
    Private Sub SWIFTFileTextBox_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SWIFTFileTextBox.DragDrop
        Dim filename As String = ""
        Dim Filenames As String()
        SWIFTFileTextBox.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".txt") Or filename.EndsWith(".csv")) Then Exit Sub
        If filename <> "" Then
            SWIFTFileTextBox.Text = filename
        End If
    End Sub

    Private Sub SWIFTFileTextBox_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SWIFTFileTextBox.DragEnter
        ' '' ''Dim format As String
        ' '' ''For Each format In e.Data.GetFormats
        ' '' ''If Format= System.Windows.Forms.DataFormats.FileDrop Then
        '' ''If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
        '' ''    e.Effect = DragDropEffects.All
        '' ''Else
        '' ''    e.Effect = DragDropEffects.None
        '' ''End If
        ' '' ''Exit For
        ' '' ''End If
        ' '' ''Next
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub
#End Region

#Region " Event Handlers for Browse Source, Browse Save Location, Preview "
    Private Sub btnBrowseSWIFTFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSWIFTFile.Click
        Dim dlgResult As DialogResult
        dlgResult = OpenFileDialog1.ShowDialog()
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SWIFTFileTextBox.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btnBrowseSaveLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSaveLocation.Click
        Dim saveFolder As New FolderBrowserDialog
        Dim dlgResult As DialogResult

        FolderBrowserDialog1.SelectedPath = My.Settings.ConvertSWIFTFolder
        dlgResult = FolderBrowserDialog1.ShowDialog
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SaveLocationTextBox.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If Not IsFieldValid() Then Exit Sub
            If _masterTemplateList Is Nothing Then Exit Sub
            If _datamodel Is Nothing Then Exit Sub
            If IsNothingOrEmptyString(_masterTemplateList.OutputFormat) Then
                MessageBox.Show(MsgReader.GetString("E02000020"), "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'MessageBox.Show("Master Template does not exist.", "Convert Common Transaction - SWIFT", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            ' Show the hash value of the source file, if GCMS Hashing is True.
            If Not ShowHash() Then Exit Sub

            Dim SWIFTTemplate As SwiftTemplateList
            If SWIFTTemplateComboBox.SelectedIndex = -1 Then Exit Sub
            If SWIFTTemplateBindingSource Is Nothing Then Exit Sub
            SWIFTTemplate = SWIFTTemplateBindingSource.Current
            If SWIFTTemplate Is Nothing Then Exit Sub

            _objPreview = BaseFileFormat.CreateFileFormat(_masterTemplateList.OutputFormat)
            _objPreview.CommonTemplateName = SwiftTemplate.SwiftTemplate
            _objPreview.MasterTemplateName = SwiftTemplate.OutputTemplate

            If _objPreview Is Nothing Then Exit Sub

            ' Assign all the values to the object
            _objPreview.SourceFormat = "SWIFT"
            _objPreview.CommonTemplateName = SWIFTTemplateComboBox.SelectedValue
            _objPreview.MasterTemplateName = MasterTemplateTextBox.Text
            _objPreview.SourceFileName = SWIFTFileTextBox.Text
            _objPreview.ValueDate = ValueDatePicker.Value
            If ValueDateCheckBox.Checked Then
                _objPreview.UseValueDate = True
            Else
                _objPreview.UseValueDate = False
            End If
            _objPreview.SaveLocation = SaveLocationTextBox.Text

            _objPreview.ValidateBeforeConversion()
            frmPreview.ShowPreview(_datamodel, _masterTemplateList, False, Nothing, _objPreview)

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", ex.Message)
        End Try
    End Sub
#End Region

#Region " On Common Template Selection "
    Private Sub SWIFTTemplateComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles SWIFTTemplateComboBox.LostFocus
        Dim SWIFTTemplate As SwiftTemplateList
        Try
            MasterTemplateTextBox.Text = ""
            If SWIFTTemplateComboBox.SelectedIndex = -1 Then Exit Sub
            If SWIFTTemplateBindingSource Is Nothing Then Exit Sub
            SWIFTTemplate = SWIFTTemplateBindingSource.Current
            If SWIFTTemplate Is Nothing Then Exit Sub
            MasterTemplateTextBox.Text = SWIFTTemplate.OutputTemplate
            _datamodel = CommonTemplateModule.LoadSWIFTCommonTemplate(SWIFTTemplate.SwiftTemplate)
            _masterTemplateList = CommonTemplateModule.GetMasterTemplateList(SWIFTTemplate.OutputTemplate)
            If _masterTemplateList.OutputFormat Is Nothing Then Exit Sub
           
            If IsValueDateMapped(_datamodel, _masterTemplateList) Then
                ValueDatePicker.Enabled = False
                ValueDateCheckBox.Checked = False
            Else
                ValueDatePicker.Enabled = True
                ValueDateCheckBox.Checked = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " On Value Date Selection "
    Private Sub ValueDateCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ValueDateCheckBox.Click
        If ValueDateCheckBox.Checked Then
            ValueDatePicker.Enabled = True
        Else
            ValueDatePicker.Enabled = False
        End If
    End Sub
#End Region
#Region " Validation Before Preview "
    Private Function IsFieldValid() As Boolean
        Dim errMessage As String = String.Empty
        If SWIFTTemplateComboBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Common Template Name") & vbNewLine
        End If
        If SWIFTFileTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Source File Name") & vbNewLine
        End If

        If SaveLocationTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Save Location") & vbNewLine
        End If
        If errMessage = String.Empty Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", errMessage)
            Return False
        End If
    End Function
#End Region
    Private Function ShowHash() As Boolean
        If _masterTemplateList.OutputFormat = GCMSFormat.OutputFormatString AndAlso My.Settings.GCMSHashing = "True" Then
            Dim objHashing As New frmGCMSHashingView
            objHashing.GetHash(SWIFTFileTextBox.Text)
            If objHashing.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return False
        End If
        Return True
    End Function
End Class