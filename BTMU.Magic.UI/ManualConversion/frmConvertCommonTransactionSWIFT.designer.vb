<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConvertCommonTransactionSWIFT
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConvertCommonTransactionSWIFT))
        Me.btnPreview = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnBrowseSaveLocation = New System.Windows.Forms.Button
        Me.btnBrowseSWIFTFile = New System.Windows.Forms.Button
        Me.ValueDateCheckBox = New System.Windows.Forms.CheckBox
        Me.ValueDatePicker = New System.Windows.Forms.DateTimePicker
        Me.SaveLocationTextBox = New System.Windows.Forms.TextBox
        Me.SWIFTFileTextBox = New System.Windows.Forms.TextBox
        Me.MasterTemplateTextBox = New System.Windows.Forms.TextBox
        Me.SWIFTTemplateComboBox = New System.Windows.Forms.ComboBox
        Me.SWIFTTemplateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.convertBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.GroupBox1.SuspendLayout()
        CType(Me.SWIFTTemplateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(311, 225)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 40)
        Me.btnPreview.TabIndex = 1
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnBrowseSaveLocation)
        Me.GroupBox1.Controls.Add(Me.btnBrowseSWIFTFile)
        Me.GroupBox1.Controls.Add(Me.ValueDateCheckBox)
        Me.GroupBox1.Controls.Add(Me.ValueDatePicker)
        Me.GroupBox1.Controls.Add(Me.SaveLocationTextBox)
        Me.GroupBox1.Controls.Add(Me.SWIFTFileTextBox)
        Me.GroupBox1.Controls.Add(Me.MasterTemplateTextBox)
        Me.GroupBox1.Controls.Add(Me.SWIFTTemplateComboBox)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(675, 202)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Template Settings"
        '
        'btnBrowseSaveLocation
        '
        Me.btnBrowseSaveLocation.Location = New System.Drawing.Point(577, 154)
        Me.btnBrowseSaveLocation.Name = "btnBrowseSaveLocation"
        Me.btnBrowseSaveLocation.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSaveLocation.TabIndex = 7
        Me.btnBrowseSaveLocation.TabStop = False
        Me.btnBrowseSaveLocation.Text = "&Browse"
        Me.btnBrowseSaveLocation.UseVisualStyleBackColor = True
        '
        'btnBrowseSWIFTFile
        '
        Me.btnBrowseSWIFTFile.Location = New System.Drawing.Point(578, 86)
        Me.btnBrowseSWIFTFile.Name = "btnBrowseSWIFTFile"
        Me.btnBrowseSWIFTFile.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSWIFTFile.TabIndex = 3
        Me.btnBrowseSWIFTFile.TabStop = False
        Me.btnBrowseSWIFTFile.Text = "&Browse"
        Me.btnBrowseSWIFTFile.UseVisualStyleBackColor = True
        '
        'ValueDateCheckBox
        '
        Me.ValueDateCheckBox.AutoSize = True
        Me.ValueDateCheckBox.Location = New System.Drawing.Point(260, 127)
        Me.ValueDateCheckBox.Name = "ValueDateCheckBox"
        Me.ValueDateCheckBox.Size = New System.Drawing.Size(101, 18)
        Me.ValueDateCheckBox.TabIndex = 5
        Me.ValueDateCheckBox.Text = "Use Value Date"
        Me.ValueDateCheckBox.UseVisualStyleBackColor = True
        '
        'ValueDatePicker
        '
        Me.ValueDatePicker.CustomFormat = "dd/MMM/yyyy"
        Me.ValueDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ValueDatePicker.Location = New System.Drawing.Point(146, 125)
        Me.ValueDatePicker.Name = "ValueDatePicker"
        Me.ValueDatePicker.Size = New System.Drawing.Size(108, 20)
        Me.ValueDatePicker.TabIndex = 4
        '
        'SaveLocationTextBox
        '
        Me.SaveLocationTextBox.AllowDrop = True
        Me.SaveLocationTextBox.Location = New System.Drawing.Point(146, 158)
        Me.SaveLocationTextBox.Name = "SaveLocationTextBox"
        Me.SaveLocationTextBox.ReadOnly = True
        Me.SaveLocationTextBox.Size = New System.Drawing.Size(425, 20)
        Me.SaveLocationTextBox.TabIndex = 6
        '
        'SWIFTFileTextBox
        '
        Me.SWIFTFileTextBox.AllowDrop = True
        Me.SWIFTFileTextBox.Location = New System.Drawing.Point(146, 90)
        Me.SWIFTFileTextBox.Name = "SWIFTFileTextBox"
        Me.SWIFTFileTextBox.ReadOnly = True
        Me.SWIFTFileTextBox.Size = New System.Drawing.Size(426, 20)
        Me.SWIFTFileTextBox.TabIndex = 2
        '
        'MasterTemplateTextBox
        '
        Me.MasterTemplateTextBox.Location = New System.Drawing.Point(146, 57)
        Me.MasterTemplateTextBox.Name = "MasterTemplateTextBox"
        Me.MasterTemplateTextBox.ReadOnly = True
        Me.MasterTemplateTextBox.Size = New System.Drawing.Size(518, 20)
        Me.MasterTemplateTextBox.TabIndex = 1
        '
        'SWIFTTemplateComboBox
        '
        Me.SWIFTTemplateComboBox.DataSource = Me.SWIFTTemplateBindingSource
        Me.SWIFTTemplateComboBox.DisplayMember = "SwiftTemplate"
        Me.SWIFTTemplateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SWIFTTemplateComboBox.FormattingEnabled = True
        Me.SWIFTTemplateComboBox.Location = New System.Drawing.Point(146, 23)
        Me.SWIFTTemplateComboBox.Name = "SWIFTTemplateComboBox"
        Me.SWIFTTemplateComboBox.Size = New System.Drawing.Size(518, 22)
        Me.SWIFTTemplateComboBox.TabIndex = 0
        Me.SWIFTTemplateComboBox.ValueMember = "SwiftTemplate"
        '
        'SWIFTTemplateBindingSource
        '
        Me.SWIFTTemplateBindingSource.DataSource = GetType(BTMU.Magic.CommonTemplate.SwiftTemplateListCollection)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 162)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 14)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Save Location :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Value Date :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "SWIFT File Name :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Master Template Name :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "SWIFT Template Name :"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "txt files|*.txt|csv files|*.csv|All files|*.*"
        '
        'frmConvertCommonTransactionSWIFT
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 271)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConvertCommonTransactionSWIFT"
        Me.ShowIcon = False
        Me.Text = "Convert Common Transaction - SWIFT (MA3020)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SWIFTTemplateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBrowseSaveLocation As System.Windows.Forms.Button
    Friend WithEvents btnBrowseSWIFTFile As System.Windows.Forms.Button
    Friend WithEvents ValueDateCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ValueDatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents SaveLocationTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SWIFTFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MasterTemplateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SWIFTTemplateComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents convertBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SWIFTTemplateBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
End Class
