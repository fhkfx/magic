Imports System.IO
Imports BTMU.Magic.Common
Public Class frmGCMSHashingView
    ''' <summary>
    ''' This function is called by Manual/Quick Conversion screens to show the
    ''' Hash Value of the input source file.
    ''' </summary>
    ''' <param name="inputSource">Name of the input source file for which the
    ''' the Hash Value is generated</param>
    ''' <remarks></remarks>
    Public Sub GetHash(ByVal inputSource As String)
        InputSourceTextBox.Text = inputSource
        HashTextBox.Text = ComputeHashAsHexString(File.ReadAllBytes(inputSource), "SHA512")
    End Sub

End Class