<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConvertCommonTransactionExcel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim CommonTemplateNameLabel As System.Windows.Forms.Label
        Dim MasterTemplateNameLabel As System.Windows.Forms.Label
        Dim SaveLocationLabel As System.Windows.Forms.Label
        Dim SourceFileNameLabel As System.Windows.Forms.Label
        Dim TransactionEndRowLabel As System.Windows.Forms.Label
        Dim ValueDateLabel As System.Windows.Forms.Label
        Dim WorksheetNameLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConvertCommonTransactionExcel))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TransactionEndRowTextBox = New BTMU.Magic.UI.NumberBox
        Me.ValueDatePicker = New System.Windows.Forms.DateTimePicker
        Me.worksheetComboBox = New System.Windows.Forms.ComboBox
        Me.worksheetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CommonTemplateNameComboBox = New System.Windows.Forms.ComboBox
        Me.commonTemplateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UseValueDateCheckBox = New System.Windows.Forms.CheckBox
        Me.SourceFileNameTextBox = New System.Windows.Forms.TextBox
        Me.SaveLocationTextBox = New System.Windows.Forms.TextBox
        Me.MasterTemplateNameTextBox = New System.Windows.Forms.TextBox
        Me.btnBrowseSaveLocation = New System.Windows.Forms.Button
        Me.btnBrowseSourceFile = New System.Windows.Forms.Button
        Me.convertBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnPreview = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CommonTemplateNameLabel = New System.Windows.Forms.Label
        MasterTemplateNameLabel = New System.Windows.Forms.Label
        SaveLocationLabel = New System.Windows.Forms.Label
        SourceFileNameLabel = New System.Windows.Forms.Label
        TransactionEndRowLabel = New System.Windows.Forms.Label
        ValueDateLabel = New System.Windows.Forms.Label
        WorksheetNameLabel = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.worksheetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.commonTemplateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CommonTemplateNameLabel
        '
        CommonTemplateNameLabel.AutoSize = True
        CommonTemplateNameLabel.Location = New System.Drawing.Point(16, 24)
        CommonTemplateNameLabel.Name = "CommonTemplateNameLabel"
        CommonTemplateNameLabel.Size = New System.Drawing.Size(127, 14)
        CommonTemplateNameLabel.TabIndex = 16
        CommonTemplateNameLabel.Text = "Common Template Name:"
        '
        'MasterTemplateNameLabel
        '
        MasterTemplateNameLabel.AutoSize = True
        MasterTemplateNameLabel.Location = New System.Drawing.Point(16, 57)
        MasterTemplateNameLabel.Name = "MasterTemplateNameLabel"
        MasterTemplateNameLabel.Size = New System.Drawing.Size(119, 14)
        MasterTemplateNameLabel.TabIndex = 17
        MasterTemplateNameLabel.Text = "Master Template Name:"
        '
        'SaveLocationLabel
        '
        SaveLocationLabel.AutoSize = True
        SaveLocationLabel.Location = New System.Drawing.Point(16, 219)
        SaveLocationLabel.Name = "SaveLocationLabel"
        SaveLocationLabel.Size = New System.Drawing.Size(79, 14)
        SaveLocationLabel.TabIndex = 18
        SaveLocationLabel.Text = "Save Location:"
        '
        'SourceFileNameLabel
        '
        SourceFileNameLabel.AutoSize = True
        SourceFileNameLabel.Location = New System.Drawing.Point(16, 88)
        SourceFileNameLabel.Name = "SourceFileNameLabel"
        SourceFileNameLabel.Size = New System.Drawing.Size(94, 14)
        SourceFileNameLabel.TabIndex = 19
        SourceFileNameLabel.Text = "Source File Name:"
        '
        'TransactionEndRowLabel
        '
        TransactionEndRowLabel.AutoSize = True
        TransactionEndRowLabel.Location = New System.Drawing.Point(16, 187)
        TransactionEndRowLabel.Name = "TransactionEndRowLabel"
        TransactionEndRowLabel.Size = New System.Drawing.Size(114, 14)
        TransactionEndRowLabel.TabIndex = 20
        TransactionEndRowLabel.Text = "Transaction End Row:"
        '
        'ValueDateLabel
        '
        ValueDateLabel.AutoSize = True
        ValueDateLabel.Location = New System.Drawing.Point(16, 155)
        ValueDateLabel.Name = "ValueDateLabel"
        ValueDateLabel.Size = New System.Drawing.Size(63, 14)
        ValueDateLabel.TabIndex = 22
        ValueDateLabel.Text = "Value Date:"
        '
        'WorksheetNameLabel
        '
        WorksheetNameLabel.AutoSize = True
        WorksheetNameLabel.Location = New System.Drawing.Point(16, 121)
        WorksheetNameLabel.Name = "WorksheetNameLabel"
        WorksheetNameLabel.Size = New System.Drawing.Size(92, 14)
        WorksheetNameLabel.TabIndex = 23
        WorksheetNameLabel.Text = "Worksheet Name:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TransactionEndRowTextBox)
        Me.GroupBox1.Controls.Add(Me.ValueDatePicker)
        Me.GroupBox1.Controls.Add(Me.worksheetComboBox)
        Me.GroupBox1.Controls.Add(Me.CommonTemplateNameComboBox)
        Me.GroupBox1.Controls.Add(WorksheetNameLabel)
        Me.GroupBox1.Controls.Add(ValueDateLabel)
        Me.GroupBox1.Controls.Add(Me.UseValueDateCheckBox)
        Me.GroupBox1.Controls.Add(TransactionEndRowLabel)
        Me.GroupBox1.Controls.Add(SourceFileNameLabel)
        Me.GroupBox1.Controls.Add(Me.SourceFileNameTextBox)
        Me.GroupBox1.Controls.Add(SaveLocationLabel)
        Me.GroupBox1.Controls.Add(Me.SaveLocationTextBox)
        Me.GroupBox1.Controls.Add(MasterTemplateNameLabel)
        Me.GroupBox1.Controls.Add(Me.MasterTemplateNameTextBox)
        Me.GroupBox1.Controls.Add(CommonTemplateNameLabel)
        Me.GroupBox1.Controls.Add(Me.btnBrowseSaveLocation)
        Me.GroupBox1.Controls.Add(Me.btnBrowseSourceFile)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(675, 249)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Template Settings"
        '
        'TransactionEndRowTextBox
        '
        Me.TransactionEndRowTextBox.Location = New System.Drawing.Point(151, 179)
        Me.TransactionEndRowTextBox.MaxLength = 6
        Me.TransactionEndRowTextBox.Name = "TransactionEndRowTextBox"
        Me.TransactionEndRowTextBox.Size = New System.Drawing.Size(81, 23)
        Me.TransactionEndRowTextBox.TabIndex = 7
        '
        'ValueDatePicker
        '
        Me.ValueDatePicker.CustomFormat = "dd/MMM/yyyy"
        Me.ValueDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ValueDatePicker.Location = New System.Drawing.Point(151, 148)
        Me.ValueDatePicker.Name = "ValueDatePicker"
        Me.ValueDatePicker.Size = New System.Drawing.Size(109, 20)
        Me.ValueDatePicker.TabIndex = 5
        '
        'worksheetComboBox
        '
        Me.worksheetComboBox.DataSource = Me.worksheetBindingSource
        Me.worksheetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.worksheetComboBox.FormattingEnabled = True
        Me.worksheetComboBox.Location = New System.Drawing.Point(151, 115)
        Me.worksheetComboBox.Name = "worksheetComboBox"
        Me.worksheetComboBox.Size = New System.Drawing.Size(291, 22)
        Me.worksheetComboBox.TabIndex = 4
        '
        'CommonTemplateNameComboBox
        '
        Me.CommonTemplateNameComboBox.DataSource = Me.commonTemplateBindingSource
        Me.CommonTemplateNameComboBox.DisplayMember = "CommonTemplate"
        Me.CommonTemplateNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CommonTemplateNameComboBox.FormattingEnabled = True
        Me.CommonTemplateNameComboBox.Location = New System.Drawing.Point(151, 20)
        Me.CommonTemplateNameComboBox.Name = "CommonTemplateNameComboBox"
        Me.CommonTemplateNameComboBox.Size = New System.Drawing.Size(510, 22)
        Me.CommonTemplateNameComboBox.TabIndex = 0
        Me.CommonTemplateNameComboBox.ValueMember = "CommonTemplate"
        '
        'commonTemplateBindingSource
        '
        Me.commonTemplateBindingSource.DataSource = GetType(BTMU.Magic.CommonTemplate.CommonTemplateExcelListCollection)
        '
        'UseValueDateCheckBox
        '
        Me.UseValueDateCheckBox.Location = New System.Drawing.Point(266, 148)
        Me.UseValueDateCheckBox.Name = "UseValueDateCheckBox"
        Me.UseValueDateCheckBox.Size = New System.Drawing.Size(141, 26)
        Me.UseValueDateCheckBox.TabIndex = 6
        Me.UseValueDateCheckBox.Text = "Use Value Date"
        '
        'SourceFileNameTextBox
        '
        Me.SourceFileNameTextBox.AllowDrop = True
        Me.SourceFileNameTextBox.Location = New System.Drawing.Point(151, 84)
        Me.SourceFileNameTextBox.Name = "SourceFileNameTextBox"
        Me.SourceFileNameTextBox.ReadOnly = True
        Me.SourceFileNameTextBox.Size = New System.Drawing.Size(413, 20)
        Me.SourceFileNameTextBox.TabIndex = 2
        '
        'SaveLocationTextBox
        '
        Me.SaveLocationTextBox.Location = New System.Drawing.Point(151, 211)
        Me.SaveLocationTextBox.Name = "SaveLocationTextBox"
        Me.SaveLocationTextBox.ReadOnly = True
        Me.SaveLocationTextBox.Size = New System.Drawing.Size(413, 20)
        Me.SaveLocationTextBox.TabIndex = 8
        '
        'MasterTemplateNameTextBox
        '
        Me.MasterTemplateNameTextBox.Location = New System.Drawing.Point(151, 53)
        Me.MasterTemplateNameTextBox.Name = "MasterTemplateNameTextBox"
        Me.MasterTemplateNameTextBox.ReadOnly = True
        Me.MasterTemplateNameTextBox.Size = New System.Drawing.Size(510, 20)
        Me.MasterTemplateNameTextBox.TabIndex = 1
        '
        'btnBrowseSaveLocation
        '
        Me.btnBrowseSaveLocation.Location = New System.Drawing.Point(574, 207)
        Me.btnBrowseSaveLocation.Name = "btnBrowseSaveLocation"
        Me.btnBrowseSaveLocation.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSaveLocation.TabIndex = 9
        Me.btnBrowseSaveLocation.TabStop = False
        Me.btnBrowseSaveLocation.Text = "&Browse"
        Me.btnBrowseSaveLocation.UseVisualStyleBackColor = True
        '
        'btnBrowseSourceFile
        '
        Me.btnBrowseSourceFile.Location = New System.Drawing.Point(574, 81)
        Me.btnBrowseSourceFile.Name = "btnBrowseSourceFile"
        Me.btnBrowseSourceFile.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSourceFile.TabIndex = 3
        Me.btnBrowseSourceFile.TabStop = False
        Me.btnBrowseSourceFile.Text = "&Browse"
        Me.btnBrowseSourceFile.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(311, 268)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 40)
        Me.btnPreview.TabIndex = 1
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Excel Files(*.xls)|*.xls|Excel 2007 Files(*.xlsx)|*.xlsx"
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.Description = "Select Save Location"
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'frmConvertCommonTransactionExcel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 317)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConvertCommonTransactionExcel"
        Me.ShowIcon = False
        Me.Text = "Convert Common Transaction - Excel (MA3010)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.worksheetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.commonTemplateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBrowseSaveLocation As System.Windows.Forms.Button
    Friend WithEvents btnBrowseSourceFile As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents convertBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UseValueDateCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SourceFileNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SaveLocationTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MasterTemplateNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents worksheetComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CommonTemplateNameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents commonTemplateBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents worksheetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValueDatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents TransactionEndRowTextBox As BTMU.Magic.UI.NumberBox
End Class
