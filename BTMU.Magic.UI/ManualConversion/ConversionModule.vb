'************************************************CHANGE REQUESTS***********************************
'SRS No             Date Modified   Description
'--------------------------------------------------------------------------------------------------
'SRS/BTMU/2010-0021 01/Apr/2010     Change the Mr.Omakase India default file name to 
'                                   _OMAYYYYMMDDhhmmss.txt
'**************************************************************************************************
Imports BTMU.Magic.Common
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.Magic.FileFormat
Imports System.IO

''' <summary>
''' Module provides helper functions specifically meant for Data Conversion 
''' </summary>
''' <remarks></remarks>
Module ConversionModule
    ''' <summary>
    ''' List of GCMS Fields
    ''' </summary>
    ''' <remarks></remarks>
    Public GCMSFields = New String() {"Settlement Account No"}

    ''' <summary>
    ''' Determines whether the value date is mapped for the given template
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template</param>
    ''' <param name="ObjMasterTemplateList">Master Template</param>
    ''' <returns>Returns a boolean value indicating whether the value date is mapped</returns>
    ''' <remarks></remarks>
    Public Function IsValueDateMapped(ByVal ObjDataModel As ICommonTemplate, ByVal ObjMasterTemplateList As MasterTemplateList) As Boolean
        Dim valueDateFieldName As String = String.Empty
        Dim _isValueDateMapped As Boolean = False
        Select Case ObjMasterTemplateList.OutputFormat
            Case GCMSFormat.OutputFormatString, EPSFormat.OutputFormatString, BULKVNFormat.OutputFormatString, iRTMSCIFormat.OutputFormatString, OiRTMSCIFormat.OutputFormatString, iRTMSGCFormat.OutputFormatString, OiRTMSGCFormat.OutputFormatString, iRTMSGDFormat.OutputFormatString, OiRTMSGDFormat.OutputFormatString, iRTMSRMFormat.OutputFormatString, OiRTMSRMFormat.OutputFormatString
                valueDateFieldName = "Value Date"
            Case VPSFormat.OutputFormatString
                valueDateFieldName = "PayDate"
            Case iFTSFormat.OutputFormatString
                valueDateFieldName = "Date"
            Case Else
                valueDateFieldName = ""
        End Select
        If valueDateFieldName <> "" Then
            For Each bankfield As MapBankField In ObjDataModel.MapBankFields
                If bankfield.BankField.ToUpper = valueDateFieldName.ToUpper And _
                   bankfield.MappedSourceFields.Count > 0 Then
                    ' Mapping exists, disable the Value Date Checkbox 
                    _isValueDateMapped = True
                    Exit For
                End If
            Next
        End If
        Return _isValueDateMapped
    End Function

    ''' <summary>
    ''' Generates Converted Transaction File and returns a boolean value upon successful generation of transaction file
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template object</param>
    ''' <param name="ObjMasterTemplateList">Master Template object</param>
    ''' <param name="IsQuickConversion">Boolean value indicating whether its a quick or manual conversion</param>
    ''' <param name="ObjPreview"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GenerateFile(ByVal ObjDataModel As ICommonTemplate, ByVal ObjMasterTemplateList As MasterTemplateList, ByVal IsQuickConversion As Boolean, ByVal ObjPreview As BaseFileFormat) As Boolean
        Dim fileConvertLog As String
        Dim IsOutputFileToBeEncrypted As Boolean
        Dim outputFileExtension As String
        Dim text As String = String.Empty
        Dim isFileGenerated As Boolean = False
        Dim fileName As String = String.Empty
        Dim inputFileName As String = String.Empty
        Dim OutputFile As String = String.Empty
        Dim isHashNeeded As Boolean = False
        Dim outputHashFile As String = String.Empty
        Dim changeOutputFormatName As String = ""
        Dim omakaseOutput2FileName As String = String.Empty
        Dim splitFileName As System.Text.StringBuilder
        Dim num_files As Int16
        Dim num_rows As Int32

        If ObjMasterTemplateList.OutputFormat = GCMSFormat.OutputFormatString AndAlso My.Settings.GCMSHashing = "True" Then isHashNeeded = True

        ' Step 1 :  If Output directory doesnot exists, create the directory
        If Not System.IO.Directory.Exists(ObjPreview.SaveLocation) Then
            System.IO.Directory.CreateDirectory(ObjPreview.SaveLocation)
        End If

        ' Step 2 :  Generate the Output File Name without extension
        Select Case ObjMasterTemplateList.OutputFormat
            Case VPSFormat.OutputFormatString
                fileName = String.Format("{0}F1-{1}", Right(ObjPreview.PreviewDataTable.Rows(0)("ACNO").ToString, 6), ObjPreview.PreviewDataTable.Rows(0)("PayDate").ToString.Replace("-", ""))
            Case Else
                changeOutputFormatName = ObjMasterTemplateList.OutputFormat.Replace("iFTS-2 MultiLine", "iFTS2").Replace("iFTS-2 SingleLine", "iFTS2").Replace("Mr.Omakase India", "_OMA_") 'SRS/BTMU/2010-0021
                'Added for Omakase Format II
                If ObjMasterTemplateList.OutputFormat.Equals(OMAKASEFormatII.OutputFormatString) Then
                    changeOutputFormatName = "_OMA_"
                End If

                '06062018 Added for removing space in AutoCheque ACMS and FTI II (FHK Maggie)
                If ObjMasterTemplateList.OutputFormat.Equals(FundsTransferInstructionFormatII.OutputFormatString) Or ObjMasterTemplateList.OutputFormat.Equals(AutoChequeFormatII.OutputFormatString) Then
                    changeOutputFormatName = changeOutputFormatName.Replace(" ", "")
                End If

                fileName = String.Format("{0}{1}{2}{3}{4}{5}{6}", changeOutputFormatName, Now.Year.ToString("0000", enUSCulture), _
                           Now.Month.ToString("00"), Now.Day.ToString("00"), _
                           Now.Hour.ToString("00"), Now.Minute.ToString("00"), _
                           Now.Second.ToString("00"))

        End Select

        If IsQuickConversion Then
            'initialize default value
            GCMSPlusFormat.SplitFile = False
            ' Read the Setting for OutputFileName
            If Not IsNothingOrEmptyString(ObjPreview.OutputFileName) Then fileName = ObjPreview.OutputFileName.Trim
            OutputFile = System.IO.Path.Combine(ObjPreview.SaveLocation, fileName)

            If ObjMasterTemplateList.OutputFormat = GCMSPlusFormat.OutputFormatString AndAlso ObjPreview.PreviewDataTable.Rows.Count > 1000 Then
                If MessageBox.Show("Number of records in source file exceeds 1000 records acceptable by GCMS Plus. Output file will be split into several files." & vbCrLf & _
                    "Do you want to proceed?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    GCMSPlusFormat.SplitFile = True
                Else
                    MessageBox.Show("Number of records in source file exceeds 1000 records acceptable by GCMS Plus." & _
                        " Please split the files and perform conversion again.", "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GCMSPlusFormat.SplitFile = False
                    Exit Function
                End If
            End If

        Else ' Manual Conversion
            ' Prompt the user with the Output File Name
            inputFileName = InputBox("Enter file name : ", "Input", fileName)
            If inputFileName.Trim() = String.Empty Then Exit Function

            ' Check if the Output File Name is valid
            If Not IsValidFileName(inputFileName) Then
                MessageBox.Show("File name contains invalid character", "Convert Common Template", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Function
            End If
            OutputFile = System.IO.Path.Combine(ObjPreview.SaveLocation, inputFileName)
        End If

        ' Step 3 :  Get the Convert Log File Name
        If TypeOf (ObjDataModel) Is CommonTemplateExcelDetail Or TypeOf (ObjDataModel) Is CommonTemplateTextDetail Then
            fileConvertLog = "ConvertCommon.log"
        Else
            fileConvertLog = "ConvertSWIFT.log"
        End If
        fileConvertLog = System.IO.Path.Combine(Application.StartupPath, fileConvertLog)

        ' Step 4 :  Check if the Source File is already converted
        If System.IO.File.Exists(fileConvertLog) Then
            Dim convertLog As DataTable
            Dim convertLogView As DataView
            convertLog = ReadConvertLog(Application.StartupPath, fileConvertLog)
            convertLogView = convertLog.DefaultView
            ' Get the date created and date last modified of the file
            Dim _fileInfo As New System.IO.FileInfo(ObjPreview.SourceFileName)
            convertLogView.RowFilter = "FileName='" & ObjPreview.SourceFileName & "'" & _
                                       " AND DateCreated='" & _fileInfo.CreationTime.ToString & "'" & _
                                       " AND DateModified='" & _fileInfo.LastWriteTime.ToString & "'"
            If convertLogView.Count > 0 Then
                If MessageBox.Show("Source File has already been converted. Do you want to convert it again?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                    Return False
                End If
            End If
        End If

        ' Step 5 :  Get the Output File Extension
        If ObjMasterTemplateList.IsFixed Then
            Dim objMaster As MasterTemplateFixed
            objMaster = (New MasterTemplate.MasterTemplateFixed).LoadFromFile2 _
                    (System.IO.Path.Combine(MasterTemplateModule.MasterTemplateViewFolder, _
                                            ObjMasterTemplateList.TemplateName) & _
                                            MasterTemplateFileExtension)
            If objMaster.EncryptCustomerOutputFile Then
                ' Encrypt the file
                IsOutputFileToBeEncrypted = True
            Else
                ' Dont encrypt the file
                IsOutputFileToBeEncrypted = False
            End If
            outputFileExtension = objMaster.CustomerOutputExtension
            OutputFile = OutputFile & "." & outputFileExtension

            Try
                If Not IsQuickConversion Then
                    If System.IO.File.Exists(OutputFile) Then
                        If MessageBox.Show("File exists.  Do you want to overwrite?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                            Exit Function
                        End If
                    End If
                End If

                If Not OutputFile = String.Empty Then
                    ObjPreview.OutputFileName = OutputFile
                    If IsNothingOrEmptyString(ObjPreview.IntermediaryFileName) Then
                        isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.SourceFileName, OutputFile, objMaster)
                    Else
                        isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.IntermediaryFileName, OutputFile, objMaster)
                    End If
                    If isFileGenerated Then
                        MessageBox.Show(String.Format("Conversion has been successfully done to {0}.", OutputFile), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show(String.Format("Error while generating the Output File : {0}.", OutputFile), "Failure", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Function
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
                Exit Function
            End Try

        Else
            Dim objMaster As MasterTemplateNonFixed
            objMaster = (New MasterTemplate.MasterTemplateNonFixed).LoadFromFile2 _
                        (System.IO.Path.Combine(MasterTemplateModule.MasterTemplateViewFolder, _
                                                ObjMasterTemplateList.TemplateName) & _
                                                MasterTemplateFileExtension)
            If objMaster.EncryptCustomerOutputFile Then
                ' Encrypt the file
                IsOutputFileToBeEncrypted = True
            Else
                ' Dont encrypt the file
                IsOutputFileToBeEncrypted = False
            End If
            outputFileExtension = objMaster.CustomerOutputExtension
            outputHashFile = OutputFile & "HASH.txt"
            OutputFile = OutputFile & "." & outputFileExtension

            Try

                If Not IsQuickConversion Then
                    If System.IO.File.Exists(OutputFile) Then
                        If MessageBox.Show("File exists.  Do you want to overwrite?", "Convert Common Transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                            Exit Function
                        End If
                    End If
                End If

                If Not OutputFile = String.Empty Then

                    'Added for GCMS Plus format
                    'Modified by Kay on 20 Jun 2011 
                    num_files = Math.Floor(ObjPreview.PreviewDataTable.Rows.Count / 1000)
                    If ObjMasterTemplateList.OutputFormat.Equals(GCMSPlusFormat.OutputFormatString) AndAlso GCMSPlusFormat.SplitFile Then
                        'To split the files for each 1000 records
                        For num_rows = 0 To num_files
                            splitFileName = New System.Text.StringBuilder(OutputFile.Substring(0, OutputFile.IndexOf(".")))
                            If splitFileName.ToString.LastIndexOf("_") = splitFileName.ToString().Length - 2 Then
                                splitFileName.Remove(splitFileName.ToString().Length - 2, 2)
                            End If
                            splitFileName = splitFileName.Append("_" + (num_rows + 1).ToString)
                            ObjPreview.OutputFileName = splitFileName.ToString + "." + outputFileExtension

                            If IsNothingOrEmptyString(ObjPreview.IntermediaryFileName) Then
                                isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.SourceFileName, ObjPreview.OutputFileName, objMaster)
                            Else
                                isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.IntermediaryFileName, ObjPreview.OutputFileName, objMaster)
                            End If

                            'update name lists
                            text = IIf(num_rows = 0, ObjPreview.OutputFileName.Substring(ObjPreview.OutputFileName.LastIndexOf("\") + 1), text + ", " + ObjPreview.OutputFileName.Substring(ObjPreview.OutputFileName.LastIndexOf("\") + 1))

                        Next
                        'Update the file names
                        OutputFile = text
                    Else

                        ObjPreview.OutputFileName = OutputFile
                        If IsNothingOrEmptyString(ObjPreview.IntermediaryFileName) Then
                            isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.SourceFileName, OutputFile, objMaster)
                        Else
                            isFileGenerated = ObjPreview.GenerateFile(frmLogin.GsUserName, ObjPreview.IntermediaryFileName, OutputFile, objMaster)
                        End If
                    End If

                    If isFileGenerated Then
                        'If ObjPreview.SourceFormat.Trim.ToUpper <> "EXCEL" Then
                        If isHashNeeded Then
                            Dim allText As String = File.ReadAllText(OutputFile)
                            Dim allHashText As String = HelperModule.ComputeHashAsHexString(allText, "SHA512")
                            'edited by fhk on 15-sep-2015, convert text file to UTF-8 with BOM format
                            Dim utf8WithBom As New System.Text.UTF8Encoding(True)
                            'Dim sWriter As New StreamWriter(outputHashFile, False)
                            Dim sWriter As New StreamWriter(outputHashFile, False, utf8WithBom)
                            'end by fhk on 15-sep-2015
                            sWriter.WriteLine(allHashText)
                            sWriter.Close()
                        End If
                        'End If


                        'to display the result message to user
                        If isHashNeeded Then
                            'For Hashed files
                            MessageBox.Show(String.Format("Conversion has been successfully done to {0}." & vbNewLine _
                                                          & "Output Hash File : {1}", OutputFile, outputHashFile), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)


                        ElseIf objMaster.OutputFormat = OMAKASEFormat.OutputFormatString Then
                            'Only for Omakase Format
                            If IsOutputFileToBeEncrypted Then
                                If OutputFile.Length > outputFileExtension.Length Then
                                    omakaseOutput2FileName = OutputFile.Substring(0, OutputFile.Length - (outputFileExtension.Length + 1)) & "FYI." & outputFileExtension
                                Else
                                    omakaseOutput2FileName = "FYI." & outputFileExtension
                                End If
                                'omakaseOutput2FileName = OutputFile.Substring(0, OutputFile.Length - 7) & "." & outputFileExtension ' To remove FYI.txt from the output filename
                                MessageBox.Show(String.Format("Conversion has been successfully done to {0}." & vbNewLine _
                                                              & "Decrypted Output File : {1}", OutputFile, omakaseOutput2FileName), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Else
                                MessageBox.Show(String.Format("Conversion has been successfully done to {0}.", OutputFile), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If


                        Else
                            'All other file formats
                            MessageBox.Show(String.Format("Conversion has been successfully done to {0}.", OutputFile), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    Else
                        MessageBox.Show(String.Format("Error while generating the Output File : {0}.", OutputFile), "Failure", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Function
                    End If
                End If


            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
                Exit Function
            End Try
        End If

        If isFileGenerated Then
            ' Write to Activity Log
            ObjPreview.WriteActivityLog(frmLogin.GsUserName, ObjPreview.SourceFileName, ObjPreview.OutputFileName)
            ' Write to Convert Log
            Dim _fileinfo As New System.IO.FileInfo(ObjPreview.SourceFileName)
            WriteConvertLog(ObjPreview.SourceFileName, _fileinfo.CreationTime.ToString, _fileinfo.LastWriteTime.ToString, fileConvertLog)
        End If
        Return isFileGenerated

    End Function

    
    ''' <summary>
    ''' This function is to Get Value Date
    ''' </summary>
    ''' <param name="ValueDate">Value Date</param>
    ''' <returns>Returns Value Date</returns>
    ''' <remarks></remarks>
    Public Function GetValueDate(ByVal ValueDate As Integer) As String
        Dim returnValueDate As String
        Dim addedDate As Date
        addedDate = Date.Today.AddDays(ValueDate)

        If Weekday(addedDate) = 1 Then
            returnValueDate = (addedDate.AddDays(1))
        ElseIf Weekday(addedDate) = 7 Then
            returnValueDate = (addedDate.AddDays(2))
        Else
            returnValueDate = addedDate.ToString
        End If
        Return returnValueDate
    End Function

    ''' <summary>
    ''' This Function is to Return Conversion Log
    ''' </summary>
    ''' <param name="filepath">Path of Log File</param>
    ''' <param name="filename">Name of Log File</param>
    ''' <returns>Returns a data table of Conversion Log</returns>
    ''' <remarks></remarks>
    Public Function GetConvertLog(ByVal filepath As String, ByVal filename As String) As DataTable
        Dim convertLog As DataTable
        convertLog = ReadConvertLog(filepath, filename)
        Return convertLog
    End Function

    Public Function DisableValueDateControl(ByVal format As String) As Boolean
        Dim formatNotUsingValueDates() As String = New String() {"iFTS-2 SingleLine", "iFTS-2 MultiLine", "CWS"}
        For Each fileformat As String In formatNotUsingValueDates
            If fileformat.Equals(format, StringComparison.InvariantCultureIgnoreCase) Then Return True
        Next
        Return False
    End Function
End Module
