Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports System.Data.OleDb
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat
''' <summary>
''' Form for Convert Common Transaction Template - Excel
''' </summary>
''' <remarks></remarks>
Public Class frmConvertCommonTransactionExcel
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Private _datamodel As CommonTemplateExcelDetail
    Private _masterTemplateList As MasterTemplateList
    Private _objPreview As BaseFileFormat

#Region " On Load "
    Private Sub frmConvertCommonTransactionExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            commonTemplateBindingSource.DataSource = CommonTemplateModule.ListAllExcelCommonTemplates()
            CommonTemplateNameComboBox.SelectedIndex = -1
            MasterTemplateNameTextBox.Clear()
            SaveLocationTextBox.Text = My.Settings.ConvertCommonFolder
            ' Check the UseValueDate checkbox and enable the ValueDatePicker
            UseValueDateCheckBox.Checked = True
            ValueDatePicker.Enabled = True
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading Excel Template : ", ex.Message.ToString)
        End Try
    End Sub
    ''' <summary>
    ''' Initializes the Form with Template Data
    ''' </summary>
    ''' <param name="objPreview">Object containing the template data</param>
    ''' <remarks></remarks>
    Public Sub SetData(ByRef objPreview As BaseFileFormat)
        Try
            ' Assign the values to each fields
            _objPreview = objPreview
            CommonTemplateNameComboBox.SelectedValue = _objPreview.CommonTemplateName
            MasterTemplateNameTextBox.Text = _objPreview.MasterTemplateName
            SourceFileNameTextBox.Text = _objPreview.SourceFileName
            PopulateWorksheetNames()
            worksheetComboBox.SelectedValue = _objPreview.WorksheetName
            worksheetComboBox.SelectedText = _objPreview.WorksheetName
            TransactionEndRowTextBox.Text = _objPreview.TransactionEndRow
            SaveLocationTextBox.Text = _objPreview.SaveLocation
            ValueDatePicker.Value = _objPreview.ValueDate
            If _objPreview.UseValueDate Then
                UseValueDateCheckBox.Checked = True
            Else
                UseValueDateCheckBox.Checked = False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading Excel Template : ", ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " Source File Drag and Drop "
    Private Sub SourceFileNameTextBox_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileNameTextBox.DragDrop
        Dim filename As String = ""
        Dim Filenames As String()
        SourceFileNameTextBox.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".xls") Or filename.EndsWith(".xlsx")) Then Exit Sub
        If filename <> "" Then
            SourceFileNameTextBox.Text = filename
            PopulateWorksheetNames()
        End If
    End Sub

    Private Sub SourceFileNameTextBox_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileNameTextBox.DragEnter
        ' ''Dim format As String
        '' ''For Each format In e.Data.GetFormats
        '' ''    If format = System.Windows.Forms.DataFormats.FileDrop Then
        '' ''        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
        '' ''            e.Effect = DragDropEffects.All
        '' ''        Else
        '' ''            e.Effect = DragDropEffects.None
        '' ''        End If
        '' ''        Exit For
        '' ''    End If
        '' ''Next
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub
#End Region

#Region " Populate Worksheet Names "
    Private Sub PopulateWorksheetNames()
        Try
            Dim worksheetDataTable As DataTable

            worksheetDataTable = GetWorksheetNames(SourceFileNameTextBox.Text.Trim)
            If worksheetDataTable Is Nothing Then Exit Sub
            worksheetBindingSource.DataSource = worksheetDataTable
            worksheetComboBox.DisplayMember = "WorksheetName"
            worksheetComboBox.ValueMember = "WorksheetName"
            worksheetComboBox.SelectedValue = worksheetDataTable.Rows(0)(0)


        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error population Worksheet Names : ", ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " Event Handlers for Browse Source, Browse Save location, Preview "
    Private Sub btnBrowseSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSourceFile.Click
        Dim dlgResult As DialogResult
        dlgResult = OpenFileDialog1.ShowDialog()
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SourceFileNameTextBox.Text = OpenFileDialog1.FileName
            PopulateWorksheetNames()
        End If

    End Sub

    Private Sub btnBrowseSaveLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSaveLocation.Click
        Dim saveFolder As New FolderBrowserDialog
        Dim dlgResult As DialogResult

        FolderBrowserDialog1.SelectedPath = My.Settings.ConvertCommonFolder
        dlgResult = FolderBrowserDialog1.ShowDialog
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SaveLocationTextBox.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If Not IsFieldValid() Then Exit Sub
            If _masterTemplateList Is Nothing Then Exit Sub
            If _datamodel Is Nothing Then Exit Sub
            If _masterTemplateList.OutputFormat Is Nothing Then
                MessageBox.Show(MsgReader.GetString("E02000020"), "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'MessageBox.Show("Master Template does not exist.", "Convert Common Transaction - SWIFT", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            ' Show the hash value of the source file, if GCMS Hashing is True.
            If Not ShowHash() Then Exit Sub

            Dim commonTemplate As CommonTemplateExcelList
            If CommonTemplateNameComboBox.SelectedIndex = -1 Then Exit Sub
            If commonTemplateBindingSource Is Nothing Then Exit Sub
            commonTemplate = commonTemplateBindingSource.Current
            If commonTemplate Is Nothing Then Exit Sub

            _objPreview = BaseFileFormat.CreateFileFormat(_masterTemplateList.OutputFormat)
            _objPreview.CommonTemplateName = CommonTemplate.CommonTemplate
            _objPreview.MasterTemplateName = CommonTemplate.OutputTemplate

            If _objPreview Is Nothing Then Exit Sub


            ' Assign all the values to the object
            _objPreview.SourceFormat = "EXCEL"
            _objPreview.CommonTemplateName = CommonTemplateNameComboBox.SelectedValue
            _objPreview.MasterTemplateName = MasterTemplateNameTextBox.Text
            _objPreview.SourceFileName = SourceFileNameTextBox.Text
            _objPreview.WorksheetName = worksheetComboBox.SelectedValue
            _objPreview.ValueDate = ValueDatePicker.Value
            If UseValueDateCheckBox.Checked Then
                _objPreview.UseValueDate = True
            Else
                _objPreview.UseValueDate = False
            End If
            Dim _remove As Integer
            If Integer.TryParse(TransactionEndRowTextBox.Number.ToString, _remove) Then
                _objPreview.TransactionEndRow = _remove
            End If

            _objPreview.SaveLocation = SaveLocationTextBox.Text

            _objPreview.ValidateBeforeConversion()
            frmPreview.ShowPreview(_datamodel, _masterTemplateList, False, Nothing, _objPreview)
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", ex.Message)
        End Try
    End Sub

#End Region

#Region " On Common Template Selection "
    Private Sub CommonTemplateNameComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommonTemplateNameComboBox.LostFocus
        Dim commonTemplate As CommonTemplateExcelList
        Try
            MasterTemplateNameTextBox.Text = ""
            If CommonTemplateNameComboBox.SelectedIndex = -1 Then Exit Sub
            If commonTemplateBindingSource Is Nothing Then Exit Sub
            commonTemplate = commonTemplateBindingSource.Current
            If commonTemplate Is Nothing Then Exit Sub
            MasterTemplateNameTextBox.Text = commonTemplate.OutputTemplate
            _datamodel = CommonTemplateModule.LoadExcelCommonTemplate(commonTemplate.CommonTemplate)
            _masterTemplateList = CommonTemplateModule.GetMasterTemplateList(commonTemplate.OutputTemplate)
            If _masterTemplateList.OutputFormat Is Nothing Then Exit Sub
          

            If IsValueDateMapped(_datamodel, _masterTemplateList) Then
                ValueDatePicker.Enabled = False
                UseValueDateCheckBox.Checked = False
            Else
                ValueDatePicker.Enabled = True
                UseValueDateCheckBox.Checked = True
            End If

            If DisableValueDateControl(_masterTemplateList.OutputFormat) Then
                ValueDatePicker.Enabled = False
                UseValueDateCheckBox.Enabled = False
            Else
                ValueDatePicker.Enabled = True
                UseValueDateCheckBox.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region

#Region " On Use Value Date Selection "
    Private Sub UseValueDateCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UseValueDateCheckBox.Click
        If UseValueDateCheckBox.Checked Then
            ValueDatePicker.Enabled = True
        Else
            ValueDatePicker.Enabled = False
        End If
    End Sub
#End Region

#Region " Validation Before Preview "
    Private Function IsFieldValid() As Boolean
        Dim errMessage As String = String.Empty
        If CommonTemplateNameComboBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Common Template Name") & vbNewLine
        End If
        If SourceFileNameTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Source File Name") & vbNewLine
        End If
        If worksheetComboBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Worksheet Name") & vbNewLine
        End If
        If TransactionEndRowTextBox.Number.ToString.Trim <> String.Empty Then
            If TransactionEndRowTextBox.Number = 0 Then
                errMessage &= String.Format(MsgReader.GetString("E03010030"), "Transaction Ends at Row") & vbNewLine
            End If
        End If
        If SaveLocationTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Save Location") & vbNewLine
        End If
        If errMessage = String.Empty Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", errMessage)
            Return False
        End If
    End Function
#End Region

    Private Function ShowHash() As Boolean
        If _masterTemplateList.OutputFormat = GCMSFormat.OutputFormatString AndAlso My.Settings.GCMSHashing = "True" Then
            Dim objHashing As New frmGCMSHashingView
            objHashing.GetHash(SourceFileNameTextBox.Text)
            If objHashing.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return False
        End If
        Return True
    End Function
End Class