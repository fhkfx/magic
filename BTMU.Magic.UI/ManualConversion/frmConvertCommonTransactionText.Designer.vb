<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConvertCommonTransactionText
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConvertCommonTransactionText))
        Me.btnPreview = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnBrowseDefinitionFile = New System.Windows.Forms.Button
        Me.txtDefinitionFile = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.RemoveRowsTextBox = New BTMU.Magic.UI.NumberBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnBrowseSaveLocation = New System.Windows.Forms.Button
        Me.btnBrowseSourceFile = New System.Windows.Forms.Button
        Me.ValueDateCheckBox = New System.Windows.Forms.CheckBox
        Me.ValueDatePicker = New System.Windows.Forms.DateTimePicker
        Me.SaveLocationTextBox = New System.Windows.Forms.TextBox
        Me.EliminateCharacterTextBox = New System.Windows.Forms.TextBox
        Me.SourceFileTextBox = New System.Windows.Forms.TextBox
        Me.MasterTemplateTextBox = New System.Windows.Forms.TextBox
        Me.CommonTemplateComboBox = New System.Windows.Forms.ComboBox
        Me.commonTemplateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.convertBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.DefinitionFileOpenDialog = New System.Windows.Forms.OpenFileDialog
        Me.GroupBox1.SuspendLayout()
        CType(Me.commonTemplateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(308, 310)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 40)
        Me.btnPreview.TabIndex = 1
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnBrowseDefinitionFile)
        Me.GroupBox1.Controls.Add(Me.txtDefinitionFile)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.RemoveRowsTextBox)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.btnBrowseSaveLocation)
        Me.GroupBox1.Controls.Add(Me.btnBrowseSourceFile)
        Me.GroupBox1.Controls.Add(Me.ValueDateCheckBox)
        Me.GroupBox1.Controls.Add(Me.ValueDatePicker)
        Me.GroupBox1.Controls.Add(Me.SaveLocationTextBox)
        Me.GroupBox1.Controls.Add(Me.EliminateCharacterTextBox)
        Me.GroupBox1.Controls.Add(Me.SourceFileTextBox)
        Me.GroupBox1.Controls.Add(Me.MasterTemplateTextBox)
        Me.GroupBox1.Controls.Add(Me.CommonTemplateComboBox)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(675, 291)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Template Settings"
        '
        'btnBrowseDefinitionFile
        '
        Me.btnBrowseDefinitionFile.Location = New System.Drawing.Point(573, 121)
        Me.btnBrowseDefinitionFile.Name = "btnBrowseDefinitionFile"
        Me.btnBrowseDefinitionFile.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseDefinitionFile.TabIndex = 20
        Me.btnBrowseDefinitionFile.Text = "&Browse"
        Me.btnBrowseDefinitionFile.UseVisualStyleBackColor = True
        '
        'txtDefinitionFile
        '
        Me.txtDefinitionFile.Location = New System.Drawing.Point(158, 126)
        Me.txtDefinitionFile.Name = "txtDefinitionFile"
        Me.txtDefinitionFile.ReadOnly = True
        Me.txtDefinitionFile.Size = New System.Drawing.Size(408, 20)
        Me.txtDefinitionFile.TabIndex = 19
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 131)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 14)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Definition File :"
        '
        'RemoveRowsTextBox
        '
        Me.RemoveRowsTextBox.Location = New System.Drawing.Point(158, 192)
        Me.RemoveRowsTextBox.MaxLength = 6
        Me.RemoveRowsTextBox.Name = "RemoveRowsTextBox"
        Me.RemoveRowsTextBox.Size = New System.Drawing.Size(75, 23)
        Me.RemoveRowsTextBox.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 200)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 14)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Remove Rows from End :"
        '
        'btnBrowseSaveLocation
        '
        Me.btnBrowseSaveLocation.Location = New System.Drawing.Point(572, 251)
        Me.btnBrowseSaveLocation.Name = "btnBrowseSaveLocation"
        Me.btnBrowseSaveLocation.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSaveLocation.TabIndex = 9
        Me.btnBrowseSaveLocation.Text = "&Browse"
        Me.btnBrowseSaveLocation.UseVisualStyleBackColor = True
        '
        'btnBrowseSourceFile
        '
        Me.btnBrowseSourceFile.Location = New System.Drawing.Point(572, 86)
        Me.btnBrowseSourceFile.Name = "btnBrowseSourceFile"
        Me.btnBrowseSourceFile.Size = New System.Drawing.Size(87, 27)
        Me.btnBrowseSourceFile.TabIndex = 3
        Me.btnBrowseSourceFile.Text = "&Browse"
        Me.btnBrowseSourceFile.UseVisualStyleBackColor = True
        '
        'ValueDateCheckBox
        '
        Me.ValueDateCheckBox.AutoSize = True
        Me.ValueDateCheckBox.Location = New System.Drawing.Point(270, 167)
        Me.ValueDateCheckBox.Name = "ValueDateCheckBox"
        Me.ValueDateCheckBox.Size = New System.Drawing.Size(101, 18)
        Me.ValueDateCheckBox.TabIndex = 5
        Me.ValueDateCheckBox.Text = "Use Value Date"
        Me.ValueDateCheckBox.UseVisualStyleBackColor = True
        '
        'ValueDatePicker
        '
        Me.ValueDatePicker.CustomFormat = "dd/MMM/yyyy"
        Me.ValueDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ValueDatePicker.Location = New System.Drawing.Point(158, 164)
        Me.ValueDatePicker.Name = "ValueDatePicker"
        Me.ValueDatePicker.Size = New System.Drawing.Size(106, 20)
        Me.ValueDatePicker.TabIndex = 4
        '
        'SaveLocationTextBox
        '
        Me.SaveLocationTextBox.AllowDrop = True
        Me.SaveLocationTextBox.Location = New System.Drawing.Point(158, 255)
        Me.SaveLocationTextBox.Name = "SaveLocationTextBox"
        Me.SaveLocationTextBox.ReadOnly = True
        Me.SaveLocationTextBox.Size = New System.Drawing.Size(408, 20)
        Me.SaveLocationTextBox.TabIndex = 8
        '
        'EliminateCharacterTextBox
        '
        Me.EliminateCharacterTextBox.Location = New System.Drawing.Point(158, 224)
        Me.EliminateCharacterTextBox.MaxLength = 1
        Me.EliminateCharacterTextBox.Name = "EliminateCharacterTextBox"
        Me.EliminateCharacterTextBox.Size = New System.Drawing.Size(42, 20)
        Me.EliminateCharacterTextBox.TabIndex = 7
        '
        'SourceFileTextBox
        '
        Me.SourceFileTextBox.AllowDrop = True
        Me.SourceFileTextBox.Location = New System.Drawing.Point(158, 89)
        Me.SourceFileTextBox.Name = "SourceFileTextBox"
        Me.SourceFileTextBox.ReadOnly = True
        Me.SourceFileTextBox.Size = New System.Drawing.Size(408, 20)
        Me.SourceFileTextBox.TabIndex = 2
        '
        'MasterTemplateTextBox
        '
        Me.MasterTemplateTextBox.Location = New System.Drawing.Point(158, 57)
        Me.MasterTemplateTextBox.Name = "MasterTemplateTextBox"
        Me.MasterTemplateTextBox.ReadOnly = True
        Me.MasterTemplateTextBox.Size = New System.Drawing.Size(501, 20)
        Me.MasterTemplateTextBox.TabIndex = 1
        '
        'CommonTemplateComboBox
        '
        Me.CommonTemplateComboBox.DataSource = Me.commonTemplateBindingSource
        Me.CommonTemplateComboBox.DisplayMember = "CommonTemplate"
        Me.CommonTemplateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CommonTemplateComboBox.FormattingEnabled = True
        Me.CommonTemplateComboBox.Location = New System.Drawing.Point(158, 25)
        Me.CommonTemplateComboBox.Name = "CommonTemplateComboBox"
        Me.CommonTemplateComboBox.Size = New System.Drawing.Size(501, 22)
        Me.CommonTemplateComboBox.TabIndex = 0
        Me.CommonTemplateComboBox.ValueMember = "CommonTemplate"
        '
        'commonTemplateBindingSource
        '
        Me.commonTemplateBindingSource.DataSource = GetType(BTMU.Magic.CommonTemplate.TextTemplateListCollection)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 260)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 14)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Save Location :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 228)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 14)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Eliminate Character :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Value Date :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Source File Name :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Master Template Name :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Common Template Name :"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DereferenceLinks = False
        Me.OpenFileDialog1.Filter = "txt files|*.txt|csv files|*.csv|All files|*.*"
        '
        'DefinitionFileOpenDialog
        '
        Me.DefinitionFileOpenDialog.Filter = """Magic Definition Templates (*.MDT)|*.MDT|All Files (*.*)|*.*"""
        '
        'frmConvertCommonTransactionText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 362)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConvertCommonTransactionText"
        Me.ShowIcon = False
        Me.Text = "Convert Common Transaction - Text (MA3000)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.commonTemplateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.convertBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBrowseSaveLocation As System.Windows.Forms.Button
    Friend WithEvents btnBrowseSourceFile As System.Windows.Forms.Button
    Friend WithEvents ValueDateCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ValueDatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents SaveLocationTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EliminateCharacterTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SourceFileTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MasterTemplateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CommonTemplateComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents commonTemplateBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents convertBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents RemoveRowsTextBox As BTMU.Magic.UI.NumberBox
    Friend WithEvents btnBrowseDefinitionFile As System.Windows.Forms.Button
    Friend WithEvents txtDefinitionFile As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DefinitionFileOpenDialog As System.Windows.Forms.OpenFileDialog
End Class
