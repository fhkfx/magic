Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports System.IO
Imports System.Data.OleDb
Imports BTMU.Magic.Common
Imports BTMU.Magic.CommonTemplate
Imports BTMU.Magic.MasterTemplate
Imports BTMU.Magic.FileFormat
Imports System.Globalization
''' <summary>
''' Form for Convert Common Transaction Template - Text
''' </summary>
''' <remarks></remarks>
Public Class frmConvertCommonTransactionText
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Private _datamodel As CommonTemplateTextDetail
    Private _masterTemplateList As MasterTemplateList
    Private _objPreview As basefileformat

#Region " On Load "
    Private Sub frmConvertCommonTransactionText_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            commonTemplateBindingSource.DataSource = CommonTemplateModule.ListAllTextCommonTemplates
            CommonTemplateComboBox.SelectedIndex = -1
            MasterTemplateTextBox.Clear()
            SaveLocationTextBox.Text = My.Settings.ConvertCommonFolder
            ' Check UseValueDate checkbox and enable ValueDatePicker
            ValueDateCheckBox.Checked = True
            ValueDatePicker.Enabled = True
            RemoveRowsTextBox.Number = "0"
            btnBrowseDefinitionFile.Enabled = False

        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading Text Template : ", ex.Message.ToString)
        End Try

    End Sub
    ''' <summary>
    ''' Initializes the GUI with Template Data
    ''' </summary>
    ''' <param name="objPreview">Preview Object Containing Template Data</param>
    ''' <remarks></remarks>
    Public Sub SetData(ByRef objPreview As BaseFileFormat)
        Try
            ' Assign the values to each fields
            _objPreview = objPreview
            CommonTemplateComboBox.SelectedValue = _objPreview.CommonTemplateName
            MasterTemplateTextBox.Text = _objPreview.MasterTemplateName
            SourceFileTextBox.Text = _objPreview.SourceFileName
            RemoveRowsTextBox.Text = _objPreview.RemoveRowsFromEnd
            SaveLocationTextBox.Text = _objPreview.SaveLocation
            ValueDatePicker.Value = _objPreview.ValueDate
            If _objPreview.UseValueDate Then
                ValueDateCheckBox.Checked = True
            Else
                ValueDateCheckBox.Checked = False
            End If
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error loading Text Template : ", ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " Source File Drag and Drop "
    Private Sub SourceFileTextBox_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragDrop
        Dim filename As String = ""
        Dim Filenames As String()
        SourceFileTextBox.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not (filename.EndsWith(".txt") Or filename.EndsWith(".csv") Or filename.EndsWith(".xls")) Then Exit Sub
        If filename <> "" Then
            SourceFileTextBox.Text = filename
        End If
    End Sub
    Private Sub SourceFileTextBox_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles SourceFileTextBox.DragEnter
        '' ''Dim format As String
        '' ''For Each format In e.Data.GetFormats()
        '' ''    If format = System.Windows.Forms.DataFormats.FileDrop Then
        '' ''        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
        '' ''            e.Effect = DragDropEffects.All
        '' ''        Else
        '' ''            e.Effect = DragDropEffects.None
        '' ''        End If
        '' ''        Exit For
        '' ''    End If
        '' ''Next
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub
    Private Sub txtDefinitionFile_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtDefinitionFile.DragDrop
        Dim filename As String = ""
        Dim Filenames As String()
        txtDefinitionFile.Focus()
        Filenames = e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop, True)
        For Each file As String In Filenames
            If System.IO.File.Exists(file) Then
                filename = file
                Exit For
            End If
        Next

        If Not filename.EndsWith(".mdt") Then Exit Sub
        If filename <> "" Then
            txtDefinitionFile.Text = filename
        End If
    End Sub

    Private Sub txtDefinitionFile_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtDefinitionFile.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub
#End Region

#Region " On Common Template Selection "
    Private Sub CommonTemplateComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommonTemplateComboBox.LostFocus

        Dim commonTemplate As TextTemplateList
        Try
            MasterTemplateTextBox.Text = ""
            SourceFileTextBox.Text = ""
            If CommonTemplateComboBox.SelectedIndex = -1 Then Exit Sub
            If commonTemplateBindingSource Is Nothing Then Exit Sub
            commonTemplate = commonTemplateBindingSource.Current
            If commonTemplate Is Nothing Then Exit Sub
            MasterTemplateTextBox.Text = commonTemplate.OutputTemplate
            _datamodel = CommonTemplateModule.LoadTextCommonTemplate(commonTemplate.CommonTemplate)
            _masterTemplateList = CommonTemplateModule.GetMasterTemplateList(commonTemplate.OutputTemplate)
            If _masterTemplateList.OutputFormat Is Nothing Then Exit Sub
            
            If IsValueDateMapped(_datamodel, _masterTemplateList) Then
                ValueDatePicker.Enabled = False
                ValueDateCheckBox.Checked = False
            Else
                ValueDatePicker.Enabled = True
                ValueDateCheckBox.Checked = True
            End If

            If DisableValueDateControl(_masterTemplateList.OutputFormat) Then
                ValueDatePicker.Enabled = False
                ValueDateCheckBox.Enabled = False
            Else
                ValueDatePicker.Enabled = True
                ValueDateCheckBox.Enabled = True
            End If

            If IsNothingOrEmptyString(_datamodel.DefinitionFileName) Then
                btnBrowseDefinitionFile.Enabled = False
                txtDefinitionFile.Text = ""
            Else
                btnBrowseDefinitionFile.Enabled = True
                txtDefinitionFile.Text = _datamodel.DefinitionFileName
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region " On Use Value Date Selection "
    Private Sub ValueDateCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ValueDateCheckBox.Click
        If ValueDateCheckBox.Checked Then
            ValueDatePicker.Enabled = True
        Else
            ValueDatePicker.Enabled = False
        End If
    End Sub
#End Region

#Region " Event Handlers for Browse Source,Browse Save Location Preview "
    Private Sub btnBrowseSourceFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSourceFile.Click
        Dim dlgResult As DialogResult
        dlgResult = OpenFileDialog1.ShowDialog()
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SourceFileTextBox.Text = OpenFileDialog1.FileName
        End If
    End Sub
    Private Sub btnBrowseDefinitionFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseDefinitionFile.Click
        Dim dlgResult As DialogResult
        dlgResult = DefinitionFileOpenDialog.ShowDialog
        If dlgResult = Windows.Forms.DialogResult.OK Then
            txtDefinitionFile.Text = DefinitionFileOpenDialog.FileName
        End If
    End Sub
    Private Sub btnBrowseSaveLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSaveLocation.Click
        Dim saveFolder As New FolderBrowserDialog
        Dim dlgResult As DialogResult
        FolderBrowserDialog1.SelectedPath = My.Settings.ConvertCommonFolder
        dlgResult = FolderBrowserDialog1.ShowDialog
        If dlgResult = Windows.Forms.DialogResult.OK Then
            SaveLocationTextBox.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim convertedSrcFilename As String = String.Empty
        Try
            
            If Not IsFieldValid() Then Exit Sub
            If _masterTemplateList Is Nothing Then Exit Sub
            If _datamodel Is Nothing Then Exit Sub
            If _masterTemplateList.OutputFormat Is Nothing Then
                MessageBox.Show(MsgReader.GetString("E02000020"), "Convert Common Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'MessageBox.Show("Master Template does not exist.", "Convert Common Transaction - SWIFT", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            ' Show the hash value of the source file, if GCMS Hashing is True.
            If Not ShowHash() Then Exit Sub

            Dim commonTemplate As TextTemplateList
            If CommonTemplateComboBox.SelectedIndex = -1 Then Exit Sub
            If commonTemplateBindingSource Is Nothing Then Exit Sub
            commonTemplate = commonTemplateBindingSource.Current
            If commonTemplate Is Nothing Then Exit Sub

            _objPreview = BaseFileFormat.CreateFileFormat(_masterTemplateList.OutputFormat)
            _objPreview.CommonTemplateName = commonTemplate.CommonTemplate
            _objPreview.MasterTemplateName = commonTemplate.OutputTemplate

            If _objPreview Is Nothing Then Exit Sub
            If Not IsFieldValid() Then Exit Sub
            ' Assign all the values to the object
            _objPreview.SourceFormat = "TXT"
            _objPreview.CommonTemplateName = CommonTemplateComboBox.SelectedValue
            _objPreview.MasterTemplateName = MasterTemplateTextBox.Text
            _objPreview.SourceFileName = SourceFileTextBox.Text
            _objPreview.ValueDate = ValueDatePicker.Value
            If ValueDateCheckBox.Checked Then
                _objPreview.UseValueDate = True
            Else
                _objPreview.UseValueDate = False
            End If
            Dim _remove As Integer
            If Integer.TryParse(RemoveRowsTextBox.Number.ToString, _remove) Then
                _objPreview.RemoveRowsFromEnd = _remove
            End If
            _objPreview.EliminatedCharacter = EliminateCharacterTextBox.Text.Trim
            _objPreview.SaveLocation = SaveLocationTextBox.Text
            _objPreview.DefinitionFile = txtDefinitionFile.Text.Trim

            _objPreview.ValidateBeforeConversion()
            If _datamodel.DefinitionFileName <> String.Empty Then
                Dim objUnstructFileCnv As New UnstructuredFileConverter.UnstructuredFileConverter()
                'objUnstructFileCnv.DefinitionFile = _datamodel.DefinitionFileName.Trim()
                objUnstructFileCnv.DefinitionFile = _objPreview.DefinitionFile
                Try
                    convertedSrcFilename = My.Settings.IntermediateFile
                    If objUnstructFileCnv.UnstructuredFileConverter(frmLogin.GsUserName, _objPreview.DefinitionFile, _objPreview.SourceFileName, convertedSrcFilename, "D", ";", "{""}") Then
                        _objPreview.IntermediaryFileName = convertedSrcFilename
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Exit Sub
                End Try
            End If
            frmPreview.ShowPreview(_datamodel, _masterTemplateList, False, Nothing, _objPreview)
        Catch ex As Exception
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", ex.Message)

        End Try

    End Sub
#End Region
#Region " Validation Before Preview "
    Private Function IsFieldValid() As Boolean
        Dim errMessage As String = String.Empty
        If CommonTemplateComboBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Common Template Name") & vbNewLine
        End If
        If SourceFileTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Source File Name") & vbNewLine
        End If

        If RemoveRowsTextBox.Number = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Remove Rows from End") & vbNewLine
        End If
        If SaveLocationTextBox.Text = String.Empty Then
            errMessage &= String.Format(MsgReader.GetString("E00000050"), "Save Location") & vbNewLine
        End If
        If errMessage = String.Empty Then
            Return True
        Else
            BTMU.Magic.Common.BTMUExceptionManager.LogAndShowError("Error on Preview : ", errMessage)
            Return False
        End If
    End Function
#End Region

    Private Function ShowHash() As Boolean
        If _masterTemplateList.OutputFormat = GCMSFormat.OutputFormatString AndAlso My.Settings.GCMSHashing = "True" Then
            Dim objHashing As New frmGCMSHashingView
            objHashing.GetHash(SourceFileTextBox.Text)
            If objHashing.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return False
        End If
        Return True
    End Function

    Private Sub ValueDateCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ValueDateCheckBox.CheckedChanged

    End Sub
End Class