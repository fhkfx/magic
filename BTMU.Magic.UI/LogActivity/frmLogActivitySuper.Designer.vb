<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogActivitySuper
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogActivitySuper))
        Me.NoLogNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.NoLogLabel = New System.Windows.Forms.Label
        Me.DisplayButton = New System.Windows.Forms.Button
        Me.PrintButton = New System.Windows.Forms.Button
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog
        Me.ActivityTextBox = New System.Windows.Forms.RichTextBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.DatePicker = New System.Windows.Forms.DateTimePicker
        Me.DateLabel = New System.Windows.Forms.Label
        CType(Me.NoLogNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NoLogNumericUpDown
        '
        Me.NoLogNumericUpDown.Location = New System.Drawing.Point(314, 10)
        Me.NoLogNumericUpDown.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NoLogNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NoLogNumericUpDown.Name = "NoLogNumericUpDown"
        Me.NoLogNumericUpDown.Size = New System.Drawing.Size(50, 20)
        Me.NoLogNumericUpDown.TabIndex = 1
        Me.NoLogNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'NoLogLabel
        '
        Me.NoLogLabel.Location = New System.Drawing.Point(232, 10)
        Me.NoLogLabel.Name = "NoLogLabel"
        Me.NoLogLabel.Size = New System.Drawing.Size(76, 23)
        Me.NoLogLabel.TabIndex = 3
        Me.NoLogLabel.Text = "No of Log File:"
        Me.NoLogLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DisplayButton
        '
        Me.DisplayButton.Location = New System.Drawing.Point(370, 8)
        Me.DisplayButton.Name = "DisplayButton"
        Me.DisplayButton.Size = New System.Drawing.Size(75, 27)
        Me.DisplayButton.TabIndex = 2
        Me.DisplayButton.Text = "&Display"
        Me.DisplayButton.UseVisualStyleBackColor = True
        '
        'PrintButton
        '
        Me.PrintButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PrintButton.Location = New System.Drawing.Point(716, 8)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(75, 27)
        Me.PrintButton.TabIndex = 3
        Me.PrintButton.Text = "&Print"
        Me.PrintButton.UseVisualStyleBackColor = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'ActivityTextBox
        '
        Me.ActivityTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ActivityTextBox.BackColor = System.Drawing.Color.White
        Me.ActivityTextBox.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivityTextBox.Location = New System.Drawing.Point(15, 46)
        Me.ActivityTextBox.Name = "ActivityTextBox"
        Me.ActivityTextBox.ReadOnly = True
        Me.ActivityTextBox.Size = New System.Drawing.Size(776, 227)
        Me.ActivityTextBox.TabIndex = 7
        Me.ActivityTextBox.Text = ""
        '
        'PrintDocument1
        '
        Me.PrintDocument1.DocumentName = "Log Activity"
        '
        'DatePicker
        '
        Me.DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DatePicker.Location = New System.Drawing.Point(62, 11)
        Me.DatePicker.Name = "DatePicker"
        Me.DatePicker.Size = New System.Drawing.Size(120, 20)
        Me.DatePicker.TabIndex = 0
        Me.DatePicker.Value = New Date(2009, 7, 10, 0, 0, 0, 0)
        '
        'DateLabel
        '
        Me.DateLabel.Location = New System.Drawing.Point(15, 11)
        Me.DateLabel.Name = "DateLabel"
        Me.DateLabel.Size = New System.Drawing.Size(40, 23)
        Me.DateLabel.TabIndex = 10
        Me.DateLabel.Text = "Date:"
        Me.DateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmLogActivitySuper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(803, 286)
        Me.Controls.Add(Me.DatePicker)
        Me.Controls.Add(Me.DateLabel)
        Me.Controls.Add(Me.ActivityTextBox)
        Me.Controls.Add(Me.PrintButton)
        Me.Controls.Add(Me.DisplayButton)
        Me.Controls.Add(Me.NoLogLabel)
        Me.Controls.Add(Me.NoLogNumericUpDown)
        Me.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmLogActivitySuper"
        Me.ShowIcon = False
        Me.Text = "Activity Log (MA0010)"
        CType(Me.NoLogNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DisplayButton As System.Windows.Forms.Button
    Friend WithEvents NoLogLabel As System.Windows.Forms.Label
    Friend WithEvents NoLogNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents PrintButton As System.Windows.Forms.Button
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents ActivityTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents DatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateLabel As System.Windows.Forms.Label
End Class
