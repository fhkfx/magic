Imports BTMU.Magic.Common
Imports System

Public Class frmLogActivityAdmin

    Private Sub DisplayButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DisplayButton.Click
        Dim msg As String = ""
        Dim isArchive As Boolean
        Dim fileName As String
        Dim NoLog As Int16 = NoLogNumericUpDown.Value
        isArchive = IIf(FolderComboBox.SelectedIndex = 0, False, True)
        For i As Int16 = NoLog To 1 Step -1
            fileName = DatePicker.Value.AddDays(i - NoLog).ToString("yyyyMMdd") & ".log"
            msg &= "File Name: " & fileName & vbCrLf
            msg &= ReadActivityLog(fileName, isArchive, frmLogin.GsUserName) & vbCrLf
        Next
        ActivityTextBox.Text = msg
    End Sub

    Private Sub frmLogActivitySuper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FolderComboBox.SelectedIndex = 0
        DatePicker.Value = Now
    End Sub

    Private StringToPrint As String

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        PrintDialog1.Document = PrintDocument1
        StringToPrint = ActivityTextBox.Text

        If PrintDialog1.ShowDialog() = DialogResult.OK Then
            Me.PrintDocument1.Print()
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim numChars As Integer
        Dim numLines As Integer
        Dim stringForPage As String
        Dim strFormat As New StringFormat()
        Dim PrintFont As Font
        PrintFont = ActivityTextBox.Font

        Dim rectDraw As New RectangleF(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height)
        Dim sizeMeasure As New SizeF(e.MarginBounds.Width, e.MarginBounds.Height - PrintFont.GetHeight(e.Graphics))
        strFormat.Trimming = StringTrimming.Word
        e.Graphics.MeasureString(StringToPrint, PrintFont, sizeMeasure, strFormat, numChars, numLines)
        stringForPage = StringToPrint.Substring(0, numChars)
        e.Graphics.DrawString(stringForPage, PrintFont, Brushes.Black, rectDraw, strFormat)
        If numChars < StringToPrint.Length Then
            StringToPrint = StringToPrint.Substring(numChars)
            e.HasMorePages = True
        Else
            e.HasMorePages = False
        End If
    End Sub
End Class