Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulate transaction detail data
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class GCMSMTTransactionDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curSequenc As String
    Private _oldSequenc As String
    Private _oriSequenc As String
    Private _curTemplateID As String
    Private _oldTemplateID As String
    Private _oriTemplateID As String
    Private _curSettlementAccNoAndName As String
    Private _oldSettlementAccNoAndName As String
    Private _oriSettlementAccNoAndName As String
    Private _curSettlementAccNo As String
    Private _oldSettlementAccNo As String
    Private _oriSettlementAccNo As String
    Private _curSettlementAccName1 As String
    Private _oldSettlementAccName1 As String
    Private _oriSettlementAccName1 As String
    Private _curSettlementAccName2 As String
    Private _oldSettlementAccName2 As String
    Private _oriSettlementAccName2 As String
    Private _curSettlementAccName3 As String
    Private _oldSettlementAccName3 As String
    Private _oriSettlementAccName3 As String
    Private _curSettlementAccName4 As String
    Private _oldSettlementAccName4 As String
    Private _oriSettlementAccName4 As String
    Private _curSettlementBank As String
    Private _oldSettlementBank As String
    Private _oriSettlementBank As String
    Private _curSettlementBranch As String
    Private _oldSettlementBranch As String
    Private _oriSettlementBranch As String
    Private _curSettlementBankAddress1 As String
    Private _oldSettlementBankAddress1 As String
    Private _oriSettlementBankAddress1 As String
    Private _curSettlementBankAddress2 As String
    Private _oldSettlementBankAddress2 As String
    Private _oriSettlementBankAddress2 As String
    Private _curSettlementAccCurrency As String
    Private _oldSettlementAccCurrency As String
    Private _oriSettlementAccCurrency As String
    Private _curValueDate As String
    Private _oldValueDate As String
    Private _oriValueDate As String
    Private _curCustomerReference As String
    Private _oldCustomerReference As String
    Private _oriCustomerReference As String
    Private _curSectorSelection As String
    Private _oldSectorSelection As String
    Private _oriSectorSelection As String
    Private _curCurrency As String
    Private _oldCurrency As String
    Private _oriCurrency As String
    Private _curRemitAmount As String
    Private _oldRemitAmount As String
    Private _oriRemitAmount As String
    Private _curExchangeMethod As String
    Private _oldExchangeMethod As String
    Private _oriExchangeMethod As String
    Private _curContractNo As String
    Private _oldContractNo As String
    Private _oriContractNo As String
    Private _curIntermediaryBankAndBranch As String
    Private _oldIntermediaryBankAndBranch As String
    Private _oriIntermediaryBankAndBranch As String
    Private _curIntermediaryBank As String
    Private _oldIntermediaryBank As String
    Private _oriIntermediaryBank As String
    Private _curIntermediaryBranch As String
    Private _oldIntermediaryBranch As String
    Private _oriIntermediaryBranch As String
    Private _curIntermediaryBankAddress1 As String
    Private _oldIntermediaryBankAddress1 As String
    Private _oriIntermediaryBankAddress1 As String
    Private _curIntermediaryBankAddress2 As String
    Private _oldIntermediaryBankAddress2 As String
    Private _oriIntermediaryBankAddress2 As String
    Private _curIntermediaryBankMasterCode As String
    Private _oldIntermediaryBankMasterCode As String
    Private _oriIntermediaryBankMasterCode As String
    Private _curIntermediaryBankOptionD As Boolean
    Private _oldIntermediaryBankOptionD As Boolean
    Private _oriIntermediaryBankOptionD As Boolean
    Private _curIntermediaryBankOptionA As Boolean
    Private _oldIntermediaryBankOptionA As Boolean
    Private _oriIntermediaryBankOptionA As Boolean
    Private _curIntermediaryBankOptionC As Boolean
    Private _oldIntermediaryBankOptionC As Boolean
    Private _oriIntermediaryBankOptionC As Boolean
    Private _curIntermediaryBankSWIFTBIC As String
    Private _oldIntermediaryBankSWIFTBIC As String
    Private _oriIntermediaryBankSWIFTBIC As String
    Private _curIntermediaryBankNCC As String
    Private _oldIntermediaryBankNCC As String
    Private _oriIntermediaryBankNCC As String
    Private _curBeneficiaryBankAndBranch As String
    Private _oldBeneficiaryBankAndBranch As String
    Private _oriBeneficiaryBankAndBranch As String
    Private _curBeneficiaryBank As String
    Private _oldBeneficiaryBank As String
    Private _oriBeneficiaryBank As String
    Private _curBeneficiaryBranch As String
    Private _oldBeneficiaryBranch As String
    Private _oriBeneficiaryBranch As String
    Private _curBeneficiaryBankAddress1 As String
    Private _oldBeneficiaryBankAddress1 As String
    Private _oriBeneficiaryBankAddress1 As String
    Private _curBeneficiaryBankAddress2 As String
    Private _oldBeneficiaryBankAddress2 As String
    Private _oriBeneficiaryBankAddress2 As String
    Private _curBeneficiaryBankMasterCode As String
    Private _oldBeneficiaryBankMasterCode As String
    Private _oriBeneficiaryBankMasterCode As String
    Private _curBeneficiaryBankOptionD As Boolean
    Private _oldBeneficiaryBankOptionD As Boolean
    Private _oriBeneficiaryBankOptionD As Boolean
    Private _curBeneficiaryBankOptionA As Boolean
    Private _oldBeneficiaryBankOptionA As Boolean
    Private _oriBeneficiaryBankOptionA As Boolean
    Private _curBeneficiaryBankOptionC As Boolean
    Private _oldBeneficiaryBankOptionC As Boolean
    Private _oriBeneficiaryBankOptionC As Boolean
    Private _curBeneficiaryBankSWIFTBIC As String
    Private _oldBeneficiaryBankSWIFTBIC As String
    Private _oriBeneficiaryBankSWIFTBIC As String
    Private _curBeneficiaryBankNCC As String
    Private _oldBeneficiaryBankNCC As String
    Private _oriBeneficiaryBankNCC As String
    Private _curBeneficiaryAccNoAndName As String
    Private _oldBeneficiaryAccNoAndName As String
    Private _oriBeneficiaryAccNoAndName As String
    Private _curBeneficiaryAccNo As String
    Private _oldBeneficiaryAccNo As String
    Private _oriBeneficiaryAccNo As String
    Private _curBeneficiaryName1 As String
    Private _oldBeneficiaryName1 As String
    Private _oriBeneficiaryName1 As String
    Private _curBeneficiaryName2 As String
    Private _oldBeneficiaryName2 As String
    Private _oriBeneficiaryName2 As String
    Private _curBeneficiaryName3 As String
    Private _oldBeneficiaryName3 As String
    Private _oriBeneficiaryName3 As String
    Private _curBeneficiaryName4 As String
    Private _oldBeneficiaryName4 As String
    Private _oriBeneficiaryName4 As String
    Private _curBeneficiaryMsg1 As String
    Private _oldBeneficiaryMsg1 As String
    Private _oriBeneficiaryMsg1 As String
    Private _curBeneficiaryMsg2 As String
    Private _oldBeneficiaryMsg2 As String
    Private _oriBeneficiaryMsg2 As String
    Private _curBeneficiaryMsg3 As String
    Private _oldBeneficiaryMsg3 As String
    Private _oriBeneficiaryMsg3 As String
    Private _curBeneficiaryMsg4 As String
    Private _oldBeneficiaryMsg4 As String
    Private _oriBeneficiaryMsg4 As String
    Private _curBeneficiaryRemitPurpose As String
    Private _oldBeneficiaryRemitPurpose As String
    Private _oriBeneficiaryRemitPurpose As String
    Private _curBeneficiaryRemitInfo1 As String
    Private _oldBeneficiaryRemitInfo1 As String
    Private _oriBeneficiaryRemitInfo1 As String
    Private _curBeneficiaryRemitInfo2 As String
    Private _oldBeneficiaryRemitInfo2 As String
    Private _oriBeneficiaryRemitInfo2 As String
    Private _curBeneficiaryRemitInfo3 As String
    Private _oldBeneficiaryRemitInfo3 As String
    Private _oriBeneficiaryRemitInfo3 As String
    Private _curBankCharges As String
    Private _oldBankCharges As String
    Private _oriBankCharges As String
    Private _curChargesAccNoAndName As String
    Private _oldChargesAccNoAndName As String
    Private _oriChargesAccNoAndName As String
    Private _curChargesAcc As String
    Private _oldChargesAcc As String
    Private _oriChargesAcc As String
    Private _curChargesAccName1 As String
    Private _oldChargesAccName1 As String
    Private _oriChargesAccName1 As String
    Private _curChargesAccName2 As String
    Private _oldChargesAccName2 As String
    Private _oriChargesAccName2 As String
    Private _curChargesAccName3 As String
    Private _oldChargesAccName3 As String
    Private _oriChargesAccName3 As String
    Private _curChargesAccName4 As String
    Private _oldChargesAccName4 As String
    Private _oriChargesAccName4 As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    Public Property Sequenc() As String
        Get
            Return _curSequenc
        End Get
        Set(ByVal value As String)
            If _curSequenc <> value Then
                _curSequenc = value
                OnPropertyChanged("Sequenc")
            End If
        End Set
    End Property
    Public Property TemplateID() As String
        Get
            Return _curTemplateID
        End Get
        Set(ByVal value As String)
            If _curTemplateID <> value Then
                _curTemplateID = value
                OnPropertyChanged("TemplateID")
            End If
        End Set
    End Property
    Public Property SettlementAccNoAndName() As String
        Get
            Return _curSettlementAccNoAndName
        End Get
        Set(ByVal value As String)
            If _curSettlementAccNoAndName <> value Then
                _curSettlementAccNoAndName = value
                OnPropertyChanged("SettlementAccNoAndName")
            End If
        End Set
    End Property
    Public Property SettlementAccNo() As String
        Get
            Return _curSettlementAccNo
        End Get
        Set(ByVal value As String)
            If _curSettlementAccNo <> value Then
                _curSettlementAccNo = value
                OnPropertyChanged("SettlementAccNo")
            End If
        End Set
    End Property
    Public Property SettlementAccName1() As String
        Get
            Return _curSettlementAccName1
        End Get
        Set(ByVal value As String)
            If _curSettlementAccName1 <> value Then
                _curSettlementAccName1 = value
                OnPropertyChanged("SettlementAccName1")
            End If
        End Set
    End Property
    Public Property SettlementAccName2() As String
        Get
            Return _curSettlementAccName2
        End Get
        Set(ByVal value As String)
            If _curSettlementAccName2 <> value Then
                _curSettlementAccName2 = value
                OnPropertyChanged("SettlementAccName2")
            End If
        End Set
    End Property
    Public Property SettlementAccName3() As String
        Get
            Return _curSettlementAccName3
        End Get
        Set(ByVal value As String)
            If _curSettlementAccName3 <> value Then
                _curSettlementAccName3 = value
                OnPropertyChanged("SettlementAccName3")
            End If
        End Set
    End Property
    Public Property SettlementAccName4() As String
        Get
            Return _curSettlementAccName4
        End Get
        Set(ByVal value As String)
            If _curSettlementAccName4 <> value Then
                _curSettlementAccName4 = value
                OnPropertyChanged("SettlementAccName4")
            End If
        End Set
    End Property
    Public Property SettlementBank() As String
        Get
            Return _curSettlementBank
        End Get
        Set(ByVal value As String)
            If _curSettlementBank <> value Then
                _curSettlementBank = value
                OnPropertyChanged("SettlementBank")
            End If
        End Set
    End Property
    Public Property SettlementBranch() As String
        Get
            Return _curSettlementBranch
        End Get
        Set(ByVal value As String)
            If _curSettlementBranch <> value Then
                _curSettlementBranch = value
                OnPropertyChanged("SettlementBranch")
            End If
        End Set
    End Property
    Public Property SettlementBankAddress1() As String
        Get
            Return _curSettlementBankAddress1
        End Get
        Set(ByVal value As String)
            If _curSettlementBankAddress1 <> value Then
                _curSettlementBankAddress1 = value
                OnPropertyChanged("SettlementBankAddress1")
            End If
        End Set
    End Property
    Public Property SettlementBankAddress2() As String
        Get
            Return _curSettlementBankAddress2
        End Get
        Set(ByVal value As String)
            If _curSettlementBankAddress2 <> value Then
                _curSettlementBankAddress2 = value
                OnPropertyChanged("SettlementBankAddress2")
            End If
        End Set
    End Property
    Public Property SettlementAccCurrency() As String
        Get
            Return _curSettlementAccCurrency
        End Get
        Set(ByVal value As String)
            If _curSettlementAccCurrency <> value Then
                _curSettlementAccCurrency = value
                OnPropertyChanged("SettlementAccCurrency")
            End If
        End Set
    End Property
    Public Property ValueDate() As String
        Get
            Return _curValueDate
        End Get
        Set(ByVal value As String)
            If _curValueDate <> value Then
                _curValueDate = value
                OnPropertyChanged("ValueDate")
            End If
        End Set
    End Property
    Public Property CustomerReference() As String
        Get
            Return _curCustomerReference
        End Get
        Set(ByVal value As String)
            If _curCustomerReference <> value Then
                _curCustomerReference = value
                OnPropertyChanged("CustomerReference")
            End If
        End Set
    End Property
    Public Property SectorSelection() As String
        Get
            Return _curSectorSelection
        End Get
        Set(ByVal value As String)
            If _curSectorSelection <> value Then
                _curSectorSelection = value
                OnPropertyChanged("SectorSelection")
            End If
        End Set
    End Property
    Public Property Currency() As String
        Get
            Return _curCurrency
        End Get
        Set(ByVal value As String)
            If _curCurrency <> value Then
                _curCurrency = value
                OnPropertyChanged("Currency")
            End If
        End Set
    End Property
    Public Property RemitAmount() As String
        Get
            Return _curRemitAmount
        End Get
        Set(ByVal value As String)
            If _curRemitAmount <> value Then
                _curRemitAmount = value
                OnPropertyChanged("RemitAmount")
            End If
        End Set
    End Property
    Public Property ExchangeMethod() As String
        Get
            Return _curExchangeMethod
        End Get
        Set(ByVal value As String)
            If _curExchangeMethod <> value Then
                _curExchangeMethod = value
                OnPropertyChanged("ExchangeMethod")
            End If
        End Set
    End Property
    Public Property ContractNo() As String
        Get
            Return _curContractNo
        End Get
        Set(ByVal value As String)
            If _curContractNo <> value Then
                _curContractNo = value
                OnPropertyChanged("ContractNo")
            End If
        End Set
    End Property
    Public Property IntermediaryBankAndBranch() As String
        Get
            Return _curIntermediaryBankAndBranch
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankAndBranch <> value Then
                _curIntermediaryBankAndBranch = value
                OnPropertyChanged("IntermediaryBankAndBranch")
            End If
        End Set
    End Property
    Public Property IntermediaryBank() As String
        Get
            Return _curIntermediaryBank
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBank <> value Then
                _curIntermediaryBank = value
                OnPropertyChanged("IntermediaryBank")
            End If
        End Set
    End Property
    Public Property IntermediaryBranch() As String
        Get
            Return _curIntermediaryBranch
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBranch <> value Then
                _curIntermediaryBranch = value
                OnPropertyChanged("IntermediaryBranch")
            End If
        End Set
    End Property
    Public Property IntermediaryBankAddress1() As String
        Get
            Return _curIntermediaryBankAddress1
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankAddress1 <> value Then
                _curIntermediaryBankAddress1 = value
                OnPropertyChanged("IntermediaryBankAddress1")
            End If
        End Set
    End Property
    Public Property IntermediaryBankAddress2() As String
        Get
            Return _curIntermediaryBankAddress2
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankAddress2 <> value Then
                _curIntermediaryBankAddress2 = value
                OnPropertyChanged("IntermediaryBankAddress2")
            End If
        End Set
    End Property
    Public Property IntermediaryBankMasterCode() As String
        Get
            Return _curIntermediaryBankMasterCode
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankMasterCode <> value Then
                _curIntermediaryBankMasterCode = value
                OnPropertyChanged("IntermediaryBankMasterCode")
            End If
        End Set
    End Property
    Public Property IntermediaryBankOptionD() As Boolean
        Get
            Return _curIntermediaryBankOptionD
        End Get
        Set(ByVal value As Boolean)
            If _curIntermediaryBankOptionD <> value Then
                _curIntermediaryBankOptionD = value
                OnPropertyChanged("IntermediaryBankOptionD")
            End If
        End Set
    End Property
    Public Property IntermediaryBankOptionA() As Boolean
        Get
            Return _curIntermediaryBankOptionA
        End Get
        Set(ByVal value As Boolean)
            If _curIntermediaryBankOptionA <> value Then
                _curIntermediaryBankOptionA = value
                OnPropertyChanged("IntermediaryBankOptionA")
            End If
        End Set
    End Property
    Public Property IntermediaryBankOptionC() As Boolean
        Get
            Return _curIntermediaryBankOptionC
        End Get
        Set(ByVal value As Boolean)
            If _curIntermediaryBankOptionC <> value Then
                _curIntermediaryBankOptionC = value
                OnPropertyChanged("IntermediaryBankOptionC")
            End If
        End Set
    End Property
    Public Property IntermediaryBankSWIFTBIC() As String
        Get
            Return _curIntermediaryBankSWIFTBIC
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankSWIFTBIC <> value Then
                _curIntermediaryBankSWIFTBIC = value
                OnPropertyChanged("IntermediaryBankSWIFTBIC")
            End If
        End Set
    End Property
    Public Property IntermediaryBankNCC() As String
        Get
            Return _curIntermediaryBankNCC
        End Get
        Set(ByVal value As String)
            If _curIntermediaryBankNCC <> value Then
                _curIntermediaryBankNCC = value
                OnPropertyChanged("IntermediaryBankNCC")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankAndBranch() As String
        Get
            Return _curBeneficiaryBankAndBranch
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankAndBranch <> value Then
                _curBeneficiaryBankAndBranch = value
                OnPropertyChanged("BeneficiaryBankAndBranch")
            End If
        End Set
    End Property
    Public Property BeneficiaryBank() As String
        Get
            Return _curBeneficiaryBank
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBank <> value Then
                _curBeneficiaryBank = value
                OnPropertyChanged("BeneficiaryBank")
            End If
        End Set
    End Property
    Public Property BeneficiaryBranch() As String
        Get
            Return _curBeneficiaryBranch
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBranch <> value Then
                _curBeneficiaryBranch = value
                OnPropertyChanged("BeneficiaryBranch")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankAddress1() As String
        Get
            Return _curBeneficiaryBankAddress1
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankAddress1 <> value Then
                _curBeneficiaryBankAddress1 = value
                OnPropertyChanged("BeneficiaryBankAddress1")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankAddress2() As String
        Get
            Return _curBeneficiaryBankAddress2
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankAddress2 <> value Then
                _curBeneficiaryBankAddress2 = value
                OnPropertyChanged("BeneficiaryBankAddress2")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankMasterCode() As String
        Get
            Return _curBeneficiaryBankMasterCode
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankMasterCode <> value Then
                _curBeneficiaryBankMasterCode = value
                OnPropertyChanged("BeneficiaryBankMasterCode")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankOptionD() As Boolean
        Get
            Return _curBeneficiaryBankOptionD
        End Get
        Set(ByVal value As Boolean)
            If _curBeneficiaryBankOptionD <> value Then
                _curBeneficiaryBankOptionD = value
                OnPropertyChanged("BeneficiaryBankOptionD")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankOptionA() As Boolean
        Get
            Return _curBeneficiaryBankOptionA
        End Get
        Set(ByVal value As Boolean)
            If _curBeneficiaryBankOptionA <> value Then
                _curBeneficiaryBankOptionA = value
                OnPropertyChanged("BeneficiaryBankOptionA")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankOptionC() As Boolean
        Get
            Return _curBeneficiaryBankOptionC
        End Get
        Set(ByVal value As Boolean)
            If _curBeneficiaryBankOptionC <> value Then
                _curBeneficiaryBankOptionC = value
                OnPropertyChanged("BeneficiaryBankOptionC")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankSWIFTBIC() As String
        Get
            Return _curBeneficiaryBankSWIFTBIC
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankSWIFTBIC <> value Then
                _curBeneficiaryBankSWIFTBIC = value
                OnPropertyChanged("BeneficiaryBankSWIFTBIC")
            End If
        End Set
    End Property
    Public Property BeneficiaryBankNCC() As String
        Get
            Return _curBeneficiaryBankNCC
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryBankNCC <> value Then
                _curBeneficiaryBankNCC = value
                OnPropertyChanged("BeneficiaryBankNCC")
            End If
        End Set
    End Property
    Public Property BeneficiaryAccNoAndName() As String
        Get
            Return _curBeneficiaryAccNoAndName
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryAccNoAndName <> value Then
                _curBeneficiaryAccNoAndName = value
                OnPropertyChanged("BeneficiaryAccNoAndName")
            End If
        End Set
    End Property
    Public Property BeneficiaryAccNo() As String
        Get
            Return _curBeneficiaryAccNo
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryAccNo <> value Then
                _curBeneficiaryAccNo = value
                OnPropertyChanged("BeneficiaryAccNo")
            End If
        End Set
    End Property
    Public Property BeneficiaryName1() As String
        Get
            Return _curBeneficiaryName1
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryName1 <> value Then
                _curBeneficiaryName1 = value
                OnPropertyChanged("BeneficiaryName1")
            End If
        End Set
    End Property
    Public Property BeneficiaryName2() As String
        Get
            Return _curBeneficiaryName2
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryName2 <> value Then
                _curBeneficiaryName2 = value
                OnPropertyChanged("BeneficiaryName2")
            End If
        End Set
    End Property
    Public Property BeneficiaryName3() As String
        Get
            Return _curBeneficiaryName3
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryName3 <> value Then
                _curBeneficiaryName3 = value
                OnPropertyChanged("BeneficiaryName3")
            End If
        End Set
    End Property
    Public Property BeneficiaryName4() As String
        Get
            Return _curBeneficiaryName4
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryName4 <> value Then
                _curBeneficiaryName4 = value
                OnPropertyChanged("BeneficiaryName4")
            End If
        End Set
    End Property
    Public Property BeneficiaryMsg1() As String
        Get
            Return _curBeneficiaryMsg1
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryMsg1 <> value Then
                _curBeneficiaryMsg1 = value
                OnPropertyChanged("BeneficiaryMsg1")
            End If
        End Set
    End Property
    Public Property BeneficiaryMsg2() As String
        Get
            Return _curBeneficiaryMsg2
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryMsg2 <> value Then
                _curBeneficiaryMsg2 = value
                OnPropertyChanged("BeneficiaryMsg2")
            End If
        End Set
    End Property
    Public Property BeneficiaryMsg3() As String
        Get
            Return _curBeneficiaryMsg3
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryMsg3 <> value Then
                _curBeneficiaryMsg3 = value
                OnPropertyChanged("BeneficiaryMsg3")
            End If
        End Set
    End Property
    Public Property BeneficiaryMsg4() As String
        Get
            Return _curBeneficiaryMsg4
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryMsg4 <> value Then
                _curBeneficiaryMsg4 = value
                OnPropertyChanged("BeneficiaryMsg4")
            End If
        End Set
    End Property
    Public Property BeneficiaryRemitPurpose() As String
        Get
            Return _curBeneficiaryRemitPurpose
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryRemitPurpose <> value Then
                _curBeneficiaryRemitPurpose = value
                OnPropertyChanged("BeneficiaryRemitPurpose")
            End If
        End Set
    End Property
    Public Property BeneficiaryRemitInfo1() As String
        Get
            Return _curBeneficiaryRemitInfo1
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryRemitInfo1 <> value Then
                _curBeneficiaryRemitInfo1 = value
                OnPropertyChanged("BeneficiaryRemitInfo1")
            End If
        End Set
    End Property
    Public Property BeneficiaryRemitInfo2() As String
        Get
            Return _curBeneficiaryRemitInfo2
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryRemitInfo2 <> value Then
                _curBeneficiaryRemitInfo2 = value
                OnPropertyChanged("BeneficiaryRemitInfo2")
            End If
        End Set
    End Property
    Public Property BeneficiaryRemitInfo3() As String
        Get
            Return _curBeneficiaryRemitInfo3
        End Get
        Set(ByVal value As String)
            If _curBeneficiaryRemitInfo3 <> value Then
                _curBeneficiaryRemitInfo3 = value
                OnPropertyChanged("BeneficiaryRemitInfo3")
            End If
        End Set
    End Property
    Public Property BankCharges() As String
        Get
            Return _curBankCharges
        End Get
        Set(ByVal value As String)
            If _curBankCharges <> value Then
                _curBankCharges = value
                OnPropertyChanged("BankCharges")
            End If
        End Set
    End Property
    Public Property ChargesAccNoAndName() As String
        Get
            Return _curChargesAccNoAndName
        End Get
        Set(ByVal value As String)
            If _curChargesAccNoAndName <> value Then
                _curChargesAccNoAndName = value
                OnPropertyChanged("ChargesAccNoAndName")
            End If
        End Set
    End Property
    Public Property ChargesAcc() As String
        Get
            Return _curChargesAcc
        End Get
        Set(ByVal value As String)
            If _curChargesAcc <> value Then
                _curChargesAcc = value
                OnPropertyChanged("ChargesAcc")
            End If
        End Set
    End Property
    Public Property ChargesAccName1() As String
        Get
            Return _curChargesAccName1
        End Get
        Set(ByVal value As String)
            If _curChargesAccName1 <> value Then
                _curChargesAccName1 = value
                OnPropertyChanged("ChargesAccName1")
            End If
        End Set
    End Property
    Public Property ChargesAccName2() As String
        Get
            Return _curChargesAccName2
        End Get
        Set(ByVal value As String)
            If _curChargesAccName2 <> value Then
                _curChargesAccName2 = value
                OnPropertyChanged("ChargesAccName2")
            End If
        End Set
    End Property
    Public Property ChargesAccName3() As String
        Get
            Return _curChargesAccName3
        End Get
        Set(ByVal value As String)
            If _curChargesAccName3 <> value Then
                _curChargesAccName3 = value
                OnPropertyChanged("ChargesAccName3")
            End If
        End Set
    End Property
    Public Property ChargesAccName4() As String
        Get
            Return _curChargesAccName4
        End Get
        Set(ByVal value As String)
            If _curChargesAccName4 <> value Then
                _curChargesAccName4 = value
                OnPropertyChanged("ChargesAccName4")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    Public Overrides Sub ApplyEdit()
        _oriSequenc = _curSequenc
        _oriTemplateID = _curTemplateID
        _oriSettlementAccNoAndName = _curSettlementAccNoAndName
        _oriSettlementAccNo = _curSettlementAccNo
        _oriSettlementAccName1 = _curSettlementAccName1
        _oriSettlementAccName2 = _curSettlementAccName2
        _oriSettlementAccName3 = _curSettlementAccName3
        _oriSettlementAccName4 = _curSettlementAccName4
        _oriSettlementBank = _curSettlementBank
        _oriSettlementBranch = _curSettlementBranch
        _oriSettlementBankAddress1 = _curSettlementBankAddress1
        _oriSettlementBankAddress2 = _curSettlementBankAddress2
        _oriSettlementAccCurrency = _curSettlementAccCurrency
        _oriValueDate = _curValueDate
        _oriCustomerReference = _curCustomerReference
        _oriSectorSelection = _curSectorSelection
        _oriCurrency = _curCurrency
        _oriRemitAmount = _curRemitAmount
        _oriExchangeMethod = _curExchangeMethod
        _oriContractNo = _curContractNo
        _oriIntermediaryBankAndBranch = _curIntermediaryBankAndBranch
        _oriIntermediaryBank = _curIntermediaryBank
        _oriIntermediaryBranch = _curIntermediaryBranch
        _oriIntermediaryBankAddress1 = _curIntermediaryBankAddress1
        _oriIntermediaryBankAddress2 = _curIntermediaryBankAddress2
        _oriIntermediaryBankMasterCode = _curIntermediaryBankMasterCode
        _oriIntermediaryBankOptionD = _curIntermediaryBankOptionD
        _oriIntermediaryBankOptionA = _curIntermediaryBankOptionA
        _oriIntermediaryBankOptionC = _curIntermediaryBankOptionC
        _oriIntermediaryBankSWIFTBIC = _curIntermediaryBankSWIFTBIC
        _oriIntermediaryBankNCC = _curIntermediaryBankNCC
        _oriBeneficiaryBankAndBranch = _curBeneficiaryBankAndBranch
        _oriBeneficiaryBank = _curBeneficiaryBank
        _oriBeneficiaryBranch = _curBeneficiaryBranch
        _oriBeneficiaryBankAddress1 = _curBeneficiaryBankAddress1
        _oriBeneficiaryBankAddress2 = _curBeneficiaryBankAddress2
        _oriBeneficiaryBankMasterCode = _curBeneficiaryBankMasterCode
        _oriBeneficiaryBankOptionD = _curBeneficiaryBankOptionD
        _oriBeneficiaryBankOptionA = _curBeneficiaryBankOptionA
        _oriBeneficiaryBankOptionC = _curBeneficiaryBankOptionC
        _oriBeneficiaryBankSWIFTBIC = _curBeneficiaryBankSWIFTBIC
        _oriBeneficiaryBankNCC = _curBeneficiaryBankNCC
        _oriBeneficiaryAccNoAndName = _curBeneficiaryAccNoAndName
        _oriBeneficiaryAccNo = _curBeneficiaryAccNo
        _oriBeneficiaryName1 = _curBeneficiaryName1
        _oriBeneficiaryName2 = _curBeneficiaryName2
        _oriBeneficiaryName3 = _curBeneficiaryName3
        _oriBeneficiaryName4 = _curBeneficiaryName4
        _oriBeneficiaryMsg1 = _curBeneficiaryMsg1
        _oriBeneficiaryMsg2 = _curBeneficiaryMsg2
        _oriBeneficiaryMsg3 = _curBeneficiaryMsg3
        _oriBeneficiaryMsg4 = _curBeneficiaryMsg4
        _oriBeneficiaryRemitPurpose = _curBeneficiaryRemitPurpose
        _oriBeneficiaryRemitInfo1 = _curBeneficiaryRemitInfo1
        _oriBeneficiaryRemitInfo2 = _curBeneficiaryRemitInfo2
        _oriBeneficiaryRemitInfo3 = _curBeneficiaryRemitInfo3
        _oriBankCharges = _curBankCharges
        _oriChargesAccNoAndName = _curChargesAccNoAndName
        _oriChargesAcc = _curChargesAcc
        _oriChargesAccName1 = _curChargesAccName1
        _oriChargesAccName2 = _curChargesAccName2
        _oriChargesAccName3 = _curChargesAccName3
        _oriChargesAccName4 = _curChargesAccName4
    End Sub

    Public Overrides Sub BeginEdit()
        _oldSequenc = _curSequenc
        _oldTemplateID = _curTemplateID
        _oldSettlementAccNoAndName = _curSettlementAccNoAndName
        _oldSettlementAccNo = _curSettlementAccNo
        _oldSettlementAccName1 = _curSettlementAccName1
        _oldSettlementAccName2 = _curSettlementAccName2
        _oldSettlementAccName3 = _curSettlementAccName3
        _oldSettlementAccName4 = _curSettlementAccName4
        _oldSettlementBank = _curSettlementBank
        _oldSettlementBranch = _curSettlementBranch
        _oldSettlementBankAddress1 = _curSettlementBankAddress1
        _oldSettlementBankAddress2 = _curSettlementBankAddress2
        _oldSettlementAccCurrency = _curSettlementAccCurrency
        _oldValueDate = _curValueDate
        _oldCustomerReference = _curCustomerReference
        _oldSectorSelection = _curSectorSelection
        _oldCurrency = _curCurrency
        _oldRemitAmount = _curRemitAmount
        _oldExchangeMethod = _curExchangeMethod
        _oldContractNo = _curContractNo
        _oldIntermediaryBankAndBranch = _curIntermediaryBankAndBranch
        _oldIntermediaryBank = _curIntermediaryBank
        _oldIntermediaryBranch = _curIntermediaryBranch
        _oldIntermediaryBankAddress1 = _curIntermediaryBankAddress1
        _oldIntermediaryBankAddress2 = _curIntermediaryBankAddress2
        _oldIntermediaryBankMasterCode = _curIntermediaryBankMasterCode
        _oldIntermediaryBankOptionD = _curIntermediaryBankOptionD
        _oldIntermediaryBankOptionA = _curIntermediaryBankOptionA
        _oldIntermediaryBankOptionC = _curIntermediaryBankOptionC
        _oldIntermediaryBankSWIFTBIC = _curIntermediaryBankSWIFTBIC
        _oldIntermediaryBankNCC = _curIntermediaryBankNCC
        _oldBeneficiaryBankAndBranch = _curBeneficiaryBankAndBranch
        _oldBeneficiaryBank = _curBeneficiaryBank
        _oldBeneficiaryBranch = _curBeneficiaryBranch
        _oldBeneficiaryBankAddress1 = _curBeneficiaryBankAddress1
        _oldBeneficiaryBankAddress2 = _curBeneficiaryBankAddress2
        _oldBeneficiaryBankMasterCode = _curBeneficiaryBankMasterCode
        _oldBeneficiaryBankOptionD = _curBeneficiaryBankOptionD
        _oldBeneficiaryBankOptionA = _curBeneficiaryBankOptionA
        _oldBeneficiaryBankOptionC = _curBeneficiaryBankOptionC
        _oldBeneficiaryBankSWIFTBIC = _curBeneficiaryBankSWIFTBIC
        _oldBeneficiaryBankNCC = _curBeneficiaryBankNCC
        _oldBeneficiaryAccNoAndName = _curBeneficiaryAccNoAndName
        _oldBeneficiaryAccNo = _curBeneficiaryAccNo
        _oldBeneficiaryName1 = _curBeneficiaryName1
        _oldBeneficiaryName2 = _curBeneficiaryName2
        _oldBeneficiaryName3 = _curBeneficiaryName3
        _oldBeneficiaryName4 = _curBeneficiaryName4
        _oldBeneficiaryMsg1 = _curBeneficiaryMsg1
        _oldBeneficiaryMsg2 = _curBeneficiaryMsg2
        _oldBeneficiaryMsg3 = _curBeneficiaryMsg3
        _oldBeneficiaryMsg4 = _curBeneficiaryMsg4
        _oldBeneficiaryRemitPurpose = _curBeneficiaryRemitPurpose
        _oldBeneficiaryRemitInfo1 = _curBeneficiaryRemitInfo1
        _oldBeneficiaryRemitInfo2 = _curBeneficiaryRemitInfo2
        _oldBeneficiaryRemitInfo3 = _curBeneficiaryRemitInfo3
        _oldBankCharges = _curBankCharges
        _oldChargesAccNoAndName = _curChargesAccNoAndName
        _oldChargesAcc = _curChargesAcc
        _oldChargesAccName1 = _curChargesAccName1
        _oldChargesAccName2 = _curChargesAccName2
        _oldChargesAccName3 = _curChargesAccName3
        _oldChargesAccName4 = _curChargesAccName4
    End Sub

    Public Overrides Sub CancelEdit()
        _curSequenc = _oldSequenc
        _curTemplateID = _oldTemplateID
        _curSettlementAccNoAndName = _oldSettlementAccNoAndName
        _curSettlementAccNo = _oldSettlementAccNo
        _curSettlementAccName1 = _oldSettlementAccName1
        _curSettlementAccName2 = _oldSettlementAccName2
        _curSettlementAccName3 = _oldSettlementAccName3
        _curSettlementAccName4 = _oldSettlementAccName4
        _curSettlementBank = _oldSettlementBank
        _curSettlementBranch = _oldSettlementBranch
        _curSettlementBankAddress1 = _oldSettlementBankAddress1
        _curSettlementBankAddress2 = _oldSettlementBankAddress2
        _curSettlementAccCurrency = _oldSettlementAccCurrency
        _curValueDate = _oldValueDate
        _curCustomerReference = _oldCustomerReference
        _curSectorSelection = _oldSectorSelection
        _curCurrency = _oldCurrency
        _curRemitAmount = _oldRemitAmount
        _curExchangeMethod = _oldExchangeMethod
        _curContractNo = _oldContractNo
        _curIntermediaryBankAndBranch = _oldIntermediaryBankAndBranch
        _curIntermediaryBank = _oldIntermediaryBank
        _curIntermediaryBranch = _oldIntermediaryBranch
        _curIntermediaryBankAddress1 = _oldIntermediaryBankAddress1
        _curIntermediaryBankAddress2 = _oldIntermediaryBankAddress2
        _curIntermediaryBankMasterCode = _oldIntermediaryBankMasterCode
        _curIntermediaryBankOptionD = _oldIntermediaryBankOptionD
        _curIntermediaryBankOptionA = _oldIntermediaryBankOptionA
        _curIntermediaryBankOptionC = _oldIntermediaryBankOptionC
        _curIntermediaryBankSWIFTBIC = _oldIntermediaryBankSWIFTBIC
        _curIntermediaryBankNCC = _oldIntermediaryBankNCC
        _curBeneficiaryBankAndBranch = _oldBeneficiaryBankAndBranch
        _curBeneficiaryBank = _oldBeneficiaryBank
        _curBeneficiaryBranch = _oldBeneficiaryBranch
        _curBeneficiaryBankAddress1 = _oldBeneficiaryBankAddress1
        _curBeneficiaryBankAddress2 = _oldBeneficiaryBankAddress2
        _curBeneficiaryBankMasterCode = _oldBeneficiaryBankMasterCode
        _curBeneficiaryBankOptionD = _oldBeneficiaryBankOptionD
        _curBeneficiaryBankOptionA = _oldBeneficiaryBankOptionA
        _curBeneficiaryBankOptionC = _oldBeneficiaryBankOptionC
        _curBeneficiaryBankSWIFTBIC = _oldBeneficiaryBankSWIFTBIC
        _curBeneficiaryBankNCC = _oldBeneficiaryBankNCC
        _curBeneficiaryAccNoAndName = _oldBeneficiaryAccNoAndName
        _curBeneficiaryAccNo = _oldBeneficiaryAccNo
        _curBeneficiaryName1 = _oldBeneficiaryName1
        _curBeneficiaryName2 = _oldBeneficiaryName2
        _curBeneficiaryName3 = _oldBeneficiaryName3
        _curBeneficiaryName4 = _oldBeneficiaryName4
        _curBeneficiaryMsg1 = _oldBeneficiaryMsg1
        _curBeneficiaryMsg2 = _oldBeneficiaryMsg2
        _curBeneficiaryMsg3 = _oldBeneficiaryMsg3
        _curBeneficiaryMsg4 = _oldBeneficiaryMsg4
        _curBeneficiaryRemitPurpose = _oldBeneficiaryRemitPurpose
        _curBeneficiaryRemitInfo1 = _oldBeneficiaryRemitInfo1
        _curBeneficiaryRemitInfo2 = _oldBeneficiaryRemitInfo2
        _curBeneficiaryRemitInfo3 = _oldBeneficiaryRemitInfo3
        _curBankCharges = _oldBankCharges
        _curChargesAccNoAndName = _oldChargesAccNoAndName
        _curChargesAcc = _oldChargesAcc
        _curChargesAccName1 = _oldChargesAccName1
        _curChargesAccName2 = _oldChargesAccName2
        _curChargesAccName3 = _oldChargesAccName3
        _curChargesAccName4 = _oldChargesAccName4
    End Sub

#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curSequenc = ReadXMLElement(reader, "_curSequenc")
            _oldSequenc = ReadXMLElement(reader, "_oldSequenc")
            _oriSequenc = ReadXMLElement(reader, "_oriSequenc")
            _curTemplateID = ReadXMLElement(reader, "_curTemplateID")
            _oldTemplateID = ReadXMLElement(reader, "_oldTemplateID")
            _oriTemplateID = ReadXMLElement(reader, "_oriTemplateID")
            _curSettlementAccNoAndName = ReadXMLElement(reader, "_curSettlementAccNoAndName")
            _oldSettlementAccNoAndName = ReadXMLElement(reader, "_oldSettlementAccNoAndName")
            _oriSettlementAccNoAndName = ReadXMLElement(reader, "_oriSettlementAccNoAndName")
            _curSettlementAccNo = ReadXMLElement(reader, "_curSettlementAccNo")
            _oldSettlementAccNo = ReadXMLElement(reader, "_oldSettlementAccNo")
            _oriSettlementAccNo = ReadXMLElement(reader, "_oriSettlementAccNo")
            _curSettlementAccName1 = ReadXMLElement(reader, "_curSettlementAccName1")
            _oldSettlementAccName1 = ReadXMLElement(reader, "_oldSettlementAccName1")
            _oriSettlementAccName1 = ReadXMLElement(reader, "_oriSettlementAccName1")
            _curSettlementAccName2 = ReadXMLElement(reader, "_curSettlementAccName2")
            _oldSettlementAccName2 = ReadXMLElement(reader, "_oldSettlementAccName2")
            _oriSettlementAccName2 = ReadXMLElement(reader, "_oriSettlementAccName2")
            _curSettlementAccName3 = ReadXMLElement(reader, "_curSettlementAccName3")
            _oldSettlementAccName3 = ReadXMLElement(reader, "_oldSettlementAccName3")
            _oriSettlementAccName3 = ReadXMLElement(reader, "_oriSettlementAccName3")
            _curSettlementAccName4 = ReadXMLElement(reader, "_curSettlementAccName4")
            _oldSettlementAccName4 = ReadXMLElement(reader, "_oldSettlementAccName4")
            _oriSettlementAccName4 = ReadXMLElement(reader, "_oriSettlementAccName4")
            _curSettlementBank = ReadXMLElement(reader, "_curSettlementBank")
            _oldSettlementBank = ReadXMLElement(reader, "_oldSettlementBank")
            _oriSettlementBank = ReadXMLElement(reader, "_oriSettlementBank")
            _curSettlementBranch = ReadXMLElement(reader, "_curSettlementBranch")
            _oldSettlementBranch = ReadXMLElement(reader, "_oldSettlementBranch")
            _oriSettlementBranch = ReadXMLElement(reader, "_oriSettlementBranch")
            _curSettlementBankAddress1 = ReadXMLElement(reader, "_curSettlementBankAddress1")
            _oldSettlementBankAddress1 = ReadXMLElement(reader, "_oldSettlementBankAddress1")
            _oriSettlementBankAddress1 = ReadXMLElement(reader, "_oriSettlementBankAddress1")
            _curSettlementBankAddress2 = ReadXMLElement(reader, "_curSettlementBankAddress2")
            _oldSettlementBankAddress2 = ReadXMLElement(reader, "_oldSettlementBankAddress2")
            _oriSettlementBankAddress2 = ReadXMLElement(reader, "_oriSettlementBankAddress2")
            _curSettlementAccCurrency = ReadXMLElement(reader, "_curSettlementAccCurrency")
            _oldSettlementAccCurrency = ReadXMLElement(reader, "_oldSettlementAccCurrency")
            _oriSettlementAccCurrency = ReadXMLElement(reader, "_oriSettlementAccCurrency")
            _curValueDate = ReadXMLElement(reader, "_curValueDate")
            _oldValueDate = ReadXMLElement(reader, "_oldValueDate")
            _oriValueDate = ReadXMLElement(reader, "_oriValueDate")
            _curCustomerReference = ReadXMLElement(reader, "_curCustomerReference")
            _oldCustomerReference = ReadXMLElement(reader, "_oldCustomerReference")
            _oriCustomerReference = ReadXMLElement(reader, "_oriCustomerReference")
            _curSectorSelection = ReadXMLElement(reader, "_curSectorSelection")
            _oldSectorSelection = ReadXMLElement(reader, "_oldSectorSelection")
            _oriSectorSelection = ReadXMLElement(reader, "_oriSectorSelection")
            _curCurrency = ReadXMLElement(reader, "_curCurrency")
            _oldCurrency = ReadXMLElement(reader, "_oldCurrency")
            _oriCurrency = ReadXMLElement(reader, "_oriCurrency")
            _curRemitAmount = ReadXMLElement(reader, "_curRemitAmount")
            _oldRemitAmount = ReadXMLElement(reader, "_oldRemitAmount")
            _oriRemitAmount = ReadXMLElement(reader, "_oriRemitAmount")
            _curExchangeMethod = ReadXMLElement(reader, "_curExchangeMethod")
            _oldExchangeMethod = ReadXMLElement(reader, "_oldExchangeMethod")
            _oriExchangeMethod = ReadXMLElement(reader, "_oriExchangeMethod")
            _curContractNo = ReadXMLElement(reader, "_curContractNo")
            _oldContractNo = ReadXMLElement(reader, "_oldContractNo")
            _oriContractNo = ReadXMLElement(reader, "_oriContractNo")
            _curIntermediaryBankAndBranch = ReadXMLElement(reader, "_curIntermediaryBankAndBranch")
            _oldIntermediaryBankAndBranch = ReadXMLElement(reader, "_oldIntermediaryBankAndBranch")
            _oriIntermediaryBankAndBranch = ReadXMLElement(reader, "_oriIntermediaryBankAndBranch")
            _curIntermediaryBank = ReadXMLElement(reader, "_curIntermediaryBank")
            _oldIntermediaryBank = ReadXMLElement(reader, "_oldIntermediaryBank")
            _oriIntermediaryBank = ReadXMLElement(reader, "_oriIntermediaryBank")
            _curIntermediaryBranch = ReadXMLElement(reader, "_curIntermediaryBranch")
            _oldIntermediaryBranch = ReadXMLElement(reader, "_oldIntermediaryBranch")
            _oriIntermediaryBranch = ReadXMLElement(reader, "_oriIntermediaryBranch")
            _curIntermediaryBankAddress1 = ReadXMLElement(reader, "_curIntermediaryBankAddress1")
            _oldIntermediaryBankAddress1 = ReadXMLElement(reader, "_oldIntermediaryBankAddress1")
            _oriIntermediaryBankAddress1 = ReadXMLElement(reader, "_oriIntermediaryBankAddress1")
            _curIntermediaryBankAddress2 = ReadXMLElement(reader, "_curIntermediaryBankAddress2")
            _oldIntermediaryBankAddress2 = ReadXMLElement(reader, "_oldIntermediaryBankAddress2")
            _oriIntermediaryBankAddress2 = ReadXMLElement(reader, "_oriIntermediaryBankAddress2")
            _curIntermediaryBankMasterCode = ReadXMLElement(reader, "_curIntermediaryBankMasterCode")
            _oldIntermediaryBankMasterCode = ReadXMLElement(reader, "_oldIntermediaryBankMasterCode")
            _oriIntermediaryBankMasterCode = ReadXMLElement(reader, "_oriIntermediaryBankMasterCode")
            _curIntermediaryBankOptionD = ReadXMLElement(reader, "_curIntermediaryBankOptionD")
            _oldIntermediaryBankOptionD = ReadXMLElement(reader, "_oldIntermediaryBankOptionD")
            _oriIntermediaryBankOptionD = ReadXMLElement(reader, "_oriIntermediaryBankOptionD")
            _curIntermediaryBankOptionA = ReadXMLElement(reader, "_curIntermediaryBankOptionA")
            _oldIntermediaryBankOptionA = ReadXMLElement(reader, "_oldIntermediaryBankOptionA")
            _oriIntermediaryBankOptionA = ReadXMLElement(reader, "_oriIntermediaryBankOptionA")
            _curIntermediaryBankOptionC = ReadXMLElement(reader, "_curIntermediaryBankOptionC")
            _oldIntermediaryBankOptionC = ReadXMLElement(reader, "_oldIntermediaryBankOptionC")
            _oriIntermediaryBankOptionC = ReadXMLElement(reader, "_oriIntermediaryBankOptionC")
            _curIntermediaryBankSWIFTBIC = ReadXMLElement(reader, "_curIntermediaryBankSWIFTBIC")
            _oldIntermediaryBankSWIFTBIC = ReadXMLElement(reader, "_oldIntermediaryBankSWIFTBIC")
            _oriIntermediaryBankSWIFTBIC = ReadXMLElement(reader, "_oriIntermediaryBankSWIFTBIC")
            _curIntermediaryBankNCC = ReadXMLElement(reader, "_curIntermediaryBankNCC")
            _oldIntermediaryBankNCC = ReadXMLElement(reader, "_oldIntermediaryBankNCC")
            _oriIntermediaryBankNCC = ReadXMLElement(reader, "_oriIntermediaryBankNCC")
            _curBeneficiaryBankAndBranch = ReadXMLElement(reader, "_curBeneficiaryBankAndBranch")
            _oldBeneficiaryBankAndBranch = ReadXMLElement(reader, "_oldBeneficiaryBankAndBranch")
            _oriBeneficiaryBankAndBranch = ReadXMLElement(reader, "_oriBeneficiaryBankAndBranch")
            _curBeneficiaryBank = ReadXMLElement(reader, "_curBeneficiaryBank")
            _oldBeneficiaryBank = ReadXMLElement(reader, "_oldBeneficiaryBank")
            _oriBeneficiaryBank = ReadXMLElement(reader, "_oriBeneficiaryBank")
            _curBeneficiaryBranch = ReadXMLElement(reader, "_curBeneficiaryBranch")
            _oldBeneficiaryBranch = ReadXMLElement(reader, "_oldBeneficiaryBranch")
            _oriBeneficiaryBranch = ReadXMLElement(reader, "_oriBeneficiaryBranch")
            _curBeneficiaryBankAddress1 = ReadXMLElement(reader, "_curBeneficiaryBankAddress1")
            _oldBeneficiaryBankAddress1 = ReadXMLElement(reader, "_oldBeneficiaryBankAddress1")
            _oriBeneficiaryBankAddress1 = ReadXMLElement(reader, "_oriBeneficiaryBankAddress1")
            _curBeneficiaryBankAddress2 = ReadXMLElement(reader, "_curBeneficiaryBankAddress2")
            _oldBeneficiaryBankAddress2 = ReadXMLElement(reader, "_oldBeneficiaryBankAddress2")
            _oriBeneficiaryBankAddress2 = ReadXMLElement(reader, "_oriBeneficiaryBankAddress2")
            _curBeneficiaryBankMasterCode = ReadXMLElement(reader, "_curBeneficiaryBankMasterCode")
            _oldBeneficiaryBankMasterCode = ReadXMLElement(reader, "_oldBeneficiaryBankMasterCode")
            _oriBeneficiaryBankMasterCode = ReadXMLElement(reader, "_oriBeneficiaryBankMasterCode")
            _curBeneficiaryBankOptionD = ReadXMLElement(reader, "_curBeneficiaryBankOptionD")
            _oldBeneficiaryBankOptionD = ReadXMLElement(reader, "_oldBeneficiaryBankOptionD")
            _oriBeneficiaryBankOptionD = ReadXMLElement(reader, "_oriBeneficiaryBankOptionD")
            _curBeneficiaryBankOptionA = ReadXMLElement(reader, "_curBeneficiaryBankOptionA")
            _oldBeneficiaryBankOptionA = ReadXMLElement(reader, "_oldBeneficiaryBankOptionA")
            _oriBeneficiaryBankOptionA = ReadXMLElement(reader, "_oriBeneficiaryBankOptionA")
            _curBeneficiaryBankOptionC = ReadXMLElement(reader, "_curBeneficiaryBankOptionC")
            _oldBeneficiaryBankOptionC = ReadXMLElement(reader, "_oldBeneficiaryBankOptionC")
            _oriBeneficiaryBankOptionC = ReadXMLElement(reader, "_oriBeneficiaryBankOptionC")
            _curBeneficiaryBankSWIFTBIC = ReadXMLElement(reader, "_curBeneficiaryBankSWIFTBIC")
            _oldBeneficiaryBankSWIFTBIC = ReadXMLElement(reader, "_oldBeneficiaryBankSWIFTBIC")
            _oriBeneficiaryBankSWIFTBIC = ReadXMLElement(reader, "_oriBeneficiaryBankSWIFTBIC")
            _curBeneficiaryBankNCC = ReadXMLElement(reader, "_curBeneficiaryBankNCC")
            _oldBeneficiaryBankNCC = ReadXMLElement(reader, "_oldBeneficiaryBankNCC")
            _oriBeneficiaryBankNCC = ReadXMLElement(reader, "_oriBeneficiaryBankNCC")
            _curBeneficiaryAccNoAndName = ReadXMLElement(reader, "_curBeneficiaryAccNoAndName")
            _oldBeneficiaryAccNoAndName = ReadXMLElement(reader, "_oldBeneficiaryAccNoAndName")
            _oriBeneficiaryAccNoAndName = ReadXMLElement(reader, "_oriBeneficiaryAccNoAndName")
            _curBeneficiaryAccNo = ReadXMLElement(reader, "_curBeneficiaryAccNo")
            _oldBeneficiaryAccNo = ReadXMLElement(reader, "_oldBeneficiaryAccNo")
            _oriBeneficiaryAccNo = ReadXMLElement(reader, "_oriBeneficiaryAccNo")
            _curBeneficiaryName1 = ReadXMLElement(reader, "_curBeneficiaryName1")
            _oldBeneficiaryName1 = ReadXMLElement(reader, "_oldBeneficiaryName1")
            _oriBeneficiaryName1 = ReadXMLElement(reader, "_oriBeneficiaryName1")
            _curBeneficiaryName2 = ReadXMLElement(reader, "_curBeneficiaryName2")
            _oldBeneficiaryName2 = ReadXMLElement(reader, "_oldBeneficiaryName2")
            _oriBeneficiaryName2 = ReadXMLElement(reader, "_oriBeneficiaryName2")
            _curBeneficiaryName3 = ReadXMLElement(reader, "_curBeneficiaryName3")
            _oldBeneficiaryName3 = ReadXMLElement(reader, "_oldBeneficiaryName3")
            _oriBeneficiaryName3 = ReadXMLElement(reader, "_oriBeneficiaryName3")
            _curBeneficiaryName4 = ReadXMLElement(reader, "_curBeneficiaryName4")
            _oldBeneficiaryName4 = ReadXMLElement(reader, "_oldBeneficiaryName4")
            _oriBeneficiaryName4 = ReadXMLElement(reader, "_oriBeneficiaryName4")
            _curBeneficiaryMsg1 = ReadXMLElement(reader, "_curBeneficiaryMsg1")
            _oldBeneficiaryMsg1 = ReadXMLElement(reader, "_oldBeneficiaryMsg1")
            _oriBeneficiaryMsg1 = ReadXMLElement(reader, "_oriBeneficiaryMsg1")
            _curBeneficiaryMsg2 = ReadXMLElement(reader, "_curBeneficiaryMsg2")
            _oldBeneficiaryMsg2 = ReadXMLElement(reader, "_oldBeneficiaryMsg2")
            _oriBeneficiaryMsg2 = ReadXMLElement(reader, "_oriBeneficiaryMsg2")
            _curBeneficiaryMsg3 = ReadXMLElement(reader, "_curBeneficiaryMsg3")
            _oldBeneficiaryMsg3 = ReadXMLElement(reader, "_oldBeneficiaryMsg3")
            _oriBeneficiaryMsg3 = ReadXMLElement(reader, "_oriBeneficiaryMsg3")
            _curBeneficiaryMsg4 = ReadXMLElement(reader, "_curBeneficiaryMsg4")
            _oldBeneficiaryMsg4 = ReadXMLElement(reader, "_oldBeneficiaryMsg4")
            _oriBeneficiaryMsg4 = ReadXMLElement(reader, "_oriBeneficiaryMsg4")
            _curBeneficiaryRemitPurpose = ReadXMLElement(reader, "_curBeneficiaryRemitPurpose")
            _oldBeneficiaryRemitPurpose = ReadXMLElement(reader, "_oldBeneficiaryRemitPurpose")
            _oriBeneficiaryRemitPurpose = ReadXMLElement(reader, "_oriBeneficiaryRemitPurpose")
            _curBeneficiaryRemitInfo1 = ReadXMLElement(reader, "_curBeneficiaryRemitInfo1")
            _oldBeneficiaryRemitInfo1 = ReadXMLElement(reader, "_oldBeneficiaryRemitInfo1")
            _oriBeneficiaryRemitInfo1 = ReadXMLElement(reader, "_oriBeneficiaryRemitInfo1")
            _curBeneficiaryRemitInfo2 = ReadXMLElement(reader, "_curBeneficiaryRemitInfo2")
            _oldBeneficiaryRemitInfo2 = ReadXMLElement(reader, "_oldBeneficiaryRemitInfo2")
            _oriBeneficiaryRemitInfo2 = ReadXMLElement(reader, "_oriBeneficiaryRemitInfo2")
            _curBeneficiaryRemitInfo3 = ReadXMLElement(reader, "_curBeneficiaryRemitInfo3")
            _oldBeneficiaryRemitInfo3 = ReadXMLElement(reader, "_oldBeneficiaryRemitInfo3")
            _oriBeneficiaryRemitInfo3 = ReadXMLElement(reader, "_oriBeneficiaryRemitInfo3")
            _curBankCharges = ReadXMLElement(reader, "_curBankCharges")
            _oldBankCharges = ReadXMLElement(reader, "_oldBankCharges")
            _oriBankCharges = ReadXMLElement(reader, "_oriBankCharges")
            _curChargesAccNoAndName = ReadXMLElement(reader, "_curChargesAccNoAndName")
            _oldChargesAccNoAndName = ReadXMLElement(reader, "_oldChargesAccNoAndName")
            _oriChargesAccNoAndName = ReadXMLElement(reader, "_oriChargesAccNoAndName")
            _curChargesAcc = ReadXMLElement(reader, "_curChargesAcc")
            _oldChargesAcc = ReadXMLElement(reader, "_oldChargesAcc")
            _oriChargesAcc = ReadXMLElement(reader, "_oriChargesAcc")
            _curChargesAccName1 = ReadXMLElement(reader, "_curChargesAccName1")
            _oldChargesAccName1 = ReadXMLElement(reader, "_oldChargesAccName1")
            _oriChargesAccName1 = ReadXMLElement(reader, "_oriChargesAccName1")
            _curChargesAccName2 = ReadXMLElement(reader, "_curChargesAccName2")
            _oldChargesAccName2 = ReadXMLElement(reader, "_oldChargesAccName2")
            _oriChargesAccName2 = ReadXMLElement(reader, "_oriChargesAccName2")
            _curChargesAccName3 = ReadXMLElement(reader, "_curChargesAccName3")
            _oldChargesAccName3 = ReadXMLElement(reader, "_oldChargesAccName3")
            _oriChargesAccName3 = ReadXMLElement(reader, "_oriChargesAccName3")
            _curChargesAccName4 = ReadXMLElement(reader, "_curChargesAccName4")
            _oldChargesAccName4 = ReadXMLElement(reader, "_oldChargesAccName4")
            _oriChargesAccName4 = ReadXMLElement(reader, "_oriChargesAccName4")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curSequenc", _curSequenc)
        WriteXmlElement(writer, "_oldSequenc", _oldSequenc)
        WriteXmlElement(writer, "_oriSequenc", _oriSequenc)
        WriteXmlElement(writer, "_curTemplateID", _curTemplateID)
        WriteXmlElement(writer, "_oldTemplateID", _oldTemplateID)
        WriteXmlElement(writer, "_oriTemplateID", _oriTemplateID)
        WriteXmlElement(writer, "_curSettlementAccNoAndName", _curSettlementAccNoAndName)
        WriteXmlElement(writer, "_oldSettlementAccNoAndName", _oldSettlementAccNoAndName)
        WriteXmlElement(writer, "_oriSettlementAccNoAndName", _oriSettlementAccNoAndName)
        WriteXmlElement(writer, "_curSettlementAccNo", _curSettlementAccNo)
        WriteXmlElement(writer, "_oldSettlementAccNo", _oldSettlementAccNo)
        WriteXmlElement(writer, "_oriSettlementAccNo", _oriSettlementAccNo)
        WriteXmlElement(writer, "_curSettlementAccName1", _curSettlementAccName1)
        WriteXmlElement(writer, "_oldSettlementAccName1", _oldSettlementAccName1)
        WriteXmlElement(writer, "_oriSettlementAccName1", _oriSettlementAccName1)
        WriteXmlElement(writer, "_curSettlementAccName2", _curSettlementAccName2)
        WriteXmlElement(writer, "_oldSettlementAccName2", _oldSettlementAccName2)
        WriteXmlElement(writer, "_oriSettlementAccName2", _oriSettlementAccName2)
        WriteXmlElement(writer, "_curSettlementAccName3", _curSettlementAccName3)
        WriteXmlElement(writer, "_oldSettlementAccName3", _oldSettlementAccName3)
        WriteXmlElement(writer, "_oriSettlementAccName3", _oriSettlementAccName3)
        WriteXmlElement(writer, "_curSettlementAccName4", _curSettlementAccName4)
        WriteXmlElement(writer, "_oldSettlementAccName4", _oldSettlementAccName4)
        WriteXmlElement(writer, "_oriSettlementAccName4", _oriSettlementAccName4)
        WriteXmlElement(writer, "_curSettlementBank", _curSettlementBank)
        WriteXmlElement(writer, "_oldSettlementBank", _oldSettlementBank)
        WriteXmlElement(writer, "_oriSettlementBank", _oriSettlementBank)
        WriteXmlElement(writer, "_curSettlementBranch", _curSettlementBranch)
        WriteXmlElement(writer, "_oldSettlementBranch", _oldSettlementBranch)
        WriteXmlElement(writer, "_oriSettlementBranch", _oriSettlementBranch)
        WriteXmlElement(writer, "_curSettlementBankAddress1", _curSettlementBankAddress1)
        WriteXmlElement(writer, "_oldSettlementBankAddress1", _oldSettlementBankAddress1)
        WriteXmlElement(writer, "_oriSettlementBankAddress1", _oriSettlementBankAddress1)
        WriteXmlElement(writer, "_curSettlementBankAddress2", _curSettlementBankAddress2)
        WriteXmlElement(writer, "_oldSettlementBankAddress2", _oldSettlementBankAddress2)
        WriteXmlElement(writer, "_oriSettlementBankAddress2", _oriSettlementBankAddress2)
        WriteXmlElement(writer, "_curSettlementAccCurrency", _curSettlementAccCurrency)
        WriteXmlElement(writer, "_oldSettlementAccCurrency", _oldSettlementAccCurrency)
        WriteXmlElement(writer, "_oriSettlementAccCurrency", _oriSettlementAccCurrency)
        WriteXmlElement(writer, "_curValueDate", _curValueDate)
        WriteXmlElement(writer, "_oldValueDate", _oldValueDate)
        WriteXmlElement(writer, "_oriValueDate", _oriValueDate)
        WriteXmlElement(writer, "_curCustomerReference", _curCustomerReference)
        WriteXmlElement(writer, "_oldCustomerReference", _oldCustomerReference)
        WriteXmlElement(writer, "_oriCustomerReference", _oriCustomerReference)
        WriteXmlElement(writer, "_curSectorSelection", _curSectorSelection)
        WriteXmlElement(writer, "_oldSectorSelection", _oldSectorSelection)
        WriteXmlElement(writer, "_oriSectorSelection", _oriSectorSelection)
        WriteXmlElement(writer, "_curCurrency", _curCurrency)
        WriteXmlElement(writer, "_oldCurrency", _oldCurrency)
        WriteXmlElement(writer, "_oriCurrency", _oriCurrency)
        WriteXmlElement(writer, "_curRemitAmount", _curRemitAmount)
        WriteXmlElement(writer, "_oldRemitAmount", _oldRemitAmount)
        WriteXmlElement(writer, "_oriRemitAmount", _oriRemitAmount)
        WriteXmlElement(writer, "_curExchangeMethod", _curExchangeMethod)
        WriteXmlElement(writer, "_oldExchangeMethod", _oldExchangeMethod)
        WriteXmlElement(writer, "_oriExchangeMethod", _oriExchangeMethod)
        WriteXmlElement(writer, "_curContractNo", _curContractNo)
        WriteXmlElement(writer, "_oldContractNo", _oldContractNo)
        WriteXmlElement(writer, "_oriContractNo", _oriContractNo)
        WriteXmlElement(writer, "_curIntermediaryBankAndBranch", _curIntermediaryBankAndBranch)
        WriteXmlElement(writer, "_oldIntermediaryBankAndBranch", _oldIntermediaryBankAndBranch)
        WriteXmlElement(writer, "_oriIntermediaryBankAndBranch", _oriIntermediaryBankAndBranch)
        WriteXmlElement(writer, "_curIntermediaryBank", _curIntermediaryBank)
        WriteXmlElement(writer, "_oldIntermediaryBank", _oldIntermediaryBank)
        WriteXmlElement(writer, "_oriIntermediaryBank", _oriIntermediaryBank)
        WriteXmlElement(writer, "_curIntermediaryBranch", _curIntermediaryBranch)
        WriteXmlElement(writer, "_oldIntermediaryBranch", _oldIntermediaryBranch)
        WriteXmlElement(writer, "_oriIntermediaryBranch", _oriIntermediaryBranch)
        WriteXmlElement(writer, "_curIntermediaryBankAddress1", _curIntermediaryBankAddress1)
        WriteXmlElement(writer, "_oldIntermediaryBankAddress1", _oldIntermediaryBankAddress1)
        WriteXmlElement(writer, "_oriIntermediaryBankAddress1", _oriIntermediaryBankAddress1)
        WriteXmlElement(writer, "_curIntermediaryBankAddress2", _curIntermediaryBankAddress2)
        WriteXmlElement(writer, "_oldIntermediaryBankAddress2", _oldIntermediaryBankAddress2)
        WriteXmlElement(writer, "_oriIntermediaryBankAddress2", _oriIntermediaryBankAddress2)
        WriteXmlElement(writer, "_curIntermediaryBankMasterCode", _curIntermediaryBankMasterCode)
        WriteXmlElement(writer, "_oldIntermediaryBankMasterCode", _oldIntermediaryBankMasterCode)
        WriteXmlElement(writer, "_oriIntermediaryBankMasterCode", _oriIntermediaryBankMasterCode)
        WriteXmlElement(writer, "_curIntermediaryBankOptionD", _curIntermediaryBankOptionD)
        WriteXmlElement(writer, "_oldIntermediaryBankOptionD", _oldIntermediaryBankOptionD)
        WriteXmlElement(writer, "_oriIntermediaryBankOptionD", _oriIntermediaryBankOptionD)
        WriteXmlElement(writer, "_curIntermediaryBankOptionA", _curIntermediaryBankOptionA)
        WriteXmlElement(writer, "_oldIntermediaryBankOptionA", _oldIntermediaryBankOptionA)
        WriteXmlElement(writer, "_oriIntermediaryBankOptionA", _oriIntermediaryBankOptionA)
        WriteXmlElement(writer, "_curIntermediaryBankOptionC", _curIntermediaryBankOptionC)
        WriteXmlElement(writer, "_oldIntermediaryBankOptionC", _oldIntermediaryBankOptionC)
        WriteXmlElement(writer, "_oriIntermediaryBankOptionC", _oriIntermediaryBankOptionC)
        WriteXmlElement(writer, "_curIntermediaryBankSWIFTBIC", _curIntermediaryBankSWIFTBIC)
        WriteXmlElement(writer, "_oldIntermediaryBankSWIFTBIC", _oldIntermediaryBankSWIFTBIC)
        WriteXmlElement(writer, "_oriIntermediaryBankSWIFTBIC", _oriIntermediaryBankSWIFTBIC)
        WriteXmlElement(writer, "_curIntermediaryBankNCC", _curIntermediaryBankNCC)
        WriteXmlElement(writer, "_oldIntermediaryBankNCC", _oldIntermediaryBankNCC)
        WriteXmlElement(writer, "_oriIntermediaryBankNCC", _oriIntermediaryBankNCC)
        WriteXmlElement(writer, "_curBeneficiaryBankAndBranch", _curBeneficiaryBankAndBranch)
        WriteXmlElement(writer, "_oldBeneficiaryBankAndBranch", _oldBeneficiaryBankAndBranch)
        WriteXmlElement(writer, "_oriBeneficiaryBankAndBranch", _oriBeneficiaryBankAndBranch)
        WriteXmlElement(writer, "_curBeneficiaryBank", _curBeneficiaryBank)
        WriteXmlElement(writer, "_oldBeneficiaryBank", _oldBeneficiaryBank)
        WriteXmlElement(writer, "_oriBeneficiaryBank", _oriBeneficiaryBank)
        WriteXmlElement(writer, "_curBeneficiaryBranch", _curBeneficiaryBranch)
        WriteXmlElement(writer, "_oldBeneficiaryBranch", _oldBeneficiaryBranch)
        WriteXmlElement(writer, "_oriBeneficiaryBranch", _oriBeneficiaryBranch)
        WriteXmlElement(writer, "_curBeneficiaryBankAddress1", _curBeneficiaryBankAddress1)
        WriteXmlElement(writer, "_oldBeneficiaryBankAddress1", _oldBeneficiaryBankAddress1)
        WriteXmlElement(writer, "_oriBeneficiaryBankAddress1", _oriBeneficiaryBankAddress1)
        WriteXmlElement(writer, "_curBeneficiaryBankAddress2", _curBeneficiaryBankAddress2)
        WriteXmlElement(writer, "_oldBeneficiaryBankAddress2", _oldBeneficiaryBankAddress2)
        WriteXmlElement(writer, "_oriBeneficiaryBankAddress2", _oriBeneficiaryBankAddress2)
        WriteXmlElement(writer, "_curBeneficiaryBankMasterCode", _curBeneficiaryBankMasterCode)
        WriteXmlElement(writer, "_oldBeneficiaryBankMasterCode", _oldBeneficiaryBankMasterCode)
        WriteXmlElement(writer, "_oriBeneficiaryBankMasterCode", _oriBeneficiaryBankMasterCode)
        WriteXmlElement(writer, "_curBeneficiaryBankOptionD", _curBeneficiaryBankOptionD)
        WriteXmlElement(writer, "_oldBeneficiaryBankOptionD", _oldBeneficiaryBankOptionD)
        WriteXmlElement(writer, "_oriBeneficiaryBankOptionD", _oriBeneficiaryBankOptionD)
        WriteXmlElement(writer, "_curBeneficiaryBankOptionA", _curBeneficiaryBankOptionA)
        WriteXmlElement(writer, "_oldBeneficiaryBankOptionA", _oldBeneficiaryBankOptionA)
        WriteXmlElement(writer, "_oriBeneficiaryBankOptionA", _oriBeneficiaryBankOptionA)
        WriteXmlElement(writer, "_curBeneficiaryBankOptionC", _curBeneficiaryBankOptionC)
        WriteXmlElement(writer, "_oldBeneficiaryBankOptionC", _oldBeneficiaryBankOptionC)
        WriteXmlElement(writer, "_oriBeneficiaryBankOptionC", _oriBeneficiaryBankOptionC)
        WriteXmlElement(writer, "_curBeneficiaryBankSWIFTBIC", _curBeneficiaryBankSWIFTBIC)
        WriteXmlElement(writer, "_oldBeneficiaryBankSWIFTBIC", _oldBeneficiaryBankSWIFTBIC)
        WriteXmlElement(writer, "_oriBeneficiaryBankSWIFTBIC", _oriBeneficiaryBankSWIFTBIC)
        WriteXmlElement(writer, "_curBeneficiaryBankNCC", _curBeneficiaryBankNCC)
        WriteXmlElement(writer, "_oldBeneficiaryBankNCC", _oldBeneficiaryBankNCC)
        WriteXmlElement(writer, "_oriBeneficiaryBankNCC", _oriBeneficiaryBankNCC)
        WriteXmlElement(writer, "_curBeneficiaryAccNoAndName", _curBeneficiaryAccNoAndName)
        WriteXmlElement(writer, "_oldBeneficiaryAccNoAndName", _oldBeneficiaryAccNoAndName)
        WriteXmlElement(writer, "_oriBeneficiaryAccNoAndName", _oriBeneficiaryAccNoAndName)
        WriteXmlElement(writer, "_curBeneficiaryAccNo", _curBeneficiaryAccNo)
        WriteXmlElement(writer, "_oldBeneficiaryAccNo", _oldBeneficiaryAccNo)
        WriteXmlElement(writer, "_oriBeneficiaryAccNo", _oriBeneficiaryAccNo)
        WriteXmlElement(writer, "_curBeneficiaryName1", _curBeneficiaryName1)
        WriteXmlElement(writer, "_oldBeneficiaryName1", _oldBeneficiaryName1)
        WriteXmlElement(writer, "_oriBeneficiaryName1", _oriBeneficiaryName1)
        WriteXmlElement(writer, "_curBeneficiaryName2", _curBeneficiaryName2)
        WriteXmlElement(writer, "_oldBeneficiaryName2", _oldBeneficiaryName2)
        WriteXmlElement(writer, "_oriBeneficiaryName2", _oriBeneficiaryName2)
        WriteXmlElement(writer, "_curBeneficiaryName3", _curBeneficiaryName3)
        WriteXmlElement(writer, "_oldBeneficiaryName3", _oldBeneficiaryName3)
        WriteXmlElement(writer, "_oriBeneficiaryName3", _oriBeneficiaryName3)
        WriteXmlElement(writer, "_curBeneficiaryName4", _curBeneficiaryName4)
        WriteXmlElement(writer, "_oldBeneficiaryName4", _oldBeneficiaryName4)
        WriteXmlElement(writer, "_oriBeneficiaryName4", _oriBeneficiaryName4)
        WriteXmlElement(writer, "_curBeneficiaryMsg1", _curBeneficiaryMsg1)
        WriteXmlElement(writer, "_oldBeneficiaryMsg1", _oldBeneficiaryMsg1)
        WriteXmlElement(writer, "_oriBeneficiaryMsg1", _oriBeneficiaryMsg1)
        WriteXmlElement(writer, "_curBeneficiaryMsg2", _curBeneficiaryMsg2)
        WriteXmlElement(writer, "_oldBeneficiaryMsg2", _oldBeneficiaryMsg2)
        WriteXmlElement(writer, "_oriBeneficiaryMsg2", _oriBeneficiaryMsg2)
        WriteXmlElement(writer, "_curBeneficiaryMsg3", _curBeneficiaryMsg3)
        WriteXmlElement(writer, "_oldBeneficiaryMsg3", _oldBeneficiaryMsg3)
        WriteXmlElement(writer, "_oriBeneficiaryMsg3", _oriBeneficiaryMsg3)
        WriteXmlElement(writer, "_curBeneficiaryMsg4", _curBeneficiaryMsg4)
        WriteXmlElement(writer, "_oldBeneficiaryMsg4", _oldBeneficiaryMsg4)
        WriteXmlElement(writer, "_oriBeneficiaryMsg4", _oriBeneficiaryMsg4)
        WriteXmlElement(writer, "_curBeneficiaryRemitPurpose", _curBeneficiaryRemitPurpose)
        WriteXmlElement(writer, "_oldBeneficiaryRemitPurpose", _oldBeneficiaryRemitPurpose)
        WriteXmlElement(writer, "_oriBeneficiaryRemitPurpose", _oriBeneficiaryRemitPurpose)
        WriteXmlElement(writer, "_curBeneficiaryRemitInfo1", _curBeneficiaryRemitInfo1)
        WriteXmlElement(writer, "_oldBeneficiaryRemitInfo1", _oldBeneficiaryRemitInfo1)
        WriteXmlElement(writer, "_oriBeneficiaryRemitInfo1", _oriBeneficiaryRemitInfo1)
        WriteXmlElement(writer, "_curBeneficiaryRemitInfo2", _curBeneficiaryRemitInfo2)
        WriteXmlElement(writer, "_oldBeneficiaryRemitInfo2", _oldBeneficiaryRemitInfo2)
        WriteXmlElement(writer, "_oriBeneficiaryRemitInfo2", _oriBeneficiaryRemitInfo2)
        WriteXmlElement(writer, "_curBeneficiaryRemitInfo3", _curBeneficiaryRemitInfo3)
        WriteXmlElement(writer, "_oldBeneficiaryRemitInfo3", _oldBeneficiaryRemitInfo3)
        WriteXmlElement(writer, "_oriBeneficiaryRemitInfo3", _oriBeneficiaryRemitInfo3)
        WriteXmlElement(writer, "_curBankCharges", _curBankCharges)
        WriteXmlElement(writer, "_oldBankCharges", _oldBankCharges)
        WriteXmlElement(writer, "_oriBankCharges", _oriBankCharges)
        WriteXmlElement(writer, "_curChargesAccNoAndName", _curChargesAccNoAndName)
        WriteXmlElement(writer, "_oldChargesAccNoAndName", _oldChargesAccNoAndName)
        WriteXmlElement(writer, "_oriChargesAccNoAndName", _oriChargesAccNoAndName)
        WriteXmlElement(writer, "_curChargesAcc", _curChargesAcc)
        WriteXmlElement(writer, "_oldChargesAcc", _oldChargesAcc)
        WriteXmlElement(writer, "_oriChargesAcc", _oriChargesAcc)
        WriteXmlElement(writer, "_curChargesAccName1", _curChargesAccName1)
        WriteXmlElement(writer, "_oldChargesAccName1", _oldChargesAccName1)
        WriteXmlElement(writer, "_oriChargesAccName1", _oriChargesAccName1)
        WriteXmlElement(writer, "_curChargesAccName2", _curChargesAccName2)
        WriteXmlElement(writer, "_oldChargesAccName2", _oldChargesAccName2)
        WriteXmlElement(writer, "_oriChargesAccName2", _oriChargesAccName2)
        WriteXmlElement(writer, "_curChargesAccName3", _curChargesAccName3)
        WriteXmlElement(writer, "_oldChargesAccName3", _oldChargesAccName3)
        WriteXmlElement(writer, "_oriChargesAccName3", _oriChargesAccName3)
        WriteXmlElement(writer, "_curChargesAccName4", _curChargesAccName4)
        WriteXmlElement(writer, "_oldChargesAccName4", _oldChargesAccName4)
        WriteXmlElement(writer, "_oriChargesAccName4", _oriChargesAccName4)
    End Sub
#End Region

#Region "  Save and Loading  "
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(GCMSMTTransactionDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub

    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As GCMSMTTransactionDetail
        Dim serializer As New XmlSerializer(GetType(GCMSMTTransactionDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), GCMSMTTransactionDetail)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub

    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As GCMSMTTransactionDetail = DirectCast(obj, GCMSMTTransactionDetail)
        _curSequenc = objE._curSequenc
        _oldSequenc = objE._oldSequenc
        _oriSequenc = objE._oriSequenc
        _curTemplateID = objE._curTemplateID
        _oldTemplateID = objE._oldTemplateID
        _oriTemplateID = objE._oriTemplateID
        _curSettlementAccNoAndName = objE._curSettlementAccNoAndName
        _oldSettlementAccNoAndName = objE._oldSettlementAccNoAndName
        _oriSettlementAccNoAndName = objE._oriSettlementAccNoAndName
        _curSettlementAccNo = objE._curSettlementAccNo
        _oldSettlementAccNo = objE._oldSettlementAccNo
        _oriSettlementAccNo = objE._oriSettlementAccNo
        _curSettlementAccName1 = objE._curSettlementAccName1
        _oldSettlementAccName1 = objE._oldSettlementAccName1
        _oriSettlementAccName1 = objE._oriSettlementAccName1
        _curSettlementAccName2 = objE._curSettlementAccName2
        _oldSettlementAccName2 = objE._oldSettlementAccName2
        _oriSettlementAccName2 = objE._oriSettlementAccName2
        _curSettlementAccName3 = objE._curSettlementAccName3
        _oldSettlementAccName3 = objE._oldSettlementAccName3
        _oriSettlementAccName3 = objE._oriSettlementAccName3
        _curSettlementAccName4 = objE._curSettlementAccName4
        _oldSettlementAccName4 = objE._oldSettlementAccName4
        _oriSettlementAccName4 = objE._oriSettlementAccName4
        _curSettlementBank = objE._curSettlementBank
        _oldSettlementBank = objE._oldSettlementBank
        _oriSettlementBank = objE._oriSettlementBank
        _curSettlementBranch = objE._curSettlementBranch
        _oldSettlementBranch = objE._oldSettlementBranch
        _oriSettlementBranch = objE._oriSettlementBranch
        _curSettlementBankAddress1 = objE._curSettlementBankAddress1
        _oldSettlementBankAddress1 = objE._oldSettlementBankAddress1
        _oriSettlementBankAddress1 = objE._oriSettlementBankAddress1
        _curSettlementBankAddress2 = objE._curSettlementBankAddress2
        _oldSettlementBankAddress2 = objE._oldSettlementBankAddress2
        _oriSettlementBankAddress2 = objE._oriSettlementBankAddress2
        _curSettlementAccCurrency = objE._curSettlementAccCurrency
        _oldSettlementAccCurrency = objE._oldSettlementAccCurrency
        _oriSettlementAccCurrency = objE._oriSettlementAccCurrency
        _curValueDate = objE._curValueDate
        _oldValueDate = objE._oldValueDate
        _oriValueDate = objE._oriValueDate
        _curCustomerReference = objE._curCustomerReference
        _oldCustomerReference = objE._oldCustomerReference
        _oriCustomerReference = objE._oriCustomerReference
        _curSectorSelection = objE._curSectorSelection
        _oldSectorSelection = objE._oldSectorSelection
        _oriSectorSelection = objE._oriSectorSelection
        _curCurrency = objE._curCurrency
        _oldCurrency = objE._oldCurrency
        _oriCurrency = objE._oriCurrency
        _curRemitAmount = objE._curRemitAmount
        _oldRemitAmount = objE._oldRemitAmount
        _oriRemitAmount = objE._oriRemitAmount
        _curExchangeMethod = objE._curExchangeMethod
        _oldExchangeMethod = objE._oldExchangeMethod
        _oriExchangeMethod = objE._oriExchangeMethod
        _curContractNo = objE._curContractNo
        _oldContractNo = objE._oldContractNo
        _oriContractNo = objE._oriContractNo
        _curIntermediaryBankAndBranch = objE._curIntermediaryBankAndBranch
        _oldIntermediaryBankAndBranch = objE._oldIntermediaryBankAndBranch
        _oriIntermediaryBankAndBranch = objE._oriIntermediaryBankAndBranch
        _curIntermediaryBank = objE._curIntermediaryBank
        _oldIntermediaryBank = objE._oldIntermediaryBank
        _oriIntermediaryBank = objE._oriIntermediaryBank
        _curIntermediaryBranch = objE._curIntermediaryBranch
        _oldIntermediaryBranch = objE._oldIntermediaryBranch
        _oriIntermediaryBranch = objE._oriIntermediaryBranch
        _curIntermediaryBankAddress1 = objE._curIntermediaryBankAddress1
        _oldIntermediaryBankAddress1 = objE._oldIntermediaryBankAddress1
        _oriIntermediaryBankAddress1 = objE._oriIntermediaryBankAddress1
        _curIntermediaryBankAddress2 = objE._curIntermediaryBankAddress2
        _oldIntermediaryBankAddress2 = objE._oldIntermediaryBankAddress2
        _oriIntermediaryBankAddress2 = objE._oriIntermediaryBankAddress2
        _curIntermediaryBankMasterCode = objE._curIntermediaryBankMasterCode
        _oldIntermediaryBankMasterCode = objE._oldIntermediaryBankMasterCode
        _oriIntermediaryBankMasterCode = objE._oriIntermediaryBankMasterCode
        _curIntermediaryBankOptionD = objE._curIntermediaryBankOptionD
        _oldIntermediaryBankOptionD = objE._oldIntermediaryBankOptionD
        _oriIntermediaryBankOptionD = objE._oriIntermediaryBankOptionD
        _curIntermediaryBankOptionA = objE._curIntermediaryBankOptionA
        _oldIntermediaryBankOptionA = objE._oldIntermediaryBankOptionA
        _oriIntermediaryBankOptionA = objE._oriIntermediaryBankOptionA
        _curIntermediaryBankOptionC = objE._curIntermediaryBankOptionC
        _oldIntermediaryBankOptionC = objE._oldIntermediaryBankOptionC
        _oriIntermediaryBankOptionC = objE._oriIntermediaryBankOptionC
        _curIntermediaryBankSWIFTBIC = objE._curIntermediaryBankSWIFTBIC
        _oldIntermediaryBankSWIFTBIC = objE._oldIntermediaryBankSWIFTBIC
        _oriIntermediaryBankSWIFTBIC = objE._oriIntermediaryBankSWIFTBIC
        _curIntermediaryBankNCC = objE._curIntermediaryBankNCC
        _oldIntermediaryBankNCC = objE._oldIntermediaryBankNCC
        _oriIntermediaryBankNCC = objE._oriIntermediaryBankNCC
        _curBeneficiaryBankAndBranch = objE._curBeneficiaryBankAndBranch
        _oldBeneficiaryBankAndBranch = objE._oldBeneficiaryBankAndBranch
        _oriBeneficiaryBankAndBranch = objE._oriBeneficiaryBankAndBranch
        _curBeneficiaryBank = objE._curBeneficiaryBank
        _oldBeneficiaryBank = objE._oldBeneficiaryBank
        _oriBeneficiaryBank = objE._oriBeneficiaryBank
        _curBeneficiaryBranch = objE._curBeneficiaryBranch
        _oldBeneficiaryBranch = objE._oldBeneficiaryBranch
        _oriBeneficiaryBranch = objE._oriBeneficiaryBranch
        _curBeneficiaryBankAddress1 = objE._curBeneficiaryBankAddress1
        _oldBeneficiaryBankAddress1 = objE._oldBeneficiaryBankAddress1
        _oriBeneficiaryBankAddress1 = objE._oriBeneficiaryBankAddress1
        _curBeneficiaryBankAddress2 = objE._curBeneficiaryBankAddress2
        _oldBeneficiaryBankAddress2 = objE._oldBeneficiaryBankAddress2
        _oriBeneficiaryBankAddress2 = objE._oriBeneficiaryBankAddress2
        _curBeneficiaryBankMasterCode = objE._curBeneficiaryBankMasterCode
        _oldBeneficiaryBankMasterCode = objE._oldBeneficiaryBankMasterCode
        _oriBeneficiaryBankMasterCode = objE._oriBeneficiaryBankMasterCode
        _curBeneficiaryBankOptionD = objE._curBeneficiaryBankOptionD
        _oldBeneficiaryBankOptionD = objE._oldBeneficiaryBankOptionD
        _oriBeneficiaryBankOptionD = objE._oriBeneficiaryBankOptionD
        _curBeneficiaryBankOptionA = objE._curBeneficiaryBankOptionA
        _oldBeneficiaryBankOptionA = objE._oldBeneficiaryBankOptionA
        _oriBeneficiaryBankOptionA = objE._oriBeneficiaryBankOptionA
        _curBeneficiaryBankOptionC = objE._curBeneficiaryBankOptionC
        _oldBeneficiaryBankOptionC = objE._oldBeneficiaryBankOptionC
        _oriBeneficiaryBankOptionC = objE._oriBeneficiaryBankOptionC
        _curBeneficiaryBankSWIFTBIC = objE._curBeneficiaryBankSWIFTBIC
        _oldBeneficiaryBankSWIFTBIC = objE._oldBeneficiaryBankSWIFTBIC
        _oriBeneficiaryBankSWIFTBIC = objE._oriBeneficiaryBankSWIFTBIC
        _curBeneficiaryBankNCC = objE._curBeneficiaryBankNCC
        _oldBeneficiaryBankNCC = objE._oldBeneficiaryBankNCC
        _oriBeneficiaryBankNCC = objE._oriBeneficiaryBankNCC
        _curBeneficiaryAccNoAndName = objE._curBeneficiaryAccNoAndName
        _oldBeneficiaryAccNoAndName = objE._oldBeneficiaryAccNoAndName
        _oriBeneficiaryAccNoAndName = objE._oriBeneficiaryAccNoAndName
        _curBeneficiaryAccNo = objE._curBeneficiaryAccNo
        _oldBeneficiaryAccNo = objE._oldBeneficiaryAccNo
        _oriBeneficiaryAccNo = objE._oriBeneficiaryAccNo
        _curBeneficiaryName1 = objE._curBeneficiaryName1
        _oldBeneficiaryName1 = objE._oldBeneficiaryName1
        _oriBeneficiaryName1 = objE._oriBeneficiaryName1
        _curBeneficiaryName2 = objE._curBeneficiaryName2
        _oldBeneficiaryName2 = objE._oldBeneficiaryName2
        _oriBeneficiaryName2 = objE._oriBeneficiaryName2
        _curBeneficiaryName3 = objE._curBeneficiaryName3
        _oldBeneficiaryName3 = objE._oldBeneficiaryName3
        _oriBeneficiaryName3 = objE._oriBeneficiaryName3
        _curBeneficiaryName4 = objE._curBeneficiaryName4
        _oldBeneficiaryName4 = objE._oldBeneficiaryName4
        _oriBeneficiaryName4 = objE._oriBeneficiaryName4
        _curBeneficiaryMsg1 = objE._curBeneficiaryMsg1
        _oldBeneficiaryMsg1 = objE._oldBeneficiaryMsg1
        _oriBeneficiaryMsg1 = objE._oriBeneficiaryMsg1
        _curBeneficiaryMsg2 = objE._curBeneficiaryMsg2
        _oldBeneficiaryMsg2 = objE._oldBeneficiaryMsg2
        _oriBeneficiaryMsg2 = objE._oriBeneficiaryMsg2
        _curBeneficiaryMsg3 = objE._curBeneficiaryMsg3
        _oldBeneficiaryMsg3 = objE._oldBeneficiaryMsg3
        _oriBeneficiaryMsg3 = objE._oriBeneficiaryMsg3
        _curBeneficiaryMsg4 = objE._curBeneficiaryMsg4
        _oldBeneficiaryMsg4 = objE._oldBeneficiaryMsg4
        _oriBeneficiaryMsg4 = objE._oriBeneficiaryMsg4
        _curBeneficiaryRemitPurpose = objE._curBeneficiaryRemitPurpose
        _oldBeneficiaryRemitPurpose = objE._oldBeneficiaryRemitPurpose
        _oriBeneficiaryRemitPurpose = objE._oriBeneficiaryRemitPurpose
        _curBeneficiaryRemitInfo1 = objE._curBeneficiaryRemitInfo1
        _oldBeneficiaryRemitInfo1 = objE._oldBeneficiaryRemitInfo1
        _oriBeneficiaryRemitInfo1 = objE._oriBeneficiaryRemitInfo1
        _curBeneficiaryRemitInfo2 = objE._curBeneficiaryRemitInfo2
        _oldBeneficiaryRemitInfo2 = objE._oldBeneficiaryRemitInfo2
        _oriBeneficiaryRemitInfo2 = objE._oriBeneficiaryRemitInfo2
        _curBeneficiaryRemitInfo3 = objE._curBeneficiaryRemitInfo3
        _oldBeneficiaryRemitInfo3 = objE._oldBeneficiaryRemitInfo3
        _oriBeneficiaryRemitInfo3 = objE._oriBeneficiaryRemitInfo3
        _curBankCharges = objE._curBankCharges
        _oldBankCharges = objE._oldBankCharges
        _oriBankCharges = objE._oriBankCharges
        _curChargesAccNoAndName = objE._curChargesAccNoAndName
        _oldChargesAccNoAndName = objE._oldChargesAccNoAndName
        _oriChargesAccNoAndName = objE._oriChargesAccNoAndName
        _curChargesAcc = objE._curChargesAcc
        _oldChargesAcc = objE._oldChargesAcc
        _oriChargesAcc = objE._oriChargesAcc
        _curChargesAccName1 = objE._curChargesAccName1
        _oldChargesAccName1 = objE._oldChargesAccName1
        _oriChargesAccName1 = objE._oriChargesAccName1
        _curChargesAccName2 = objE._curChargesAccName2
        _oldChargesAccName2 = objE._oldChargesAccName2
        _oriChargesAccName2 = objE._oriChargesAccName2
        _curChargesAccName3 = objE._curChargesAccName3
        _oldChargesAccName3 = objE._oldChargesAccName3
        _oriChargesAccName3 = objE._oriChargesAccName3
        _curChargesAccName4 = objE._curChargesAccName4
        _oldChargesAccName4 = objE._oldChargesAccName4
        _oriChargesAccName4 = objE._oriChargesAccName4
    End Sub

    ''' <summary>
    ''' Copy transaction detail
    ''' </summary>
    ''' <param name="objTransDet">Instance of transaction detail</param>
    ''' <remarks></remarks>
    Public Sub CopyTransactionDetail(ByVal objTransDet As GCMSMTTransactionDetail)
        Clone(objTransDet)
    End Sub

    ''' <summary>
    ''' Unmark dirty flag   
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UnmarkDirty()
        _isDirty = False
    End Sub

#End Region

End Class


Partial Public Class GCMSMTTransactionDetailCollection
    Inherits System.ComponentModel.BindingList(Of GCMSMTTransactionDetail)
End Class
