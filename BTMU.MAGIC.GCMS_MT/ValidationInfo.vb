Imports System
Imports System.IO
Imports System.Globalization
Imports System.Text.RegularExpressions
Imports BTMU.MAGIC.Common


Partial Public Class GCMSMTTransactionDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub OnPropertyChanged(ByVal propertyName As String)
        MyBase.OnPropertyChanged(propertyName)
        If propertyName = "SettlementAccNoAndName" Then
            OnPropertyChanged("Currency")
            OnPropertyChanged("BankCharges")
        ElseIf propertyName = "Currency" Then
            OnPropertyChanged("RemitAmount")
            OnPropertyChanged("ExchangeMethod")
        ElseIf propertyName = "ExchangeMethod" Then
            OnPropertyChanged("ContractNo")
        ElseIf propertyName = "BankCharges" Then
            OnPropertyChanged("ChargesAccNoAndName")
        ElseIf propertyName = "SettlementAccCurrency" Then
            OnPropertyChanged("ExchangeMethod")
        End If

    End Sub

    Protected Overrides Sub Initialize()

        'Required 
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SettlementAccNoAndName", "Settlement Acc. No/Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("ValueDate", "Value Date"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("CustomerReference", "Customer Reference"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SectorSelection", "Sector Selection"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Currency", "Currency"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("RemitAmount", "Remittance Amount"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BeneficiaryName1", "Beneficiary Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BankCharges", "Bank Charges"))

        'Format 
        ValidationEngine.AddValidationRule(AddressOf E04000270, New Common.ValidationArgs("ValueDate", "Value Date")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000280, New Common.ValidationArgs("ValueDate", "Value Date")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000330, New Common.ValidationArgs("RemitAmount", "Remit Amount")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000340, New Common.ValidationArgs("RemitAmount", "Remit Amount")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000350, New Common.ValidationArgs("RemitAmount", "Remit Amount")) '--

        ValidationEngine.AddValidationRule(AddressOf E04000360, New Common.ValidationArgs("ExchangeMethod", "Exchange Method")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000370, New Common.ValidationArgs("ExchangeMethod", "Exchange Method")) '--

        ValidationEngine.AddValidationRule(AddressOf E04000380, New Common.ValidationArgs("ContractNo", "Contract No")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000390, New Common.ValidationArgs("ContractNo", "Contract No")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000400, New Common.ValidationArgs("ContractNo", "Contract No")) '--

        ValidationEngine.AddValidationRule(AddressOf E04000410, New Common.ValidationArgs("IntermediaryBank", "Intermediary Bank/Branch/Address")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000420, New Common.ValidationArgs("IntermediaryBank", "Intermediary Bank/Branch/Address")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000430, New Common.ValidationArgs("BeneficiaryBank", "Beneficiary Bank/Branch/Address")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000440, New Common.ValidationArgs("BeneficiaryBank", "Beneficiary Bank/Branch/Address")) '--

        ValidationEngine.AddValidationRule(AddressOf E04000470, New Common.ValidationArgs("ChargesAccNoAndName", "Charges Acc/Name")) '--
        ValidationEngine.AddValidationRule(AddressOf E04000480, New Common.ValidationArgs("ChargesAccNoAndName", "Charges Acc/Name")) '--

        ValidationEngine.Validate()

    End Sub

    ''' <summary>
    ''' Validate value date format
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid date, False = invalid date</returns>
    ''' <remarks></remarks>
    Public Function E04000270(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) AndAlso _
                Not IsDateDataType(value, "yyMMdd", False) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000270"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate value date range
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid date, False = invalid date</returns>
    ''' <remarks></remarks>
    Public Function E04000280(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim boolValid As Boolean = True
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) AndAlso IsDateDataType(value, "yyMMdd", False) Then
                Dim provider As CultureInfo = CultureInfo.InvariantCulture
                Dim valueDate As DateTime = DateTime.ParseExact(value, "yyMMdd", provider)

                If valueDate >= DateTime.Now.Date AndAlso valueDate <= DateTime.Now.Date.AddDays(20) Then
                Else
                    boolValid = False
                End If
            End If

            If Not boolValid Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000280"), ValidationArgs.GetFriendlyPropertyName(e)) '--
            End If

            Return boolValid

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate remittance amount format for a valid number
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid amount, False = invalid amount</returns>
    ''' <remarks></remarks>
    Public Function E04000330(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try

            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) And Not IsNumeric(value) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000330"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate remittance amount (greater than zero)
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid amount, False = invalid amount</returns>
    ''' <remarks></remarks>
    Public Function E04000340(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) AndAlso IsNumeric(value) AndAlso Double.Parse(value) <= 0 Then
                e.Description = _
                        String.Format(MsgReader.GetString("E04000340"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate remittance amount decimal places based on currency
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid amount, False = invalid amount</returns>
    ''' <remarks></remarks>
    Public Function E04000350(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim boolValid As Boolean = True
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) AndAlso IsNumeric(value) AndAlso Double.Parse(value) > 0 Then
                Dim shrtDeftDecimal As Short = HelperModule.GetCurrencyDecimalPoint(Me.Currency)
                Dim shrtInpDecimalLen As Short = 0
                Dim shrtInpNonDecimalLen As Short = value.Length
                Dim strInpDecimal As String()

                If value.Contains(".") Then
                    strInpDecimal = value.Split(".")
                    shrtInpNonDecimalLen = strInpDecimal(0).Trim.Length
                    shrtInpDecimalLen = strInpDecimal(1).Trim.Length
                End If
                If shrtDeftDecimal = 0 AndAlso value.Contains(".") Then boolValid = False
                If shrtInpDecimalLen > shrtDeftDecimal Then boolValid = False
                If shrtInpNonDecimalLen >= 15 Then boolValid = False
            End If

            If Not boolValid Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000350"), ValidationArgs.GetFriendlyPropertyName(e)) '--
            End If

            Return boolValid

        Catch ex As Exception
            MessageBox.Show("errror")
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate exchange method. 
    ''' When Remittance Currency and Settlement Account currency are the same, Exchange 
    ''' Method must be blank.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid exchange method, False = invalid exchange method</returns>
    ''' <remarks></remarks>
    Public Function E04000360(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Not IsNothingOrEmptyString(value)) AndAlso _
                (Not IsNothingOrEmptyString(Me.Currency)) AndAlso _
                (Not IsNothingOrEmptyString(Me.SettlementAccCurrency)) AndAlso _
                (Me.Currency = Me.SettlementAccCurrency) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000360"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function

    ''' <summary>
    ''' Validate exchange method. 
    ''' When Remittance Currency and Settlement Account currency are different, Exchange 
    ''' Method must be either Spot or Cont.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid exchange method, False = invalid exchange method</returns>
    ''' <remarks></remarks>
    Public Function E04000370(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (IsNothingOrEmptyString(value)) AndAlso _
                (Not IsNothingOrEmptyString(Me.Currency)) AndAlso _
                (Not IsNothingOrEmptyString(Me.SettlementAccCurrency)) AndAlso _
                (Me.Currency <> Me.SettlementAccCurrency) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000370"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function

    ''' <summary>
    ''' Validate contract no
    ''' Contract no is a mandatory field when Cont. is selected for Exchange Method.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid contract no, False = invalid contract no</returns>
    ''' <remarks></remarks>
    Public Function E04000380(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (IsNothingOrEmptyString(Me.ExchangeMethod)) AndAlso (Not IsNothingOrEmptyString(value)) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000380"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function

    ''' <summary>
    ''' Validate contract no
    ''' Contract no must be blank when Cont. is not selected for Exchange Method.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid contract no, False = invalid contract no</returns>
    ''' <remarks></remarks>
    Public Function E04000390(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Me.ExchangeMethod.ToUpper = "SPOT") AndAlso (Not IsNothingOrEmptyString(value)) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000390"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function

    ''' <summary>
    ''' Validate contract no
    ''' Contract no is a mandatory field when Cont. is selected for exchange method
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid contract no, False = invalid contract no</returns>
    ''' <remarks></remarks>
    Public Function E04000400(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Me.ExchangeMethod.ToUpper = "CONT") AndAlso (IsNothingOrEmptyString(value)) Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000400"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function

    ''' <summary>
    ''' Validate intermediary Bank and Address
    ''' Intermediary Bank and Address is a mandatory field when Intermediary Bank 
    ''' option A or C is selected
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid, False = invalid </returns>
    ''' <remarks></remarks>
    Public Function E04000410(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Me.IntermediaryBankOptionA Or Me.IntermediaryBankOptionC) AndAlso IsNothingOrEmptyString(value) Then
                e.Description = _
                        String.Format(MsgReader.GetString("E04000410"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate intermediary swift code
    ''' Intermediary SWIFT BIC code must be 8 or 11 characters. 
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid, False = invalid </returns>
    ''' <remarks></remarks>
    Public Function E04000420(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim boolValid As Boolean = True
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.IntermediaryBankOptionA AndAlso Not IsNothingOrEmptyString(value) Then
                If value.Length <> 8 And value.Length <> 11 Then
                    boolValid = False
                End If
            End If

            If Not boolValid Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000420"), ValidationArgs.GetFriendlyPropertyName(e)) '--
            End If

            Return boolValid

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate beneficiary bank and address
    ''' Beneficiary Bank and Address is a mandatory field when Beneficiary Bank 
    ''' option A or C is selected
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid, False = invalid </returns>
    ''' <remarks></remarks>
    Public Function E04000430(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Me.BeneficiaryBankOptionA Or Me.BeneficiaryBankOptionC) AndAlso IsNothingOrEmptyString(value) Then
                e.Description = _
                        String.Format(MsgReader.GetString("E04000430"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate beneficiary swift code
    ''' Beneficiary SWIFT BIC code must be 8 or 11 characters. 
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid, False = invalid</returns>
    ''' <remarks></remarks>
    Public Function E04000440(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim boolValid As Boolean = True
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.BeneficiaryBankOptionA AndAlso Not IsNothingOrEmptyString(value) Then
                If value.Length <> 8 And value.Length <> 11 Then
                    boolValid = False
                End If
            End If

            If Not boolValid Then
                e.Description = _
                    String.Format(MsgReader.GetString("E04000440"), ValidationArgs.GetFriendlyPropertyName(e)) '--
            End If

            Return boolValid

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate charges account
    ''' Charges account no must be blank when Beneficiary(BEN) is selected for Bank Charges.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns>Flag: True = valid, False = invalid</returns>
    ''' <remarks></remarks>
    Public Function E04000470(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Not IsNothingOrEmptyString(Me.BankCharges)) AndAlso _
                (Me.BankCharges.ToUpper() = "BEN") AndAlso _
                (Not IsNothingOrEmptyString(value)) Then
                e.Description = _
                        String.Format(MsgReader.GetString("E04000470"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

    ''' <summary>
    ''' Validate charges account
    ''' Charges account no must not be the same as Settlement Account No when Bank Charges is OUR or SHA.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function E04000480(ByVal target As Object, ByVal e As ValidationArgs) As Boolean '--

        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If (Not IsNothingOrEmptyString(Me.BankCharges)) AndAlso _
                (Me.BankCharges.ToUpper() <> "BEN") AndAlso _
                (Not IsNothingOrEmptyString(Me.SettlementAccNo)) AndAlso _
                (Not IsNothingOrEmptyString(Me.ChargesAcc)) AndAlso _
                (Me.SettlementAccNo = Me.ChargesAcc) Then
                e.Description = _
                        String.Format(MsgReader.GetString("E04000480"), ValidationArgs.GetFriendlyPropertyName(e)) '--
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function

End Class
