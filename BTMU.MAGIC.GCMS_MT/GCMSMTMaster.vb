Imports BTMU.MAGIC.Common
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates transaction master data
''' </summary>
''' <remarks></remarks>
Public Class GCMSMTMaster

#Region "Variables"

    'Constants
    Private Const MAX_NO_LEN = 34
    Private Const MAX_NAME_LEN = 35
    Private Const MAX_INTERMEDIARY_LEN = 140
    Private Const MAX_BENEFICIARY_LEN = 140
    Private Const MAX_BENEFICIARY_MSG_LEN = 140
    Private Const MAX_BENEFICIARY_INFO_LEN = 105

    Private Const MAX_BENE_CUST_TEMPLATE_CNT = 11
    Private Const MAX_BENE_BANK_TEMPLATE_CNT = 8
    Private Const MAX_TEMPLATE_CNT = 47
    Private Const MAX_CURR_TEMPLATE_CNT = 1
    Private Const MAX_SETT_TEMPLATE_CNT = 12
    Private Const MAX_SAVED_TEMPLATE_CNT = 19

    'Private variables
    Private GCMSMTMasterDS As DataSet
    Private CurrencyDT As DataTable
    Private SettlementDT As DataTable
    Private BeneficiaryCustDT As DataTable
    Private BeneficiaryBankDT As DataTable
    Private TransactionTemplateDT As DataTable

    Private BankChargesDT As DataTable
    Private Currency0DecimalDT As DataTable
    Private Currency3DecimalDT As DataTable

    Private SectorSelectionDT As New DataTable
    Private ExchangeMethodDT As New DataTable

#End Region

#Region "Properties"
    Public ReadOnly Property PMaxNoLength() As Integer
        Get
            Return MAX_NO_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxNameLength() As Integer
        Get
            Return MAX_NAME_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxIntermediaryLength() As Integer
        Get
            Return MAX_INTERMEDIARY_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxBeneficiaryLength() As Integer
        Get
            Return MAX_BENEFICIARY_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxBeneficiaryMsgLength() As Integer
        Get
            Return MAX_BENEFICIARY_MSG_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxBeneficiaryInfoLength() As Integer
        Get
            Return MAX_BENEFICIARY_INFO_LEN
        End Get
    End Property

    Public ReadOnly Property PMaxBeneficiaryCustCnt() As Integer
        Get
            Return MAX_BENE_CUST_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PMaxBeneficiaryBankCnt() As Integer
        Get
            Return MAX_BENE_BANK_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PMaxTemplateCnt() As Integer
        Get
            Return MAX_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PMaxSettlementCnt() As Integer
        Get
            Return MAX_SETT_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PMaxSavedTemplateCnt() As Integer
        Get
            Return MAX_SAVED_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PMaxCurrencyCnt()
        Get
            Return MAX_CURR_TEMPLATE_CNT
        End Get
    End Property

    Public ReadOnly Property PCurrencyDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("CurrencyMaster")
        End Get
    End Property

    Public ReadOnly Property PSettlementDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("SettlementMaster")
        End Get
    End Property

    Public ReadOnly Property PBeneficiaryCustDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("BeneficiaryCustMaster")
        End Get
    End Property

    Public ReadOnly Property PBeneficiaryBankDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("BeneficiaryBankMaster")
        End Get
    End Property

    Public ReadOnly Property PTransactionTemplateDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("TransactionTemplateMaster")
        End Get
    End Property

    Public ReadOnly Property PTemplateID() As DataColumn
        Get
            Return GCMSMTMasterDS.Tables("TransactionTemplateMaster").Columns("TemplateID")
        End Get
    End Property

    Public ReadOnly Property PBeneBankAndBranchName() As DataColumn
        Get
            Return GCMSMTMasterDS.Tables("BeneficiaryBankMaster").Columns("BankAndBranchName")
        End Get
    End Property

    Public ReadOnly Property PBeneAccountNoAndName() As DataColumn
        Get
            Return GCMSMTMasterDS.Tables("BeneficiaryCustMaster").Columns("AccountNoAndName")
        End Get
    End Property

    Public ReadOnly Property PSettlementAccAndName() As DataColumn
        Get
            Return GCMSMTMasterDS.Tables("SettlemenMaster").Columns("AccountNoAndName")
        End Get
    End Property

    Public ReadOnly Property PBankChargesDT() As DataTable
        Get
            Return GCMSMTMasterDS.Tables("BankCharges")
        End Get
    End Property

    Public ReadOnly Property PExchangeMethodDT() As DataTable
        Get
            Return ExchangeMethodDT
        End Get
    End Property

    Public ReadOnly Property PSectorSelectionDT() As DataTable
        Get
            Return SectorSelectionDT
        End Get
    End Property

#End Region

    'Class constructor
    Public Sub New(ByVal ChargesType As String)

        'Initialise dataset
        GCMSMTMasterDS = New DataSet("GCMSMTMaster")

        'Initialise datatable
        CurrencyDT = New DataTable("CurrencyMaster")
        SettlementDT = New DataTable("SettlementMaster")
        BeneficiaryCustDT = New DataTable("BeneficiaryCustMaster")
        BeneficiaryBankDT = New DataTable("BeneficiaryBankMaster")
        TransactionTemplateDT = New DataTable("TransactionTemplateMaster")
        BankChargesDT = New DataTable("BankCharges")

        'Add datatable to dataset
        GCMSMTMasterDS.Tables.Add(CurrencyDT)
        GCMSMTMasterDS.Tables.Add(SettlementDT)
        GCMSMTMasterDS.Tables.Add(BeneficiaryCustDT)
        GCMSMTMasterDS.Tables.Add(BeneficiaryBankDT)
        GCMSMTMasterDS.Tables.Add(TransactionTemplateDT)
        GCMSMTMasterDS.Tables.Add(BankChargesDT)

        'Add columns to datatables
        'Currency Master
        CurrencyDT.Columns.Add("CurrencyCode", GetType(String))

        'Settlement Master
        SettlementDT.Columns.Add("AccountNoAndName", GetType(String))
        SettlementDT.Columns.Add("AccountNo", GetType(String))
        SettlementDT.Columns.Add("AccountName1", GetType(String))
        SettlementDT.Columns.Add("AccountName2", GetType(String))
        SettlementDT.Columns.Add("AccountName3", GetType(String))
        SettlementDT.Columns.Add("AccountName4", GetType(String))
        SettlementDT.Columns.Add("CurrencyCode", GetType(String))
        SettlementDT.Columns.Add("BankCode", GetType(String))
        SettlementDT.Columns.Add("Bank", GetType(String))
        SettlementDT.Columns.Add("Branch", GetType(String))
        SettlementDT.Columns.Add("BankAddress1", GetType(String))
        SettlementDT.Columns.Add("BankAddress2", GetType(String))

        'Beneficiary Customer
        BeneficiaryCustDT.Columns.Add("AccountNoAndName", GetType(String))
        BeneficiaryCustDT.Columns.Add("AccountNo", GetType(String))
        BeneficiaryCustDT.Columns.Add("AccountName1", GetType(String))
        BeneficiaryCustDT.Columns.Add("AccountName2", GetType(String))
        BeneficiaryCustDT.Columns.Add("AccountName3", GetType(String))
        BeneficiaryCustDT.Columns.Add("AccountName4", GetType(String))
        BeneficiaryCustDT.Columns.Add("BankName", GetType(String))
        BeneficiaryCustDT.Columns.Add("BranchName", GetType(String))
        BeneficiaryCustDT.Columns.Add("BankAddressLine1", GetType(String))
        BeneficiaryCustDT.Columns.Add("BankAddressLine2", GetType(String))
        BeneficiaryCustDT.Columns.Add("BankMasterCode", GetType(String))

        'Beneficiary Bank
        BeneficiaryBankDT.Columns.Add("BankAndBranchName", GetType(String))
        BeneficiaryBankDT.Columns.Add("BankName", GetType(String))
        BeneficiaryBankDT.Columns.Add("BranchName", GetType(String))
        BeneficiaryBankDT.Columns.Add("BankAddress1", GetType(String))
        BeneficiaryBankDT.Columns.Add("BankAddress2", GetType(String))
        BeneficiaryBankDT.Columns.Add("BankMasterCode", GetType(String))
        BeneficiaryBankDT.Columns.Add("SwiftBIC", GetType(String))
        BeneficiaryBankDT.Columns.Add("NCC", GetType(String))

        'Transaction saved template
        TransactionTemplateDT.Columns.Add("TemplateID", GetType(String))
        TransactionTemplateDT.Columns.Add("SettlementAccNo", GetType(String))
        TransactionTemplateDT.Columns.Add("SettlementAccName1", GetType(String))
        TransactionTemplateDT.Columns.Add("SettlementAccName2", GetType(String))
        TransactionTemplateDT.Columns.Add("SettlementAccName3", GetType(String))
        TransactionTemplateDT.Columns.Add("SettlementAccName4", GetType(String))
        TransactionTemplateDT.Columns.Add("CustomerRef", GetType(String))
        TransactionTemplateDT.Columns.Add("SectorSelection", GetType(String))
        TransactionTemplateDT.Columns.Add("Currency", GetType(String))
        TransactionTemplateDT.Columns.Add("RemitAmount", GetType(String))
        TransactionTemplateDT.Columns.Add("ExchangeMethod", GetType(String))
        TransactionTemplateDT.Columns.Add("ContractNo", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBank", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBranch", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankAddress1", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankAddress2", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankMasterCode", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankOption", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankSWIFTBIC", GetType(String))
        TransactionTemplateDT.Columns.Add("IntermediaryBankNCC", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBank", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBranch", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankAddress1", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankAddress2", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankMasterCode", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankOption", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankSWIFTBIC", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryBankNCC", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryAccNo", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryName1", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryName2", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryName3", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryName4", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryMsg1", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryMsg2", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryMsg3", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryMsg4", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryRemitPurpose", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryRemitInfo1", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryRemitInfo2", GetType(String))
        TransactionTemplateDT.Columns.Add("BeneficiaryRemitInfo3", GetType(String))
        TransactionTemplateDT.Columns.Add("BankCharges", GetType(String))
        TransactionTemplateDT.Columns.Add("ChargesAcc", GetType(String))
        TransactionTemplateDT.Columns.Add("ChargesAccName1", GetType(String))
        TransactionTemplateDT.Columns.Add("ChargesAccName2", GetType(String))
        TransactionTemplateDT.Columns.Add("ChargesAccName3", GetType(String))
        TransactionTemplateDT.Columns.Add("ChargesAccName4", GetType(String))

        'Bank Charges
        BankChargesDT.Columns.Add("BankChargesDesc", GetType(String))
        BankChargesDT.Columns.Add("BankChargesCode", GetType(String))
        BankChargesDT.Rows.Add(BankChargesDT.NewRow())
        BankChargesDT.Rows(0).Item("BankChargesDesc") = ""
        BankChargesDT.Rows(0).Item("BankChargesCode") = ""

        '0 = Duplex (OUR, SHA)
        '1 = Triplex (BEN, OUR, SHA)
        If ChargesType = "0" Then
            BankChargesDT.Rows.Add(BankChargesDT.NewRow())
            BankChargesDT.Rows(1).Item("BankChargesDesc") = "Applicant(OUR)"
            BankChargesDT.Rows(1).Item("BankChargesCode") = "OUR"
            BankChargesDT.Rows.Add(BankChargesDT.NewRow())
            BankChargesDT.Rows(2).Item("BankChargesDesc") = "Share(SHA)"
            BankChargesDT.Rows(2).Item("BankChargesCode") = "SHA"
        Else
            BankChargesDT.Rows.Add(BankChargesDT.NewRow())
            BankChargesDT.Rows(1).Item("BankChargesDesc") = "Beneficiary(BEN)"
            BankChargesDT.Rows(1).Item("BankChargesCode") = "BEN"
            BankChargesDT.Rows.Add(BankChargesDT.NewRow())
            BankChargesDT.Rows(2).Item("BankChargesDesc") = "Applicant(OUR)"
            BankChargesDT.Rows(2).Item("BankChargesCode") = "OUR"
            BankChargesDT.Rows.Add(BankChargesDT.NewRow())
            BankChargesDT.Rows(3).Item("BankChargesDesc") = "Share(SHA)"
            BankChargesDT.Rows(3).Item("BankChargesCode") = "SHA"
        End If

        'Sector Selection
        SectorSelectionDT.Columns.Add("SectorSelection", GetType(String))
        SectorSelectionDT.Rows.Add(SectorSelectionDT.NewRow())
        SectorSelectionDT.Rows(0).Item("SectorSelection") = ""
        SectorSelectionDT.Rows.Add(SectorSelectionDT.NewRow())
        SectorSelectionDT.Rows(1).Item("SectorSelection") = "Book Transfer"
        SectorSelectionDT.Rows.Add(SectorSelectionDT.NewRow())
        SectorSelectionDT.Rows(2).Item("SectorSelection") = "Domestic"
        SectorSelectionDT.Rows.Add(SectorSelectionDT.NewRow())
        SectorSelectionDT.Rows(3).Item("SectorSelection") = "International"

        'Exchange Method
        ExchangeMethodDT.Columns.Add("ExchangeMethod", GetType(String))
        ExchangeMethodDT.Rows.Add(ExchangeMethodDT.NewRow())
        ExchangeMethodDT.Rows(0).Item("ExchangeMethod") = ""
        ExchangeMethodDT.Rows.Add(ExchangeMethodDT.NewRow())
        ExchangeMethodDT.Rows(1).Item("ExchangeMethod") = "Spot"
        ExchangeMethodDT.Rows.Add(ExchangeMethodDT.NewRow())
        ExchangeMethodDT.Rows(2).Item("ExchangeMethod") = "Cont"
    End Sub

    ''' <summary>
    ''' Insert each line of currency code from master file into currency datatable
    ''' </summary>
    ''' <param name="currencyLine">Currency line from master file</param>
    ''' <param name="msgReader">Object to access resource for message code</param>
    ''' <param name="boolPopulateOnce">Flag: True = first line, False = the rest of the lines</param>
    ''' <remarks></remarks>
    Public Sub PopuplateCurrencyRow(ByVal currencyLine As String, _
                                    ByVal msgReader As Global.System.Resources.ResourceManager, _
                                    ByVal boolPopulateOnce As Boolean)

        Dim currencyRow As DataRow
        Dim currencyArr() As String

        Try
            currencyRow = CurrencyDT.NewRow()

            If boolPopulateOnce Then
                currencyRow.Item("CurrencyCode") = currencyLine
                CurrencyDT.Rows.Add(currencyRow)
            Else
                currencyArr = Common.HelperModule.Split(currencyLine, ",", """", False)
                If currencyArr IsNot Nothing Then
                    If currencyArr.Length <> PMaxCurrencyCnt Then
                        Throw New MagicException(msgReader.GetString("E04000130")) '--
                    End If

                    If Not IsNothingOrEmptyString(currencyLine) Then
                        If currencyArr.Length > 0 Then
                            currencyRow.Item("CurrencyCode") = currencyArr(0)
                            CurrencyDT.Rows.Add(currencyRow)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Insert each line of settlement data from master file into settlement datatable
    ''' </summary>
    ''' <param name="settlementLine">Settlement line from master file</param>
    ''' <param name="msgReader">Object to access resource for message code</param>
    ''' <remarks></remarks>
    Public Sub PopuplateSettlementRow(ByVal settlementLine As String, ByVal msgReader As Global.System.Resources.ResourceManager)

        Dim settlementRow As DataRow
        Dim settlementArr() As String
        Dim index As Integer

        Try
            settlementRow = SettlementDT.NewRow()
            settlementArr = Common.HelperModule.Split(settlementLine, ",", """", False)

            If settlementArr IsNot Nothing Then
                If settlementArr.Length <> PMaxSettlementCnt Then
                    Throw New MagicException(msgReader.GetString("E04000100")) '--
                End If

                For index = 0 To settlementArr.Length - 1
                    If settlementArr(index) IsNot Nothing Then
                        settlementRow(index) = settlementArr(index).Trim
                    Else
                        settlementRow(index) = ""
                    End If
                Next
                SettlementDT.Rows.Add(settlementRow)
            End If

        Catch MagicEx4 As MagicException
            Throw New MagicException(MagicEx4.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Insert each line of beneficiary Bank data from master file into beneficiary 
    ''' bank datatable
    ''' </summary>
    ''' <param name="beneficiaryBankLine">Beneficiary Bank line from master file</param>
    ''' <param name="msgReader">Object to access resource for message code</param>
    ''' <remarks></remarks>
    Public Sub PopuplateBeneficiaryBankRow(ByVal beneficiaryBankLine As String, ByVal msgReader As Global.System.Resources.ResourceManager)

        Dim beneficiaryBankRow As DataRow
        Dim beneficiaryBankArr() As String
        Dim index As Integer
        Dim reg As Regex = New Regex("^[0-9]*$")

        Try
            beneficiaryBankRow = BeneficiaryBankDT.NewRow()
            beneficiaryBankArr = Common.HelperModule.Split(beneficiaryBankLine, ",", """", False)

            If beneficiaryBankArr IsNot Nothing Then
                If beneficiaryBankArr.Length <> PMaxBeneficiaryBankCnt Then
                    Throw New MagicException(msgReader.GetString("E04000160")) '--
                End If

                For index = 0 To beneficiaryBankArr.Length - 1
                    If beneficiaryBankArr(index) IsNot Nothing Then
                        beneficiaryBankRow(index) = beneficiaryBankArr(index).Trim
                    Else
                        beneficiaryBankRow(index) = ""
                    End If
                Next

                'Check bank master code, must be digits only
                If Not IsNothingOrEmptyString(beneficiaryBankRow(5).ToString()) AndAlso Not reg.IsMatch(beneficiaryBankRow(5).ToString()) Then
                    Throw New MagicException(msgReader.GetString("E04000160")) '--
                End If
                BeneficiaryBankDT.Rows.Add(beneficiaryBankRow)
            End If

        Catch MagicEx4 As MagicException
            Throw New MagicException(MagicEx4.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Insert each line of beneficiary customer data from master file into beneficiary 
    ''' customer datatable
    ''' </summary>
    ''' <param name="beneficiaryCustLine">Beneficiary Customer line from master file</param>
    ''' <param name="msgReader">Object to access resource for message code</param>
    ''' <remarks></remarks>
    Public Sub PopuplateBeneficiaryCustRow(ByVal beneficiaryCustLine As String, ByVal msgReader As Global.System.Resources.ResourceManager)
        Dim beneficiaryCustRow As DataRow
        Dim beneficiaryCustArr() As String
        Dim index As Integer

        Try
            beneficiaryCustRow = BeneficiaryCustDT.NewRow()
            beneficiaryCustArr = Common.HelperModule.Split(beneficiaryCustLine, ",", """", False)

            If beneficiaryCustArr IsNot Nothing Then
                If beneficiaryCustArr.Length <> PMaxBeneficiaryCustCnt Then
                    Throw New MagicException(msgReader.GetString("E04000180")) '--
                End If

                For index = 0 To beneficiaryCustArr.Length - 1
                    If beneficiaryCustArr(index) IsNot Nothing Then
                        beneficiaryCustRow(index) = beneficiaryCustArr(index).Trim
                    Else
                        beneficiaryCustRow(index) = ""
                    End If
                Next

                BeneficiaryCustDT.Rows.Add(beneficiaryCustRow)
            End If

        Catch MagicEx4 As MagicException
            Throw New MagicException(MagicEx4.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Insert each line of template data from master file into template datatable
    ''' </summary>
    ''' <param name="templateLine">Template master line from master file</param>
    ''' <param name="msgReader">Object to access resource for message code</param>
    ''' <remarks></remarks>
    Public Sub PopuplateTemplateRow(ByVal templateLine As String, ByVal msgReader As Global.System.Resources.ResourceManager)
        Dim templateRow As DataRow
        Dim templateArr() As String
        Dim index As Integer

        Try
            templateRow = TransactionTemplateDT.NewRow()
            templateArr = Common.HelperModule.Split(templateLine, ",", """", False)

            If templateArr IsNot Nothing Then
                If templateArr.Length <> PMaxTemplateCnt Then
                    Throw New MagicException(msgReader.GetString("E04000080")) '--
                End If

                For index = 0 To templateArr.Length - 1
                    If templateArr(index) IsNot Nothing Then
                        templateRow(index) = templateArr(index).Trim
                    Else
                        templateRow(index) = ""
                    End If
                Next

                TransactionTemplateDT.Rows.Add(templateRow)
            End If

        Catch MagicEx4 As MagicException
            Throw New MagicException(MagicEx4.Message)
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Find settlement account name in datatable
    ''' </summary>
    ''' <param name="settlementAccNo">Settlement account no</param>
    ''' <returns>Settlement account name</returns>
    ''' <remarks></remarks>
    Public Function GetSettlementAccName1(ByVal settlementAccNo As String) As String

        Dim dr As DataRow = Nothing

        Try
            GetSettlementAccName1 = ""

            If settlementAccNo IsNot Nothing Then
                For Each dr In GCMSMTMasterDS.Tables("SettlementMaster").Rows
                    If dr("AccountNo").ToString.Trim = settlementAccNo.Trim Then
                        GetSettlementAccName1 = dr("AccountName1").ToString.Trim
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find settlement detail in datatable
    ''' </summary>
    ''' <param name="settlementAcc">Search keyword</param>
    ''' <param name="colName">Search type. Search by AccountNo or AccountNoAndName</param>
    ''' <returns>Settlement data row</returns>
    ''' <remarks></remarks>
    Public Function GetSettlementRow(ByVal settlementAcc As String, ByVal colName As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetSettlementRow = drTemp

            If settlementAcc IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("SettlementMaster").Rows
                    If colName = "AccountNo" Then
                        If drTemp(colName).ToString.Trim = settlementAcc.Trim Then
                            GetSettlementRow = drTemp
                            Exit For
                        End If
                    ElseIf colName = "AccountNoAndName" Then
                        If drTemp(colName).ToString.Trim = settlementAcc.Trim Then
                            GetSettlementRow = drTemp
                            Exit For
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find beneficiary customer detail in datatable
    ''' </summary>
    ''' <param name="beneficiaryAccNoAndName">Search keyword</param>
    ''' <returns>Beneficiary customer data row</returns>
    ''' <remarks></remarks>
    Public Function GetBeneficiaryCustRow(ByVal beneficiaryAccNoAndName As String) As DataRow
        Dim drTemp As DataRow = Nothing

        Try
            GetBeneficiaryCustRow = drTemp

            If beneficiaryAccNoAndName IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("BeneficiaryCustMaster").Rows
                    If drTemp("AccountNoAndName").ToString.Trim = beneficiaryAccNoAndName.Trim Then
                        GetBeneficiaryCustRow = drTemp
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find beneficiary bank detail in datatable
    ''' </summary>
    ''' <param name="beneficiaryBankAndBranch">Search keyword</param>
    ''' <returns>Beneficiary bank data row</returns>
    ''' <remarks></remarks>
    Public Function GetBeneficiaryBankRow(ByVal beneficiaryBankAndBranch As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetBeneficiaryBankRow = drTemp

            If beneficiaryBankAndBranch IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("BeneficiaryBankMaster").Rows
                    If drTemp("BankAndBranchName").ToString.Trim = beneficiaryBankAndBranch.Trim Then
                        GetBeneficiaryBankRow = drTemp
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find template master detail in datable
    ''' </summary>
    ''' <param name="templateID">Search keyword</param>
    ''' <returns>Template master data row</returns>
    ''' <remarks></remarks>
    Public Function GetTemplateRow(ByVal templateID As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetTemplateRow = drTemp

            If templateID IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("TransactionTemplateMaster").Rows
                    If drTemp("TemplateID").ToString.Trim = templateID.Trim Then
                        GetTemplateRow = drTemp
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find beneficiary bank detail in datable
    ''' </summary>
    ''' <param name="strBankName">Search keyword 1</param>
    ''' <param name="strBranchName">Search keyword 2</param>
    ''' <returns>Beneficiary bank data row</returns>
    ''' <remarks></remarks>
    Public Function GetBankAndBranchRow(ByVal strBankName As String, ByVal strBranchName As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetBankAndBranchRow = drTemp

            If strBankName IsNot Nothing And strBranchName IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("BeneficiaryBankMaster").Rows
                    If drTemp("BankName").ToString.Trim = strBankName.Trim And _
                        drTemp("BranchName").ToString.Trim = strBranchName.Trim Then
                        GetBankAndBranchRow = drTemp
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find beneficiary customer detail in datatable
    ''' </summary>
    ''' <param name="strAccNo">Search keyword 1</param>
    ''' <param name="strAccName">Search keyword 2</param>
    ''' <returns>Settlement account data row</returns>
    ''' <remarks></remarks>
    Public Function GetAccNoAndNameRow(ByVal strAccNo As String, ByVal strAccName As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetAccNoAndNameRow = drTemp

            If strAccNo IsNot Nothing And strAccName IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("BeneficiaryCustMaster").Rows
                    If drTemp("AccountNo").ToString.Trim = strAccNo.Trim And drTemp("AccountName1").ToString.Trim = strAccName.Trim Then
                        GetAccNoAndNameRow = drTemp
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find bank charges detail in datatable
    ''' </summary>
    ''' <param name="strBankCharges">Search keyword</param>
    ''' <returns>Bank charges data row</returns>
    ''' <remarks></remarks>
    Public Function GetBankChargesDTRow(ByVal strBankCharges As String) As DataRow

        Dim drTemp As DataRow = Nothing

        Try
            GetBankChargesDTRow = drTemp

            If strBankCharges IsNot Nothing Then
                For Each drTemp In GCMSMTMasterDS.Tables("BankCharges").Rows
                    If drTemp("BankChargesCode").ToString.Trim = strBankCharges.Trim Then
                        GetBankChargesDTRow = drTemp
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find a value in a given string based on the given search type.
    ''' </summary>
    ''' <param name="detailLine">String to be searched</param>
    ''' <param name="type">Search type</param>
    ''' <returns>Result of the searching value</returns>
    ''' <remarks></remarks>
    Public Function GetDetailLine(ByVal detailLine As String, ByVal type As Short) As String

        GetDetailLine = ""

        Try
            If type = 1 Then 'Intermediary Bank
                GetDetailLine = detailLine.Substring(0, MAX_NAME_LEN)
            ElseIf type = 2 Then 'Intermediary Branch
                GetDetailLine = detailLine.Substring((MAX_NAME_LEN * (type - 1)), MAX_NAME_LEN)
            ElseIf type = 3 Then 'Intermediary Bank Address 1
                GetDetailLine = detailLine.Substring((MAX_NAME_LEN * (type - 1)), MAX_NAME_LEN)
            ElseIf type = 4 Then 'Intermediary Bank Address 2
                GetDetailLine = detailLine.Substring((MAX_NAME_LEN * (type - 1)), MAX_NAME_LEN)
            End If
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Find index of the selected item in a combobox 
    ''' </summary>
    ''' <param name="strKey">Search keyword</param>
    ''' <param name="cmbxValues">Comboxbox to search</param>
    ''' <returns>An index of the selected item</returns>
    ''' <remarks></remarks>
    Public Function GetSelectedIndexInArray(ByVal strKey As String, ByVal cmbxValues As ComboBox) As Integer

        GetSelectedIndexInArray = 0

        Try
            For counter As Integer = 0 To cmbxValues.Items.Count - 1
                If cmbxValues.Items(counter).Contains(strKey.Trim) Then
                    GetSelectedIndexInArray = counter
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

    End Function

End Class
