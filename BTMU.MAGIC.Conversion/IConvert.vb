Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.Common

Public Interface IConvert
    Property CommonTemplateName() As String
    Property MasterTemplateName() As String
    Property SourceFileName() As String
    Property WorksheetName() As String
    Property ValueDate() As String
    Property UseValueDate() As Boolean
    Property TransactionEndRow() As Integer
    Property RemoveRowsFromEnd() As Integer
    Property EliminateCharacter() As String
    Property SaveLocation() As String
    Property OutputFileName() As String
    Property IntermediaryFileName() As String


    Sub ValidateBeforePreview()

End Interface
