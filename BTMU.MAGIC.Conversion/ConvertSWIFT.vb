
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate
Imports System.Globalization

Public Class ConvertSWIFT
    Implements IConvert
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

#Region " Private Variables "
    Private _SWIFTTemplateName As String
    Private _masterTemplateName As String
    Private _SWIFTFileName As String
    Private _worksheetName As String
    Private _valueDate As String
    Private _useValueDate As Boolean
    Private _transactionEndRow As Integer
    Private _removeRowsFromEnd As Integer
    Private _eliminateCharacter As String
    Private _saveLocation As String
    Private _outputFileName As String
    Private _intermediaryFileName As String
#End Region

#Region " Public Properties "
    Public Property SWIFTTemplateName() As String Implements IConvert.CommonTemplateName
        Get
            Return _SWIFTTemplateName
        End Get
        Set(ByVal value As String)
            _SWIFTTemplateName = value
        End Set
    End Property
    Public Property MasterTemplateName() As String Implements IConvert.MasterTemplateName
        Get
            Return _masterTemplateName
        End Get
        Set(ByVal value As String)
            _masterTemplateName = value
        End Set
    End Property
    Public Property SWIFTFileName() As String Implements IConvert.SourceFileName
        Get
            Return _SWIFTFileName
        End Get
        Set(ByVal value As String)
            _SWIFTFileName = value
        End Set
    End Property
    Public Property ValueDate() As String Implements IConvert.ValueDate
        Get
            Return _valueDate
        End Get
        Set(ByVal value As String)
            _valueDate = value
        End Set
    End Property
    Public Property UseValueDate() As Boolean Implements IConvert.UseValueDate
        Get
            Return _useValueDate
        End Get
        Set(ByVal value As Boolean)
            _useValueDate = value
        End Set
    End Property
    Public Property SaveLocation() As String Implements IConvert.SaveLocation
        Get
            Return _saveLocation
        End Get
        Set(ByVal value As String)
            _saveLocation = value
        End Set
    End Property
    Public Property TransactionEndRow() As Integer Implements IConvert.TransactionEndRow
        Get
            Return _transactionEndRow
        End Get
        Set(ByVal value As Integer)
            _transactionEndRow = value
        End Set
    End Property
    Public Property RemoveRowsFromEnd() As Integer Implements IConvert.RemoveRowsFromEnd
        Get
            Return _removeRowsFromEnd
        End Get
        Set(ByVal value As Integer)
            _removeRowsFromEnd = value
        End Set
    End Property
    Public Property EliminateCharacter() As String Implements IConvert.EliminateCharacter
        Get
            Return _eliminateCharacter
        End Get
        Set(ByVal value As String)
            _eliminateCharacter = value
        End Set
    End Property
    Public Property WorksheetName() As String Implements IConvert.WorksheetName
        Get
            Return _worksheetName
        End Get
        Set(ByVal value As String)
            _worksheetName = value
        End Set
    End Property
    Public Property OutputFileName() As String Implements IConvert.OutputFileName
        Get
            Return _outputFileName
        End Get
        Set(ByVal value As String)
            _outputFileName = value
        End Set
    End Property
    Public Property IntermediaryFileName() As String Implements IConvert.IntermediaryFileName
        Get
            Return _IntermediaryFileName
        End Get
        Set(ByVal value As String)
            _IntermediaryFileName = value
        End Set
    End Property

#End Region

    Public Sub ValidateBeforePreview() Implements IConvert.ValidateBeforePreview
        ' Validation - Common Template Name : Cannot be empty
        If IsNothingOrEmptyString(SWIFTTemplateName) Then
            Throw New Exception(String.Format(MsgReader.GetString("E00000050"), "SWIFT Template Name"))
        End If
        ' Validation - Source File Name : Cannot be empty
        If IsNothingOrEmptyString(SWIFTFileName) Then
            Throw New Exception(String.Format(MsgReader.GetString("E00000050"), "SWIFT File Name"))
        End If
        ' Validation - Value Date : Should not be less than today
        Dim provider As CultureInfo = CultureInfo.InvariantCulture
        If UseValueDate Then
            If IsDate(ValueDate) Then
                If Date.Compare(Convert.ToDateTime(ValueDate), Date.Today) > 1 Then
                    Throw New Exception(String.Format(MsgReader.GetString("E03000010"), "Value Date"))
                End If
            Else
                Throw New Exception(String.Format(MsgReader.GetString("E03000010"), "Value Date"))
            End If
        End If
        ' Validation - Save Location : Cannot be empty
        If IsNothingOrEmptyString(SaveLocation) Then
            Throw New Exception(String.Format(MsgReader.GetString("E00000050"), "Save Location"))
        End If

    End Sub
End Class
