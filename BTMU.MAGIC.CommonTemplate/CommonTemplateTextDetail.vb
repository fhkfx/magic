Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common
Imports System.Xml.XPath

''' <summary>
''' This class encapsulates a Common Transaction Template for Text file
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class CommonTemplateTextDetail
    Inherits BTMU.MAGIC.Common.BusinessBase
    Implements ICommonTemplate

    <NonSerialized()> Public Shared DataContainer As CommonTemplateTextDetail
    <NonSerialized()> Private _SourceData As DataTable
    <NonSerialized()> Private _PreviewSourceData As DataTable
    <NonSerialized()> Public _tblPaymentSrc As DataTable
    <NonSerialized()> Public _tblTaxSrc As DataTable
    <NonSerialized()> Public _tblInvoiceSrc As DataTable
    <NonSerialized()> Private _FilteredSourceData As DataView
    <NonSerialized()> Private _outputfileformat As String
    Public Sub New()
        Initialize()
        DataContainer = Me
    End Sub

#Region " Private Fields "

    Private _curIsDraft As Boolean
    Private _oldIsDraft As Boolean
    Private _oriIsDraft As Boolean
    Private _curOutputTemplateName As String
    Private _oldOutputTemplateName As String
    Private _oriOutputTemplateName As String
    Private _curCommonTemplateName As String
    Private _oldCommonTemplateName As String
    Private _oriCommonTemplateName As String
    Private _curIsEnabled As Boolean
    Private _oldIsEnabled As Boolean
    Private _oriIsEnabled As Boolean
    Private _curSourceFileName As String
    Private _oldSourceFileName As String
    Private _oriSourceFileName As String
    Private _curDefinitionFileName As String
    Private _oldDefinitionFileName As String
    Private _oriDefinitionFileName As String
    Private _curIsFixedWidthFile As Boolean
    Private _oldIsFixedWidthFile As Boolean
    Private _oriIsFixedWidthFile As Boolean
    Private _curIsCarriageReturn As Boolean
    Private _oldIsCarriageReturn As Boolean
    Private _oriIsCarriageReturn As Boolean
    Private _curSampleRowNumber As String
    Private _oldSampleRowNumber As String
    Private _oriSampleRowNumber As String
    Private _curIsDelimited As Boolean
    Private _oldIsDelimited As Boolean
    Private _oriIsDelimited As Boolean
    Private _curStartingCharacterPos As String
    Private _oldStartingCharacterPos As String
    Private _oriStartingCharacterPos As String
    Private _curLenOfStartingCharacter As String
    Private _oldLenOfStartingCharacter As String
    Private _oriLenOfStartingCharacter As String
    Private _curSampleRow As String
    Private _oldSampleRow As String
    Private _oriSampleRow As String
    Private _curDateSeparator As String
    Private _oldDateSeparator As String
    Private _oriDateSeparator As String
    Private _curDateType As String
    Private _oldDateType As String
    Private _oriDateType As String
    Private _curIsZeroInDate As Boolean
    Private _oldIsZeroInDate As Boolean
    Private _oriIsZeroInDate As Boolean
    Private _curDecimalSeparator As String
    Private _oldDecimalSeparator As String
    Private _oriDecimalSeparator As String
    Private _curThousandSeparator As String
    Private _oldThousandSeparator As String
    Private _oriThousandSeparator As String
    Private _curIsAmountInDollars As Boolean
    Private _oldIsAmountInDollars As Boolean
    Private _oriIsAmountInDollars As Boolean
    Private _curIsAmountInCents As Boolean
    Private _oldIsAmountInCents As Boolean
    Private _oriIsAmountInCents As Boolean
    Private _curEnclosureCharacter As String
    Private _oldEnclosureCharacter As String
    Private _oriEnclosureCharacter As String
    Private _curDelimiterCharacter As String
    Private _oldDelimiterCharacter As String
    Private _oriDelimiterCharacter As String
    Private _curOtherSeparator As String
    Private _oldOtherSeparator As String
    Private _oriOtherSeparator As String
    Private _curHeaderRowNumber As String
    Private _oldHeaderRowNumber As String
    Private _oriHeaderRowNumber As String
    Private _curTransactionStartRowNumber As String
    Private _oldTransactionStartRowNumber As String
    Private _oriTransactionStartRowNumber As String
    Private _curIsHeaderAField As Boolean
    Private _oldIsHeaderAField As Boolean
    Private _oriIsHeaderAField As Boolean
    Private _curDuplicateTxnRef1 As String
    Private _oldDuplicateTxnRef1 As String
    Private _oriDuplicateTxnRef1 As String
    Private _curDuplicateTxnRef2 As String
    Private _oldDuplicateTxnRef2 As String
    Private _oriDuplicateTxnRef2 As String
    Private _curDuplicateTxnRef3 As String
    Private _oldDuplicateTxnRef3 As String
    Private _oriDuplicateTxnRef3 As String
    Private _curIsAdviceRecordForEveryRow As Boolean
    Private _oldIsAdviceRecordForEveryRow As Boolean
    Private _oriIsAdviceRecordForEveryRow As Boolean
    Private _curIsAdviceRecordAfterCharacter As Boolean
    Private _oldIsAdviceRecordAfterCharacter As Boolean
    Private _oriIsAdviceRecordAfterCharacter As Boolean
    Private _curIsAdviceRecordDelimited As Boolean
    Private _oldIsAdviceRecordDelimited As Boolean
    Private _oriIsAdviceRecordDelimited As Boolean
    Private _curAdviceRecordCharPos As String
    Private _oldAdviceRecordCharPos As String
    Private _oriAdviceRecordCharPos As String
    Private _curAdviceRecordDelimiter As String
    Private _oldAdviceRecordDelimiter As String
    Private _oriAdviceRecordDelimiter As String
    Private _curFilters As New RowFilterCollection
    Private _curIsFilterConditionAND As Boolean
    Private _oldIsFilterConditionAND As Boolean
    Private _oriIsFilterConditionAND As Boolean
    Private _curIsFilterConditionOR As Boolean
    Private _oldIsFilterConditionOR As Boolean
    Private _oriIsFilterConditionOR As Boolean
    Private _curMapSourceFields As New MapSourceFieldCollection
    Private _curMapBankFields As New MapBankFieldCollection
    Private _curTranslatorSettings As New TranslatorSettingCollection
    Private _curEditableSettings As New EditableSettingCollection
    Private _curLookupSettings As New LookupSettingCollection
    Private _curCalculatedFields As New CalculatedFieldCollection
    Private _curStringManipulations As New StringManipulationCollection

    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
    Private _curPColumn As String
    Private _oldPColumn As String
    Private _oriPColumn As String
    Private _curPIndicator As String
    Private _oldPIndicator As String
    Private _oriPIndicator As String
    Private _curWColumn As String
    Private _oldWColumn As String
    Private _oriWColumn As String
    Private _curWIndicator As String
    Private _oldWIndicator As String
    Private _oriWIndicator As String
    Private _curIColumn As String
    Private _oldIColumn As String
    Private _oriIColumn As String
    Private _curIIndicator As String
    Private _oldIIndicator As String
    Private _oriIIndicator As String
#End Region

#Region " Public Property "
    ''' <summary>
    ''' Determines whether the Template is a Draft
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _curIsDraft
        End Get
        Set(ByVal value As Boolean)
            If _curIsDraft <> value Then
                _curIsDraft = value
                OnPropertyChanged("IsDraft")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get the Name of Output Template(Master Template)
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplateName() As String Implements ICommonTemplate.OutputTemplateName
        Get
            Return _curOutputTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOutputTemplateName <> value Then
                _curOutputTemplateName = value
                OnPropertyChanged("OutputTemplateName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Common Template
    ''' </summary>
    ''' <value>Name of Common Template</value>
    ''' <returns>Returns Name of Common Template</returns>
    ''' <remarks></remarks>
    Public Property CommonTemplateName() As String
        Get
            Return _curCommonTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curCommonTemplateName <> value Then
                _curCommonTemplateName = value
                OnPropertyChanged("CommonTemplateName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Template is Enabled
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _curIsEnabled
        End Get
        Set(ByVal value As Boolean)
            If _curIsEnabled <> value Then
                _curIsEnabled = value
                OnPropertyChanged("IsEnabled")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of SourceFile
    ''' </summary>
    ''' <value>Name of SourceFile</value>
    ''' <returns>Returns Name of SourceFile</returns>
    ''' <remarks></remarks>
    Public Property SourceFileName() As String
        Get
            Return _curSourceFileName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSourceFileName <> value Then
                _curSourceFileName = value
                OnPropertyChanged("SourceFileName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Definition File
    ''' </summary>
    ''' <value>Name of Definition File</value>
    ''' <returns>Returns Name of Definition File</returns>
    ''' <remarks></remarks>
    Public Property DefinitionFileName() As String Implements ICommonTemplate.DefinitionFile
        Get
            Return _curDefinitionFileName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDefinitionFileName <> value Then
                _curDefinitionFileName = value
                OnPropertyChanged("DefinitionFileName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Source file is a fixed width file
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsFixedWidthFile() As Boolean
        Get
            Return _curIsFixedWidthFile
        End Get
        Set(ByVal value As Boolean)
            If _curIsFixedWidthFile <> value Then
                _curIsFixedWidthFile = value
                OnPropertyChanged("IsFixedWidthFile")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the transaction records in Source file are delimited by Carriage Return 
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsCarriageReturn() As Boolean
        Get
            Return _curIsCarriageReturn
        End Get
        Set(ByVal value As Boolean)
            If _curIsCarriageReturn <> value Then
                _curIsCarriageReturn = value
                OnPropertyChanged("IsCarriageReturn")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Sample Row No.
    ''' </summary>
    ''' <value>Sample Row No.</value>
    ''' <returns>Returns Sample Row No.</returns>
    ''' <remarks></remarks>
    Public Property SampleRowNumber() As String
        Get
            Return _curSampleRowNumber
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSampleRowNumber <> value Then
                _curSampleRowNumber = value
                OnPropertyChanged("SampleRowNumber")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the transaction records in Source file are delimited 
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsDelimited() As Boolean
        Get
            Return _curIsDelimited
        End Get
        Set(ByVal value As Boolean)
            If _curIsDelimited <> value Then
                _curIsDelimited = value
                OnPropertyChanged("IsDelimited")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Oridinal Position of Transaction Starting Character
    ''' </summary>
    ''' <value>Oridinal Position of Transaction Starting Character</value>
    ''' <returns>Returns Oridinal Position of Transaction Starting Character</returns>
    ''' <remarks></remarks>
    Public Property StartingCharacterPos() As String
        Get
            Return _curStartingCharacterPos
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curStartingCharacterPos <> value Then
                _curStartingCharacterPos = value
                OnPropertyChanged("StartingCharacterPos")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Length of each Transaction Record
    ''' </summary>
    ''' <value>Length of each Transaction Record</value>
    ''' <returns>Returns Length of each Transaction Record</returns>
    ''' <remarks></remarks>
    Public Property LenOfStartingCharacter() As String
        Get
            Return _curLenOfStartingCharacter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLenOfStartingCharacter <> value Then
                _curLenOfStartingCharacter = value
                OnPropertyChanged("LenOfStartingCharacter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Sample Transaction Record
    ''' </summary>
    ''' <value>Sample Transaction Record</value>
    ''' <returns>Returns Sample Transaction Record</returns>
    ''' <remarks></remarks>
    Public Property SampleRow() As String
        Get
            Return _curSampleRow
        End Get
        Set(ByVal value As String)
            'If Not value Is Nothing Then value = value.Trim()

            If _curSampleRow <> value Then
                _curSampleRow = value
                OnPropertyChanged("SampleRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Separator
    ''' </summary>
    ''' <value>Date Separator</value>
    ''' <returns>Returns Date Separator</returns>
    ''' <remarks></remarks>
    Public Property DateSeparator() As String Implements ICommonTemplate.DateSeparator
        Get
            Return _curDateSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateSeparator <> value Then
                _curDateSeparator = value
                OnPropertyChanged("DateSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Type
    ''' </summary>
    ''' <value>Date Type</value>
    ''' <returns>Returns Date Type</returns>
    ''' <remarks></remarks>
    Public Property DateType() As String Implements ICommonTemplate.DateType
        Get
            Return _curDateType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateType <> value Then
                _curDateType = value
                OnPropertyChanged("DateType")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether zeros are part of the Date
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether zeros are part of Date</returns>
    ''' <remarks></remarks>
    Public Property IsZeroInDate() As Boolean Implements ICommonTemplate.IsZerosInDate
        Get
            Return _curIsZeroInDate
        End Get
        Set(ByVal value As Boolean)
            If _curIsZeroInDate <> value Then
                _curIsZeroInDate = value
                OnPropertyChanged("IsZeroInDate")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Decimal Separator
    ''' </summary>
    ''' <value>Decimal Separator</value>
    ''' <returns>Returns Decimal Separator</returns>
    ''' <remarks></remarks>
    Public Property DecimalSeparator() As String Implements ICommonTemplate.DecimalSeparator
        Get
            Return _curDecimalSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDecimalSeparator <> value Then
                _curDecimalSeparator = value
                OnPropertyChanged("DecimalSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Thousand Separator
    ''' </summary>
    ''' <value>Thousand Separator</value>
    ''' <returns>Returns Thousand Separator</returns>
    ''' <remarks></remarks>
    Public Property ThousandSeparator() As String Implements ICommonTemplate.ThousandSeparator
        Get
            Return _curThousandSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curThousandSeparator <> value Then
                _curThousandSeparator = value
                OnPropertyChanged("ThousandSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in dollars
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in dollars</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInDollars() As Boolean Implements ICommonTemplate.IsAmountInDollars
        Get
            Return _curIsAmountInDollars
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInDollars <> value Then
                _curIsAmountInDollars = value
                OnPropertyChanged("IsAmountInDollars")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in Cents
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in Cents</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInCents() As Boolean Implements ICommonTemplate.IsAmountInCents
        Get
            Return _curIsAmountInCents
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInCents <> value Then
                _curIsAmountInCents = value
                OnPropertyChanged("IsAmountInCents")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Enclosure Character that encloses the Source Fields
    ''' </summary>
    ''' <value>Enclosure Character</value>
    ''' <returns>Returns Enclosure Character</returns>
    ''' <remarks></remarks>
    Public Property EnclosureCharacter() As String
        Get
            Return _curEnclosureCharacter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curEnclosureCharacter <> value Then
                _curEnclosureCharacter = value
                OnPropertyChanged("EnclosureCharacter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Delimiter Character that delimits the Source Fields
    ''' </summary>
    ''' <value>Delimiter Character</value>
    ''' <returns>Returns Delimiter Character</returns>
    ''' <remarks></remarks>
    Public Property DelimiterCharacter() As String
        Get
            Return _curDelimiterCharacter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDelimiterCharacter <> value Then
                _curDelimiterCharacter = value
                OnPropertyChanged("DelimiterCharacter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set user defined Delimiter Character that delimits the Source Fields
    ''' </summary>
    ''' <value>User Defined Delimiter Character</value>
    ''' <returns>Returns User Defined Delimiter Character</returns>
    ''' <remarks></remarks>
    Public Property OtherSeparator() As String
        Get
            Return _curOtherSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOtherSeparator <> value Then
                _curOtherSeparator = value
                OnPropertyChanged("OtherSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Header Row No.
    ''' </summary>
    ''' <value>Header Row No.</value>
    ''' <returns>Returns Header Row No.</returns>
    ''' <remarks></remarks>
    Public Property HeaderRowNumber() As String
        Get
            Return _curHeaderRowNumber
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curHeaderRowNumber <> value Then
                _curHeaderRowNumber = value
                OnPropertyChanged("HeaderRowNumber")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Transaction Start Row No.
    ''' </summary>
    ''' <value>Transaction Start Row No.</value>
    ''' <returns>Returns Transaction Start Row No.</returns>
    ''' <remarks></remarks>
    Public Property TransactionStartRowNumber() As String
        Get
            Return _curTransactionStartRowNumber
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curTransactionStartRowNumber <> value Then
                _curTransactionStartRowNumber = value
                OnPropertyChanged("TransactionStartRowNumber")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Header should be used as Source Field Name
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns Boolean</returns>
    ''' <remarks></remarks>
    Public Property IsHeaderAField() As Boolean
        Get
            Return _curIsHeaderAField
        End Get
        Set(ByVal value As Boolean)
            If _curIsHeaderAField <> value Then
                _curIsHeaderAField = value
                OnPropertyChanged("IsHeaderAField")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 1</value>
    ''' <returns>Returns the First Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef1() As String Implements ICommonTemplate.TransactionReferenceField1
        Get
            Return _curDuplicateTxnRef1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef1 <> value Then
                _curDuplicateTxnRef1 = value
                OnPropertyChanged("DuplicateTxnRef1")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 2</value>
    ''' <returns>Returns the Second Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef2() As String Implements ICommonTemplate.TransactionReferenceField2
        Get
            Return _curDuplicateTxnRef2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef2 <> value Then
                _curDuplicateTxnRef2 = value
                OnPropertyChanged("DuplicateTxnRef2")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 3</value>
    ''' <returns>Returns the Third Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef3() As String Implements ICommonTemplate.TransactionReferenceField3
        Get
            Return _curDuplicateTxnRef3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef3 <> value Then
                _curDuplicateTxnRef3 = value
                OnPropertyChanged("DuplicateTxnRef3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is for every row
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record setting is for every row</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordForEveryRow() As Boolean Implements ICommonTemplate.IsAdviceRecordForEveryRow
        Get
            Return _curIsAdviceRecordForEveryRow
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordForEveryRow <> value Then
                _curIsAdviceRecordForEveryRow = value
                OnPropertyChanged("IsAdviceRecordForEveryRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Advice record is after every character
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is after every character</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordAfterCharacter() As Boolean Implements ICommonTemplate.IsAdviceRecordAfterChar
        Get
            Return _curIsAdviceRecordAfterCharacter
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordAfterCharacter <> value Then
                _curIsAdviceRecordAfterCharacter = value
                OnPropertyChanged("IsAdviceRecordAfterCharacter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is delimited
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is delimited</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordDelimited() As Boolean Implements ICommonTemplate.IsAdviceRecordDelimited
        Get
            Return _curIsAdviceRecordDelimited
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordDelimited <> value Then
                _curIsAdviceRecordDelimited = value
                OnPropertyChanged("IsAdviceRecordDelimited")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set the ordinal position of Advice Record Character
    ''' </summary>
    ''' <value>Ordinal position of Advice Record Character</value>
    ''' <returns>Returns the Ordinal position of Advice Record Character</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecordCharPos() As String Implements ICommonTemplate.AdviceRecordChar
        Get
            Return _curAdviceRecordCharPos
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecordCharPos <> value Then
                _curAdviceRecordCharPos = value
                OnPropertyChanged("AdviceRecordCharPos")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Advice Record Delimiter
    ''' </summary>
    ''' <value>Advice Record Delimiter</value>
    ''' <returns>Returns the Advice Record Delimiter</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecordDelimiter() As String Implements ICommonTemplate.AdviceRecordDelimiter
        Get
            Return _curAdviceRecordDelimiter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecordDelimiter <> value Then
                _curAdviceRecordDelimiter = value
                OnPropertyChanged("AdviceRecordDelimiter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to get a List of Filters
    ''' </summary>
    ''' <value>Row Filter</value>
    ''' <returns>Returns list of Row Filters</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Filters() As RowFilterCollection Implements ICommonTemplate.Filters
        Get
            Return _curFilters
        End Get
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is AND for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether AND condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionAND() As Boolean Implements ICommonTemplate.IsFilterConditionAND
        Get
            Return _curIsFilterConditionAND
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionAND <> value Then
                _curIsFilterConditionAND = value
                OnPropertyChanged("IsFilterConditionAND")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is OR for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether OR condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionOR() As Boolean
        Get
            Return _curIsFilterConditionOR
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionOR <> value Then
                _curIsFilterConditionOR = value
                OnPropertyChanged("IsFilterConditionOR")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get a List of Source Fields
    ''' </summary>
    ''' <value>Source Field</value>
    ''' <returns>Returns a list of Source Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapSourceFields() As MapSourceFieldCollection Implements ICommonTemplate.MapSourceFields
        Get
            Return _curMapSourceFields
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get a List of Bank Fields
    ''' </summary>
    ''' <value>Bank Field</value>
    ''' <returns>Returns a List of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapBankFields() As MapBankFieldCollection Implements ICommonTemplate.MapBankFields
        Get
            Return _curMapBankFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Translator Settings
    ''' </summary>
    ''' <value>Translator Setting</value>
    ''' <returns>Returns a List of Translator Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TranslatorSettings() As TranslatorSettingCollection Implements ICommonTemplate.TranslatorSettings
        Get
            Return _curTranslatorSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Editable Settings
    ''' </summary>
    ''' <value>Editable Setting</value>
    ''' <returns>Returns a List of Editable Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditableSettings() As EditableSettingCollection Implements ICommonTemplate.EditableSettings
        Get
            Return _curEditableSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Lookup Settings
    ''' </summary>
    ''' <value>Lookup Setting</value>
    ''' <returns>Returns a List of Lookup Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LookupSettings() As LookupSettingCollection Implements ICommonTemplate.LookupSettings
        Get
            Return _curLookupSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Calculated Fields
    ''' </summary>
    ''' <value>Calculated Field</value>
    ''' <returns>Returns a List of Calculated Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CalculatedFields() As CalculatedFieldCollection Implements ICommonTemplate.CalculatedFields
        Get
            Return _curCalculatedFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of String Manipulation Settings
    ''' </summary>
    ''' <value>String Manipulation Setting</value>
    ''' <returns>returns a List of String Manipulation Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property StringManipulations() As StringManipulationCollection Implements ICommonTemplate.StringManipulations
        Get
            Return _curStringManipulations
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the User that created this Object
    ''' </summary>
    ''' <value>User ID</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Created Date and Time of this object
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Created Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Modified Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Modified Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that originally modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Original Modified Date and Time of this objecdt
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Original Modified Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get/Set the P-record Indicator Column for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PColumn() As String
        Get
            Return _curPColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curPColumn <> value Then
                _curPColumn = value
                OnPropertyChanged("PColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the P-record Indicator for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PIndicator() As String
        Get
            Return _curPIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curPIndicator <> value Then
                _curPIndicator = value
                OnPropertyChanged("PIndicator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the W-record Indicator Column for iFTS-2 format
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WColumn() As String
        Get
            Return _curWColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWColumn <> value Then
                _curWColumn = value
                OnPropertyChanged("WColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the W-record Indicator for iFTS-2 format
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WIndicator() As String
        Get
            Return _curWIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWIndicator <> value Then
                _curWIndicator = value
                OnPropertyChanged("WIndicator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the I-record Indicator Column for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IColumn() As String
        Get
            Return _curIColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curIColumn <> value Then
                _curIColumn = value
                OnPropertyChanged("IColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the I-record Indicator for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IIndicator() As String
        Get
            Return _curIIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curIIndicator <> value Then
                _curIIndicator = value
                OnPropertyChanged("IIndicator")
            End If
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Marks the Template as modified
    ''' </summary>
    ''' <param name="value">Boolean value indicating whether the Template has been modified</param>
    ''' <remarks></remarks>
    Public Sub SetDirty(ByVal value As Boolean)
        _isDirty = value
    End Sub
    ''' <summary>
    ''' Apply the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriOutputTemplateName = _curOutputTemplateName
        _oriCommonTemplateName = _curCommonTemplateName
        _oriIsEnabled = _curIsEnabled
        _oriIsDraft = _curIsDraft
        _oriSourceFileName = _curSourceFileName
        _oriDefinitionFileName = _curDefinitionFileName
        _oriIsFixedWidthFile = _curIsFixedWidthFile
        _oriIsCarriageReturn = _curIsCarriageReturn
        _oriSampleRowNumber = _curSampleRowNumber
        _oriIsDelimited = _curIsDelimited
        _oriStartingCharacterPos = _curStartingCharacterPos
        _oriLenOfStartingCharacter = _curLenOfStartingCharacter
        _oriSampleRow = _curSampleRow
        _oriDateSeparator = _curDateSeparator
        _oriDateType = _curDateType
        _oriIsZeroInDate = _curIsZeroInDate
        _oriDecimalSeparator = _curDecimalSeparator
        _oriThousandSeparator = _curThousandSeparator
        _oriIsAmountInDollars = _curIsAmountInDollars
        _oriIsAmountInCents = _curIsAmountInCents
        _oriEnclosureCharacter = _curEnclosureCharacter
        _oriDelimiterCharacter = _curDelimiterCharacter
        _oriOtherSeparator = _curOtherSeparator
        _oriHeaderRowNumber = _curHeaderRowNumber
        _oriTransactionStartRowNumber = _curTransactionStartRowNumber
        _oriIsHeaderAField = _curIsHeaderAField
        _oriDuplicateTxnRef1 = _curDuplicateTxnRef1
        _oriDuplicateTxnRef2 = _curDuplicateTxnRef2
        _oriDuplicateTxnRef3 = _curDuplicateTxnRef3
        _oriIsAdviceRecordForEveryRow = _curIsAdviceRecordForEveryRow
        _oriIsAdviceRecordAfterCharacter = _curIsAdviceRecordAfterCharacter
        _oriIsAdviceRecordDelimited = _curIsAdviceRecordDelimited
        _oriAdviceRecordCharPos = _curAdviceRecordCharPos
        _oriAdviceRecordDelimiter = _curAdviceRecordDelimiter
        _oriIsFilterConditionAND = _curIsFilterConditionAND
        _oriIsFilterConditionOR = _curIsFilterConditionOR
        _oriPColumn = _curPColumn
        _oriPIndicator = _curPIndicator
        _oriWColumn = _curWColumn
        _oriWIndicator = _curWIndicator
        _oriIColumn = _curIColumn
        _oriIIndicator = _curIIndicator
        _isDirty = False
    End Sub
    ''' <summary>
    ''' Begin making changes to the fields of this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldOutputTemplateName = _curOutputTemplateName
        _oldCommonTemplateName = _curCommonTemplateName
        _oldIsEnabled = _curIsEnabled
        _oldIsDraft = _curIsDraft
        _oldSourceFileName = _curSourceFileName
        _oldDefinitionFileName = _curDefinitionFileName
        _oldIsFixedWidthFile = _curIsFixedWidthFile
        _oldIsCarriageReturn = _curIsCarriageReturn
        _oldSampleRowNumber = _curSampleRowNumber
        _oldIsDelimited = _curIsDelimited
        _oldStartingCharacterPos = _curStartingCharacterPos
        _oldLenOfStartingCharacter = _curLenOfStartingCharacter
        _oldSampleRow = _curSampleRow
        _oldDateSeparator = _curDateSeparator
        _oldDateType = _curDateType
        _oldIsZeroInDate = _curIsZeroInDate
        _oldDecimalSeparator = _curDecimalSeparator
        _oldThousandSeparator = _curThousandSeparator
        _oldIsAmountInDollars = _curIsAmountInDollars
        _oldIsAmountInCents = _curIsAmountInCents
        _oldEnclosureCharacter = _curEnclosureCharacter
        _oldDelimiterCharacter = _curDelimiterCharacter
        _oldOtherSeparator = _curOtherSeparator
        _oldHeaderRowNumber = _curHeaderRowNumber
        _oldTransactionStartRowNumber = _curTransactionStartRowNumber
        _oldIsHeaderAField = _curIsHeaderAField
        _oldDuplicateTxnRef1 = _curDuplicateTxnRef1
        _oldDuplicateTxnRef2 = _curDuplicateTxnRef2
        _oldDuplicateTxnRef3 = _curDuplicateTxnRef3
        _oldIsAdviceRecordForEveryRow = _curIsAdviceRecordForEveryRow
        _oldIsAdviceRecordAfterCharacter = _curIsAdviceRecordAfterCharacter
        _oldIsAdviceRecordDelimited = _curIsAdviceRecordDelimited
        _oldAdviceRecordCharPos = _curAdviceRecordCharPos
        _oldAdviceRecordDelimiter = _curAdviceRecordDelimiter
        _oldIsFilterConditionAND = _curIsFilterConditionAND
        _oldIsFilterConditionOR = _curIsFilterConditionOR
        _oldPColumn = _curPColumn
        _oldPIndicator = _curPIndicator
        _oldWColumn = _curWColumn
        _oldWIndicator = _curWIndicator
        _oldIColumn = _curIColumn
        _oldIIndicator = _curIIndicator
    End Sub
    ''' <summary>
    ''' Cancel the changes made to the fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()

        _curOutputTemplateName = _oldOutputTemplateName
        _curCommonTemplateName = _oldCommonTemplateName
        _curIsEnabled = _oldIsEnabled
        _curIsDraft = _oldIsDraft
        _curSourceFileName = _oldSourceFileName
        _curDefinitionFileName = _oldDefinitionFileName
        _curIsFixedWidthFile = _oldIsFixedWidthFile
        _curIsCarriageReturn = _oldIsCarriageReturn
        _curSampleRowNumber = _oldSampleRowNumber
        _curIsDelimited = _oldIsDelimited
        _curStartingCharacterPos = _oldStartingCharacterPos
        _curLenOfStartingCharacter = _oldLenOfStartingCharacter
        _curSampleRow = _oldSampleRow
        _curDateSeparator = _oldDateSeparator
        _curDateType = _oldDateType
        _curIsZeroInDate = _oldIsZeroInDate
        _curDecimalSeparator = _oldDecimalSeparator
        _curThousandSeparator = _oldThousandSeparator
        _curIsAmountInDollars = _oldIsAmountInDollars
        _curIsAmountInCents = _oldIsAmountInCents
        _curEnclosureCharacter = _oldEnclosureCharacter
        _curDelimiterCharacter = _oldDelimiterCharacter
        _curOtherSeparator = _oldOtherSeparator
        _curHeaderRowNumber = _oldHeaderRowNumber
        _curTransactionStartRowNumber = _oldTransactionStartRowNumber
        _curIsHeaderAField = _oldIsHeaderAField
        _curDuplicateTxnRef1 = _oldDuplicateTxnRef1
        _curDuplicateTxnRef2 = _oldDuplicateTxnRef2
        _curDuplicateTxnRef3 = _oldDuplicateTxnRef3
        _curIsAdviceRecordForEveryRow = _oldIsAdviceRecordForEveryRow
        _curIsAdviceRecordAfterCharacter = _oldIsAdviceRecordAfterCharacter
        _curIsAdviceRecordDelimited = _oldIsAdviceRecordDelimited
        _curAdviceRecordCharPos = _oldAdviceRecordCharPos
        _curAdviceRecordDelimiter = _oldAdviceRecordDelimiter
        _curIsFilterConditionAND = _oldIsFilterConditionAND
        _curIsFilterConditionOR = _oldIsFilterConditionOR
        _curPColumn = _oldPColumn
        _curPIndicator = _oldPIndicator
        _curWColumn = _oldWColumn
        _curWIndicator = _oldWIndicator
        _curIColumn = _oldIColumn
        _curIIndicator = _oldIIndicator
    End Sub

#Region " Serialization "

    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)

        'Dont add newer properties here. Newer properties should be appended. 
        On Error Resume Next
        MyBase.ReadXml(reader)

        _curOutputTemplateName = ReadXMLElement(reader, "_curOutputTemplateName")
        _oldOutputTemplateName = ReadXMLElement(reader, "_oldOutputTemplateName")
        _oriOutputTemplateName = ReadXMLElement(reader, "_oriOutputTemplateName")
        _curCommonTemplateName = ReadXMLElement(reader, "_curCommonTemplateName")
        _oldCommonTemplateName = ReadXMLElement(reader, "_oldCommonTemplateName")
        _oriCommonTemplateName = ReadXMLElement(reader, "_oriCommonTemplateName")
        _curIsEnabled = ReadXMLElement(reader, "_curIsEnabled")
        _oldIsEnabled = ReadXMLElement(reader, "_oldIsEnabled")
        _oriIsEnabled = ReadXMLElement(reader, "_oriIsEnabled")
        _curIsDraft = ReadXMLElement(reader, "_curIsDraft")
        _oldIsDraft = ReadXMLElement(reader, "_oldIsDraft")
        _oriIsDraft = ReadXMLElement(reader, "_oriIsDraft")
        _curSourceFileName = ReadXMLElement(reader, "_curSourceFileName")
        _oldSourceFileName = ReadXMLElement(reader, "_oldSourceFileName")
        _oriSourceFileName = ReadXMLElement(reader, "_oriSourceFileName")
        _curDefinitionFileName = ReadXMLElement(reader, "_curDefinitionFileName")
        _oldDefinitionFileName = ReadXMLElement(reader, "_oldDefinitionFileName")
        _oriDefinitionFileName = ReadXMLElement(reader, "_oriDefinitionFileName")
        _curIsFixedWidthFile = ReadXMLElement(reader, "_curIsFixedWidthFile")
        _oldIsFixedWidthFile = ReadXMLElement(reader, "_oldIsFixedWidthFile")
        _oriIsFixedWidthFile = ReadXMLElement(reader, "_oriIsFixedWidthFile")
        _curIsCarriageReturn = ReadXMLElement(reader, "_curIsCarriageReturn")
        _oldIsCarriageReturn = ReadXMLElement(reader, "_oldIsCarriageReturn")
        _oriIsCarriageReturn = ReadXMLElement(reader, "_oriIsCarriageReturn")
        _curSampleRowNumber = ReadXMLElement(reader, "_curSampleRowNumber")
        _oldSampleRowNumber = ReadXMLElement(reader, "_oldSampleRowNumber")
        _oriSampleRowNumber = ReadXMLElement(reader, "_oriSampleRowNumber")
        _curIsDelimited = ReadXMLElement(reader, "_curIsDelimited")
        _oldIsDelimited = ReadXMLElement(reader, "_oldIsDelimited")
        _oriIsDelimited = ReadXMLElement(reader, "_oriIsDelimited")
        _curStartingCharacterPos = ReadXMLElement(reader, "_curStartingCharacterPos")
        _oldStartingCharacterPos = ReadXMLElement(reader, "_oldStartingCharacterPos")
        _oriStartingCharacterPos = ReadXMLElement(reader, "_oriStartingCharacterPos")
        _curLenOfStartingCharacter = ReadXMLElement(reader, "_curLenOfStartingCharacter")
        _oldLenOfStartingCharacter = ReadXMLElement(reader, "_oldLenOfStartingCharacter")
        _oriLenOfStartingCharacter = ReadXMLElement(reader, "_oriLenOfStartingCharacter")
        _curSampleRow = ReadXMLElement(reader, "_curSampleRow")
        _oldSampleRow = ReadXMLElement(reader, "_oldSampleRow")
        _oriSampleRow = ReadXMLElement(reader, "_oriSampleRow")
        _curDateSeparator = ReadXMLElement(reader, "_curDateSeparator")
        _oldDateSeparator = ReadXMLElement(reader, "_oldDateSeparator")
        _oriDateSeparator = ReadXMLElement(reader, "_oriDateSeparator")
        _curDateType = ReadXMLElement(reader, "_curDateType")
        _oldDateType = ReadXMLElement(reader, "_oldDateType")
        _oriDateType = ReadXMLElement(reader, "_oriDateType")
        _curIsZeroInDate = ReadXMLElement(reader, "_curIsZeroInDate")
        _oldIsZeroInDate = ReadXMLElement(reader, "_oldIsZeroInDate")
        _oriIsZeroInDate = ReadXMLElement(reader, "_oriIsZeroInDate")
        _curDecimalSeparator = ReadXMLElement(reader, "_curDecimalSeparator")
        _oldDecimalSeparator = ReadXMLElement(reader, "_oldDecimalSeparator")
        _oriDecimalSeparator = ReadXMLElement(reader, "_oriDecimalSeparator")
        _curThousandSeparator = ReadXMLElement(reader, "_curThousandSeparator")
        _oldThousandSeparator = ReadXMLElement(reader, "_oldThousandSeparator")
        _oriThousandSeparator = ReadXMLElement(reader, "_oriThousandSeparator")
        _curIsAmountInDollars = ReadXMLElement(reader, "_curIsAmountInDollars")
        _oldIsAmountInDollars = ReadXMLElement(reader, "_oldIsAmountInDollars")
        _oriIsAmountInDollars = ReadXMLElement(reader, "_oriIsAmountInDollars")
        _curIsAmountInCents = ReadXMLElement(reader, "_curIsAmountInCents")
        _oldIsAmountInCents = ReadXMLElement(reader, "_oldIsAmountInCents")
        _oriIsAmountInCents = ReadXMLElement(reader, "_oriIsAmountInCents")
        _curEnclosureCharacter = ReadXMLElement(reader, "_curEnclosureCharacter")
        _oldEnclosureCharacter = ReadXMLElement(reader, "_oldEnclosureCharacter")
        _oriEnclosureCharacter = ReadXMLElement(reader, "_oriEnclosureCharacter")
        _curDelimiterCharacter = ReadXMLElement(reader, "_curDelimiterCharacter")
        _oldDelimiterCharacter = ReadXMLElement(reader, "_oldDelimiterCharacter")
        _oriDelimiterCharacter = ReadXMLElement(reader, "_oriDelimiterCharacter")
        _curOtherSeparator = ReadXMLElement(reader, "_curOtherSeparator")
        _oldOtherSeparator = ReadXMLElement(reader, "_oldOtherSeparator")
        _oriOtherSeparator = ReadXMLElement(reader, "_oriOtherSeparator")
        _curHeaderRowNumber = ReadXMLElement(reader, "_curHeaderRowNumber")
        _oldHeaderRowNumber = ReadXMLElement(reader, "_oldHeaderRowNumber")
        _oriHeaderRowNumber = ReadXMLElement(reader, "_oriHeaderRowNumber")
        _curTransactionStartRowNumber = ReadXMLElement(reader, "_curTransactionStartRowNumber")
        _oldTransactionStartRowNumber = ReadXMLElement(reader, "_oldTransactionStartRowNumber")
        _oriTransactionStartRowNumber = ReadXMLElement(reader, "_oriTransactionStartRowNumber")
        _curIsHeaderAField = ReadXMLElement(reader, "_curIsHeaderAField")
        _oldIsHeaderAField = ReadXMLElement(reader, "_oldIsHeaderAField")
        _oriIsHeaderAField = ReadXMLElement(reader, "_oriIsHeaderAField")
        _curDuplicateTxnRef1 = ReadXMLElement(reader, "_curDuplicateTxnRef1")
        _oldDuplicateTxnRef1 = ReadXMLElement(reader, "_oldDuplicateTxnRef1")
        _oriDuplicateTxnRef1 = ReadXMLElement(reader, "_oriDuplicateTxnRef1")
        _curDuplicateTxnRef2 = ReadXMLElement(reader, "_curDuplicateTxnRef2")
        _oldDuplicateTxnRef2 = ReadXMLElement(reader, "_oldDuplicateTxnRef2")
        _oriDuplicateTxnRef2 = ReadXMLElement(reader, "_oriDuplicateTxnRef2")
        _curDuplicateTxnRef3 = ReadXMLElement(reader, "_curDuplicateTxnRef3")
        _oldDuplicateTxnRef3 = ReadXMLElement(reader, "_oldDuplicateTxnRef3")
        _oriDuplicateTxnRef3 = ReadXMLElement(reader, "_oriDuplicateTxnRef3")
        _curIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_curIsAdviceRecordForEveryRow")
        _oldIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_oldIsAdviceRecordForEveryRow")
        _oriIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_oriIsAdviceRecordForEveryRow")
        _curIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_curIsAdviceRecordAfterCharacter")
        _oldIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_oldIsAdviceRecordAfterCharacter")
        _oriIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_oriIsAdviceRecordAfterCharacter")
        _curIsAdviceRecordDelimited = ReadXMLElement(reader, "_curIsAdviceRecordDelimited")
        _oldIsAdviceRecordDelimited = ReadXMLElement(reader, "_oldIsAdviceRecordDelimited")
        _oriIsAdviceRecordDelimited = ReadXMLElement(reader, "_oriIsAdviceRecordDelimited")
        _curAdviceRecordCharPos = ReadXMLElement(reader, "_curAdviceRecordCharPos")
        _oldAdviceRecordCharPos = ReadXMLElement(reader, "_oldAdviceRecordCharPos")
        _oriAdviceRecordCharPos = ReadXMLElement(reader, "_oriAdviceRecordCharPos")
        _curAdviceRecordDelimiter = ReadXMLElement(reader, "_curAdviceRecordDelimiter")
        _oldAdviceRecordDelimiter = ReadXMLElement(reader, "_oldAdviceRecordDelimiter")
        _oriAdviceRecordDelimiter = ReadXMLElement(reader, "_oriAdviceRecordDelimiter")
        _curFilters.ReadXml(reader)
        _curIsFilterConditionAND = ReadXMLElement(reader, "_curIsFilterConditionAND")
        _oldIsFilterConditionAND = ReadXMLElement(reader, "_oldIsFilterConditionAND")
        _oriIsFilterConditionAND = ReadXMLElement(reader, "_oriIsFilterConditionAND")
        _curIsFilterConditionOR = ReadXMLElement(reader, "_curIsFilterConditionOR")
        _oldIsFilterConditionOR = ReadXMLElement(reader, "_oldIsFilterConditionOR")
        _oriIsFilterConditionOR = ReadXMLElement(reader, "_oriIsFilterConditionOR")
        _curMapSourceFields.ReadXml(reader)
        _curMapBankFields.ReadXml(reader)
        _curTranslatorSettings.ReadXml(reader)
        _curEditableSettings.ReadXml(reader)
        _curLookupSettings.ReadXml(reader)
        _curCalculatedFields.ReadXml(reader)
        _curStringManipulations.ReadXml(reader)


        _curPColumn = ReadXMLElement(reader, "_curPColumn")
        _oldPColumn = ReadXMLElement(reader, "_oldPColumn")
        _oriPColumn = ReadXMLElement(reader, "_oriPColumn")
        _curPIndicator = ReadXMLElement(reader, "_curPIndicator")
        _oldPIndicator = ReadXMLElement(reader, "_oldPIndicator")
        _oriPIndicator = ReadXMLElement(reader, "_oriPIndicator")
        _curWColumn = ReadXMLElement(reader, "_curWColumn")
        _oldWColumn = ReadXMLElement(reader, "_oldWColumn")
        _oriWColumn = ReadXMLElement(reader, "_oriWColumn")
        _curWIndicator = ReadXMLElement(reader, "_curWIndicator")
        _oldWIndicator = ReadXMLElement(reader, "_oldWIndicator")
        _oriWIndicator = ReadXMLElement(reader, "_oriWIndicator")
        _curIColumn = ReadXMLElement(reader, "_curIColumn")
        _oldIColumn = ReadXMLElement(reader, "_oldIColumn")
        _oriIColumn = ReadXMLElement(reader, "_oriIColumn")
        _curIIndicator = ReadXMLElement(reader, "_curIIndicator")
        _oldIIndicator = ReadXMLElement(reader, "_oldIIndicator")
        _oriIIndicator = ReadXMLElement(reader, "_oriIIndicator")

        reader.ReadEndElement()
    End Sub

    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        'Dont add any newer properties here..instead ...append them to this function
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curOutputTemplateName", _curOutputTemplateName)
        WriteXmlElement(writer, "_oldOutputTemplateName", _oldOutputTemplateName)
        WriteXmlElement(writer, "_oriOutputTemplateName", _oriOutputTemplateName)
        WriteXmlElement(writer, "_curCommonTemplateName", _curCommonTemplateName)
        WriteXmlElement(writer, "_oldCommonTemplateName", _oldCommonTemplateName)
        WriteXmlElement(writer, "_oriCommonTemplateName", _oriCommonTemplateName)
        WriteXmlElement(writer, "_curIsEnabled", _curIsEnabled)
        WriteXmlElement(writer, "_oldIsEnabled", _oldIsEnabled)
        WriteXmlElement(writer, "_oriIsEnabled", _oriIsEnabled)
        WriteXmlElement(writer, "_curIsDraft", _curIsDraft)
        WriteXmlElement(writer, "_oldIsDraft", _oldIsDraft)
        WriteXmlElement(writer, "_oriIsDraft", _oriIsDraft)
        WriteXmlElement(writer, "_curSourceFileName", _curSourceFileName)
        WriteXmlElement(writer, "_oldSourceFileName", _oldSourceFileName)
        WriteXmlElement(writer, "_oriSourceFileName", _oriSourceFileName)

        WriteXmlElement(writer, "_curDefinitionFileName", _curDefinitionFileName)
        WriteXmlElement(writer, "_oldDefinitionFileName", _oldDefinitionFileName)
        WriteXmlElement(writer, "_oriDefinitionFileName", _oriDefinitionFileName)

        WriteXmlElement(writer, "_curIsFixedWidthFile", _curIsFixedWidthFile)
        WriteXmlElement(writer, "_oldIsFixedWidthFile", _oldIsFixedWidthFile)
        WriteXmlElement(writer, "_oriIsFixedWidthFile", _oriIsFixedWidthFile)
        WriteXmlElement(writer, "_curIsCarriageReturn", _curIsCarriageReturn)
        WriteXmlElement(writer, "_oldIsCarriageReturn", _oldIsCarriageReturn)
        WriteXmlElement(writer, "_oriIsCarriageReturn", _oriIsCarriageReturn)
        WriteXmlElement(writer, "_curSampleRowNumber", _curSampleRowNumber)
        WriteXmlElement(writer, "_oldSampleRowNumber", _oldSampleRowNumber)
        WriteXmlElement(writer, "_oriSampleRowNumber", _oriSampleRowNumber)
        WriteXmlElement(writer, "_curIsDelimited", _curIsDelimited)
        WriteXmlElement(writer, "_oldIsDelimited", _oldIsDelimited)
        WriteXmlElement(writer, "_oriIsDelimited", _oriIsDelimited)
        WriteXmlElement(writer, "_curStartingCharacterPos", _curStartingCharacterPos)
        WriteXmlElement(writer, "_oldStartingCharacterPos", _oldStartingCharacterPos)
        WriteXmlElement(writer, "_oriStartingCharacterPos", _oriStartingCharacterPos)
        WriteXmlElement(writer, "_curLenOfStartingCharacter", _curLenOfStartingCharacter)
        WriteXmlElement(writer, "_oldLenOfStartingCharacter", _oldLenOfStartingCharacter)
        WriteXmlElement(writer, "_oriLenOfStartingCharacter", _oriLenOfStartingCharacter)
        WriteXmlElement(writer, "_curSampleRow", _curSampleRow)
        WriteXmlElement(writer, "_oldSampleRow", _oldSampleRow)
        WriteXmlElement(writer, "_oriSampleRow", _oriSampleRow)
        WriteXmlElement(writer, "_curDateSeparator", _curDateSeparator)
        WriteXmlElement(writer, "_oldDateSeparator", _oldDateSeparator)
        WriteXmlElement(writer, "_oriDateSeparator", _oriDateSeparator)
        WriteXmlElement(writer, "_curDateType", _curDateType)
        WriteXmlElement(writer, "_oldDateType", _oldDateType)
        WriteXmlElement(writer, "_oriDateType", _oriDateType)
        WriteXmlElement(writer, "_curIsZeroInDate", _curIsZeroInDate)
        WriteXmlElement(writer, "_oldIsZeroInDate", _oldIsZeroInDate)
        WriteXmlElement(writer, "_oriIsZeroInDate", _oriIsZeroInDate)
        WriteXmlElement(writer, "_curDecimalSeparator", _curDecimalSeparator)
        WriteXmlElement(writer, "_oldDecimalSeparator", _oldDecimalSeparator)
        WriteXmlElement(writer, "_oriDecimalSeparator", _oriDecimalSeparator)
        WriteXmlElement(writer, "_curThousandSeparator", _curThousandSeparator)
        WriteXmlElement(writer, "_oldThousandSeparator", _oldThousandSeparator)
        WriteXmlElement(writer, "_oriThousandSeparator", _oriThousandSeparator)
        WriteXmlElement(writer, "_curIsAmountInDollars", _curIsAmountInDollars)
        WriteXmlElement(writer, "_oldIsAmountInDollars", _oldIsAmountInDollars)
        WriteXmlElement(writer, "_oriIsAmountInDollars", _oriIsAmountInDollars)
        WriteXmlElement(writer, "_curIsAmountInCents", _curIsAmountInCents)
        WriteXmlElement(writer, "_oldIsAmountInCents", _oldIsAmountInCents)
        WriteXmlElement(writer, "_oriIsAmountInCents", _oriIsAmountInCents)
        WriteXmlElement(writer, "_curEnclosureCharacter", _curEnclosureCharacter)
        WriteXmlElement(writer, "_oldEnclosureCharacter", _oldEnclosureCharacter)
        WriteXmlElement(writer, "_oriEnclosureCharacter", _oriEnclosureCharacter)
        WriteXmlElement(writer, "_curDelimiterCharacter", _curDelimiterCharacter)
        WriteXmlElement(writer, "_oldDelimiterCharacter", _oldDelimiterCharacter)
        WriteXmlElement(writer, "_oriDelimiterCharacter", _oriDelimiterCharacter)
        WriteXmlElement(writer, "_curOtherSeparator", _curOtherSeparator)
        WriteXmlElement(writer, "_oldOtherSeparator", _oldOtherSeparator)
        WriteXmlElement(writer, "_oriOtherSeparator", _oriOtherSeparator)
        WriteXmlElement(writer, "_curHeaderRowNumber", _curHeaderRowNumber)
        WriteXmlElement(writer, "_oldHeaderRowNumber", _oldHeaderRowNumber)
        WriteXmlElement(writer, "_oriHeaderRowNumber", _oriHeaderRowNumber)
        WriteXmlElement(writer, "_curTransactionStartRowNumber", _curTransactionStartRowNumber)
        WriteXmlElement(writer, "_oldTransactionStartRowNumber", _oldTransactionStartRowNumber)
        WriteXmlElement(writer, "_oriTransactionStartRowNumber", _oriTransactionStartRowNumber)
        WriteXmlElement(writer, "_curIsHeaderAField", _curIsHeaderAField)
        WriteXmlElement(writer, "_oldIsHeaderAField", _oldIsHeaderAField)
        WriteXmlElement(writer, "_oriIsHeaderAField", _oriIsHeaderAField)
        WriteXmlElement(writer, "_curDuplicateTxnRef1", _curDuplicateTxnRef1)
        WriteXmlElement(writer, "_oldDuplicateTxnRef1", _oldDuplicateTxnRef1)
        WriteXmlElement(writer, "_oriDuplicateTxnRef1", _oriDuplicateTxnRef1)
        WriteXmlElement(writer, "_curDuplicateTxnRef2", _curDuplicateTxnRef2)
        WriteXmlElement(writer, "_oldDuplicateTxnRef2", _oldDuplicateTxnRef2)
        WriteXmlElement(writer, "_oriDuplicateTxnRef2", _oriDuplicateTxnRef2)
        WriteXmlElement(writer, "_curDuplicateTxnRef3", _curDuplicateTxnRef3)
        WriteXmlElement(writer, "_oldDuplicateTxnRef3", _oldDuplicateTxnRef3)
        WriteXmlElement(writer, "_oriDuplicateTxnRef3", _oriDuplicateTxnRef3)
        WriteXmlElement(writer, "_curIsAdviceRecordForEveryRow", _curIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_oldIsAdviceRecordForEveryRow", _oldIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_oriIsAdviceRecordForEveryRow", _oriIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_curIsAdviceRecordAfterCharacter", _curIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_oldIsAdviceRecordAfterCharacter", _oldIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_oriIsAdviceRecordAfterCharacter", _oriIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_curIsAdviceRecordDelimited", _curIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_oldIsAdviceRecordDelimited", _oldIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_oriIsAdviceRecordDelimited", _oriIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_curAdviceRecordCharPos", _curAdviceRecordCharPos)
        WriteXmlElement(writer, "_oldAdviceRecordCharPos", _oldAdviceRecordCharPos)
        WriteXmlElement(writer, "_oriAdviceRecordCharPos", _oriAdviceRecordCharPos)
        WriteXmlElement(writer, "_curAdviceRecordDelimiter", _curAdviceRecordDelimiter)
        WriteXmlElement(writer, "_oldAdviceRecordDelimiter", _oldAdviceRecordDelimiter)
        WriteXmlElement(writer, "_oriAdviceRecordDelimiter", _oriAdviceRecordDelimiter)
        _curFilters.WriteXml(writer)
        WriteXmlElement(writer, "_curIsFilterConditionAND", _curIsFilterConditionAND)
        WriteXmlElement(writer, "_oldIsFilterConditionAND", _oldIsFilterConditionAND)
        WriteXmlElement(writer, "_oriIsFilterConditionAND", _oriIsFilterConditionAND)
        WriteXmlElement(writer, "_curIsFilterConditionOR", _curIsFilterConditionOR)
        WriteXmlElement(writer, "_oldIsFilterConditionOR", _oldIsFilterConditionOR)
        WriteXmlElement(writer, "_oriIsFilterConditionOR", _oriIsFilterConditionOR)
        _curMapSourceFields.WriteXml(writer)
        _curMapBankFields.WriteXml(writer)
        _curTranslatorSettings.WriteXml(writer)
        _curEditableSettings.WriteXml(writer)
        _curLookupSettings.WriteXml(writer)
        _curCalculatedFields.WriteXml(writer)
        _curStringManipulations.WriteXml(writer)


        WriteXmlElement(writer, "_curPColumn", _curPColumn)
        WriteXmlElement(writer, "_oldPColumn", _oldPColumn)
        WriteXmlElement(writer, "_oriPColumn", _oriPColumn)
        WriteXmlElement(writer, "_curPIndicator", _curPIndicator)
        WriteXmlElement(writer, "_oldPIndicator", _oldPIndicator)
        WriteXmlElement(writer, "_oriPIndicator", _oriPIndicator)
        WriteXmlElement(writer, "_curWColumn", _curWColumn)
        WriteXmlElement(writer, "_oldWColumn", _oldWColumn)
        WriteXmlElement(writer, "_oriWColumn", _oriWColumn)
        WriteXmlElement(writer, "_curWIndicator", _curWIndicator)
        WriteXmlElement(writer, "_oldWIndicator", _oldWIndicator)
        WriteXmlElement(writer, "_oriWIndicator", _oriWIndicator)
        WriteXmlElement(writer, "_curIColumn", _curIColumn)
        WriteXmlElement(writer, "_oldIColumn", _oldIColumn)
        WriteXmlElement(writer, "_oriIColumn", _oriIColumn)
        WriteXmlElement(writer, "_curIIndicator", _curIIndicator)
        WriteXmlElement(writer, "_oldIIndicator", _oldIIndicator)
        WriteXmlElement(writer, "_oriIIndicator", _oriIIndicator)
        'Append new Properties here


    End Sub

#End Region

#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves this object to given file
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(CommonTemplateTextDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads this object from given file
    ''' </summary>
    ''' <param name="filename">Name of file</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As CommonTemplateTextDetail
        Dim serializer As New XmlSerializer(GetType(CommonTemplateTextDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), CommonTemplateTextDetail)

        stream.Close()
        Clone(objE)
        Validate()
        DataContainer = objE
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As CommonTemplateTextDetail = DirectCast(obj, CommonTemplateTextDetail)
        _curOutputTemplateName = objE._curOutputTemplateName
        _oldOutputTemplateName = objE._oldOutputTemplateName
        _oriOutputTemplateName = objE._oriOutputTemplateName
        _curCommonTemplateName = objE._curCommonTemplateName
        _oldCommonTemplateName = objE._oldCommonTemplateName
        _oriCommonTemplateName = objE._oriCommonTemplateName
        _curIsEnabled = objE._curIsEnabled
        _oldIsEnabled = objE._oldIsEnabled
        _oriIsEnabled = objE._oriIsEnabled
        _curIsDraft = objE._curIsDraft
        _oldIsDraft = objE._oldIsDraft
        _oriIsDraft = objE._oriIsDraft
        _curSourceFileName = objE._curSourceFileName
        _oldSourceFileName = objE._oldSourceFileName
        _oriSourceFileName = objE._oriSourceFileName

        _curDefinitionFileName = objE._curDefinitionFileName
        _oldDefinitionFileName = objE._oldDefinitionFileName
        _oriDefinitionFileName = objE._oriDefinitionFileName

        _curIsFixedWidthFile = objE._curIsFixedWidthFile
        _oldIsFixedWidthFile = objE._oldIsFixedWidthFile
        _oriIsFixedWidthFile = objE._oriIsFixedWidthFile
        _curIsCarriageReturn = objE._curIsCarriageReturn
        _oldIsCarriageReturn = objE._oldIsCarriageReturn
        _oriIsCarriageReturn = objE._oriIsCarriageReturn
        _curSampleRowNumber = objE._curSampleRowNumber
        _oldSampleRowNumber = objE._oldSampleRowNumber
        _oriSampleRowNumber = objE._oriSampleRowNumber
        _curIsDelimited = objE._curIsDelimited
        _oldIsDelimited = objE._oldIsDelimited
        _oriIsDelimited = objE._oriIsDelimited
        _curStartingCharacterPos = objE._curStartingCharacterPos
        _oldStartingCharacterPos = objE._oldStartingCharacterPos
        _oriStartingCharacterPos = objE._oriStartingCharacterPos
        _curLenOfStartingCharacter = objE._curLenOfStartingCharacter
        _oldLenOfStartingCharacter = objE._oldLenOfStartingCharacter
        _oriLenOfStartingCharacter = objE._oriLenOfStartingCharacter
        _curSampleRow = objE._curSampleRow
        _oldSampleRow = objE._oldSampleRow
        _oriSampleRow = objE._oriSampleRow
        _curDateSeparator = objE._curDateSeparator
        _oldDateSeparator = objE._oldDateSeparator
        _oriDateSeparator = objE._oriDateSeparator
        _curDateType = objE._curDateType
        _oldDateType = objE._oldDateType
        _oriDateType = objE._oriDateType
        _curIsZeroInDate = objE._curIsZeroInDate
        _oldIsZeroInDate = objE._oldIsZeroInDate
        _oriIsZeroInDate = objE._oriIsZeroInDate
        _curDecimalSeparator = objE._curDecimalSeparator
        _oldDecimalSeparator = objE._oldDecimalSeparator
        _oriDecimalSeparator = objE._oriDecimalSeparator
        _curThousandSeparator = objE._curThousandSeparator
        _oldThousandSeparator = objE._oldThousandSeparator
        _oriThousandSeparator = objE._oriThousandSeparator
        _curIsAmountInDollars = objE._curIsAmountInDollars
        _oldIsAmountInDollars = objE._oldIsAmountInDollars
        _oriIsAmountInDollars = objE._oriIsAmountInDollars
        _curIsAmountInCents = objE._curIsAmountInCents
        _oldIsAmountInCents = objE._oldIsAmountInCents
        _oriIsAmountInCents = objE._oriIsAmountInCents
        _curEnclosureCharacter = objE._curEnclosureCharacter
        _oldEnclosureCharacter = objE._oldEnclosureCharacter
        _oriEnclosureCharacter = objE._oriEnclosureCharacter
        _curDelimiterCharacter = objE._curDelimiterCharacter
        _oldDelimiterCharacter = objE._oldDelimiterCharacter
        _oriDelimiterCharacter = objE._oriDelimiterCharacter
        _curOtherSeparator = objE._curOtherSeparator
        _oldOtherSeparator = objE._oldOtherSeparator
        _oriOtherSeparator = objE._oriOtherSeparator
        _curHeaderRowNumber = objE._curHeaderRowNumber
        _oldHeaderRowNumber = objE._oldHeaderRowNumber
        _oriHeaderRowNumber = objE._oriHeaderRowNumber
        _curTransactionStartRowNumber = objE._curTransactionStartRowNumber
        _oldTransactionStartRowNumber = objE._oldTransactionStartRowNumber
        _oriTransactionStartRowNumber = objE._oriTransactionStartRowNumber
        _curIsHeaderAField = objE._curIsHeaderAField
        _oldIsHeaderAField = objE._oldIsHeaderAField
        _oriIsHeaderAField = objE._oriIsHeaderAField
        _curDuplicateTxnRef1 = objE._curDuplicateTxnRef1
        _oldDuplicateTxnRef1 = objE._oldDuplicateTxnRef1
        _oriDuplicateTxnRef1 = objE._oriDuplicateTxnRef1
        _curDuplicateTxnRef2 = objE._curDuplicateTxnRef2
        _oldDuplicateTxnRef2 = objE._oldDuplicateTxnRef2
        _oriDuplicateTxnRef2 = objE._oriDuplicateTxnRef2
        _curDuplicateTxnRef3 = objE._curDuplicateTxnRef3
        _oldDuplicateTxnRef3 = objE._oldDuplicateTxnRef3
        _oriDuplicateTxnRef3 = objE._oriDuplicateTxnRef3
        _curIsAdviceRecordForEveryRow = objE._curIsAdviceRecordForEveryRow
        _oldIsAdviceRecordForEveryRow = objE._oldIsAdviceRecordForEveryRow
        _oriIsAdviceRecordForEveryRow = objE._oriIsAdviceRecordForEveryRow
        _curIsAdviceRecordAfterCharacter = objE._curIsAdviceRecordAfterCharacter
        _oldIsAdviceRecordAfterCharacter = objE._oldIsAdviceRecordAfterCharacter
        _oriIsAdviceRecordAfterCharacter = objE._oriIsAdviceRecordAfterCharacter
        _curIsAdviceRecordDelimited = objE._curIsAdviceRecordDelimited
        _oldIsAdviceRecordDelimited = objE._oldIsAdviceRecordDelimited
        _oriIsAdviceRecordDelimited = objE._oriIsAdviceRecordDelimited
        _curAdviceRecordCharPos = objE._curAdviceRecordCharPos
        _oldAdviceRecordCharPos = objE._oldAdviceRecordCharPos
        _oriAdviceRecordCharPos = objE._oriAdviceRecordCharPos
        _curAdviceRecordDelimiter = objE._curAdviceRecordDelimiter
        _oldAdviceRecordDelimiter = objE._oldAdviceRecordDelimiter
        _oriAdviceRecordDelimiter = objE._oriAdviceRecordDelimiter
        _curFilters = objE._curFilters
        _curIsFilterConditionAND = objE._curIsFilterConditionAND
        _oldIsFilterConditionAND = objE._oldIsFilterConditionAND
        _oriIsFilterConditionAND = objE._oriIsFilterConditionAND
        _curIsFilterConditionOR = objE._curIsFilterConditionOR
        _oldIsFilterConditionOR = objE._oldIsFilterConditionOR
        _oriIsFilterConditionOR = objE._oriIsFilterConditionOR
        _curMapSourceFields = objE._curMapSourceFields
        _curMapBankFields = objE._curMapBankFields
        _curTranslatorSettings = objE._curTranslatorSettings
        _curEditableSettings = objE._curEditableSettings
        _curLookupSettings = objE._curLookupSettings
        _curCalculatedFields = objE._curCalculatedFields
        _curStringManipulations = objE._curStringManipulations


        _curPColumn = objE._curPColumn
        _oldPColumn = objE._oldPColumn
        _oriPColumn = objE._oriPColumn
        _curPIndicator = objE._curPIndicator
        _oldPIndicator = objE._oldPIndicator
        _oriPIndicator = objE._oriPIndicator
        _curWColumn = objE._curWColumn
        _oldWColumn = objE._oldWColumn
        _oriWColumn = objE._oriWColumn
        _curWIndicator = objE._curWIndicator
        _oldWIndicator = objE._oldWIndicator
        _oriWIndicator = objE._oriWIndicator
        _curIColumn = objE._curIColumn
        _oldIColumn = objE._oldIColumn
        _oriIColumn = objE._oriIColumn
        _curIIndicator = objE._curIIndicator
        _oldIIndicator = objE._oldIIndicator
        _oriIIndicator = objE._oriIIndicator
    End Sub
    ''' <summary>
    ''' Validates the Fields of this template
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Validate()

        ValidationEngine.Validate()

        For Each childFilters As RowFilter In _curFilters
            childFilters.Validate()
        Next

        For Each childMapSourceFields As MapSourceField In _curMapSourceFields
            childMapSourceFields.Validate()
        Next

        For Each childMapBankFields As MapBankField In _curMapBankFields
            childMapBankFields.Validate()
        Next

        For Each childTranslatorSettings As TranslatorSetting In _curTranslatorSettings
            childTranslatorSettings.Validate()
        Next

        For Each childEditableSettings As EditableSetting In _curEditableSettings
            childEditableSettings.Validate()
        Next

        For Each childLookupSettings As LookupSetting In _curLookupSettings
            childLookupSettings.Validate()
        Next

        For Each childCalculatedFields As CalculatedField In _curCalculatedFields
            childCalculatedFields.Validate()
        Next

        For Each childStringManipulations As StringManipulation In _curStringManipulations
            childStringManipulations.Validate()
        Next


    End Sub
    ''' <summary>
    ''' Determines whether the Template Data is valid
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the Template Data is valid</returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property IsValid() As Boolean
        Get
            If Not ValidationEngine.IsValid Then
                Return False
            End If

            Dim childFilters As RowFilter
            For Each childFilters In _curFilters
                If Not childFilters.IsValid Then
                    Return False
                End If
            Next

            For Each childMapSourceFields As MapSourceField In _curMapSourceFields
                If Not childMapSourceFields.IsValid Then
                    Return False
                End If
            Next

            For Each childMapBankFields As MapBankField In _curMapBankFields
                If Not childMapBankFields.IsValid Then
                    Return False
                End If
            Next

            For Each childTranslatorSettings As TranslatorSetting In _curTranslatorSettings
                If Not childTranslatorSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childEditableSettings As EditableSetting In _curEditableSettings
                If Not childEditableSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childLookupSettings As LookupSetting In _curLookupSettings
                If Not childLookupSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childCalculatedFields As CalculatedField In _curCalculatedFields
                If Not childCalculatedFields.IsValid Then
                    Return False
                End If
            Next

            For Each childStringManipulations As StringManipulation In _curStringManipulations
                If Not childStringManipulations.IsValid Then
                    Return False
                End If
            Next

            Return True

        End Get

    End Property

    ''' <summary>
    ''' Saves the template to filename and deletes the filetobedeleted, if given
    ''' </summary>
    ''' <param name="filename">Name of file to save the template</param>
    ''' <param name="filetobedeleted">file to delete upon saving template</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile2(ByVal Filename As String, Optional ByVal filetobedeleted As String = "")

        If IsChild Then
            _isDirty = True
            Throw New Exception("Unable to save child object")
        End If


        Try

            If filetobedeleted.Trim().Length > 0 Then
                If File.Exists(filetobedeleted) Then File.Delete(filetobedeleted)
            End If

            Dim texttemplatelist As New MAGIC.CommonTemplate.TextTemplateList
            Dim fileTemplate As New BTMU.MAGIC.Common.MagicFileTemplate
            Dim serializerDetail As New XmlSerializer(GetType(CommonTemplateTextDetail))
            Dim serializerList As New XmlSerializer(GetType(MAGIC.CommonTemplate.TextTemplateList))
            Dim serializerFileTemplate As New XmlSerializer(GetType(BTMU.MAGIC.Common.MagicFileTemplate))
            Dim stream As New MemoryStream
            Dim streamFile As FileStream = File.Open(Filename, FileMode.Create)

            texttemplatelist.CommonTemplate = CommonTemplateName
            texttemplatelist.OutputTemplate = OutputTemplateName
            texttemplatelist.SourceFile = SourceFileName
            texttemplatelist.IsEnabled = IsEnabled
            texttemplatelist.IsDraft = IsDraft

            serializerDetail.Serialize(stream, Me)
            fileTemplate.DetailValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
            stream.Close()

            stream = New MemoryStream
            serializerList.Serialize(stream, texttemplatelist)
            fileTemplate.ListValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
            stream.Close()

            serializerFileTemplate.Serialize(streamFile, fileTemplate)
            streamFile.Close()
        Catch ex As System.Exception
            _isDirty = True
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Loads the template from the given file
    ''' </summary>
    ''' <param name="filename">template file name</param>
    ''' <returns>Returns the Template loaded</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadFromFile2(ByVal filename As String) As CommonTemplateTextDetail


        Dim serializer As New XmlSerializer(GetType(CommonTemplateTextDetail))
        Dim content As String
        Dim doc As XPathDocument = New XPathDocument(filename)
        Dim nav As XPathNavigator = doc.CreateNavigator()
        Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/DetailValue")
        Dim detailObj As New CommonTemplateTextDetail
        Iterator.MoveNext()
        content = Iterator.Current.Value

        Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
        stream.Position = 0

        Dim xmlSetting As New System.Xml.XmlReaderSettings()
        xmlSetting.IgnoreWhitespace = False
        Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(stream, xmlSetting)

        detailObj = CType(serializer.Deserialize(xmlReader), CommonTemplateTextDetail)
        stream.Close()

        DataContainer = detailObj

        xmlReader.Close()

        'Refresh the following bank field properties
        RefreshBankFields(detailObj)

        Dim normstr As New Text.StringBuilder()

        If detailObj.SampleRow IsNot Nothing Then
            For Each chr As Char In detailObj.SampleRow
                If Asc(chr) = 10 Then
                    normstr.AppendLine()
                Else
                    normstr.Append(chr)
                End If
            Next
            detailObj.SampleRow = normstr.ToString()
        End If

        Return detailObj

    End Function

    Private Shared Sub RefreshBankFields(ByVal objtemplate As ICommonTemplate)

        'Sync these properties of BankFields (Common Template & Master Template)
        '1. DataLength
        '2. DefaultValue
        '3. DataSample
        '4. Mandatory
        '5. Map Separator
        '6. Include Decimal
        '7. Decimal Separator
        '8. Header
        '9. Detail
        '10. Trailer
        Dim _mstTmpUtility As New MasterTemplate.MasterTemplateSharedFunction

        If MasterTemplate.MasterTemplateSharedFunction.IsFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateFixedDetail In objmastertmp.MasterTemplateFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        'bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                        bkfield.DecimalSeparator = _masterFixedDetail.DecimalSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer
                        'bkfield.CarriageReturn = _masterFixedDetail.CarriageReturn

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        ElseIf MasterTemplate.MasterTemplateSharedFunction.IsNonFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateNonFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            'modified by Kay @ 7/6/2011
            'Add for consolidate field
            If objtemplate.MapBankFields.Count = objmastertmp.MasterTemplateNonFixedDetailCollection.Count - 1 Then
                Dim bkfield As New MapBankField
                bkfield.BankField = "Consolidate Field"
                bkfield.DataLength = 255
                bkfield.DataType = "Text"
                Select Case objmastertmp.OutputFormat
                    Case "iFTS-2 MultiLine", "iFTS-2 SingleLine"
                        bkfield.Detail = "Payment"
                    Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM"
                        bkfield.Detail = "Transaction"
                    Case Else
                        bkfield.Detail = "Yes"
                End Select

                objtemplate.MapBankFields.Add(bkfield)

            End If

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateNonFixedDetail In objmastertmp.MasterTemplateNonFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        'bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer
                        'bkfield.CarriageReturn = _masterFixedDetail.CarriageReturn

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        End If

    End Sub

#End Region

#Region "Source Field Population"
    ''' <summary>
    ''' Clears the these settings: Row Filter, BankFields, Translator, Editable Fields, Lookup, Calculated and String Manipulation
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearSourceFieldDependantCollections()
        Filters.Clear()
        MapBankFields.Clear()
        TranslatorSettings.Clear()
        EditableSettings.Clear()
        LookupSettings.Clear()
        CalculatedFields.Clear()
        StringManipulations.Clear()


    End Sub


    Public Sub SetDefaultTranslation()
        OnPropertyChanged("OutputTemplateName")
    End Sub

    ''' <summary>
    ''' Generates an array of Values (Source Values to fill up the Customer Data Table) from the given line of data
    ''' </summary>
    ''' <param name="strOutputFormat">Output file format (from MasterTemplate)</param>
    ''' <remarks></remarks>
    Private Sub GenerateSourceData(ByVal strOutputFormat As String, ByVal NewSrcFilename As String)


        Dim AllTransactionRecords() As String
        Dim names As New List(Of String)
        Dim values As New List(Of String)
        Dim SuccessfullySeparated As Boolean = False
        Dim _decideNameValue As Int32
        Dim CurSourceRecord As DataRow
        Dim intLineNumber As Int32 = 0

        Try
            Dim encoding As System.Text.Encoding = GetFileEncoding(NewSrcFilename)
            AllTransactionRecords = File.ReadAllLines(NewSrcFilename, encoding)
            Dim intLineCount As Int32 = AllTransactionRecords.Length

            Dim counter As Int32
            Dim intTxnStartLine As Int32 = Convert.ToInt32(TransactionStartRowNumber)

            'fixed width input source file format is not applicable for iFTS2 format
            If IsFixedWidthFile Then

                If IsDelimited Then
                    'code to produce array of lines from the delimited transaction file
                    Dim allrecords As New List(Of String)
                    Dim LOCT As Integer = Convert.ToInt32(LenOfStartingCharacter)
                    Dim SCP As Integer = Convert.ToInt32(StartingCharacterPos) - 1
                    Dim transData As String = File.ReadAllText(NewSrcFilename, encoding)

                    If SCP + 1 > transData.Length Then Throw New System.Exception(MsgReader.GetString("E10000001"))
                    If (SCP + 1 + LOCT) > transData.Length Then Throw New System.Exception(MsgReader.GetString("E10000002"))

                    allrecords.Add(transData.Substring(SCP, LOCT))

                    Dim nextPos As Integer = SCP + LOCT

                    While (nextPos + LOCT) <= transData.Length

                        allrecords.Add(transData.Substring(nextPos, LOCT))

                        nextPos += LOCT

                    End While

                    AllTransactionRecords = allrecords.ToArray()
                    intLineCount = AllTransactionRecords.Length
                End If

                'Array of Indices of Delimiter(;)
                Dim delIndices As New List(Of Integer)
                Dim delIndex = 0

                Do

                    delIndex = SampleRow.IndexOf(";", delIndex)
                    If delIndex = -1 Then Exit Do
                    delIndices.Add(delIndex)
                    delIndex = delIndex + 1
                    If delIndex >= SampleRow.Length Then Exit Do

                Loop Until delIndex = -1
                While intLineNumber < intLineCount

                    intLineNumber = intLineNumber + 1
                    If intLineNumber < intTxnStartLine Then Continue While

                    names.Clear()  ' Clear the list for each row
                    values.Clear() ' Clear the list for each row

                    _decideNameValue = Convert.ToInt32(HeaderRowNumber).CompareTo(Convert.ToInt32(intLineNumber))

                    SuccessfullySeparated = SeparateFixedWidthFile(names, values, AllTransactionRecords, _decideNameValue, intLineNumber, delIndices)

                    If Not SuccessfullySeparated Then
                        Throw New System.Exception(MsgReader.GetString("E10000003"))
                    End If

                    ' Check if the total fields in the source file and total fields in commontemplate are the same
                    If names.Count < MapSourceFields.Count Then
                        Throw New UnequalNumberOfFields(MsgReader.GetString("E03000020"))
                    End If

                    'Single Source Record
                    CurSourceRecord = SourceData.NewRow()
                    counter = 0
                    ' Add field values to the datarow
                    For Each col As DataColumn In _SourceData.Columns
                        CurSourceRecord(counter) = values(counter)
                        counter = counter + 1
                    Next

                    'Add each row to datatable
                    _SourceData.Rows.Add(CurSourceRecord)

                End While


            Else 'Non-fixed width source file''''''''''''''''''''''''''''''''''''''''''''

                Dim sDelimiter As String

                Select Case DelimiterCharacter
                    Case "Other"
                        sDelimiter = OtherSeparator
                    Case "<Tab>"
                        sDelimiter = vbTab
                    Case "<Space>"
                        sDelimiter = " "
                    Case Else
                        sDelimiter = DelimiterCharacter
                End Select

                While intLineNumber < intLineCount

                    intLineNumber = intLineNumber + 1

                    If intLineNumber < intTxnStartLine Then Continue While

                    names.Clear()
                    values.Clear()

                    _decideNameValue = Convert.ToInt32(HeaderRowNumber).CompareTo(Convert.ToInt32(intLineNumber))

                    SuccessfullySeparated = SeparateNonFixedWidthFile(names, values, strOutputFormat, sDelimiter, _decideNameValue, intLineNumber, AllTransactionRecords)

                    If Not SuccessfullySeparated Then
                        Throw New System.Exception(MsgReader.GetString("E10000003"))
                    End If

                    ' Check if the total fields in the source file and total fields in commontemplate are the same
                    If names.Count < MapSourceFields.Count Then
                        Throw New UnequalNumberOfFields(MsgReader.GetString("E03000020"))
                    End If

                    'Single Source Record
                    CurSourceRecord = _SourceData.NewRow()

                    ' Add field values to the datarow
                    counter = 0
                    For Each col As DataColumn In _SourceData.Columns
                        CurSourceRecord(counter) = values(counter)
                        counter = counter + 1
                    Next

                    'Add each row to datatable
                    _SourceData.Rows.Add(CurSourceRecord)

                End While

            End If
        Catch ex As UnequalNumberOfFields
            Throw New UnequalNumberOfFields(String.Format("Record: {0}. {1}", intLineNumber, ex.Message))
        Catch ex As Exception
            Throw New Exception("Error reading Source File(GenerateSourceData) : " & ex.Message.ToString)
        End Try



    End Sub

    ''' <summary>
    ''' Generates a list of Source Fields from the source file
    ''' </summary>
    ''' <param name="outputfileformat">Output File format</param>
    ''' <param name="isfixed">indicates whether the Master Template is Fixed or Non-Fixed</param>
    ''' <remarks></remarks>
    Public Sub PopulateSourceFields(ByVal outputfileformat As String, ByVal isfixed As Boolean)

        Dim names As New List(Of String)
        Dim values As New List(Of String)
        Dim SuccessfullySeparated As Boolean = False

        Dim sOutputFileFormat As String = outputfileformat
        Dim encoding As System.Text.Encoding = GetFileEncoding(SourceFileName)
        Dim AllTransactionRecords() As String = File.ReadAllLines(SourceFileName, encoding)
        Dim intLineCount As Int32 = AllTransactionRecords.Length
        Dim _decideNameValue As Int32 = Convert.ToInt32(HeaderRowNumber).CompareTo(Convert.ToInt32(SampleRowNumber))

        ''fixed width input source file format is not applicable for iFTS2 format
        If IsFixedWidthFile Then

            If SampleRow.Trim() = "" Then Exit Sub
            If SampleRow.LastIndexOf(";") = -1 Then Exit Sub

            'Array of Indices of Delimiter(;)
            Dim delIndices As New List(Of Integer)
            Dim delIndex = 0

            Do

                delIndex = SampleRow.IndexOf(";", delIndex)
                If delIndex = -1 Then Exit Do
                delIndices.Add(delIndex)
                delIndex = delIndex + 1
                If delIndex >= SampleRow.Length Then Exit Do

            Loop Until delIndex = -1


            SuccessfullySeparated = SeparateFixedWidthFile(names, values, AllTransactionRecords, _decideNameValue, SampleRowNumber, delIndices)
            If Not SuccessfullySeparated Then Exit Sub

        Else 'Non-fixed width source file'''''''''
            SuccessfullySeparated = SeparateNonFixedWidthFile(names, values, sOutputFileFormat, DelimiterCharacter, _decideNameValue, SampleRowNumber, AllTransactionRecords)
            If Not SuccessfullySeparated Then Exit Sub
        End If

        MapSourceFields.Clear()
        Dim counter As Integer
        For counter = 0 To names.Count - 1
            Dim _objMapsrcField As New MapSourceField()
            _objMapsrcField.SourceFieldName = names.Item(counter)
            _objMapsrcField.SourceFieldValue = values.Item(counter)
            MapSourceFields.Add(_objMapsrcField)
        Next

    End Sub

#Region "Delimited Field Names and Values"
    Private Function IsDelimitedValueFound(ByVal sCustomerFieldValue As String _
                                , ByVal sDelimiter As String) As Boolean

        Return System.Text.RegularExpressions.Regex.IsMatch(sCustomerFieldValue _
                          , System.Text.RegularExpressions.Regex.Escape(sDelimiter))
    End Function

    Private Sub ExtractDelimitedFieldValues(ByVal sCustomerFieldValue As String _
                    , ByVal sDelimiter As String, ByVal values As List(Of String))

        sCustomerFieldValue = IIf(sCustomerFieldValue(sCustomerFieldValue.Length - 1) = sDelimiter _
                                      , sCustomerFieldValue, sCustomerFieldValue & sDelimiter)

        Dim matchedValues As System.Text.RegularExpressions.Match

        ' Field values could possibly be enclosed by ' or "" and are separated by given delimiters.
        ' Mind to escape the delimiter, if its possibly one of the built-in reg exp reserved symbols
        matchedValues = System.Text.RegularExpressions.Regex.Match(sCustomerFieldValue _
                            , "("".*?""" & System.Text.RegularExpressions.Regex.Escape(sDelimiter) _
                            & "?)|('.*?'" & System.Text.RegularExpressions.Regex.Escape(sDelimiter) & _
                            "?)|(.*?" & System.Text.RegularExpressions.Regex.Escape(sDelimiter) & ")")

        Dim tmp As String
        While matchedValues.Success
            'strip off the ' or "" and the delimiter
            tmp = System.Text.RegularExpressions.Regex.Replace(matchedValues.Value, System.Text.RegularExpressions.Regex.Escape(sDelimiter) & "$", "")
            tmp = System.Text.RegularExpressions.Regex.Replace(tmp, "^['""]", "")
            tmp = System.Text.RegularExpressions.Regex.Replace(tmp, "['""]$", "")
            values.Add(tmp)
            matchedValues = matchedValues.NextMatch()
        End While

    End Sub

#End Region

#Region "Enclosed Field Names and Values"

    Private Function IsEnclosedValueFound(ByVal sCustomerFieldValue As String _
                            , ByVal sEnclosureCharacter As String) As Boolean

        Return System.Text.RegularExpressions.Regex.IsMatch(sCustomerFieldValue _
                    , System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                    & ".*?" & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter))

    End Function

    Private Sub ExtractEnclosedFieldNames(ByVal sCustomerFieldName As String _
                , ByVal sEnclosureCharacter As String, ByVal names As List(Of String))

        Dim matchedNames As System.Text.RegularExpressions.Match
        matchedNames = System.Text.RegularExpressions.Regex.Match(sCustomerFieldName _
                            , System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                            & ".*?" & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter))

        Dim tmp As String
        Dim counter As Int32 = 1

        While matchedNames.Success

            'Strip off the enclosure character from value
            tmp = System.Text.RegularExpressions.Regex.Replace(matchedNames.Value, "^" _
                                & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                                                , "")
            tmp = System.Text.RegularExpressions.Regex.Replace(tmp, _
                                System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) & "$", "")

            If tmp Is Nothing Or tmp.Trim() = "" Then
                tmp = String.Format("Field {0}", counter)
            End If

            names.Add(tmp)

            matchedNames = matchedNames.NextMatch()
            counter += 1
        End While


    End Sub

    Private Sub ExtractEnclosedFieldValues(ByVal sCustomerFieldValue As String _
                    , ByVal sEnclosureCharacter As String, ByVal values As List(Of String))

        Dim matchedValues As System.Text.RegularExpressions.Match
        matchedValues = System.Text.RegularExpressions.Regex.Match(sCustomerFieldValue _
                            , System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                            & ".*?" & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter))

        Dim tmp As String
        While matchedValues.Success

            'Strip off the enclosure character from value
            tmp = System.Text.RegularExpressions.Regex.Replace(matchedValues.Value, "^" _
            & System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) _
                                                , "")
            tmp = System.Text.RegularExpressions.Regex.Replace(tmp, _
                    System.Text.RegularExpressions.Regex.Escape(sEnclosureCharacter) & "$", "")
            values.Add(tmp)

            matchedValues = matchedValues.NextMatch()

        End While

    End Sub

#End Region

    Private Sub getCustomerFieldNamesAndFieldValues(ByRef sCustomerFieldName As String _
                    , ByRef sCustomerFieldValue As String, ByVal sOutputFileFormat As String _
                 , ByVal _decideNameValue As Integer, ByVal LineNumber As Integer, ByVal AllTransactionLines() As String)

        Dim counter As Int32 = 0
        Dim sText As String

        sCustomerFieldValue = String.Empty
        sCustomerFieldName = String.Empty

        If Convert.ToInt32(HeaderRowNumber) <> 0 Then
            counter = 1

            For Each sText In AllTransactionLines

                Select Case _decideNameValue
                    Case -1
                        If counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                        ElseIf counter = CInt(LineNumber) Then
                            sCustomerFieldValue = sText
                            Exit For
                        End If
                    Case 0
                        If counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                            sCustomerFieldValue = sText
                            Exit For
                        End If
                    Case 1
                        If counter = Convert.ToInt32(LineNumber) Then
                            sCustomerFieldValue = sText
                        ElseIf counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                            Exit For
                        End If
                End Select
                counter = counter + 1
            Next

        Else
            'Read File
            counter = 1

            For Each sText In AllTransactionLines

                If counter = Convert.ToInt32(LineNumber) Then
                    sCustomerFieldValue = sText
                    Exit For
                End If
                counter = counter + 1
            Next

        End If
    End Sub

    Private Function SeparateNonFixedWidthFile(ByVal names As List(Of String), ByVal values As List(Of String) _
                                    , ByVal sOutputFileFormat As String, ByVal sDelimiter As String _
                                    , ByVal _decideNameValue As Integer, ByVal intLineNumber As Integer _
                                    , ByVal AllTransactionRecords() As String) As Boolean

        Dim sCustomerFieldName As String = String.Empty
        Dim sCustomerFieldValue As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim found As Boolean = False
        Dim counter As Int32

        'get Customer Field Names and Field Values
        getCustomerFieldNamesAndFieldValues(sCustomerFieldName, sCustomerFieldValue, sOutputFileFormat, _decideNameValue, intLineNumber, AllTransactionRecords)

        If sCustomerFieldName = String.Empty AndAlso sCustomerFieldValue = String.Empty Then
            Return False
        End If

        '#1 - Check if there is a value (could be enclosed in either ' or "") delimited by given delimiter
        found = IsDelimitedValueFound(sCustomerFieldValue, sDelimiter)

        If Not found Then Throw New MagicException(String.Format("Record: {0} 'Delimiter' is not found.", intLineNumber))

        If EnclosureCharacter <> "None" Then 'Field Names and Values are enclosed

            '#1 - Check if there exists a value enclosed by given enclosure character
            sEnclosureCharacter = IIf(EnclosureCharacter = "Single Quote(')", "'", """")

            'found = IsEnclosedValueFound(sCustomerFieldValue, sEnclosureCharacter)
            'If Not found Then Return False

            '#2 - Extract enclosed field values
            values.AddRange(HelperModule.Split(sCustomerFieldValue, sDelimiter, sEnclosureCharacter, True))
            If IsHeaderAField AndAlso Convert.ToInt32(HeaderRowNumber) <> 0 Then
                '#3 -  Extract enclosed field names  
                names.AddRange(HelperModule.Split(sCustomerFieldName, sDelimiter, sEnclosureCharacter, True))
                HelperModule.FormatMapSourceFields(names)
            Else
                '#3 -  Auto populate (such as field 1, field 2, field 3, ...) as number of fields as values
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If

        Else ' Field names and values are not enclosed but delimited. Field values are delimited and may've optionally been enclosed in ' or " 


            '#2 - Extract field values (could be enclosed in either ' or "") delimited by given delimiter 
            values.AddRange(HelperModule.Split(sCustomerFieldValue, sDelimiter, Nothing, True))

            If IsHeaderAField AndAlso Convert.ToInt32(HeaderRowNumber) <> 0 Then
                '#3 -  Extract field names delimited by given delimiter
                names.AddRange(HelperModule.Split(sCustomerFieldName, sDelimiter, Nothing, True))
                HelperModule.FormatMapSourceFields(names)
            Else
                '#3 -  Auto populate (such as field 1, field 2, field 3, ...) as number of fields as values
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If

        End If

        If names.Count <> values.Count Then Throw New UnequalNumberOfFields(" Header and Sample Rows dont have equal number of Fields!")

        Return True

    End Function

    Private Function SeparateFixedWidthFile(ByVal names As List(Of String), ByVal values As List(Of String) _
        , ByVal AllLines() As String, ByVal _decideNameValue As Int32 _
        , ByVal LineNumber As Integer, ByVal delIndices As List(Of Integer)) As Boolean

        Dim sCustomerFieldName As String = String.Empty
        Dim sCustomerFieldValue As String = String.Empty

        Dim counter As Int32 = 0

        If IsCarriageReturn Then

            counter = 1

            For Each sText As String In AllLines

                Select Case _decideNameValue
                    Case -1
                        If counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                        ElseIf counter = CInt(LineNumber) Then
                            sCustomerFieldValue = sText
                            Exit For
                        End If
                    Case 0
                        If counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                            sCustomerFieldValue = sText
                            Exit For
                        End If
                    Case 1
                        If counter = Convert.ToInt32(LineNumber) Then
                            sCustomerFieldValue = sText
                        ElseIf counter = Convert.ToInt32(HeaderRowNumber) Then
                            sCustomerFieldName = sText
                            Exit For
                        End If
                End Select
                counter = counter + 1
            Next
            'update customer fieldname
            If Not IsNothingOrEmptyString(sCustomerFieldName) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldName.Length Then
                        sCustomerFieldName = sCustomerFieldName.Insert(delIndices(indexCounter), ";")
                    End If
                Next
            End If
            'update customer fieldvalue

            If Not IsNothingOrEmptyString(sCustomerFieldValue) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldValue.Length Then
                        sCustomerFieldValue = sCustomerFieldValue.Insert(delIndices(indexCounter), ";")
                    End If
                Next
            End If

            Dim _values() As String = HelperModule.Split(sCustomerFieldValue, ";", Nothing, True)
            If _values IsNot Nothing Then
                For Each value As String In _values
                    values.Add(value)
                Next
            End If
            If IsHeaderAField AndAlso Convert.ToInt32(HeaderRowNumber) <> 0 Then

                names.AddRange(HelperModule.Split(sCustomerFieldName, ";", Nothing, True))
                HelperModule.FormatMapSourceFields(names)

            Else

                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next

            End If

        ElseIf IsDelimited Then

            sCustomerFieldName = AllLines(LineNumber - 1)
            sCustomerFieldValue = sCustomerFieldName

            'update customer fieldname
            If Not IsNothingOrEmptyString(sCustomerFieldName) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldName.Length Then
                        sCustomerFieldName = sCustomerFieldName.Insert(delIndices(indexCounter), ";")
                    End If
                Next
            End If
            'update customer fieldvalue

            If Not IsNothingOrEmptyString(sCustomerFieldValue) Then
                For indexCounter As Integer = 0 To delIndices.Count - 1
                    If delIndices(indexCounter) < sCustomerFieldValue.Length Then
                        sCustomerFieldValue = sCustomerFieldValue.Insert(delIndices(indexCounter), ";")
                    End If
                Next
            End If

            Dim _values() As String = HelperModule.Split(sCustomerFieldValue, ";", Nothing, True)
            If _values IsNot Nothing Then
                For Each value As String In _values
                    values.Add(value)
                Next
            End If

            If IsHeaderAField AndAlso Convert.ToInt32(StartingCharacterPos) > 1 Then

                names.AddRange(HelperModule.Split(sCustomerFieldName, ";", Nothing, True))
                HelperModule.FormatMapSourceFields(names)

            Else
                For counter = 1 To values.Count
                    names.Add(String.Format("Field {0}", counter))
                Next
            End If


        End If

        If names.Count <> values.Count Then Throw New UnequalNumberOfFields(" Header and Sample Rows dont have equal number of Fields!")

        Return True

    End Function

    ''' <summary>
    ''' Retrieves a Sample Row of Transaction Record from the given Source File
    ''' </summary>
    ''' <param name="srcFilename">Name of Source File</param>
    ''' <param name="outputfileformat">Output File Format</param>
    ''' <remarks></remarks>
    Public Sub RetrieveSampleRow(ByVal srcFilename As String, ByVal outputfileformat As String)

        Dim lineDataFound As Boolean = False

        Dim counter As Int32 = 1
        Dim lineText As String = ""

        Dim file As System.IO.StreamReader = Nothing

        Try
            Dim encoding As System.Text.Encoding = GetFileEncoding(srcFilename)
            file = New System.IO.StreamReader(srcFilename, encoding)


            If IsCarriageReturn Then

                If SampleRowNumber = "" Then Exit Sub

                While Not file.EndOfStream

                    lineText = file.ReadLine()
                    If Convert.ToInt32(SampleRowNumber) = counter Then
                        lineDataFound = True
                        Exit While
                    End If

                    counter = counter + 1

                End While

            ElseIf IsDelimited Then
                lineDataFound = True
                lineText = file.ReadLine()
                If Convert.ToInt32(StartingCharacterPos) >= lineText.Length Then Throw New System.Exception("Start Character Position is falling outside the range of Sample Row")

                If (Convert.ToInt32(StartingCharacterPos) + Convert.ToInt32(LenOfStartingCharacter)) > lineText.Length Then Throw New System.Exception("Length of Character Transaction is falling outside the range of Sample Row")

                lineText = lineText.Substring(Convert.ToInt32(StartingCharacterPos) _
                                , Convert.ToInt32(LenOfStartingCharacter))
            End If

        Catch ex As Exception
            Throw New System.Exception(ex.Message)
        Finally
            If Not (file Is Nothing) Then
                file.Close()
            End If
        End Try

        If lineDataFound Then
            SampleRow = lineText
        Else
            SampleRow = ""
        End If

        SourceFileName = srcFilename

    End Sub

#End Region

    Private Sub ApplyReferenceFieldSetting(ByVal fileFormat As String)

        Try
            Dim line As Integer = 0
            Dim record As String = ""
            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(fileFormat) Then
                record = "Payment Record"
            Else
                record = "Record"
            End If
            Dim strGroupByColumns As String = String.Format("{0},{1},{2}" _
                                        , DuplicateTxnRef1, DuplicateTxnRef2, DuplicateTxnRef3).Trim(",")

            Dim _intRecordCount As Int32 = _PreviewSourceData.Rows.Count

            If _intRecordCount = 0 Or strGroupByColumns = "" Then Exit Sub


            Dim _strFilter As String = ""
            Dim _dtviewTxnFieldGroup As DataView

            For _counter As Int32 = 0 To _intRecordCount - 1

                _strFilter = "1=1"

                If DuplicateTxnRef1 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef1), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef1)))
                If DuplicateTxnRef2 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef2), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef2)))
                If DuplicateTxnRef3 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef3), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef3)))

                _dtviewTxnFieldGroup = _PreviewSourceData.DefaultView
                _dtviewTxnFieldGroup.RowFilter = _strFilter

                If _dtviewTxnFieldGroup.Count > 1 Then
                    Dim stbError As New System.Text.StringBuilder
                    For Each dv As DataRowView In _dtviewTxnFieldGroup
                        If record.Equals("Payment Record", StringComparison.InvariantCultureIgnoreCase) Then
                            line = _PreviewSourceData.Rows.IndexOf(dv.Row) + 1
                        Else
                            line = _PreviewSourceData.Rows.IndexOf(dv.Row) + (Convert.ToInt32(TransactionStartRowNumber) - 1) + 1
                        End If

                        If DuplicateTxnRef1 <> "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005020") _
                                           , line, DuplicateTxnRef1, DuplicateTxnRef2, DuplicateTxnRef3, record))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005030") _
                                           , line, DuplicateTxnRef1, DuplicateTxnRef2, record))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005040") _
                                            , line, DuplicateTxnRef1, DuplicateTxnRef3, record))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005050") _
                                           , line, DuplicateTxnRef2, DuplicateTxnRef3, record))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005060") _
                                           , line, DuplicateTxnRef1, record))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005070") _
                                           , line, DuplicateTxnRef2, record))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005080") _
                                                                   , line, DuplicateTxnRef3, record))

                        End If
                        _dtviewTxnFieldGroup.RowFilter = String.Empty
                    Next
                    If stbError.ToString <> String.Empty Then
                        _dtviewTxnFieldGroup.RowFilter = String.Empty
                        Throw New Exception(stbError.ToString())
                    End If

                End If
            Next
        Catch ex As Exception
            Throw New Exception(String.Format(MsgReader.GetString("E10000000"), ex.Message))
        End Try

    End Sub

    Public Sub DuplicateCheck(Optional ByVal fileFormat As String = "") Implements ICommonTemplate.DuplicateCheck
        ApplyReferenceFieldSetting(fileFormat)
    End Sub

    ''' <summary>
    ''' Prepares the Template for previewing
    ''' </summary>
    ''' <param name="strOutputFileFormat">Name of Output File format</param>
    ''' <param name="SourceFile">Name of source file</param>
    ''' <param name="WorksheetName">Name of work sheet</param>
    ''' <param name="TransactionEndRow">Row number of last transaction record</param>
    ''' <param name="RemoveRowsFromEnd">The number of rows to be removed from the very last record in the Source File</param>
    ''' <remarks></remarks>
    Public Sub PrepareForPreview(Optional ByVal strOutputFileFormat As String = "" _
                                , Optional ByVal SourceFile As String = "" _
                                , Optional ByVal WorksheetName As String = "" _
                                , Optional ByVal TransactionEndRow As Integer = 0 _
                                , Optional ByVal RemoveRowsFromEnd As Integer = 0 _
                    ) Implements ICommonTemplate.PrepareForPreview

        'outputfileformat is referenced in applyreferencefieldsetting/applyrowfilter functions
        _outputfileformat = strOutputFileFormat

        '#1. Prepare SourceData
        _SourceData = New DataTable("SourceData")

        If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) Then
            GenerateSourceTransactionSet(strOutputFileFormat, SourceFile, RemoveRowsFromEnd)
        Else
            
            For Each SourceField As MapSourceField In MapSourceFields
                _SourceData.Columns.Add(New DataColumn(SourceField.SourceFieldName, GetType(String)))
            Next

            '#2. Copy Source Fields
            If SourceFile = "" Then 'Preview For Sample Record

                Dim CurSourceRecord As DataRow = _SourceData.NewRow()
                For Each SourceField As MapSourceField In MapSourceFields
                    CurSourceRecord(SourceField.SourceFieldName) = SourceField.SourceFieldValue
                Next

                _SourceData.Rows.Add(CurSourceRecord)

            Else 'Preview for Conversion

                GenerateSourceData(strOutputFileFormat, SourceFile)
                If RemoveRowsFromEnd > 0 Then
                    If RemoveRowsFromEnd > _SourceData.Rows.Count Then Throw New MagicException(String.Format(MsgReader.GetString("E10000004"), _SourceData.Rows.Count))

                    'remove the removes from end
                    For _deletecounter As Integer = 1 To RemoveRowsFromEnd
                        _SourceData.Rows.RemoveAt(_SourceData.Rows.Count - 1)
                    Next

                End If

            End If
        End If

        Try
            Dim errorPrefix As String = "Record"
            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) Then
                _SourceData = _tblPaymentSrc.Copy()
                errorPrefix = "Payment Record"
            End If

            'If "iFTS-2 MultiLine".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) _
            '    OrElse "Mr.Omakase India".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) Then
            '    _SourceData = _tblPaymentSrc.Copy()
            'End If

            '#3. Apply Reference Field Setting
            'If _SourceData.Rows.Count > 1 Then ApplyReferenceFieldSetting(strOutputFileFormat)

            '#4. Apply Filters on Source Data
            _FilteredSourceData = _SourceData.DefaultView
            _FilteredSourceData.RowFilter = ""
            If Filters.Count > 0 Then ApplyRowFilters()

            If _FilteredSourceData.Count = 0 Then Throw New System.Exception(MsgReader.GetString("E10000005"))

            '#5 Format Date Values to Bank Field Date Format
            FormatDateValuesToBankFieldDateFormat(errorPrefix)

            '#6 Format Numeric Values to Bank Field Number Format
            FormatNumberValuesToBankFieldNumberFormat(errorPrefix)



        Catch ex As Exception
            Throw New MagicException(ex.Message)
        Finally

            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) AndAlso _SourceData.Rows.Count > 0 Then

                _tblPaymentSrc = _SourceData.Copy()
                Dim strFilter As String = String.Empty

                For Each _row As DataRow In _tblPaymentSrc.Rows
                    strFilter &= IIf(strFilter = String.Empty, "", ",") & _row("ID").ToString()
                Next
                If Not IsNothingOrEmptyString(strFilter) Then strFilter = "ID in (" & strFilter & ")"

                Dim dv As DataView = _tblTaxSrc.DefaultView
                dv.RowFilter = strFilter
                _tblTaxSrc = dv.ToTable()

                dv = _tblInvoiceSrc.DefaultView
                dv.RowFilter = strFilter
                _tblInvoiceSrc = dv.ToTable()

                _SourceData = New DataTable("SourceData")
                _SourceData = _tblTaxSrc.Copy()
                FormatDateValuesToBankFieldDateFormat("Tax Record")
                FormatNumberValuesToBankFieldNumberFormat("Tax Record")
                _tblTaxSrc = _SourceData.Copy()
                _SourceData = New DataTable("SourceData")
                _SourceData = _tblInvoiceSrc.Copy()
                FormatDateValuesToBankFieldDateFormat("Invoice Record")
                FormatNumberValuesToBankFieldNumberFormat("Invoice Record")
                _tblInvoiceSrc = _SourceData.Copy()
            End If
            'If "iFTS-2 MultiLine".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) _
            'OrElse "Mr.Omakase India".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) Then
            '    _tblPaymentSrc = _SourceData.Copy()
            '    Dim strFilter As String = String.Empty

            '    For Each _row As DataRow In _tblPaymentSrc.Rows
            '        strFilter &= IIf(strFilter = String.Empty, "", ",") & _row("ID").ToString()
            '    Next
            '    strFilter = "ID in (" & strFilter & ")"

            '    Dim dv As DataView = _tblTaxSrc.DefaultView
            '    dv.RowFilter = strFilter
            '    _tblTaxSrc = dv.ToTable()

            '    dv = _tblInvoiceSrc.DefaultView
            '    dv.RowFilter = strFilter
            '    _tblInvoiceSrc = dv.ToTable()

            'End If
        End Try

    End Sub

    Private Sub ApplyRowFilters()
         
        Try
            '#4.1. '''Apply Date/Number Setting here''''''''''
            Dim origCulture = System.Threading.Thread.CurrentThread.CurrentCulture
            Dim newCulture As New System.Globalization.CultureInfo("en-US", True)
            newCulture.NumberFormat.NumberDecimalSeparator = IIf(DecimalSeparator = "No Separator", ".", DecimalSeparator)
            newCulture.NumberFormat.NumberGroupSeparator = IIf(ThousandSeparator = "No Separator", "", ThousandSeparator)
            newCulture.DateTimeFormat.ShortDatePattern = GetDateFormatPattern(DateSeparator, DateType)
            newCulture.DateTimeFormat.LongDatePattern = newCulture.DateTimeFormat.ShortDatePattern
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

            '#4.2. '''Rename  Columns referred in the Row Filter and add a column for each of the "Filter Type"s
            ' Make sure there is no duplicate column added for the duplicate filters

            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) > -1 AndAlso _SourceData.Columns.IndexOf(_flt.FilterField & "_0") > -1 Then Continue For

                If _flt.FilterType.Equals("Numeric", StringComparison.InvariantCultureIgnoreCase) Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.Decimal))
                ElseIf _flt.FilterType.Equals("Date", StringComparison.InvariantCultureIgnoreCase) Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.DateTime))

                    Dim temp1 As DateTime
                    Dim temp2 As DateTime

                    If _flt.FilterValue Is Nothing Then
                    Else
                        Dim arDate As String() = System.Text.RegularExpressions.Regex.Split(_flt.FilterValue, "\|\|")

                        If arDate.Length > 0 AndAlso HelperModule.IsDateDataType(arDate(0), Me.DateSeparator, Me.DateType, temp1, Me.IsZeroInDate) Then
                            _flt.setTempDateString(temp1.ToString("yyyy/MM/dd"))
                        End If

                        If arDate.Length > 1 AndAlso HelperModule.IsDateDataType(arDate(1), Me.DateSeparator, Me.DateType, temp2, Me.IsZeroInDate) Then
                            _flt.setTempDateString(temp1.ToString("yyyy/MM/dd") & "||" & temp2.ToString("yyyy/MM/dd"))
                        End If
                    End If
                End If
            Next


            '#4.3 ''' Convert and Copy values to Columns referred in Row Filter 
            ' a null value is assumed in the place where data conversion fails
            For Each row As DataRow In _SourceData.Rows

                For Each _flt As RowFilter In Filters
                    Select Case _flt.FilterType.ToUpper()
                        Case "NUMERIC"

                            'If Decimal.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
                            If Decimal.TryParse(IIf(row(_flt.FilterField & "_0") Is DBNull.Value, "", row(_flt.FilterField & "_0")), Nothing) Then
                                row(_flt.FilterField) = Convert.ToDecimal(row(_flt.FilterField & "_0"))
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                        Case "DATE"
                            Dim temp As DateTime

                            'If HelperModule.IsDateDataType(row(_flt.FilterField & "_0"), Me.DateSeparator, Me.DateType, temp, Me.IsZeroInDate) Then
                            If HelperModule.IsDateDataType(IIf(row(_flt.FilterField & "_0") Is DBNull.Value, "", row(_flt.FilterField & "_0")), Me.DateSeparator, Me.DateType, temp, Me.IsZeroInDate) Then
                                row(_flt.FilterField) = temp
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                    End Select
                Next

            Next

            '#4.4. '''Filter the rows
            Dim shxtTableView As DataView
            shxtTableView = _SourceData.DefaultView
            Dim test As String
            test = Filters.ConditionString(IIf(IsFilterConditionAND, "AND", "OR"))

            shxtTableView.RowFilter = test '"  1 = 1 AND  ([Inv Date] is null) " 'test ' "  1 = 1 AND  ( [Inv Date] = '' ) "
            '#4.5. '''Remove Date/Number Setting '''''''''''''''''''''''''''''''''''''
            System.Threading.Thread.CurrentThread.CurrentCulture = origCulture

            '#4.6. '''Remove expression columns
            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) = -1 Or _SourceData.Columns.IndexOf(_flt.FilterField & "_0") = -1 Then Continue For

                _SourceData.Columns.Remove(_flt.FilterField)
                _SourceData.Columns(_flt.FilterField & "_0").ColumnName = _flt.FilterField

            Next

            'Just to please the bluddy DataView/DataTable data update behaviour!!!
            Dim tmpData As DataTable
            tmpData = shxtTableView.ToTable()
            shxtTableView.Dispose()
            _SourceData.Dispose()
            _SourceData = tmpData
            _FilteredSourceData = _SourceData.DefaultView

        Catch ex As Exception

            Throw New Exception(String.Format(MsgReader.GetString("E10000006"), ex.Message.ToString))

        End Try

    End Sub

    Private Sub FormatDateValuesToBankFieldDateFormat(ByVal ErrorPrefix As String)

        'a. Date Values in the Source Fields must comply to the date type specified in the Form <Retrieve Tab>
        'b. Convert the Date value to a date format as specified in the master template 'Date Format' property and update the Source Data Table

        Dim _convertedDateValue As DateTime
        Dim _dtMpSrcField As MapSourceField
        Dim _strBankFieldDateFormat As String
        


        Dim rowNo As Integer = Convert.ToInt32(TransactionStartRowNumber) - 1
        If Not ErrorPrefix.Equals("Record", StringComparison.InvariantCultureIgnoreCase) Then rowNo = 0
        For Each row As DataRow In _SourceData.Rows

            For Each col As DataColumn In _SourceData.Columns

                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = col.ColumnName
                If IsNothingOrEmptyString(row(col)) Then Continue For

                _dtMpSrcField.SourceFieldValue = row(col).ToString()

                'Convert Date value from the date format of Source File to that of Master Template
                'if the data type of bankfield that this field is mapped to is a DateTime 
                _strBankFieldDateFormat = GetBankFieldDateFormatForSourceField(_dtMpSrcField)
                If _strBankFieldDateFormat = "" Then Continue For

                If Not (Common.HelperModule.IsDateDataType(row(col).ToString(), DateSeparator, DateType, _convertedDateValue, IsZeroInDate)) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005100"), rowNo + 1, col.ColumnName, ErrorPrefix))

                'validate the source date format before converting it to master template date format
                row(col) = _convertedDateValue.ToString(_strBankFieldDateFormat, HelperModule.enUSCulture)

            Next
            rowNo += 1
        Next

    End Sub

    Private Sub FormatNumberValuesToBankFieldNumberFormat(ByVal ErrorPrefix As String)

        'a. Numeric Values in the Source Fields must comply to the 'Number Setting' specified in the Form <Retrieve Tab>
        'b. Convert the Numeric Values in the source fields to a Number format  that uses . as decimal separator and , as group separator 

        Dim _convertedNumberValue As Decimal
        Dim _dtMpSrcField As MapSourceField
        Dim rowNo As Integer = Convert.ToInt32(TransactionStartRowNumber) - 1
        If Not ErrorPrefix.Equals("Record", StringComparison.InvariantCultureIgnoreCase) Then rowNo = 0
        For Each row As DataRow In _SourceData.Rows

            For Each col As DataColumn In _SourceData.Columns

                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = col.ColumnName

                If IsNothingOrEmptyString(row(col)) Then Continue For

                _dtMpSrcField.SourceFieldValue = row(col).ToString()


                'Convert Number value from the Number format of Source File to that of Master Template
                'if the data type of bankfield that this field is mapped to is a Number or Currency 
                If Not IsMappedBankFieldDataTypeNumber(_dtMpSrcField) Then Continue For

                'Unless there is a value, no need to validate its value against numeric format
                If HelperModule.IsNothingOrEmptyString(row(col).ToString()) Then Continue For

                'Prefix the Negative Number Symbol '-', in case its found suffixed at the value
                If row(col).ToString().EndsWith("-") Then row(col) = "-" & row(col).ToString().Substring(0, row(col).Length - 1)

                If Not Common.HelperModule.IsDecimalDataType(row(col).ToString(), DecimalSeparator, ThousandSeparator, _convertedNumberValue) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005110"), rowNo + 1, col.ColumnName, ErrorPrefix))

                row(col) = _convertedNumberValue

            Next
            rowNo += 1
        Next

    End Sub

    Private Function GetBankFieldDateFormatForSourceField(ByVal SrcField As MapSourceField) As String

        For Each _bnkField As MapBankField In MapBankFields

            If _bnkField.DataType Is Nothing Then Continue For

            If Not _bnkField.DataType.Equals("DateTime", StringComparison.InvariantCultureIgnoreCase) Then Continue For

            If _bnkField.MappedSourceFields.IsFound(SrcField) Then Return _bnkField.DateFormat.Replace("D", "d").Replace("Y", "y")

        Next

        Return ""

    End Function

    Private Function IsMappedBankFieldDataTypeNumber(ByVal SrcField As MapSourceField) As Boolean

        For Each _bnkField As MapBankField In MapBankFields
            If _bnkField.DataType Is Nothing Then Continue For
            If Not (_bnkField.DataType.ToUpper() = "NUMBER" Or _bnkField.DataType.ToUpper() = "CURRENCY") Then Continue For

            If _bnkField.MappedSourceFields.IsFound(SrcField) Then Return True

        Next

        Return False

    End Function

    ''' <summary>
    ''' This property is to Get Filtered Source Data
    ''' </summary>
    ''' <value>DataView</value>
    ''' <returns>Returns a Data Table of Records after applying Row Filter Conditions</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property FilteredSourceData() As System.Data.DataView Implements ICommonTemplate.FilteredSourceData
        Get
            Return _FilteredSourceData
        End Get
    End Property

    ''' <summary>
    ''' This property is to Get Source Data
    ''' </summary>
    ''' <value>DataTable</value>
    ''' <returns>Returns a Data Table of Transaction Records from Source File</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SourceData() As System.Data.DataTable Implements ICommonTemplate.SourceData
        Get
            Return _SourceData
        End Get
    End Property

    Public Property PreviewSourceData() As System.Data.DataTable Implements ICommonTemplate.PreviewSourceData
        Get
            Return _PreviewSourceData
        End Get
        Set(ByVal value As System.Data.DataTable)
            _PreviewSourceData = value
        End Set
    End Property

#Region " Methods used for formats with Record Types "
    ''' <summary>
    ''' This function is used only be formats using Record Type Indicators.
    ''' This function is used to get one set of transaction based on the sample row selected
    ''' from the Sample Source File.
    ''' </summary>
    ''' <returns>One set of transaction as a string</returns>
    ''' <remarks></remarks>
    Public Function PopulateSampleTransactionSet() As String
        Dim lineText As String
        Dim counter As Integer = 0
        Dim transactionText As New System.Text.StringBuilder
        Dim sDelimiter As String = ""
        Dim found As Boolean = False
        Dim values As New List(Of String)
        Dim isPFound As Boolean = False
        Dim isWFound As Boolean = False
        Dim isIFound As Boolean = False
        Dim filestrm As FileStream = Nothing
        Dim file As System.IO.StreamReader = Nothing
        Dim intPColumn As Integer = 0
        Dim intWColumn As Nullable(Of Integer)
        Dim intIColumn As Nullable(Of Integer)
        Dim currentRecordType As String = ""
        Dim lineValue As New List(Of String)

        ' Step 1 : Get the column index for the P-Column, W-Column, I-Column
        intPColumn = PColumn - 1
        If Not IsNothingOrEmptyString(WColumn) Then intWColumn = WColumn - 1
        If Not IsNothingOrEmptyString(IColumn) Then intIColumn = IColumn - 1

        ' Step 1 : Get the delimiter used to separate the fields
        Select Case DelimiterCharacter
            Case "Other"
                sDelimiter = OtherSeparator
            Case "<Tab>"
                sDelimiter = vbTab
            Case "<Space>"
                sDelimiter = " "
            Case Else
                sDelimiter = DelimiterCharacter
        End Select

        ' Step 2 : Read the sample source file
        Dim encoding As System.Text.Encoding = GetFileEncoding(SourceFileName)
        filestrm = New System.IO.FileStream(SourceFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        file = New System.IO.StreamReader(filestrm, encoding)

        Try
            ' Step 3 : Initialize the variables
            isPFound = False
            isWFound = False
            isIFound = False

            ' Step 4 : Loop through each line in the source file
            While Not file.EndOfStream
                ' Step 4.1 : Read one line from the source file
                lineText = file.ReadLine()

                ' Step 4.2 : Check if the specified delimiter is found in the line
                found = IsDelimitedValueFound(lineText, sDelimiter)
                If Not found Then Throw New MagicException(MsgReader.GetString("E10000057"))

                ' Step 4.2 : Initialise the list which holds the line values after splitting the line with delimiter
                values = New List(Of String)

                If counter < Convert.ToInt32(SampleRowNumber) - 1 Then
                    counter += 1
                    Continue While
                End If

                ' Step 4.4 : Check if there is a valid Record Type Indicator
                If intWColumn.HasValue AndAlso intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf intWColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf Not (intWColumn.HasValue) AndAlso Not (intIColumn.HasValue) Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                End If

                    ' Step 4.3 : Check if the line is the sample row selected
                If Convert.ToInt32(SampleRowNumber) - 1 = counter Then
                    ' Step 4.3.1 : Check if the Sample Row is a P-record
                    If currentRecordType = PIndicator Then
                        If Not isPFound Then
                            transactionText.AppendLine(lineText)
                            isPFound = True
                        End If
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNumber))
                    End If
                ElseIf counter > Convert.ToInt32(SampleRowNumber) - 1 Then
                    If Not isPFound Then Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNumber))
                    ' Step 4.3.2 : Get the first W-record of the sample P-record, if any,
                    If currentRecordType = WIndicator Then
                        If Not isWFound Then
                            transactionText.AppendLine(lineText)
                            isWFound = True
                        End If
                    ElseIf currentRecordType = IIndicator Then
                        If Not isPFound Then Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNumber))
                        If Not isIFound Then
                            transactionText.AppendLine(lineText)
                            isIFound = True
                        End If
                    ElseIf currentRecordType = PIndicator Then
                        Exit While
                    End If
                End If
                counter = counter + 1
            End While
        Catch ex As MagicException
            Throw New Exception(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        Finally
            If Not (file Is Nothing) Then file.Close()
            If Not (filestrm Is Nothing) Then filestrm.Close()
        End Try
        Return transactionText.ToString
    End Function

    ''' <summary>
    ''' This function is used only by formats using Record Type Indicators.
    ''' This function is used to check the record type of a line from the source file
    ''' </summary>
    ''' <param name="lineText">One line from the source file</param>
    ''' <param name="recordType">The Record Type to check</param>
    ''' <param name="sDelimiter">Delimiter used to separate the fields in the line</param>
    ''' <param name="values">List which holds the field values after splitting the line with delimiter</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function CheckRecordTypeOfLine(ByVal lineText As String, ByVal recordType As String, ByVal sDelimiter As String, ByRef values As List(Of String)) As Boolean
        Dim sEnclosureCharacter As String
        Dim columnIndex As Integer = 0
        Dim found As Boolean = False
        Dim indicator As String = ""
        Try
            values.Clear()
            ' Step 1 : Check if there exists a value enclosed by given enclosure character
            If EnclosureCharacter <> "None" Then 'Field Names and Values are enclosed
                sEnclosureCharacter = IIf(EnclosureCharacter = "Single Quote(')", "'", """")
                found = IsEnclosedValueFound(lineText, sEnclosureCharacter)

                ' Step 1.1 : Extract enclosed field values
                values.AddRange(HelperModule.Split(lineText, sDelimiter, sEnclosureCharacter, True))
            Else
                ' Step 1.2 : Extract field values (could be enclosed in either ' or "") delimited by given delimiter 
                values.AddRange(HelperModule.Split(lineText, sDelimiter, Nothing, True))
            End If

            ' Step 3 : Get the column index and indicator for the record type
            Select Case recordType
                Case PIndicator
                    columnIndex = Convert.ToInt32(PColumn)
                    indicator = PIndicator
                Case WIndicator
                    columnIndex = Convert.ToInt32(WColumn)
                    indicator = WIndicator
                Case IIndicator
                    columnIndex = Convert.ToInt32(IColumn)
                    indicator = IIndicator
            End Select

            ' Step 4 : Column Index must be less than the total field count
            If values.Count > columnIndex Then
                If values(columnIndex - 1).Equals(indicator, StringComparison.InvariantCultureIgnoreCase) Then
                    Return True
                Else
                    Return False
                End If
            Else
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), recordType, columnIndex))
            End If

        Catch ex As MagicException
            Throw New Exception(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        End Try
        Return False
    End Function

    ''' <summary>
    ''' This function is used only by formats using Record Type Indicators.
    ''' This is used to generate the name and value list for the sample transaction from the sample source file.
    ''' </summary>
    ''' <param name="filename">The Sample Source File</param>
    ''' <param name="names">List which holds the field names</param>
    ''' <param name="values">List which holds the field values</param>
    ''' <param name="sOutputFileFormat">Output File Format Name</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function SeparateNonFixedWidthFileWithRecordTypes(ByVal filename As String, ByVal names As List(Of String), ByVal values As List(Of String) _
                                , ByVal sOutputFileFormat As String) As Boolean

        Dim sCustomerFieldName As String = String.Empty
        Dim sCustomerFieldValue As String = String.Empty
        Dim found As Boolean = False
        Dim counter As Int32
        Dim isPFound As Boolean = False
        Dim isWFound As Boolean = False
        Dim isIFound As Boolean = False
        Dim filestrm As FileStream = Nothing
        Dim file As System.IO.StreamReader = Nothing
        Dim lineText As String
        Dim lineValue As New List(Of String)
        Dim intPColumn As Integer = 0
        Dim intWColumn As Nullable(Of Integer)
        Dim intIColumn As Nullable(Of Integer)
        Dim currentRecordType As String = ""

        ' Step 1 : Get the column index for the P-Column, W-Column, I-Column
        intPColumn = PColumn
        If Not IsNothingOrEmptyString(WColumn) Then intWColumn = WColumn - 1
        If Not IsNothingOrEmptyString(IColumn) Then intIColumn = IColumn - 1

        ' Step 1 : Get the delimiter used to separate the fields in a line
        Dim sDelimiter As String
        Select Case DelimiterCharacter
            Case "Other"
                sDelimiter = OtherSeparator
            Case "<Tab>"
                sDelimiter = vbTab
            Case "<Space>"
                sDelimiter = " "
            Case Else
                sDelimiter = DelimiterCharacter
        End Select

        ' Step 2 : Read the file
        Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
        filestrm = New System.IO.FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        file = New System.IO.StreamReader(filestrm, encoding)
        Try
            ' Step 3 : Initialise the variables
            isPFound = False
            isWFound = False
            isIFound = False

            ' Step 4 : Loop through each line in the file
            While Not file.EndOfStream
                ' Step 4.1 : Read one line
                lineText = file.ReadLine()

                ' Step 4.2 : Check if the specified delimiter exists in the line
                found = IsDelimitedValueFound(lineText, sDelimiter)
                If Not found Then Throw New MagicException(MsgReader.GetString("E10000057"))

                ' Step 4.3 : Initialise the list to hold the field values
                lineValue = New List(Of String)

                If counter < Convert.ToInt32(TransactionStartRowNumber) - 1 Then
                    counter += 1
                    Continue While
                End If

                If intWColumn.HasValue AndAlso intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf intWColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(lineText, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                ElseIf Not (intWColumn.HasValue) AndAlso Not (intIColumn.HasValue) Then
                    If CheckRecordTypeOfLine(lineText, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), counter + 1))
                    End If
                End If

                ' Step 4.5 : Check if the Sample Row is a valid P-record
                If Convert.ToInt32(SampleRowNumber) - 1 = counter Then
                    If currentRecordType = PIndicator Then
                        If Not isPFound Then
                            'Get the name and value
                            For counter = 1 To lineValue.Count
                                names.Add(String.Format("P-Field{0}", counter))
                            Next
                            values.AddRange(lineValue)
                            isPFound = True
                        End If
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNumber))
                    End If
                ElseIf counter > Convert.ToInt32(SampleRowNumber) - 1 Then
                    If Not isPFound Then Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNumber))
                    If currentRecordType = WIndicator Then
                        If Not isWFound Then
                            'Get the name and value
                            For counter = 1 To lineValue.Count
                                names.Add(String.Format("W-Field{0}", counter))
                            Next
                            values.AddRange(lineValue)
                            isWFound = True
                        End If
                    ElseIf currentRecordType = IIndicator Then
                        If Not isIFound Then
                            'Get the name and value
                            For counter = 1 To lineValue.Count
                                names.Add(String.Format("I-Field{0}", counter))
                            Next
                            values.AddRange(lineValue)
                            isIFound = True
                        End If
                    ElseIf currentRecordType = PIndicator Then
                        Exit While
                    End If
                End If
                counter = counter + 1
            End While
        Catch ex As MagicException
            Throw New Exception(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        End Try
        Return True

    End Function

    Private Sub GenerateSourceTransactionSet(ByVal strOutputFormat As String, ByVal NewSrcFilename As String, ByVal RemoveRowsFromEnd As Integer)
        Dim AllTransactionRecords() As String
        Dim tmpRecords() As String = {}
        Dim names As New List(Of String)
        Dim values As New List(Of String)
        Dim SuccessfullySeparated As Boolean = False
        Dim CurSourceRecord As DataRow
        Dim intLineNumber As Int32 = 0
        Dim sDelimiter As String
        Dim counter As Int32
        Dim found As Boolean = False
        Dim lineValue As New List(Of String)
        Dim isPFound As Boolean = False
        Dim isWFound As Boolean = False
        Dim isIFound As Boolean = False
        Dim PNo As Integer = 0
        Dim intPColumn As Integer = 0
        Dim intWColumn As Nullable(Of Integer)
        Dim intIColumn As Nullable(Of Integer)
        Dim currentRecordType As String = ""

        Try
            ' Step 1 : Get the column index for the P-Column, W-Column, I-Column
            intPColumn = PColumn
            If Not IsNothingOrEmptyString(WColumn) Then intWColumn = WColumn - 1
            If Not IsNothingOrEmptyString(IColumn) Then intIColumn = IColumn - 1

            _tblPaymentSrc = New DataTable("Payment")
            _tblTaxSrc = New DataTable("PaymentTax")
            _tblInvoiceSrc = New DataTable("PaymentInvoice")

            _tblPaymentSrc.Columns.Add("ID", GetType(String))
            _tblTaxSrc.Columns.Add("ID", GetType(String))
            _tblInvoiceSrc.Columns.Add("ID", GetType(String))


            Dim encoding As System.Text.Encoding = GetFileEncoding(NewSrcFilename)
            AllTransactionRecords = File.ReadAllLines(NewSrcFilename, encoding)

            'remove the removes from end
            If RemoveRowsFromEnd >= AllTransactionRecords.Length Then
                Throw New MagicException("System failed to remove records from the source file as 'Remove Rows from End' exceeds or empties the records in source file.Please fill in Remove Rows From End again.")
            Else
                ReDim Preserve AllTransactionRecords(AllTransactionRecords.Length - RemoveRowsFromEnd - 1)
            End If
           

            Dim intLineCount As Int32 = AllTransactionRecords.Length
            Dim intTxnStartLine As Int32 = Convert.ToInt32(TransactionStartRowNumber)

            Select Case DelimiterCharacter
                Case "Other"
                    sDelimiter = OtherSeparator
                Case "<Tab>"
                    sDelimiter = vbTab
                Case "<Space>"
                    sDelimiter = " "
                Case Else
                    sDelimiter = DelimiterCharacter
            End Select

            isPFound = False
            isWFound = False
            isIFound = False
            ' While intLineNumber < intLineCount


            For Each line As String In AllTransactionRecords
                intLineNumber = intLineNumber + 1
                If intLineNumber < intTxnStartLine Then Continue For
                names.Clear()
                values.Clear()

                found = IsDelimitedValueFound(line, sDelimiter)
                If Not found Then Throw New MagicException(MsgReader.GetString("E10000057"))

                lineValue.Clear()

                If intWColumn.HasValue AndAlso intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(line, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(line, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    ElseIf CheckRecordTypeOfLine(line, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), intLineNumber))
                    End If
                ElseIf intWColumn.HasValue Then
                    If CheckRecordTypeOfLine(line, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(line, WIndicator, sDelimiter, lineValue) Then
                        currentRecordType = WIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), intLineNumber))
                    End If
                ElseIf intIColumn.HasValue Then
                    If CheckRecordTypeOfLine(line, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    ElseIf CheckRecordTypeOfLine(line, IIndicator, sDelimiter, lineValue) Then
                        currentRecordType = IIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), intLineNumber))
                    End If
                ElseIf Not (intWColumn.HasValue) AndAlso Not (intIColumn.HasValue) Then
                    If CheckRecordTypeOfLine(line, PIndicator, sDelimiter, lineValue) Then
                        currentRecordType = PIndicator
                    Else
                        Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), intLineNumber))
                    End If
                End If

                Select Case currentRecordType
                    Case PIndicator
                        If Not isPFound Then
                            'Create columns for the datatable
                            For intI As Integer = 0 To lineValue.Count - 1
                                _tblPaymentSrc.Columns.Add(String.Format("P-Field{0}", intI + 1), GetType(String))
                            Next
                        Else
                            If lineValue.Count + 1 <> _tblPaymentSrc.Columns.Count Then Throw New Exception(MsgReader.GetString("E03000020"))
                        End If
                        'Single Source Record
                        CurSourceRecord = _tblPaymentSrc.NewRow()
                        ' Add field values to the datarow
                        PNo += 1
                        CurSourceRecord(0) = PNo
                        counter = 0
                        For intI As Integer = 0 To lineValue.Count - 1
                            CurSourceRecord(intI + 1) = lineValue(intI)
                            counter = counter + 1
                        Next
                        'Add each row to datatable
                        _tblPaymentSrc.Rows.Add(CurSourceRecord)
                        isPFound = True
                    Case WIndicator
                        If Not isPFound Then Throw New Exception(MsgReader.GetString("E02010130"))
                        If Not isWFound Then
                            'Create columns for the datatable
                            For intI As Integer = 0 To lineValue.Count - 1
                                _tblTaxSrc.Columns.Add(String.Format("W-Field{0}", intI + 1), GetType(String))
                            Next
                        Else
                            If lineValue.Count + 1 <> _tblTaxSrc.Columns.Count Then Throw New Exception(MsgReader.GetString("E03000020"))
                        End If
                        'Single Source Record
                        CurSourceRecord = _tblTaxSrc.NewRow()
                        ' Add field values to the datarow
                        CurSourceRecord(0) = PNo
                        counter = 0
                        For intI As Integer = 0 To lineValue.Count - 1
                            CurSourceRecord(intI + 1) = lineValue(intI)
                            counter = counter + 1
                        Next
                        'Add each row to datatable
                        _tblTaxSrc.Rows.Add(CurSourceRecord)
                        isWFound = True
                    Case IIndicator
                        If Not isPFound Then Throw New Exception(MsgReader.GetString("E02010130"))
                        If Not isIFound Then
                            'Create columns for the datatable
                            For intI As Integer = 0 To lineValue.Count - 1
                                _tblInvoiceSrc.Columns.Add(String.Format("I-Field{0}", intI + 1), GetType(String))
                            Next
                        Else
                            If lineValue.Count + 1 <> _tblInvoiceSrc.Columns.Count Then Throw New UnequalNumberOfFields(MsgReader.GetString("E03000020"))

                        End If
                        'Single Source Record
                        CurSourceRecord = _tblInvoiceSrc.NewRow()
                        ' Add field values to the datarow
                        CurSourceRecord(0) = PNo
                        counter = 0
                        For intI As Integer = 0 To lineValue.Count - 1
                            CurSourceRecord(intI + 1) = lineValue(intI)
                            counter = counter + 1
                        Next
                        'Add each row to datatable
                        _tblInvoiceSrc.Rows.Add(CurSourceRecord)
                        isIFound = True
                End Select
                counter = counter + 1
            Next

            'End While

        Catch ex As UnequalNumberOfFields
            Throw New UnequalNumberOfFields(String.Format("Record: {0} - {1}", intLineNumber, ex.Message))
        Catch ex As Exception
            Throw New Exception("Error reading Source File : " & ex.Message.ToString)
        End Try
    End Sub
#End Region

End Class

''' <summary>
''' Collection of Common Transaction Template for Text File
''' </summary>
''' <remarks></remarks>
Partial Public Class CommonTemplateTextDetailCollection
    Inherits BusinessBaseCollection(Of CommonTemplateTextDetail)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of CommonTemplateTextDetail)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of CommonTemplateTextDetail)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of CommonTemplateTextDetail)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As CommonTemplateTextDetail
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("CommonTemplateTextDetailCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "CommonTemplateTextDetail"
            child = New CommonTemplateTextDetail
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub

    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As CommonTemplateTextDetail
        writer.WriteStartElement("CommonTemplateTextDetailCollection")
        For Each child In Me
            writer.WriteStartElement("CommonTemplateTextDetail")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub

End Class
