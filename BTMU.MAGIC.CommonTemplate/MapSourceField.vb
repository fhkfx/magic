Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Source Field
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class MapSourceField
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curSequenceNumber As Nullable(Of Integer)
    Private _oldSequenceNumber As Nullable(Of Integer)
    Private _oriSequenceNumber As Nullable(Of Integer)
    Private _curSourceFieldName As String
    Private _oldSourceFieldName As String
    Private _oriSourceFieldName As String
    Private _curSourceFieldValue As String
    Private _oldSourceFieldValue As String
    Private _oriSourceFieldValue As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This property is to Get/Set Sequence Number of Fields
    ''' </summary>
    ''' <value>Sequence Number</value>
    ''' <returns>Returns Sequence Number of Source Field</returns>
    ''' <remarks></remarks>
    Public Property SequenceNumber() As Nullable(Of Integer)
        Get
            Return _curSequenceNumber
        End Get
        Set(ByVal value As Nullable(Of Integer))

            If _curSequenceNumber.HasValue And value.HasValue Then
                If _curSequenceNumber.Value <> value.Value Then
                    _curSequenceNumber = value
                    OnPropertyChanged("SequenceNumber")
                End If
            ElseIf Not _curSequenceNumber.HasValue And Not value.HasValue Then
                'Do nothing
            Else
                _curSequenceNumber = value
                OnPropertyChanged("SequenceNumber")
            End If

        End Set

    End Property
    ''' <summary>
    ''' This property is to Get/Set Source Field Name
    ''' </summary>
    ''' <value> Source Field Name</value>
    ''' <returns>Returns  Source Field Name</returns>
    ''' <remarks></remarks>
    Public Property SourceFieldName() As String
        Get
            Return _curSourceFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSourceFieldName <> value Then
                _curSourceFieldName = value
                OnPropertyChanged("SourceFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Source Field Value
    ''' </summary>
    ''' <value> Source Field Value</value>
    ''' <returns>Returns  Source Field Value</returns>
    ''' <remarks></remarks>
    Public Property SourceFieldValue() As String
        Get
            Return _curSourceFieldValue
        End Get
        Set(ByVal value As String)
            'If Not value Is Nothing Then value = value.Trim()

            If _curSourceFieldValue <> value Then
                _curSourceFieldValue = value
                OnPropertyChanged("SourceFieldValue")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Applies changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriSequenceNumber = _curSequenceNumber
        _oriSourceFieldName = _curSourceFieldName
        _oriSourceFieldValue = _curSourceFieldValue
    End Sub
    ''' <summary>
    ''' Begins making changes to the object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldSequenceNumber = _curSequenceNumber
        _oldSourceFieldName = _curSourceFieldName
        _oldSourceFieldValue = _curSourceFieldValue
    End Sub
    ''' <summary>
    ''' Cancels changes made to the object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curSequenceNumber = _oldSequenceNumber
        _curSourceFieldName = _oldSourceFieldName
        _curSourceFieldValue = _oldSourceFieldValue
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curSequenceNumber = ReadXMLElement(reader, "_curSequenceNumber")
            _oldSequenceNumber = ReadXMLElement(reader, "_oldSequenceNumber")
            _oriSequenceNumber = ReadXMLElement(reader, "_oriSequenceNumber")
            _curSourceFieldName = ReadXMLElement(reader, "_curSourceFieldName")
            _oldSourceFieldName = ReadXMLElement(reader, "_oldSourceFieldName")
            _oriSourceFieldName = ReadXMLElement(reader, "_oriSourceFieldName")
            _curSourceFieldValue = ReadXMLElement(reader, "_curSourceFieldValue")
            _oldSourceFieldValue = ReadXMLElement(reader, "_oldSourceFieldValue")
            _oriSourceFieldValue = ReadXMLElement(reader, "_oriSourceFieldValue")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curSequenceNumber", _curSequenceNumber)
        WriteXmlElement(writer, "_oldSequenceNumber", _oldSequenceNumber)
        WriteXmlElement(writer, "_oriSequenceNumber", _oriSequenceNumber)
        WriteXmlElement(writer, "_curSourceFieldName", _curSourceFieldName)
        WriteXmlElement(writer, "_oldSourceFieldName", _oldSourceFieldName)
        WriteXmlElement(writer, "_oriSourceFieldName", _oriSourceFieldName)
        WriteXmlElement(writer, "_curSourceFieldValue", _curSourceFieldValue)
        WriteXmlElement(writer, "_oldSourceFieldValue", _oldSourceFieldValue)
        WriteXmlElement(writer, "_oriSourceFieldValue", _oriSourceFieldValue)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(MapSourceField))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the object from the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As MapSourceField
        Dim serializer As New XmlSerializer(GetType(MapSourceField))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), MapSourceField)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As MapSourceField = DirectCast(obj, MapSourceField)
        _curSequenceNumber = objE._curSequenceNumber
        _oldSequenceNumber = objE._oldSequenceNumber
        _oriSequenceNumber = objE._oriSequenceNumber
        _curSourceFieldName = objE._curSourceFieldName
        _oldSourceFieldName = objE._oldSourceFieldName
        _oriSourceFieldName = objE._oriSourceFieldName
        _curSourceFieldValue = objE._curSourceFieldValue
        _oldSourceFieldValue = objE._oldSourceFieldValue
        _oriSourceFieldValue = objE._oriSourceFieldValue
    End Sub
#End Region

    ''' <summary>
    '''  Duplicates the Source File Object
    ''' </summary>
    ''' <param name="dupSourceField">Given Source Field Object</param>
    ''' <remarks></remarks>
    Public Sub CopyTo(ByRef dupSourceField As MapSourceField)

        dupSourceField._curSequenceNumber = _curSequenceNumber
        dupSourceField._oldSequenceNumber = _oldSequenceNumber
        dupSourceField._oriSequenceNumber = _oriSequenceNumber
        dupSourceField._curSourceFieldName = _curSourceFieldName
        dupSourceField._oldSourceFieldName = _oldSourceFieldName
        dupSourceField._oriSourceFieldName = _oriSourceFieldName
        dupSourceField._curSourceFieldValue = _curSourceFieldValue
        dupSourceField._oldSourceFieldValue = _oldSourceFieldValue
        dupSourceField._oriSourceFieldValue = _oriSourceFieldValue
        dupSourceField._curCreatedBy = _curCreatedBy
        dupSourceField._curCreatedDate = _curCreatedDate
        dupSourceField._curModifiedBy = _curModifiedBy
        dupSourceField._curModifiedDate = _curModifiedDate
        dupSourceField._oriModifiedBy = _oriModifiedBy
        dupSourceField._oriModifiedDate = _oriModifiedDate

    End Sub


End Class

''' <summary>
''' Collection of Map Source Fields
''' </summary>
''' <remarks></remarks>
Partial Public Class MapSourceFieldCollection
    Inherits BusinessBaseCollection(Of MapSourceField)
    Implements System.Xml.Serialization.IXmlSerializable
    ''' <summary>
    ''' Determines whether the given Source Field is found in this collection
    ''' </summary>
    ''' <param name="objMapSrcFld">Given Source field</param>
    ''' <returns>Returns a boolean value indicating whether the source field is found in this collection</returns>
    ''' <remarks></remarks>
    Public Function IsFound(ByVal objMapSrcFld As MapSourceField) As Boolean

        Return IsFound(objMapSrcFld.SourceFieldName)

    End Function

    ''' <summary>
    ''' Determines whether the given Source Field is found in this collection
    ''' </summary>
    ''' <param name="objMapSrcFldName">Given Source field</param>
    ''' <returns>Returns a boolean value indicating whether the source field is found in this collection</returns>
    ''' <remarks></remarks>
    Public Function IsFound(ByVal objMapSrcFldName As String) As Boolean

        For Each srcfield As MapSourceField In Items
            If (objMapSrcFldName = srcfield.SourceFieldName) Then Return True
        Next

        Return False

    End Function
    ''' <summary>
    ''' Updates the given Source Field value
    ''' </summary>
    ''' <param name="objMapSrcField">Source Field</param>
    ''' <remarks></remarks>
    Public Sub UpdateSourceValue(ByVal objMapSrcField As MapSourceField)

        For counter As Int32 = 0 To Items.Count - 1
            If (objMapSrcField.SourceFieldName = Item(counter).SourceFieldName) Then
                Item(counter).SourceFieldValue = objMapSrcField.SourceFieldValue
                Exit Sub
            End If
        Next

    End Sub
    ''' <summary>
    ''' Removes the Given source field from the collection
    ''' </summary>
    ''' <param name="objMapSrcFld">Source Field</param>
    ''' <returns>Returns a boolean vlaue indicating whether the Source Field is removed from this collection </returns>
    ''' <remarks></remarks>
    Public Overloads Function Remove(ByVal objMapSrcFld As MapSourceField) As Boolean

        Dim canRemove = False
        Dim position As Int32 = 0

        For Each srcfield As MapSourceField In Items

            If objMapSrcFld.SourceFieldName = srcfield.SourceFieldName Then '_
                ' And objMapSrcFld.SourceFieldValue = srcfield.SourceFieldValue) Then
                canRemove = True
                Exit For
            End If

            position = position + 1
        Next

        If canRemove Then
            Items.RemoveAt(position)
        End If

        Return canRemove

    End Function


    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of MapSourceField)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of MapSourceField)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of MapSourceField)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As MapSourceField
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("MapSourceFieldCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "MapSourceField"
            child = New MapSourceField
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As MapSourceField
        writer.WriteStartElement("MapSourceFieldCollection")
        For Each child In Me
            writer.WriteStartElement("MapSourceField")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class

 