Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' This class encapsulates a Calculated Bank Field in a Common Transaction Template
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class CalculatedField
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curBankFieldName As String
    Private _oldBankFieldName As String
    Private _oriBankFieldName As String
    Private _curOperand1 As String
    Private _oldOperand1 As String
    Private _oriOperand1 As String
    Private _curOperator1 As String
    Private _oldOperator1 As String
    Private _oriOperator1 As String
    Private _curOperand2 As String
    Private _oldOperand2 As String
    Private _oriOperand2 As String
    Private _curOperator2 As String
    Private _oldOperator2 As String
    Private _oriOperator2 As String
    Private _curOperand3 As String
    Private _oldOperand3 As String
    Private _oriOperand3 As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This Property is to Get/Set Name of Bank Field
    ''' </summary>
    ''' <value>Name of Bank Field</value>
    ''' <returns>Returns Name of Bank Field</returns>
    ''' <remarks></remarks>
    Public Property BankFieldName() As String
        Get
            Return _curBankFieldName
        End Get
        Set(ByVal value As String)

            If Not value Is Nothing Then value = value.Trim()

            If _curBankFieldName <> value Then
                _curBankFieldName = value
                OnPropertyChanged("BankFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the First Operand(Source Field)
    ''' </summary>
    ''' <value>Source Field</value>
    ''' <returns>Returns Name of Source Field</returns>
    ''' <remarks></remarks>
    Public Property Operand1() As String
        Get
            Return _curOperand1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOperand1 <> value Then
                _curOperand1 = value
                OnPropertyChanged("Operand1")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the First Operator(either + or -) 
    ''' </summary>
    ''' <value>First Operator</value>
    ''' <returns>Returns the Operator1</returns>
    ''' <remarks></remarks>
    Public Property Operator1() As String
        Get
            Return _curOperator1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOperator1 <> value Then
                _curOperator1 = value
                OnPropertyChanged("Operator1")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Second Operand(Source Field)
    ''' </summary>
    ''' <value>Name of Source Field</value>
    ''' <returns>Returns the Name of Source Field</returns>
    ''' <remarks></remarks>
    Public Property Operand2() As String
        Get
            Return _curOperand2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOperand2 <> value Then
                _curOperand2 = value
                OnPropertyChanged("Operand2")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Second Operator(either +, - or None)  
    ''' </summary>
    ''' <value>Second Operator</value>
    ''' <returns>Returns the Operator2</returns>
    ''' <remarks></remarks>
    Public Property Operator2() As String
        Get
            Return _curOperator2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOperator2 <> value Then
                _curOperator2 = value
                OnPropertyChanged("Operator2")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Third Operand (Souce Field) that is optional 
    ''' </summary>
    ''' <value>Third Operand</value>
    ''' <returns>Returns the Value of Operand3</returns>
    ''' <remarks></remarks>
    Public Property Operand3() As String
        Get
            Return _curOperand3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOperand3 <> value Then
                _curOperand3 = value
                OnPropertyChanged("Operand3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the User that created the Calculated Field
    ''' </summary>
    ''' <value>User ID</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Created Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Created Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that modified the Calculated Field
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Modified Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Modified Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that originally modified the Calculated Field
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Original Modified Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Original Modified Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Apply the changes
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriBankFieldName = _curBankFieldName
        _oriOperand1 = _curOperand1
        _oriOperator1 = _curOperator1
        _oriOperand2 = _curOperand2
        _oriOperator2 = _curOperator2
        _oriOperand3 = _curOperand3
    End Sub
    ''' <summary>
    ''' Begin making changes to the fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldBankFieldName = _curBankFieldName
        _oldOperand1 = _curOperand1
        _oldOperator1 = _curOperator1
        _oldOperand2 = _curOperand2
        _oldOperator2 = _curOperator2
        _oldOperand3 = _curOperand3
    End Sub
    ''' <summary>
    ''' Cancel changes made to the fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curBankFieldName = _oldBankFieldName
        _curOperand1 = _oldOperand1
        _curOperator1 = _oldOperator1
        _curOperand2 = _oldOperand2
        _curOperator2 = _oldOperator2
        _curOperand3 = _oldOperand3
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankFieldName = ReadXMLElement(reader, "_curBankFieldName")
            _oldBankFieldName = ReadXMLElement(reader, "_oldBankFieldName")
            _oriBankFieldName = ReadXMLElement(reader, "_oriBankFieldName")
            _curOperand1 = ReadXMLElement(reader, "_curOperand1")
            _oldOperand1 = ReadXMLElement(reader, "_oldOperand1")
            _oriOperand1 = ReadXMLElement(reader, "_oriOperand1")
            _curOperator1 = ReadXMLElement(reader, "_curOperator1")
            _oldOperator1 = ReadXMLElement(reader, "_oldOperator1")
            _oriOperator1 = ReadXMLElement(reader, "_oriOperator1")
            _curOperand2 = ReadXMLElement(reader, "_curOperand2")
            _oldOperand2 = ReadXMLElement(reader, "_oldOperand2")
            _oriOperand2 = ReadXMLElement(reader, "_oriOperand2")
            _curOperator2 = ReadXMLElement(reader, "_curOperator2")
            _oldOperator2 = ReadXMLElement(reader, "_oldOperator2")
            _oriOperator2 = ReadXMLElement(reader, "_oriOperator2")
            _curOperand3 = ReadXMLElement(reader, "_curOperand3")
            _oldOperand3 = ReadXMLElement(reader, "_oldOperand3")
            _oriOperand3 = ReadXMLElement(reader, "_oriOperand3")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankFieldName", _curBankFieldName)
        WriteXmlElement(writer, "_oldBankFieldName", _oldBankFieldName)
        WriteXmlElement(writer, "_oriBankFieldName", _oriBankFieldName)
        WriteXmlElement(writer, "_curOperand1", _curOperand1)
        WriteXmlElement(writer, "_oldOperand1", _oldOperand1)
        WriteXmlElement(writer, "_oriOperand1", _oriOperand1)
        WriteXmlElement(writer, "_curOperator1", _curOperator1)
        WriteXmlElement(writer, "_oldOperator1", _oldOperator1)
        WriteXmlElement(writer, "_oriOperator1", _oriOperator1)
        WriteXmlElement(writer, "_curOperand2", _curOperand2)
        WriteXmlElement(writer, "_oldOperand2", _oldOperand2)
        WriteXmlElement(writer, "_oriOperand2", _oriOperand2)
        WriteXmlElement(writer, "_curOperator2", _curOperator2)
        WriteXmlElement(writer, "_oldOperator2", _oldOperator2)
        WriteXmlElement(writer, "_oriOperator2", _oriOperator2)
        WriteXmlElement(writer, "_curOperand3", _curOperand3)
        WriteXmlElement(writer, "_oldOperand3", _oldOperand3)
        WriteXmlElement(writer, "_oriOperand3", _oriOperand3)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the Calculated Field to given file
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(CalculatedField))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads Calculated Field from given file
    ''' </summary>
    ''' <param name="filename">Name of file</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As CalculatedField
        Dim serializer As New XmlSerializer(GetType(CalculatedField))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), CalculatedField)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As CalculatedField = DirectCast(obj, CalculatedField)
        _curBankFieldName = objE._curBankFieldName
        _oldBankFieldName = objE._oldBankFieldName
        _oriBankFieldName = objE._oriBankFieldName
        _curOperand1 = objE._curOperand1
        _oldOperand1 = objE._oldOperand1
        _oriOperand1 = objE._oriOperand1
        _curOperator1 = objE._curOperator1
        _oldOperator1 = objE._oldOperator1
        _oriOperator1 = objE._oriOperator1
        _curOperand2 = objE._curOperand2
        _oldOperand2 = objE._oldOperand2
        _oriOperand2 = objE._oriOperand2
        _curOperator2 = objE._curOperator2
        _oldOperator2 = objE._oldOperator2
        _oriOperator2 = objE._oriOperator2
        _curOperand3 = objE._curOperand3
        _oldOperand3 = objE._oldOperand3
        _oriOperand3 = objE._oriOperand3
    End Sub
#End Region
End Class
''' <summary>
''' Collection Class for CalculatedField
''' </summary>
''' <remarks></remarks>
Partial Public Class CalculatedFieldCollection
    Inherits BusinessBaseCollection(Of CalculatedField)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of CalculatedField)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of CalculatedField)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of CalculatedField)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As CalculatedField
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("CalculatedFieldCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "CalculatedField"
            child = New CalculatedField
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub

    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As CalculatedField
        writer.WriteStartElement("CalculatedFieldCollection")
        For Each child In Me
            writer.WriteStartElement("CalculatedField")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
