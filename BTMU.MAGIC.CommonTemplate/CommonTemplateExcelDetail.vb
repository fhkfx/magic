Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports System.Xml.XPath
Imports BTMU.MAGIC.Common
Imports System.Data.OleDb
Imports System.Data

''' <summary>
''' This class encapsulates a Common Transaction Template for Excel file
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class CommonTemplateExcelDetail
    Inherits BTMU.MAGIC.Common.BusinessBase
    Implements ICommonTemplate
    Public Sub New()
        Initialize()
        DataContainer = Me
    End Sub

    <NonSerialized()> Public Shared DataContainer As CommonTemplateExcelDetail
    <NonSerialized()> Private _SourceData As DataTable
    <NonSerialized()> Private _PreviewSourceData As DataTable
    <NonSerialized()> Private _FilteredSourceData As DataView
    <NonSerialized()> Public _tblPaymentSrc As DataTable
    <NonSerialized()> Public _tblTaxSrc As DataTable
    <NonSerialized()> Public _tblInvoiceSrc As DataTable
    <NonSerialized()> Private _outputfileformat As String


#Region " Private Fields "
    Private _curIsDraft As Boolean
    Private _oldIsDraft As Boolean
    Private _oriIsDraft As Boolean
    Private _curOutputTemplate As String
    Private _oldOutputTemplate As String
    Private _oriOutputTemplate As String
    Private _curCommonTemplateName As String
    Private _oldCommonTemplateName As String
    Private _oriCommonTemplateName As String
    Private _curSampleSourceFile As String
    Private _oldSampleSourceFile As String
    Private _oriSampleSourceFile As String
    Private _curIsEnabled As Boolean
    Private _oldIsEnabled As Boolean
    Private _oriIsEnabled As Boolean
    Private _curWorksheetName As String
    Private _oldWorksheetName As String
    Private _oriWorksheetName As String
    Private _curWorksheets As New ExcelWorksheetCollection
    Private _curStartColumn As String
    Private _oldStartColumn As String
    Private _oriStartColumn As String
    Private _curEndColumn As String
    Private _oldEndColumn As String
    Private _oriEndColumn As String
    Private _curSampleRowNo As String
    Private _oldSampleRowNo As String
    Private _oriSampleRowNo As String
    Private _curHeaderAtRow As String
    Private _oldHeaderAtRow As String
    Private _oriHeaderAtRow As String
    Private _curTransactionStartingRow As String
    Private _oldTransactionStartingRow As String
    Private _oriTransactionStartingRow As String
    Private _curHeaderAsFieldName As Boolean
    Private _oldHeaderAsFieldName As Boolean
    Private _oriHeaderAsFieldName As Boolean
    Private _curDecimalSeparator As String
    Private _oldDecimalSeparator As String
    Private _oriDecimalSeparator As String
    Private _curThousandSeparator As String
    Private _oldThousandSeparator As String
    Private _oriThousandSeparator As String
    Private _curDateType As String
    Private _oldDateType As String
    Private _oriDateType As String
    Private _curDateSeparator As String
    Private _oldDateSeparator As String
    Private _oriDateSeparator As String
    Private _curIsZerosInDate As Boolean
    Private _oldIsZerosInDate As Boolean
    Private _oriIsZerosInDate As Boolean
    Private _curReferenceField1 As String
    Private _oldReferenceField1 As String
    Private _oriReferenceField1 As String
    Private _curReferenceField2 As String
    Private _oldReferenceField2 As String
    Private _oriReferenceField2 As String
    Private _curReferenceField3 As String
    Private _oldReferenceField3 As String
    Private _oriReferenceField3 As String
    Private _curRemittanceAmount As String
    Private _oldRemittanceAmount As String
    Private _oriRemittanceAmount As String
    Private _curIsAmountInDollars As Boolean
    Private _oldIsAmountInDollars As Boolean
    Private _oriIsAmountInDollars As Boolean
    Private _curIsAmountInCents As Boolean
    Private _oldIsAmountInCents As Boolean
    Private _oriIsAmountInCents As Boolean
    Private _curAdviceRecord As String
    Private _oldAdviceRecord As String
    Private _oriAdviceRecord As String
    Private _curIsAdviceRecForEveryRow As Boolean
    Private _oldIsAdviceRecForEveryRow As Boolean
    Private _oriIsAdviceRecForEveryRow As Boolean
    Private _curIsAdviceRecAfterEveryChar As Boolean
    Private _oldIsAdviceRecAfterEveryChar As Boolean
    Private _oriIsAdviceRecAfterEveryChar As Boolean
    Private _curIsAdviceRecDelimiter As Boolean
    Private _oldIsAdviceRecDelimiter As Boolean
    Private _oriIsAdviceRecDelimiter As Boolean
    Private _curAdviceRecChar As String
    Private _oldAdviceRecChar As String
    Private _oriAdviceRecChar As String
    Private _curAdviceRecDelimiter As String
    Private _oldAdviceRecDelimiter As String
    Private _oriAdviceRecDelimiter As String
    Private _curFilterCondition As String
    Private _oldFilterCondition As String
    Private _oriFilterCondition As String
    Private _curIsFilterConditionAND As Boolean
    Private _oldIsFilterConditionAND As Boolean
    Private _oriIsFilterConditionAND As Boolean
    Private _curIsFilterConditionOR As Boolean
    Private _oldIsFilterConditionOR As Boolean
    Private _oriIsFilterConditionOR As Boolean
    Private _curFilters As New RowFilterCollection
    Private _curMapSourceFields As New MapSourceFieldCollection
    Private _curMapBankFields As New MapBankFieldCollection
    Private _curTranslatorSettings As New TranslatorSettingCollection
    Private _curEditableSettings As New EditableSettingCollection
    Private _curLookupSettings As New LookupSettingCollection
    Private _curCalculatedFields As New CalculatedFieldCollection
    Private _curStringManipulations As New StringManipulationCollection

    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
    Private _sampleRowDataTable As DataTable
    Private _curPColumn As String
    Private _oldPColumn As String
    Private _oriPColumn As String
    Private _curPIndicator As String
    Private _oldPIndicator As String
    Private _oriPIndicator As String
    Private _curWColumn As String
    Private _oldWColumn As String
    Private _oriWColumn As String
    Private _curWIndicator As String
    Private _oldWIndicator As String
    Private _oriWIndicator As String
    Private _curIColumn As String
    Private _oldIColumn As String
    Private _oriIColumn As String
    Private _curIIndicator As String
    Private _oldIIndicator As String
    Private _oriIIndicator As String
#End Region

#Region " Public Property "

    ''' <summary>
    ''' This property is to Get a List of Editable Bank Fields
    ''' </summary>
    ''' <value>Array of Bank Fields</value>
    ''' <returns>Returns Array of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditableBankFields() As String()

        Get

            Return Nothing

        End Get

    End Property
    ''' <summary>
    ''' This property is to Get a Sample Row from Excel file
    ''' </summary>
    ''' <value>Data Table of Sample Row</value>
    ''' <returns>Returns a Data Table</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SampleRowDataTable() As DataTable
        Get
            Return _sampleRowDataTable
        End Get
    End Property

    ''' <summary>
    ''' Determines whether the Template is a Draft
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _curIsDraft
        End Get
        Set(ByVal value As Boolean)
            If _curIsDraft <> value Then
                _curIsDraft = value
                OnPropertyChanged("IsDraft")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get the Name of Output Template(Master Template)
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplate() As String Implements ICommonTemplate.OutputTemplateName
        Get
            Return _curOutputTemplate
        End Get
        Set(ByVal value As String)

            If Not value Is Nothing Then value = value.Trim()

            If _curOutputTemplate <> value Then
                _curOutputTemplate = value
                OnPropertyChanged("OutputTemplate")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Common Template
    ''' </summary>
    ''' <value>Name of Common Template</value>
    ''' <returns>Returns Name of Common Template</returns>
    ''' <remarks></remarks>
    Public Property CommonTemplateName() As String
        Get
            Return _curCommonTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curCommonTemplateName <> value Then
                _curCommonTemplateName = value
                OnPropertyChanged("CommonTemplateName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Sample SourceFile
    ''' </summary>
    ''' <value>Name of Sample SourceFile</value>
    ''' <returns>Returns Name of Sample SourceFile</returns>
    ''' <remarks></remarks>
    Public Property SampleSourceFile() As String
        Get
            Return _curSampleSourceFile
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSampleSourceFile <> value Then
                _curSampleSourceFile = value
                OnPropertyChanged("SampleSourceFile")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Definition File
    ''' </summary>
    ''' <value>Name of Definition File</value>
    ''' <returns>Returns Name of Definition File</returns>
    ''' <remarks></remarks>
    Public Property DefinitionFile() As String Implements ICommonTemplate.DefinitionFile
        Get
            Return ""
        End Get
        Set(ByVal value As String)
            ' ignored
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Template is Enabled
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _curIsEnabled
        End Get
        Set(ByVal value As Boolean)
            If _curIsEnabled <> value Then
                _curIsEnabled = value
                OnPropertyChanged("IsEnabled")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of Worksheet
    ''' </summary>
    ''' <value>Name of Worksheet</value>
    ''' <returns>Returns Name of Worksheet</returns>
    ''' <remarks></remarks>
    Public Property WorksheetName() As String
        Get
            Return _curWorksheetName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWorksheetName <> value Then
                _curWorksheetName = value
                OnPropertyChanged("WorksheetName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get a list of WorkSheets availabe in the Sample Source File
    ''' </summary>
    ''' <value>List of Worksheets</value>
    ''' <returns>Returns Name of Worksheet</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Worksheets() As ExcelWorksheetCollection
        Get
            Return _curWorksheets
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Start Column 
    ''' </summary>
    ''' <value>Start Column </value>
    ''' <returns>Returns Name of Start Column</returns>
    ''' <remarks></remarks>
    Public Property StartColumn() As String
        Get
            Return _curStartColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curStartColumn <> value Then
                _curStartColumn = value
                OnPropertyChanged("StartColumn")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set End Column 
    ''' </summary>
    ''' <value>End Column </value>
    ''' <returns>Returns Name of End Column</returns>
    ''' <remarks></remarks>
    Public Property EndColumn() As String
        Get
            Return _curEndColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curEndColumn <> value Then
                _curEndColumn = value
                OnPropertyChanged("EndColumn")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Sample Row No.
    ''' </summary>
    ''' <value>Sample Row No.</value>
    ''' <returns>Returns Sample Row No.</returns>
    ''' <remarks></remarks>
    Public Property SampleRowNo() As String
        Get
            Return _curSampleRowNo
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSampleRowNo <> value Then
                _curSampleRowNo = value
                OnPropertyChanged("SampleRowNo")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Header Row No.
    ''' </summary>
    ''' <value>Header Row No.</value>
    ''' <returns>Returns Header Row No.</returns>
    ''' <remarks></remarks>
    Public Property HeaderAtRow() As String
        Get
            Return _curHeaderAtRow
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curHeaderAtRow <> value Then
                _curHeaderAtRow = value
                OnPropertyChanged("HeaderAtRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Header Row No.
    ''' </summary>
    ''' <value>Header Row No.</value>
    ''' <returns>Returns Header Row No.</returns>
    ''' <remarks></remarks>
    Public Property TransactionStartingRow() As String
        Get
            Return _curTransactionStartingRow
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curTransactionStartingRow <> value Then
                _curTransactionStartingRow = value
                OnPropertyChanged("TransactionStartingRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Header should be used as Source Field Name
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns Boolean</returns>
    ''' <remarks></remarks>
    Public Property HeaderAsFieldName() As Boolean
        Get
            Return _curHeaderAsFieldName
        End Get
        Set(ByVal value As Boolean)
            If _curHeaderAsFieldName <> value Then
                _curHeaderAsFieldName = value
                OnPropertyChanged("HeaderAsFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Decimal Separator
    ''' </summary>
    ''' <value>Decimal Separator</value>
    ''' <returns>Returns Decimal Separator</returns>
    ''' <remarks></remarks>
    Public Property DecimalSeparator() As String Implements ICommonTemplate.DecimalSeparator
        Get
            Return _curDecimalSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDecimalSeparator <> value Then
                _curDecimalSeparator = value
                OnPropertyChanged("DecimalSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Thousand Separator
    ''' </summary>
    ''' <value>Thousand Separator</value>
    ''' <returns>Returns Thousand Separator</returns>
    ''' <remarks></remarks>
    Public Property ThousandSeparator() As String Implements ICommonTemplate.ThousandSeparator
        Get
            Return _curThousandSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curThousandSeparator <> value Then
                _curThousandSeparator = value
                OnPropertyChanged("ThousandSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Type
    ''' </summary>
    ''' <value>Date Type</value>
    ''' <returns>Returns Date Type</returns>
    ''' <remarks></remarks>
    Public Property DateType() As String Implements ICommonTemplate.DateType
        Get
            Return _curDateType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateType <> value Then
                _curDateType = value
                OnPropertyChanged("DateType")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Separator
    ''' </summary>
    ''' <value>Date Separator</value>
    ''' <returns>Returns Date Separator</returns>
    ''' <remarks></remarks>
    Public Property DateSeparator() As String Implements ICommonTemplate.DateSeparator
        Get
            Return _curDateSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateSeparator <> value Then
                _curDateSeparator = value
                OnPropertyChanged("DateSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether zeros are part of the Date
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether zeros are part of Date</returns>
    ''' <remarks></remarks>
    Public Property IsZerosInDate() As Boolean Implements ICommonTemplate.IsZerosInDate
        Get
            Return _curIsZerosInDate
        End Get
        Set(ByVal value As Boolean)
            If _curIsZerosInDate <> value Then
                _curIsZerosInDate = value
                OnPropertyChanged("IsZerosInDate")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 1</value>
    ''' <returns>Returns the First Reference Field</returns>
    ''' <remarks></remarks>
    Public Property ReferenceField1() As String Implements ICommonTemplate.TransactionReferenceField1
        Get
            Return _curReferenceField1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curReferenceField1 <> value Then
                _curReferenceField1 = value
                OnPropertyChanged("ReferenceField1")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 2</value>
    ''' <returns>Returns the Second Reference Field</returns>
    ''' <remarks></remarks>
    Public Property ReferenceField2() As String Implements ICommonTemplate.TransactionReferenceField2
        Get
            Return _curReferenceField2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curReferenceField2 <> value Then
                _curReferenceField2 = value
                OnPropertyChanged("ReferenceField2")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 3</value>
    ''' <returns>Returns the Third Reference Field</returns>
    ''' <remarks></remarks>
    Public Property ReferenceField3() As String Implements ICommonTemplate.TransactionReferenceField3
        Get
            Return _curReferenceField3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curReferenceField3 <> value Then
                _curReferenceField3 = value
                OnPropertyChanged("ReferenceField3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Remittance Amount Type
    ''' </summary>
    ''' <value>Dollar/Cent</value>
    ''' <returns>Returns either "Dollar" or "Cent"</returns>
    ''' <remarks></remarks>
    Public Property RemittanceAmount() As String
        Get
            Dim returnValue As String = String.Empty
            If IsAmountInDollars Then
                returnValue = "Dollar"
            ElseIf IsAmountInCents Then
                returnValue = "Cent"
            End If
            Return returnValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            Select Case value
                Case "Dollar"
                    IsAmountInDollars = True
                Case "Cent"
                    IsAmountInCents = True
            End Select
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in dollars
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in dollars</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInDollars() As Boolean Implements ICommonTemplate.IsAmountInDollars
        Get
            Return _curIsAmountInDollars
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInDollars <> value Then
                _curIsAmountInDollars = value
                OnPropertyChanged("IsAmountInDollars")

                If IsAmountInDollars Then
                    IsAmountInCents = False
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in Cents
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in Cents</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInCents() As Boolean Implements ICommonTemplate.IsAmountInCents
        Get
            Return _curIsAmountInCents
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInCents <> value Then
                _curIsAmountInCents = value
                OnPropertyChanged("IsAmountInCents")

                If IsAmountInCents Then
                    IsAmountInDollars = False
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Type of Advise Record
    ''' </summary>
    ''' <value>One of "EveryRow", "EveryChar", "Delimiter"</value>
    ''' <returns>Returns the type of Advice Record</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecord() As String
        Get
            Dim returnValue As String = String.Empty
            If IsAdviceRecForEveryRow Then
                returnValue = "EveryRow"
            ElseIf IsAdviceRecAfterEveryChar Then
                returnValue = "EveryChar"
            ElseIf IsAdviceRecDelimiter Then
                returnValue = "Delimiter"
            End If
            Return returnValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            Select Case value
                Case "EveryRow"
                    IsAdviceRecForEveryRow = True
                Case "EveryChar"
                    IsAdviceRecAfterEveryChar = True
                Case "Delimiter"
                    IsAdviceRecDelimiter = True

            End Select
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is for every row
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record setting is for every row</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecForEveryRow() As Boolean Implements ICommonTemplate.IsAdviceRecordForEveryRow
        Get
            Return _curIsAdviceRecForEveryRow
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecForEveryRow <> value Then
                _curIsAdviceRecForEveryRow = value
                OnPropertyChanged("IsAdviceRecForEveryRow")


                ' If this is true, set other choices as false
                If IsAdviceRecForEveryRow Then
                    IsAdviceRecAfterEveryChar = False
                    IsAdviceRecDelimiter = False
                    AdviceRecChar = String.Empty
                    AdviceRecDelimiter = String.Empty
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Advice record is after every character
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is after every character</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecAfterEveryChar() As Boolean Implements ICommonTemplate.IsAdviceRecordAfterChar
        Get
            Return _curIsAdviceRecAfterEveryChar
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecAfterEveryChar <> value Then
                _curIsAdviceRecAfterEveryChar = value
                OnPropertyChanged("IsAdviceRecAfterEveryChar")


                ' If this is true, set other choices as false
                If IsAdviceRecAfterEveryChar Then
                    IsAdviceRecForEveryRow = False
                    IsAdviceRecDelimiter = False
                    AdviceRecDelimiter = String.Empty
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is delimited
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is delimited</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecDelimiter() As Boolean Implements ICommonTemplate.IsAdviceRecordDelimited
        Get
            Return _curIsAdviceRecDelimiter
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecDelimiter <> value Then
                _curIsAdviceRecDelimiter = value
                OnPropertyChanged("IsAdviceRecDelimiter")

                ' If this is true, set other choices as false
                If IsAdviceRecDelimiter Then
                    IsAdviceRecForEveryRow = False
                    IsAdviceRecAfterEveryChar = False
                    AdviceRecChar = String.Empty
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Advice Record Character
    ''' </summary>
    ''' <value>Advice Record Character</value>
    ''' <returns>Returns the Advice Record Character</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecChar() As String Implements ICommonTemplate.AdviceRecordChar
        Get
            Return _curAdviceRecChar
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecChar <> value Then
                _curAdviceRecChar = value
                OnPropertyChanged("AdviceRecChar")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Advice Record Delimiter
    ''' </summary>
    ''' <value>Advice Record Delimiter</value>
    ''' <returns>Returns the Advice Record Delimiter</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecDelimiter() As String Implements ICommonTemplate.AdviceRecordDelimiter
        Get
            Return _curAdviceRecDelimiter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecDelimiter <> value Then
                _curAdviceRecDelimiter = value
                OnPropertyChanged("AdviceRecDelimiter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Filter Condition
    ''' </summary>
    ''' <value>Filter Condition</value>
    ''' <returns>Returns the Filter Condition</returns>
    ''' <remarks></remarks>
    Public Property FilterCondition() As String
        Get
            Dim returnValue As String = String.Empty
            If IsFilterConditionAND Then
                returnValue = "AND"
            ElseIf IsFilterConditionOR Then
                returnValue = "OR"
            End If
            Return returnValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            Select Case value
                Case "AND"
                    IsFilterConditionAND = True
                Case "OR"
                    IsFilterConditionOR = True
            End Select
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is AND for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether AND condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionAND() As Boolean Implements ICommonTemplate.IsFilterConditionAND
        Get
            Return _curIsFilterConditionAND
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionAND <> value Then
                _curIsFilterConditionAND = value
                OnPropertyChanged("IsFilterConditionAND")


                If IsFilterConditionAND Then
                    IsFilterConditionOR = False
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is OR for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether OR condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionOR() As Boolean
        Get
            Return _curIsFilterConditionOR
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionOR <> value Then
                _curIsFilterConditionOR = value
                OnPropertyChanged("IsFilterConditionOR")

                If IsFilterConditionOR Then
                    IsFilterConditionAND = False
                End If
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to get a List of Filters
    ''' </summary>
    ''' <value>Row Filter</value>
    ''' <returns>Returns list of Row Filters</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Filters() As RowFilterCollection Implements ICommonTemplate.Filters
        Get
            Return _curFilters
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Source Fields
    ''' </summary>
    ''' <value>Source Field</value>
    ''' <returns>Returns a list of Source Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapSourceFields() As MapSourceFieldCollection Implements ICommonTemplate.MapSourceFields
        Get
            Return _curMapSourceFields
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get a List of Bank Fields
    ''' </summary>
    ''' <value>Bank Field</value>
    ''' <returns>Returns a List of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapBankFields() As MapBankFieldCollection Implements ICommonTemplate.MapBankFields
        Get
            Return _curMapBankFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Translator Settings
    ''' </summary>
    ''' <value>Translator Setting</value>
    ''' <returns>Returns a List of Translator Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TranslatorSettings() As TranslatorSettingCollection Implements ICommonTemplate.TranslatorSettings
        Get
            Return _curTranslatorSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Editable Settings
    ''' </summary>
    ''' <value>Editable Setting</value>
    ''' <returns>Returns a List of Editable Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditableSettings() As EditableSettingCollection Implements ICommonTemplate.EditableSettings
        Get
            Return _curEditableSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Lookup Settings
    ''' </summary>
    ''' <value>Lookup Setting</value>
    ''' <returns>Returns a List of Lookup Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LookupSettings() As LookupSettingCollection Implements ICommonTemplate.LookupSettings
        Get
            Return _curLookupSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Calculated Fields
    ''' </summary>
    ''' <value>Calculated Field</value>
    ''' <returns>Returns a List of Calculated Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CalculatedFields() As CalculatedFieldCollection Implements ICommonTemplate.CalculatedFields
        Get
            Return _curCalculatedFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of String Manipulation Settings
    ''' </summary>
    ''' <value>String Manipulation Setting</value>
    ''' <returns>returns a List of String Manipulation Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property StringManipulations() As StringManipulationCollection Implements ICommonTemplate.StringManipulations
        Get
            Return _curStringManipulations
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the User that created this Object
    ''' </summary>
    ''' <value>User ID</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Created Date and Time of this object
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Created Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Modified Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Modified Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that originally modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Original Modified Date and Time of this objecdt
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Original Modified Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property

    ''' <summary>
    ''' This property is to Get/Set the P-record Indicator Column for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PColumn() As String
        Get
            Return _curPColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curPColumn <> value Then
                _curPColumn = value
                OnPropertyChanged("PColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the P-record Indicator for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PIndicator() As String
        Get
            Return _curPIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curPIndicator <> value Then
                _curPIndicator = value
                OnPropertyChanged("PIndicator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the W-record Indicator Column for iFTS-2 format
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WColumn() As String
        Get
            Return _curWColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWColumn <> value Then
                _curWColumn = value
                OnPropertyChanged("WColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the W-record Indicator for iFTS-2 format
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WIndicator() As String
        Get
            Return _curWIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWIndicator <> value Then
                _curWIndicator = value
                OnPropertyChanged("WIndicator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the I-record Indicator Column for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IColumn() As String
        Get
            Return _curIColumn
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curIColumn <> value Then
                _curIColumn = value
                OnPropertyChanged("IColumn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set the I-record Indicator for iFTS-2/Mr. Omakase India file formats.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IIndicator() As String
        Get
            Return _curIIndicator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curIIndicator <> value Then
                _curIIndicator = value
                OnPropertyChanged("IIndicator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get Filtered Source Data
    ''' </summary>
    ''' <value>DataView</value>
    ''' <returns>Returns a Data Table of Records after applying Row Filter Conditions</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property FilteredSourceData() As System.Data.DataView Implements ICommonTemplate.FilteredSourceData
        Get
            Return _FilteredSourceData
        End Get
    End Property

    ''' <summary>
    ''' This property is to Get Source Data
    ''' </summary>
    ''' <value>DataTable</value>
    ''' <returns>Returns a Data Table of Transaction Records from Source File</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SourceData() As System.Data.DataTable Implements ICommonTemplate.SourceData
        Get
            Return _SourceData
        End Get
    End Property

    Public Property PreviewSourceData() As System.Data.DataTable Implements ICommonTemplate.PreviewSourceData
        Get
            Return _PreviewSourceData
        End Get
        Set(ByVal value As System.Data.DataTable)
            _PreviewSourceData = value
        End Set
    End Property

    Public ReadOnly Property PSourceData() As System.Data.DataTable
        Get
            Return _tblPaymentSrc
        End Get
    End Property

    Public ReadOnly Property WSourceData() As DataTable
        Get
            Return _tblTaxSrc
        End Get
    End Property

    Public ReadOnly Property ISourceData() As DataTable
        Get
            Return _tblInvoiceSrc
        End Get
    End Property

#End Region

    ''' <summary>
    ''' Apply the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriIsDraft = _curIsDraft
        _oriOutputTemplate = _curOutputTemplate
        _oriCommonTemplateName = _curCommonTemplateName
        _oriSampleSourceFile = _curSampleSourceFile
        _oriIsEnabled = _curIsEnabled
        _oriWorksheetName = _curWorksheetName
        _oriStartColumn = _curStartColumn
        _oriEndColumn = _curEndColumn
        _oriSampleRowNo = _curSampleRowNo
        _oriHeaderAtRow = _curHeaderAtRow
        _oriTransactionStartingRow = _curTransactionStartingRow
        _oriHeaderAsFieldName = _curHeaderAsFieldName
        _oriDecimalSeparator = _curDecimalSeparator
        _oriThousandSeparator = _curThousandSeparator
        _oriDateType = _curDateType
        _oriDateSeparator = _curDateSeparator
        _oriIsZerosInDate = _curIsZerosInDate
        _oriReferenceField1 = _curReferenceField1
        _oriReferenceField2 = _curReferenceField2
        _oriReferenceField3 = _curReferenceField3
        _oriRemittanceAmount = _curRemittanceAmount
        _oriIsAmountInDollars = _curIsAmountInDollars
        _oriIsAmountInCents = _curIsAmountInCents
        _oriAdviceRecord = _curAdviceRecord
        _oriIsAdviceRecForEveryRow = _curIsAdviceRecForEveryRow
        _oriIsAdviceRecAfterEveryChar = _curIsAdviceRecAfterEveryChar
        _oriIsAdviceRecDelimiter = _curIsAdviceRecDelimiter
        _oriAdviceRecChar = _curAdviceRecChar
        _oriAdviceRecDelimiter = _curAdviceRecDelimiter
        _oriFilterCondition = _curFilterCondition
        _oriIsFilterConditionAND = _curIsFilterConditionAND
        _oriIsFilterConditionOR = _curIsFilterConditionOR
        _oriFilterCondition = _curFilterCondition
        _oriPColumn = _curPColumn
        _oriPIndicator = _curPIndicator
        _oriWColumn = _curWColumn
        _oriWIndicator = _curWIndicator
        _oriIColumn = _curIColumn
        _oriIIndicator = _curIIndicator
    End Sub
    ''' <summary>
    ''' Begin making changes to the fields of this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldIsDraft = _curIsDraft
        _oldOutputTemplate = _curOutputTemplate
        _oldCommonTemplateName = _curCommonTemplateName
        _oldSampleSourceFile = _curSampleSourceFile
        _oldIsEnabled = _curIsEnabled
        _oldWorksheetName = _curWorksheetName
        _oldStartColumn = _curStartColumn
        _oldEndColumn = _curEndColumn
        _oldSampleRowNo = _curSampleRowNo
        _oldHeaderAtRow = _curHeaderAtRow
        _oldTransactionStartingRow = _curTransactionStartingRow
        _oldHeaderAsFieldName = _curHeaderAsFieldName
        _oldDecimalSeparator = _curDecimalSeparator
        _oldThousandSeparator = _curThousandSeparator
        _oldDateType = _curDateType
        _oldDateSeparator = _curDateSeparator
        _oldIsZerosInDate = _curIsZerosInDate
        _oldReferenceField1 = _curReferenceField1
        _oldReferenceField2 = _curReferenceField2
        _oldReferenceField3 = _curReferenceField3
        _oldRemittanceAmount = _curRemittanceAmount
        _oldIsAmountInDollars = _curIsAmountInDollars
        _oldIsAmountInCents = _curIsAmountInCents
        _oldAdviceRecord = _curAdviceRecord
        _oldIsAdviceRecForEveryRow = _curIsAdviceRecForEveryRow
        _oldIsAdviceRecAfterEveryChar = _curIsAdviceRecAfterEveryChar
        _oldIsAdviceRecDelimiter = _curIsAdviceRecDelimiter
        _oldAdviceRecChar = _curAdviceRecChar
        _oldAdviceRecDelimiter = _curAdviceRecDelimiter
        _oldFilterCondition = _curFilterCondition
        _oldIsFilterConditionAND = _curIsFilterConditionAND
        _oldIsFilterConditionOR = _curIsFilterConditionOR
        _oldFilterCondition = _curFilterCondition
        _oldPColumn = _curPColumn
        _oldPIndicator = _curPIndicator
        _oldWColumn = _curWColumn
        _oldWIndicator = _curWIndicator
        _oldIColumn = _curIColumn
        _oldIIndicator = _curIIndicator
    End Sub
    ''' <summary>
    ''' Cancel the changes made to the fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curIsDraft = _oldIsDraft
        _curOutputTemplate = _oldOutputTemplate
        _curCommonTemplateName = _oldCommonTemplateName
        _curSampleSourceFile = _oldSampleSourceFile
        _curIsEnabled = _oldIsEnabled
        _curWorksheetName = _oldWorksheetName
        _curStartColumn = _oldStartColumn
        _curEndColumn = _oldEndColumn
        _curSampleRowNo = _oldSampleRowNo
        _curHeaderAtRow = _oldHeaderAtRow
        _curTransactionStartingRow = _oldTransactionStartingRow
        _curHeaderAsFieldName = _oldHeaderAsFieldName
        _curDecimalSeparator = _oldDecimalSeparator
        _curThousandSeparator = _oldThousandSeparator
        _curDateType = _oldDateType
        _curDateSeparator = _oldDateSeparator
        _curIsZerosInDate = _oldIsZerosInDate
        _curReferenceField1 = _oldReferenceField1
        _curReferenceField2 = _oldReferenceField2
        _curReferenceField3 = _oldReferenceField3
        _curRemittanceAmount = _oldRemittanceAmount
        _curIsAmountInDollars = _oldIsAmountInDollars
        _curIsAmountInCents = _oldIsAmountInCents
        _curAdviceRecord = _oldAdviceRecord
        _curIsAdviceRecForEveryRow = _oldIsAdviceRecForEveryRow
        _curIsAdviceRecAfterEveryChar = _oldIsAdviceRecAfterEveryChar
        _curIsAdviceRecDelimiter = _oldIsAdviceRecDelimiter
        _curAdviceRecChar = _oldAdviceRecChar
        _curAdviceRecDelimiter = _oldAdviceRecDelimiter
        _curFilterCondition = _oldFilterCondition
        _curIsFilterConditionAND = _oldIsFilterConditionAND
        _curIsFilterConditionOR = _oldIsFilterConditionOR
        _curFilterCondition = _oldFilterCondition
        _curPColumn = _oldPColumn
        _curPIndicator = _oldPIndicator
        _curWColumn = _oldWColumn
        _curWIndicator = _oldWIndicator
        _curIColumn = _oldIColumn
        _curIIndicator = _oldIIndicator
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curIsDraft = ReadXMLElement(reader, "_curIsDraft")
            _oldIsDraft = ReadXMLElement(reader, "_oldIsDraft")
            _oriIsDraft = ReadXMLElement(reader, "_oriIsDraft")
            _curOutputTemplate = ReadXMLElement(reader, "_curOutputTemplate")
            _oldOutputTemplate = ReadXMLElement(reader, "_oldOutputTemplate")
            _oriOutputTemplate = ReadXMLElement(reader, "_oriOutputTemplate")
            _curCommonTemplateName = ReadXMLElement(reader, "_curCommonTemplateName")
            _oldCommonTemplateName = ReadXMLElement(reader, "_oldCommonTemplateName")
            _oriCommonTemplateName = ReadXMLElement(reader, "_oriCommonTemplateName")
            _curSampleSourceFile = ReadXMLElement(reader, "_curSampleSourceFile")
            _oldSampleSourceFile = ReadXMLElement(reader, "_oldSampleSourceFile")
            _oriSampleSourceFile = ReadXMLElement(reader, "_oriSampleSourceFile")
            _curIsEnabled = ReadXMLElement(reader, "_curIsEnabled")
            _oldIsEnabled = ReadXMLElement(reader, "_oldIsEnabled")
            _oriIsEnabled = ReadXMLElement(reader, "_oriIsEnabled")
            _curWorksheetName = ReadXMLElement(reader, "_curWorksheetName")
            _oldWorksheetName = ReadXMLElement(reader, "_oldWorksheetName")
            _oriWorksheetName = ReadXMLElement(reader, "_oriWorksheetName")
            _curWorksheets.ReadXml(reader)
            _curStartColumn = ReadXMLElement(reader, "_curStartColumn")
            _oldStartColumn = ReadXMLElement(reader, "_oldStartColumn")
            _oriStartColumn = ReadXMLElement(reader, "_oriStartColumn")
            _curEndColumn = ReadXMLElement(reader, "_curEndColumn")
            _oldEndColumn = ReadXMLElement(reader, "_oldEndColumn")
            _oriEndColumn = ReadXMLElement(reader, "_oriEndColumn")
            _curSampleRowNo = ReadXMLElement(reader, "_curSampleRowNo")
            _oldSampleRowNo = ReadXMLElement(reader, "_oldSampleRowNo")
            _oriSampleRowNo = ReadXMLElement(reader, "_oriSampleRowNo")
            _curHeaderAtRow = ReadXMLElement(reader, "_curHeaderAtRow")
            _oldHeaderAtRow = ReadXMLElement(reader, "_oldHeaderAtRow")
            _oriHeaderAtRow = ReadXMLElement(reader, "_oriHeaderAtRow")
            _curTransactionStartingRow = ReadXMLElement(reader, "_curTransactionStartingRow")
            _oldTransactionStartingRow = ReadXMLElement(reader, "_oldTransactionStartingRow")
            _oriTransactionStartingRow = ReadXMLElement(reader, "_oriTransactionStartingRow")
            _curHeaderAsFieldName = ReadXMLElement(reader, "_curHeaderAsFieldName")
            _oldHeaderAsFieldName = ReadXMLElement(reader, "_oldHeaderAsFieldName")
            _oriHeaderAsFieldName = ReadXMLElement(reader, "_oriHeaderAsFieldName")
            _curDecimalSeparator = ReadXMLElement(reader, "_curDecimalSeparator")
            _oldDecimalSeparator = ReadXMLElement(reader, "_oldDecimalSeparator")
            _oriDecimalSeparator = ReadXMLElement(reader, "_oriDecimalSeparator")
            _curThousandSeparator = ReadXMLElement(reader, "_curThousandSeparator")
            _oldThousandSeparator = ReadXMLElement(reader, "_oldThousandSeparator")
            _oriThousandSeparator = ReadXMLElement(reader, "_oriThousandSeparator")
            _curDateType = ReadXMLElement(reader, "_curDateType")
            _oldDateType = ReadXMLElement(reader, "_oldDateType")
            _oriDateType = ReadXMLElement(reader, "_oriDateType")
            _curDateSeparator = ReadXMLElement(reader, "_curDateSeparator")
            _oldDateSeparator = ReadXMLElement(reader, "_oldDateSeparator")
            _oriDateSeparator = ReadXMLElement(reader, "_oriDateSeparator")
            _curIsZerosInDate = ReadXMLElement(reader, "_curIsZerosInDate")
            _oldIsZerosInDate = ReadXMLElement(reader, "_oldIsZerosInDate")
            _oriIsZerosInDate = ReadXMLElement(reader, "_oriIsZerosInDate")
            _curReferenceField1 = ReadXMLElement(reader, "_curReferenceField1")
            _oldReferenceField1 = ReadXMLElement(reader, "_oldReferenceField1")
            _oriReferenceField1 = ReadXMLElement(reader, "_oriReferenceField1")
            _curReferenceField2 = ReadXMLElement(reader, "_curReferenceField2")
            _oldReferenceField2 = ReadXMLElement(reader, "_oldReferenceField2")
            _oriReferenceField2 = ReadXMLElement(reader, "_oriReferenceField2")
            _curReferenceField3 = ReadXMLElement(reader, "_curReferenceField3")
            _oldReferenceField3 = ReadXMLElement(reader, "_oldReferenceField3")
            _oriReferenceField3 = ReadXMLElement(reader, "_oriReferenceField3")
            _curRemittanceAmount = ReadXMLElement(reader, "_curRemittanceAmount")
            _oldRemittanceAmount = ReadXMLElement(reader, "_oldRemittanceAmount")
            _oriRemittanceAmount = ReadXMLElement(reader, "_oriRemittanceAmount")
            _curIsAmountInDollars = ReadXMLElement(reader, "_curIsAmountInDollars")
            _oldIsAmountInDollars = ReadXMLElement(reader, "_oldIsAmountInDollars")
            _oriIsAmountInDollars = ReadXMLElement(reader, "_oriIsAmountInDollars")
            _curIsAmountInCents = ReadXMLElement(reader, "_curIsAmountInCents")
            _oldIsAmountInCents = ReadXMLElement(reader, "_oldIsAmountInCents")
            _oriIsAmountInCents = ReadXMLElement(reader, "_oriIsAmountInCents")
            _curAdviceRecord = ReadXMLElement(reader, "_curAdviceRecord")
            _oldAdviceRecord = ReadXMLElement(reader, "_oldAdviceRecord")
            _oriAdviceRecord = ReadXMLElement(reader, "_oriAdviceRecord")
            _curIsAdviceRecForEveryRow = ReadXMLElement(reader, "_curIsAdviceRecForEveryRow")
            _oldIsAdviceRecForEveryRow = ReadXMLElement(reader, "_oldIsAdviceRecForEveryRow")
            _oriIsAdviceRecForEveryRow = ReadXMLElement(reader, "_oriIsAdviceRecForEveryRow")
            _curIsAdviceRecAfterEveryChar = ReadXMLElement(reader, "_curIsAdviceRecAfterEveryChar")
            _oldIsAdviceRecAfterEveryChar = ReadXMLElement(reader, "_oldIsAdviceRecAfterEveryChar")
            _oriIsAdviceRecAfterEveryChar = ReadXMLElement(reader, "_oriIsAdviceRecAfterEveryChar")
            _curIsAdviceRecDelimiter = ReadXMLElement(reader, "_curIsAdviceRecDelimiter")
            _oldIsAdviceRecDelimiter = ReadXMLElement(reader, "_oldIsAdviceRecDelimiter")
            _oriIsAdviceRecDelimiter = ReadXMLElement(reader, "_oriIsAdviceRecDelimiter")
            _curAdviceRecChar = ReadXMLElement(reader, "_curAdviceRecChar")
            _oldAdviceRecChar = ReadXMLElement(reader, "_oldAdviceRecChar")
            _oriAdviceRecChar = ReadXMLElement(reader, "_oriAdviceRecChar")
            _curAdviceRecDelimiter = ReadXMLElement(reader, "_curAdviceRecDelimiter")
            _oldAdviceRecDelimiter = ReadXMLElement(reader, "_oldAdviceRecDelimiter")
            _oriAdviceRecDelimiter = ReadXMLElement(reader, "_oriAdviceRecDelimiter")
            _curFilterCondition = ReadXMLElement(reader, "_curFilterCondition")
            _oldFilterCondition = ReadXMLElement(reader, "_oldFilterCondition")
            _oriFilterCondition = ReadXMLElement(reader, "_oriFilterCondition")
            _curIsFilterConditionAND = ReadXMLElement(reader, "_curIsFilterConditionAND")
            _oldIsFilterConditionAND = ReadXMLElement(reader, "_oldIsFilterConditionAND")
            _oriIsFilterConditionAND = ReadXMLElement(reader, "_oriIsFilterConditionAND")
            _curIsFilterConditionOR = ReadXMLElement(reader, "_curIsFilterConditionOR")
            _oldIsFilterConditionOR = ReadXMLElement(reader, "_oldIsFilterConditionOR")
            _oriIsFilterConditionOR = ReadXMLElement(reader, "_oriIsFilterConditionOR")
            _curFilterCondition = ReadXMLElement(reader, "_curFilterCondition")
            _oldFilterCondition = ReadXMLElement(reader, "_oldFilterCondition")
            _oriFilterCondition = ReadXMLElement(reader, "_oriFilterCondition")
            _curFilters.ReadXml(reader)
            _curMapSourceFields.ReadXml(reader)
            _curMapBankFields.ReadXml(reader)
            _curTranslatorSettings.ReadXml(reader)
            _curEditableSettings.ReadXml(reader)
            _curLookupSettings.ReadXml(reader)
            _curCalculatedFields.ReadXml(reader)
            _curStringManipulations.ReadXml(reader)


            _curPColumn = ReadXMLElement(reader, "_curPColumn")
            _oldPColumn = ReadXMLElement(reader, "_oldPColumn")
            _oriPColumn = ReadXMLElement(reader, "_oriPColumn")
            _curPIndicator = ReadXMLElement(reader, "_curPIndicator")
            _oldPIndicator = ReadXMLElement(reader, "_oldPIndicator")
            _oriPIndicator = ReadXMLElement(reader, "_oriPIndicator")
            _curWColumn = ReadXMLElement(reader, "_curWColumn")
            _oldWColumn = ReadXMLElement(reader, "_oldWColumn")
            _oriWColumn = ReadXMLElement(reader, "_oriWColumn")
            _curWIndicator = ReadXMLElement(reader, "_curWIndicator")
            _oldWIndicator = ReadXMLElement(reader, "_oldWIndicator")
            _oriWIndicator = ReadXMLElement(reader, "_oriWIndicator")
            _curIColumn = ReadXMLElement(reader, "_curIColumn")
            _oldIColumn = ReadXMLElement(reader, "_oldIColumn")
            _oriIColumn = ReadXMLElement(reader, "_oriIColumn")
            _curIIndicator = ReadXMLElement(reader, "_curIIndicator")
            _oldIIndicator = ReadXMLElement(reader, "_oldIIndicator")
            _oriIIndicator = ReadXMLElement(reader, "_oriIIndicator")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curIsDraft", _curIsDraft)
        WriteXmlElement(writer, "_oldIsDraft", _oldIsDraft)
        WriteXmlElement(writer, "_oriIsDraft", _oriIsDraft)
        WriteXmlElement(writer, "_curOutputTemplate", _curOutputTemplate)
        WriteXmlElement(writer, "_oldOutputTemplate", _oldOutputTemplate)
        WriteXmlElement(writer, "_oriOutputTemplate", _oriOutputTemplate)
        WriteXmlElement(writer, "_curCommonTemplateName", _curCommonTemplateName)
        WriteXmlElement(writer, "_oldCommonTemplateName", _oldCommonTemplateName)
        WriteXmlElement(writer, "_oriCommonTemplateName", _oriCommonTemplateName)
        WriteXmlElement(writer, "_curSampleSourceFile", _curSampleSourceFile)
        WriteXmlElement(writer, "_oldSampleSourceFile", _oldSampleSourceFile)
        WriteXmlElement(writer, "_oriSampleSourceFile", _oriSampleSourceFile)
        WriteXmlElement(writer, "_curIsEnabled", _curIsEnabled)
        WriteXmlElement(writer, "_oldIsEnabled", _oldIsEnabled)
        WriteXmlElement(writer, "_oriIsEnabled", _oriIsEnabled)
        WriteXmlElement(writer, "_curWorksheetName", _curWorksheetName)
        WriteXmlElement(writer, "_oldWorksheetName", _oldWorksheetName)
        WriteXmlElement(writer, "_oriWorksheetName", _oriWorksheetName)
        _curWorksheets.WriteXml(writer)
        WriteXmlElement(writer, "_curStartColumn", _curStartColumn)
        WriteXmlElement(writer, "_oldStartColumn", _oldStartColumn)
        WriteXmlElement(writer, "_oriStartColumn", _oriStartColumn)
        WriteXmlElement(writer, "_curEndColumn", _curEndColumn)
        WriteXmlElement(writer, "_oldEndColumn", _oldEndColumn)
        WriteXmlElement(writer, "_oriEndColumn", _oriEndColumn)
        WriteXmlElement(writer, "_curSampleRowNo", _curSampleRowNo)
        WriteXmlElement(writer, "_oldSampleRowNo", _oldSampleRowNo)
        WriteXmlElement(writer, "_oriSampleRowNo", _oriSampleRowNo)
        WriteXmlElement(writer, "_curHeaderAtRow", _curHeaderAtRow)
        WriteXmlElement(writer, "_oldHeaderAtRow", _oldHeaderAtRow)
        WriteXmlElement(writer, "_oriHeaderAtRow", _oriHeaderAtRow)
        WriteXmlElement(writer, "_curTransactionStartingRow", _curTransactionStartingRow)
        WriteXmlElement(writer, "_oldTransactionStartingRow", _oldTransactionStartingRow)
        WriteXmlElement(writer, "_oriTransactionStartingRow", _oriTransactionStartingRow)
        WriteXmlElement(writer, "_curHeaderAsFieldName", _curHeaderAsFieldName)
        WriteXmlElement(writer, "_oldHeaderAsFieldName", _oldHeaderAsFieldName)
        WriteXmlElement(writer, "_oriHeaderAsFieldName", _oriHeaderAsFieldName)
        WriteXmlElement(writer, "_curDecimalSeparator", _curDecimalSeparator)
        WriteXmlElement(writer, "_oldDecimalSeparator", _oldDecimalSeparator)
        WriteXmlElement(writer, "_oriDecimalSeparator", _oriDecimalSeparator)
        WriteXmlElement(writer, "_curThousandSeparator", _curThousandSeparator)
        WriteXmlElement(writer, "_oldThousandSeparator", _oldThousandSeparator)
        WriteXmlElement(writer, "_oriThousandSeparator", _oriThousandSeparator)
        WriteXmlElement(writer, "_curDateType", _curDateType)
        WriteXmlElement(writer, "_oldDateType", _oldDateType)
        WriteXmlElement(writer, "_oriDateType", _oriDateType)
        WriteXmlElement(writer, "_curDateSeparator", _curDateSeparator)
        WriteXmlElement(writer, "_oldDateSeparator", _oldDateSeparator)
        WriteXmlElement(writer, "_oriDateSeparator", _oriDateSeparator)
        WriteXmlElement(writer, "_curIsZerosInDate", _curIsZerosInDate)
        WriteXmlElement(writer, "_oldIsZerosInDate", _oldIsZerosInDate)
        WriteXmlElement(writer, "_oriIsZerosInDate", _oriIsZerosInDate)
        WriteXmlElement(writer, "_curReferenceField1", _curReferenceField1)
        WriteXmlElement(writer, "_oldReferenceField1", _oldReferenceField1)
        WriteXmlElement(writer, "_oriReferenceField1", _oriReferenceField1)
        WriteXmlElement(writer, "_curReferenceField2", _curReferenceField2)
        WriteXmlElement(writer, "_oldReferenceField2", _oldReferenceField2)
        WriteXmlElement(writer, "_oriReferenceField2", _oriReferenceField2)
        WriteXmlElement(writer, "_curReferenceField3", _curReferenceField3)
        WriteXmlElement(writer, "_oldReferenceField3", _oldReferenceField3)
        WriteXmlElement(writer, "_oriReferenceField3", _oriReferenceField3)
        WriteXmlElement(writer, "_curRemittanceAmount", _curRemittanceAmount)
        WriteXmlElement(writer, "_oldRemittanceAmount", _oldRemittanceAmount)
        WriteXmlElement(writer, "_oriRemittanceAmount", _oriRemittanceAmount)
        WriteXmlElement(writer, "_curIsAmountInDollars", _curIsAmountInDollars)
        WriteXmlElement(writer, "_oldIsAmountInDollars", _oldIsAmountInDollars)
        WriteXmlElement(writer, "_oriIsAmountInDollars", _oriIsAmountInDollars)
        WriteXmlElement(writer, "_curIsAmountInCents", _curIsAmountInCents)
        WriteXmlElement(writer, "_oldIsAmountInCents", _oldIsAmountInCents)
        WriteXmlElement(writer, "_oriIsAmountInCents", _oriIsAmountInCents)
        WriteXmlElement(writer, "_curAdviceRecord", _curAdviceRecord)
        WriteXmlElement(writer, "_oldAdviceRecord", _oldAdviceRecord)
        WriteXmlElement(writer, "_oriAdviceRecord", _oriAdviceRecord)
        WriteXmlElement(writer, "_curIsAdviceRecForEveryRow", _curIsAdviceRecForEveryRow)
        WriteXmlElement(writer, "_oldIsAdviceRecForEveryRow", _oldIsAdviceRecForEveryRow)
        WriteXmlElement(writer, "_oriIsAdviceRecForEveryRow", _oriIsAdviceRecForEveryRow)
        WriteXmlElement(writer, "_curIsAdviceRecAfterEveryChar", _curIsAdviceRecAfterEveryChar)
        WriteXmlElement(writer, "_oldIsAdviceRecAfterEveryChar", _oldIsAdviceRecAfterEveryChar)
        WriteXmlElement(writer, "_oriIsAdviceRecAfterEveryChar", _oriIsAdviceRecAfterEveryChar)
        WriteXmlElement(writer, "_curIsAdviceRecDelimiter", _curIsAdviceRecDelimiter)
        WriteXmlElement(writer, "_oldIsAdviceRecDelimiter", _oldIsAdviceRecDelimiter)
        WriteXmlElement(writer, "_oriIsAdviceRecDelimiter", _oriIsAdviceRecDelimiter)
        WriteXmlElement(writer, "_curAdviceRecChar", _curAdviceRecChar)
        WriteXmlElement(writer, "_oldAdviceRecChar", _oldAdviceRecChar)
        WriteXmlElement(writer, "_oriAdviceRecChar", _oriAdviceRecChar)
        WriteXmlElement(writer, "_curAdviceRecDelimiter", _curAdviceRecDelimiter)
        WriteXmlElement(writer, "_oldAdviceRecDelimiter", _oldAdviceRecDelimiter)
        WriteXmlElement(writer, "_oriAdviceRecDelimiter", _oriAdviceRecDelimiter)
        WriteXmlElement(writer, "_curFilterCondition", _curFilterCondition)
        WriteXmlElement(writer, "_oldFilterCondition", _oldFilterCondition)
        WriteXmlElement(writer, "_oriFilterCondition", _oriFilterCondition)
        WriteXmlElement(writer, "_curIsFilterConditionAND", _curIsFilterConditionAND)
        WriteXmlElement(writer, "_oldIsFilterConditionAND", _oldIsFilterConditionAND)
        WriteXmlElement(writer, "_oriIsFilterConditionAND", _oriIsFilterConditionAND)
        WriteXmlElement(writer, "_curIsFilterConditionOR", _curIsFilterConditionOR)
        WriteXmlElement(writer, "_oldIsFilterConditionOR", _oldIsFilterConditionOR)
        WriteXmlElement(writer, "_oriIsFilterConditionOR", _oriIsFilterConditionOR)
        WriteXmlElement(writer, "_curFilterCondition", _curFilterCondition)
        WriteXmlElement(writer, "_oldFilterCondition", _oldFilterCondition)
        WriteXmlElement(writer, "_oriFilterCondition", _oriFilterCondition)
        _curFilters.WriteXml(writer)
        _curMapSourceFields.WriteXml(writer)
        _curMapBankFields.WriteXml(writer)
        _curTranslatorSettings.WriteXml(writer)
        _curEditableSettings.WriteXml(writer)
        _curLookupSettings.WriteXml(writer)
        _curCalculatedFields.WriteXml(writer)
        _curStringManipulations.WriteXml(writer)


        WriteXmlElement(writer, "_curPColumn", _curPColumn)
        WriteXmlElement(writer, "_oldPColumn", _oldPColumn)
        WriteXmlElement(writer, "_oriPColumn", _oriPColumn)
        WriteXmlElement(writer, "_curPIndicator", _curPIndicator)
        WriteXmlElement(writer, "_oldPIndicator", _oldPIndicator)
        WriteXmlElement(writer, "_oriPIndicator", _oriPIndicator)
        WriteXmlElement(writer, "_curWColumn", _curWColumn)
        WriteXmlElement(writer, "_oldWColumn", _oldWColumn)
        WriteXmlElement(writer, "_oriWColumn", _oriWColumn)
        WriteXmlElement(writer, "_curWIndicator", _curWIndicator)
        WriteXmlElement(writer, "_oldWIndicator", _oldWIndicator)
        WriteXmlElement(writer, "_oriWIndicator", _oriWIndicator)
        WriteXmlElement(writer, "_curIColumn", _curIColumn)
        WriteXmlElement(writer, "_oldIColumn", _oldIColumn)
        WriteXmlElement(writer, "_oriIColumn", _oriIColumn)
        WriteXmlElement(writer, "_curIIndicator", _curIIndicator)
        WriteXmlElement(writer, "_oldIIndicator", _oldIIndicator)
        WriteXmlElement(writer, "_oriIIndicator", _oriIIndicator)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves this object to given file
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(CommonTemplateExcelDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub

    ''' <summary>
    ''' Loads this object from given file
    ''' </summary>
    ''' <param name="filename">Name of file</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As CommonTemplateExcelDetail
        Dim serializer As New XmlSerializer(GetType(CommonTemplateExcelDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), CommonTemplateExcelDetail)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As CommonTemplateExcelDetail = DirectCast(obj, CommonTemplateExcelDetail)
        _curOutputTemplate = objE._curOutputTemplate
        _oldOutputTemplate = objE._oldOutputTemplate
        _oriOutputTemplate = objE._oriOutputTemplate
        _curCommonTemplateName = objE._curCommonTemplateName
        _oldCommonTemplateName = objE._oldCommonTemplateName
        _oriCommonTemplateName = objE._oriCommonTemplateName
        _curSampleSourceFile = objE._curSampleSourceFile
        _oldSampleSourceFile = objE._oldSampleSourceFile
        _oriSampleSourceFile = objE._oriSampleSourceFile
        _curIsEnabled = objE._curIsEnabled
        _oldIsEnabled = objE._oldIsEnabled
        _oriIsEnabled = objE._oriIsEnabled
        _curWorksheetName = objE._curWorksheetName
        _oldWorksheetName = objE._oldWorksheetName
        _oriWorksheetName = objE._oriWorksheetName
        _curWorksheets = objE._curWorksheets
        _curStartColumn = objE._curStartColumn
        _oldStartColumn = objE._oldStartColumn
        _oriStartColumn = objE._oriStartColumn
        _curEndColumn = objE._curEndColumn
        _oldEndColumn = objE._oldEndColumn
        _oriEndColumn = objE._oriEndColumn
        _curSampleRowNo = objE._curSampleRowNo
        _oldSampleRowNo = objE._oldSampleRowNo
        _oriSampleRowNo = objE._oriSampleRowNo
        _curHeaderAtRow = objE._curHeaderAtRow
        _oldHeaderAtRow = objE._oldHeaderAtRow
        _oriHeaderAtRow = objE._oriHeaderAtRow
        _curTransactionStartingRow = objE._curTransactionStartingRow
        _oldTransactionStartingRow = objE._oldTransactionStartingRow
        _oriTransactionStartingRow = objE._oriTransactionStartingRow
        _curHeaderAsFieldName = objE._curHeaderAsFieldName
        _oldHeaderAsFieldName = objE._oldHeaderAsFieldName
        _oriHeaderAsFieldName = objE._oriHeaderAsFieldName
        _curDecimalSeparator = objE._curDecimalSeparator
        _oldDecimalSeparator = objE._oldDecimalSeparator
        _oriDecimalSeparator = objE._oriDecimalSeparator
        _curThousandSeparator = objE._curThousandSeparator
        _oldThousandSeparator = objE._oldThousandSeparator
        _oriThousandSeparator = objE._oriThousandSeparator
        _curDateType = objE._curDateType
        _oldDateType = objE._oldDateType
        _oriDateType = objE._oriDateType
        _curDateSeparator = objE._curDateSeparator
        _oldDateSeparator = objE._oldDateSeparator
        _oriDateSeparator = objE._oriDateSeparator
        _curIsZerosInDate = objE._curIsZerosInDate
        _oldIsZerosInDate = objE._oldIsZerosInDate
        _oriIsZerosInDate = objE._oriIsZerosInDate
        _curReferenceField1 = objE._curReferenceField1
        _oldReferenceField1 = objE._oldReferenceField1
        _oriReferenceField1 = objE._oriReferenceField1
        _curReferenceField2 = objE._curReferenceField2
        _oldReferenceField2 = objE._oldReferenceField2
        _oriReferenceField2 = objE._oriReferenceField2
        _curReferenceField3 = objE._curReferenceField3
        _oldReferenceField3 = objE._oldReferenceField3
        _oriReferenceField3 = objE._oriReferenceField3
        _curRemittanceAmount = objE._curRemittanceAmount
        _oldRemittanceAmount = objE._oldRemittanceAmount
        _oriRemittanceAmount = objE._oriRemittanceAmount
        _curIsAmountInDollars = objE._curIsAmountInDollars
        _oldIsAmountInDollars = objE._oldIsAmountInDollars
        _oriIsAmountInDollars = objE._oriIsAmountInDollars
        _curIsAmountInCents = objE._curIsAmountInCents
        _oldIsAmountInCents = objE._oldIsAmountInCents
        _oriIsAmountInCents = objE._oriIsAmountInCents
        _curAdviceRecord = objE._curAdviceRecord
        _oldAdviceRecord = objE._oldAdviceRecord
        _oriAdviceRecord = objE._oriAdviceRecord
        _curIsAdviceRecForEveryRow = objE._curIsAdviceRecForEveryRow
        _oldIsAdviceRecForEveryRow = objE._oldIsAdviceRecForEveryRow
        _oriIsAdviceRecForEveryRow = objE._oriIsAdviceRecForEveryRow
        _curIsAdviceRecAfterEveryChar = objE._curIsAdviceRecAfterEveryChar
        _oldIsAdviceRecAfterEveryChar = objE._oldIsAdviceRecAfterEveryChar
        _oriIsAdviceRecAfterEveryChar = objE._oriIsAdviceRecAfterEveryChar
        _curIsAdviceRecDelimiter = objE._curIsAdviceRecDelimiter
        _oldIsAdviceRecDelimiter = objE._oldIsAdviceRecDelimiter
        _oriIsAdviceRecDelimiter = objE._oriIsAdviceRecDelimiter
        _curAdviceRecChar = objE._curAdviceRecChar
        _oldAdviceRecChar = objE._oldAdviceRecChar
        _oriAdviceRecChar = objE._oriAdviceRecChar
        _curAdviceRecDelimiter = objE._curAdviceRecDelimiter
        _oldAdviceRecDelimiter = objE._oldAdviceRecDelimiter
        _oriAdviceRecDelimiter = objE._oriAdviceRecDelimiter
        _curFilterCondition = objE._curFilterCondition
        _oldFilterCondition = objE._oldFilterCondition
        _oriFilterCondition = objE._oriFilterCondition
        _curIsFilterConditionAND = objE._curIsFilterConditionAND
        _oldIsFilterConditionAND = objE._oldIsFilterConditionAND
        _oriIsFilterConditionAND = objE._oriIsFilterConditionAND
        _curIsFilterConditionOR = objE._curIsFilterConditionOR
        _oldIsFilterConditionOR = objE._oldIsFilterConditionOR
        _oriIsFilterConditionOR = objE._oriIsFilterConditionOR
        _curFilterCondition = objE._curFilterCondition
        _oldFilterCondition = objE._oldFilterCondition
        _oriFilterCondition = objE._oriFilterCondition
        _curFilters = objE._curFilters
        _curMapSourceFields = objE._curMapSourceFields
        _curMapBankFields = objE._curMapBankFields
        _curTranslatorSettings = objE._curTranslatorSettings
        _curEditableSettings = objE._curEditableSettings
        _curLookupSettings = objE._curLookupSettings
        _curCalculatedFields = objE._curCalculatedFields
        _curStringManipulations = objE._curStringManipulations

        _curPColumn = objE._curPColumn
        _oldPColumn = objE._oldPColumn
        _oriPColumn = objE._oriPColumn
        _curPIndicator = objE._curPIndicator
        _oldPIndicator = objE._oldPIndicator
        _oriPIndicator = objE._oriPIndicator
        _curWColumn = objE._curWColumn
        _oldWColumn = objE._oldWColumn
        _oriWColumn = objE._oriWColumn
        _curWIndicator = objE._curWIndicator
        _oldWIndicator = objE._oldWIndicator
        _oriWIndicator = objE._oriWIndicator
        _curIColumn = objE._curIColumn
        _oldIColumn = objE._oldIColumn
        _oriIColumn = objE._oriIColumn
        _curIIndicator = objE._curIIndicator
        _oldIIndicator = objE._oldIIndicator
        _oriIIndicator = objE._oriIIndicator
        
    End Sub

    ''' <summary>
    ''' Validates the Fields of this template
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Validate()
        ValidationEngine.Validate()

        'Dim childWorksheets As Worksheets
        'For Each childWorksheets In _curWorksheets
        '    childWorksheets.Validate()
        'Next

        Dim childFilters As RowFilter
        For Each childFilters In _curFilters
            childFilters.Validate()
        Next

        Dim childMapSourceFields As MapSourceField
        For Each childMapSourceFields In _curMapSourceFields
            childMapSourceFields.Validate()
        Next

        Dim childMapBankFields As MapBankField
        For Each childMapBankFields In _curMapBankFields
            childMapBankFields.Validate()
        Next

        Dim childTranslatorSettings As TranslatorSetting
        For Each childTranslatorSettings In _curTranslatorSettings
            childTranslatorSettings.Validate()
        Next

        Dim childEditableSettings As EditableSetting
        For Each childEditableSettings In _curEditableSettings
            childEditableSettings.Validate()
        Next

        Dim childLookupSettings As LookupSetting
        For Each childLookupSettings In _curLookupSettings
            childLookupSettings.Validate()
        Next

        Dim childCalculatedFields As CalculatedField
        For Each childCalculatedFields In _curCalculatedFields
            childCalculatedFields.Validate()
        Next

        Dim childStringManipulations As StringManipulation
        For Each childStringManipulations In _curStringManipulations
            childStringManipulations.Validate()
        Next

    End Sub
    ''' <summary>
    ''' Determines whether the Template fields are valid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property IsValid() As Boolean
        Get
            If Not ValidationEngine.IsValid Then
                Return False
            End If

            'Dim childWorksheets As Worksheets
            'For Each childWorksheets In _curWorksheets
            '    If Not childWorksheets.IsValid Then
            '        Return False
            '    End If
            'Next

            Dim childFilters As RowFilter
            For Each childFilters In _curFilters
                If Not childFilters.IsValid Then
                    Return False
                End If
            Next

            Dim childMapSourceFields As MapSourceField
            For Each childMapSourceFields In _curMapSourceFields
                If Not childMapSourceFields.IsValid Then
                    Return False
                End If
            Next

            Dim childMapBankFields As MapBankField
            For Each childMapBankFields In _curMapBankFields
                If Not childMapBankFields.IsValid Then
                    Return False
                End If
            Next

            Dim childTranslatorSettings As TranslatorSetting
            For Each childTranslatorSettings In _curTranslatorSettings
                If Not childTranslatorSettings.IsValid Then
                    Return False
                End If
            Next

            Dim childEditableSettings As EditableSetting
            For Each childEditableSettings In _curEditableSettings
                If Not childEditableSettings.IsValid Then
                    Return False
                End If
            Next

            Dim childLookupSettings As LookupSetting
            For Each childLookupSettings In _curLookupSettings
                If Not childLookupSettings.IsValid Then
                    Return False
                End If
            Next

            Dim childCalculatedFields As CalculatedField
            For Each childCalculatedFields In _curCalculatedFields
                If Not childCalculatedFields.IsValid Then
                    Return False
                End If
            Next

            Dim childStringManipulations As StringManipulation
            For Each childStringManipulations In _curStringManipulations
                If Not childStringManipulations.IsValid Then
                    Return False
                End If
            Next



            Return True
        End Get
    End Property
#End Region
#Region " Save and Load 2 "
    ''' <summary>
    ''' Saves the template to filename and deletes the filetobedeleted, if given
    ''' </summary>
    ''' <param name="filename">Name of file to save the template</param>
    ''' <param name="filetobedeleted">file to delete upon saving template</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile2(ByVal filename As String, Optional ByVal filetobedeleted As String = "")

        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If

        If filetobedeleted.Trim().Length > 0 Then
            If File.Exists(filetobedeleted) Then File.Delete(filetobedeleted)
        End If

        Dim excelList As New MAGIC.CommonTemplate.CommonTemplateExcelList
        Dim fileTemplate As New BTMU.MAGIC.Common.MagicFileTemplate
        Dim excelDetailSerializer As New XmlSerializer(GetType(CommonTemplateExcelDetail))
        Dim excelListSerializer As New XmlSerializer(GetType(MAGIC.CommonTemplate.CommonTemplateExcelList))
        Dim fileTemplateSerializer As New XmlSerializer(GetType(BTMU.MAGIC.Common.MagicFileTemplate))
        Dim stream As New MemoryStream
        Dim streamFile As FileStream = File.Open(filename, FileMode.Create)

        excelList.CommonTemplate = CommonTemplateName
        excelList.OutputTemplate = OutputTemplate
        excelList.SourceFile = SampleSourceFile
        excelList.WorksheetName = WorksheetName
        excelList.IsEnabled = IsEnabled
        excelList.IsDraft = IsDraft

        excelDetailSerializer.Serialize(stream, Me)
        fileTemplate.DetailValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
        'fileTemplate.DetailValue = System.Text.Encoding.UTF8.GetString(stream.ToArray())
        stream.Close()

        stream = New MemoryStream
        excelListSerializer.Serialize(stream, excelList)
        fileTemplate.ListValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
        'fileTemplate.ListValue = System.Text.Encoding.UTF8.GetString(stream.ToArray())
        stream.Close()

        fileTemplateSerializer.Serialize(streamFile, fileTemplate)
        streamFile.Close()
    End Sub

    ''' <summary>
    ''' Loads the template from the given file
    ''' </summary>
    ''' <param name="filename">template file name</param>
    ''' <returns>Returns the Template loaded</returns>
    ''' <remarks></remarks>
    Public Function LoadFromFile2(ByVal filename As String) As CommonTemplateExcelDetail
        Dim lstDetail As New CommonTemplateExcelDetailCollection

        Dim serializer As New XmlSerializer(GetType(CommonTemplateExcelDetail))
        Dim content As String
        Dim doc As XPathDocument = New XPathDocument(filename)
        Dim nav As XPathNavigator = doc.CreateNavigator()
        Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/DetailValue")
        Dim item As New CommonTemplateExcelDetail
        Iterator.MoveNext()
        content = Iterator.Current.Value

        Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
        stream.Position = 0

        Dim xmlSetting As New System.Xml.XmlReaderSettings()
        xmlSetting.IgnoreWhitespace = False
        Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(stream, xmlSetting)

        item = CType(serializer.Deserialize(xmlReader), CommonTemplateExcelDetail)
        stream.Close()
        xmlReader.Close()

        DataContainer = item

        'Refresh the following bank field properties
        RefreshBankFields(item)

        Return item

    End Function

    Private Shared Sub RefreshBankFields(ByVal objtemplate As ICommonTemplate)

        'Sync these properties of BankFields (Common Template & Master Template)
        '1. DataLength
        '2. DefaultValue
        '3. DataSample
        '4. Mandatory
        '5. Map Separator
        '6. Include Decimal
        '7. Decimal Separator
        '8. Header
        '9. Detail
        '10. Trailer
        Dim _mstTmpUtility As New MasterTemplate.MasterTemplateSharedFunction

        If MasterTemplate.MasterTemplateSharedFunction.IsFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateFixedDetail In objmastertmp.MasterTemplateFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        'bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                        bkfield.DecimalSeparator = _masterFixedDetail.DecimalSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer
                        'bkfield.CarriageReturn = _masterFixedDetail.CarriageReturn

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        ElseIf MasterTemplate.MasterTemplateSharedFunction.IsNonFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateNonFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            'modified by Kay @ 7/6/2011
            'Add for consolidate field
            If objtemplate.MapBankFields.Count = objmastertmp.MasterTemplateNonFixedDetailCollection.Count - 1 Then
                Dim bkfield As New MapBankField
                bkfield.BankField = "Consolidate Field"
                bkfield.DataLength = 255
                bkfield.DataType = "Text"
                Select Case objmastertmp.OutputFormat
                    Case "iFTS-2 MultiLine", "iFTS-2 SingleLine"
                        bkfield.Detail = "Payment"
                    Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM"
                        bkfield.Detail = "Transaction"
                    Case Else
                        bkfield.Detail = "Yes"
                End Select

                objtemplate.MapBankFields.Add(bkfield)

            End If

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateNonFixedDetail In objmastertmp.MasterTemplateNonFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        'bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer
                        'bkfield.CarriageReturn = _masterFixedDetail.CarriageReturn

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        End If

    End Sub

#End Region

#Region " Public Methods "
    ''' <summary>
    ''' This function either returns a sample row as a datatable or the entire worksheet as a datatable
    ''' </summary>
    ''' <param name="IsNew"></param>
    ''' <param name="IsSampleRow"></param>
    ''' <returns>If IsSampleRow and if the file exists, the function returns Sample Row as datatable
    ''' If not IsSampleRow and if the file exists, the function returns entire worksheet as datatable.
    ''' If file doesnot exists, the function returns Sample Row as datatable.</returns>
    ''' <remarks></remarks>
    Public Function PopulateSampleDataGrid(Optional ByVal IsNew As Boolean = False, Optional ByVal IsSampleRow As Boolean = False, Optional ByVal IsFromCommonTemplate As Boolean = False) As DataTable
        Dim repeatedColumnNo As Integer
        Dim workSheetDataTable As New DataTable
        Dim worksheetDataTableCpy As DataTable

        Dim startCol As Integer
        Dim endCol As Integer
        'Dim ColNo As Integer
        Dim transactionStartRowNo As Integer
        Dim headerRowNo As Integer
        Dim SampleRow As Integer
        Dim RemoveRowNo As Integer = -1
        Try
            If Not System.IO.File.Exists(SampleSourceFile) Then
                Throw New MagicException(String.Format("Source File : {0} doesnot exists. Please check your file path", SampleSourceFile))
            End If

            ' #1.   Get the Worksheet Data Table
            workSheetDataTable = ExcelReaderInterop.ExcelOpenSpreadsheets(SampleSourceFile, WorksheetName, True)
            If workSheetDataTable Is Nothing Then Throw New MagicException("No record found.")
            For Each row As DataRow In workSheetDataTable.Rows

                For Each col As DataColumn In workSheetDataTable.Columns
                    If IsNothingOrEmptyString(row(col.ColumnName)) Then
                        row(col.ColumnName) = String.Empty
                    ElseIf row(col.ColumnName) Is DBNull.Value Then
                        row(col.ColumnName) = String.Empty
                    End If
                Next

            Next

            ' #2.   Copy the Worksheet Data Table
            worksheetDataTableCpy = workSheetDataTable.Copy

            ' #3.   Check the Start Column and End Column
            startCol = GetColumnIndex((StartColumn))
            endCol = GetColumnIndex((EndColumn))
            If startCol > worksheetDataTableCpy.Columns.Count Then
                Throw New MagicException("Start Column is out of XL Range")
            End If

            If endCol > worksheetDataTableCpy.Columns.Count Then
                ' Add the extra columns to the datatable
                For intI As Integer = worksheetDataTableCpy.Columns.Count To endCol - 1
                    worksheetDataTableCpy.Columns.Add("Field" & (intI + 1).ToString, GetType(String))
                Next
            End If

            ' #4.   Check the Header Row , Sample Row , Transaction Start Row
            headerRowNo = Convert.ToInt32(HeaderAtRow.Trim) - 1
            If headerRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException("Invalid Header Row No")
            End If

            SampleRow = Convert.ToInt32(SampleRowNo.Trim) - 1
            If SampleRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException("Invalid Sample Row No")
            End If

            transactionStartRowNo = Convert.ToInt32(TransactionStartingRow.Trim) - 1
            If transactionStartRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException("Invalid Transaction Start Row No")
            End If

            ' #5.   Get the columns from Start Column to End Column
            ' Remove the columns at the end till End Column from the DataTable
            If workSheetDataTable.Columns.Count <> endCol Then
                For intI As Integer = workSheetDataTable.Columns.Count - 1 To endCol Step -1
                    worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(intI))
                Next
            End If

            ' Remove the columns at the start till Start Column from the Datatable
            For intI As Integer = 1 To GetColumnIndex(StartColumn) - 1
                worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(0))
            Next

            ' #6.   Discard the original DataTable
            ' No need of this datatable, as the Datatable has been copied.
            workSheetDataTable = Nothing

            ' #7.   Get the Source Field Names from the HeaderRow
            Dim columnNames As New List(Of String)
            If HeaderAsFieldName Then
                ' Check the Header at Row
                If headerRowNo >= 0 Then
                    columnNames.Clear()
                    ' Get the header row and retrieve the field names
                    For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
                        repeatedColumnNo = 1
                        If worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim() = String.Empty Then
                            columnNames.Add("Field" & (intI + 1).ToString)
                            'worksheetDataTableCpy.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
                        Else
                            If IsUniqueColumn(columnNames, worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim()) Then
                                columnNames.Add(worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim())
                                'worksheetDataTableCpy.Columns(intI).ColumnName = worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim()
                            Else
                                ' Column Name is repeated.So need to generate a new column Name
                                columnNames.Add(worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim() & repeatedColumnNo.ToString)
                                'worksheetDataTableCpy.Columns(intI).ColumnName = worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString.Trim() & (intI + 1).ToString '& repeatedColumnNo.ToString
                                repeatedColumnNo += 1
                            End If
                        End If
                    Next

                Else
                    ' Generate the columnNames as Field1, Field2 etc
                    columnNames.Clear()
                    For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
                        columnNames.Add("Field" & (intI + 1).ToString)
                        'worksheetDataTableCpy.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
                    Next
                End If
            Else
                ' No need to show the header as field name
                ' Generate the columnNames as Field1, Field2 etc
                columnNames.Clear()
                For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
                    columnNames.Add("Field" & (intI + 1).ToString)
                    'worksheetDataTableCpy.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
                Next
            End If

            ' #7.   Return the Worksheet Data Table
            ' If need only SampleRow, return the datatable with only the Sample Row
            If IsSampleRow Then
                ' Return only the Sample Row
                Dim sampleDataTable As New DataTable
                'For Each _column As DataColumn In worksheetDataTableCpy.Columns
                '    sampleDataTable.Columns.Add(_column.ColumnName, GetType(String))
                'Next
                For Each columnName As String In columnNames
                    sampleDataTable.Columns.Add(columnName, GetType(String))
                Next
                sampleDataTable.Rows.Add(worksheetDataTableCpy.Rows(SampleRow).ItemArray)
                Return sampleDataTable
            Else
                Dim sampleDataTable As New DataTable
                For Each columnName As String In columnNames
                    sampleDataTable.Columns.Add(columnName, GetType(String))
                Next
                For intRow As Integer = 0 To worksheetDataTableCpy.Rows.Count - 1
                    sampleDataTable.Rows.Add(worksheetDataTableCpy.Rows(intRow).ItemArray)
                Next
                Return sampleDataTable
            End If

        Catch ex As MagicException
            Throw New MagicException(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(String.Format("Error reading Source File(PopulateSampleDataGrid) : {0}", ex.Message.ToString))
        End Try
    End Function

    ''' <summary>
    ''' This function is used to get one set of transaction which consist of Payment,Tax and Invoice records
    ''' based on the selected Payment Sample Row
    ''' </summary>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function PopulateSampleTransactionSet() As DataTable
        Dim workSheetDataTable As DataTable
        Dim sampleRow As Integer = 0
        Dim sampleTransactionDataTable As New DataTable
        Dim rowNo As Integer = 0
        Dim intPColumn As Integer = 0
        Dim intWColumn As Nullable(Of Integer)
        Dim intIColumn As Nullable(Of Integer)
        Dim sampleRowDataTable As New DataTable
        Dim sampleRecord As DataRow
        Dim sampleDataSet As New DataSet
        Dim taxDatarelation As DataRelation
        Dim invoiceDataRelation As DataRelation

        Try
            ' Step 1 : Read the worksheet into a datatable
            workSheetDataTable = GenerateWorksheetDataTable(SampleSourceFile, WorksheetName, 0, True)

            If workSheetDataTable Is Nothing Then Throw New MagicException("No record found.")

            ' Step 2 : Replace all dbnull value with empty string
            For Each row As DataRow In workSheetDataTable.Rows
                For Each col As DataColumn In workSheetDataTable.Columns
                    If row(col) Is DBNull.Value Then row(col) = ""
                Next
            Next

            ' Step 3 : Copy the datatable structure to sample transaction datatable
            sampleTransactionDataTable = workSheetDataTable.Copy
            sampleTransactionDataTable.Rows.Clear()

            ' Step 4 : Get the column index for the P-Column, W-Column, I-column
            intPColumn = GetColumnIndex(PColumn) - 1
            If Not IsNothingOrEmptyString(WColumn) Then intWColumn = GetColumnIndex(WColumn) - 1
            If Not IsNothingOrEmptyString(IColumn) Then intIColumn = GetColumnIndex(IColumn) - 1

            ' Step 5 : Verify the Record Type Columns
            If intPColumn > workSheetDataTable.Columns.Count Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "P Column", PColumn))
            End If
            If intWColumn.HasValue Then
                If intWColumn.Value > workSheetDataTable.Columns.Count Then
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "W Column", WColumn))
                End If
            End If

            If intIColumn.HasValue Then
                If intIColumn.Value > workSheetDataTable.Columns.Count Then
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "I Column", IColumn))
                End If
            End If

            ' Step 6 : Check whether the Sample Row selected is a valid P-record
            sampleRow = Convert.ToDecimal(SampleRowNo) - 1
            If Not IsNothingOrEmptyString(workSheetDataTable.Rows(sampleRow)(intPColumn)) Then
                If Not workSheetDataTable.Rows(sampleRow)(intPColumn).ToString.Equals(PIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNo))
                End If
            Else
                Throw New MagicException(String.Format(MsgReader.GetString("E02010070"), SampleRowNo))
            End If

            ' Step 7 : Get one transaction set
            sampleTransactionDataTable.Rows.Add(workSheetDataTable.Rows(sampleRow).ItemArray)
            For intRow As Integer = sampleRow + 1 To workSheetDataTable.Rows.Count - 1
                If PIndicator.Equals(workSheetDataTable.Rows(intRow)(intPColumn).ToString, StringComparison.InvariantCultureIgnoreCase) Then Exit For
                sampleTransactionDataTable.Rows.Add(workSheetDataTable.Rows(intRow).ItemArray)
            Next
            ' Step 8 : Populate the P, W, I source datatable from the transaction set
            GenerateSourceTransactionSet(sampleTransactionDataTable)

            ' Step 9 : Add the P, W, I datatable to a dataset
            _tblPaymentSrc.TableName = "Payment"
            _tblTaxSrc.TableName = "PaymentTax"
            _tblInvoiceSrc.TableName = "PaymentInvoice"
            sampleDataSet.Tables.Add(_tblPaymentSrc)

            If _tblTaxSrc.Rows.Count > 0 Then
                sampleDataSet.Tables.Add(_tblTaxSrc)
                taxDatarelation = New DataRelation("Tax", sampleDataSet.Tables("Payment").Columns("ID"), sampleDataSet.Tables("PaymentTax").Columns("ID"))
                sampleDataSet.Relations.Add(taxDatarelation)
            End If

            If _tblInvoiceSrc.Rows.Count > 0 Then
                sampleDataSet.Tables.Add(_tblInvoiceSrc)
                invoiceDataRelation = New DataRelation("Invoice", sampleDataSet.Tables("Payment").Columns("ID"), sampleDataSet.Tables("PaymentInvoice").Columns("ID"))
                sampleDataSet.Relations.Add(invoiceDataRelation)
            End If

            ' Step 10 : Add columns to the sampleRowDataTable
            For inti As Integer = 0 To sampleDataSet.Tables.Count - 1
                For Each column As DataColumn In sampleDataSet.Tables(inti).Columns
                    If column.ColumnName = "ID" Then Continue For
                    sampleRowDataTable.Columns.Add(column.ColumnName, GetType(String))
                Next
            Next

            ' Step 11 : Generate the sample row 
            sampleRecord = sampleRowDataTable.NewRow
            For Each column As DataColumn In sampleDataSet.Tables("Payment").Columns
                If column.ColumnName = "ID" Then Continue For
                sampleRecord(column.ColumnName) = sampleDataSet.Tables(0).Rows(0)(column).ToString
            Next

            ' Step 11.1 : Get the W-record
            If _tblTaxSrc.Rows.Count > 0 Then
                rowNo = 0
                For Each row As DataRow In sampleDataSet.Tables("Payment").Rows(0).GetChildRows("Tax")
                    If rowNo > 0 Then Exit For
                    For Each column As DataColumn In sampleDataSet.Tables("PaymentTax").Columns
                        If column.ColumnName = "ID" Then Continue For
                        sampleRecord(column.ColumnName) = row(column).ToString
                    Next
                    rowNo += 1
                Next
            End If

            ' Step 11.2 : Get the I-record
            If _tblInvoiceSrc.Rows.Count > 0 Then
                rowNo = 0
                For Each row As DataRow In sampleDataSet.Tables("Payment").Rows(0).GetChildRows("Invoice")
                    If rowNo > 0 Then Exit For
                    For Each column As DataColumn In sampleDataSet.Tables("PaymentInvoice").Columns
                        If column.ColumnName = "ID" Then Continue For
                        sampleRecord(column.ColumnName) = row(column).ToString
                    Next
                    rowNo += 1
                Next
            End If

            ' Step 11.3 : Add the record to the datatable
            sampleRowDataTable.Rows.Add(sampleRecord)

            Return sampleRowDataTable
        Catch ex As MagicException
            Throw New MagicException(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(String.Format("Error on getting sample transaction : {0}", ex.Message.ToString))
        End Try

        'TODO : Remove the commented code after UAT
        '' ''    GenerateSourceTransactionSet(worksheetDataTable)

        '' ''    sampleDataSet.Tables.Add(_tblPaymentSrc)
        '' ''    If _tblTaxSrc.Rows.Count > 0 Then
        '' ''        sampleDataSet.Tables.Add(_tblTaxSrc)
        '' ''        taxDataRelation = New DataRelation("Tax", sampleDataSet.Tables("Payment").Columns("ID"), sampleDataSet.Tables("PaymentTax").Columns("ID"))
        '' ''        sampleDataSet.Relations.Add(taxDataRelation)
        '' ''    End If

        '' ''    If _tblInvoiceSrc.Rows.Count > 0 Then
        '' ''        sampleDataSet.Tables.Add(_tblInvoiceSrc)
        '' ''        invoiceDataRelation = New DataRelation("Invoice", sampleDataSet.Tables("Payment").Columns("ID"), sampleDataSet.Tables("PaymentInvoice").Columns("ID"))
        '' ''        sampleDataSet.Relations.Add(invoiceDataRelation)
        '' ''    End If

        '' ''    'Adding columns to the sampleTransactionDataTable
        '' ''    For inti As Integer = 0 To sampleDataSet.Tables.Count - 1
        '' ''        For Each column As DataColumn In sampleDataSet.Tables(inti).Columns
        '' ''            If column.ColumnName = "ID" Then Continue For
        '' ''            sampleTransactionDataTable.Columns.Add(column.ColumnName, GetType(String))
        '' ''        Next
        '' ''    Next

        '' ''    sampleRow = Convert.ToInt32(SampleRowNo) - 1
        '' ''    If sampleRow > sampleDataSet.Tables(0).Rows.Count - 1 Then Throw New MagicException(MsgReader.GetString("E02010060"))
        '' ''    sampleRecord = sampleTransactionDataTable.NewRow
        '' ''    For Each column As DataColumn In sampleDataSet.Tables("Payment").Columns
        '' ''        If column.ColumnName = "ID" Then Continue For
        '' ''        sampleRecord(column.ColumnName) = sampleDataSet.Tables(0).Rows(sampleRow)(column).ToString
        '' ''    Next
        '' ''    'sampleTransactionDataTable.Rows.Add(sampleRecord)

        '' ''    'Get the W-record
        '' ''    If _tblTaxSrc.Rows.Count > 0 Then
        '' ''        rowNo = 0
        '' ''        For Each row As DataRow In sampleDataSet.Tables("Payment").Rows(sampleRow).GetChildRows("Tax")
        '' ''            If rowNo > 0 Then Exit For
        '' ''            'sampleRecord = sampleTransactionDataTable.NewRow
        '' ''            For Each column As DataColumn In sampleDataSet.Tables("PaymentTax").Columns
        '' ''                If column.ColumnName = "ID" Then Continue For
        '' ''                sampleRecord(column.ColumnName) = row(column).ToString
        '' ''            Next
        '' ''            rowNo += 1
        '' ''            'sampleTransactionDataTable.Rows.Add(sampleRecord)
        '' ''        Next
        '' ''    End If


        '' ''    'Get the I-record
        '' ''    If _tblInvoiceSrc.Rows.Count > 0 Then
        '' ''        rowNo = 0
        '' ''        For Each row As DataRow In sampleDataSet.Tables("Payment").Rows(sampleRow).GetChildRows("Invoice")
        '' ''            If rowNo > 0 Then Exit For
        '' ''            'sampleRecord = sampleTransactionDataTable.NewRow
        '' ''            For Each column As DataColumn In sampleDataSet.Tables("PaymentInvoice").Columns
        '' ''                If column.ColumnName = "ID" Then Continue For
        '' ''                sampleRecord(column.ColumnName) = row(column).ToString
        '' ''            Next
        '' ''            rowNo += 1
        '' ''        Next
        '' ''    End If

        '' ''    sampleTransactionDataTable.Rows.Add(sampleRecord)

        '' ''    For inti As Integer = sampleDataSet.Relations.Count - 1 To 0 Step -1
        '' ''        sampleDataSet.Relations.RemoveAt(inti)
        '' ''    Next
        '' ''    'sampleDataSet.Relations.Remove("Tax")
        '' ''    'sampleDataSet.Relations.Remove("Invoice")

        '' ''    'sampleDataSet.Tables.Remove(_pSourceData)
        '' ''    'sampleDataSet.Tables.Remove(_wSourceData)
        '' ''    'sampleDataSet.Tables.Remove(_iSourceData)
        '' ''    sampleDataSet.Dispose()
        '' ''Catch ex As MagicException
        '' ''    Throw New MagicException(ex.Message.ToString)
        '' ''Catch ex As Exception
        '' ''    Throw New MagicException(String.Format("Error populating Sample Transaction Set : {0}", ex.Message.ToString))
        '' ''End Try
        '' ''Return sampleTransactionDataTable
    End Function

    ''' <summary>
    ''' Generates a list of Source Fields from the source file
    ''' </summary>
    ''' <param name="worksheetDataTable">Data Table of Work sheet names</param>
    ''' <param name="IsNew">indicates whether the template source file is new</param>
    ''' <remarks></remarks>
    Public Sub PopulateSourceField(ByVal worksheetDataTable As DataTable, Optional ByVal IsNew As Boolean = False) ' Sample Row DataTable is used,which has only 1 row
        Dim _curMapSourceField As BTMU.MAGIC.CommonTemplate.MapSourceField
        Dim drSourceCol As DataColumn

        Try
            MapSourceFields.Clear()
            For Each drSourceCol In worksheetDataTable.Columns
                _curMapSourceField = New BTMU.MAGIC.CommonTemplate.MapSourceField
                _curMapSourceField.SourceFieldName = drSourceCol.ColumnName.ToString
                If IsDBNull(worksheetDataTable.Rows(0)(drSourceCol)) Then
                    _curMapSourceField.SourceFieldValue = String.Empty
                Else
                    If String.IsNullOrEmpty(worksheetDataTable.Rows(0)(drSourceCol)) Then
                        _curMapSourceField.SourceFieldValue = String.Empty
                    Else
                        _curMapSourceField.SourceFieldValue = worksheetDataTable.Rows(0)(drSourceCol)
                    End If
                End If
                MapSourceFields.Add(_curMapSourceField)
            Next


        Catch ex As Exception
            BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error populating Source Fields : ", ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Prepares the Template for previewing
    ''' </summary>
    ''' <param name="strOutputFileFormat">Name of Output File format</param>
    ''' <param name="SourceFile">Name of source file</param>
    ''' <param name="WorksheetName">Name of work sheet</param>
    ''' <param name="TransactionEndRow">Row number of last transaction record</param>
    ''' <param name="RemoveRowsFromEnd">The number of rows to be removed from the very last record in the Source File</param>
    ''' <remarks></remarks>
    Public Sub PrepareForPreview(Optional ByVal strOutputFileFormat As String = "", Optional ByVal SourceFile As String = "", Optional ByVal WorksheetName As String = "", Optional ByVal TransactionEndRow As Integer = 0, Optional ByVal RemoveRowsFromEnd As Integer = 0) Implements ICommonTemplate.PrepareForPreview
        Dim worksheetDatatable As DataTable
        '#1. Prepare SourceData
        'code to create datatable 
        _SourceData = New DataTable("SourceData")

        If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) Then
            worksheetDatatable = GenerateWorksheetDataTable(SourceFile, WorksheetName, TransactionEndRow, False)
            If worksheetDatatable Is Nothing Then Throw New MagicException("No source record found.")
            GenerateSourceTransactionSet(worksheetDatatable)
        Else
            

            For Each SourceField As MapSourceField In MapSourceFields
                _SourceData.Columns.Add(New DataColumn(SourceField.SourceFieldName, GetType(String)))
            Next

            '#2. Copy Source Rows
            GenerateSourceData(SourceFile, WorksheetName, TransactionEndRow)

            If _SourceData.Rows.Count = 0 Then Throw New MagicException("No source record is found.")
        End If

        Try
            Dim errorPrefix As String = "Record"
            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) Then
                _SourceData = _tblPaymentSrc.Copy()
                errorPrefix = "Payment Record"
            End If
            'If "iFTS-2 MultiLine".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) _
            'OrElse "Mr.Omakase India".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) Then
            '    _SourceData = _tblPaymentSrc.Copy()
            'End If

            '#3. Apply Reference Field Setting
            'ApplyReferenceFieldSetting(strOutputFileFormat)


            '#4. Apply Filters on Source Data
            _FilteredSourceData = _SourceData.DefaultView
            _FilteredSourceData.RowFilter = ""
            If Filters.Count > 0 Then ApplyRowFilters()

            If _FilteredSourceData.Count = 0 Then Throw New MagicException("No source record is found.")

            '#5 Format Date Values to Bank Field Date Format
            FormatDateValuesToBankFieldDateFormat(errorPrefix)

            '#6 Format Numeric Values to Bank Field Number Format
            FormatNumberValuesToBankFieldNumberFormat(errorPrefix)

        Catch ex As Exception
            Throw New MagicException(ex.Message)
        Finally
            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(strOutputFileFormat) Then
                _tblPaymentSrc = _SourceData.Copy()
                Dim strFilter As String = String.Empty

                For Each _row As DataRow In _tblPaymentSrc.Rows
                    strFilter &= IIf(strFilter = String.Empty, "", ",") & _row("ID").ToString()
                Next
                If Not IsNothingOrEmptyString(strFilter) Then strFilter = "ID in (" & strFilter & ")"

                Dim dv As DataView = _tblTaxSrc.DefaultView
                dv.RowFilter = strFilter
                _tblTaxSrc = dv.ToTable()

                dv = _tblInvoiceSrc.DefaultView
                dv.RowFilter = strFilter
                _tblInvoiceSrc = dv.ToTable()

                _SourceData = New DataTable("SourceData")
                _SourceData = _tblTaxSrc.Copy()
                FormatDateValuesToBankFieldDateFormat("Tax Record")
                FormatNumberValuesToBankFieldNumberFormat("Tax Record")
                _tblTaxSrc = _SourceData.Copy()
                _SourceData = New DataTable("SourceData")
                _SourceData = _tblInvoiceSrc.Copy()
                FormatDateValuesToBankFieldDateFormat("Invoice Record")
                FormatNumberValuesToBankFieldNumberFormat("Invoice Record")
                _tblInvoiceSrc = _SourceData.Copy()

            End If
            'If "iFTS-2 MultiLine".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) _
            'OrElse "Mr.Omakase India".Equals(_outputfileformat, StringComparison.InvariantCultureIgnoreCase) Then
            '    _tblPaymentSrc = _SourceData.Copy()
            'End If
        End Try
    End Sub
    Public Sub DuplicateCheck(Optional ByVal fileFormat As String = "") Implements ICommonTemplate.DuplicateCheck
        ApplyReferenceFieldSetting(fileFormat)
    End Sub
    Public Sub SetDefaultTranslation()
        OnPropertyChanged("OutputTemplate")
    End Sub
#End Region

#Region " Private Methods "
    'Private Sub ApplyRowFilters()
    '    Try
    '        '#4.1. '''Apply Date/Number Setting here''''''''''
    '        Dim origCulture = System.Threading.Thread.CurrentThread.CurrentCulture
    '        Dim newCulture As New System.Globalization.CultureInfo("en-US", True)
    '        newCulture.NumberFormat.NumberDecimalSeparator = IIf(DecimalSeparator = "No Separator", ".", DecimalSeparator)
    '        newCulture.NumberFormat.NumberGroupSeparator = IIf(ThousandSeparator = "No Separator", "", ThousandSeparator)
    '        newCulture.DateTimeFormat.ShortDatePattern = GetDateFormatPattern(DateSeparator, DateType)
    '        newCulture.DateTimeFormat.LongDatePattern = newCulture.DateTimeFormat.ShortDatePattern
    '        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
    '        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

    '        '#4.2. '''Rename  Columns referred in the Row Filter and add a column for each of the "Filter Type"s
    '        ' Make sure there is no duplicate column added for the duplicate filters

    '        For Each _flt As RowFilter In Filters

    '            If _SourceData.Columns.IndexOf(_flt.FilterField) > -1 AndAlso _SourceData.Columns.IndexOf(_flt.FilterField & "_0") > -1 Then Continue For

    '            If _flt.FilterType.Equals("Numeric", StringComparison.InvariantCultureIgnoreCase) Then
    '                _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
    '                _SourceData.Columns.Add(_flt.FilterField & "_0", GetType(System.Decimal))
    '            ElseIf _flt.FilterType.Equals("Date", StringComparison.InvariantCultureIgnoreCase) Then
    '                _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
    '                _SourceData.Columns.Add(_flt.FilterField & "_0", GetType(System.DateTime))
    '            End If

    '        Next


    '        '#4.3 ''' Convert and Copy values to Columns referred in Row Filter 
    '        ' a null value is assumed in the place where data conversion fails
    '        For Each row As DataRow In _SourceData.Rows
    '            For Each _flt As RowFilter In Filters
    '                Select Case _flt.FilterType.ToUpper()
    '                    Case "NUMERIC"
    '                        If IsDBNull(row(_flt.FilterField & "_0")) Then
    '                            row(_flt.FilterField) = DBNull.Value
    '                        Else
    '                            If Decimal.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
    '                                row(_flt.FilterField) = Convert.ToDecimal(row(_flt.FilterField & "_0"))
    '                            Else
    '                                row(_flt.FilterField) = DBNull.Value
    '                            End If
    '                        End If

    '                    Case "DATE"
    '                        If IsDBNull(row(_flt.FilterField & "_0")) Then
    '                            row(_flt.FilterField) = DBNull.Value
    '                        Else
    '                            If DateTime.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
    '                                row(_flt.FilterField) = Convert.ToDateTime(row(_flt.FilterField & "_0"))
    '                            Else
    '                                row(_flt.FilterField) = DBNull.Value
    '                            End If
    '                        End If

    '                End Select
    '            Next

    '        Next

    '        '#4.4. '''Filter the rows
    '        _FilteredSourceData.RowFilter = Filters.ConditionString(IIf(IsFilterConditionAND, "AND", "OR"))

    '        '#4.5. '''Remove Date/Number Setting '''''''''''''''''''''''''''''''''''''
    '        System.Threading.Thread.CurrentThread.CurrentCulture = origCulture

    '        '#4.6. '''Remove expression columns
    '        For Each _flt As RowFilter In Filters

    '            If _SourceData.Columns.IndexOf(_flt.FilterField) = -1 Or _SourceData.Columns.IndexOf(_flt.FilterField & "_0") = -1 Then Continue For

    '            _SourceData.Columns.Remove(_flt.FilterField)
    '            _SourceData.Columns(_flt.FilterField & "_0").ColumnName = _flt.FilterField

    '        Next


    '        'Just to please the bluddy DataView/DataTable data update behaviour!!!
    '        Dim tmpData As DataTable
    '        tmpData = _FilteredSourceData.ToTable()
    '        _FilteredSourceData.Dispose()
    '        _SourceData.Dispose()
    '        _SourceData = tmpData
    '        _FilteredSourceData = _SourceData.DefaultView
    '    Catch ex As Exception
    '        Throw New Exception("Error applying Filter Settings : " & vbNewLine & ex.Message.ToString)
    '    End Try
    'End Sub
    Private Sub ApplyRowFilters()
        'Try
        '    '#4.1. '''Apply Date/Number Setting here''''''''''
        '    Dim origCulture = System.Threading.Thread.CurrentThread.CurrentCulture
        '    Dim newCulture As New System.Globalization.CultureInfo("en-US", True)
        '    newCulture.NumberFormat.NumberDecimalSeparator = IIf(DecimalSeparator = "No Separator", "", DecimalSeparator)
        '    newCulture.NumberFormat.NumberGroupSeparator = IIf(ThousandSeparator = "No Separator", "", ThousandSeparator)
        '    newCulture.DateTimeFormat.ShortDatePattern = GetDateFormatPattern(DateSeparator, DateType)
        '    newCulture.DateTimeFormat.LongDatePattern = newCulture.DateTimeFormat.ShortDatePattern
        '    System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

        '    '#4.2. '''Rename  Columns referred in the Row Filter and add a column for each of the "Filter Type"s
        '    ' Make sure there is no duplicate column added for the duplicate filters

        '    For Each _flt As RowFilter In Filters

        '        If _SourceData.Columns.IndexOf(_flt.FilterField) > -1 AndAlso _SourceData.Columns.IndexOf(_flt.FilterField & "_0") > -1 Then Continue For

        '        If _flt.FilterType.Equals("Numeric", StringComparison.InvariantCultureIgnoreCase) Then
        '            _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
        '            _SourceData.Columns.Add(_flt.FilterField, GetType(System.Decimal))

        '        ElseIf _flt.FilterType.Equals("Date", StringComparison.InvariantCultureIgnoreCase) Then
        '            _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
        '            _SourceData.Columns.Add(_flt.FilterField, GetType(System.DateTime))
        '        End If

        '    Next




        '    '#4.3 ''' Convert and Copy values to Columns referred in Row Filter 
        '    ' a null value is assumed in the place where data conversion fails
        '    For Each row As DataRow In _SourceData.Rows

        '        For Each _flt As RowFilter In Filters
        '            Select Case _flt.FilterType.ToUpper()
        '                Case "NUMERIC"

        '                    If Decimal.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
        '                        row(_flt.FilterField) = Convert.ToDecimal(row(_flt.FilterField & "_0"))
        '                    Else
        '                        row(_flt.FilterField) = DBNull.Value
        '                    End If

        '                Case "DATE"

        '                    If DateTime.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
        '                        row(_flt.FilterField) = Convert.ToDateTime(row(_flt.FilterField & "_0"))
        '                    Else
        '                        row(_flt.FilterField) = DBNull.Value
        '                    End If

        '            End Select
        '        Next

        '    Next
        Try
            '#4.1. '''Apply Date/Number Setting here''''''''''
            Dim origCulture = System.Threading.Thread.CurrentThread.CurrentCulture
            Dim newCulture As New System.Globalization.CultureInfo("en-US", True)
            newCulture.NumberFormat.NumberDecimalSeparator = IIf(DecimalSeparator = "No Separator", ".", DecimalSeparator)
            newCulture.NumberFormat.NumberGroupSeparator = IIf(ThousandSeparator = "No Separator", "", ThousandSeparator)
            newCulture.DateTimeFormat.ShortDatePattern = GetDateFormatPattern(DateSeparator, DateType)
            newCulture.DateTimeFormat.LongDatePattern = newCulture.DateTimeFormat.ShortDatePattern
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

            '#4.2. '''Rename  Columns referred in the Row Filter and add a column for each of the "Filter Type"s
            ' Make sure there is no duplicate column added for the duplicate filters

            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) > -1 AndAlso _SourceData.Columns.IndexOf(_flt.FilterField & "_0") > -1 Then Continue For

                If _flt.FilterType.Equals("Numeric", StringComparison.InvariantCultureIgnoreCase) Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.Decimal))
                ElseIf _flt.FilterType.Equals("Date", StringComparison.InvariantCultureIgnoreCase) Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.DateTime))

                    Dim temp1 As DateTime
                    Dim temp2 As DateTime

                    If _flt.FilterValue Is Nothing Then
                    Else
                        Dim arDate As String() = System.Text.RegularExpressions.Regex.Split(_flt.FilterValue, "\|\|")

                        If arDate.Length > 0 AndAlso HelperModule.IsDateDataType(arDate(0), Me.DateSeparator, Me.DateType, temp1, Me.IsZerosInDate) Then
                            _flt.setTempDateString(temp1.ToString("yyyy/MM/dd"))
                        End If

                        If arDate.Length > 1 AndAlso HelperModule.IsDateDataType(arDate(1), Me.DateSeparator, Me.DateType, temp2, Me.IsZerosInDate) Then
                            _flt.setTempDateString(temp1.ToString("yyyy/MM/dd") & "||" & temp2.ToString("yyyy/MM/dd"))
                        End If
                    End If
                End If
            Next


            '#4.3 ''' Convert and Copy values to Columns referred in Row Filter 
            ' a null value is assumed in the place where data conversion fails
            For Each row As DataRow In _SourceData.Rows

                For Each _flt As RowFilter In Filters
                    Select Case _flt.FilterType.ToUpper()
                        Case "NUMERIC"

                            If Decimal.TryParse(IIf(row(_flt.FilterField & "_0") Is DBNull.Value, "", row(_flt.FilterField & "_0")), Nothing) Then
                                row(_flt.FilterField) = Convert.ToDecimal(row(_flt.FilterField & "_0"))
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                        Case "DATE"
                            Dim temp As DateTime

                            If HelperModule.IsDateDataType(IIf(row(_flt.FilterField & "_0") Is DBNull.Value, "", row(_flt.FilterField & "_0")), Me.DateSeparator, Me.DateType, temp, Me.IsZerosInDate) Then
                                row(_flt.FilterField) = temp
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                    End Select
                Next

            Next

            '#4.4. '''Filter the rows
            Dim shxtTableView As DataView
            shxtTableView = _SourceData.DefaultView
            shxtTableView.RowFilter = Filters.ConditionString(IIf(IsFilterConditionAND, "AND", "OR"))
            '#4.5. '''Remove Date/Number Setting '''''''''''''''''''''''''''''''''''''
            System.Threading.Thread.CurrentThread.CurrentCulture = origCulture

            '#4.6. '''Remove expression columns
            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) = -1 Or _SourceData.Columns.IndexOf(_flt.FilterField & "_0") = -1 Then Continue For

                _SourceData.Columns.Remove(_flt.FilterField)
                _SourceData.Columns(_flt.FilterField & "_0").ColumnName = _flt.FilterField

            Next

            'Just to please the bluddy DataView/DataTable data update behaviour!!!
            Dim tmpData As DataTable
            tmpData = shxtTableView.ToTable()
            shxtTableView.Dispose()
            _SourceData.Dispose()
            _SourceData = tmpData
            _FilteredSourceData = _SourceData.DefaultView

        Catch ex As Exception

            Throw New Exception("Error applying Filter Settings : " & vbNewLine & ex.Message.ToString)

        End Try

    End Sub
    Private Sub ApplyReferenceFieldSetting(ByVal fileFormat As String)
        'Dim rowNo As Integer = Convert.ToInt32(TransactionStartingRow) - 1
        Dim line As Integer = 0
        Dim record As String = ""

        Try

            If BTMU.MAGIC.Common.HelperModule.IsFileFormatUsingRecordTypes(fileFormat) Then
                record = "Payment Record"
            Else
                record = "Record"
            End If

            Dim strGroupByColumns As String = String.Format("{0},{1},{2}" _
                                        , ReferenceField1, ReferenceField2, ReferenceField3).Trim(",")

            Dim _intRecordCount As Int32 = _PreviewSourceData.Rows.Count

            If _intRecordCount = 0 Or strGroupByColumns = "" Then Exit Sub


            Dim _strFilter As String = ""
            Dim _dtviewTxnFieldGroup As DataView

            For _counter As Int32 = 0 To _intRecordCount - 1

                _strFilter = "1=1"

                If ReferenceField1 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(ReferenceField1), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(ReferenceField1)))
                If ReferenceField2 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(ReferenceField2), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(ReferenceField2)))
                If ReferenceField3 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(ReferenceField3), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(ReferenceField3)))

                _dtviewTxnFieldGroup = _PreviewSourceData.DefaultView
                _dtviewTxnFieldGroup.RowFilter = _strFilter

                If _dtviewTxnFieldGroup.Count > 1 Then
                    Dim stbError As New System.Text.StringBuilder
                    For Each dv As DataRowView In _dtviewTxnFieldGroup
                        If record.Equals("Payment Record", StringComparison.InvariantCultureIgnoreCase) Then
                            line = _PreviewSourceData.Rows.IndexOf(dv.Row) + 1
                        Else
                            line = _PreviewSourceData.Rows.IndexOf(dv.Row) + 1 'line = _PreviewSourceData.Rows.IndexOf(dv.Row) + (Convert.ToInt32(TransactionStartingRow) - 1) + 1
                        End If
                        If ReferenceField1 <> "" And ReferenceField2 <> "" And ReferenceField3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005020") _
                                           , line, ReferenceField1, ReferenceField2, ReferenceField3, record))
                        ElseIf ReferenceField1 <> "" And ReferenceField2 <> "" And ReferenceField3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005030") _
                                           , line, ReferenceField1, ReferenceField2, record))
                        ElseIf ReferenceField1 <> "" And ReferenceField2 = "" And ReferenceField3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005040") _
                                            , line, ReferenceField1, ReferenceField3, record))
                        ElseIf ReferenceField1 = "" And ReferenceField2 <> "" And ReferenceField3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005050") _
                                            , line, ReferenceField2, ReferenceField3, record))
                        ElseIf ReferenceField1 <> "" And ReferenceField2 = "" And ReferenceField3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005060") _
                                            , line, ReferenceField1, record))
                        ElseIf ReferenceField1 = "" And ReferenceField2 <> "" And ReferenceField3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005070") _
                                            , line, ReferenceField2, record))
                        ElseIf ReferenceField1 = "" And ReferenceField2 = "" And ReferenceField3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005080") _
                                            , line, ReferenceField3, record))

                        End If

                    Next
                    If stbError.ToString <> String.Empty Then
                        _dtviewTxnFieldGroup.RowFilter = String.Empty
                        Throw New Exception(stbError.ToString())
                    End If

                End If
                _dtviewTxnFieldGroup.RowFilter = String.Empty
                'rowNo += 1
            Next
        Catch ex As Exception
            Throw New Exception("Error applying Reference Field Settings : " & vbNewLine & ex.Message.ToString)
        End Try

    End Sub
    Private Sub FormatDateValuesToBankFieldDateFormat(ByVal errorPrefix As String)

        'a. Date Values in the Source Fields must comply to the date type specified in the Form <Retrieve Tab>
        'b. Convert the Date value to a date format as specified in the master template 'Date Format' property and update the Source Data Table

        Dim _convertedDateValue As DateTime
        Dim _dtMpSrcField As MapSourceField
        Dim _strBankFieldDateFormat As String
        Dim rowNo As Integer
        rowNo = Convert.ToInt32(TransactionStartingRow) - 1
        If Not errorPrefix.Equals("Record", StringComparison.InvariantCultureIgnoreCase) Then rowNo = 0
        For Each row As DataRow In _SourceData.Rows

            For Each col As DataColumn In _SourceData.Columns

                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = col.ColumnName


                If IsNothingOrEmptyString(row(col)) Then Continue For

                _dtMpSrcField.SourceFieldValue = row(col).ToString()


                'Convert Date value from the date format of Source File to that of Master Template
                'if the data type of bankfield that this field is mapped to is a DateTime 
                _strBankFieldDateFormat = GetBankFieldDateFormatForSourceField(_dtMpSrcField)
                If _strBankFieldDateFormat = "" Then Continue For

                If Not (Common.HelperModule.IsDateDataType(row(col).ToString(), DateSeparator, DateType, _convertedDateValue, IsZerosInDate)) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005100"), rowNo + 1, col.ColumnName, errorPrefix))

                'validate the source date format before converting it to master template date format
                row(col) = _convertedDateValue.ToString(_strBankFieldDateFormat, HelperModule.enUSCulture)

            Next
            rowNo += 1
        Next

    End Sub
    Private Sub FormatNumberValuesToBankFieldNumberFormat(ByVal errorPrefix As String)

        'a. Numeric Values in the Source Fields must comply to the 'Number Setting' specified in the Form <Retrieve Tab>
        'b. Convert the Numeric Values in the source fields to a Number format  that uses . as decimal separator and , as group separator 

        Dim _convertedNumberValue As Decimal
        Dim _dtMpSrcField As MapSourceField
        Dim rowNo As Integer
        Dim colNo As Integer
        rowNo = Convert.ToInt32(TransactionStartingRow) - 1
        If Not errorPrefix.Equals("Record", StringComparison.InvariantCultureIgnoreCase) Then rowNo = 0
        colNo = GetColumnIndex(StartColumn) - 1
        For Each row As DataRow In _SourceData.Rows

            For Each col As DataColumn In _SourceData.Columns

                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = col.ColumnName
                If IsNothingOrEmptyString(row(col)) Then Continue For

                _dtMpSrcField.SourceFieldValue = row(col).ToString()

                'Convert Number value from the Number format of Source File to that of Master Template
                'if the data type of bankfield that this field is mapped to is a Number or Currency 
                If Not IsMappedBankFieldDataTypeNumber(_dtMpSrcField) Then Continue For

                'Unless there is a value, no need to validate its value against numeric format
                If HelperModule.IsNothingOrEmptyString(row(col).ToString()) Then Continue For

                'Prefix the Negative Number Symbol '-', in case its found suffixed at the value
                If row(col).ToString().EndsWith("-") Then row(col) = "-" & row(col).ToString().Substring(0, row(col).Length - 1)

                If Not Common.HelperModule.IsDecimalDataType(row(col).ToString(), DecimalSeparator, ThousandSeparator, _convertedNumberValue) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005110"), rowNo + 1, col.ColumnName, errorPrefix))

                row(col) = _convertedNumberValue
                colNo += 1
            Next
            rowNo += 1
        Next

    End Sub
    ''' <summary>
    ''' Generates the Source Data Table.This function is called from Conversion Screen to preview the data
    ''' </summary>
    ''' <param name="sourceFileName">Source File Name</param>
    ''' <param name="sourceWorksheetName">Source Worksheet Name</param>
    ''' <param name="transactionEndRow">Transaction End Row</param>
    ''' <remarks></remarks>
    Private Sub GenerateSourceData(ByVal sourceFileName As String, ByVal sourceWorksheetName As String, ByVal transactionEndRow As Integer)
        Try
            If Not File.Exists(sourceFileName) Then Throw New Exception("Source File not found.Please check your file path.")
            Dim workSheetDataTable As DataTable
            Dim worksheetDataTableCpy As DataTable

            Dim endCol As Integer
            Dim startCol As Integer
            Dim originalColumnCount As Integer
            Dim transactionStartRow As Integer
            Dim RemoveRowNo As Integer = -1
            Dim originalEndRow As Integer

            ' #1.   Get the Worksheet Data Table
            workSheetDataTable = ExcelReaderInterop.ExcelOpenSpreadsheets(sourceFileName, sourceWorksheetName, True)



            If workSheetDataTable Is Nothing Then Exit Sub

            For Each row As DataRow In workSheetDataTable.Rows

                For Each col As DataColumn In workSheetDataTable.Columns
                    If IsNothingOrEmptyString(row(col.ColumnName)) Then
                        row(col.ColumnName) = String.Empty
                    ElseIf row(col.ColumnName) Is DBNull.Value Then
                        row(col.ColumnName) = String.Empty
                    End If
                Next

            Next

            ' #2.   Check the Start Column and End Column
            startCol = GetColumnIndex(StartColumn)
            endCol = GetColumnIndex(EndColumn)

            If startCol > workSheetDataTable.Columns.Count Then
                Throw New MagicException("Start Column Index is out of Range of XL Columns")
            End If

            ' #3.   Add the extra columns to the worksheetDataTable
            If endCol > workSheetDataTable.Columns.Count Then
                originalColumnCount = workSheetDataTable.Columns.Count
                For intI As Integer = originalColumnCount To endCol - 1
                    workSheetDataTable.Columns.Add("Field" & (intI + 1).ToString, GetType(String))
                Next
                ' Assign an empty string to the newly created columns
                For intI As Integer = 0 To workSheetDataTable.Rows.Count - 1
                    For intJ As Integer = originalColumnCount To workSheetDataTable.Columns.Count - 1
                        workSheetDataTable.Rows(intI)(intJ) = ""
                    Next
                Next
            End If

            ' #3.   If TransactionEndRow is 0, then the Preview is from Common Template
            originalEndRow = transactionEndRow
            If transactionEndRow = 0 Then transactionEndRow = workSheetDataTable.Rows.Count

            ' Validation - Transaction End Row : 'Transaction End Row' should be greater than 
            '                                    'Transaction Start' Row setting in the Common Template.
            transactionStartRow = Convert.ToInt32(TransactionStartingRow.Trim) - 1
            If transactionEndRow < transactionStartRow Then
                Throw New MagicException(MsgReader.GetString("E03010020"))
            End If

            If transactionEndRow > workSheetDataTable.Rows.Count Then Throw New MagicException("Transaction Ends at Row is greater than " & _
                                                                    "the total no:of records in the excel file.Please fill in Transaction Ends at Row again.")
            ' #2.   Copy the Worksheet Data Table
            worksheetDataTableCpy = workSheetDataTable.Copy

            ' #5.   Get the columns from Start Column to End Column
            ' Remove the columns at the end till End Column from the DataTable
            If workSheetDataTable.Columns.Count <> endCol Then
                For intI As Integer = workSheetDataTable.Columns.Count - 1 To endCol Step -1
                    worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(intI))
                Next
            End If
            ' Remove the columns at the start till Start Column from the Datatable
            For intI As Integer = 1 To startCol - 1
                worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(0))
            Next

            ' #6.   Discard the original DataTable
            ' No need of this datatable, as the Datatable has been copied.
            workSheetDataTable = Nothing

            ' Validation - Check whether the no:of columns in source file and commontemplate mapped source fields are the same. 
            ' Total fields in the Source file should be the same as total fields in the common template.
            If MapSourceFields.Count <> worksheetDataTableCpy.Columns.Count Then
                Throw New MagicException(MsgReader.GetString("E03000020"))
            End If

            ' '' '' #7.   Get the Source Field Names from the HeaderRow
            '' ''Dim columnNames As New List(Of String)
            '' ''If HeaderAsFieldName Then

            '' ''    repeatedColumnNo = 1
            '' ''    headerRowNo = Convert.ToInt32(HeaderAtRow.Trim) - 1
            '' ''    ' Check the Header at Row
            '' ''    If headerRowNo >= 0 Then
            '' ''        ' Get the header row and retrieve the field names

            '' ''        For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
            '' ''            If columnNames.IndexOf(worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString) = -1 Then
            '' ''                columnNames.Add(worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString)
            '' ''                worksheetDataTableCpy.Columns(intI).ColumnName = worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString
            '' ''            Else
            '' ''                ' Column Name is repeated.So need to generate a new column Name
            '' ''                columnNames.Add(worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString & repeatedColumnNo.ToString)
            '' ''                worksheetDataTableCpy.Columns(intI).ColumnName = worksheetDataTableCpy.Rows(headerRowNo)(intI).ToString & repeatedColumnNo.ToString
            '' ''                repeatedColumnNo += 1
            '' ''            End If
            '' ''        Next
            '' ''    Else
            '' ''        ' Generate the columnNames as Field1, Field2 etc
            '' ''        For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
            '' ''            worksheetDataTableCpy.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
            '' ''        Next
            '' ''    End If
            '' ''Else
            '' ''    ' No need to show the header as field name
            '' ''    ' Generate the columnNames as Field1, Field2 etc
            '' ''    For intI As Integer = 0 To worksheetDataTableCpy.Columns.Count - 1
            '' ''        worksheetDataTableCpy.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
            '' ''    Next
            '' ''End If


            ' '' '' #7.   Check whether the column names match the MapSourceField Names
            '' ''For Each _column As DataColumn In worksheetDataTableCpy.Columns
            '' ''    If Not IsSourceFieldFound(_column.ColumnName) Then
            '' ''        Throw New MagicException("Source Field not found")
            '' ''    End If
            '' ''Next


            ' #7.   Get the row from Transaction Start Row to End Row
            For intI As Integer = transactionStartRow To transactionEndRow - 1
                SourceData.Rows.Add(worksheetDataTableCpy.Rows(intI).ItemArray)
            Next
            '*********************************************************
            'GOT BUG!!!
            'Below code contras with iFTS2 Single Line Excel file without no data in the first few col.
            'REF >> SRS No. SRSBTMU2010-0057
            '*********************************************************
            ' TO remove all the rows that start with empty first column 
            ' Should not be attempted if its used from Manual/Quick Conversion as the no.of transaction rows are explicitly defined
            'If originalEndRow = 0 Then

            '    For intRow As Integer = 0 To SourceData.Rows.Count - 1
            '        If SourceData.Rows(intRow)(0).ToString.Trim = String.Empty Then
            '            RemoveRowNo = intRow
            '            Exit For
            '        End If
            '    Next
            '    If RemoveRowNo >= 0 Then
            '        For intI As Integer = SourceData.Rows.Count - 1 To RemoveRowNo Step -1
            '            SourceData.Rows.RemoveAt(intI)
            '        Next
            '    End If

            'End If


            worksheetDataTableCpy = Nothing

        Catch ex As MagicException
            Throw New MagicException(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error reading Source File(GenerateSourceData) : " & ex.Message.ToString)
        End Try
    End Sub
    ''' <summary>
    ''' This function is used to generate the P,W and I records into 3 different datatables
    ''' </summary>
    ''' <param name="worksheetDataTable">The whole excel worksheet datatable</param>
    ''' <remarks></remarks>
    Private Sub GenerateSourceTransactionSet(ByVal worksheetDataTable As DataTable)
        'TODO : Remove commented code after UAT
        'Dim recordArrayList As New ArrayList
        'Dim isEndOfRecordType As Boolean = False
        Dim currentRecordType As String = ""
        Dim pNo As Integer = 0
        Dim intPColumn As Integer = 0
        Dim intWColumn As Nullable(Of Integer)
        Dim intIColumn As Nullable(Of Integer)
        Dim currentRecord As DataRow
        Dim rowNo As Integer = 0

        ' Step 1 : Get the column index for the P-Column, W-Column, I-Column
        intPColumn = GetColumnIndex(PColumn) - 1
        If Not IsNothingOrEmptyString(WColumn) Then intWColumn = GetColumnIndex(WColumn) - 1
        If Not IsNothingOrEmptyString(IColumn) Then intIColumn = GetColumnIndex(IColumn) - 1

        ' Step 2 : Create one datatable each for P, W and I records
        _tblPaymentSrc = New DataTable("Payment")
        _tblTaxSrc = New DataTable("PaymentTax")
        _tblInvoiceSrc = New DataTable("PaymentInvoice")

        ' Step 3 : Add columns to the datatable
        _tblPaymentSrc.Columns.Add("ID", GetType(String))
        _tblTaxSrc.Columns.Add("ID", GetType(String))
        _tblInvoiceSrc.Columns.Add("ID", GetType(String))

        For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
            _tblPaymentSrc.Columns.Add("P-Field" & (intCol + 1).ToString, GetType(String))
        Next
        For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
            _tblTaxSrc.Columns.Add("W-Field" & (intCol + 1).ToString, GetType(String))
        Next
        For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
            _tblInvoiceSrc.Columns.Add("I-Field" & (intCol + 1).ToString, GetType(String))
        Next

        ' Step 4 :   Identify the record type for each row
        rowNo = Convert.ToInt32(TransactionStartingRow)

        ' Transaction Starting Row must be a valid P-record
        If IsNothingOrEmptyString(worksheetDataTable.Rows(0)(intPColumn)) Then
            Throw New MagicException("'Transaction Start Row' must be a valid P-record.")
        Else
            If Not PIndicator.Equals(worksheetDataTable.Rows(0)(intPColumn).ToString, StringComparison.InvariantCultureIgnoreCase) Then
                Throw New MagicException("'Transaction Start Row' must be a valid P-record.")
            End If
        End If

        For Each row As DataRow In worksheetDataTable.Rows
            If IsNothingOrEmptyString(row(intPColumn)) Then row(intPColumn) = ""
            If intWColumn.HasValue Then If IsNothingOrEmptyString(row(intWColumn.Value)) Then row(intWColumn.Value) = ""
            If intIColumn.HasValue Then If IsNothingOrEmptyString(row(intIColumn.Value)) Then row(intIColumn.Value) = ""

            currentRecordType = ""

            If intWColumn.HasValue AndAlso intIColumn.HasValue Then
                If row(intPColumn).ToString.Trim.Equals(PIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = PIndicator
                ElseIf row(intWColumn.Value).ToString.Trim.Equals(WIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = WIndicator
                ElseIf row(intIColumn.Value).ToString.Trim.Equals(IIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = IIndicator
                Else
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), rowNo))
                End If
            ElseIf intWColumn.HasValue Then
                If row(intPColumn).ToString.Trim.Equals(PIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = PIndicator
                ElseIf row(intWColumn.Value).ToString.Trim.Equals(WIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = WIndicator
                Else
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), rowNo))
                End If
            ElseIf intIColumn.HasValue Then
                If row(intPColumn).ToString.Trim.Equals(PIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = PIndicator
                ElseIf row(intIColumn.Value).ToString.Trim.Equals(IIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = IIndicator
                Else
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), rowNo))
                End If
            ElseIf Not (intWColumn.HasValue) AndAlso Not (intIColumn.HasValue) Then
                If row(intPColumn).ToString.Trim.Equals(PIndicator, StringComparison.InvariantCultureIgnoreCase) Then
                    currentRecordType = PIndicator
                Else
                    Throw New MagicException(String.Format(MsgReader.GetString("E02010080"), rowNo))
                End If
            End If

            Select Case currentRecordType
                Case PIndicator
                    pNo += 1
                    currentRecord = _tblPaymentSrc.NewRow
                    currentRecord(0) = pNo
                    For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
                        currentRecord(intCol + 1) = row(intCol).ToString
                    Next
                    _tblPaymentSrc.Rows.Add(currentRecord)

                Case WIndicator
                    currentRecord = _tblTaxSrc.NewRow
                    currentRecord(0) = pNo
                    For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
                        currentRecord(intCol + 1) = row(intCol).ToString
                    Next
                    _tblTaxSrc.Rows.Add(currentRecord)
                Case IIndicator
                    currentRecord = _tblInvoiceSrc.NewRow
                    currentRecord(0) = pNo
                    For intCol As Integer = 0 To worksheetDataTable.Columns.Count - 1
                        currentRecord(intCol + 1) = row(intCol).ToString
                    Next
                    _tblInvoiceSrc.Rows.Add(currentRecord)
            End Select
            rowNo += 1
        Next



        'TODO : Commented : Need to remove later.

        ' '' ''Step 3 :   Split the row based on the indicator found, and add the column data to an arraylist until the next indicator is found
        ' '' ''           One transaction set is till the next P record.Every transaction set should start with a P record
        '' ''For Each row As DataRow In worksheetDataTable.Rows
        '' ''    For Each col As DataColumn In worksheetDataTable.Columns
        '' ''        If recordArrayList.Count = 0 Then
        '' ''            ' Search for P-indicator
        '' ''            If row(col) Is DBNull.Value Then row(col) = ""
        '' ''            If row(col).ToString = PIndicator Then
        '' ''                recordArrayList.Add(row(col).ToString)
        '' ''                currentRecordType = row(col).ToString
        '' ''                pNo += 1
        '' ''            End If
        '' ''        Else
        '' ''            If Not currentRecordType = "" Then
        '' ''                If row(col) Is DBNull.Value Then row(col) = ""

        '' ''                If WIndicator <> "" And IIndicator <> "" Then
        '' ''                    If row(col).ToString = PIndicator OrElse row(col).ToString = WIndicator OrElse row(col).ToString = IIndicator Then
        '' ''                        isEndOfRecordType = True
        '' ''                    Else
        '' ''                        isEndOfRecordType = False
        '' ''                    End If
        '' ''                ElseIf WIndicator = "" And IIndicator = "" Then
        '' ''                    If row(col).ToString = PIndicator Then
        '' ''                        isEndOfRecordType = True
        '' ''                    Else
        '' ''                        isEndOfRecordType = False
        '' ''                    End If
        '' ''                ElseIf WIndicator <> "" And IIndicator = "" Then
        '' ''                    If row(col).ToString = PIndicator OrElse row(col).ToString = WIndicator Then
        '' ''                        isEndOfRecordType = True
        '' ''                    Else
        '' ''                        isEndOfRecordType = False
        '' ''                    End If
        '' ''                ElseIf WIndicator = "" And IIndicator <> "" Then
        '' ''                    If row(col).ToString = PIndicator OrElse row(col).ToString = IIndicator Then
        '' ''                        isEndOfRecordType = True
        '' ''                    Else
        '' ''                        isEndOfRecordType = False
        '' ''                    End If
        '' ''                End If

        '' ''                If isEndOfRecordType Then
        '' ''                    Select Case currentRecordType
        '' ''                        Case PIndicator
        '' ''                            ProcessArray(recordArrayList, currentRecordType, pNo, _tblPaymentSrc, "P-Field")
        '' ''                        Case WIndicator
        '' ''                            ProcessArray(recordArrayList, currentRecordType, pNo, _tblTaxSrc, "W-Field")
        '' ''                        Case IIndicator
        '' ''                            ProcessArray(recordArrayList, currentRecordType, pNo, _tblInvoiceSrc, "I-Field")
        '' ''                    End Select

        '' ''                    currentRecordType = (row(col).ToString)
        '' ''                    If currentRecordType = PIndicator Then pNo += 1
        '' ''                    recordArrayList.Clear()
        '' ''                    recordArrayList.Add(row(col).ToString)
        '' ''                Else
        '' ''                    recordArrayList.Add(row(col).ToString)
        '' ''                End If
        '' ''            End If

        '' ''        End If
        '' ''    Next
        '' ''Next

    End Sub
    Private Sub GetWorksheetNames(ByVal ExcelFile As String)
        ' Use Oledb to get the WorksheetNames
        ' This function is not used anywhere.
        Dim excelConnectionString As String
        Dim excelConnection As OleDbConnection
        Dim tempWorksheetDataTable As DataTable
        Dim tempWorksheetDataRow As DataRow
        Dim worksheetDataTable As DataTable
        Dim worksheetDataRow As DataRow
        Dim restrictions As String() = {Nothing, Nothing, Nothing, "TABLE"}

        If ExcelFile.Trim = String.Empty Then Exit Sub
        If Not System.IO.File.Exists(ExcelFile) Then
            Throw New Exception(String.Format("Excel File '{0}' doesnot exists. Please check the file path.", ExcelFile))
        End If

        If ExcelReaderInterop.IsFileOpen(ExcelFile) Then
            Exit Sub
        End If


        excelConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", ExcelFile)
        excelConnection = New OleDbConnection(excelConnectionString)
        excelConnection.Open()

        Try
            worksheetDataTable = New DataTable()
            worksheetDataTable.Columns.Add("WorksheetName")
            worksheetDataTable.Columns.Add("ReadableWorksheetName")

            tempWorksheetDataTable = excelConnection.GetSchema("Tables", restrictions)
            worksheetDataTable.Rows.Clear()
            For Each tempWorksheetDataRow In tempWorksheetDataTable.Rows

                If (Not tempWorksheetDataRow("TABLE_NAME").ToString().Replace("'", "").EndsWith("$")) Then
                Else
                    worksheetDataRow = worksheetDataTable.NewRow()

                    If tempWorksheetDataRow("TABLE_NAME").ToString.StartsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(0, 1)
                    End If

                    If tempWorksheetDataRow("TABLE_NAME").ToString.EndsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(tempWorksheetDataRow("TABLE_NAME").ToString.Length - 1, 1)
                    End If

                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Replace("''", "'")
                    worksheetDataRow("WorksheetName") = tempWorksheetDataRow("TABLE_NAME")
                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Remove(tempWorksheetDataRow("TABLE_NAME").ToString().Length - 1, 1)
                    worksheetDataRow("ReadableWorksheetName") = tempWorksheetDataRow("TABLE_NAME")
                    worksheetDataTable.Rows.Add(worksheetDataRow)
                    Dim worksheetObj As New MAGIC.CommonTemplate.ExcelWorksheet
                    worksheetObj.ReadableWorksheetName = worksheetDataRow("ReadableWorksheetName").ToString
                    worksheetObj.WorksheetName = worksheetDataRow("WorksheetName").ToString
                    Worksheets.Add(worksheetObj)
                End If
            Next
            ' bsWorksheetCollection.DataSource = worksheetDataTable

            If worksheetDataTable.Rows.Count > 0 Then
                WorksheetName = worksheetDataTable.Rows(0)(0)
            End If

        Catch ex As Exception
            BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error getting sample source file : ", ex.Message)
        Finally
            If excelConnection.State = ConnectionState.Open Then
                excelConnection.Close()
            End If
            excelConnection = Nothing
        End Try

    End Sub

    'TODO : Remove the commented Code after UAT
    '' ''Private Sub ProcessArray(ByVal recordArrayList As ArrayList, ByVal recordType As String, ByVal pNo As Integer, ByVal sourceDataTable As DataTable, ByVal fieldPrefix As String)
    '' ''    Dim colNo As Integer = 0
    '' ''    Dim pDataRow As DataRow
    '' ''    Dim fieldName As String = ""
    '' ''    'Remove the empty columns at the end of the arrayList
    '' ''    For intI As Integer = recordArrayList.Count - 1 To 0 Step -1
    '' ''        If recordArrayList.Item(intI).ToString = "" Then
    '' ''            recordArrayList.RemoveAt(intI)
    '' ''        Else
    '' ''            Exit For
    '' ''        End If
    '' ''    Next

    '' ''    recordArrayList.Insert(0, pNo.ToString)
    '' ''    If sourceDataTable.Columns.Count = 0 Then
    '' ''        'Create columns for the P-datatable
    '' ''        colNo = 0

    '' ''        For Each data As String In recordArrayList

    '' ''            If colNo = 0 Then
    '' ''                sourceDataTable.Columns.Add("ID", GetType(String))
    '' ''            Else
    '' ''                fieldName = fieldPrefix & colNo.ToString
    '' ''                sourceDataTable.Columns.Add(fieldName, GetType(String))
    '' ''            End If

    '' ''            colNo += 1

    '' ''        Next
    '' ''    Else
    '' ''        ' Every P,W,I should have the same no: of fields
    '' ''        If recordArrayList.Count > sourceDataTable.Columns.Count Then
    '' ''            For inti As Integer = sourceDataTable.Columns.Count To recordArrayList.Count - 1
    '' ''                fieldName = fieldPrefix & inti.ToString
    '' ''                sourceDataTable.Columns.Add(fieldName, GetType(String))
    '' ''            Next
    '' ''        End If
    '' ''    End If

    '' ''    'Add data to the datatable
    '' ''    pDataRow = sourceDataTable.NewRow
    '' ''    colNo = 0
    '' ''    For Each data As String In recordArrayList
    '' ''        pDataRow(colNo) = data
    '' ''        colNo += 1
    '' ''    Next
    '' ''    sourceDataTable.Rows.Add(pDataRow)

    '' ''End Sub

    Private Function GetBankFieldDateFormatForSourceField(ByVal SrcField As MapSourceField) As String

        For Each _bnkField As MapBankField In MapBankFields

            If Not _bnkField.DataType = "DateTime" Then Continue For

            If _bnkField.MappedSourceFields.IsFound(SrcField) Then Return _bnkField.DateFormat.Replace("D", "d").Replace("Y", "y")

        Next

        Return ""

    End Function
    Private Sub GenerateFieldNames(ByRef worksheetDataTable As DataTable)
        Dim repeatedColumnNo As Integer
        Dim headerRowNo As Integer = Convert.ToInt32(HeaderAtRow.Trim) - 1
        Dim columnNames As New List(Of String)

        If HeaderAsFieldName Then
            ' Check the Header at Row
            If headerRowNo >= 0 Then
                ' Get the header row and retrieve the field names
                For intI As Integer = 0 To worksheetDataTable.Columns.Count - 1
                    repeatedColumnNo = 1
                    If worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim() = String.Empty Then
                        'Throw New MagicException("Cannot retrieve field names from Header Row.Please check the Header Row")
                        worksheetDataTable.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
                    Else
                        If IsUniqueColumn(columnNames, worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim()) Then
                            columnNames.Add(worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim())
                            worksheetDataTable.Columns(intI).ColumnName = worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim()
                        Else
                            ' Column Name is repeated.So need to generate a new column Name
                            columnNames.Add(worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim() & repeatedColumnNo.ToString)
                            worksheetDataTable.Columns(intI).ColumnName = worksheetDataTable.Rows(headerRowNo)(intI).ToString.Trim() & (intI + 1).ToString '& repeatedColumnNo.ToString
                            repeatedColumnNo += 1
                        End If
                    End If
                Next
            Else
                ' Generate the columnNames as Field1, Field2 etc
                For intI As Integer = 0 To worksheetDataTable.Columns.Count - 1
                    worksheetDataTable.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
                Next
            End If
        Else
            ' No need to show the header as field name
            ' Generate the columnNames as Field1, Field2 etc
            For intI As Integer = 0 To worksheetDataTable.Columns.Count - 1
                worksheetDataTable.Columns(intI).ColumnName = "Field" & (intI + 1).ToString
            Next
        End If
    End Sub
    ''' <summary>
    ''' This function is used to get the desired range of data from an excel spreadsheet.
    ''' </summary>
    ''' <param name="sourceFile">The Source File Name from which to extract data</param>
    ''' <param name="worksheet">The worksheet name to be used</param>
    ''' <param name="transactionEndsAtRow">The Row No which denotes the end of transaction</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function GenerateWorksheetDataTable(ByVal sourceFile As String, ByVal worksheet As String, ByVal transactionEndsAtRow As Integer, ByVal isSampleRow As Boolean) As DataTable
        Dim workSheetDataTable As New DataTable
        Dim worksheetDataTableCpy As DataTable

        Dim startCol As Integer
        Dim endCol As Integer
        Dim transactionStartRowNo As Integer
        Dim headerRowNo As Integer
        Dim SampleRow As Integer
        Dim RemoveRowNo As Integer = -1
        Dim originalEndRow As Integer

        Try
            ' Step 1 : Check if the source file exist.
            If Not System.IO.File.Exists(sourceFile) Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010090"), sourceFile))
            End If

            ' Step 2 : Get the Worksheet Data Table
            workSheetDataTable = ExcelReaderInterop.ExcelOpenSpreadsheets(sourceFile, worksheet, True)

            For Each row As DataRow In workSheetDataTable.Rows

                For Each col As DataColumn In workSheetDataTable.Columns
                    If IsNothingOrEmptyString(row(col.ColumnName)) Then
                        row(col.ColumnName) = String.Empty
                    ElseIf row(col.ColumnName) Is DBNull.Value Then
                        row(col.ColumnName) = String.Empty
                    End If
                Next

            Next

            ' Step 3 : Copy the Worksheet Data Table
            worksheetDataTableCpy = workSheetDataTable.Copy

            ' Step 4 : Get the Start and End Column index
            startCol = GetColumnIndex((StartColumn))
            endCol = GetColumnIndex((EndColumn))

            ' Step 5 : Check if the Start and End Column is within the excel column range
            If startCol > worksheetDataTableCpy.Columns.Count Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "Start Column", StartColumn))
            End If

            If endCol > worksheetDataTableCpy.Columns.Count Then
                ' Add the extra columns to the datatable
                For intI As Integer = worksheetDataTableCpy.Columns.Count To endCol - 1
                    worksheetDataTableCpy.Columns.Add("Field" & (intI + 1).ToString, GetType(String))
                Next
            End If

            ' Step 6 : Check if Header Row , Sample Row , Transaction Start Row is with the excel row range
            headerRowNo = Convert.ToInt32(HeaderAtRow.Trim) - 1
            If headerRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "Header at Row", HeaderAtRow))
            End If

            SampleRow = Convert.ToInt32(SampleRowNo.Trim) - 1
            If SampleRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "Sample Row No", SampleRowNo))
            End If

            transactionStartRowNo = Convert.ToInt32(TransactionStartingRow.Trim) - 1
            If transactionStartRowNo > worksheetDataTableCpy.Rows.Count Then
                Throw New MagicException(String.Format(MsgReader.GetString("E02010110"), "Transaction Start Row", TransactionStartingRow))
            End If



            ' Step 7 : Remove all the columns from the datatable after the end column
            If endCol < workSheetDataTable.Columns.Count Then
                For intI As Integer = workSheetDataTable.Columns.Count - 1 To endCol Step -1
                    worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(intI))
                Next
            End If

            ' Step 8 : Remove all the columns from the Datatable before the Start Column
            For intI As Integer = 1 To GetColumnIndex(StartColumn) - 1
                worksheetDataTableCpy.Columns.Remove(worksheetDataTableCpy.Columns(0))
            Next

            If isSampleRow Then Return worksheetDataTableCpy

            ' Step 9 : Remove all the rows from the datatable after the Transaction Ends At Row
            ' If the Preview is from Common Template, Transaction Ends At Row is 0.
            originalEndRow = transactionEndsAtRow
            If transactionEndsAtRow = 0 Then transactionEndsAtRow = worksheetDataTableCpy.Rows.Count
            If transactionEndsAtRow > workSheetDataTable.Rows.Count Then Throw New MagicException("Transaction Ends at Row is greater than " & _
                                            "the total no:of records in the excel file.Please fill in Transaction Ends at Row again.")

            If transactionEndsAtRow < worksheetDataTableCpy.Rows.Count Then
                For intI As Integer = worksheetDataTableCpy.Rows.Count - 1 To transactionEndsAtRow Step -1
                    worksheetDataTableCpy.Rows.RemoveAt(intI)
                Next
            End If

            ' Step 10 : Remove all the rows from the datatable before the Transaction Starting Row
            For intI As Integer = 0 To transactionStartRowNo - 1
                worksheetDataTableCpy.Rows.RemoveAt(0)
            Next

            ' Step 11 : Clean up
            workSheetDataTable = Nothing

            

            Return worksheetDataTableCpy

        Catch ex As MagicException
            Throw New MagicException(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception(String.Format("Error reading Source File : {0}", ex.Message.ToString))
        End Try
    End Function
    Private Function IsMappedBankFieldDataTypeNumber(ByVal SrcField As MapSourceField) As Boolean

        For Each _bnkField As MapBankField In MapBankFields

            If Not (_bnkField.DataType = "Number" Or _bnkField.DataType = "Currency") Then Continue For

            If _bnkField.MappedSourceFields.IsFound(SrcField) Then Return True

        Next

        Return False

    End Function
    Private Function IsSourceFieldFound(ByVal FieldName As String) As Boolean
        Dim isFound As Boolean = False
        For Each sourcefield As MapSourceField In MapSourceFields
            If sourcefield.SourceFieldName.ToUpper = FieldName.ToUpper Then
                isFound = True
                Exit For
            End If
        Next
        Return isFound
    End Function
    Private Function IsUniqueColumn(ByVal columns As List(Of String), ByVal column As String) As Boolean

        For Each strColumn As String In columns
            If strColumn.Equals(column, StringComparison.InvariantCultureIgnoreCase) Then
                Return False
            End If
        Next

        Return True

    End Function

#End Region

End Class

''' <summary>
''' Collection of Common Transaction Template for Excel File
''' </summary>
''' <remarks></remarks>
Partial Public Class CommonTemplateExcelDetailCollection
    'Inherits System.ComponentModel.BindingList(Of CommonTemplateExcelDetail)
    Inherits BusinessBaseCollection(Of CommonTemplateExcelDetail)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of CommonTemplateExcelDetail)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of CommonTemplateExcelDetail)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of CommonTemplateExcelDetail)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As CommonTemplateExcelDetail
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("CommonTemplateExcelDetailCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "CommonTemplateExcelDetail"
            child = New CommonTemplateExcelDetail
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As CommonTemplateExcelDetail
        writer.WriteStartElement("CommonTemplateExcelDetailCollection")
        For Each child In Me
            writer.WriteStartElement("CommonTemplateExcelDetail")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
