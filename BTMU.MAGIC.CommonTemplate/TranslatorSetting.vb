Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Translator Settings
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class TranslatorSetting
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curBankFieldName As String
    Private _oldBankFieldName As String
    Private _oriBankFieldName As String
    Private _curCustomerValue As String
    Private _oldCustomerValue As String
    Private _oriCustomerValue As String
    Private _curBankValue As String
    Private _oldBankValue As String
    Private _oriBankValue As String
    Private _curBankValueEmpty As Boolean
    Private _oldBankValueEmpty As Boolean
    Private _oriBankValueEmpty As Boolean
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
    Private _defaultValues As String
#End Region
#Region " Public Property "
    ''' <summary>
    ''' Default Values of a Bank Field
    ''' </summary>
    ''' <value>Comma Separated list of Default values</value>
    ''' <returns>Returns Default Values</returns>
    ''' <remarks></remarks>
    Public Property DefaultValues() As String
        Get
            Return _defaultValues
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _defaultValues <> value Then
                _defaultValues = value
                OnPropertyChanged("DefaultValues")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Sample Values of Bank Field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DefaultBankValues() As String()
        Get
            Return _defaultValues.Split(",")
        End Get
    End Property

    'Public Property BankFieldName() As String
    '    Get
    '        Return _curBankFieldName
    '    End Get
    '    Set(ByVal value As String)
    '        If _curBankFieldName <> value Then
    '            _curBankFieldName = value
    '            OnPropertyChanged("BankFieldName")
    '            SetDefaultCustomerValue(value)
    '        End If
    '    End Set
    'End Property
    ''' <summary>
    ''' This property is to Get/Set Customer Value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomerValue() As String
        Get
            Return _curCustomerValue
        End Get
        Set(ByVal value As String)
            ' If Not value Is Nothing Then value = value.Trim()

            If _curCustomerValue <> value Then
                _curCustomerValue = value
                OnPropertyChanged("CustomerValue")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the bank value is empty
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>returns a boolean value indicating whether the bank value is empty</returns>
    ''' <remarks></remarks>
    Public Property BankValueEmpty() As Boolean
        Get
            Return _curBankValueEmpty
        End Get
        Set(ByVal value As Boolean)
            If _curBankValueEmpty <> value Then
                _curBankValueEmpty = value
                OnPropertyChanged("BankValueEmpty")
            End If
        End Set

    End Property
    ''' <summary>
    ''' This property is to Set/Get Bank Value
    ''' </summary>
    ''' <value>Bank Value</value>
    ''' <returns>Returns the Bank Value</returns>
    ''' <remarks></remarks>
    Public Property BankValue() As String
        Get
            Return _curBankValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankValue <> value Then
                _curBankValue = value
                OnPropertyChanged("BankValue")
            End If
        End Set
    End Property

    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' This Property is to Get/Set the Bank Field Name
    ''' </summary>
    ''' <value>Bank Field Name</value>
    ''' <returns>returns Bank Field Name</returns>
    ''' <remarks></remarks>
    Public Property BankFieldName() As String
        Get
            Return _curBankFieldName
        End Get
        Set(ByVal value As String)

            If Not value Is Nothing Then value = value.Trim()


            Dim _DataModel As ICommonTemplate = Nothing

            If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
                _DataModel = CommonTemplateTextDetail.DataContainer
            ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
                _DataModel = CommonTemplateExcelDetail.DataContainer
            ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.TranslatorSettings Is Me.MyCollection Then
                _DataModel = SwiftTemplate.DataContainer
            End If

            If _curBankFieldName <> value Then
                Dim temp As String = _curBankFieldName

                _curBankFieldName = value
                OnPropertyChanged("BankFieldName")
                SetDefaultCustomerValue(value)

                If _DataModel IsNot Nothing Then
                    For Each _objItem As MapBankField In _DataModel.MapBankFields
                        If _objItem.BankField = value Or _objItem.BankField = temp Then
                            _objItem.Validate("BankFieldExt")
                        End If
                    Next
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' Apply the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriBankFieldName = _curBankFieldName
        _oriCustomerValue = _curCustomerValue
        _oriBankValue = _curBankValue
    End Sub
    ''' <summary>
    ''' Begin changes
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldBankFieldName = _curBankFieldName
        _oldCustomerValue = _curCustomerValue
        _oldBankValue = _curBankValue
    End Sub
    ''' <summary>
    ''' Cancels the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curBankFieldName = _oldBankFieldName
        _curCustomerValue = _oldCustomerValue
        _curBankValue = _oldBankValue
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankFieldName = ReadXMLElement(reader, "_curBankFieldName")
            _oldBankFieldName = ReadXMLElement(reader, "_oldBankFieldName")
            _oriBankFieldName = ReadXMLElement(reader, "_oriBankFieldName")
            _curCustomerValue = ReadXMLElement(reader, "_curCustomerValue")
            _oldCustomerValue = ReadXMLElement(reader, "_oldCustomerValue")
            _oriCustomerValue = ReadXMLElement(reader, "_oriCustomerValue")
            _curBankValue = ReadXMLElement(reader, "_curBankValue")
            _oldBankValue = ReadXMLElement(reader, "_oldBankValue")
            _oriBankValue = ReadXMLElement(reader, "_oriBankValue")
            _curBankValueEmpty = ReadXMLElement(reader, "_curBankValueEmpty")
            _oldBankValueEmpty = ReadXMLElement(reader, "_oldBankValueEmpty")
            _oriBankValueEmpty = ReadXMLElement(reader, "_oriBankValueEmpty")
            _defaultValues = ReadXMLElement(reader, "_defaultValues")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankFieldName", _curBankFieldName)
        WriteXmlElement(writer, "_oldBankFieldName", _oldBankFieldName)
        WriteXmlElement(writer, "_oriBankFieldName", _oriBankFieldName)
        WriteXmlElement(writer, "_curCustomerValue", _curCustomerValue)
        WriteXmlElement(writer, "_oldCustomerValue", _oldCustomerValue)
        WriteXmlElement(writer, "_oriCustomerValue", _oriCustomerValue)
        WriteXmlElement(writer, "_curBankValue", _curBankValue)
        WriteXmlElement(writer, "_oldBankValue", _oldBankValue)
        WriteXmlElement(writer, "_oriBankValue", _oriBankValue)
        WriteXmlElement(writer, "_curBankValueEmpty", _curBankValueEmpty)
        WriteXmlElement(writer, "_oldBankValueEmpty", _oldBankValueEmpty)
        WriteXmlElement(writer, "_oriBankValueEmpty", _oriBankValueEmpty)
        WriteXmlElement(writer, "_defaultValues", _defaultValues)

    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">name of the file</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(TranslatorSetting))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the objecdt from the given file
    ''' </summary>
    ''' <param name="filename">filename</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As TranslatorSetting
        Dim serializer As New XmlSerializer(GetType(TranslatorSetting))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), TranslatorSetting)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As TranslatorSetting = DirectCast(obj, TranslatorSetting)
        _curBankFieldName = objE._curBankFieldName
        _oldBankFieldName = objE._oldBankFieldName
        _oriBankFieldName = objE._oriBankFieldName
        _curCustomerValue = objE._curCustomerValue
        _oldCustomerValue = objE._oldCustomerValue
        _oriCustomerValue = objE._oriCustomerValue
        _curBankValue = objE._curBankValue
        _oldBankValue = objE._oldBankValue
        _oriBankValue = objE._oriBankValue

        _curBankValueEmpty = objE._curBankValueEmpty
        _oldBankValueEmpty = objE._oldBankValueEmpty
        _oriBankValueEmpty = objE._oriBankValueEmpty

        _defaultValues = objE._defaultValues
    End Sub
#End Region

    ''' <summary>
    ''' Sets a Default Customer Value for the given bank field
    ''' </summary>
    ''' <param name="_bankFieldName">Name of Bank Field</param>
    ''' <remarks></remarks>
    Public Sub SetDefaultCustomerValue(ByVal _bankFieldName As String)
        Dim _DataModel As ICommonTemplate = Nothing

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        End If

        If Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        End If

        If Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        End If

        If _DataModel Is Nothing Then
            Exit Sub
        End If

        For Each bankfield As MapBankField In _DataModel.MapBankFields
            If bankfield.BankField = _bankFieldName Then
                CustomerValue = bankfield.MappedValues
                Exit Sub
            End If
        Next

        OnPropertyChanged("CustomerValue")

    End Sub

End Class

''' <summary>
''' Collection of Translator Settings
''' </summary>
''' <remarks></remarks>
Partial Public Class TranslatorSettingCollection
    Inherits BusinessBaseCollection(Of TranslatorSetting)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of TranslatorSetting)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of TranslatorSetting)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of TranslatorSetting)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As TranslatorSetting
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("TranslatorSettingCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "TranslatorSetting"
            child = New TranslatorSetting
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As TranslatorSetting
        writer.WriteStartElement("TranslatorSettingCollection")
        For Each child In Me
            writer.WriteStartElement("TranslatorSetting")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
