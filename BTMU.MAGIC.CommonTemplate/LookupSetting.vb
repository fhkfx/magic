Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Lookup Settings
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class LookupSetting
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curBankFieldName As String
    Private _oldBankFieldName As String
    Private _oriBankFieldName As String
    Private _curTableName As String
    Private _oldTableName As String
    Private _oriTableName As String
    Private _curSourceFieldName As String
    Private _oldSourceFieldName As String
    Private _oriSourceFieldName As String
    Private _curLookupKey As String
    Private _oldLookupKey As String
    Private _oriLookupKey As String
    Private _curLookupValue As String
    Private _oldLookupValue As String
    Private _oriLookupValue As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime

    Private _curTableName2 As String
    Private _oldTableName2 As String
    Private _oriTableName2 As String
    Private _curLookupKey2 As String
    Private _oldLookupKey2 As String
    Private _oriLookupKey2 As String
    Private _curLookupValue2 As String
    Private _oldLookupValue2 As String
    Private _oriLookupValue2 As String

    Private _curTableName3 As String
    Private _oldTableName3 As String
    Private _oriTableName3 As String
    Private _curLookupKey3 As String
    Private _oldLookupKey3 As String
    Private _oriLookupKey3 As String
    Private _curLookupValue3 As String
    Private _oldLookupValue3 As String
    Private _oriLookupValue3 As String
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This Property is to Get/Set the Bank Field Name
    ''' </summary>
    ''' <value>Bank Field Name</value>
    ''' <returns>returns Bank Field Name</returns>
    ''' <remarks></remarks>
    Public Property BankFieldName() As String
        Get
            Return _curBankFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankFieldName <> value Then
                _curBankFieldName = value
                OnPropertyChanged("BankFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Table Name
    ''' </summary>
    ''' <value>Table Name</value>
    ''' <returns>returns Table Name</returns>
    ''' <remarks></remarks>
    Public Property TableName() As String
        Get
            Return _curTableName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curTableName <> value Then
                _curTableName = value
                OnPropertyChanged("TableName")
            End If
        End Set
    End Property
    Public Property TableName2() As String
        Get
            Return _curTableName2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curTableName2 <> value Then
                _curTableName2 = value
                OnPropertyChanged("TableName2")
            End If
        End Set
    End Property
    Public Property TableName3() As String
        Get
            Return _curTableName3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curTableName3 <> value Then
                _curTableName3 = value
                OnPropertyChanged("TableName3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Source Field Name
    ''' </summary>
    ''' <value>Source Field Name</value>
    ''' <returns>Returns Source Field Name</returns>
    ''' <remarks></remarks>
    Public Property SourceFieldName() As String
        Get
            Return _curSourceFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSourceFieldName <> value Then
                _curSourceFieldName = value
                OnPropertyChanged("SourceFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Lookup Key
    ''' </summary>
    ''' <value>Lookup Key</value>
    ''' <returns>Returns Lookup Key</returns>
    ''' <remarks></remarks>
    Public Property LookupKey() As String
        Get
            Return _curLookupKey
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupKey <> value Then
                _curLookupKey = value
                OnPropertyChanged("LookupKey")
            End If
        End Set
    End Property
    Public Property LookupKey2() As String
        Get
            Return _curLookupKey2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupKey2 <> value Then
                _curLookupKey2 = value
                OnPropertyChanged("LookupKey2")
            End If
        End Set
    End Property
    Public Property LookupKey3() As String
        Get
            Return _curLookupKey3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupKey3 <> value Then
                _curLookupKey3 = value
                OnPropertyChanged("LookupKey3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Lookup Value
    ''' </summary>
    ''' <value>Lookup Value</value>
    ''' <returns>Returns Lookup Value</returns>
    ''' <remarks></remarks>
    Public Property LookupValue() As String
        Get
            Return _curLookupValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupValue <> value Then
                _curLookupValue = value
                OnPropertyChanged("LookupValue")
            End If
        End Set
    End Property
    Public Property LookupValue2() As String
        Get
            Return _curLookupValue2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupValue2 <> value Then
                _curLookupValue2 = value
                OnPropertyChanged("LookupValue2")
            End If
        End Set
    End Property
    Public Property LookupValue3() As String
        Get
            Return _curLookupValue3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curLookupValue3 <> value Then
                _curLookupValue3 = value
                OnPropertyChanged("LookupValue3")
            End If
        End Set
    End Property

    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property

#End Region
    ''' <summary>
    ''' Apply the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriBankFieldName = _curBankFieldName
        _oriTableName = _curTableName
        _oriSourceFieldName = _curSourceFieldName
        _oriLookupKey = _curLookupKey
        _oriLookupValue = _curLookupValue

        _oriTableName2 = _curTableName2
        _oriLookupKey2 = _curLookupKey2
        _oriLookupValue2 = _curLookupValue2

        _oriTableName3 = _curTableName3
        _oriLookupKey3 = _curLookupKey3
        _oriLookupValue3 = _curLookupValue3
    End Sub
    ''' <summary>
    ''' Begin changes
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldBankFieldName = _curBankFieldName
        _oldTableName = _curTableName
        _oldSourceFieldName = _curSourceFieldName
        _oldLookupKey = _curLookupKey
        _oldLookupValue = _curLookupValue

        _oldTableName2 = _curTableName2
        _oldLookupKey2 = _curLookupKey2
        _oldLookupValue2 = _curLookupValue2

        _oldTableName3 = _curTableName3
        _oldLookupKey3 = _curLookupKey3
        _oldLookupValue3 = _curLookupValue3
    End Sub
    ''' <summary>
    ''' Cancels the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curBankFieldName = _oldBankFieldName
        _curTableName = _oldTableName
        _curSourceFieldName = _oldSourceFieldName
        _curLookupKey = _oldLookupKey
        _curLookupValue = _oldLookupValue

        _curTableName2 = _oldTableName2
        _curLookupKey2 = _oldLookupKey2
        _curLookupValue2 = _oldLookupValue2

        _curTableName3 = _oldTableName3
        _curLookupKey3 = _oldLookupKey3
        _curLookupValue3 = _oldLookupValue3
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankFieldName = ReadXMLElement(reader, "_curBankFieldName")
            _oldBankFieldName = ReadXMLElement(reader, "_oldBankFieldName")
            _oriBankFieldName = ReadXMLElement(reader, "_oriBankFieldName")
            _curTableName = ReadXMLElement(reader, "_curTableName")
            _oldTableName = ReadXMLElement(reader, "_oldTableName")
            _oriTableName = ReadXMLElement(reader, "_oriTableName")
            _curSourceFieldName = ReadXMLElement(reader, "_curSourceFieldName")
            _oldSourceFieldName = ReadXMLElement(reader, "_oldSourceFieldName")
            _oriSourceFieldName = ReadXMLElement(reader, "_oriSourceFieldName")
            _curLookupKey = ReadXMLElement(reader, "_curLookupKey")
            _oldLookupKey = ReadXMLElement(reader, "_oldLookupKey")
            _oriLookupKey = ReadXMLElement(reader, "_oriLookupKey")
            _curLookupValue = ReadXMLElement(reader, "_curLookupValue")
            _oldLookupValue = ReadXMLElement(reader, "_oldLookupValue")
            _oriLookupValue = ReadXMLElement(reader, "_oriLookupValue")

            _curTableName2 = ReadXMLElement(reader, "_curTableName2")
            _oldTableName2 = ReadXMLElement(reader, "_oldTableName2")
            _oriTableName2 = ReadXMLElement(reader, "_oriTableName2")
            _curLookupKey2 = ReadXMLElement(reader, "_curLookupKey2")
            _oldLookupKey2 = ReadXMLElement(reader, "_oldLookupKey2")
            _oriLookupKey2 = ReadXMLElement(reader, "_oriLookupKey2")
            _curLookupValue2 = ReadXMLElement(reader, "_curLookupValue2")
            _oldLookupValue2 = ReadXMLElement(reader, "_oldLookupValue2")
            _oriLookupValue2 = ReadXMLElement(reader, "_oriLookupValue2")

            _curTableName3 = ReadXMLElement(reader, "_curTableName3")
            _oldTableName3 = ReadXMLElement(reader, "_oldTableName3")
            _oriTableName3 = ReadXMLElement(reader, "_oriTableName3")
            _curLookupKey3 = ReadXMLElement(reader, "_curLookupKey3")
            _oldLookupKey3 = ReadXMLElement(reader, "_oldLookupKey3")
            _oriLookupKey3 = ReadXMLElement(reader, "_oriLookupKey3")
            _curLookupValue3 = ReadXMLElement(reader, "_curLookupValue3")
            _oldLookupValue3 = ReadXMLElement(reader, "_oldLookupValue3")
            _oriLookupValue3 = ReadXMLElement(reader, "_oriLookupValue3")

            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankFieldName", _curBankFieldName)
        WriteXmlElement(writer, "_oldBankFieldName", _oldBankFieldName)
        WriteXmlElement(writer, "_oriBankFieldName", _oriBankFieldName)
        WriteXmlElement(writer, "_curTableName", _curTableName)
        WriteXmlElement(writer, "_oldTableName", _oldTableName)
        WriteXmlElement(writer, "_oriTableName", _oriTableName)
        WriteXmlElement(writer, "_curSourceFieldName", _curSourceFieldName)
        WriteXmlElement(writer, "_oldSourceFieldName", _oldSourceFieldName)
        WriteXmlElement(writer, "_oriSourceFieldName", _oriSourceFieldName)
        WriteXmlElement(writer, "_curLookupKey", _curLookupKey)
        WriteXmlElement(writer, "_oldLookupKey", _oldLookupKey)
        WriteXmlElement(writer, "_oriLookupKey", _oriLookupKey)
        WriteXmlElement(writer, "_curLookupValue", _curLookupValue)
        WriteXmlElement(writer, "_oldLookupValue", _oldLookupValue)
        WriteXmlElement(writer, "_oriLookupValue", _oriLookupValue)

        WriteXmlElement(writer, "_curTableName2", _curTableName2)
        WriteXmlElement(writer, "_oldTableName2", _oldTableName2)
        WriteXmlElement(writer, "_oriTableName2", _oriTableName2)
        WriteXmlElement(writer, "_curLookupKey2", _curLookupKey2)
        WriteXmlElement(writer, "_oldLookupKey2", _oldLookupKey2)
        WriteXmlElement(writer, "_oriLookupKey2", _oriLookupKey2)
        WriteXmlElement(writer, "_curLookupValue2", _curLookupValue2)
        WriteXmlElement(writer, "_oldLookupValue2", _oldLookupValue2)
        WriteXmlElement(writer, "_oriLookupValue2", _oriLookupValue2)

        WriteXmlElement(writer, "_curTableName3", _curTableName3)
        WriteXmlElement(writer, "_oldTableName3", _oldTableName3)
        WriteXmlElement(writer, "_oriTableName3", _oriTableName3)
        WriteXmlElement(writer, "_curLookupKey3", _curLookupKey3)
        WriteXmlElement(writer, "_oldLookupKey3", _oldLookupKey3)
        WriteXmlElement(writer, "_oriLookupKey3", _oriLookupKey3)
        WriteXmlElement(writer, "_curLookupValue3", _curLookupValue3)
        WriteXmlElement(writer, "_oldLookupValue3", _oldLookupValue3)
        WriteXmlElement(writer, "_oriLookupValue3", _oriLookupValue3)

    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">name of the file</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(LookupSetting))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the objecdt from the given file
    ''' </summary>
    ''' <param name="filename">filename</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As LookupSetting
        Dim serializer As New XmlSerializer(GetType(LookupSetting))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), LookupSetting)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As LookupSetting = DirectCast(obj, LookupSetting)
        _curBankFieldName = objE._curBankFieldName
        _oldBankFieldName = objE._oldBankFieldName
        _oriBankFieldName = objE._oriBankFieldName
        _curTableName = objE._curTableName
        _oldTableName = objE._oldTableName
        _oriTableName = objE._oriTableName
        _curSourceFieldName = objE._curSourceFieldName
        _oldSourceFieldName = objE._oldSourceFieldName
        _oriSourceFieldName = objE._oriSourceFieldName
        _curLookupKey = objE._curLookupKey
        _oldLookupKey = objE._oldLookupKey
        _oriLookupKey = objE._oriLookupKey
        _curLookupValue = objE._curLookupValue
        _oldLookupValue = objE._oldLookupValue
        _oriLookupValue = objE._oriLookupValue

        _curTableName2 = objE._curTableName2
        _oldTableName2 = objE._oldTableName2
        _oriTableName2 = objE._oriTableName2
        _curLookupKey2 = objE._curLookupKey2
        _oldLookupKey2 = objE._oldLookupKey2
        _oriLookupKey2 = objE._oriLookupKey2
        _curLookupValue2 = objE._curLookupValue2
        _oldLookupValue2 = objE._oldLookupValue2
        _oriLookupValue2 = objE._oriLookupValue2

        _curTableName3 = objE._curTableName3
        _oldTableName3 = objE._oldTableName3
        _oriTableName3 = objE._oriTableName3
        _curLookupKey3 = objE._curLookupKey3
        _oldLookupKey3 = objE._oldLookupKey3
        _oriLookupKey3 = objE._oriLookupKey3
        _curLookupValue3 = objE._curLookupValue3
        _oldLookupValue3 = objE._oldLookupValue3
        _oriLookupValue3 = objE._oriLookupValue3
    End Sub
#End Region
End Class

''' <summary>
''' Collection of Lookup Settings
''' </summary>
''' <remarks></remarks>

Partial Public Class LookupSettingCollection
    Inherits BusinessBaseCollection(Of LookupSetting)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of LookupSetting)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of LookupSetting)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of LookupSetting)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As LookupSetting
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("LookupSettingCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "LookupSetting"
            child = New LookupSetting
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As LookupSetting
        writer.WriteStartElement("LookupSettingCollection")
        For Each child In Me
            writer.WriteStartElement("LookupSetting")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
