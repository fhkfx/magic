Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Snapshot information about Common Transaction Template for Swift
''' </summary>
''' <remarks></remarks>
Public Class SwiftTemplateList
    Inherits BTMU.MAGIC.Common.CommonListItem


#Region " Private Fields "
    Private _rowno As Int32
    Private _swifttemplate As String
    Private _outputtemplate As String
    Private _sourcefile As String
    Private _isenabled As Boolean
    Private _isdraft As Boolean
#End Region

#Region " Public Property "
    ''' <summary>
    ''' This property is to Set/Get Record Number
    ''' </summary>
    ''' <value>Record Number</value>
    ''' <returns>Returns Record Number</returns>
    ''' <remarks></remarks>
    Public Property RowNo() As Int32
        Get
            Return _rowno
        End Get
        Set(ByVal value As Int32)
            _rowno = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Set/Get Name of Swift Template
    ''' </summary>
    ''' <value>Swift Template</value>
    ''' <returns>Returns Name of Swift Template</returns>
    ''' <remarks></remarks>
    Public Property SwiftTemplate() As String
        Get
            Return _swifttemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _swifttemplate = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Output Template
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template name</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplate() As String
        Get
            Return _outputtemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _outputtemplate = value

        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Source File
    ''' </summary>
    ''' <value>Source File</value>
    ''' <returns>Returns  Source File</returns>
    ''' <remarks></remarks>
    Public Property SourceFile() As String
        Get
            Return _sourcefile
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _sourcefile = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is enabled
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is enabled</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _isenabled
        End Get
        Set(ByVal value As Boolean)
            _isenabled = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is a draft
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is a draft</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _isdraft
        End Get
        Set(ByVal value As Boolean)
            _isdraft = value
        End Set
    End Property
#End Region

End Class
''' <summary>
''' Collection of Snapshot of Common Transaction Template for Swift
''' </summary>
''' <remarks></remarks>
Partial Public Class SwiftTemplateListCollection
    Inherits System.ComponentModel.BindingList(Of SwiftTemplateList)

    Private _bndListView As BTMU.MAGIC.Common.BindingListViewCommon(Of SwiftTemplateList)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListViewCommon(Of SwiftTemplateList)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListViewCommon(Of SwiftTemplateList)
        Get
            Return _bndListView
        End Get
    End Property
End Class

