Imports System.Data
Imports System.Data.OleDb
Imports BTMU.MAGIC.Common

Public Module ExcelHelperModule
    ''' <summary>
    ''' <para>This function is to get the Column Index for the Excel Column letter</para>
    ''' </summary>
    ''' <param name="columnLetter">Excel Column Letter(Eg:-A,B,C,AA etc)</param>
    ''' <returns>ColumnIndex as integer</returns>
    ''' <remarks></remarks>
    Public Function GetColumnIndex(ByVal columnLetter As String) As Integer

        Dim leftLetterAscii As String
        Dim leftLetterVal As Integer
        Dim rightLetterAscii As String
        Dim rightLetterVal As Integer
        Try
            Select Case Len(columnLetter)
                Case 1
                    Return Asc(UCase(columnLetter)) - 64
                Case 2 'AA,AB,AC, etc
                    leftLetterAscii = Asc(columnLetter.Trim.ToUpper.Substring(0, 1))
                    leftLetterVal = leftLetterAscii - 64
                    rightLetterAscii = Asc(columnLetter.Trim.ToUpper.Substring(columnLetter.Trim.Length - 1, 1))
                    rightLetterVal = rightLetterAscii - 64
                    Return (leftLetterVal * 26) + rightLetterVal
                Case Else
                    Return -1
            End Select
        Catch ex As Exception
            BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error getting Column Index : ", ex.Message)
        End Try

    End Function
    ''' <summary>
    ''' This function is to get Name of Column for the given Column Index
    ''' </summary>
    ''' <param name="iColumnIndex">Oridinal Number of Column</param>
    ''' <returns>Returns Name of Column at the given Column Index</returns>
    ''' <remarks></remarks>
    Public Function GetColumnString(ByVal iColumnIndex As Integer) As String
        Dim _columnString As String = String.Empty
        Dim leftString As String
        Dim leftInteger As Integer
        Dim rightString As String
        Dim rightInteger As Integer

        Select Case iColumnIndex
            Case Is <= 26
                _columnString = Chr(iColumnIndex + 64)
            Case Is > 26
                leftInteger = Int(iColumnIndex / 26)
                If iColumnIndex Mod 26 = 0 Then
                    leftInteger = leftInteger - 1
                End If
                leftString = Chr(leftInteger + 64)

                rightInteger = iColumnIndex Mod 26
                If rightInteger = 0 Then
                    rightString = Chr(rightInteger + 64 + 26)
                Else
                    rightString = Chr(rightInteger + 64)
                End If

                _columnString = leftString & rightString
            Case Else
                _columnString = ""
        End Select
        Return _columnString
    End Function

    ''' <summary>
    ''' This function is to get all the available work sheet names in the given excel file
    ''' </summary>
    ''' <param name="excelFile">Name of excel file</param>
    ''' <returns>Returns a Data Table of Work sheet names in the given excel file</returns>
    ''' <remarks></remarks>
    Public Function GetWorksheetNames(ByVal excelFile As String) As DataTable

        Dim excelConnection As OleDbConnection

        Dim tempWorksheetDataTable As DataTable
        Dim tempWorksheetDataRow As DataRow
        Dim worksheetDataTable As DataTable
        Dim worksheetDataRow As DataRow

        Dim restrictions As String() = {Nothing, Nothing, Nothing, "TABLE"}

        worksheetDataTable = New DataTable()
        worksheetDataTable.Columns.Add("WorksheetName")

        If Not System.IO.File.Exists(excelFile) Then
            Throw New MagicException(String.Format("Excel File '{0}' doesnot exists. Please check the file path.", excelFile))
        End If

        If ExcelReaderInterop.IsFileOpen(excelFile) Then
            Return worksheetDataTable
        End If

        Try
            excelConnection = New OleDbConnection(IIf(excelFile.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase), _
                String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended properties=""Excel 12.0 Xml;HDR=No""", excelFile), _
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)))
            excelConnection.Open()

        Catch ex As OleDbException
            Throw New MagicException("Your system does not support Excel 2007!")
        Catch ex As Exception
            Throw New MagicException(ex.Message)
        End Try

        Try
            tempWorksheetDataTable = excelConnection.GetSchema("Tables", restrictions)
            worksheetDataTable.Rows.Clear()
            For Each tempWorksheetDataRow In tempWorksheetDataTable.Rows

                If (Not tempWorksheetDataRow("TABLE_NAME").ToString().Replace("'", "").EndsWith("$")) Then
                Else
                    worksheetDataRow = worksheetDataTable.NewRow()

                    If tempWorksheetDataRow("TABLE_NAME").ToString.StartsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(0, 1)
                    End If

                    If tempWorksheetDataRow("TABLE_NAME").ToString.EndsWith("'") Then
                        tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString.Remove(tempWorksheetDataRow("TABLE_NAME").ToString.Length - 1, 1)
                    End If

                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Replace("''", "'")
                    tempWorksheetDataRow("TABLE_NAME") = tempWorksheetDataRow("TABLE_NAME").ToString().Remove(tempWorksheetDataRow("TABLE_NAME").ToString().Length - 1, 1)
                    worksheetDataRow("WorksheetName") = tempWorksheetDataRow("TABLE_NAME")
                    worksheetDataTable.Rows.Add(worksheetDataRow)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error on getting Worksheet Names(GetWorksheetNames) : {0}", ex.Message.ToString))
        Finally
            If excelConnection.State = ConnectionState.Open Then
                excelConnection.Close()
            End If
            excelConnection = Nothing
        End Try
        Return worksheetDataTable
    End Function

End Module
