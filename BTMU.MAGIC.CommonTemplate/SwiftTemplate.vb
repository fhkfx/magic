Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common
Imports System.Text.RegularExpressions
Imports System.Xml.XPath
Imports System.Data

''' <summary>
''' This class encapsulates a Common Transaction Template for SWIFT file
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class SwiftTemplate
    Inherits BTMU.MAGIC.Common.BusinessBase
    Implements ICommonTemplate

    <NonSerialized()> Public Shared DataContainer As SwiftTemplate

    <NonSerialized()> Private _SourceData As DataTable
    <NonSerialized()> Private _PreviewSourceData As DataTable
    <NonSerialized()> Private _FilteredSourceData As DataView

    Public Sub New()
        Initialize()
        DataContainer = Me
    End Sub

#Region " Private Fields "

    Private _curIsDraft As Boolean
    Private _oldIsDraft As Boolean
    Private _oriIsDraft As Boolean

    Private _curOutputTemplateName As String
    Private _oldOutputTemplateName As String
    Private _oriOutputTemplateName As String
    Private _curSwiftTemplateName As String
    Private _oldSwiftTemplateName As String
    Private _oriSwiftTemplateName As String
    Private _curIsEnabled As Boolean
    Private _oldIsEnabled As Boolean
    Private _oriIsEnabled As Boolean
    Private _curSourceFileName As String
    Private _oldSourceFileName As String
    Private _oriSourceFileName As String

    Private _curSampleRow As String
    Private _oldSampleRow As String
    Private _oriSampleRow As String
    Private _curDateSeparator As String
    Private _oldDateSeparator As String
    Private _oriDateSeparator As String
    Private _curDateType As String
    Private _oldDateType As String
    Private _oriDateType As String
    Private _curIsZeroInDate As Boolean
    Private _oldIsZeroInDate As Boolean
    Private _oriIsZeroInDate As Boolean
    Private _curDecimalSeparator As String
    Private _oldDecimalSeparator As String
    Private _oriDecimalSeparator As String

    Private _curIsAmountInDollars As Boolean
    Private _oldIsAmountInDollars As Boolean
    Private _oriIsAmountInDollars As Boolean
    Private _curIsAmountInCents As Boolean
    Private _oldIsAmountInCents As Boolean
    Private _oriIsAmountInCents As Boolean

    Private _curDuplicateTxnRef1 As String
    Private _oldDuplicateTxnRef1 As String
    Private _oriDuplicateTxnRef1 As String
    Private _curDuplicateTxnRef2 As String
    Private _oldDuplicateTxnRef2 As String
    Private _oriDuplicateTxnRef2 As String
    Private _curDuplicateTxnRef3 As String
    Private _oldDuplicateTxnRef3 As String
    Private _oriDuplicateTxnRef3 As String
    Private _curIsAdviceRecordForEveryRow As Boolean
    Private _oldIsAdviceRecordForEveryRow As Boolean
    Private _oriIsAdviceRecordForEveryRow As Boolean
    Private _curIsAdviceRecordAfterCharacter As Boolean
    Private _oldIsAdviceRecordAfterCharacter As Boolean
    Private _oriIsAdviceRecordAfterCharacter As Boolean
    Private _curIsAdviceRecordDelimited As Boolean
    Private _oldIsAdviceRecordDelimited As Boolean
    Private _oriIsAdviceRecordDelimited As Boolean
    Private _curAdviceRecordCharPos As String
    Private _oldAdviceRecordCharPos As String
    Private _oriAdviceRecordCharPos As String
    Private _curAdviceRecordDelimiter As String
    Private _oldAdviceRecordDelimiter As String
    Private _oriAdviceRecordDelimiter As String
    Private _curFilters As New RowFilterCollection
    Private _curIsFilterConditionAND As Boolean
    Private _oldIsFilterConditionAND As Boolean
    Private _oriIsFilterConditionAND As Boolean
    Private _curIsFilterConditionOR As Boolean
    Private _oldIsFilterConditionOR As Boolean
    Private _oriIsFilterConditionOR As Boolean
    Private _curMapSourceFields As New MapSourceFieldCollection
    Private _curMapBankFields As New MapBankFieldCollection
    Private _curTranslatorSettings As New TranslatorSettingCollection
    Private _curEditableSettings As New EditableSettingCollection
    Private _curLookupSettings As New LookupSettingCollection
    Private _curCalculatedFields As New CalculatedFieldCollection
    Private _curStringManipulations As New StringManipulationCollection

    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region

#Region " Public Property "
    ''' <summary>
    ''' Determines whether the Template is a Draft
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _curIsDraft
        End Get
        Set(ByVal value As Boolean)
            If _curIsDraft <> value Then
                _curIsDraft = value
                OnPropertyChanged("IsDraft")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get the Name of Output Template(Master Template)
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplateName() As String Implements ICommonTemplate.OutputTemplateName
        Get
            Return _curOutputTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curOutputTemplateName <> value Then
                _curOutputTemplateName = value
                OnPropertyChanged("OutputTemplateName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of SWIFT Template
    ''' </summary>
    ''' <value>Name of SWIFT Template</value>
    ''' <returns>Returns Name of SWIFT Template</returns>
    ''' <remarks></remarks>
    Public Property SwiftTemplateName() As String
        Get
            Return _curSwiftTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSwiftTemplateName <> value Then
                _curSwiftTemplateName = value
                OnPropertyChanged("SwiftTemplateName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This is not used
    ''' </summary>
    ''' <value>This is ignored</value>
    ''' <returns>Empty String</returns>
    ''' <remarks></remarks>
    Public Property DefinitionFile() As String Implements ICommonTemplate.DefinitionFile
        Get
            Return ""
        End Get
        Set(ByVal value As String)
            ' ignored
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Template is Enabled
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a Boolean Value</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _curIsEnabled
        End Get
        Set(ByVal value As Boolean)
            If _curIsEnabled <> value Then
                _curIsEnabled = value
                OnPropertyChanged("IsEnabled")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get the Name of SourceFile
    ''' </summary>
    ''' <value>Name of SourceFile</value>
    ''' <returns>Returns Name of SourceFile</returns>
    ''' <remarks></remarks>
    Public Property SourceFileName() As String
        Get
            Return _curSourceFileName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSourceFileName <> value Then
                _curSourceFileName = value
                OnPropertyChanged("SourceFileName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get a List of Editable Bank Fields
    ''' </summary>
    ''' <value>Editable Bank Fields</value>
    ''' <returns>Returns a List of Editable Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditableBankFields() As String()

        Get
            Return Nothing

        End Get

    End Property
    ''' <summary>
    ''' This Property is to Get/Set the Sample Transaction Record
    ''' </summary>
    ''' <value>Sample Transaction Record</value>
    ''' <returns>Returns Sample Transaction Record</returns>
    ''' <remarks></remarks>
    Public Property SampleRow() As String
        Get
            Return _curSampleRow
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curSampleRow <> value Then
                _curSampleRow = value
                OnPropertyChanged("SampleRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Separator
    ''' </summary>
    ''' <value>Date Separator</value>
    ''' <returns>Returns Date Separator</returns>
    ''' <remarks></remarks>
    Public Property DateSeparator() As String Implements ICommonTemplate.DateSeparator
        Get
            'Return _curDateSeparator
            Return "No Space"
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateSeparator <> value Then
                _curDateSeparator = value
                OnPropertyChanged("DateSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Type
    ''' </summary>
    ''' <value>Date Type</value>
    ''' <returns>Returns Date Type</returns>
    ''' <remarks></remarks>
    Public Property DateType() As String Implements ICommonTemplate.DateType
        Get
            Return _curDateType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateType <> value Then
                _curDateType = value
                OnPropertyChanged("DateType")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether zeros are part of the Date
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether zeros are part of Date</returns>
    ''' <remarks></remarks>
    Public Property IsZeroInDate() As Boolean Implements ICommonTemplate.IsZerosInDate
        Get
            Return _curIsZeroInDate
        End Get
        Set(ByVal value As Boolean)
            If _curIsZeroInDate <> value Then
                _curIsZeroInDate = value
                OnPropertyChanged("IsZeroInDate")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Decimal Separator
    ''' </summary>
    ''' <value>Decimal Separator</value>
    ''' <returns>Returns Decimal Separator</returns>
    ''' <remarks></remarks>
    Public Property DecimalSeparator() As String Implements ICommonTemplate.DecimalSeparator
        Get
            Return _curDecimalSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDecimalSeparator <> value Then
                _curDecimalSeparator = value
                OnPropertyChanged("DecimalSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Thousand Separator
    ''' </summary>
    ''' <value>Thousand Separator</value>
    ''' <returns>Returns Thousand Separator</returns>
    ''' <remarks></remarks>
    Public Property ThousandSeparator() As String Implements ICommonTemplate.ThousandSeparator
        Get
            Return ""
        End Get
        Set(ByVal value As String)
            'ignored
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in dollars
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in dollars</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInDollars() As Boolean Implements ICommonTemplate.IsAmountInDollars
        Get
            Return _curIsAmountInDollars
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInDollars <> value Then
                _curIsAmountInDollars = value
                OnPropertyChanged("IsAmountInDollars")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the amount is in Cents
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean Value indicating whether the amount is in Cents</returns>
    ''' <remarks></remarks>
    Public Property IsAmountInCents() As Boolean Implements ICommonTemplate.IsAmountInCents
        Get
            Return _curIsAmountInCents
        End Get
        Set(ByVal value As Boolean)
            If _curIsAmountInCents <> value Then
                _curIsAmountInCents = value
                OnPropertyChanged("IsAmountInCents")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 1</value>
    ''' <returns>Returns the First Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef1() As String Implements ICommonTemplate.TransactionReferenceField1
        Get
            Return _curDuplicateTxnRef1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef1 <> value Then
                _curDuplicateTxnRef1 = value
                OnPropertyChanged("DuplicateTxnRef1")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 2</value>
    ''' <returns>Returns the Second Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef2() As String Implements ICommonTemplate.TransactionReferenceField2
        Get
            Return _curDuplicateTxnRef2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef2 <> value Then
                _curDuplicateTxnRef2 = value
                OnPropertyChanged("DuplicateTxnRef2")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Reference Field to identify Duplicate Transaction Records
    ''' </summary>
    ''' <value>Reference Field 3</value>
    ''' <returns>Returns the Third Reference Field</returns>
    ''' <remarks></remarks>
    Public Property DuplicateTxnRef3() As String Implements ICommonTemplate.TransactionReferenceField3
        Get
            Return _curDuplicateTxnRef3
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDuplicateTxnRef3 <> value Then
                _curDuplicateTxnRef3 = value
                OnPropertyChanged("DuplicateTxnRef3")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is for every row
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record setting is for every row</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordForEveryRow() As Boolean Implements ICommonTemplate.IsAdviceRecordForEveryRow
        Get
            Return _curIsAdviceRecordForEveryRow
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordForEveryRow <> value Then
                _curIsAdviceRecordForEveryRow = value
                OnPropertyChanged("IsAdviceRecordForEveryRow")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the Advice record is after every character
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is after every character</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordAfterCharacter() As Boolean Implements ICommonTemplate.IsAdviceRecordAfterChar
        Get
            Return _curIsAdviceRecordAfterCharacter
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordAfterCharacter <> value Then
                _curIsAdviceRecordAfterCharacter = value
                OnPropertyChanged("IsAdviceRecordAfterCharacter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether Advice Record is delimited
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the advice record is delimited</returns>
    ''' <remarks></remarks>
    Public Property IsAdviceRecordDelimited() As Boolean Implements ICommonTemplate.IsAdviceRecordDelimited
        Get
            Return _curIsAdviceRecordDelimited
        End Get
        Set(ByVal value As Boolean)
            If _curIsAdviceRecordDelimited <> value Then
                _curIsAdviceRecordDelimited = value
                OnPropertyChanged("IsAdviceRecordDelimited")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set the ordinal position of Advice Record Character
    ''' </summary>
    ''' <value>Ordinal position of Advice Record Character</value>
    ''' <returns>Returns the Ordinal position of Advice Record Character</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecordCharPos() As String Implements ICommonTemplate.AdviceRecordChar
        Get
            Return _curAdviceRecordCharPos
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecordCharPos <> value Then
                _curAdviceRecordCharPos = value
                OnPropertyChanged("AdviceRecordCharPos")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Advice Record Delimiter
    ''' </summary>
    ''' <value>Advice Record Delimiter</value>
    ''' <returns>Returns the Advice Record Delimiter</returns>
    ''' <remarks></remarks>
    Public Property AdviceRecordDelimiter() As String Implements ICommonTemplate.AdviceRecordDelimiter
        Get
            Return _curAdviceRecordDelimiter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curAdviceRecordDelimiter <> value Then
                _curAdviceRecordDelimiter = value
                OnPropertyChanged("AdviceRecordDelimiter")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to get a List of Filters
    ''' </summary>
    ''' <value>Row Filter</value>
    ''' <returns>Returns list of Row Filters</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Filters() As RowFilterCollection Implements ICommonTemplate.Filters
        Get
            Return _curFilters
        End Get
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is AND for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether AND condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionAND() As Boolean Implements ICommonTemplate.IsFilterConditionAND
        Get
            Return _curIsFilterConditionAND
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionAND <> value Then
                _curIsFilterConditionAND = value
                OnPropertyChanged("IsFilterConditionAND")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether the intended boolean condition is OR for evaluating Row Filter Conditions on Source File
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether OR condition is employed on Row Filter conditions</returns>
    ''' <remarks></remarks>
    Public Property IsFilterConditionOR() As Boolean
        Get
            Return _curIsFilterConditionOR
        End Get
        Set(ByVal value As Boolean)
            If _curIsFilterConditionOR <> value Then
                _curIsFilterConditionOR = value
                OnPropertyChanged("IsFilterConditionOR")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get a List of Source Fields
    ''' </summary>
    ''' <value>Source Field</value>
    ''' <returns>Returns a list of Source Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapSourceFields() As MapSourceFieldCollection Implements ICommonTemplate.MapSourceFields
        Get
            Return _curMapSourceFields
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get a List of Bank Fields
    ''' </summary>
    ''' <value>Bank Field</value>
    ''' <returns>Returns a List of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MapBankFields() As MapBankFieldCollection Implements ICommonTemplate.MapBankFields
        Get
            Return _curMapBankFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Translator Settings
    ''' </summary>
    ''' <value>Translator Setting</value>
    ''' <returns>Returns a List of Translator Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TranslatorSettings() As TranslatorSettingCollection Implements ICommonTemplate.TranslatorSettings
        Get
            Return _curTranslatorSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Editable Settings
    ''' </summary>
    ''' <value>Editable Setting</value>
    ''' <returns>Returns a List of Editable Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property EditableSettings() As EditableSettingCollection Implements ICommonTemplate.EditableSettings
        Get
            Return _curEditableSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Lookup Settings
    ''' </summary>
    ''' <value>Lookup Setting</value>
    ''' <returns>Returns a List of Lookup Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property LookupSettings() As LookupSettingCollection Implements ICommonTemplate.LookupSettings
        Get
            Return _curLookupSettings
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of Calculated Fields
    ''' </summary>
    ''' <value>Calculated Field</value>
    ''' <returns>Returns a List of Calculated Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CalculatedFields() As CalculatedFieldCollection Implements ICommonTemplate.CalculatedFields
        Get
            Return _curCalculatedFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get a List of String Manipulation Settings
    ''' </summary>
    ''' <value>String Manipulation Setting</value>
    ''' <returns>returns a List of String Manipulation Setting</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property StringManipulations() As StringManipulationCollection Implements ICommonTemplate.StringManipulations
        Get
            Return _curStringManipulations
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the User that created this Object
    ''' </summary>
    ''' <value>User ID</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Created Date and Time of this object
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Created Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Modified Date and Time of Calculated Field
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Modified Date and Time of Calculated Field  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the User ID of Application User that originally modified this object
    ''' </summary>
    ''' <value>User ID of Application User</value>
    ''' <returns>Returns the User ID of Application User</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    ''' <summary>
    ''' This Property is to Get the Original Modified Date and Time of this objecdt
    ''' </summary>
    ''' <value>DateTime</value>
    ''' <returns>Returns the Original Modified Date and Time  </returns>
    ''' <remarks></remarks>
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region

#Region "Begin/Apply/Cancel Edit"
    Public Sub SetDirty(ByVal value As Boolean)
        _isDirty = value
    End Sub
    Public Overrides Sub ApplyEdit()
        _oriOutputTemplateName = _curOutputTemplateName
        _oriSwiftTemplateName = _curSwiftTemplateName
        _oriIsEnabled = _curIsEnabled
        _oriIsDraft = _curIsDraft
        _oriSourceFileName = _curSourceFileName

        _oriSampleRow = _curSampleRow
        _oriDateSeparator = _curDateSeparator
        _oriDateType = _curDateType
        _oriIsZeroInDate = _curIsZeroInDate
        _oriDecimalSeparator = _curDecimalSeparator

        _oriIsAmountInDollars = _curIsAmountInDollars
        _oriIsAmountInCents = _curIsAmountInCents

        _oriDuplicateTxnRef1 = _curDuplicateTxnRef1
        _oriDuplicateTxnRef2 = _curDuplicateTxnRef2
        _oriDuplicateTxnRef3 = _curDuplicateTxnRef3
        _oriIsAdviceRecordForEveryRow = _curIsAdviceRecordForEveryRow
        _oriIsAdviceRecordAfterCharacter = _curIsAdviceRecordAfterCharacter
        _oriIsAdviceRecordDelimited = _curIsAdviceRecordDelimited
        _oriAdviceRecordCharPos = _curAdviceRecordCharPos
        _oriAdviceRecordDelimiter = _curAdviceRecordDelimiter
        _oriIsFilterConditionAND = _curIsFilterConditionAND
        _oriIsFilterConditionOR = _curIsFilterConditionOR

        _isDirty = False
    End Sub
    Public Overrides Sub BeginEdit()
        _oldOutputTemplateName = _curOutputTemplateName
        _oldSwiftTemplateName = _curSwiftTemplateName
        _oldIsEnabled = _curIsEnabled
        _oldIsDraft = _curIsDraft
        _oldSourceFileName = _curSourceFileName

        _oldSampleRow = _curSampleRow
        _oldDateSeparator = _curDateSeparator
        _oldDateType = _curDateType
        _oldIsZeroInDate = _curIsZeroInDate
        _oldDecimalSeparator = _curDecimalSeparator

        _oldIsAmountInDollars = _curIsAmountInDollars
        _oldIsAmountInCents = _curIsAmountInCents

        _oldDuplicateTxnRef1 = _curDuplicateTxnRef1
        _oldDuplicateTxnRef2 = _curDuplicateTxnRef2
        _oldDuplicateTxnRef3 = _curDuplicateTxnRef3
        _oldIsAdviceRecordForEveryRow = _curIsAdviceRecordForEveryRow
        _oldIsAdviceRecordAfterCharacter = _curIsAdviceRecordAfterCharacter
        _oldIsAdviceRecordDelimited = _curIsAdviceRecordDelimited
        _oldAdviceRecordCharPos = _curAdviceRecordCharPos
        _oldAdviceRecordDelimiter = _curAdviceRecordDelimiter
        _oldIsFilterConditionAND = _curIsFilterConditionAND
        _oldIsFilterConditionOR = _curIsFilterConditionOR
    End Sub
    Public Overrides Sub CancelEdit()
        _curOutputTemplateName = _oldOutputTemplateName
        _curSwiftTemplateName = _oldSwiftTemplateName
        _curIsEnabled = _oldIsEnabled
        _curIsDraft = _oldIsDraft
        _curSourceFileName = _oldSourceFileName

        _curSampleRow = _oldSampleRow
        _curDateSeparator = _oldDateSeparator
        _curDateType = _oldDateType
        _curIsZeroInDate = _oldIsZeroInDate
        _curDecimalSeparator = _oldDecimalSeparator

        _curIsAmountInDollars = _oldIsAmountInDollars
        _curIsAmountInCents = _oldIsAmountInCents

        _curDuplicateTxnRef1 = _oldDuplicateTxnRef1
        _curDuplicateTxnRef2 = _oldDuplicateTxnRef2
        _curDuplicateTxnRef3 = _oldDuplicateTxnRef3
        _curIsAdviceRecordForEveryRow = _oldIsAdviceRecordForEveryRow
        _curIsAdviceRecordAfterCharacter = _oldIsAdviceRecordAfterCharacter
        _curIsAdviceRecordDelimited = _oldIsAdviceRecordDelimited
        _curAdviceRecordCharPos = _oldAdviceRecordCharPos
        _curAdviceRecordDelimiter = _oldAdviceRecordDelimiter
        _curIsFilterConditionAND = _oldIsFilterConditionAND
        _curIsFilterConditionOR = _oldIsFilterConditionOR
    End Sub

#End Region

#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curOutputTemplateName = ReadXMLElement(reader, "_curOutputTemplateName")
            _oldOutputTemplateName = ReadXMLElement(reader, "_oldOutputTemplateName")
            _oriOutputTemplateName = ReadXMLElement(reader, "_oriOutputTemplateName")
            _curSwiftTemplateName = ReadXMLElement(reader, "_curSwiftTemplateName")
            _oldSwiftTemplateName = ReadXMLElement(reader, "_oldSwiftTemplateName")
            _oriSwiftTemplateName = ReadXMLElement(reader, "_oriSwiftTemplateName")
            _curIsEnabled = ReadXMLElement(reader, "_curIsEnabled")
            _oldIsEnabled = ReadXMLElement(reader, "_oldIsEnabled")
            _oriIsEnabled = ReadXMLElement(reader, "_oriIsEnabled")
            _curIsDraft = ReadXMLElement(reader, "_curIsDraft")
            _oldIsDraft = ReadXMLElement(reader, "_oldIsDraft")
            _oriIsDraft = ReadXMLElement(reader, "_oriIsDraft")
            _curSourceFileName = ReadXMLElement(reader, "_curSourceFileName")
            _oldSourceFileName = ReadXMLElement(reader, "_oldSourceFileName")
            _oriSourceFileName = ReadXMLElement(reader, "_oriSourceFileName")

            _curSampleRow = ReadXMLElement(reader, "_curSampleRow")
            _oldSampleRow = ReadXMLElement(reader, "_oldSampleRow")
            _oriSampleRow = ReadXMLElement(reader, "_oriSampleRow")
            _curDateSeparator = ReadXMLElement(reader, "_curDateSeparator")
            _oldDateSeparator = ReadXMLElement(reader, "_oldDateSeparator")
            _oriDateSeparator = ReadXMLElement(reader, "_oriDateSeparator")
            _curDateType = ReadXMLElement(reader, "_curDateType")
            _oldDateType = ReadXMLElement(reader, "_oldDateType")
            _oriDateType = ReadXMLElement(reader, "_oriDateType")
            _curIsZeroInDate = ReadXMLElement(reader, "_curIsZeroInDate")
            _oldIsZeroInDate = ReadXMLElement(reader, "_oldIsZeroInDate")
            _oriIsZeroInDate = ReadXMLElement(reader, "_oriIsZeroInDate")
            _curDecimalSeparator = ReadXMLElement(reader, "_curDecimalSeparator")
            _oldDecimalSeparator = ReadXMLElement(reader, "_oldDecimalSeparator")
            _oriDecimalSeparator = ReadXMLElement(reader, "_oriDecimalSeparator")

            _curIsAmountInDollars = ReadXMLElement(reader, "_curIsAmountInDollars")
            _oldIsAmountInDollars = ReadXMLElement(reader, "_oldIsAmountInDollars")
            _oriIsAmountInDollars = ReadXMLElement(reader, "_oriIsAmountInDollars")
            _curIsAmountInCents = ReadXMLElement(reader, "_curIsAmountInCents")
            _oldIsAmountInCents = ReadXMLElement(reader, "_oldIsAmountInCents")
            _oriIsAmountInCents = ReadXMLElement(reader, "_oriIsAmountInCents")

            _curDuplicateTxnRef1 = ReadXMLElement(reader, "_curDuplicateTxnRef1")
            _oldDuplicateTxnRef1 = ReadXMLElement(reader, "_oldDuplicateTxnRef1")
            _oriDuplicateTxnRef1 = ReadXMLElement(reader, "_oriDuplicateTxnRef1")
            _curDuplicateTxnRef2 = ReadXMLElement(reader, "_curDuplicateTxnRef2")
            _oldDuplicateTxnRef2 = ReadXMLElement(reader, "_oldDuplicateTxnRef2")
            _oriDuplicateTxnRef2 = ReadXMLElement(reader, "_oriDuplicateTxnRef2")
            _curDuplicateTxnRef3 = ReadXMLElement(reader, "_curDuplicateTxnRef3")
            _oldDuplicateTxnRef3 = ReadXMLElement(reader, "_oldDuplicateTxnRef3")
            _oriDuplicateTxnRef3 = ReadXMLElement(reader, "_oriDuplicateTxnRef3")
            _curIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_curIsAdviceRecordForEveryRow")
            _oldIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_oldIsAdviceRecordForEveryRow")
            _oriIsAdviceRecordForEveryRow = ReadXMLElement(reader, "_oriIsAdviceRecordForEveryRow")
            _curIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_curIsAdviceRecordAfterCharacter")
            _oldIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_oldIsAdviceRecordAfterCharacter")
            _oriIsAdviceRecordAfterCharacter = ReadXMLElement(reader, "_oriIsAdviceRecordAfterCharacter")
            _curIsAdviceRecordDelimited = ReadXMLElement(reader, "_curIsAdviceRecordDelimited")
            _oldIsAdviceRecordDelimited = ReadXMLElement(reader, "_oldIsAdviceRecordDelimited")
            _oriIsAdviceRecordDelimited = ReadXMLElement(reader, "_oriIsAdviceRecordDelimited")
            _curAdviceRecordCharPos = ReadXMLElement(reader, "_curAdviceRecordCharPos")
            _oldAdviceRecordCharPos = ReadXMLElement(reader, "_oldAdviceRecordCharPos")
            _oriAdviceRecordCharPos = ReadXMLElement(reader, "_oriAdviceRecordCharPos")
            _curAdviceRecordDelimiter = ReadXMLElement(reader, "_curAdviceRecordDelimiter")
            _oldAdviceRecordDelimiter = ReadXMLElement(reader, "_oldAdviceRecordDelimiter")
            _oriAdviceRecordDelimiter = ReadXMLElement(reader, "_oriAdviceRecordDelimiter")
            _curFilters.ReadXml(reader)
            _curIsFilterConditionAND = ReadXMLElement(reader, "_curIsFilterConditionAND")
            _oldIsFilterConditionAND = ReadXMLElement(reader, "_oldIsFilterConditionAND")
            _oriIsFilterConditionAND = ReadXMLElement(reader, "_oriIsFilterConditionAND")
            _curIsFilterConditionOR = ReadXMLElement(reader, "_curIsFilterConditionOR")
            _oldIsFilterConditionOR = ReadXMLElement(reader, "_oldIsFilterConditionOR")
            _oriIsFilterConditionOR = ReadXMLElement(reader, "_oriIsFilterConditionOR")
            _curMapSourceFields.ReadXml(reader)
            _curMapBankFields.ReadXml(reader)
            _curTranslatorSettings.ReadXml(reader)
            _curEditableSettings.ReadXml(reader)
            _curLookupSettings.ReadXml(reader)
            _curCalculatedFields.ReadXml(reader)
            _curStringManipulations.ReadXml(reader)

            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curOutputTemplateName", _curOutputTemplateName)
        WriteXmlElement(writer, "_oldOutputTemplateName", _oldOutputTemplateName)
        WriteXmlElement(writer, "_oriOutputTemplateName", _oriOutputTemplateName)
        WriteXmlElement(writer, "_curSwiftTemplateName", _curSwiftTemplateName)
        WriteXmlElement(writer, "_oldSwiftTemplateName", _oldSwiftTemplateName)
        WriteXmlElement(writer, "_oriSwiftTemplateName", _oriSwiftTemplateName)
        WriteXmlElement(writer, "_curIsEnabled", _curIsEnabled)
        WriteXmlElement(writer, "_oldIsEnabled", _oldIsEnabled)
        WriteXmlElement(writer, "_oriIsEnabled", _oriIsEnabled)
        WriteXmlElement(writer, "_curIsDraft", _curIsDraft)
        WriteXmlElement(writer, "_oldIsDraft", _oldIsDraft)
        WriteXmlElement(writer, "_oriIsDraft", _oriIsDraft)
        WriteXmlElement(writer, "_curSourceFileName", _curSourceFileName)
        WriteXmlElement(writer, "_oldSourceFileName", _oldSourceFileName)
        WriteXmlElement(writer, "_oriSourceFileName", _oriSourceFileName)

        WriteXmlElement(writer, "_curSampleRow", _curSampleRow)
        WriteXmlElement(writer, "_oldSampleRow", _oldSampleRow)
        WriteXmlElement(writer, "_oriSampleRow", _oriSampleRow)
        WriteXmlElement(writer, "_curDateSeparator", _curDateSeparator)
        WriteXmlElement(writer, "_oldDateSeparator", _oldDateSeparator)
        WriteXmlElement(writer, "_oriDateSeparator", _oriDateSeparator)
        WriteXmlElement(writer, "_curDateType", _curDateType)
        WriteXmlElement(writer, "_oldDateType", _oldDateType)
        WriteXmlElement(writer, "_oriDateType", _oriDateType)
        WriteXmlElement(writer, "_curIsZeroInDate", _curIsZeroInDate)
        WriteXmlElement(writer, "_oldIsZeroInDate", _oldIsZeroInDate)
        WriteXmlElement(writer, "_oriIsZeroInDate", _oriIsZeroInDate)
        WriteXmlElement(writer, "_curDecimalSeparator", _curDecimalSeparator)
        WriteXmlElement(writer, "_oldDecimalSeparator", _oldDecimalSeparator)
        WriteXmlElement(writer, "_oriDecimalSeparator", _oriDecimalSeparator)

        WriteXmlElement(writer, "_curIsAmountInDollars", _curIsAmountInDollars)
        WriteXmlElement(writer, "_oldIsAmountInDollars", _oldIsAmountInDollars)
        WriteXmlElement(writer, "_oriIsAmountInDollars", _oriIsAmountInDollars)
        WriteXmlElement(writer, "_curIsAmountInCents", _curIsAmountInCents)
        WriteXmlElement(writer, "_oldIsAmountInCents", _oldIsAmountInCents)
        WriteXmlElement(writer, "_oriIsAmountInCents", _oriIsAmountInCents)

        WriteXmlElement(writer, "_curDuplicateTxnRef1", _curDuplicateTxnRef1)
        WriteXmlElement(writer, "_oldDuplicateTxnRef1", _oldDuplicateTxnRef1)
        WriteXmlElement(writer, "_oriDuplicateTxnRef1", _oriDuplicateTxnRef1)
        WriteXmlElement(writer, "_curDuplicateTxnRef2", _curDuplicateTxnRef2)
        WriteXmlElement(writer, "_oldDuplicateTxnRef2", _oldDuplicateTxnRef2)
        WriteXmlElement(writer, "_oriDuplicateTxnRef2", _oriDuplicateTxnRef2)
        WriteXmlElement(writer, "_curDuplicateTxnRef3", _curDuplicateTxnRef3)
        WriteXmlElement(writer, "_oldDuplicateTxnRef3", _oldDuplicateTxnRef3)
        WriteXmlElement(writer, "_oriDuplicateTxnRef3", _oriDuplicateTxnRef3)
        WriteXmlElement(writer, "_curIsAdviceRecordForEveryRow", _curIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_oldIsAdviceRecordForEveryRow", _oldIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_oriIsAdviceRecordForEveryRow", _oriIsAdviceRecordForEveryRow)
        WriteXmlElement(writer, "_curIsAdviceRecordAfterCharacter", _curIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_oldIsAdviceRecordAfterCharacter", _oldIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_oriIsAdviceRecordAfterCharacter", _oriIsAdviceRecordAfterCharacter)
        WriteXmlElement(writer, "_curIsAdviceRecordDelimited", _curIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_oldIsAdviceRecordDelimited", _oldIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_oriIsAdviceRecordDelimited", _oriIsAdviceRecordDelimited)
        WriteXmlElement(writer, "_curAdviceRecordCharPos", _curAdviceRecordCharPos)
        WriteXmlElement(writer, "_oldAdviceRecordCharPos", _oldAdviceRecordCharPos)
        WriteXmlElement(writer, "_oriAdviceRecordCharPos", _oriAdviceRecordCharPos)
        WriteXmlElement(writer, "_curAdviceRecordDelimiter", _curAdviceRecordDelimiter)
        WriteXmlElement(writer, "_oldAdviceRecordDelimiter", _oldAdviceRecordDelimiter)
        WriteXmlElement(writer, "_oriAdviceRecordDelimiter", _oriAdviceRecordDelimiter)
        _curFilters.WriteXml(writer)
        WriteXmlElement(writer, "_curIsFilterConditionAND", _curIsFilterConditionAND)
        WriteXmlElement(writer, "_oldIsFilterConditionAND", _oldIsFilterConditionAND)
        WriteXmlElement(writer, "_oriIsFilterConditionAND", _oriIsFilterConditionAND)
        WriteXmlElement(writer, "_curIsFilterConditionOR", _curIsFilterConditionOR)
        WriteXmlElement(writer, "_oldIsFilterConditionOR", _oldIsFilterConditionOR)
        WriteXmlElement(writer, "_oriIsFilterConditionOR", _oriIsFilterConditionOR)
        _curMapSourceFields.WriteXml(writer)
        _curMapBankFields.WriteXml(writer)
        _curTranslatorSettings.WriteXml(writer)
        _curEditableSettings.WriteXml(writer)
        _curLookupSettings.WriteXml(writer)
        _curCalculatedFields.WriteXml(writer)
        _curStringManipulations.WriteXml(writer)


    End Sub
#End Region

#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves this object to given file
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(SwiftTemplate))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads this object from given file
    ''' </summary>
    ''' <param name="filename">Name of file</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As SwiftTemplate
        Dim serializer As New XmlSerializer(GetType(SwiftTemplate))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), SwiftTemplate)
        stream.Close()
        Clone(objE)
        Validate()
        DataContainer = objE
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As SwiftTemplate = DirectCast(obj, SwiftTemplate)
        _curOutputTemplateName = objE._curOutputTemplateName
        _oldOutputTemplateName = objE._oldOutputTemplateName
        _oriOutputTemplateName = objE._oriOutputTemplateName
        _curSwiftTemplateName = objE._curSwiftTemplateName
        _oldSwiftTemplateName = objE._oldSwiftTemplateName
        _oriSwiftTemplateName = objE._oriSwiftTemplateName
        _curIsEnabled = objE._curIsEnabled
        _oldIsEnabled = objE._oldIsEnabled
        _oriIsEnabled = objE._oriIsEnabled
        _curIsDraft = objE._curIsDraft
        _oldIsDraft = objE._oldIsDraft
        _oriIsDraft = objE._oriIsDraft
        _curSourceFileName = objE._curSourceFileName
        _oldSourceFileName = objE._oldSourceFileName
        _oriSourceFileName = objE._oriSourceFileName

        _curSampleRow = objE._curSampleRow
        _oldSampleRow = objE._oldSampleRow
        _oriSampleRow = objE._oriSampleRow
        _curDateSeparator = objE._curDateSeparator
        _oldDateSeparator = objE._oldDateSeparator
        _oriDateSeparator = objE._oriDateSeparator
        _curDateType = objE._curDateType
        _oldDateType = objE._oldDateType
        _oriDateType = objE._oriDateType
        _curIsZeroInDate = objE._curIsZeroInDate
        _oldIsZeroInDate = objE._oldIsZeroInDate
        _oriIsZeroInDate = objE._oriIsZeroInDate
        _curDecimalSeparator = objE._curDecimalSeparator
        _oldDecimalSeparator = objE._oldDecimalSeparator
        _oriDecimalSeparator = objE._oriDecimalSeparator

        _curIsAmountInDollars = objE._curIsAmountInDollars
        _oldIsAmountInDollars = objE._oldIsAmountInDollars
        _oriIsAmountInDollars = objE._oriIsAmountInDollars
        _curIsAmountInCents = objE._curIsAmountInCents
        _oldIsAmountInCents = objE._oldIsAmountInCents
        _oriIsAmountInCents = objE._oriIsAmountInCents

        _curDuplicateTxnRef1 = objE._curDuplicateTxnRef1
        _oldDuplicateTxnRef1 = objE._oldDuplicateTxnRef1
        _oriDuplicateTxnRef1 = objE._oriDuplicateTxnRef1
        _curDuplicateTxnRef2 = objE._curDuplicateTxnRef2
        _oldDuplicateTxnRef2 = objE._oldDuplicateTxnRef2
        _oriDuplicateTxnRef2 = objE._oriDuplicateTxnRef2
        _curDuplicateTxnRef3 = objE._curDuplicateTxnRef3
        _oldDuplicateTxnRef3 = objE._oldDuplicateTxnRef3
        _oriDuplicateTxnRef3 = objE._oriDuplicateTxnRef3
        _curIsAdviceRecordForEveryRow = objE._curIsAdviceRecordForEveryRow
        _oldIsAdviceRecordForEveryRow = objE._oldIsAdviceRecordForEveryRow
        _oriIsAdviceRecordForEveryRow = objE._oriIsAdviceRecordForEveryRow
        _curIsAdviceRecordAfterCharacter = objE._curIsAdviceRecordAfterCharacter
        _oldIsAdviceRecordAfterCharacter = objE._oldIsAdviceRecordAfterCharacter
        _oriIsAdviceRecordAfterCharacter = objE._oriIsAdviceRecordAfterCharacter
        _curIsAdviceRecordDelimited = objE._curIsAdviceRecordDelimited
        _oldIsAdviceRecordDelimited = objE._oldIsAdviceRecordDelimited
        _oriIsAdviceRecordDelimited = objE._oriIsAdviceRecordDelimited
        _curAdviceRecordCharPos = objE._curAdviceRecordCharPos
        _oldAdviceRecordCharPos = objE._oldAdviceRecordCharPos
        _oriAdviceRecordCharPos = objE._oriAdviceRecordCharPos
        _curAdviceRecordDelimiter = objE._curAdviceRecordDelimiter
        _oldAdviceRecordDelimiter = objE._oldAdviceRecordDelimiter
        _oriAdviceRecordDelimiter = objE._oriAdviceRecordDelimiter
        _curFilters = objE._curFilters
        _curIsFilterConditionAND = objE._curIsFilterConditionAND
        _oldIsFilterConditionAND = objE._oldIsFilterConditionAND
        _oriIsFilterConditionAND = objE._oriIsFilterConditionAND
        _curIsFilterConditionOR = objE._curIsFilterConditionOR
        _oldIsFilterConditionOR = objE._oldIsFilterConditionOR
        _oriIsFilterConditionOR = objE._oriIsFilterConditionOR
        _curMapSourceFields = objE._curMapSourceFields
        _curMapBankFields = objE._curMapBankFields
        _curTranslatorSettings = objE._curTranslatorSettings
        _curEditableSettings = objE._curEditableSettings
        _curLookupSettings = objE._curLookupSettings
        _curCalculatedFields = objE._curCalculatedFields
        _curStringManipulations = objE._curStringManipulations


    End Sub

    ''' <summary>
    ''' Validates the Fields of this template
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Validate()

        ValidationEngine.Validate()

        For Each childFilters As RowFilter In _curFilters
            childFilters.Validate()
        Next

        For Each childMapSourceFields As MapSourceField In _curMapSourceFields
            childMapSourceFields.Validate()
        Next

        For Each childMapBankFields As MapBankField In _curMapBankFields
            childMapBankFields.Validate()
        Next

        For Each childTranslatorSettings As TranslatorSetting In _curTranslatorSettings
            childTranslatorSettings.Validate()
        Next

        For Each childEditableSettings As EditableSetting In _curEditableSettings
            childEditableSettings.Validate()
        Next

        For Each childLookupSettings As LookupSetting In _curLookupSettings
            childLookupSettings.Validate()
        Next

        For Each childCalculatedFields As CalculatedField In _curCalculatedFields
            childCalculatedFields.Validate()
        Next

        For Each childStringManipulations As StringManipulation In _curStringManipulations
            childStringManipulations.Validate()
        Next


    End Sub

    ''' <summary>
    ''' Determines whether the Template Data is valid
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the Template Data is valid</returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property IsValid() As Boolean
        Get
            If Not ValidationEngine.IsValid Then
                Return False
            End If

            Dim childFilters As RowFilter
            For Each childFilters In _curFilters
                If Not childFilters.IsValid Then
                    Return False
                End If
            Next

            For Each childMapSourceFields As MapSourceField In _curMapSourceFields
                If Not childMapSourceFields.IsValid Then
                    Return False
                End If
            Next

            For Each childMapBankFields As MapBankField In _curMapBankFields
                If Not childMapBankFields.IsValid Then
                    Return False
                End If
            Next

            For Each childTranslatorSettings As TranslatorSetting In _curTranslatorSettings
                If Not childTranslatorSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childEditableSettings As EditableSetting In _curEditableSettings
                If Not childEditableSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childLookupSettings As LookupSetting In _curLookupSettings
                If Not childLookupSettings.IsValid Then
                    Return False
                End If
            Next

            For Each childCalculatedFields As CalculatedField In _curCalculatedFields
                If Not childCalculatedFields.IsValid Then
                    Return False
                End If
            Next

            For Each childStringManipulations As StringManipulation In _curStringManipulations
                If Not childStringManipulations.IsValid Then
                    Return False
                End If
            Next


            Return True

        End Get

    End Property

    ''' <summary>
    ''' Saves the template to filename and deletes the filetobedeleted, if given
    ''' </summary>
    ''' <param name="filename">Name of file to save the template</param>
    ''' <param name="filetobedeleted">file to delete upon saving template</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile2(ByVal Filename As String, Optional ByVal filetobedeleted As String = "")

        If IsChild Then
            _isDirty = True
            Throw New Exception("Unable to save child object")
        End If


        Try
            If filetobedeleted.Trim().Length > 0 Then
                If File.Exists(filetobedeleted) Then File.Delete(filetobedeleted)
            End If

            Dim _objSwiftTemplateList As New SwiftTemplateList
            Dim fileTemplate As New BTMU.MAGIC.Common.MagicFileTemplate
            Dim serializerDetail As New XmlSerializer(GetType(SwiftTemplate))
            Dim serializerList As New XmlSerializer(GetType(SwiftTemplateList))
            Dim serializerFileTemplate As New XmlSerializer(GetType(BTMU.MAGIC.Common.MagicFileTemplate))
            Dim stream As New MemoryStream
            Dim streamFile As FileStream = File.Open(Filename, FileMode.Create)

            _objSwiftTemplateList.SwiftTemplate = SwiftTemplateName
            _objSwiftTemplateList.OutputTemplate = OutputTemplateName
            _objSwiftTemplateList.SourceFile = SourceFileName
            _objSwiftTemplateList.IsEnabled = IsEnabled
            _objSwiftTemplateList.IsDraft = IsDraft

            serializerDetail.Serialize(stream, Me)
            fileTemplate.DetailValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
            stream.Close()

            stream = New MemoryStream
            serializerList.Serialize(stream, _objSwiftTemplateList)
            fileTemplate.ListValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
            stream.Close()

            serializerFileTemplate.Serialize(streamFile, fileTemplate)
            streamFile.Close()
        Catch ex As System.Exception
            _isDirty = True
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Loads the template from the given file
    ''' </summary>
    ''' <param name="filename">template file name</param>
    ''' <returns>Returns the Template loaded</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadFromFile2(ByVal filename As String) As SwiftTemplate


        Dim serializer As New XmlSerializer(GetType(SwiftTemplate))
        Dim content As String
        Dim doc As XPathDocument = New XPathDocument(filename)
        Dim nav As XPathNavigator = doc.CreateNavigator()
        Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/DetailValue")
        Dim detailObj As New SwiftTemplate
        Iterator.MoveNext()
        content = Iterator.Current.Value

        Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
        stream.Position = 0

        Dim xmlSetting As New System.Xml.XmlReaderSettings()
        xmlSetting.IgnoreWhitespace = False
        Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(stream, xmlSetting)

        detailObj = CType(serializer.Deserialize(xmlReader), SwiftTemplate)
        stream.Close()
        xmlReader.Close()

        DataContainer = detailObj

        'Refresh the following bank field properties
        RefreshBankFields(detailObj)

        Return detailObj

    End Function


    Private Shared Sub RefreshBankFields(ByVal objtemplate As ICommonTemplate)

        'Sync these properties of BankFields (Common Template & Master Template)
        '1. DataLength
        '2. DefaultValue
        '3. DataSample
        '4. Mandatory
        '5. Map Separator
        '6. Include Decimal
        '7. Decimal Separator
        '8. Header
        '9. Detail
        '10. Trailer
        Dim _mstTmpUtility As New MasterTemplate.MasterTemplateSharedFunction

        If MasterTemplate.MasterTemplateSharedFunction.IsFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateFixedDetail In objmastertmp.MasterTemplateFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.IncludeDecimal = _masterFixedDetail.IncludeDecimal
                        bkfield.DecimalSeparator = _masterFixedDetail.DecimalSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        ElseIf MasterTemplate.MasterTemplateSharedFunction.IsNonFixedMasterTemplate(objtemplate.OutputTemplateName) Then

            Dim objmastertmp As New MasterTemplate.MasterTemplateNonFixed()
            objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
               Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, objtemplate.OutputTemplateName) _
                , _mstTmpUtility.MasterTemplateFileExtension))

            'modified by Kay @ 7/6/2011
            'Add for consolidate field
            If objtemplate.MapBankFields.Count = objmastertmp.MasterTemplateNonFixedDetailCollection.Count - 1 Then
                Dim bkfield As New MapBankField
                bkfield.BankField = "Consolidate Field"
                bkfield.DataLength = 255
                bkfield.DataType = "Text"
                Select Case objmastertmp.OutputFormat
                    Case "iFTS-2 MultiLine", "iFTS-2 SingleLine"
                        bkfield.Detail = "Payment"
                    Case "iRTMSCI", "iRTMSGC", "iRTMSGD", "iRTMSRM"
                        bkfield.Detail = "Transaction"
                    Case Else
                        bkfield.Detail = "Yes"
                End Select

                objtemplate.MapBankFields.Add(bkfield)

            End If

            For Each _masterFixedDetail As MasterTemplate.MasterTemplateNonFixedDetail In objmastertmp.MasterTemplateNonFixedDetailCollection
                For Each bkfield As MapBankField In objtemplate.MapBankFields
                    If bkfield.BankField = _masterFixedDetail.FieldName Then
                        bkfield.DefaultValue = _masterFixedDetail.DefaultValue
                        bkfield.DataLength = _masterFixedDetail.DataLength
                        bkfield.Mandatory = _masterFixedDetail.Mandatory
                        bkfield.MapSeparator = _masterFixedDetail.MapSeparator
                        bkfield.Header = _masterFixedDetail.Header
                        bkfield.Detail = _masterFixedDetail.Detail
                        bkfield.Trailer = _masterFixedDetail.Trailer

                        If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                            bkfield.BankSampleValue = ""
                        Else
                            bkfield.BankSampleValue = _masterFixedDetail.DataSample
                        End If
                    End If
                Next

            Next

        End If

    End Sub


#End Region


#Region "Preview / Convert Swift Transaction Data"
    ''' <summary>
    ''' Determines whether the given file is in valid SWIFT Format
    ''' </summary>
    ''' <param name="filename">Name of Swift File</param>
    ''' <returns>Returns a boolean value indicating whether the given file is in SWIFT Format</returns>
    ''' <remarks></remarks>
    Public Shared Function IsValidSwiftFile(ByVal filename As String) As Boolean

        If Not System.IO.File.Exists(filename) Then Return False

        Dim encoding As System.Text.Encoding = GetFileEncoding(filename)
        Dim SwiftReader As System.IO.StreamReader = New System.IO.StreamReader(filename, encoding)

        Try

            'Return SwiftReader.ReadLine().StartsWith(":20") ' Swift Tag for Transaction Reference No
            'Return SwiftReader.ReadLine().StartsWith("Q:") ' Swift Tag for Transaction Reference No
            Return True
        Catch ex As Exception

            Return False

        Finally

            SwiftReader.Close()

        End Try

        Return False

    End Function
    ''' <summary>
    ''' Retrieves a Sample Row of Transaction Record from the given Source File
    ''' </summary>
    ''' <param name="srcFileSwift">Name of Source File</param>
    ''' <remarks></remarks>
    Public Sub RetrieveSampleRow(ByVal srcFileSwift As String)

        Dim strSampleLine As String

        If Not SwiftTemplate.IsValidSwiftFile(srcFileSwift) Then Throw New System.Exception(MsgReader.GetString("E10000007"))

        Dim encoding As System.Text.Encoding = GetFileEncoding(srcFileSwift)

        Dim SwiftReader As System.IO.StreamReader = New System.IO.StreamReader(srcFileSwift, encoding)
        SampleRow = ""
        Dim stb As New System.Text.StringBuilder

        Try

            Do
                strSampleLine = SwiftReader.ReadLine()
                If strSampleLine = "-" Then Exit Do

                stb.AppendLine(strSampleLine)
            Loop Until SwiftReader.EndOfStream

            ' SampleRow = SampleRow & strSampleLine & vbCrLf
            SampleRow = stb.ToString

        Catch ex As Exception

            Throw New System.Exception(ex.Message)

        Finally

            SwiftReader.Close()

        End Try

        SourceFileName = srcFileSwift

        OnPropertyChanged("SourceFileName")
        OnPropertyChanged("SampleRow")

    End Sub

    ''' <summary>
    ''' Generates Source Fields from the given Swift File
    ''' </summary>
    ''' <param name="newSrcFilename">Name of Swift File</param>
    ''' <param name="namesSrcFields">List of existing Source Fields</param>
    ''' <param name="newMapSrcFields">List of New Source Fields</param>
    ''' <remarks></remarks>
    Public Sub GetSourceFields(ByVal newSrcFilename As String, ByRef namesSrcFields As List(Of String), Optional ByRef newMapSrcFields As MapSourceFieldCollection = Nothing)

        'MapSourceFields.Clear()
        Dim encoding As System.Text.Encoding = GetFileEncoding(newSrcFilename)

        Dim SwiftReader As System.IO.StreamReader = New System.IO.StreamReader(newSrcFilename, encoding)

        Dim strSwiftRowData As String
        Dim strSwiftTag As String = ""
        Dim iTagSequenceNo As Int32 = 0
        Dim isSet61 As Boolean = False
        Dim isSet86 As Boolean = False
        Dim isSet61_9 As Boolean = False

        Dim isResumeLine As Boolean = False
        Dim resumeLine As String = ""

        Try
            Do
                If isResumeLine Then
                    strSwiftRowData = resumeLine
                    isResumeLine = False
                Else
                    strSwiftRowData = SwiftReader.ReadLine()
                End If

                If strSwiftRowData = "-" Then Exit Do
                If strSwiftRowData = "-}" Then Exit Do

                If strSwiftRowData.StartsWith(":") Then

                    If isSet61 And Not isSet61_9 Then
                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-9"
                        _objMapSrcFld.SourceFieldValue = ""
                        newMapSrcFields.Add(_objMapSrcFld)
                        isSet61_9 = True
                    End If

                    iTagSequenceNo = 0
                    Dim swiftValues() As String = strSwiftRowData.Substring(1).Split(":")
                    If swiftValues.Length <= 1 Then Throw New System.Exception(MsgReader.GetString("E10000007"))
                    strSwiftTag = swiftValues(0)

                    Select Case strSwiftTag

                        Case "86"

                            If isSet86 Then
                                Continue Do
                            End If

                            Dim temp As String = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            Dim nextLine As String = ""

                            Dim count As Int16 = 0
                            While 1
                                If count >= 4 Then
                                    Exit While
                                End If

                                nextLine = SwiftReader.ReadLine()

                                If nextLine.StartsWith(":") Then
                                    isResumeLine = True
                                    resumeLine = nextLine
                                    Exit While
                                End If

                                temp = temp + " " + nextLine

                                count = count + 1
                            End While

                            Dim startIndexOfORD As Int16 = temp.IndexOf("(ORD)")
                            If startIndexOfORD > 0 Then
                                Dim endIndexOfORD As Int16 = temp.IndexOf("(", startIndexOfORD + 5)
                                If endIndexOfORD > 0 Then
                                    temp = temp.Substring(startIndexOfORD + 5, endIndexOfORD - (startIndexOfORD + 6))
                                Else
                                    temp = temp.Substring(startIndexOfORD + 5)
                                End If
                            Else
                                temp = ""
                            End If

                            namesSrcFields.Add("86-1")

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "86-1"
                            _objMapSrcFld.SourceFieldValue = temp 'IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            newMapSrcFields.Add(_objMapSrcFld)
                            isSet86 = True

                        Case "61"

                            If isSet61 Then
                                Continue Do
                            End If

                            Dim offset As Integer = 0
                            Dim temp As String = ""
                            Dim testInt As Integer

                            namesSrcFields.Add("61-1")
                            namesSrcFields.Add("61-2")
                            namesSrcFields.Add("61-3")
                            namesSrcFields.Add("61-4")
                            namesSrcFields.Add("61-5")
                            namesSrcFields.Add("61-6")
                            namesSrcFields.Add("61-7")
                            namesSrcFields.Add("61-8")

                            ' 61-1
                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-1"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 6)
                            newMapSrcFields.Add(_objMapSrcFld)
                            offset = 6

                            ' 61-2
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-2"
                            If Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                                offset = offset + 4
                            Else
                                _objMapSrcFld.SourceFieldValue = ""
                            End If
                            newMapSrcFields.Add(_objMapSrcFld)

                            ' 61-3
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-3"
                            If swiftValues(1).Substring(offset, 1).Equals("R") Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 2)
                                offset = offset + 2
                            Else
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                                offset = offset + 1
                            End If
                            newMapSrcFields.Add(_objMapSrcFld)

                            ' 61-4
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-4"
                            If Not Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                                offset = offset + 1
                            Else
                                _objMapSrcFld.SourceFieldValue = ""
                            End If
                            newMapSrcFields.Add(_objMapSrcFld)

                            ' 61-5
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-5"
                            testInt = swiftValues(1).Substring(offset).IndexOf("N")
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                            newMapSrcFields.Add(_objMapSrcFld)
                            offset = offset + testInt

                            ' 61-6
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-6"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                            newMapSrcFields.Add(_objMapSrcFld)
                            offset = offset + 4

                            ' 61-7
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-7"
                            testInt = swiftValues(1).Substring(offset).IndexOf("/")
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                            newMapSrcFields.Add(_objMapSrcFld)
                            offset = offset + testInt

                            ' 61-8
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-8"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset)
                            newMapSrcFields.Add(_objMapSrcFld)

                            ' 61-9
                            iTagSequenceNo = 8

                            isSet61 = True

                        Case "25"

                            namesSrcFields.Add("25-1")
                            namesSrcFields.Add("25-3")

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "25-1"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 3)
                            newMapSrcFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "25-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(8)
                            newMapSrcFields.Add(_objMapSrcFld)

                        Case "60F"

                            namesSrcFields.Add("60-2")
                            namesSrcFields.Add("60-3")

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-2"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                            newMapSrcFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                            newMapSrcFields.Add(_objMapSrcFld)

                        Case "60M"

                            namesSrcFields.Add("60-2")
                            namesSrcFields.Add("60-3")

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-2"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                            newMapSrcFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                            newMapSrcFields.Add(_objMapSrcFld)

                        Case "32A" 'Pre determined to have 3 values

                            namesSrcFields.Add("32A-1")
                            namesSrcFields.Add("32A-2")
                            namesSrcFields.Add("32A-3")

                            If Not newMapSrcFields Is Nothing Then
                                Dim _objMapSrcFld As New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "32A-1"
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 6) 'First value is a Date
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "32A-2"
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(6, 3) 'Currency Code
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "32A-3"
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(9) 'Amount
                                newMapSrcFields.Add(_objMapSrcFld)
                            End If

                        Case "59"       'Pre determined to have 5 values
                            namesSrcFields.Add("59-1")
                            namesSrcFields.Add("59-2")
                            namesSrcFields.Add("59-3")
                            namesSrcFields.Add("59-4")
                            namesSrcFields.Add("59-5")

                            If Not newMapSrcFields Is Nothing Then
                                Dim _objMapSrcFld As New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "59-1"
                                If (swiftValues(1).StartsWith("/")) Then
                                    _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                                Else
                                    _objMapSrcFld.SourceFieldValue = swiftValues(1)
                                End If

                                ' _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "59-2"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "59-3"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "59-4"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "59-5"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)
                            End If



                        Case "70" 'Pre determined to have 4 values

                            namesSrcFields.Add("70-1")
                            namesSrcFields.Add("70-2")
                            namesSrcFields.Add("70-3")
                            namesSrcFields.Add("70-4")

                            If Not newMapSrcFields Is Nothing Then
                                Dim _objMapSrcFld As New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "70-1"

                                If (swiftValues(1).StartsWith("/")) Then
                                    _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                                Else
                                    _objMapSrcFld.SourceFieldValue = swiftValues(1)
                                End If


                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "70-2"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "70-3"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)

                                _objMapSrcFld = New MapSourceField()
                                _objMapSrcFld.SourceFieldName = "70-4"
                                _objMapSrcFld.SourceFieldValue = ""
                                newMapSrcFields.Add(_objMapSrcFld)
                            End If

                        Case Else

                            iTagSequenceNo = iTagSequenceNo + 1
                            namesSrcFields.Add(String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo))

                            If Not newMapSrcFields Is Nothing Then
                                Dim _objMapSrcFld As New MapSourceField()
                                _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)
                                'Remove the very first "/", if there is any in the value part
                                _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                                newMapSrcFields.Add(_objMapSrcFld)
                            End If

                    End Select

                Else

                    If strSwiftTag = "61" Then
                        If isSet61_9 Then
                            Continue Do
                        Else
                            isSet61_9 = True
                        End If
                    End If

                    iTagSequenceNo = iTagSequenceNo + 1

                    If (strSwiftTag = "70" Or strSwiftTag = "59") Then
                        If Not newMapSrcFields Is Nothing Then
                            For Each srcField As MapSourceField In newMapSrcFields
                                If srcField.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo + 1) Then

                                    srcField.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                                    Exit For
                                End If
                            Next
                        End If

                    Else
                        namesSrcFields.Add(String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo))
                        If Not newMapSrcFields Is Nothing Then
                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)

                            _objMapSrcFld.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                            newMapSrcFields.Add(_objMapSrcFld)
                        End If
                    End If

                End If

            Loop Until SwiftReader.EndOfStream

        Catch ex As Exception

            Throw New System.Exception(ex.Message)

        Finally

            SwiftReader.Close()

        End Try

    End Sub

    ''' <summary>
    ''' Generates a list of Source Fields from the source file
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateSourceFields()

        Filters.Clear()
        MapSourceFields.Clear()

        Dim encoding As System.Text.Encoding = GetFileEncoding(SourceFileName)
        Dim SwiftReader As System.IO.StreamReader = New System.IO.StreamReader(SourceFileName, encoding)

        Dim strSwiftRowData As String
        Dim strSwiftTag As String = ""
        Dim iTagSequenceNo As Int32 = 0
        Dim isSet61 As Boolean = False
        Dim isSet86 As Boolean = False
        Dim isSet61_9 As Boolean = False

        Dim isResumeLine As Boolean = False
        Dim resumeLine As String = ""

        Try
            Do
                If isResumeLine Then
                    strSwiftRowData = resumeLine
                    isResumeLine = False
                Else
                    strSwiftRowData = SwiftReader.ReadLine()
                End If

                If strSwiftRowData = "-" Then Exit Do
                If strSwiftRowData = "-}" Then Exit Do

                If strSwiftRowData.StartsWith(":") Then

                    If isSet61 And Not isSet61_9 Then
                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-9"
                        _objMapSrcFld.SourceFieldValue = ""
                        MapSourceFields.Add(_objMapSrcFld)
                        isSet61_9 = True
                    End If

                    iTagSequenceNo = 0
                    Dim swiftValues() As String = strSwiftRowData.Substring(1).Split(":")
                    If swiftValues.Length <= 1 Then Throw New System.Exception(MsgReader.GetString("E10000007"))
                    strSwiftTag = swiftValues(0)

                    Select Case strSwiftTag

                        Case "86"

                            If isSet86 Then
                                Continue Do
                            End If

                            Dim temp As String = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            Dim nextLine As String = ""

                            Dim count As Int16 = 0
                            While 1
                                If count >= 4 Then
                                    Exit While
                                End If

                                nextLine = SwiftReader.ReadLine()

                                If nextLine.StartsWith(":") Then
                                    isResumeLine = True
                                    resumeLine = nextLine
                                    Exit While
                                End If

                                temp = temp + " " + nextLine

                                count = count + 1
                            End While

                            Dim startIndexOfORD As Int16 = temp.IndexOf("(ORD)")
                            If startIndexOfORD > 0 Then
                                Dim endIndexOfORD As Int16 = temp.IndexOf("(", startIndexOfORD + 5)
                                If endIndexOfORD > 0 Then
                                    temp = temp.Substring(startIndexOfORD + 5, endIndexOfORD - (startIndexOfORD + 6))
                                Else
                                    temp = temp.Substring(startIndexOfORD + 5)
                                End If
                            Else
                                temp = ""
                            End If

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "86-1"
                            _objMapSrcFld.SourceFieldValue = temp 'IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            MapSourceFields.Add(_objMapSrcFld)
                            isSet86 = True

                        Case "61"

                            If isSet61 Then
                                Continue Do
                            End If

                            Dim offset As Integer = 0
                            Dim temp As String = ""
                            Dim testInt As Integer

                            ' 61-1
                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-1"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 6)
                            MapSourceFields.Add(_objMapSrcFld)
                            offset = 6

                            ' 61-2
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-2"
                            If Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                                offset = offset + 4
                            Else
                                _objMapSrcFld.SourceFieldValue = ""
                            End If
                            MapSourceFields.Add(_objMapSrcFld)

                            ' 61-3
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-3"
                            If swiftValues(1).Substring(offset, 1).Equals("R") Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 2)
                                offset = offset + 2
                            Else
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                                offset = offset + 1
                            End If
                            MapSourceFields.Add(_objMapSrcFld)

                            ' 61-4
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-4"
                            If Not Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                                offset = offset + 1
                            Else
                                _objMapSrcFld.SourceFieldValue = ""
                            End If
                            MapSourceFields.Add(_objMapSrcFld)

                            ' 61-5
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-5"
                            testInt = swiftValues(1).Substring(offset).IndexOf("N")
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                            MapSourceFields.Add(_objMapSrcFld)
                            offset = offset + testInt

                            ' 61-6
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-6"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                            MapSourceFields.Add(_objMapSrcFld)
                            offset = offset + 4

                            ' 61-7
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-7"
                            testInt = swiftValues(1).Substring(offset).IndexOf("/")
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                            MapSourceFields.Add(_objMapSrcFld)
                            offset = offset + testInt

                            ' 61-8
                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "61-8"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset)
                            MapSourceFields.Add(_objMapSrcFld)

                            ' 61-9
                            iTagSequenceNo = 8

                            isSet61 = True

                        Case "25"

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "25-1"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 3)
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "25-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(8)
                            MapSourceFields.Add(_objMapSrcFld)

                        Case "60F"

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-2"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                            MapSourceFields.Add(_objMapSrcFld)

                        Case "60M"

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-2"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "60-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                            MapSourceFields.Add(_objMapSrcFld)

                        Case "32A"

                            'Pre determined to have 3 values

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "32A-1"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 6) 'First value is a Date
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "32A-2"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(6, 3) 'Currency Code
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "32A-3"
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(9) 'Amount
                            MapSourceFields.Add(_objMapSrcFld)

                        Case "70"
                            'Pre determined to have 4 values

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "70-1"
                            If (swiftValues(1).StartsWith("/")) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                            Else
                                _objMapSrcFld.SourceFieldValue = swiftValues(1)
                            End If
                            ' _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "70-2"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "70-3"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "70-4"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                        Case "59"
                            'Pre determined to have 5 values

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "59-1"
                            If (swiftValues(1).StartsWith("/")) Then
                                _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                            Else
                                _objMapSrcFld.SourceFieldValue = swiftValues(1)
                            End If
                            '_objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "59-2"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "59-3"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "59-4"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)

                            _objMapSrcFld = New MapSourceField()
                            _objMapSrcFld.SourceFieldName = "59-5"
                            _objMapSrcFld.SourceFieldValue = ""
                            MapSourceFields.Add(_objMapSrcFld)


                        Case Else

                            iTagSequenceNo = iTagSequenceNo + 1

                            Dim _objMapSrcFld As New MapSourceField()
                            _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)
                            'Remove the very first "/", if there is any in the value part
                            _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                            MapSourceFields.Add(_objMapSrcFld)

                    End Select

                Else

                    If strSwiftTag = "61" Then
                        If isSet61_9 Then
                            Continue Do
                        Else
                            isSet61_9 = True
                        End If
                    End If

                    iTagSequenceNo = iTagSequenceNo + 1

                    If (strSwiftTag = "70" Or strSwiftTag = "59") Then
                        For Each srcField As MapSourceField In MapSourceFields
                            If srcField.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo + 1) Then
                                srcField.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                                Exit For
                            End If
                        Next

                    Else
                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)
                        _objMapSrcFld.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                        MapSourceFields.Add(_objMapSrcFld)
                    End If

                End If

            Loop Until SwiftReader.EndOfStream

        Catch ex As Exception

            Throw New System.Exception(ex.Message)

        Finally

            SwiftReader.Close()

        End Try

        OnPropertyChanged("MapSourceFields")

    End Sub

    Public Sub SetDefaultTranslation()
        OnPropertyChanged("OutputTemplateName")
    End Sub

    Private Sub GenerateSourceData(ByVal SourceFileForSwiftTemplate As String)
        Try

            If Not IsValidSwiftFile(SourceFileForSwiftTemplate) Then Throw New System.Exception(MsgReader.GetString("E10000007"))

            Dim strSourceData As String = File.ReadAllText(SourceFileForSwiftTemplate)

            If Not _curOutputTemplateName = "GCMS Plus Credit Data Format" Then
                Dim regMatch As Match = Regex.Match(strSourceData, ":20:(.)*?(\n)+?-(" & vbCrLf & "|$)", RegexOptions.Singleline)

                While regMatch.Success
                    ParseSourceFields(regMatch.Value)
                    regMatch = regMatch.NextMatch()
                End While
            Else
                Dim bodyStart As Integer = strSourceData.IndexOf(":61:")
                Dim bodyEnd As Integer = strSourceData.IndexOf(":62")
                Dim header As String = strSourceData.Substring(0, bodyStart)
                Dim footer As String = strSourceData.Substring(bodyEnd)
                Dim body As String = strSourceData.Substring(bodyStart, bodyEnd - bodyStart)

                Dim sepatators() As String = {":61:"}
                Dim records As String() = body.Split(sepatators, StringSplitOptions.RemoveEmptyEntries)
                For Each record As String In records
                    ParseSourceFields(header & ":61:" & record & footer)
                Next
            End If
            
        Catch ex As Exception
            Throw New Exception("Error on reading Source File(GenerateSourceData) : " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub ParseSourceFields(ByVal strSourceText As String)

        Dim tmpMapSourceFields As New MapSourceFieldCollection()

        tmpMapSourceFields.Clear()

        Dim SwiftReader As System.IO.StringReader = New StringReader(strSourceText)

        Dim strSwiftRowData As String
        Dim strSwiftTag As String = ""
        Dim iTagSequenceNo As Int32 = 0
        Dim isSet61 As Boolean = False
        Dim isSet86 As Boolean = False
        Dim isSet61_9 As Boolean = False

        Dim isResumeLine As Boolean = False
        Dim resumeLine As String = ""

        Do
            If isResumeLine Then
                strSwiftRowData = resumeLine
                isResumeLine = False
            Else
                strSwiftRowData = SwiftReader.ReadLine()
            End If

            If strSwiftRowData = "-" Then Exit Do
            If strSwiftRowData = "-}" Then Exit Do

            If strSwiftRowData.StartsWith(":") Then

                If isSet61 And Not isSet61_9 Then
                    Dim _objMapSrcFld As New MapSourceField()
                    _objMapSrcFld.SourceFieldName = "61-9"
                    _objMapSrcFld.SourceFieldValue = ""
                    tmpMapSourceFields.Add(_objMapSrcFld)
                    isSet61_9 = True
                End If

                iTagSequenceNo = 0
                Dim swiftValues() As String = strSwiftRowData.Substring(1).Split(":")
                If swiftValues.Length <= 1 Then Throw New System.Exception(MsgReader.GetString("E10000007"))
                strSwiftTag = swiftValues(0)

                Select Case strSwiftTag

                    Case "86"

                        If isSet86 Then
                            Continue Do
                        End If

                        Dim temp As String = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                        Dim nextLine As String = ""

                        Dim count As Int16 = 0
                        While 1
                            If count >= 4 Then
                                Exit While
                            End If

                            nextLine = SwiftReader.ReadLine()

                            If nextLine.StartsWith(":") Then
                                isResumeLine = True
                                resumeLine = nextLine
                                Exit While
                            End If

                            temp = temp + " " + nextLine

                            count = count + 1
                        End While

                        Dim startIndexOfORD As Int16 = temp.IndexOf("(ORD)")
                        If startIndexOfORD > 0 Then
                            Dim endIndexOfORD As Int16 = temp.IndexOf("(", startIndexOfORD + 5)
                            If endIndexOfORD > 0 Then
                                temp = temp.Substring(startIndexOfORD + 5, endIndexOfORD - (startIndexOfORD + 6))
                            Else
                                temp = temp.Substring(startIndexOfORD + 5)
                            End If
                        Else
                            temp = ""
                        End If

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "86-1"
                        _objMapSrcFld.SourceFieldValue = temp 'IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                        tmpMapSourceFields.Add(_objMapSrcFld)
                        isSet86 = True

                    Case "61"

                        If isSet61 Then
                            Continue Do
                        End If

                        Dim offset As Integer = 0
                        Dim temp As String = ""
                        Dim testInt As Integer

                        ' 61-1
                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-1"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 6)
                        tmpMapSourceFields.Add(_objMapSrcFld)
                        offset = 6

                        ' 61-2
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-2"
                        If Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                            offset = offset + 4
                        Else
                            _objMapSrcFld.SourceFieldValue = ""
                        End If
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        ' 61-3
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-3"
                        If swiftValues(1).Substring(offset, 1).Equals("R") Then
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 2)
                            offset = offset + 2
                        Else
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                            offset = offset + 1
                        End If
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        ' 61-4
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-4"
                        If Not Integer.TryParse(swiftValues(1).Substring(offset, 1), testInt) Then
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 1)
                            offset = offset + 1
                        Else
                            _objMapSrcFld.SourceFieldValue = ""
                        End If
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        ' 61-5
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-5"
                        testInt = swiftValues(1).Substring(offset).IndexOf("N")
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                        tmpMapSourceFields.Add(_objMapSrcFld)
                        offset = offset + testInt

                        ' 61-6
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-6"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, 4)
                        tmpMapSourceFields.Add(_objMapSrcFld)
                        offset = offset + 4

                        ' 61-7
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-7"
                        testInt = swiftValues(1).Substring(offset).IndexOf("/")
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset, testInt)
                        tmpMapSourceFields.Add(_objMapSrcFld)
                        offset = offset + testInt

                        ' 61-8
                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "61-8"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(offset)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        ' 61-9
                        iTagSequenceNo = 8

                        isSet61 = True

                    Case "25"

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "25-1"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 3)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "25-3"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(8)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                    Case "60F"

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "60-2"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "60-3"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                    Case "60M"

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "60-2"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(1, 6)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "60-3"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(7, 3)
                        tmpMapSourceFields.Add(_objMapSrcFld)

                    Case "32A"

                        'Pre determined to have 3 values

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "32A-1"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(0, 6) 'First value is a Date
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "32A-2"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(6, 3) 'Currency Code
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "32A-3"
                        _objMapSrcFld.SourceFieldValue = swiftValues(1).Substring(9) 'Amount
                        tmpMapSourceFields.Add(_objMapSrcFld)

                    Case "59"
                        'Pre determined to have 5 values

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "59-1"
                        If (swiftValues(1).StartsWith("/")) Then
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                        Else
                            _objMapSrcFld.SourceFieldValue = swiftValues(1)
                        End If
                        ' _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "59-2"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "59-3"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "59-4"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "59-5"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                    Case "70"
                        'Pre determined to have 4 values

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "70-1"
                        If (swiftValues(1).StartsWith("/")) Then
                            _objMapSrcFld.SourceFieldValue = swiftValues(1).Remove(0, 1)
                        Else
                            _objMapSrcFld.SourceFieldValue = swiftValues(1)
                        End If
                        '_objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "70-2"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "70-3"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)

                        _objMapSrcFld = New MapSourceField()
                        _objMapSrcFld.SourceFieldName = "70-4"
                        _objMapSrcFld.SourceFieldValue = ""
                        tmpMapSourceFields.Add(_objMapSrcFld)


                    Case Else

                        iTagSequenceNo = iTagSequenceNo + 1

                        Dim _objMapSrcFld As New MapSourceField()
                        _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)
                        'Remove the very first "/", if there is any in the value part
                        _objMapSrcFld.SourceFieldValue = IIf(swiftValues(1).StartsWith("/"), swiftValues(1).Remove(0, 1), swiftValues(1))
                        tmpMapSourceFields.Add(_objMapSrcFld)

                End Select

            Else

                If strSwiftTag = "61" Then
                    If isSet61_9 Then
                        Continue Do
                    Else
                        isSet61_9 = True
                    End If
                End If

                iTagSequenceNo = iTagSequenceNo + 1

                If (strSwiftTag = "70" Or strSwiftTag = "59") Then
                    For Each srcField As MapSourceField In tmpMapSourceFields
                        If srcField.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo + 1) Then
                            srcField.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                            Exit For
                        End If
                    Next

                Else
                    Dim _objMapSrcFld As New MapSourceField()
                    _objMapSrcFld.SourceFieldName = String.Format("{0}-{1}", strSwiftTag, iTagSequenceNo)
                    _objMapSrcFld.SourceFieldValue = strSwiftRowData '.StartsWith("/"), strSwiftRowData.Remove(0, 1), strSwiftRowData)
                    tmpMapSourceFields.Add(_objMapSrcFld)
                End If

            End If

        Loop Until SwiftReader.Peek() = -1

        SwiftReader.Close()

        'Add it to DataSOurce
        Dim newrecord As DataRow = _SourceData.NewRow()

        For Each srcField As MapSourceField In tmpMapSourceFields
            For Each col As DataColumn In _SourceData.Columns
                If col.ColumnName.Equals(srcField.SourceFieldName, StringComparison.InvariantCultureIgnoreCase) Then
                    newrecord(srcField.SourceFieldName) = srcField.SourceFieldValue
                End If
            Next

        Next

        _SourceData.Rows.Add(newrecord)

    End Sub

    Private Sub ApplyReferenceFieldSetting()
        Try
            Dim strGroupByColumns As String = String.Format("{0},{1},{2}" _
                                        , DuplicateTxnRef1, DuplicateTxnRef2, DuplicateTxnRef3).Trim(",")

            Dim _intRecordCount As Int32 = _PreviewSourceData.Rows.Count

            If _intRecordCount = 0 Or strGroupByColumns = "" Then Exit Sub


            Dim _strFilter As String = ""
            Dim _dtviewTxnFieldGroup As DataView

            For _counter As Int32 = 0 To _intRecordCount - 1

                _strFilter = "1=1"

                If DuplicateTxnRef1 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef1), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef1)))
                If DuplicateTxnRef2 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef2), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef2)))
                If DuplicateTxnRef3 <> "" Then _strFilter = String.Format(" {0} AND {1}='{2}'", _strFilter, HelperModule.EscapeSpecialCharsForColumnName(DuplicateTxnRef3), HelperModule.EscapeSingleQuote(_PreviewSourceData.Rows(_counter)(DuplicateTxnRef3)))

                _dtviewTxnFieldGroup = _PreviewSourceData.DefaultView
                _dtviewTxnFieldGroup.RowFilter = _strFilter

                If _dtviewTxnFieldGroup.Count > 1 Then
                    Dim stbError As New System.Text.StringBuilder
                    For Each dv As DataRowView In _dtviewTxnFieldGroup
                        Dim line As Integer = _PreviewSourceData.Rows.IndexOf(dv.Row) + 1
                        If DuplicateTxnRef1 <> "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005020") _
                                           , line, DuplicateTxnRef1, DuplicateTxnRef2, DuplicateTxnRef3))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005030") _
                                           , line, DuplicateTxnRef1, DuplicateTxnRef2))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005040") _
                                            , line, DuplicateTxnRef1, DuplicateTxnRef3))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005050") _
                                           , line, DuplicateTxnRef2, DuplicateTxnRef3))
                        ElseIf DuplicateTxnRef1 <> "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005060") _
                                           , line, DuplicateTxnRef1))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 <> "" And DuplicateTxnRef3 = "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005070") _
                                           , line, DuplicateTxnRef2))
                        ElseIf DuplicateTxnRef1 = "" And DuplicateTxnRef2 = "" And DuplicateTxnRef3 <> "" Then
                            stbError.AppendLine(String.Format(MsgReader.GetString("E0005080") _
                                                                   , line, DuplicateTxnRef3))

                        End If

                    Next
                    If stbError.ToString <> String.Empty Then
                        _dtviewTxnFieldGroup.RowFilter = String.Empty
                        Throw New Exception(stbError.ToString())
                    End If

                End If
                _dtviewTxnFieldGroup.RowFilter = String.Empty
            Next
        Catch ex As Exception
            Throw New Exception("Error applying Reference Field Settings : " & vbNewLine & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Prepares the Template for previewing
    ''' </summary>
    ''' <param name="strOutputFileFormat">Name of Output File format</param>
    ''' <param name="SourceFile">Name of source file</param>
    ''' <param name="WorksheetName">Name of work sheet</param>
    ''' <param name="TransactionEndRow">Row number of last transaction record</param>
    ''' <param name="RemoveRowsFromEnd">The number of rows to be removed from the very last record in the Source File</param>
    ''' <remarks></remarks>
    Public Sub PrepareForPreview(Optional ByVal strOutputFileFormat As String = "" _
                                , Optional ByVal SourceFile As String = "" _
                                , Optional ByVal WorksheetName As String = "" _
                                , Optional ByVal TransactionEndRow As Integer = 0 _
                                , Optional ByVal RemoveRowsFromEnd As Integer = 0 _
                ) Implements ICommonTemplate.PrepareForPreview

        '#1. Prepare SourceData
        _SourceData = New DataTable("SourceData")

        For Each SourceField As MapSourceField In MapSourceFields
            _SourceData.Columns.Add(New DataColumn(SourceField.SourceFieldName, GetType(String)))
        Next

        '#2. Copy Source Fields
        If SourceFile = "" Then 'Preview For Sample Record

            Dim CurSourceRecord As DataRow = _SourceData.NewRow()
            For Each SourceField As MapSourceField In MapSourceFields
                CurSourceRecord(SourceField.SourceFieldName) = SourceField.SourceFieldValue
            Next

            _SourceData.Rows.Add(CurSourceRecord)

        Else 'Preview for Conversion

            GenerateSourceData(SourceFile)

        End If

        '#3. Apply Reference Field Setting
        'If _SourceData.Rows.Count > 1 Then ApplyReferenceFieldSetting()

        '#4. Apply Filters on Source Data
        _FilteredSourceData = _SourceData.DefaultView
        _FilteredSourceData.RowFilter = ""
        If Filters.Count > 0 Then ApplyRowFilters()

        If _FilteredSourceData.Count = 0 Then Throw New System.Exception(MsgReader.GetString("E10000005"))

        '#5 Format Date Values to Bank Field Date Format
        FormatDateValuesToBankFieldDateFormat()

        '#6 Format Numeric Values to Bank Field Number Format
        FormatNumberValuesToBankFieldNumberFormat()

    End Sub
    Public Sub DuplicateCheck(Optional ByVal fileFormat As String = "") Implements ICommonTemplate.DuplicateCheck
        ApplyReferenceFieldSetting()
    End Sub
    Private Sub ApplyRowFilters()
        Try
            '#4.1. '''Apply Date/Number Setting here''''''''''
            Dim origCulture = System.Threading.Thread.CurrentThread.CurrentCulture
            Dim newCulture As New System.Globalization.CultureInfo("en-US", True)
            newCulture.NumberFormat.NumberDecimalSeparator = IIf(DecimalSeparator = "No Separator", ".", DecimalSeparator)
            newCulture.NumberFormat.NumberGroupSeparator = IIf(ThousandSeparator = "No Separator", "", ThousandSeparator)
            newCulture.DateTimeFormat.ShortDatePattern = GetDateFormatPattern(DateSeparator, DateType)
            newCulture.DateTimeFormat.LongDatePattern = newCulture.DateTimeFormat.ShortDatePattern
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 

            '#4.2. '''Rename  Columns referred in the Row Filter and add a column for each of the "Filter Type"s
            ' Make sure there is no duplicate column added for the duplicate filters

            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) > -1 AndAlso _SourceData.Columns.IndexOf(_flt.FilterField & "_0") > -1 Then Continue For

                If _flt.FilterType = "Numeric" Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.Decimal))
                ElseIf _flt.FilterType = "Date" Then
                    _SourceData.Columns(_flt.FilterField).ColumnName = _flt.FilterField & "_0"
                    _SourceData.Columns.Add(_flt.FilterField, GetType(System.DateTime))
                End If

            Next

            '#4.3 ''' Convert and Copy values to Columns referred in Row Filter 
            ' a null value is assumed in the place where data conversion fails
            For Each row As DataRow In _SourceData.Rows

                For Each _flt As RowFilter In Filters
                    Select Case _flt.FilterType
                        Case "Numeric"

                            If Decimal.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
                                row(_flt.FilterField) = Convert.ToDecimal(row(_flt.FilterField & "_0"))
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                        Case "Date"

                            If DateTime.TryParse(row(_flt.FilterField & "_0"), Nothing) Then
                                row(_flt.FilterField) = Convert.ToDateTime(row(_flt.FilterField & "_0"))
                            Else
                                row(_flt.FilterField) = DBNull.Value
                            End If

                    End Select
                Next

            Next

            '#4.4. '''Filter the rows
            _FilteredSourceData.RowFilter = Filters.ConditionString(IIf(IsFilterConditionAND, "AND", "OR"))

            '#4.5. '''Remove Date/Number Setting '''''''''''''''''''''''''''''''''''''
            System.Threading.Thread.CurrentThread.CurrentCulture = origCulture

            '#4.6. '''Remove expression columns
            For Each _flt As RowFilter In Filters

                If _SourceData.Columns.IndexOf(_flt.FilterField) = -1 Or _SourceData.Columns.IndexOf(_flt.FilterField & "_0") = -1 Then Continue For

                _SourceData.Columns.Remove(_flt.FilterField)
                _SourceData.Columns(_flt.FilterField & "_0").ColumnName = _flt.FilterField

            Next


            'Just to please the bluddy DataView/DataTable data update behaviour!!!
            Dim tmpData As DataTable
            tmpData = _FilteredSourceData.ToTable()
            _FilteredSourceData.Dispose()
            _SourceData.Dispose()
            _SourceData = tmpData
            _FilteredSourceData = _SourceData.DefaultView
        Catch ex As Exception
            Throw New Exception("Error applying Filter Settings : " & vbNewLine & ex.Message.ToString)
        End Try

    End Sub

    Private Sub FormatDateValuesToBankFieldDateFormat()

        'a. Date Values in the Source Fields must comply to the date type specified in the Form <Retrieve Tab>
        'b. Convert the Date value to a date format as specified in the master template 'Date Format' property and update the Source Data Table


        Dim _convertedDateValue As DateTime
        Dim _dtMpSrcField As MapSourceField
        Dim _strBankFieldDateFormat As String
        Dim rowNo As Integer = 0

        If Not _curOutputTemplateName = "GCMS Plus Credit Data Format" Then
            For Each row As DataRowView In _FilteredSourceData

                If row("32A-1") Is Nothing Then Continue For

                If row("32A-1").ToString() = "" Then Continue For

                If row("32A-1").ToString().Length <> DateType.Length Or (Not (Common.HelperModule.IsDateDataType(row("32A-1").ToString(), "", DateType, _convertedDateValue, False))) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005100"), rowNo + 1, "32A-1", "Record"))

                'Convert Date value from the date format of Source File to that of Master Template
                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = "32A-1" : _dtMpSrcField.SourceFieldValue = row("32A-1").ToString()
                _strBankFieldDateFormat = GetBankFieldDateFormatForSourceField(_dtMpSrcField)
                If _strBankFieldDateFormat = "" Then Continue For

                row("32A-1") = _convertedDateValue.ToString(_strBankFieldDateFormat, HelperModule.enUSCulture)
                rowNo += 1
            Next
        Else
            For Each row As DataRowView In _FilteredSourceData

                If row("61-1") Is Nothing Then Continue For

                If row("61-1").ToString() = "" Then Continue For

                If row("61-1").ToString().Length <> DateType.Length Or (Not (Common.HelperModule.IsDateDataType(row("61-1").ToString(), "", DateType, _convertedDateValue, False))) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005100"), rowNo + 1, "61-1", "Record"))

                'Convert Date value from the date format of Source File to that of Master Template
                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = "61-1" : _dtMpSrcField.SourceFieldValue = row("61-1").ToString()
                _strBankFieldDateFormat = GetBankFieldDateFormatForSourceField(_dtMpSrcField)
                If _strBankFieldDateFormat = "" Then Continue For

                row("61-1") = _convertedDateValue.ToString(_strBankFieldDateFormat, HelperModule.enUSCulture)
                rowNo += 1
            Next

            For Each row As DataRowView In _FilteredSourceData

                If row("60-2") Is Nothing Then Continue For

                If row("60-2").ToString() = "" Then Continue For

                If row("60-2").ToString().Length <> DateType.Length Or (Not (Common.HelperModule.IsDateDataType(row("60-2").ToString(), "", DateType, _convertedDateValue, False))) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005100"), rowNo + 1, "60-2", "Record"))

                'Convert Date value from the date format of Source File to that of Master Template
                _dtMpSrcField = New MapSourceField()
                _dtMpSrcField.SourceFieldName = "60-2" : _dtMpSrcField.SourceFieldValue = row("60-2").ToString()
                _strBankFieldDateFormat = GetBankFieldDateFormatForSourceField(_dtMpSrcField)
                If _strBankFieldDateFormat = "" Then Continue For

                row("60-2") = _convertedDateValue.ToString(_strBankFieldDateFormat, HelperModule.enUSCulture)
                rowNo += 1
            Next
        End If

    End Sub

    Private Sub FormatNumberValuesToBankFieldNumberFormat()

        'a. Numeric Values in the Source Fields must comply to the 'Number Setting' specified in the Form <Retrieve Tab>
        'b. Convert the Numeric Values in the source fields to a Number format  that uses . as decimal separator and , as group separator 

        Dim _convertedNumberValue As Decimal
        Dim rowNo As Integer = 0

        If Not _curOutputTemplateName = "GCMS Plus Credit Data Format" Then
            For Each row As DataRowView In _FilteredSourceData

                If HelperModule.IsNothingOrEmptyString(row("32A-3")) Then Continue For


                'Prefix the Negative Number Symbol '-', in case its found suffixed at the value
                If row("32A-3").ToString().EndsWith("-") Then row("32A-3") = "-" & row("32A-3").ToString().Substring(0, row("32A-3").ToString().Length - 1)

                If Not Common.HelperModule.IsDecimalDataType(row("32A-3").ToString(), DecimalSeparator, "", _convertedNumberValue) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005110"), rowNo + 1, "32A-3", "Record"))

                If DecimalSeparator = "," Then row("32A-3") = row("32A-3").ToString().Replace(",", ".")
                rowNo += 1
            Next
        Else
            For Each row As DataRowView In _FilteredSourceData

                If HelperModule.IsNothingOrEmptyString(row("61-5")) Then Continue For


                'Prefix the Negative Number Symbol '-', in case its found suffixed at the value
                If row("61-5").ToString().EndsWith("-") Then row("61-5") = "-" & row("61-5").ToString().Substring(0, row("61-5").ToString().Length - 1)

                If Not Common.HelperModule.IsDecimalDataType(row("61-5").ToString(), DecimalSeparator, "", _convertedNumberValue) Then Throw New System.Exception(String.Format(MsgReader.GetString("E0005110"), rowNo + 1, "61-5", "Record"))

                If DecimalSeparator = "," Then row("61-5") = row("61-5").ToString().Replace(",", ".")
                rowNo += 1
            Next
        End If
    End Sub

    ' return the dateformat stored in the bank field, if the bank field's date type is datetime
    Private Function GetBankFieldDateFormatForSourceField(ByVal SrcField As MapSourceField) As String

        For Each _bnkField As MapBankField In MapBankFields

            If Not _bnkField.DataType = "DateTime" Then Continue For

            If _bnkField.MappedSourceFields.IsFound(SrcField) Then Return _bnkField.DateFormat.Replace("D", "d").Replace("Y", "y")

        Next

        Return ""

    End Function

    ''' <summary>
    ''' This property is to Get Filtered Source Data
    ''' </summary>
    ''' <value>DataView</value>
    ''' <returns>Returns a Data Table of Records after applying Row Filter Conditions</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property FilteredSourceData() As System.Data.DataView Implements ICommonTemplate.FilteredSourceData
        Get
            Return _FilteredSourceData
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get Source Data
    ''' </summary>
    ''' <value>DataTable</value>
    ''' <returns>Returns a Data Table of Transaction Records from Source File</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SourceData() As System.Data.DataTable Implements ICommonTemplate.SourceData
        Get
            Return _SourceData
        End Get
    End Property

    Public Property PreviewSourceData() As System.Data.DataTable Implements ICommonTemplate.PreviewSourceData
        Get
            Return _PreviewSourceData
        End Get
        Set(ByVal value As System.Data.DataTable)
            _PreviewSourceData = value
        End Set
    End Property

#End Region


End Class
''' <summary>
''' Collection of Common Transaction Template for Swift File
''' </summary>
''' <remarks></remarks>
Partial Public Class SwiftTemplateCollection
    Inherits BusinessBaseCollection(Of SwiftTemplate)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of SwiftTemplate)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of SwiftTemplate)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of SwiftTemplate)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    'Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
    '    Dim child As SwiftTemplate
    '    reader.ReadStartElement("SwiftTemplateCollection")
    '    While reader.LocalName = "SwiftTemplate"
    '        child = New SwiftTemplate
    '        DirectCast(child, IXmlSerializable).ReadXml(reader)
    '        Me.Add(child)
    '    End While
    '    If Me.Count > 0 Then reader.ReadEndElement()
    'End Sub
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As SwiftTemplate
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("SwiftTemplateCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "SwiftTemplate"
            child = New SwiftTemplate
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub

    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As SwiftTemplate
        writer.WriteStartElement("SwiftTemplateCollection")
        For Each child In Me
            writer.WriteStartElement("SwiftTemplate")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
