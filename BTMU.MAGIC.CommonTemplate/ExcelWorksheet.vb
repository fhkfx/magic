Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Excel Work Sheet Settings of Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class ExcelWorksheet
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curReadableWorksheetName As String
    Private _oldReadableWorksheetName As String
    Private _oriReadableWorksheetName As String
    Private _curWorksheetName As String
    Private _oldWorksheetName As String
    Private _oriWorksheetName As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This property is to Get/Set the Work sheet name that is readable
    ''' </summary>
    ''' <value>Name of Work sheet</value>
    ''' <returns>Returns Name of Work sheet</returns>
    ''' <remarks></remarks>
    Public Property ReadableWorksheetName() As String
        Get
            Return _curReadableWorksheetName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curReadableWorksheetName <> value Then
                _curReadableWorksheetName = value
                OnPropertyChanged("ReadableWorksheetName")
            End If
        End Set

    End Property
    ''' <summary>
    ''' This property is to Get/Set the Work sheet name 
    ''' </summary>
    ''' <value>Name of Work sheet</value>
    ''' <returns>Returns Name of Work sheet</returns>
    ''' <remarks></remarks>
    Public Property WorksheetName() As String
        Get
            Return _curWorksheetName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curWorksheetName <> value Then
                _curWorksheetName = value
                OnPropertyChanged("WorksheetName")
            End If
        End Set

    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Apply the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriReadableWorksheetName = _curReadableWorksheetName
        _oriWorksheetName = _curWorksheetName
    End Sub
    ''' <summary>
    ''' Begin changes
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldReadableWorksheetName = _curReadableWorksheetName
        _oldWorksheetName = _curWorksheetName
    End Sub
    ''' <summary>
    ''' Cancels the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curReadableWorksheetName = _oldReadableWorksheetName
        _curWorksheetName = _oldWorksheetName
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curReadableWorksheetName = ReadXMLElement(reader, "_curReadableWorksheetName")
            _oldReadableWorksheetName = ReadXMLElement(reader, "_oldReadableWorksheetName")
            _oriReadableWorksheetName = ReadXMLElement(reader, "_oriReadableWorksheetName")
            _curWorksheetName = ReadXMLElement(reader, "_curWorksheetName")
            _oldWorksheetName = ReadXMLElement(reader, "_oldWorksheetName")
            _oriWorksheetName = ReadXMLElement(reader, "_oriWorksheetName")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curReadableWorksheetName", _curReadableWorksheetName)
        WriteXmlElement(writer, "_oldReadableWorksheetName", _oldReadableWorksheetName)
        WriteXmlElement(writer, "_oriReadableWorksheetName", _oriReadableWorksheetName)
        WriteXmlElement(writer, "_curWorksheetName", _curWorksheetName)
        WriteXmlElement(writer, "_oldWorksheetName", _oldWorksheetName)
        WriteXmlElement(writer, "_oriWorksheetName", _oriWorksheetName)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">name of the file</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(ExcelWorksheet))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the objecdt from the given file
    ''' </summary>
    ''' <param name="filename">filename</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As ExcelWorksheet
        Dim serializer As New XmlSerializer(GetType(ExcelWorksheet))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), ExcelWorksheet)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As ExcelWorksheet = DirectCast(obj, ExcelWorksheet)
        _curReadableWorksheetName = objE._curReadableWorksheetName
        _oldReadableWorksheetName = objE._oldReadableWorksheetName
        _oriReadableWorksheetName = objE._oriReadableWorksheetName
        _curWorksheetName = objE._curWorksheetName
        _oldWorksheetName = objE._oldWorksheetName
        _oriWorksheetName = objE._oriWorksheetName
    End Sub
#End Region
End Class
''' <summary>
''' Collection of Worksheet Settings
''' </summary>
''' <remarks></remarks>
Partial Public Class ExcelWorksheetCollection
    Inherits System.ComponentModel.BindingList(Of ExcelWorksheet)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of ExcelWorksheet)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of ExcelWorksheet)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of ExcelWorksheet)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As ExcelWorksheet
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("ExcelWorksheetCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "ExcelWorksheet"
            child = New ExcelWorksheet
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As ExcelWorksheet
        writer.WriteStartElement("ExcelWorksheetCollection")
        For Each child In Me
            writer.WriteStartElement("ExcelWorksheet")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
