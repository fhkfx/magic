'Imports System.IO
'Imports System.ComponentModel
'Imports System.Windows.Forms
'Imports System.Xml.Serialization
'Imports BTMU.MAGIC.Common
'Imports System.Text.RegularExpressions

'<Serializable()> _
'Partial Public Class RowFilter
'    Inherits BTMU.MAGIC.Common.BusinessBase

'    Public Sub New()
'        Initialize()
'    End Sub

'#Region " Private Fields "

'    Private _curFilterField As String
'    Private _oldFilterField As String
'    Private _oriFilterField As String
'    Private _curFilterType As String
'    Private _oldFilterType As String
'    Private _oriFilterType As String
'    Private _curFilterOperator As String
'    Private _oldFilterOperator As String
'    Private _oriFilterOperator As String
'    Private _curFilterValue As String
'    Private _oldFilterValue As String
'    Private _oriFilterValue As String
'    Private _curCreatedBy As String
'    Private _curCreatedDate As DateTime
'    Private _curModifiedBy As String
'    Private _curModifiedDate As DateTime
'    Private _oriModifiedBy As String
'    Private _oriModifiedDate As DateTime
'#End Region
'#Region " Public Property "

'    Private Function GetFormattedValue(ByVal _value As String, ByVal _operator As String) As String

'        If _operator.Contains("LIKE") Or _operator.Contains("NOT LIKE") Or _operator.Contains("CONTAIN") Or _operator.Contains("NOT CONTAIN") Then

'            Dim strExpressionBuilder As New System.Text.StringBuilder
'            For Each c As Char In _value
'                Select Case c
'                    Case "["
'                        strExpressionBuilder.Append("[[]")
'                    Case "]"
'                        strExpressionBuilder.Append("[]]")
'                    Case "*"
'                        strExpressionBuilder.Append("[*]")
'                    Case "%"
'                        strExpressionBuilder.Append("[%]")
'                    Case "'"
'                        strExpressionBuilder.Append("''")
'                    Case Else
'                        strExpressionBuilder.Append(c)
'                End Select
'            Next


'            Return "'%" & strExpressionBuilder.ToString() & "%'"

'        End If

'        If FilterType = "String" Then Return "'" & _value.Replace("'", "''") & "'"

'        If FilterType = "Date" Then Return "#" & _value & "#"

'        Return _value

'    End Function

'    Private Function GetAppropriateOperator() As String

'        If (FilterType = "String" AndAlso (FilterOperator.ToUpper() = "CONTAIN")) Then Return "LIKE"

'        If (FilterType = "String" AndAlso (FilterOperator.ToUpper() = "NOT CONTAIN")) Then Return "NOT LIKE"

'        Return FilterOperator

'    End Function


'    Public ReadOnly Property ConditionString() As String

'        Get
'            Dim filterOperator As String = GetAppropriateOperator()


'            Select Case FilterOperator

'                Case "> AND <", "> AND <=", ">= AND <", ">= AND <="

'                    Dim values2() As String = Regex.Split(FilterValue.Trim(), Regex.Escape("||"))

'                    If values2.Length <> 2 Then Throw New System.Exception("Filter Value is not in expected format!, Expected Format: Value1||Value2")

'                    Return String.Format(" ( ({0} {1} {2}) AND ({3} {4} {5}) ) " _
'                        , HelperModule.EscapeSpecialCharsForColumnName(FilterField), filterOperator.Substring(0, 2).Trim(), GetFormattedValue(values2(0), filterOperator) _
'                        , HelperModule.EscapeSpecialCharsForColumnName(FilterField), filterOperator.Substring(6).Trim(), GetFormattedValue(values2(1), filterOperator))

'                Case Else
'                    Return String.Format(" {0} {1} {2} ", HelperModule.EscapeSpecialCharsForColumnName(FilterField), GetAppropriateOperator() _
'                    , GetFormattedValue(FilterValue, filterOperator))

'            End Select

'        End Get

'    End Property

'    Public Property FilterField() As String
'        Get
'            Return _curFilterField
'        End Get
'        Set(ByVal value As String)
'            If _curFilterField <> value Then
'                _curFilterField = value
'                OnPropertyChanged("FilterField")
'            End If
'        End Set
'    End Property
'    Public Property FilterType() As String
'        Get
'            Return _curFilterType
'        End Get
'        Set(ByVal value As String)
'            If _curFilterType <> value Then
'                _curFilterType = value
'                OnPropertyChanged("FilterType")
'            End If
'        End Set
'    End Property
'    Public Property FilterOperator() As String
'        Get
'            Return _curFilterOperator
'        End Get
'        Set(ByVal value As String)
'            If _curFilterOperator <> value Then
'                _curFilterOperator = value
'                OnPropertyChanged("FilterOperator")
'            End If
'        End Set
'    End Property
'    Public Property FilterValue() As String
'        Get
'            Return _curFilterValue
'        End Get
'        Set(ByVal value As String)
'            If _curFilterValue <> value Then
'                _curFilterValue = value
'                OnPropertyChanged("FilterValue")
'            End If
'        End Set
'    End Property
'    Public ReadOnly Property CreatedBy() As String
'        Get
'            Return _curCreatedBy
'        End Get
'    End Property
'    Public ReadOnly Property CreatedDate() As DateTime
'        Get
'            Return _curCreatedDate
'        End Get
'    End Property
'    Public ReadOnly Property ModifiedBy() As String
'        Get
'            Return _curModifiedBy
'        End Get
'    End Property
'    Public ReadOnly Property ModifiedDate() As DateTime
'        Get
'            Return _curModifiedDate
'        End Get
'    End Property
'    Public ReadOnly Property OriginalModifiedBy() As String
'        Get
'            Return _oriModifiedBy
'        End Get
'    End Property
'    Public ReadOnly Property OriginalModifiedDate() As DateTime
'        Get
'            Return _oriModifiedDate
'        End Get
'    End Property
'#End Region

'    Public Overrides Sub ApplyEdit()
'        _oriFilterField = _curFilterField
'        _oriFilterType = _curFilterType
'        _oriFilterOperator = _curFilterOperator
'        _oriFilterValue = _curFilterValue
'    End Sub

'    Public Overrides Sub BeginEdit()
'        _oldFilterField = _curFilterField
'        _oldFilterType = _curFilterType
'        _oldFilterOperator = _curFilterOperator
'        _oldFilterValue = _curFilterValue
'    End Sub

'    Public Overrides Sub CancelEdit()
'        _curFilterField = _oldFilterField
'        _curFilterType = _oldFilterType
'        _curFilterOperator = _oldFilterOperator
'        _curFilterValue = _oldFilterValue
'    End Sub

'#Region " Serialization "

'    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
'        Try
'            MyBase.ReadXml(reader)
'            _curFilterField = ReadXMLElement(reader, "_curFilterField")
'            _oldFilterField = ReadXMLElement(reader, "_oldFilterField")
'            _oriFilterField = ReadXMLElement(reader, "_oriFilterField")

'            _curFilterType = ReadXMLElement(reader, "_curFilterType")
'            _oldFilterType = ReadXMLElement(reader, "_oldFilterType")
'            _oriFilterType = ReadXMLElement(reader, "_oriFilterType")

'            _curFilterOperator = ReadXMLElement(reader, "_curFilterOperator")
'            _oldFilterOperator = ReadXMLElement(reader, "_oldFilterOperator")
'            _oriFilterOperator = ReadXMLElement(reader, "_oriFilterOperator")
'            _curFilterValue = ReadXMLElement(reader, "_curFilterValue")
'            _oldFilterValue = ReadXMLElement(reader, "_oldFilterValue")
'            _oriFilterValue = ReadXMLElement(reader, "_oriFilterValue")
'            reader.ReadEndElement()
'        Catch ex As Exception
'            MessageBox.Show(ex.Message)
'        End Try
'    End Sub

'    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
'        MyBase.WriteXml(writer)
'        WriteXmlElement(writer, "_curFilterField", _curFilterField)
'        WriteXmlElement(writer, "_oldFilterField", _oldFilterField)
'        WriteXmlElement(writer, "_oriFilterField", _oriFilterField)

'        WriteXmlElement(writer, "_curFilterType", _curFilterType)
'        WriteXmlElement(writer, "_oldFilterType", _oldFilterType)
'        WriteXmlElement(writer, "_oriFilterType", _oriFilterType)

'        WriteXmlElement(writer, "_curFilterOperator", _curFilterOperator)
'        WriteXmlElement(writer, "_oldFilterOperator", _oldFilterOperator)
'        WriteXmlElement(writer, "_oriFilterOperator", _oriFilterOperator)
'        WriteXmlElement(writer, "_curFilterValue", _curFilterValue)
'        WriteXmlElement(writer, "_oldFilterValue", _oldFilterValue)
'        WriteXmlElement(writer, "_oriFilterValue", _oriFilterValue)
'    End Sub

'#End Region

'#Region "  Save and Loading  "

'    Public Sub SaveToFile(ByVal filename As String)
'        If IsChild Then
'            Throw New Exception("Unable to save child object")
'        End If
'        Dim serializer As New XmlSerializer(GetType(RowFilter))
'        Dim stream As FileStream = File.Open(filename, FileMode.Create)
'        serializer.Serialize(stream, Me)
'        stream.Close()
'    End Sub

'    Public Sub LoadFromFile(ByVal filename As String)
'        If IsChild Then
'            Throw New Exception("Unable to load child object")
'        End If
'        Dim objE As RowFilter
'        Dim serializer As New XmlSerializer(GetType(RowFilter))
'        Dim stream As FileStream = File.Open(filename, FileMode.Open)
'        objE = DirectCast(serializer.Deserialize(stream), RowFilter)
'        stream.Close()
'        Clone(objE)
'        Validate()
'    End Sub

'    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
'        MyBase.Clone(obj)
'        Dim objE As RowFilter = DirectCast(obj, RowFilter)
'        _curFilterField = objE._curFilterField
'        _oldFilterField = objE._oldFilterField
'        _oriFilterField = objE._oriFilterField
'        _curFilterOperator = objE._curFilterOperator
'        _oldFilterOperator = objE._oldFilterOperator
'        _oriFilterOperator = objE._oriFilterOperator

'        _curFilterType = objE._curFilterType
'        _oldFilterType = objE._oldFilterType
'        _oriFilterType = objE._oriFilterType

'        _curFilterValue = objE._curFilterValue
'        _oldFilterValue = objE._oldFilterValue
'        _oriFilterValue = objE._oriFilterValue
'    End Sub

'#End Region


'End Class

'Partial Public Class RowFilterCollection
'    Inherits BusinessBaseCollection(Of RowFilter)
'    Implements System.Xml.Serialization.IXmlSerializable

'    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of RowFilter)

'    Public Sub New()
'        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of RowFilter)(Me)
'    End Sub

'    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of RowFilter)
'        Get
'            Return _bndListView
'        End Get
'    End Property

'    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
'        Return Nothing
'    End Function
'    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
'        Dim child As RowFilter
'        reader.ReadStartElement("RowFilterCollection")
'        While reader.LocalName = "RowFilter"
'            child = New RowFilter
'            DirectCast(child, IXmlSerializable).ReadXml(reader)
'            Me.Add(child)
'        End While
'        If Me.Count > 0 Then reader.ReadEndElement()
'    End Sub
'    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
'        Dim child As RowFilter
'        writer.WriteStartElement("RowFilterCollection")
'        For Each child In Me
'            writer.WriteStartElement("RowFilter")
'            DirectCast(child, IXmlSerializable).WriteXml(writer)
'            writer.WriteEndElement()
'        Next
'        writer.WriteEndElement()
'    End Sub


'    Public Function ConditionString(ByVal ConditionOperator As String) As String

'        Dim strFilterSetting As String = " 1 = 1 "
'        Dim dicFilter As New Dictionary(Of String, String)
'        Dim lstFilter As New List(Of String)

'        For Each filter As RowFilter In Items
'            If Not dicFilter.ContainsKey(filter.FilterField) Then
'                dicFilter.Add(filter.FilterField, String.Empty)
'            End If
'            If dicFilter(filter.FilterField).Length = 0 Then
'                dicFilter(filter.FilterField) = filter.ConditionString
'            Else
'                dicFilter(filter.FilterField) = dicFilter(filter.FilterField) & " OR " & filter.ConditionString
'            End If
'        Next

'        For Each temp As String In dicFilter.Keys
'            lstFilter.Add("(" & dicFilter(temp) & ")")
'        Next

'        For Each temp As String In lstFilter
'            strFilterSetting = String.Format(" {0} {1} {2} ", strFilterSetting, ConditionOperator, temp)
'        Next

'        Return strFilterSetting

'    End Function


'    Public Function IsDuplicateRowFilter(ByVal givenFilter As RowFilter) As Boolean

'        Dim frequency As Int32 = 0

'        For Each filter As RowFilter In Items

'            If givenFilter.FilterField = filter.FilterField AndAlso givenFilter.FilterType = filter.FilterType AndAlso givenFilter.FilterOperator = filter.FilterOperator AndAlso (givenFilter.FilterValue & "").Equals(filter.FilterValue, StringComparison.InvariantCultureIgnoreCase) Then frequency = frequency + 1

'            If frequency >= 2 Then Return True
'        Next

'        Return (frequency >= 2)

'    End Function

'    Public Function hasDuplicateFilterType(ByVal givenFilter As RowFilter) As Boolean

'        For Each filter As RowFilter In Items

'            If givenFilter.FilterField = filter.FilterField Then
'                If givenFilter.FilterType <> filter.FilterType Then Return True
'            End If

'        Next

'        Return False

'    End Function

'End Class


Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Row Filtering Condition
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class RowFilter
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curFilterField As String
    Private _oldFilterField As String
    Private _oriFilterField As String
    Private _curFilterType As String
    Private _oldFilterType As String
    Private _oriFilterType As String
    Private _curFilterOperator As String
    Private _oldFilterOperator As String
    Private _oriFilterOperator As String
    Private _curFilterValue As String
    Private _oldFilterValue As String
    Private _oriFilterValue As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "

    Private Function GetFormattedValue(ByVal _value As String, ByVal _operator As String, ByVal _dateString As String) As String

        If _operator.Contains("LIKE") Then

            Return "'%" & _value.Replace("*", "[*]").Replace("*", "[*]").Replace("%", "[%]").Replace("[", "[[]").Replace("]", "[]]").Replace("'", "''") & "%'"

        End If

        If FilterType = "String" Then Return "'" & _value.Replace("'", "''") & "'"

        If FilterType = "Date" Then Return "#" & _dateString & "#"

        Return _value

    End Function

    Private Function GetAppropriateOperator() As String

        If (FilterType = "String" AndAlso (FilterOperator.ToUpper() = "CONTAIN")) Then Return "LIKE"

        If (FilterType = "String" AndAlso (FilterOperator.ToUpper() = "NOT CONTAIN")) Then Return "NOT LIKE"

        Return FilterOperator

    End Function

    ''' <summary>
    ''' This property is to Get Condition String
    ''' </summary>
    ''' <value>Condition String</value>
    ''' <returns>Returns Condition String</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ConditionString() As String

        Get
            Dim filterOperator As String = GetAppropriateOperator()


            Select Case FilterOperator

                Case "> AND <", "> AND <=", ">= AND <", ">= AND <="

                    If (FilterValue Is Nothing) Then
                        Throw New System.Exception("Filter Value is not in expected format!, Expected Format: Value1||Value2")
                    Else
                        Dim values2() As String = Regex.Split(FilterValue.Trim(), Regex.Escape("||"))
                        Dim date1 As String = String.Empty
                        Dim date2 As String = String.Empty
                        If Not IsNothingOrEmptyString(_tempDateString) Then
                            Dim valuesDate2() As String = Regex.Split(_tempDateString, Regex.Escape("||"))
                            date1 = valuesDate2(0)
                            date2 = valuesDate2(1)

                        End If

                        If values2.Length <> 2 Then Throw New System.Exception("Filter Value is not in expected format!, Expected Format: Value1||Value2")

                        Return String.Format(" ( ([{0}] {1} {2}) AND ([{3}] {4} {5}) ) " _
                            , FilterField.Replace("]", "\]"), filterOperator.Substring(0, 2).Trim(), GetFormattedValue(values2(0), filterOperator, date1) _
                            , FilterField.Replace("]", "\]"), filterOperator.Substring(6).Trim(), GetFormattedValue(values2(1), filterOperator, date2))

                    End If

                Case Else
                    If FilterValue Is Nothing Then
                        If filterOperator = "=" Then
                            If FilterType.ToUpper() = "STRING" Then
                                Return String.Format(" [{0}] = '' ", FilterField.Replace("]", "\]"))
                            Else
                                Return String.Format(" [{0}] is null ", FilterField.Replace("]", "\]"))
                            End If
                        Else
                            If FilterType.ToUpper() = "STRING" Then
                                Return String.Format(" [{0}] <> '' ", FilterField.Replace("]", "\]"))
                            Else
                                Return String.Format(" [{0}] is not null ", FilterField.Replace("]", "\]"))
                            End If

                        End If
                    Else
                        If filterOperator = "<>" Then
                            Return String.Format(" [{0}] {1} {2} or [{3}] is null ", FilterField.Replace("]", "\]"), GetAppropriateOperator() _
                                                                        , GetFormattedValue(FilterValue, filterOperator, _tempDateString), FilterField.Replace("]", "\]"))
                        Else
                            Return String.Format(" [{0}] {1} {2} ", FilterField.Replace("]", "\]"), GetAppropriateOperator() _
                                                                                                    , GetFormattedValue(FilterValue, filterOperator, _tempDateString))
                        End If
                    End If

            End Select

        End Get

    End Property
    ''' <summary>
    ''' This property is to Get Filter Field
    ''' </summary>
    ''' <value>Filter Field</value>
    ''' <returns>Returns Filter Field</returns>
    ''' <remarks></remarks>
    Public Property FilterField() As String
        Get
            Return _curFilterField
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curFilterField <> value Then
                _curFilterField = value
                OnPropertyChanged("FilterField")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get Filter Type
    ''' </summary>
    ''' <value>Filter Type</value>
    ''' <returns>Returns Filter Type</returns>
    ''' <remarks></remarks>
    Public Property FilterType() As String
        Get
            Return _curFilterType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curFilterType <> value Then
                _curFilterType = value
                OnPropertyChanged("FilterType")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get Filter Operator
    ''' </summary>
    ''' <value>Filter Operator</value>
    ''' <returns>Returns Filter Operator</returns>
    ''' <remarks></remarks>
    Public Property FilterOperator() As String
        Get
            Return _curFilterOperator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curFilterOperator <> value Then
                _curFilterOperator = value
                OnPropertyChanged("FilterOperator")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get Filter Value
    ''' </summary>
    ''' <value>Filter Value</value>
    ''' <returns>Returns Filter Value</returns>
    ''' <remarks></remarks>
    Public Property FilterValue() As String
        Get
            Return _curFilterValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curFilterValue <> value Then
                _curFilterValue = value
                OnPropertyChanged("FilterValue")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Applies the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriFilterField = _curFilterField
        _oriFilterType = _curFilterType
        _oriFilterOperator = _curFilterOperator
        _oriFilterValue = _curFilterValue
    End Sub
    ''' <summary>
    ''' Begins editing this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldFilterField = _curFilterField
        _oldFilterType = _curFilterType
        _oldFilterOperator = _curFilterOperator
        _oldFilterValue = _curFilterValue
    End Sub
    ''' <summary>
    ''' Cancels the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curFilterField = _oldFilterField
        _curFilterType = _oldFilterType
        _curFilterOperator = _oldFilterOperator
        _curFilterValue = _oldFilterValue
    End Sub

#Region " Serialization "

    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curFilterField = ReadXMLElement(reader, "_curFilterField")
            _oldFilterField = ReadXMLElement(reader, "_oldFilterField")
            _oriFilterField = ReadXMLElement(reader, "_oriFilterField")

            _curFilterType = ReadXMLElement(reader, "_curFilterType")
            _oldFilterType = ReadXMLElement(reader, "_oldFilterType")
            _oriFilterType = ReadXMLElement(reader, "_oriFilterType")

            _curFilterOperator = ReadXMLElement(reader, "_curFilterOperator")
            _oldFilterOperator = ReadXMLElement(reader, "_oldFilterOperator")
            _oriFilterOperator = ReadXMLElement(reader, "_oriFilterOperator")
            _curFilterValue = ReadXMLElement(reader, "_curFilterValue")
            _oldFilterValue = ReadXMLElement(reader, "_oldFilterValue")
            _oriFilterValue = ReadXMLElement(reader, "_oriFilterValue")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curFilterField", _curFilterField)
        WriteXmlElement(writer, "_oldFilterField", _oldFilterField)
        WriteXmlElement(writer, "_oriFilterField", _oriFilterField)

        WriteXmlElement(writer, "_curFilterType", _curFilterType)
        WriteXmlElement(writer, "_oldFilterType", _oldFilterType)
        WriteXmlElement(writer, "_oriFilterType", _oriFilterType)

        WriteXmlElement(writer, "_curFilterOperator", _curFilterOperator)
        WriteXmlElement(writer, "_oldFilterOperator", _oldFilterOperator)
        WriteXmlElement(writer, "_oriFilterOperator", _oriFilterOperator)
        WriteXmlElement(writer, "_curFilterValue", _curFilterValue)
        WriteXmlElement(writer, "_oldFilterValue", _oldFilterValue)
        WriteXmlElement(writer, "_oriFilterValue", _oriFilterValue)
    End Sub

#End Region

#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(RowFilter))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the object from the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As RowFilter
        Dim serializer As New XmlSerializer(GetType(RowFilter))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), RowFilter)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub

    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As RowFilter = DirectCast(obj, RowFilter)
        _curFilterField = objE._curFilterField
        _oldFilterField = objE._oldFilterField
        _oriFilterField = objE._oriFilterField
        _curFilterOperator = objE._curFilterOperator
        _oldFilterOperator = objE._oldFilterOperator
        _oriFilterOperator = objE._oriFilterOperator

        _curFilterType = objE._curFilterType
        _oldFilterType = objE._oldFilterType
        _oriFilterType = objE._oriFilterType

        _curFilterValue = objE._curFilterValue
        _oldFilterValue = objE._oldFilterValue
        _oriFilterValue = objE._oriFilterValue
    End Sub

#End Region

    Private _tempDateString As String
    ''' <summary>
    ''' Sets a Temporary Date Value in order to initialize the Date Value, if any, in the filter codition
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub setTempDateString(ByVal value As String)
        _tempDateString = value
    End Sub
End Class

''' <summary>
''' Collection of Row Filters
''' </summary>
''' <remarks></remarks>
Partial Public Class RowFilterCollection
    Inherits BusinessBaseCollection(Of RowFilter)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of RowFilter)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of RowFilter)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of RowFilter)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml

        Dim child As RowFilter
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("RowFilterCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "RowFilter"
            child = New RowFilter
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As RowFilter
        writer.WriteStartElement("RowFilterCollection")
        For Each child In Me
            writer.WriteStartElement("RowFilter")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
    ''' <summary>
    ''' This is to Get the Combined Condition String 
    ''' </summary>
    ''' <param name="ConditionOperator">Condition Operator</param>
    ''' <returns>Returns the Combined Condition string</returns>
    ''' <remarks></remarks>
    Public Function ConditionString(ByVal ConditionOperator As String) As String
        ' Modified on : 17/02/2010
        ' Modified By : Meera
        ' The original function is commented. The function is modified for Not Contain and <> Filter Operator

        Dim strFilterSetting As String = " 1 = 1 AND "
        Dim dicFilter As New Dictionary(Of String, String)
        Dim dicANDFilter As New Dictionary(Of String, String)
        Dim dicORFilter As New Dictionary(Of String, String)
        Dim lstFilter As New List(Of String)
        Dim notEqual As String = "<>"
        Dim notContain As String = "Not Contain"

        For Each filter As RowFilter In Items
            If filter.FilterOperator = notEqual OrElse filter.FilterOperator = notContain Then
                If Not dicANDFilter.ContainsKey(filter.FilterField) Then
                    dicANDFilter.Add(filter.FilterField, String.Empty)
                End If

                If dicANDFilter(filter.FilterField).Length = 0 Then
                    dicANDFilter(filter.FilterField) = filter.ConditionString
                Else
                    dicANDFilter(filter.FilterField) = dicANDFilter(filter.FilterField) & " AND " & filter.ConditionString
                End If
            End If
        Next

        For Each filter As RowFilter In Items
            If filter.FilterOperator <> notEqual AndAlso filter.FilterOperator <> notContain Then
                If Not dicORFilter.ContainsKey(filter.FilterField) Then
                    dicORFilter.Add(filter.FilterField, String.Empty)
                End If
                If dicORFilter(filter.FilterField).Length = 0 Then
                    dicORFilter(filter.FilterField) = filter.ConditionString
                Else
                    dicORFilter(filter.FilterField) = dicORFilter(filter.FilterField) & " OR " & filter.ConditionString
                End If
            End If
        Next


        For Each ANDFilter As String In dicANDFilter.Keys
            dicFilter.Add(ANDFilter, dicANDFilter(ANDFilter))
        Next

        For Each ORFilter As String In dicORFilter.Keys
            If Not dicFilter.ContainsKey(ORFilter) Then
                dicFilter.Add(ORFilter, String.Empty)
            End If
            If dicFilter(ORFilter).Length = 0 Then
                dicFilter(ORFilter) = dicORFilter(ORFilter)
            Else
                dicFilter(ORFilter) = "(" & dicFilter(ORFilter) & ") AND (" & dicORFilter(ORFilter) & ")"
            End If
        Next

        For Each temp As String In dicFilter.Keys
            lstFilter.Add("(" & dicFilter(temp) & ")")
        Next

        Dim cnt As Integer = 0
        For Each temp As String In lstFilter
            If cnt = 0 Then
                strFilterSetting = String.Format(" {0} {1} ", strFilterSetting, temp)
            Else
                strFilterSetting = String.Format(" {0} {1} {2} ", strFilterSetting, ConditionOperator, temp)
            End If
            cnt += 1
        Next

        Return strFilterSetting

    End Function


    ' This function represent the old Logic for applying filter setting
    'Public Function ConditionString(ByVal ConditionOperator As String) As String

    '    Dim strFilterSetting As String = " 1 = 1 "
    '    Dim dicFilter As New Dictionary(Of String, String)
    '    Dim lstFilter As New List(Of String)

    '    For Each filter As RowFilter In Items
    '        If Not dicFilter.ContainsKey(filter.FilterField) Then
    '            dicFilter.Add(filter.FilterField, String.Empty)
    '        End If
    '        If dicFilter(filter.FilterField).Length = 0 Then
    '            dicFilter(filter.FilterField) = filter.ConditionString
    '        Else
    '            dicFilter(filter.FilterField) = dicFilter(filter.FilterField) & " OR " & filter.ConditionString
    '        End If
    '    Next

    '    For Each temp As String In dicFilter.Keys
    '        lstFilter.Add("(" & dicFilter(temp) & ")")
    '    Next

    '    For Each temp As String In lstFilter
    '        strFilterSetting = String.Format(" {0} {1} {2} ", strFilterSetting, ConditionOperator, temp)
    '    Next

    '    Return strFilterSetting

    'End Function

    ''' <summary>
    ''' Determines whether the row filter is a duplicate
    ''' </summary>
    ''' <param name="givenFilter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsDuplicateRowFilter(ByVal givenFilter As RowFilter) As Boolean

        Dim frequency As Int32 = 0

        For Each filter As RowFilter In Items

            If givenFilter.FilterField = filter.FilterField AndAlso givenFilter.FilterType = filter.FilterType AndAlso givenFilter.FilterOperator = filter.FilterOperator AndAlso (givenFilter.FilterValue & "").Equals(filter.FilterValue, StringComparison.InvariantCultureIgnoreCase) Then frequency = frequency + 1

            If frequency >= 2 Then Return True
        Next

        Return (frequency >= 2)

    End Function

    ''' <summary>
    ''' Determines whether the given Row Filter has any duplicates in this collection object
    ''' </summary>
    ''' <param name="givenFilter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hasDuplicateFilterType(ByVal givenFilter As RowFilter) As Boolean

        For Each filter As RowFilter In Items

            If givenFilter.FilterField = filter.FilterField Then
                If givenFilter.FilterType <> filter.FilterType Then Return True
            End If

        Next

        Return False

    End Function

End Class
