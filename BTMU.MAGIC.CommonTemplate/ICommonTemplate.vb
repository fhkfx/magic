''' <summary>
''' Abstracts the Functions of Common Transaction Template 
''' </summary>
''' <remarks></remarks>
Public Interface ICommonTemplate

    ReadOnly Property Filters() As RowFilterCollection

    ReadOnly Property MapSourceFields() As MapSourceFieldCollection

    ReadOnly Property MapBankFields() As MapBankFieldCollection

    ReadOnly Property TranslatorSettings() As TranslatorSettingCollection

    ReadOnly Property EditableSettings() As EditableSettingCollection


    ReadOnly Property LookupSettings() As LookupSettingCollection

    ReadOnly Property CalculatedFields() As CalculatedFieldCollection

    ReadOnly Property StringManipulations() As StringManipulationCollection

    Property IsAmountInDollars() As Boolean
    Property IsAmountInCents() As Boolean


    Property TransactionReferenceField1() As String
    Property TransactionReferenceField2() As String
    Property TransactionReferenceField3() As String

    Property DateSeparator() As String
    Property DateType() As String
    Property DecimalSeparator() As String
    Property ThousandSeparator() As String
    Property IsFilterConditionAND() As Boolean

    Property OutputTemplateName() As String
    Property DefinitionFile() As String
    Property IsZerosInDate() As Boolean

    Property IsAdviceRecordForEveryRow() As Boolean
    Property IsAdviceRecordAfterChar() As Boolean
    Property IsAdviceRecordDelimited() As Boolean
    Property AdviceRecordChar() As String
    Property AdviceRecordDelimiter() As String

    Sub PrepareForPreview(Optional ByVal strOutputFileFormat As String = "", Optional ByVal SourceFile As String = "", Optional ByVal WorksheetName As String = "", Optional ByVal TransactionEndRow As Integer = 0, Optional ByVal RemoveRowsFromEnd As Integer = 0)

    ReadOnly Property SourceData() As DataTable

    ReadOnly Property FilteredSourceData() As DataView

    Sub DuplicateCheck(Optional ByVal fileFormat As String = "")
    Property PreviewSourceData() As DataTable

End Interface