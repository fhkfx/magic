Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.common

''' <summary>
''' Encapsulates the Snapshot information about Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
Public Class CommonTemplateExcelList
    Inherits BTMU.MAGIC.Common.CommonListItem
#Region " Private Fields "
    Private _rowNo As Int32
    Private _commonTemplate As String
    Private _outputTemplate As String
    Private _sampleSourceFile As String
    Private _worksheetName As String
    Private _isEnabled As Boolean
    Private _isDraft As Boolean
#End Region

#Region " Public Property "
    ''' <summary>
    ''' This property is to Set/Get Record Number
    ''' </summary>
    ''' <value>Record Number</value>
    ''' <returns>Returns Record Number</returns>
    ''' <remarks></remarks>
    Public Property RowNo() As Int32
        Get
            Return _rowNo
        End Get
        Set(ByVal value As Int32)
            _rowNo = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Set/Get Name of Common Template
    ''' </summary>
    ''' <value>Common Template</value>
    ''' <returns>Returns Name of Common Template</returns>
    ''' <remarks></remarks>
    Public Property CommonTemplate() As String
        Get
            Return _commonTemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _commonTemplate = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Output Template
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template name</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplate() As String
        Get
            Return _outputTemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _outputTemplate = value

        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Source File
    ''' </summary>
    ''' <value>Source File</value>
    ''' <returns>Returns  Source File</returns>
    ''' <remarks></remarks>
    Public Property SourceFile() As String
        Get
            Return _sampleSourceFile
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _sampleSourceFile = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set WorksheetName
    ''' </summary>
    ''' <value>WorksheetName</value>
    ''' <returns>Returns  WorksheetName</returns>
    ''' <remarks></remarks>
    Public Property WorksheetName() As String
        Get
            Return _worksheetName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _worksheetName = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is enabled
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is enabled</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _isEnabled
        End Get
        Set(ByVal value As Boolean)
            _isEnabled = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is a draft
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is a draft</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _isDraft
        End Get
        Set(ByVal value As Boolean)
            _isDraft = value
        End Set
    End Property
#End Region
End Class
''' <summary>
''' Collection of Snapshot of Common Transaction Template for Excel
''' </summary>
''' <remarks></remarks>
Partial Public Class CommonTemplateExcelListCollection
    Inherits System.ComponentModel.BindingList(Of CommonTemplateExcelList)

    Private _bndListView As BTMU.MAGIC.Common.BindingListViewCommon(Of CommonTemplateExcelList)



    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListViewCommon(Of CommonTemplateExcelList)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListViewCommon(Of CommonTemplateExcelList)
        Get
            Return _bndListView
        End Get
    End Property

End Class
