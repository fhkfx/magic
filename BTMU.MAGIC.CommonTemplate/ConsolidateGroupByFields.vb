Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the ConsolidateGroupByFields Bank Field Settings
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class ConsolidateGroupByFields
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curBankFieldName As String
    Private _oldBankFieldName As String
    Private _oriBankFieldName As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This Property is to Get/Set the Bank Field Name
    ''' </summary>
    ''' <value>Bank Field Name</value>
    ''' <returns>returns Bank Field Name</returns>
    ''' <remarks></remarks>
    Public Property BankFieldName() As String
        Get
            Return _curBankFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankFieldName <> value Then
                _curBankFieldName = value
                OnPropertyChanged("BankFieldName")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Apply the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriBankFieldName = _curBankFieldName
    End Sub
    ''' <summary>
    ''' Begin changes
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldBankFieldName = _curBankFieldName
    End Sub
    ''' <summary>
    ''' Cancels the Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curBankFieldName = _oldBankFieldName
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankFieldName = ReadXMLElement(reader, "_curBankFieldName")
            _oldBankFieldName = ReadXMLElement(reader, "_oldBankFieldName")
            _oriBankFieldName = ReadXMLElement(reader, "_oriBankFieldName")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankFieldName", _curBankFieldName)
        WriteXmlElement(writer, "_oldBankFieldName", _oldBankFieldName)
        WriteXmlElement(writer, "_oriBankFieldName", _oriBankFieldName)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the object to the given file
    ''' </summary>
    ''' <param name="filename">name of the file</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(ConsolidateGroupByFields))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the objecdt from the given file
    ''' </summary>
    ''' <param name="filename">filename</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As ConsolidateGroupByFields
        Dim serializer As New XmlSerializer(GetType(ConsolidateGroupByFields))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), ConsolidateGroupByFields)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As ConsolidateGroupByFields = DirectCast(obj, ConsolidateGroupByFields)
        _curBankFieldName = objE._curBankFieldName
        _oldBankFieldName = objE._oldBankFieldName
        _oriBankFieldName = objE._oriBankFieldName
    End Sub
#End Region
End Class
''' <summary>
''' Collection of ConsolidateGroupByFields Settings
''' </summary>
''' <remarks></remarks>
Partial Public Class ConsolidateGroupByFieldsCollection
    Inherits BusinessBaseCollection(Of ConsolidateGroupByFields)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of ConsolidateGroupByFields)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of ConsolidateGroupByFields)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of ConsolidateGroupByFields)
        Get
            Return _bndListView
        End Get
    End Property
    ''' <summary>
    ''' Determines whether there exists a duplicate ConsolidateGroupByFieldsin the collection
    ''' </summary>
    ''' <param name="groupbyFieldName">Name of Bank Field</param>
    ''' <returns>boolean value indicating whether there exists a duplicate ConsolidateGroupByFields </returns>
    ''' <remarks></remarks>
    Public Function IsFound(ByVal groupbyFieldName As String) As Boolean

        For Each groupbyField As ConsolidateGroupByFields In Items
            If (groupbyField.BankFieldName = groupbyFieldName) Then Return True
        Next

        Return False

    End Function


    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As ConsolidateGroupByFields
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("ConsolidateGroupByFieldsCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "ConsolidateGroupByFields"
            child = New ConsolidateGroupByFields
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As ConsolidateGroupByFields
        writer.WriteStartElement("ConsolidateGroupByFieldsCollection")
        For Each child In Me
            writer.WriteStartElement("ConsolidateGroupByFields")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
