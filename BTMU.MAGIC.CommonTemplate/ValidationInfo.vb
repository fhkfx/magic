Imports System
Imports System.IO
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common
Imports System.Text.RegularExpressions

 

Partial Public Class CommonTemplateTextDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("OutputTemplateName", "Output Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("CommonTemplateName", "Common Template Name"))

        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("CommonTemplateName", "[.,;!@#$%^&*+=/\?'""|:<>`~]+?", "Common Template Name", MsgReader.GetString("E02000030"), True)) '"Please do not input the following characters for Common Template Name (. , ; !  @ # $ % ^ & * + = / \ ? ' "" | : < > ` ~)"

        ValidationEngine.AddValidationRule(AddressOf E01010010, New Common.ValidationArgs("CommonTemplateName", "Common Template Name"))

        ValidationEngine.AddValidationRule(AddressOf ValidateMapping, New Common.ValidationArgs("MapBankFields", "Bank Field"))
        ValidationEngine.Validate()
    End Sub
    ''' <summary>
    ''' Validates the name of Template against special characters
    ''' </summary>
    ''' <param name="target">target template</param>
    ''' <param name="e">Validation event argument</param>
    ''' <returns>Returns a boolean value indicating whether the validation is successful</returns>
    ''' <remarks></remarks>
    Public Function E01010010(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

    Function ValidateMapping(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean

        'There shall exist source fields and bank fields
        If MapBankFields.Count = 0 Or MapSourceFields.Count = 0 Then
            e.Description = MsgReader.GetString("E02000320")  '"'Source Field' or 'Bank Field' is empty, please separate the data first."
            Return False
        End If

        Return True

    End Function

End Class

'Partial Public Class SwiftTemplate
'    Inherits BTMU.MAGIC.Common.BusinessBase

'    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

'    Protected Overrides Sub Initialize()
'        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("OutputTemplateName", "Output Template Name"))
'        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SwiftTemplateName", "Swift Template Name"))
'        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("SwiftTemplateName", "[.,;!@#$%^&*+=/\?'""|:<>`~]+?", "SWIFT Template Name", MsgReader.GetString("E02000030"), True)) '"Please do not input the following characters for Common Template Name (. , ; !  @ # $ % ^ & * + = / \ ? ' "" | : < > ` ~)"
'        ValidationEngine.AddValidationRule(AddressOf E01010010, New Common.ValidationArgs("SwiftTemplateName", "Swift Template Name"))
'        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SourceFileName", "SWIFT File Name"))
'        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))
'        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DateType", "Date Type"))
'        ValidationEngine.AddValidationRule(AddressOf ValidateMapping, New Common.ValidationArgs("MapBankFields", "Bank Field"))
'        ValidationEngine.Validate()
'    End Sub

'    Public Function E01010010(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
'        Try
'            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
'            Dim isFound As Integer
'            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
'            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
'                e.Description = _
'                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
'                Return False
'            Else
'                Return True
'            End If
'        Catch ex As Exception
'            Return True
'        End Try

'    End Function

'    Function ValidateMapping(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean

'        'There shall exist source fields and bank fields
'        If MapBankFields.Count = 0 Or MapSourceFields.Count = 0 Then
'            e.Description = MsgReader.GetString("E02000320")  '"'Source Field' or 'Bank Field' is empty, please separate the data first."
'            Return False
'        End If

'        Return True

'    End Function

'End Class
Partial Public Class SwiftTemplate
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("OutputTemplateName", "Output Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SwiftTemplateName", "Swift Template Name"))
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("SwiftTemplateName", "[.,;!@#$%^&*+=/\?'""|:<>`~]+?", "SWIFT Template Name", MsgReader.GetString("E02000030"), True)) '"Please do not input the following characters for Common Template Name (. , ; !  @ # $ % ^ & * + = / \ ? ' "" | : < > ` ~)"
        ValidationEngine.AddValidationRule(AddressOf E01010010, New Common.ValidationArgs("SwiftTemplateName", "Swift Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SourceFileName", "SWIFT File Name"))
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DateType", "Date Type"))
        ValidationEngine.AddValidationRule(AddressOf ValidateMapping, New Common.ValidationArgs("MapBankFields", "Bank Field"))
        ValidationEngine.Validate()
    End Sub
    ''' <summary>
    ''' Validates the name of Template against special characters
    ''' </summary>
    ''' <param name="target">target template</param>
    ''' <param name="e">Validation event argument</param>
    ''' <returns>Returns a boolean value indicating whether the validation is successful</returns>
    ''' <remarks></remarks>
    Public Function E01010010(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

    Function ValidateMapping(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean

        'There shall exist source fields and bank fields
        If MapBankFields.Count = 0 Or MapSourceFields.Count = 0 Then
            e.Description = MsgReader.GetString("E02000320")  '"'Source Field' or 'Bank Field' is empty, please separate the data first."
            Return False
        End If

        Return True

    End Function

End Class


 

Partial Public Class CommonTemplateExcelDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("OutputTemplate", "Master Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("CommonTemplateName", "Common Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("CommonTemplateName", "[.,;!@#$%^&*+=/\?'""|:<>`~]+?", "Common Template Name", MsgReader.GetString("E02000030"), True)) '"Please do not input the following characters for Common Template Name (. , ; !  @ # $ % ^ & * + = / \ ? ' "" | : < > ` ~)"
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SampleSourceFile", "Source File Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("WorksheetName", "Worksheet Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("StartColumn", "Start Column"))
        ValidationEngine.AddValidationRule(AddressOf ValidateStartColumn, New Common.ValidationArgs("StartColumn", "Start Column"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("EndColumn", "End Column"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SampleRowNo", "Sample Row"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("HeaderAtRow", "Header At Row"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("TransactionStartingRow", "Transaction Start Row"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("ThousandSeparator", "Thousand Separator"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DateType", "Date Type"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DateSeparator", "Date Separator"))
        ValidationEngine.AddValidationRule(AddressOf AdviceChar, New Common.ValidationArgs("AdviceRecChar", "No. of Characters per Advice Record"))
        ValidationEngine.AddValidationRule(AddressOf AdviceDelimiter, New Common.ValidationArgs("AdviceRecDelimiter", "Delimiter"))
        ValidationEngine.AddValidationRule(AddressOf TransactionStartRow, New Common.ValidationArgs("TransactionStartingRow", "Transaction Start Row"))
        ValidationEngine.AddValidationRule(AddressOf SampleRow, New Common.ValidationArgs("SampleRowNo", "Sample Row"))
        ValidationEngine.AddValidationRule(AddressOf DecimalSep, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))
        ValidationEngine.AddValidationRule(AddressOf ValidateMapping, New Common.ValidationArgs("MapBankFields", "Bank Field"))
        ValidationEngine.Validate()
    End Sub
    ''' <summary>
    ''' ' Start Column should be less than End Column. Please fill in the Start Column again.
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ValidateStartColumn(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        If IsNothingOrEmptyString(Me.StartColumn) Or IsNothingOrEmptyString(Me.EndColumn) Then Return True
        If GetColumnIndex(Me.StartColumn) > GetColumnIndex(Me.EndColumn) Then
            e.Description = MsgReader.GetString("E02010010")
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' Validation - Decimal Separator and Thousand Separator must be different
    ''' </summary>
    ''' <param name="target"></param>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function DecimalSep(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        If IsNothingOrEmptyString(Me.DecimalSeparator) Or IsNothingOrEmptyString(Me.ThousandSeparator) Then Return True

        If (Me.DecimalSeparator.Trim.Length = 1 And Me.ThousandSeparator.Trim.Length = 1) And (Me.DecimalSeparator.Trim = Me.ThousandSeparator.Trim) Then
            e.Description = MsgReader.GetString("E02000070")
            Return False
        End If

        Return True

    End Function


    Function SampleRow(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Not IsNothingOrEmptyString(value) Then
            If Convert.ToInt32(value) < Convert.ToInt32(Me.TransactionStartingRow) Then
                e.Description = MsgReader.GetString("E02000040")
                Return False
            End If
        End If
        Return True

    End Function
    Function TransactionStartRow(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Not IsNothingOrEmptyString(value) Then
            If Convert.ToInt32(value) < Convert.ToInt32(Me.HeaderAtRow) Then
                e.Description = MsgReader.GetString("E02000160")
                Return False
            End If
        End If
        Return True

    End Function
    Function AdviceChar(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Me.IsAdviceRecAfterEveryChar And IsNothingOrEmptyString(value) Then
            e.Description = String.Format(MsgReader.GetString("E02000270"), ValidationArgs.GetFriendlyPropertyName(e))
            Return False
        End If
        Return True

    End Function
    Function AdviceDelimiter(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean
        Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Me.IsAdviceRecDelimiter And IsNothingOrEmptyString(value) Then
            e.Description = String.Format(MsgReader.GetString("E02000280"), ValidationArgs.GetFriendlyPropertyName(e))
            Return False
        End If
        Return True
    End Function
    Function ValidateMapping(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean

        'There shall exist source fields and bank fields
        If MapBankFields.Count = 0 Or MapSourceFields.Count = 0 Then
            e.Description = MsgReader.GetString("E02000320")  '"'Source Field' or 'Bank Field' is empty, please separate the data first."
            Return False
        End If

        Return True

    End Function

End Class


 

Partial Public Class RowFilter
    Inherits BTMU.MAGIC.Common.BusinessBase

    Protected Overrides Sub Initialize()
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FilterField", "Filter Field"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FilterOperator", "Filter Operator"))
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FilterValue", "Filter Value"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FilterType", "Data Type"))
        'ValidationEngine.AddValidationRule(AddressOf ValidateFilterValue, New Common.ValidationArgs("FilterValue", "Filter Value"))
        ValidationEngine.AddValidationRule(AddressOf ValidateFilterValueStringRequired, New Common.ValidationArgs("FilterValue", "Filter Value"))
        ValidationEngine.Validate()
    End Sub

    Function ValidateFilterValueStringRequired(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If FilterOperator <> "=" And FilterOperator <> "<>" Then
            If FilterValue Is Nothing Then
                args.Description = "Please fill in 'Filter Value' field."
                Return False
            Else
                Return True
            End If
        End If
        Return True
    End Function

End Class

 

Partial Public Class RowFilterCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

End Class


 

Partial Public Class MapBankField
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()

        ValidationEngine.AddValidationRule(AddressOf MapExists, New Common.ValidationArgs("BankFieldExt", "Bank Field"))
        ValidationEngine.Validate()

    End Sub


    Function MapExists(ByVal target As Object, ByVal e As Common.ValidationArgs) As Boolean

        'There shall exist source fields and bank fields
        If Mandatory AndAlso MappedSourceFields.Count = 0 AndAlso (Not IsTranslated(BankField)) Then
            e.Description = String.Format(MsgReader.GetString("E02000410"), BankField)
            Return False
        End If

        Return True

    End Function

    Function IsTranslated(ByVal _BankField As String) As Boolean

        Dim _DataModel As ICommonTemplate

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.MapBankFields Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.MapBankFields Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.MapBankFields Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If


        For Each _transltdField As TranslatorSetting In _DataModel.TranslatorSettings
            If _transltdField.BankFieldName = _BankField Then
                Return True
            End If
        Next

        Return False

    End Function

End Class



 

Partial Public Class TranslatorSetting
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub OnPropertyChanged(ByVal propertyName As String)

        MyBase.OnPropertyChanged(propertyName)

        If propertyName = "BankValue" Then
            Validate("BankValueEmpty")
            Validate("CustomerValue")
        ElseIf propertyName = "BankValueEmpty" Then
            Validate("BankValue")
        ElseIf propertyName = "CustomerValue" Then
            Validate("BankValue")
        End If

    End Sub


    Protected Overrides Sub Initialize()

        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BankFieldName", "Bank Field"))

        'ValidationEngine.AddValidationRule(AddressOf ValidateMappedField, New Common.ValidationArgs("BankFieldName", "Bank Field"))

        ValidationEngine.AddValidationRule(AddressOf ValidateCustomerField, New Common.ValidationArgs("CustomerValue", "Customer Value"))

        'ValidationEngine.AddValidationRule(AddressOf ValidateCustomerField, New Common.ValidationArgs("BankValue", "Bank Value"))

        ' ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateBankField, New Common.ValidationArgs("BankFieldName", "Bank Field"))

        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateBankField, New Common.ValidationArgs("CustomerValue", "Customer Value"))


        ValidationEngine.AddValidationRule(AddressOf ValidateEmptyBankField, New Common.ValidationArgs("BankValue", "Bank Value"))

        ValidationEngine.AddValidationRule(AddressOf ValidateEmptyBankField, New Common.ValidationArgs("BankValueEmpty", "Bank Value Empty"))

        ValidationEngine.Validate()

    End Sub

     

    Function ValidateEmptyBankField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        If BankValueEmpty Then

            If HelperModule.IsNothingOrEmptyString(BankValue) Then
                Return True
            Else
                'args.Description = "Bank Value must be empty. Please Clear Bank Value or Uncheck the 'BankValueEmpty' Checkbox."
                args.Description = MsgReader.GetString("E10000008")

                Return False
            End If

        Else

            If HelperModule.IsNothingOrEmptyString(BankValue) Then
                'Please fill in Bank Value or Check the 'BankValueEmpty' Checkbox.
                args.Description = MsgReader.GetString("E10000009")
                Return False
            End If

        End If

        Return True

    End Function



    Function ValidateCustomerField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        '#1. Translated bank fields shall either have customer value or bank value

        'If CustomerValue Is Nothing AndAlso BankValue Is Nothing Then

        '    args.Description = "Please fill in Source Value or Bank Value."
        '    Return False

        'ElseIf CustomerValue Is Nothing AndAlso (Not (BankValue Is Nothing)) Then

        '    If BankValue.Trim() = String.Empty Then

        '        args.Description = "Please fill in Source Value or Bank Value."
        '        Return False

        '    End If

        'ElseIf (Not (CustomerValue Is Nothing)) AndAlso BankValue Is Nothing Then

        '    If CustomerValue.Trim() = String.Empty Then
        '        args.Description = "Please fill in Source Value or Bank Value."
        '        Return False

        '    End If

        'ElseIf CustomerValue.Trim() = String.Empty AndAlso BankValue.Trim() = String.Empty Then

        '    args.Description = "Please fill in Source Value or Bank Value."
        '    Return False

        '    '#2.Customer value shall be blank for unmapped bank fields
        'Else
        If (Not IsMappedBankField(BankFieldName)) AndAlso (Not HelperModule.IsNothingOrEmptyString(CustomerValue)) Then

            'Unmapped Bank Field must have Blank Source Value.
            args.Description = MsgReader.GetString("E10000010")
            Return False

        End If

        Return True



    End Function


    Function IsMappedBankField(ByVal _BankFieldName As String) As Boolean

        Dim _DataModel As ICommonTemplate

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _bankField As MapBankField In _DataModel.MapBankFields
            If _bankField.BankField = _BankFieldName AndAlso _bankField.MappedSourceFields.Count > 0 Then
                Return True
            End If
        Next

        Return False

    End Function

    Function ValidateDuplicateBankField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0
        Dim _DataModel As ICommonTemplate

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.TranslatorSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If


        For Each _objItem As TranslatorSetting In _DataModel.TranslatorSettings
            If BankFieldName = _objItem.BankFieldName _
                AndAlso CustomerValue = _objItem.CustomerValue _
            Then
                frequency = frequency + 1
                If (frequency > 1) Then
                    'Please remove the duplicate data in 'Translator Setting' grid.
                    args.Description = MsgReader.GetString("E10000011")
                    Return False
                End If
            End If
        Next

        Return True

    End Function

End Class


Partial Public Class TranslatorSettingCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Dim _DataModel As ICommonTemplate
        Dim deletedItem As TranslatorSetting

        deletedItem = Me(index)

        MyBase.RemoveItem(index)


        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.TranslatorSettings Is Me Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.TranslatorSettings Is Me Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.TranslatorSettings Is Me Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Sub
        End If

        For Each _objItem As MapBankField In _DataModel.MapBankFields
            If _objItem.BankField = deletedItem.BankFieldName Then
                _objItem.Validate("BankFieldExt")
            End If
        Next

    End Sub
End Class



 

Partial Public Class EditableSetting
    Inherits BTMU.MAGIC.Common.BusinessBase
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateEditableBankField, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.Validate()
    End Sub
    Function ValidateDuplicateEditableBankField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0
        Dim _DataModel As ICommonTemplate


        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.EditableSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.EditableSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.EditableSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _objItem As BTMU.MAGIC.CommonTemplate.EditableSetting In _DataModel.EditableSettings
            If BankFieldName = _objItem.BankFieldName Then
                frequency = frequency + 1
                If (frequency > 1) Then
                    'Please remove the duplicate data in 'Editable Setting' grid.
                    args.Description = MsgReader.GetString("E10000012")
                    Return False
                End If
            End If
        Next

        Return True

    End Function

End Class

Partial Public Class EditableSettingCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

End Class


 

Partial Public Class LookupSetting
    Inherits BTMU.MAGIC.Common.BusinessBase
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()

        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("TableName", "Table Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("SourceFieldName", "Source Field"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("LookupKey", "Lookup Key"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("LookupValue", "Lookup Value"))
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateLookupBankField, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateLookupBankField2, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateLookupBankField3, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf ValidateLookupKey2, New Common.ValidationArgs("LookupKey2", "Lookup Key2"))
        ValidationEngine.AddValidationRule(AddressOf ValidateLookupValue2, New Common.ValidationArgs("LookupValue2", "Lookup Value2"))
        ValidationEngine.AddValidationRule(AddressOf ValidateLookupKey3, New Common.ValidationArgs("LookupKey3", "Lookup Key3"))
        ValidationEngine.AddValidationRule(AddressOf ValidateLookupValue3, New Common.ValidationArgs("LookupValue3", "Lookup Value3"))

        ValidationEngine.Validate()
    End Sub

    Function ValidateLookupValue3(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If Not IsNothingOrEmptyString(TableName3) Then
            If IsNothingOrEmptyString(LookupValue3) Then
                args.Description = "Please fill in 'Lookup Value3' field."
                Return False
            End If
        End If
        Return True
    End Function
    Function ValidateLookupKey3(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If Not IsNothingOrEmptyString(TableName3) Then
            If IsNothingOrEmptyString(LookupKey3) Then
                args.Description = "Please fill in 'Lookup Key3' field."
                Return False
            End If
        End If
        Return True
    End Function
    Function ValidateLookupValue2(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If Not IsNothingOrEmptyString(TableName2) Then
            If IsNothingOrEmptyString(LookupValue2) Then
                args.Description = "Please fill in 'Lookup Value2' field."
                Return False
            End If
        End If
        Return True
    End Function
    Function ValidateLookupKey2(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If Not IsNothingOrEmptyString(TableName2) Then
            If IsNothingOrEmptyString(LookupKey2) Then
                args.Description = "Please fill in 'Lookup Key2' field."
                Return False
            End If
        End If
        Return True
    End Function

    Function ValidateDuplicateLookupBankField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0
        Dim _DataModel As ICommonTemplate


        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If


        For Each _objItem As LookupSetting In _DataModel.LookupSettings
            If _objItem.LookupKey <> String.Empty AndAlso _objItem.LookupValue <> String.Empty AndAlso _objItem.LookupKey = _objItem.LookupValue Then
                ''Lookup Key' and 'Lookup Value' shall be different.
                args.Description = MsgReader.GetString("E10000013")
                Return False
            End If
            If BankFieldName = _objItem.BankFieldName Then
                frequency = frequency + 1
                If (frequency > 1) Then
                    'Please remove the duplicate data in 'Lookup Setting' grid.
                    args.Description = MsgReader.GetString("E10000014")
                    Return False
                End If
            End If
        Next

        Return True

    End Function
    Function ValidateDuplicateLookupBankField2(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim _DataModel As ICommonTemplate

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _objItem As LookupSetting In _DataModel.LookupSettings

            If _objItem.LookupKey2 <> String.Empty AndAlso _objItem.LookupValue2 <> String.Empty AndAlso _objItem.LookupKey2 = _objItem.LookupValue2 Then
                ''Lookup Key' and 'Lookup Value' shall be different.
                args.Description = "'Lookup Key2' and 'Lookup Value2' shall be different."
                Return False
            End If
        Next

        Return True

    End Function
    Function ValidateDuplicateLookupBankField3(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim _DataModel As ICommonTemplate

        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.LookupSettings Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _objItem As LookupSetting In _DataModel.LookupSettings
            If _objItem.LookupKey3 <> String.Empty AndAlso _objItem.LookupValue3 <> String.Empty AndAlso _objItem.LookupKey3 = _objItem.LookupValue3 Then
                ''Lookup Key' and 'Lookup Value' shall be different.
                args.Description = "'Lookup Key3' and 'Lookup Value3' shall be different."
                Return False
            End If
        Next

        Return True

    End Function

End Class

Partial Public Class LookupSettingCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

End Class

 

Partial Public Class CalculatedField
    Inherits BTMU.MAGIC.Common.BusinessBase
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()

        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Operand1", "Operand 1"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Operator1", "Operator 1"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Operand2", "Operand 2"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Operator2", "Operator 2"))
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateCalculationBankField, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf ValidateOperator2, New Common.ValidationArgs("Operator2", "Operand 3"))
        ValidationEngine.Validate()
    End Sub

    Function ValidateDuplicateCalculationBankField(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0
        Dim _DataModel As ICommonTemplate



        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.CalculatedFields Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.CalculatedFields Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.CalculatedFields Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _objItem As CalculatedField In _DataModel.CalculatedFields
            If BankFieldName = _objItem.BankFieldName Then '_
                'AndAlso Operand1 = _objItem.Operand1 _
                'AndAlso Operator1 = _objItem.Operator1 _
                'AndAlso Operand2 = _objItem.Operand2 _
                'AndAlso Operator2 = _objItem.Operator2 _
                'AndAlso Operand3 = _objItem.Operand3 _
                'Then
                frequency = frequency + 1
                If (frequency > 1) Then
                    'Please remove the duplicate entry from 'Calculation' grid.
                    args.Description = MsgReader.GetString("E10000015")
                    Return False
                End If
            End If
        Next


        Return True


    End Function

    Function ValidateOperator2(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0

        If Operand3 = String.Empty AndAlso Operator2 <> "None" Then
            'Operand 3 cannot be empty while Operator 2 is not 'None'
            args.Description = MsgReader.GetString("E10000016")
            Return False
        End If

        If Operand3 <> String.Empty AndAlso Operator2 = "None" Then
            'Operand 3 cannot have a value while Operator 2 is  'None' 
            args.Description = MsgReader.GetString("E10000017")
            Return False
        End If

        Return True

    End Function

End Class

Partial Public Class CalculatedFieldCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

End Class

 

Partial Public Class StringManipulation
    Inherits BTMU.MAGIC.Common.BusinessBase
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()

        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FunctionName", "Manipulation Function"))
        ValidationEngine.AddValidationRule(AddressOf ValidateStartPosition, New Common.ValidationArgs("StartPosition", "Start Position"))
        'ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("NumberOfCharacters", "Number of Characters"))
        ValidationEngine.AddValidationRule(AddressOf ValidateDuplicateStringManipEntry, New Common.ValidationArgs("BankFieldName", "Bank Field"))
        ValidationEngine.AddValidationRule(AddressOf ValidateNoOfCharacters, New Common.ValidationArgs("NumberOfCharacters", "No.Of Characters"))
        ValidationEngine.AddValidationRule(AddressOf ValidateNoOfCharactersStringRequired, New Common.ValidationArgs("NumberOfCharacters", "Number of Characters"))
        ValidationEngine.Validate()
    End Sub

    Function ValidateNoOfCharactersStringRequired(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If FunctionName <> "TRIM" Then
            If NumberOfCharacters.HasValue Then
                Return True
            Else
                args.Description = "Please fill in 'Number of Characters' field."
                Return False
            End If
        End If
        Return True
    End Function

    Function ValidateNoOfCharacters(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        If FunctionName <> "TRIM" Then
            If NumberOfCharacters.HasValue Then
                If NumberOfCharacters.Value <= 0 Then
                    'String Manipulation function requires a value more than '0' for 'No.Of Characters'
                    args.Description = MsgReader.GetString("E10000018")
                    Return False
                End If
            End If
        End If
        Return True
    End Function


    Function ValidateStartPosition(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean
        Try
            ''''''''''''''''''''''''''''''''Modified by Jacky'''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''original code'''''''''''''''''''''''
            If (FunctionName = "LEFT" Or FunctionName = "RIGHT" Or FunctionName = "REMOVE LAST" Or FunctionName = "TRIM") _
                  AndAlso (StartingPosition.HasValue) Then
                'String Manipulation function does not require 'Start Position'
                args.Description = MsgReader.GetString("E10000019")
                Return False
            End If

            If (FunctionName = "REMOVE" Or FunctionName = "SUBSTRING") Then
                If (StartingPosition.HasValue) Then
                    If StartingPosition.Value <= 0 Then
                        'String Manipulation function requires a value more than '0' for 'Start Position'
                        args.Description = MsgReader.GetString("E10000020")
                        Return False
                    End If
                Else
                    ''Start Position' is required
                    args.Description = MsgReader.GetString("E10000021")
                    Return False
                End If
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            BTMU.MAGIC.Common.BTMUExceptionManager.LogAndShowError("Error", ex.StackTrace())
        End Try
        Return True
    End Function

    Function ValidateDuplicateStringManipEntry(ByVal target As Object, ByVal args As Common.ValidationArgs) As Boolean

        Dim frequency As Int32 = 0
        Dim _DataModel As ICommonTemplate


        If Not IsNothing(CommonTemplateTextDetail.DataContainer) AndAlso CommonTemplateTextDetail.DataContainer.StringManipulations Is Me.MyCollection Then
            _DataModel = CommonTemplateTextDetail.DataContainer
        ElseIf Not IsNothing(CommonTemplateExcelDetail.DataContainer) AndAlso CommonTemplateExcelDetail.DataContainer.StringManipulations Is Me.MyCollection Then
            _DataModel = CommonTemplateExcelDetail.DataContainer
        ElseIf Not IsNothing(SwiftTemplate.DataContainer) AndAlso SwiftTemplate.DataContainer.StringManipulations Is Me.MyCollection Then
            _DataModel = SwiftTemplate.DataContainer
        Else
            Exit Function
        End If

        For Each _objItem As StringManipulation In _DataModel.StringManipulations
            If BankFieldName = _objItem.BankFieldName Then
                frequency = frequency + 1
                If (frequency > 1) Then
                    'Please remove the duplicate entry from 'String Manipulation' grid.
                    args.Description = MsgReader.GetString("E10000022")
                    Return False
                End If
            End If
        Next



        Return True

    End Function

End Class

Partial Public Class StringManipulationCollection
    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)

    End Sub

End Class
