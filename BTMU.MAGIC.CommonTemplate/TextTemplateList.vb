Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Snapshot information about Common Transaction Template for Text
''' </summary>
''' <remarks></remarks>
Public Class TextTemplateList
    Inherits BTMU.MAGIC.Common.CommonListItem


#Region " Private Fields "
    Private _rowno As Int32
    Private _commontemplate As String
    Private _outputtemplate As String
    Private _sourcefile As String
    Private _isenabled As Boolean
    Private _isdraft As Boolean
#End Region

#Region " Public Property "
    ''' <summary>
    ''' This property is to Set/Get Record Number
    ''' </summary>
    ''' <value>Record Number</value>
    ''' <returns>Returns Record Number</returns>
    ''' <remarks></remarks>
    Public Property RowNo() As Int32
        Get
            Return _rowno
        End Get
        Set(ByVal value As Int32)
            _rowno = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Set/Get Name of Common Template
    ''' </summary>
    ''' <value>Common Template</value>
    ''' <returns>Returns Name of Common Template</returns>
    ''' <remarks></remarks>
    Public Property CommonTemplate() As String
        Get
            Return _commontemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _commontemplate = value
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Output Template
    ''' </summary>
    ''' <value>Output Template</value>
    ''' <returns>Returns Output Template name</returns>
    ''' <remarks></remarks>
    Public Property OutputTemplate() As String
        Get
            Return _outputtemplate
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _outputtemplate = value

        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Source File
    ''' </summary>
    ''' <value>Source File</value>
    ''' <returns>Returns  Source File</returns>
    ''' <remarks></remarks>
    Public Property SourceFile() As String
        Get
            Return _sourcefile
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            _sourcefile = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is enabled
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is enabled</returns>
    ''' <remarks></remarks>
    Public Property IsEnabled() As Boolean
        Get
            Return _isenabled
        End Get
        Set(ByVal value As Boolean)
            _isenabled = value
        End Set
    End Property
    ''' <summary>
    '''Determines whether the template is a draft
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether the template is a draft</returns>
    ''' <remarks></remarks>
    Public Property IsDraft() As Boolean
        Get
            Return _isdraft
        End Get
        Set(ByVal value As Boolean)
            _isdraft = value
        End Set
    End Property
#End Region

End Class
''' <summary>
''' Collection of Snapshot of Common Transaction Template for Text
''' </summary>
''' <remarks></remarks>
Partial Public Class TextTemplateListCollection
    Inherits System.ComponentModel.BindingList(Of TextTemplateList)

    Private _bndListView As BTMU.MAGIC.Common.BindingListViewCommon(Of TextTemplateList)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListViewCommon(Of TextTemplateList)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListViewCommon(Of TextTemplateList)
        Get
            Return _bndListView
        End Get
    End Property
End Class

