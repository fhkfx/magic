Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates String Manipulation Settings
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class StringManipulation
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()

    End Sub

#Region " Private Fields "

    Private _curBankFieldName As String
    Private _oldBankFieldName As String
    Private _oriBankFieldName As String
    Private _curFunctionName As String
    Private _oldFunctionName As String
    Private _oriFunctionName As String
    Private _curStartingPosition As Nullable(Of Integer)
    Private _oldStartingPosition As Nullable(Of Integer)
    Private _oriStartingPosition As Nullable(Of Integer)
    Private _curNumberOfCharacters As Nullable(Of Integer)
    Private _oldNumberOfCharacters As Nullable(Of Integer)
    Private _oriNumberOfCharacters As Nullable(Of Integer)
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    ''' <summary>
    ''' This property is to Get/Set Bank Field Name
    ''' </summary>
    ''' <value>Bank Field Name</value>
    ''' <returns>Returns Bank Field Name</returns>
    ''' <remarks></remarks>
    Public Property BankFieldName() As String
        Get
            Return _curBankFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankFieldName <> value Then
                _curBankFieldName = value
                OnPropertyChanged("BankFieldName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Function Name
    ''' </summary>
    ''' <value>Function Name</value>
    ''' <returns>Returns Function Name</returns>
    ''' <remarks></remarks>
    Public Property FunctionName() As String
        Get
            Return _curFunctionName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curFunctionName <> value Then
                _curFunctionName = value
                OnPropertyChanged("FunctionName")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Starting Position
    ''' </summary>
    ''' <value>Starting Position</value>
    ''' <returns>Returns Starting Position</returns>
    ''' <remarks></remarks>
    ''' '''''''''''''''''modified by Jacky on 2018-11-07'''''''''''''''''''''''''''''
    ''' ''''''''''''''''''''''original code'''''''''''''''''''''''''''''''''''
    Public Property StartingPosition() As Nullable(Of Integer)
        Get
            If _curFunctionName = "LEFT" Or _curFunctionName = "RIGHT" Or _curFunctionName = "REMOVE LAST" Or _curFunctionName = "TRIM" Or _curFunctionName = "LEFT & TRIM" Or _curFunctionName = "RIGHT & TRIM" Or _curFunctionName = "REMOVE LAST & TRIM" Then _curStartingPosition = Nothing
            Return _curStartingPosition
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If _curStartingPosition.HasValue And value.HasValue Then
                If _curStartingPosition.Value <> value.Value Then
                    _curStartingPosition = value
                    OnPropertyChanged("StartingPosition")
                End If
            ElseIf Not _curStartingPosition.HasValue And Not value.HasValue Then
                'Do nothing
                _curStartingPosition = value
            Else
                _curStartingPosition = value
                OnPropertyChanged("StartingPosition")
            End If
        End Set
    End Property

    ''' <summary>
    ''' This property is to Get/Set Number Of Characters
    ''' </summary>
    ''' <value>Number Of Characters</value>
    ''' <returns>Returns Number Of Characters</returns>
    ''' <remarks></remarks>
    Public Property NumberOfCharacters() As Nullable(Of Integer)
        Get
            If _curNumberOfCharacters.HasValue Then
                If _curNumberOfCharacters.Value = 0 Then
                    Return Nothing
                Else
                    Return _curNumberOfCharacters.Value
                End If
            Else
                Return _curNumberOfCharacters
            End If
        End Get
        Set(ByVal value As Nullable(Of Integer))

            If _curNumberOfCharacters.HasValue And value.HasValue Then
                If _curNumberOfCharacters.Value <> value.Value Then
                    _curNumberOfCharacters = value
                    OnPropertyChanged("NumberOfCharacters")
                End If
            ElseIf Not _curNumberOfCharacters.HasValue And Not value.HasValue Then
                'Do nothing
            Else
                _curNumberOfCharacters = value
                OnPropertyChanged("NumberOfCharacters")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Applies Changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriBankFieldName = _curBankFieldName
        _oriFunctionName = _curFunctionName
        _oriStartingPosition = _curStartingPosition
        _oriNumberOfCharacters = _curNumberOfCharacters
    End Sub
    ''' <summary>
    ''' Begins making changes to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldBankFieldName = _curBankFieldName
        _oldFunctionName = _curFunctionName
        _oldStartingPosition = _curStartingPosition
        _oldNumberOfCharacters = _curNumberOfCharacters
    End Sub
    ''' <summary>
    ''' Cancels the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curBankFieldName = _oldBankFieldName
        _curFunctionName = _oldFunctionName
        _curStartingPosition = _oldStartingPosition
        _curNumberOfCharacters = _oldNumberOfCharacters
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankFieldName = ReadXMLElement(reader, "_curBankFieldName")
            _oldBankFieldName = ReadXMLElement(reader, "_oldBankFieldName")
            _oriBankFieldName = ReadXMLElement(reader, "_oriBankFieldName")
            _curFunctionName = ReadXMLElement(reader, "_curFunctionName")
            _oldFunctionName = ReadXMLElement(reader, "_oldFunctionName")
            _oriFunctionName = ReadXMLElement(reader, "_oriFunctionName")
            _curStartingPosition = ReadXMLElement(reader, "_curStartingPosition")
            _oldStartingPosition = ReadXMLElement(reader, "_oldStartingPosition")
            _oriStartingPosition = ReadXMLElement(reader, "_oriStartingPosition")
            _curNumberOfCharacters = ReadXMLElement(reader, "_curNumberOfCharacters")
            _oldNumberOfCharacters = ReadXMLElement(reader, "_oldNumberOfCharacters")
            _oriNumberOfCharacters = ReadXMLElement(reader, "_oriNumberOfCharacters")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankFieldName", _curBankFieldName)
        WriteXmlElement(writer, "_oldBankFieldName", _oldBankFieldName)
        WriteXmlElement(writer, "_oriBankFieldName", _oriBankFieldName)
        WriteXmlElement(writer, "_curFunctionName", _curFunctionName)
        WriteXmlElement(writer, "_oldFunctionName", _oldFunctionName)
        WriteXmlElement(writer, "_oriFunctionName", _oriFunctionName)
        WriteXmlElement(writer, "_curStartingPosition", _curStartingPosition)
        WriteXmlElement(writer, "_oldStartingPosition", _oldStartingPosition)
        WriteXmlElement(writer, "_oriStartingPosition", _oriStartingPosition)
        WriteXmlElement(writer, "_curNumberOfCharacters", _curNumberOfCharacters)
        WriteXmlElement(writer, "_oldNumberOfCharacters", _oldNumberOfCharacters)
        WriteXmlElement(writer, "_oriNumberOfCharacters", _oriNumberOfCharacters)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves the changes made to this object
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(StringManipulation))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads this object from the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As StringManipulation
        Dim serializer As New XmlSerializer(GetType(StringManipulation))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), StringManipulation)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As StringManipulation = DirectCast(obj, StringManipulation)
        _curBankFieldName = objE._curBankFieldName
        _oldBankFieldName = objE._oldBankFieldName
        _oriBankFieldName = objE._oriBankFieldName
        _curFunctionName = objE._curFunctionName
        _oldFunctionName = objE._oldFunctionName
        _oriFunctionName = objE._oriFunctionName
        _curStartingPosition = objE._curStartingPosition
        _oldStartingPosition = objE._oldStartingPosition
        _oriStartingPosition = objE._oriStartingPosition
        _curNumberOfCharacters = objE._curNumberOfCharacters
        _oldNumberOfCharacters = objE._oldNumberOfCharacters
        _oriNumberOfCharacters = objE._oriNumberOfCharacters
    End Sub
#End Region
End Class
''' <summary>
''' Collection of String Manipulation Settings
''' </summary>
''' <remarks></remarks>
Partial Public Class StringManipulationCollection
    Inherits BusinessBaseCollection(Of StringManipulation)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of StringManipulation)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of StringManipulation)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of StringManipulation)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As StringManipulation
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("StringManipulationCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "StringManipulation"
            child = New StringManipulation
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As StringManipulation
        writer.WriteStartElement("StringManipulationCollection")
        For Each child In Me
            writer.WriteStartElement("StringManipulation")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
