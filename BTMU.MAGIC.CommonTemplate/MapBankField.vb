Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates a Bank Field
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class MapBankField
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curBankField As String
    Private _oldBankField As String
    Private _oriBankField As String
    Private _curBankSampleValue As String
    Private _oldBankSampleValue As String
    Private _oriBankSampleValue As String
    Private _curMapSeparator As String
    Private _oldMapSeparator As String
    Private _oriMapSeparator As String
    Private _curMandatory As Boolean
    Private _oldMandatory As Boolean
    Private _oriMandatory As Boolean

    Private _curCarriageReturn As Boolean
    Private _oldCarriageReturn As Boolean
    Private _oriCarriageReturn As Boolean

    Private _curHeader As Boolean
    Private _oldHeader As Boolean
    Private _oriHeader As Boolean
    Private _curDetail As String
    Private _oldDetail As String
    Private _oriDetail As String
    Private _curTrailer As Boolean
    Private _oldTrailer As Boolean
    Private _oriTrailer As Boolean
    Private _curMappedSourceFields As New MapSourceFieldCollection
    Private _curDataType As String
    Private _oldDataType As String
    Private _oriDataType As String
    Private _curDateFormat As String
    Private _oldDateFormat As String
    Private _oriDateFormat As String
    Private _curDataLength As Nullable(Of Integer)
    Private _oldDataLength As Nullable(Of Integer)
    Private _oriDataLength As Nullable(Of Integer)
    Private _curPosition As Integer
    Private _oldPosition As Integer
    Private _oriPosition As Integer
    Private _curDefaultValue As String
    Private _oldDefaultValue As String
    Private _oriDefaultValue As String
    Private _curIncludeDecimal As String
    Private _oldIncludeDecimal As String
    Private _oriIncludeDecimal As String
    Private _curDecimalSeparator As String
    Private _oldDecimalSeparator As String
    Private _oriDecimalSeparator As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "

    ''' <summary>
    ''' Extended Bank Field
    ''' </summary>
    ''' <value>
    ''' Returns Bank Field name prefixed with characters to identify 
    ''' if its (M)andatory/(O)ptional, and (H)eader, (D)etail, (t)ransaction, (T)ail or (a)dvice </value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property BankFieldExt() As String
        Get
            Dim _opt2 As String = ""
            _opt2 &= IIf(Header, "H", "")
            _opt2 &= IIf(Detail = "Yes", "D", "")
            _opt2 &= IIf(Detail = "Transaction", "t", "")
            _opt2 &= IIf(Detail = "Advice", "a", "")
            _opt2 &= IIf(Detail = "Payment", "p", "")
            _opt2 &= IIf(Detail = "WHT", "w", "")
            _opt2 &= IIf(Detail = "Invoice", "i", "")
            _opt2 &= IIf(Trailer, "T", "")
            Return String.Format("[{0}][{1}]{2}", IIf(Mandatory, "M", "O"), _
               _opt2, _
                BankField)
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get the resolved Field names
    ''' </summary>
    ''' <value>Mapped Field Names</value>
    ''' <returns>Returns Mapped Field Names</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MappedFields() As String
        Get
            Dim fields As String = ""
            For Each bk As MapSourceField In MappedSourceFields
                fields &= bk.SourceFieldName & "||"
            Next
            If fields.IndexOf("||") <> -1 Then
                fields = fields.Substring(0, fields.Length - 2)
            End If
            Return fields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get the values of mapped fields
    ''' </summary>
    ''' <value>Values of Mapped Source fields</value>
    ''' <returns>Returns Values of Mapped Source fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MappedValues() As String
        ' Modified On : 17/02/2010
        ' Modified By : Meera
        ' Requested By : AITB on 17/02/2010
        ' This modification is done to eliminate the // in the mapped value 
        Get
            Dim values As String = ""
            Dim columnNumber As Integer = 1
            For Each bv As MapSourceField In MappedSourceFields
                If Not IsNothingOrEmptyString(bv.SourceFieldValue) Then ' If loop added for the modification
                    values &= bv.SourceFieldValue & MapSeparator
                    If CarriageReturn Then
                        values &= Environment.NewLine
                    End If
                    If (BankField = "Intermediary Bank/Branch/Address") Then
                        If columnNumber = 1 Then
                            values = values.PadRight(35, " ")
                        ElseIf columnNumber = 2 Then
                            values = values.PadRight(105, " ")
                        Else
                            values = values.PadRight(140, " ")
                        End If
                    End If
                Else
                    values &= bv.SourceFieldValue
                    If (BankField = "Intermediary Bank/Branch/Address") Then
                        If columnNumber = 1 Then
                            values = values.PadRight(35, " ")
                        ElseIf columnNumber = 2 Then
                            values = values.PadRight(105, " ")
                        Else
                            values = values.PadRight(140, " ")
                        End If
                    End If
                End If
                columnNumber += 1
            Next
            If Not MapSeparator Is Nothing Then
                If MapSeparator <> String.Empty And values.IndexOf(MapSeparator) <> -1 Then
                    If CarriageReturn Then
                        values = values.Substring(0, values.Length - MapSeparator.Length - Environment.NewLine.Length)
                    Else
                        values = values.Substring(0, values.Length - MapSeparator.Length)
                    End If
                End If
            End If
            Return values
        End Get
    End Property

    ''' <summary>
    ''' This property is to Get/Set Bank Field Name
    ''' </summary>
    ''' <value>Bank Field Name</value>
    ''' <returns>Returns Bank Field Name</returns>
    ''' <remarks></remarks>
    Public Property BankField() As String
        Get
            Return _curBankField
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankField <> value Then
                _curBankField = value
                OnPropertyChanged("BankField")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Bank Sample Value
    ''' </summary>
    ''' <value>Bank Sample Value</value>
    ''' <returns>Returns Bank Sample Value</returns>
    ''' <remarks></remarks>
    Public Property BankSampleValue() As String
        Get
            Return _curBankSampleValue
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curBankSampleValue <> value Then
                _curBankSampleValue = value
                OnPropertyChanged("BankSampleValue")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Map Separator
    ''' </summary>
    ''' <value>Map Separator</value>
    ''' <returns>Returns Map Separator</returns>
    ''' <remarks></remarks>
    Public Property MapSeparator() As String
        Get
            Return _curMapSeparator
        End Get
        Set(ByVal value As String)
            If _curMapSeparator <> value Then
                _curMapSeparator = value
                OnPropertyChanged("MapSeparator")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether this bank field is mandatory
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether this field is mandatory</returns>
    ''' <remarks></remarks>
    Public Property Mandatory() As Boolean
        Get
            Return _curMandatory
        End Get
        Set(ByVal value As Boolean)
            If _curMandatory <> value Then
                _curMandatory = value
                OnPropertyChanged("Mandatory")
            End If
        End Set
    End Property

    Public Property CarriageReturn() As Boolean
        Get
            Return _curCarriageReturn
        End Get
        Set(ByVal value As Boolean)
            If _curCarriageReturn <> value Then
                _curCarriageReturn = value
                OnPropertyChanged("CarriageReturn")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Determines whether this bank field is a header
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether this bank field is a Header</returns>
    ''' <remarks></remarks>
    Public Property Header() As Boolean
        Get
            Return _curHeader
        End Get
        Set(ByVal value As Boolean)
            If _curHeader <> value Then
                _curHeader = value
                OnPropertyChanged("Header")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether this bank field is a Detail field
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether this bank field is a Detail field</returns>
    ''' <remarks></remarks>
    Public Property Detail() As String
        Get
            Return _curDetail
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDetail <> value Then
                _curDetail = value
                OnPropertyChanged("Detail")
            End If
        End Set
    End Property
    ''' <summary>
    ''' Determines whether this bank field is a Trailer field
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether this bank field is a Trailer field</returns>
    ''' <remarks></remarks>
    Public Property Trailer() As Boolean
        Get
            Return _curTrailer
        End Get
        Set(ByVal value As Boolean)
            If _curTrailer <> value Then
                _curTrailer = value
                OnPropertyChanged("Trailer")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get a List of Mapped Source Fields
    ''' </summary>
    ''' <value>Mapped Source Fields</value>
    ''' <returns>Returns a List of Mapped Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MappedSourceFields() As MapSourceFieldCollection
        Get
            Return _curMappedSourceFields
        End Get
    End Property
    ''' <summary>
    ''' This property is to Get/Set Data Type of Bank Field
    ''' </summary>
    ''' <value>Data Type</value>
    ''' <returns>Returns Data Type</returns>
    ''' <remarks></remarks>
    Public Property DataType() As String
        Get
            Return _curDataType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDataType <> value Then
                _curDataType = value
                OnPropertyChanged("DataType")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Date Format of Bank Field
    ''' </summary>
    ''' <value>Date Format</value>
    ''' <returns>Returns Date Format </returns>
    ''' <remarks></remarks>
    Public Property DateFormat() As String
        Get
            Return _curDateFormat
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDateFormat <> value Then
                _curDateFormat = value
                OnPropertyChanged("DateFormat")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Data Length of Bank Field
    ''' </summary>
    ''' <value>Data Length</value>
    ''' <returns>Returns Data Length</returns>
    ''' <remarks></remarks>
    Public Property DataLength() As Nullable(Of Integer)
        Get
            Return _curDataLength
        End Get
        Set(ByVal value As Nullable(Of Integer))

            If _curDataLength.HasValue And value.HasValue Then
                If _curDataLength.Value <> value.Value Then
                    _curDataLength = value
                    OnPropertyChanged("DataLength")
                End If
            ElseIf Not _curDataLength.HasValue And Not value.HasValue Then
                'Do nothing
            Else
                _curDataLength = value
                OnPropertyChanged("DataLength")
            End If

        End Set
    End Property
    ''' <summary>
    ''' This Property is to Get/Set Position
    ''' </summary>
    ''' <value>Position</value>
    ''' <returns>Returns Position</returns>
    ''' <remarks></remarks>
    Public Property Position() As Integer
        Get
            Return _curPosition
        End Get
        Set(ByVal value As Integer)
            If _curPosition <> value Then
                _curPosition = value
                OnPropertyChanged("Position")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Default Value of Bank Field
    ''' </summary>
    ''' <value>Default Value</value>
    ''' <returns>Returns Default Value</returns>
    ''' <remarks></remarks>
    Public Property DefaultValue() As String
        Get
            Return _curDefaultValue
        End Get
        Set(ByVal value As String)
            'If Not value Is Nothing Then value = value.Trim()

            If _curDefaultValue <> value Then
                _curDefaultValue = value
                OnPropertyChanged("DefaultValue")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Include Decimal 
    ''' </summary>
    ''' <value>Include Decimal</value>
    ''' <returns>Returns Include Decimal</returns>
    ''' <remarks></remarks>
    Public Property IncludeDecimal() As String
        Get
            Return _curIncludeDecimal
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curIncludeDecimal <> value Then
                _curIncludeDecimal = value
                OnPropertyChanged("IncludeDecimal")
            End If
        End Set
    End Property
    ''' <summary>
    ''' This property is to Get/Set Decimal Separator 
    ''' </summary>
    ''' <value>Decimal Separator </value>
    ''' <returns>Returns Decimal Separator </returns>
    ''' <remarks></remarks>
    Public Property DecimalSeparator() As String
        Get
            Return _curDecimalSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim()

            If _curDecimalSeparator <> value Then
                _curDecimalSeparator = value
                OnPropertyChanged("DecimalSeparator")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    Public Overrides Sub ApplyEdit()
        _oriBankField = _curBankField
        _oriBankSampleValue = _curBankSampleValue
        _oriMapSeparator = _curMapSeparator
        _oriMandatory = _curMandatory

        _oriCarriageReturn = _curCarriageReturn

        _oriHeader = _curHeader
        _oriDetail = _curDetail
        _oriTrailer = _curTrailer
        _oriDataType = _curDataType
        _oriDateFormat = _curDateFormat
        _oriDataLength = _curDataLength
        _oriPosition = _curPosition
        _oriDefaultValue = _curDefaultValue
        _oriIncludeDecimal = _curIncludeDecimal
        _oriDecimalSeparator = _curDecimalSeparator
    End Sub
    Public Overrides Sub BeginEdit()
        _oldBankField = _curBankField
        _oldBankSampleValue = _curBankSampleValue
        _oldMapSeparator = _curMapSeparator
        _oldMandatory = _curMandatory

        _oldCarriageReturn = _curCarriageReturn

        _oldHeader = _curHeader
        _oldDetail = _curDetail
        _oldTrailer = _curTrailer
        _oldDataType = _curDataType
        _oldDateFormat = _curDateFormat
        _oldDataLength = _curDataLength

        _oldPosition = _curPosition
        _oldDefaultValue = _curDefaultValue
        _oldIncludeDecimal = _curIncludeDecimal
        _oldDecimalSeparator = _curDecimalSeparator

    End Sub
    Public Overrides Sub CancelEdit()
        _curBankField = _oldBankField
        _curBankSampleValue = _oldBankSampleValue
        _curMapSeparator = _oldMapSeparator
        _curMandatory = _oldMandatory

        _curCarriageReturn = _oldCarriageReturn

        _curHeader = _oldHeader
        _curDetail = _oldDetail
        _curTrailer = _oldTrailer
        _curDataType = _oldDataType
        _curDateFormat = _oldDateFormat
        _curDataLength = _oldDataLength
        _curPosition = _oldPosition
        _curDefaultValue = _oldDefaultValue
        _curIncludeDecimal = _oldIncludeDecimal
        _curDecimalSeparator = _oldDecimalSeparator
 
    End Sub
#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curBankField = ReadXMLElement(reader, "_curBankField")
            _oldBankField = ReadXMLElement(reader, "_oldBankField")
            _oriBankField = ReadXMLElement(reader, "_oriBankField")
            _curBankSampleValue = ReadXMLElement(reader, "_curBankSampleValue")
            _oldBankSampleValue = ReadXMLElement(reader, "_oldBankSampleValue")
            _oriBankSampleValue = ReadXMLElement(reader, "_oriBankSampleValue")
            _curMapSeparator = ReadXMLElement(reader, "_curMapSeparator")
            _oldMapSeparator = ReadXMLElement(reader, "_oldMapSeparator")
            _oriMapSeparator = ReadXMLElement(reader, "_oriMapSeparator")
            _curMandatory = ReadXMLElement(reader, "_curMandatory")
            _oldMandatory = ReadXMLElement(reader, "_oldMandatory")
            _oriMandatory = ReadXMLElement(reader, "_oriMandatory")

            Dim temp As String
            temp = ReadXMLElement(reader, "_curCarriageReturn")
            If temp Is Nothing Then
                _curCarriageReturn = False
            Else
                _curCarriageReturn = temp
            End If
            temp = ReadXMLElement(reader, "_oldCarriageReturn")
            If temp Is Nothing Then
                _oldCarriageReturn = False
            Else
                _oldCarriageReturn = temp
            End If
            temp = ReadXMLElement(reader, "_oriCarriageReturn")
            If temp Is Nothing Then
                _oriCarriageReturn = False
            Else
                _oriCarriageReturn = temp
            End If

            _curHeader = ReadXMLElement(reader, "_curHeader")
            _oldHeader = ReadXMLElement(reader, "_oldHeader")
            _oriHeader = ReadXMLElement(reader, "_oriHeader")
            _curDetail = ReadXMLElement(reader, "_curDetail")
            _oldDetail = ReadXMLElement(reader, "_oldDetail")
            _oriDetail = ReadXMLElement(reader, "_oriDetail")
            _curTrailer = ReadXMLElement(reader, "_curTrailer")
            _oldTrailer = ReadXMLElement(reader, "_oldTrailer")
            _oriTrailer = ReadXMLElement(reader, "_oriTrailer")
            _curMappedSourceFields.ReadXml(reader)
            _curDataType = ReadXMLElement(reader, "_curDataType")
            _oldDataType = ReadXMLElement(reader, "_oldDataType")
            _oriDataType = ReadXMLElement(reader, "_oriDataType")
            _curDateFormat = ReadXMLElement(reader, "_curDateFormat")
            _oldDateFormat = ReadXMLElement(reader, "_oldDateFormat")
            _oriDateFormat = ReadXMLElement(reader, "_oriDateFormat")
            _curDataLength = ReadXMLElement(reader, "_curDataLength")
            _oldDataLength = ReadXMLElement(reader, "_oldDataLength")
            _oriDataLength = ReadXMLElement(reader, "_oriDataLength")
            _curPosition = ReadXMLElement(reader, "_curPosition")
            _oldPosition = ReadXMLElement(reader, "_oldPosition")
            _oriPosition = ReadXMLElement(reader, "_oriPosition")

            _curDefaultValue = ReadXMLElement(reader, "_curDefaultValue")
            _oldDefaultValue = ReadXMLElement(reader, "_oldDefaultValue")
            _oriDefaultValue = ReadXMLElement(reader, "_oriDefaultValue")

            _curIncludeDecimal = ReadXMLElement(reader, "_curIncludeDecimal")
            _oldIncludeDecimal = ReadXMLElement(reader, "_oldIncludeDecimal")
            _oriIncludeDecimal = ReadXMLElement(reader, "_oriIncludeDecimal")

            _curDecimalSeparator = ReadXMLElement(reader, "_curDecimalSeparator")
            _oldDecimalSeparator = ReadXMLElement(reader, "_oldDecimalSeparator")
            _oriDecimalSeparator = ReadXMLElement(reader, "_oriDecimalSeparator")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curBankField", _curBankField)
        WriteXmlElement(writer, "_oldBankField", _oldBankField)
        WriteXmlElement(writer, "_oriBankField", _oriBankField)
        WriteXmlElement(writer, "_curBankSampleValue", _curBankSampleValue)
        WriteXmlElement(writer, "_oldBankSampleValue", _oldBankSampleValue)
        WriteXmlElement(writer, "_oriBankSampleValue", _oriBankSampleValue)
        WriteXmlElement(writer, "_curMapSeparator", _curMapSeparator)
        WriteXmlElement(writer, "_oldMapSeparator", _oldMapSeparator)
        WriteXmlElement(writer, "_oriMapSeparator", _oriMapSeparator)
        WriteXmlElement(writer, "_curMandatory", _curMandatory)
        WriteXmlElement(writer, "_oldMandatory", _oldMandatory)
        WriteXmlElement(writer, "_oriMandatory", _oriMandatory)

        WriteXmlElement(writer, "_curCarriageReturn", _curCarriageReturn)
        WriteXmlElement(writer, "_oldCarriageReturn", _oldCarriageReturn)
        WriteXmlElement(writer, "_oriCarriageReturn", _oriCarriageReturn)

        WriteXmlElement(writer, "_curHeader", _curHeader)
        WriteXmlElement(writer, "_oldHeader", _oldHeader)
        WriteXmlElement(writer, "_oriHeader", _oriHeader)
        WriteXmlElement(writer, "_curDetail", _curDetail)
        WriteXmlElement(writer, "_oldDetail", _oldDetail)
        WriteXmlElement(writer, "_oriDetail", _oriDetail)
        WriteXmlElement(writer, "_curTrailer", _curTrailer)
        WriteXmlElement(writer, "_oldTrailer", _oldTrailer)
        WriteXmlElement(writer, "_oriTrailer", _oriTrailer)
        _curMappedSourceFields.WriteXml(writer)
        WriteXmlElement(writer, "_curDataType", _curDataType)
        WriteXmlElement(writer, "_oldDataType", _oldDataType)
        WriteXmlElement(writer, "_oriDataType", _oriDataType)
        WriteXmlElement(writer, "_curDateFormat", _curDateFormat)
        WriteXmlElement(writer, "_oldDateFormat", _oldDateFormat)
        WriteXmlElement(writer, "_oriDateFormat", _oriDateFormat)
        WriteXmlElement(writer, "_curDataLength", _curDataLength)
        WriteXmlElement(writer, "_oldDataLength", _oldDataLength)
        WriteXmlElement(writer, "_oriDataLength", _oriDataLength)

        WriteXmlElement(writer, "_curPosition", _curPosition)
        WriteXmlElement(writer, "_oldPosition", _oldPosition)
        WriteXmlElement(writer, "_oriPosition", _oriPosition)

        WriteXmlElement(writer, "_curDefaultValue", _curDefaultValue)
        WriteXmlElement(writer, "_oldDefaultValue", _oldDefaultValue)
        WriteXmlElement(writer, "_oriDefaultValue", _oriDefaultValue)
  
        WriteXmlElement(writer, "_curIncludeDecimal", _curIncludeDecimal)
        WriteXmlElement(writer, "_oldIncludeDecimal", _oldIncludeDecimal)
        WriteXmlElement(writer, "_oriIncludeDecimal", _oriIncludeDecimal)

        WriteXmlElement(writer, "_curDecimalSeparator", _curDecimalSeparator)
        WriteXmlElement(writer, "_oldDecimalSeparator", _oldDecimalSeparator)
        WriteXmlElement(writer, "_oriDecimalSeparator", _oriDecimalSeparator)
 
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves to the Given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(MapBankField))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads the Objec from the given file
    ''' </summary>
    ''' <param name="filename">File Name</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As MapBankField
        Dim serializer As New XmlSerializer(GetType(MapBankField))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), MapBankField)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As MapBankField = DirectCast(obj, MapBankField)
        _curBankField = objE._curBankField
        _oldBankField = objE._oldBankField
        _oriBankField = objE._oriBankField
        _curBankSampleValue = objE._curBankSampleValue
        _oldBankSampleValue = objE._oldBankSampleValue
        _oriBankSampleValue = objE._oriBankSampleValue
        _curMapSeparator = objE._curMapSeparator
        _oldMapSeparator = objE._oldMapSeparator
        _oriMapSeparator = objE._oriMapSeparator
        _curMandatory = objE._curMandatory
        _oldMandatory = objE._oldMandatory
        _oriMandatory = objE._oriMandatory

        _curCarriageReturn = objE._curCarriageReturn
        _oldCarriageReturn = objE._oldCarriageReturn
        _oriCarriageReturn = objE._oriCarriageReturn

        _curHeader = objE._curHeader
        _oldHeader = objE._oldHeader
        _oriHeader = objE._oriHeader
        _curDetail = objE._curDetail
        _oldDetail = objE._oldDetail
        _oriDetail = objE._oriDetail
        _curTrailer = objE._curTrailer
        _oldTrailer = objE._oldTrailer
        _oriTrailer = objE._oriTrailer
        _curMappedSourceFields = objE._curMappedSourceFields
        _curDataType = objE._curDataType
        _oldDataType = objE._oldDataType
        _oriDataType = objE._oriDataType
        _curDateFormat = objE._curDateFormat
        _oldDateFormat = objE._oldDateFormat
        _oriDateFormat = objE._oriDateFormat
        _curDataLength = objE._curDataLength
        _oldDataLength = objE._oldDataLength
        _oriDataLength = objE._oriDataLength
        _curPosition = objE._curPosition
        _oldPosition = objE._oldPosition
        _oriPosition = objE._oriPosition
        _curDefaultValue = objE._curDefaultValue
        _oldDefaultValue = objE._oldDefaultValue
        _oriDefaultValue = objE._oriDefaultValue
        _curIncludeDecimal = objE._curIncludeDecimal
        _oldIncludeDecimal = objE._oldIncludeDecimal
        _oriIncludeDecimal = objE._oriIncludeDecimal
        _curDecimalSeparator = objE._curDecimalSeparator
        _oldDecimalSeparator = objE._oldDecimalSeparator
        _oriDecimalSeparator = objE._oriDecimalSeparator

    End Sub
    ''' <summary>
    ''' Validates the Fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Validate()
        ValidationEngine.Validate()
        'Dim childMappedSourceFields As MapSourceField
        'For Each childMappedSourceFields In _curMappedSourceFields
        '    childMappedSourceFields.Validate()
        'Next
    End Sub
    ''' <summary>
    ''' Determines whether the fields are valid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property IsValid() As Boolean
        Get
            If Not ValidationEngine.IsValid Then
                Return False
            End If
            'Dim childMappedSourceFields As MapSourceField
            'For Each childMappedSourceFields In _curMappedSourceFields
            '    If Not childMappedSourceFields.IsValid Then
            '        Return False
            '    End If
            'Next
            Return True
        End Get
    End Property
#End Region


    Public Sub MappingChanged()
        OnPropertyChanged("BankFieldExt")
    End Sub

    Public Sub CopyTo(ByRef duplicateBankField As MapBankField)
        duplicateBankField._curBankField = _curBankField
        duplicateBankField._oldBankField = _oldBankField
        duplicateBankField._oriBankField = _oriBankField
        duplicateBankField._curBankSampleValue = _curBankSampleValue
        duplicateBankField._oldBankSampleValue = _oldBankSampleValue
        duplicateBankField._oriBankSampleValue = _oriBankSampleValue
        duplicateBankField._curMapSeparator = _curMapSeparator
        duplicateBankField._oldMapSeparator = _oldMapSeparator
        duplicateBankField._oriMapSeparator = _oriMapSeparator
        duplicateBankField._curMandatory = _curMandatory
        duplicateBankField._oldMandatory = _oldMandatory
        duplicateBankField._oriMandatory = _oriMandatory

        duplicateBankField._curCarriageReturn = _curCarriageReturn
        duplicateBankField._oldCarriageReturn = _oldCarriageReturn
        duplicateBankField._oriCarriageReturn = _oriCarriageReturn

        duplicateBankField._curHeader = _curHeader
        duplicateBankField._oldHeader = _oldHeader
        duplicateBankField._oriHeader = _oriHeader
        duplicateBankField._curDetail = _curDetail
        duplicateBankField._oldDetail = _oldDetail
        duplicateBankField._oriDetail = _oriDetail
        duplicateBankField._curTrailer = _curTrailer
        duplicateBankField._oldTrailer = _oldTrailer
        duplicateBankField._oriTrailer = _oriTrailer

        For Each SourceField As MapSourceField In _curMappedSourceFields
            Dim dupSourceField As New MapSourceField()
            SourceField.CopyTo(dupSourceField)
            duplicateBankField._curMappedSourceFields.Add(dupSourceField)
        Next

        duplicateBankField._curDataType = _curDataType
        duplicateBankField._oldDataType = _oldDataType
        duplicateBankField._oriDataType = _oriDataType
        duplicateBankField._curDateFormat = _curDateFormat
        duplicateBankField._oldDateFormat = _oldDateFormat
        duplicateBankField._oriDateFormat = _oriDateFormat
        duplicateBankField._curDataLength = _curDataLength
        duplicateBankField._oldDataLength = _oldDataLength
        duplicateBankField._oriDataLength = _oriDataLength
        duplicateBankField._curPosition = _curPosition
        duplicateBankField._oldPosition = _oldPosition
        duplicateBankField._oriPosition = _oriPosition
        duplicateBankField._curDefaultValue = _curDefaultValue
        duplicateBankField._oldDefaultValue = _oldDefaultValue
        duplicateBankField._oriDefaultValue = _oriDefaultValue
        duplicateBankField._curIncludeDecimal = _curIncludeDecimal
        duplicateBankField._oldIncludeDecimal = _oldIncludeDecimal
        duplicateBankField._oriIncludeDecimal = _oriIncludeDecimal
        duplicateBankField._curDecimalSeparator = _curDecimalSeparator
        duplicateBankField._oldDecimalSeparator = _oldDecimalSeparator
        duplicateBankField._oriDecimalSeparator = _oriDecimalSeparator

        duplicateBankField._curCreatedBy = _curCreatedBy
        duplicateBankField._curCreatedDate = _curCreatedDate
        duplicateBankField._curModifiedBy = _curModifiedBy
        duplicateBankField._curModifiedDate = _curModifiedDate
        duplicateBankField._oriModifiedBy = _oriModifiedBy
        duplicateBankField._oriModifiedDate = _oriModifiedDate

    End Sub

End Class
''' <summary>
''' Collection of Bank Fields
''' </summary>
''' <remarks></remarks>
Partial Public Class MapBankFieldCollection
    Inherits BusinessBaseCollection(Of MapBankField)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of MapBankField)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of MapBankField)(Me)
    End Sub

    Public Overloads Function IndexOf(ByVal bankfieldname As String) As Int32

        Dim _cnt As Int32 = 0
        For Each bkfield As MapBankField In Items
            If bkfield.BankField.Equals(bankfieldname, StringComparison.InvariantCultureIgnoreCase) Then Return _cnt
            _cnt += 1
        Next

        Return -1

    End Function


    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of MapBankField)
        Get
            Return _bndListView
        End Get
    End Property


    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As MapBankField
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("MapBankFieldCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "MapBankField"
            child = New MapBankField
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()

    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As MapBankField
        writer.WriteStartElement("MapBankFieldCollection")
        For Each child In Me
            writer.WriteStartElement("MapBankField")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class

 
