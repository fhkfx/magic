Imports btmu.Magic.MasterTemplate
Imports System.IO
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

Partial Public Class MasterTemplateSharedFunction
    Inherits BTMU.MAGIC.Common.BusinessBase

    ''Please dont make any changes here... It must derive these values from the Main Program
    ' These variables are set at the Main Program during startup
    Public Shared _masterTemplateViewFolder As String
    Public Shared _masterTemplateViewDraftFolder As String
    Public Shared _masterTemplateFileExtension As String

    Public ReadOnly Property MasterTemplateViewFolder() As String
        Get
            'Return System.Windows.Forms.Application.StartupPath & _masterTemplateViewFolder
            Return _masterTemplateViewFolder
        End Get
    End Property

    Public ReadOnly Property MasterTemplateViewDraftFolder() As String
        Get
            'Return System.Windows.Forms.Application.StartupPath & _masterTemplateViewDraftFolder
            Return _masterTemplateViewDraftFolder
        End Get
    End Property

    Public ReadOnly Property MasterTemplateFileExtension() As String
        Get
            Return _masterTemplateFileExtension
        End Get
    End Property

    Public Shared Function ListAllMasterTemplateList() As MasterTemplateListCollection
        Dim filePath As String
        Dim content As String
        Dim serializerMasterTemplateList As New XmlSerializer(GetType(MasterTemplateList))
        Dim lstMaster As New MasterTemplateListCollection
        Dim mf As New MasterTemplateSharedFunction

        Try
            For Each filePath In System.IO.Directory.GetFiles(mf.MasterTemplateViewFolder, "*" & mf.MasterTemplateFileExtension, SearchOption.AllDirectories)

                Dim doc As XPathDocument = New XPathDocument(filePath)
                Dim nav As XPathNavigator = doc.CreateNavigator()
                Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/ListValue")
                Dim item As New MasterTemplateList
                Iterator.MoveNext()
                content = Iterator.Current.Value


                'Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(content))
                Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
                stream.Position = 0

                item = CType(serializerMasterTemplateList.Deserialize(stream), MasterTemplateList)

                lstMaster.Add(item)
            Next

            Return lstMaster
        Catch ex As Exception
            Return lstMaster
        End Try

    End Function

    Public Shared Function LoadFixedMasterTemplate(ByVal TemplateFile As String) As MasterTemplateFixedDetailCollection
        Dim lstMaster As New MasterTemplateFixedDetailCollection
        Dim item As MasterTemplateFixed

        item = (New MasterTemplateFixed).LoadFromFile2(TemplateFile)
        lstMaster = item.MasterTemplateFixedDetailCollection

        Return lstMaster
    End Function

    Public Shared Function IsFixedMasterTemplate(ByVal TemplateName As String) As Boolean

        Try
            Dim mf As New MasterTemplateSharedFunction

            Dim item As MasterTemplateFixed
            TemplateName = System.IO.Path.Combine(mf.MasterTemplateViewFolder, TemplateName) & mf.MasterTemplateFileExtension
            item = (New MasterTemplateFixed).LoadFromFile2(TemplateName)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function IsNonFixedMasterTemplate(ByVal TemplateName As String) As Boolean

        Try
            Dim mf As New MasterTemplateSharedFunction

            Dim item As MasterTemplateNonFixed
            TemplateName = System.IO.Path.Combine(mf.MasterTemplateViewFolder, TemplateName) & mf.MasterTemplateFileExtension
            item = (New MasterTemplateNonFixed).LoadFromFile2(TemplateName)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Shared Function LoadNonFixedMasterTemplate(ByVal TemplateFile As String) As MasterTemplateNonFixedDetailCollection
        Dim lstMaster As New MasterTemplateNonFixedDetailCollection
        Dim item As MasterTemplateNonFixed

        item = (New MasterTemplateNonFixed).LoadFromFile2(TemplateFile)
        lstMaster = item.MasterTemplateNonFixedDetailCollection

        Return lstMaster
    End Function

    Public Shared Function MasterTemplateExists(ByVal mastertemplateName As String) As Boolean
        ' Shall return true, if there exists, in the underlying file system, 
        ' a master template (could either be a fixed or non fixed type) with the given master template name
        ' or false, otherwise
        Dim mf As New MasterTemplateSharedFunction

        Try
            If System.IO.File.Exists(System.IO.Path.Combine(mf.MasterTemplateViewFolder, mastertemplateName) & mf.MasterTemplateFileExtension) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function GetMasterTemplateOutputFormat(ByVal masterTemplateName As String) As String

        Dim mf As New MasterTemplateSharedFunction

        Try
            Dim mtF As MasterTemplateFixed

            mtF = (New MasterTemplateFixed).LoadFromFile2(System.IO.Path.Combine(mf.MasterTemplateViewFolder, masterTemplateName) & mf.MasterTemplateFileExtension)

            Return mtF.OutputFormat

        Catch ex As Exception

        End Try


        Dim mtNF As MasterTemplateNonFixed

        mtNF = (New MasterTemplateNonFixed).LoadFromFile2(System.IO.Path.Combine(mf.MasterTemplateViewFolder, masterTemplateName) & mf.MasterTemplateFileExtension)

        Return mtNF.OutputFormat

    End Function

    Public Overrides Sub ApplyEdit()
    End Sub
    Public Overrides Sub BeginEdit()
    End Sub
    Public Overrides Sub CancelEdit()
    End Sub

End Class
