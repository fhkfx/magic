Public Class MasterTemplateList
    Inherits BTMU.MAGIC.Common.CommonListItem
    Private _templateName As String
    Private _description As String
    Private _outputFormat As String
    Private _templateStatus As Boolean
    Private _encryptCustomerOutputFile As Boolean
    Private _isDraft As Boolean
    Private _isFixed As Boolean

    Public Property TemplateName() As String
        Get
            Return _templateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            _templateName = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            _description = value
        End Set
    End Property

    Public Property OutputFormat() As String
        Get
            Return _outputFormat
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            _outputFormat = value
        End Set
    End Property

    Public Property TemplateStatus() As Boolean
        Get
            Return _templateStatus
        End Get
        Set(ByVal value As Boolean)
            _templateStatus = value
        End Set
    End Property

    Public Property EncryptCustomerOutputFile() As Boolean
        Get
            Return _encryptCustomerOutputFile
        End Get
        Set(ByVal value As Boolean)
            _encryptCustomerOutputFile = value
        End Set
    End Property

    Public Property IsDraft() As Boolean
        Get
            Return _isDraft
        End Get
        Set(ByVal value As Boolean)
            _isDraft = value
        End Set
    End Property

    Public Property IsFixed() As Boolean
        Get
            Return _isFixed
        End Get
        Set(ByVal value As Boolean)
            _isFixed = value
        End Set
    End Property

End Class

Partial Public Class MasterTemplateListCollection
    Inherits System.ComponentModel.BindingList(Of MasterTemplateList)

    Private _bndListView As BTMU.MAGIC.Common.BindingListViewCommon(Of MasterTemplateList)



    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListViewCommon(Of MasterTemplateList)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListViewCommon(Of MasterTemplateList)
        Get
            Return _bndListView
        End Get
    End Property
End Class

