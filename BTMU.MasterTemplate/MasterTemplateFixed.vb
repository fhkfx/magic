Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.XPath
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' Encapsulates the Snapshot information about Master Template Fixed
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class MasterTemplateFixed
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curTemplateName As String
    Private _oldTemplateName As String
    Private _oriTemplateName As String
    Private _curDescription As String
    Private _oldDescription As String
    Private _oriDescription As String
    Private _curTemplateStatus As Boolean
    Private _oldTemplateStatus As Boolean
    Private _oriTemplateStatus As Boolean
    Private _curCustomerOutputExtension As String
    Private _oldCustomerOutputExtension As String
    Private _oriCustomerOutputExtension As String
    Private _curEncryptCustomerOutputFile As Boolean
    Private _oldEncryptCustomerOutputFile As Boolean
    Private _oriEncryptCustomerOutputFile As Boolean

    Private _curDelimiterCharacter As String
    Private _oldDelimiterCharacter As String
    Private _oriDelimiterCharacter As String

    Private _curNewLine As Boolean
    Private _oldNewLine As Boolean
    Private _oriNewLine As Boolean

    Private _curDelimiter As Boolean
    Private _oldDelimiter As Boolean
    Private _oriDelimiter As Boolean

    Private _curMasterTemplateFixedDetailCollection As New MasterTemplateFixedDetailCollection
    Private _curOutputFormat As String
    Private _oldOutputFormat As String
    Private _oriOutputFormat As String
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region

#Region " Public Property "
    Public Property TemplateName() As String
        Get
            Return _curTemplateName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curTemplateName <> value Then
                _curTemplateName = value
                OnPropertyChanged("TemplateName")
            End If
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _curDescription
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDescription <> value Then
                _curDescription = value
                OnPropertyChanged("Description")
            End If
        End Set
    End Property
    Public Property TemplateStatus() As Boolean
        Get
            Return _curTemplateStatus
        End Get
        Set(ByVal value As Boolean)

            If _curTemplateStatus <> value Then
                _curTemplateStatus = value
                OnPropertyChanged("TemplateStatus")
            End If
        End Set
    End Property
    Public Property CustomerOutputExtension() As String
        Get
            Return _curCustomerOutputExtension
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curCustomerOutputExtension <> value Then
                _curCustomerOutputExtension = value
                OnPropertyChanged("CustomerOutputExtension")
            End If
        End Set
    End Property
    Public Property EncryptCustomerOutputFile() As Boolean
        Get
            Return _curEncryptCustomerOutputFile
        End Get
        Set(ByVal value As Boolean)
            If _curEncryptCustomerOutputFile <> value Then
                _curEncryptCustomerOutputFile = value
                OnPropertyChanged("EncryptCustomerOutputFile")
            End If
        End Set
    End Property
    Public Property DelimiterCharacter() As String
        Get
            Return _curDelimiterCharacter
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDelimiterCharacter <> value Then
                _curDelimiterCharacter = value
                _curNewLine = False
                _curDelimiter = True
                OnPropertyChanged("DelimiterCharacter")
            End If
        End Set
    End Property

    Public Property NewLine() As Boolean
        Get
            Return _curNewLine
        End Get
        Set(ByVal value As Boolean)
            If _curNewLine <> value Then
                _curNewLine = value
                _curDelimiter = Not _curNewLine
                OnPropertyChanged("NewLine")
            End If
        End Set
    End Property
    Public Property Delimiter() As Boolean
        Get
            Return _curDelimiter
        End Get
        Set(ByVal value As Boolean)
            If _curDelimiter <> value Then
                _curDelimiter = value
                _curNewLine = Not _curDelimiter
                OnPropertyChanged("Delimiter")
            End If
        End Set
    End Property
    Public ReadOnly Property MasterTemplateFixedDetailCollection() As MasterTemplateFixedDetailCollection
        Get
            Return _curMasterTemplateFixedDetailCollection
        End Get
    End Property
    Public Property OutputFormat() As String
        Get
            Return _curOutputFormat
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curOutputFormat <> value Then
                _curOutputFormat = value
                OnPropertyChanged("OutputFormat")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region

    Public Overrides Sub ApplyEdit()
        _oriTemplateName = _curTemplateName
        _oriDescription = _curDescription
        _oriTemplateStatus = _curTemplateStatus
        _oriCustomerOutputExtension = _curCustomerOutputExtension
        _oriEncryptCustomerOutputFile = _curEncryptCustomerOutputFile
        _oriDelimiterCharacter = _curDelimiterCharacter
        _oriNewLine = _curNewLine
        _oriDelimiter = _curDelimiter
        _oriOutputFormat = _curOutputFormat
    End Sub
    Public Overrides Sub BeginEdit()
        _oldTemplateName = _curTemplateName
        _oldDescription = _curDescription
        _oldTemplateStatus = _curTemplateStatus
        _oldCustomerOutputExtension = _curCustomerOutputExtension
        _oldEncryptCustomerOutputFile = _curEncryptCustomerOutputFile
        _oldDelimiterCharacter = _curDelimiterCharacter
        _oldNewLine = _curNewLine
        _oldDelimiter = _curDelimiter
        _oldOutputFormat = _curOutputFormat
    End Sub
    Public Overrides Sub CancelEdit()
        _curTemplateName = _oldTemplateName
        _curDescription = _oldDescription
        _curTemplateStatus = _oldTemplateStatus
        _curCustomerOutputExtension = _oldCustomerOutputExtension
        _curEncryptCustomerOutputFile = _oldEncryptCustomerOutputFile
        _curDelimiterCharacter = _oldDelimiterCharacter
        _curNewLine = _oldNewLine
        _curDelimiter = _oldDelimiter
        _curOutputFormat = _oldOutputFormat
    End Sub

#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curTemplateName = ReadXMLElement(reader, "_curTemplateName")
            _oldTemplateName = ReadXMLElement(reader, "_oldTemplateName")
            _oriTemplateName = ReadXMLElement(reader, "_oriTemplateName")
            _curDescription = ReadXMLElement(reader, "_curDescription")
            _oldDescription = ReadXMLElement(reader, "_oldDescription")
            _oriDescription = ReadXMLElement(reader, "_oriDescription")
            _curTemplateStatus = ReadXMLElement(reader, "_curTemplateStatus")
            _oldTemplateStatus = ReadXMLElement(reader, "_oldTemplateStatus")
            _oriTemplateStatus = ReadXMLElement(reader, "_oriTemplateStatus")
            _curCustomerOutputExtension = ReadXMLElement(reader, "_curCustomerOutputExtension")
            _oldCustomerOutputExtension = ReadXMLElement(reader, "_oldCustomerOutputExtension")
            _oriCustomerOutputExtension = ReadXMLElement(reader, "_oriCustomerOutputExtension")
            _curEncryptCustomerOutputFile = ReadXMLElement(reader, "_curEncryptCustomerOutputFile")
            _oldEncryptCustomerOutputFile = ReadXMLElement(reader, "_oldEncryptCustomerOutputFile")
            _oriEncryptCustomerOutputFile = ReadXMLElement(reader, "_oriEncryptCustomerOutputFile")
            _curDelimiterCharacter = ReadXMLElement(reader, "_curDelimiterCharacter")
            _oldDelimiterCharacter = ReadXMLElement(reader, "_oldDelimiterCharacter")
            _oriDelimiterCharacter = ReadXMLElement(reader, "_oriDelimiterCharacter")

            _curNewLine = ReadXMLElement(reader, "_curNewLine")
            _oldNewLine = ReadXMLElement(reader, "_oldNewLine")
            _oriNewLine = ReadXMLElement(reader, "_oriNewLine")
            _curDelimiter = ReadXMLElement(reader, "_curDelimiter")
            _oldDelimiter = ReadXMLElement(reader, "_oldDelimiter")
            _oriDelimiter = ReadXMLElement(reader, "_oriDelimiter")

            _curMasterTemplateFixedDetailCollection.ReadXml(reader)
            _curOutputFormat = ReadXMLElement(reader, "_curOutputFormat")
            _oldOutputFormat = ReadXMLElement(reader, "_oldOutputFormat")
            _oriOutputFormat = ReadXMLElement(reader, "_oriOutputFormat")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curTemplateName", _curTemplateName)
        WriteXmlElement(writer, "_oldTemplateName", _oldTemplateName)
        WriteXmlElement(writer, "_oriTemplateName", _oriTemplateName)
        WriteXmlElement(writer, "_curDescription", _curDescription)
        WriteXmlElement(writer, "_oldDescription", _oldDescription)
        WriteXmlElement(writer, "_oriDescription", _oriDescription)
        WriteXmlElement(writer, "_curTemplateStatus", _curTemplateStatus)
        WriteXmlElement(writer, "_oldTemplateStatus", _oldTemplateStatus)
        WriteXmlElement(writer, "_oriTemplateStatus", _oriTemplateStatus)
        WriteXmlElement(writer, "_curCustomerOutputExtension", _curCustomerOutputExtension)
        WriteXmlElement(writer, "_oldCustomerOutputExtension", _oldCustomerOutputExtension)
        WriteXmlElement(writer, "_oriCustomerOutputExtension", _oriCustomerOutputExtension)
        WriteXmlElement(writer, "_curEncryptCustomerOutputFile", _curEncryptCustomerOutputFile)
        WriteXmlElement(writer, "_oldEncryptCustomerOutputFile", _oldEncryptCustomerOutputFile)
        WriteXmlElement(writer, "_oriEncryptCustomerOutputFile", _oriEncryptCustomerOutputFile)
        WriteXmlElement(writer, "_curDelimiterCharacter", _curDelimiterCharacter)
        WriteXmlElement(writer, "_oldDelimiterCharacter", _oldDelimiterCharacter)
        WriteXmlElement(writer, "_oriDelimiterCharacter", _oriDelimiterCharacter)

        WriteXmlElement(writer, "_curNewLine", _curNewLine)
        WriteXmlElement(writer, "_oldNewLine", _oldNewLine)
        WriteXmlElement(writer, "_oriNewLine", _oriNewLine)
        WriteXmlElement(writer, "_curDelimiter", _curDelimiter)
        WriteXmlElement(writer, "_oldDelimiter", _oldDelimiter)
        WriteXmlElement(writer, "_oriDelimiter", _oriDelimiter)

        _curMasterTemplateFixedDetailCollection.WriteXml(writer)
        WriteXmlElement(writer, "_curOutputFormat", _curOutputFormat)
        WriteXmlElement(writer, "_oldOutputFormat", _oldOutputFormat)
        WriteXmlElement(writer, "_oriOutputFormat", _oriOutputFormat)
    End Sub
#End Region
#Region "  Save and Loading  "

    Private _isDraft As Boolean

    Public Property IsDraft() As Boolean
        Get
            Return _isDraft
        End Get
        Set(ByVal value As Boolean)
            _isDraft = value
        End Set
    End Property

    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(MasterTemplateFixed))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub

    Public Sub SaveToFile2(ByVal filename As String, ByVal fileToBeDeleted As String) ', Optional ByVal fileToBeDeleted As String = "")
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If

        Dim masterTemplateList As New MasterTemplateList

        Dim fileTemplate As New BTMU.MAGIC.Common.MagicFileTemplate


        Dim serializerMasterTemplateFixed As New XmlSerializer(GetType(MasterTemplateFixed))
        Dim serializerMasterTemplateList As New XmlSerializer(GetType(MasterTemplateList))
        Dim serializerFileTemplate As New XmlSerializer(GetType(BTMU.MAGIC.Common.MagicFileTemplate))

        Dim stream As New MemoryStream

        If fileToBeDeleted.Trim.Length > 0 Then File.Delete(fileToBeDeleted)

        Dim streamFile As FileStream = File.Open(filename, FileMode.Create)

        masterTemplateList.TemplateName = TemplateName
        masterTemplateList.Description = Description
        masterTemplateList.OutputFormat = OutputFormat
        masterTemplateList.EncryptCustomerOutputFile = EncryptCustomerOutputFile
        masterTemplateList.TemplateStatus = TemplateStatus
        masterTemplateList.IsDraft = IsDraft
        masterTemplateList.IsFixed = True

        serializerMasterTemplateFixed.Serialize(stream, Me)
        fileTemplate.DetailValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
        stream.Close()

        stream = New MemoryStream
        serializerMasterTemplateList.Serialize(stream, masterTemplateList)
        fileTemplate.ListValue = AESEncrypt(System.Text.Encoding.UTF8.GetString(stream.ToArray()))
        stream.Close()

        serializerFileTemplate.Serialize(streamFile, fileTemplate)
        streamFile.Close()
    End Sub

    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As MasterTemplateFixed
        Dim serializer As New XmlSerializer(GetType(MasterTemplateFixed))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), MasterTemplateFixed)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub

    'Public Function LoadFromFile2(ByVal filename As String) As MasterTemplateFixed
    '    Dim lstDetail As New MasterTemplateFixedCollection
    '    Dim serializer As New XmlSerializer(GetType(MasterTemplateFixed))
    '    Dim content As String
    '    Dim doc As XPathDocument = New XPathDocument(filename)
    '    Dim nav As XPathNavigator = doc.CreateNavigator()
    '    Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/DetailValue")
    '    Dim item As New MasterTemplateFixed
    '    Iterator.MoveNext()
    '    content = Iterator.Current.Value

    '    Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
    '    stream.Position = 0

    '    item = CType(serializer.Deserialize(stream), MasterTemplateFixed)
    '    stream.Close()

    '    Return item
    'End Function

    Public Function LoadFromFile2(ByVal filename As String) As MasterTemplateFixed
        Dim lstDetail As New MasterTemplateNonFixedCollection

        Dim serializer As New XmlSerializer(GetType(MasterTemplateFixed))
        Dim content As String
        Dim doc As XPathDocument = New XPathDocument(filename)
        Dim nav As XPathNavigator = doc.CreateNavigator()
        Dim Iterator As XPathNodeIterator = nav.Select("/MagicFileTemplate/DetailValue")
        Dim item As New MasterTemplateFixed
        Iterator.MoveNext()
        content = Iterator.Current.Value

        Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(AESDecrypt(content)))
        stream.Position = 0

        Dim xmlSetting As New System.Xml.XmlReaderSettings()
        xmlSetting.IgnoreWhitespace = False
        Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(stream, xmlSetting)

        item = CType(serializer.Deserialize(xmlReader), MasterTemplateFixed)
        stream.Close()
        xmlReader.Close()

        Return item

    End Function


    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As MasterTemplateFixed = DirectCast(obj, MasterTemplateFixed)
        _curTemplateName = objE._curTemplateName
        _oldTemplateName = objE._oldTemplateName
        _oriTemplateName = objE._oriTemplateName
        _curDescription = objE._curDescription
        _oldDescription = objE._oldDescription
        _oriDescription = objE._oriDescription
        _curTemplateStatus = objE._curTemplateStatus
        _oldTemplateStatus = objE._oldTemplateStatus
        _oriTemplateStatus = objE._oriTemplateStatus
        _curCustomerOutputExtension = objE._curCustomerOutputExtension
        _oldCustomerOutputExtension = objE._oldCustomerOutputExtension
        _oriCustomerOutputExtension = objE._oriCustomerOutputExtension
        _curEncryptCustomerOutputFile = objE._curEncryptCustomerOutputFile
        _oldEncryptCustomerOutputFile = objE._oldEncryptCustomerOutputFile
        _oriEncryptCustomerOutputFile = objE._oriEncryptCustomerOutputFile
        _curDelimiterCharacter = objE._curDelimiterCharacter
        _oldDelimiterCharacter = objE._oldDelimiterCharacter
        _oriDelimiterCharacter = objE._oriDelimiterCharacter

        _curNewLine = objE._curNewLine
        _oldNewLine = objE._oldNewLine
        _oriNewLine = objE._oriNewLine
        _curDelimiter = objE._curDelimiter
        _oldDelimiter = objE._oldDelimiter
        _oriDelimiter = objE._oriDelimiter

        _curMasterTemplateFixedDetailCollection = objE._curMasterTemplateFixedDetailCollection
        _curOutputFormat = objE._curOutputFormat
        _oldOutputFormat = objE._oldOutputFormat
        _oriOutputFormat = objE._oriOutputFormat
    End Sub
    Public Overrides Sub Validate()
        ValidationEngine.Validate()
        Dim childMasterTemplateFixedDetail As MasterTemplateFixedDetail
        For Each childMasterTemplateFixedDetail In _curMasterTemplateFixedDetailCollection
            childMasterTemplateFixedDetail.Validate()
        Next
    End Sub
    Public Overrides ReadOnly Property IsValid() As Boolean
        Get
            If Not ValidationEngine.IsValid Then
                Return False
            End If
            Dim childMasterTemplateFixedDetail As MasterTemplateFixedDetail
            For Each childMasterTemplateFixedDetail In _curMasterTemplateFixedDetailCollection
                If Not childMasterTemplateFixedDetail.IsValid Then
                    Return False
                End If
            Next
            Return True
        End Get
    End Property
#End Region

    Public Overrides ReadOnly Property IsDirty() As Boolean
        Get
            'If _isDirty Then
            '    Return _isDirty
            'Else
            For Each item As MasterTemplateFixedDetail In MasterTemplateFixedDetailCollection
                If item.IsDirty Then
                    Return item.IsDirty
                    Exit Property
                End If
            Next
            Return MyBase.IsDirty
            'End If
        End Get
    End Property
End Class

''' <summary>
''' Collection of Snapshot of Master Template Fixed
''' </summary>
''' <remarks></remarks>
Partial Public Class MasterTemplateFixedCollection
    Inherits System.ComponentModel.BindingList(Of MasterTemplateFixed)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixed)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixed)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixed)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As MasterTemplateFixed
        reader.ReadStartElement("MasterTemplateFixedCollection")
        While reader.LocalName = "MasterTemplateFixed"
            child = New MasterTemplateFixed
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub
    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As MasterTemplateFixed
        writer.WriteStartElement("MasterTemplateFixedCollection")
        For Each child In Me
            writer.WriteStartElement("MasterTemplateFixed")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub
End Class
