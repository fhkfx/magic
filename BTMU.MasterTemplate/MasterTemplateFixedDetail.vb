Imports System.IO
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

''' <summary>
''' This class encapsulates Master Template Fixed for detail
''' </summary>
''' <remarks></remarks>
<Serializable()> _
Partial Public Class MasterTemplateFixedDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Public Sub New()
        Initialize()
    End Sub

#Region " Private Fields "

    Private _curPosition As Integer
    Private _oldPosition As Integer
    Private _oriPosition As Integer
    Private _curFieldName As String
    Private _oldFieldName As String
    Private _oriFieldName As String
    Private _curDataType As String
    Private _oldDataType As String
    Private _oriDataType As String
    Private _curDateFormat As String
    Private _oldDateFormat As String
    Private _oriDateFormat As String
    Private _curDataLength As Nullable(Of Integer)
    Private _oldDataLength As Nullable(Of Integer)
    Private _oriDataLength As Nullable(Of Integer)
    Private _curMandatory As Boolean
    Private _oldMandatory As Boolean
    Private _oriMandatory As Boolean

    Private _curCarriageReturn As Boolean
    Private _oldCarriageReturn As Boolean
    Private _oriCarriageReturn As Boolean

    Private _curDefaultValue As String
    Private _oldDefaultValue As String
    Private _oriDefaultValue As String

    Private _curIncludeDecimal As String
    Private _oldIncludeDecimal As String
    Private _oriIncludeDecimal As String
    Private _curDecimalSeparator As String
    Private _oldDecimalSeparator As String
    Private _oriDecimalSeparator As String

    Private _curDataSample As String
    Private _oldDataSample As String
    Private _oriDataSample As String
    Private _curMapSeparator As String
    Private _oldMapSeparator As String
    Private _oriMapSeparator As String
    Private _curHeader As Boolean
    Private _oldHeader As Boolean
    Private _oriHeader As Boolean
    Private _curDetail As String
    Private _oldDetail As String
    Private _oriDetail As String
    Private _curTrailer As Boolean
    Private _oldTrailer As Boolean
    Private _oriTrailer As Boolean
    Private _curCreatedBy As String
    Private _curCreatedDate As DateTime
    Private _curModifiedBy As String
    Private _curModifiedDate As DateTime
    Private _oriModifiedBy As String
    Private _oriModifiedDate As DateTime
#End Region
#Region " Public Property "
    Public Property Position() As Integer
        Get
            Return _curPosition
        End Get
        Set(ByVal value As Integer)
            If _curPosition <> value Then
                _curPosition = value
                OnPropertyChanged("Position")
            End If
        End Set
    End Property
    Public Property FieldName() As String
        Get
            Return _curFieldName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curFieldName <> value Then
                _curFieldName = value
                OnPropertyChanged("FieldName")
            End If
        End Set
    End Property
    Public Property DataType() As String
        Get
            Return _curDataType
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDataType <> value Then
                _curDataType = value
                OnPropertyChanged("DataType")
            End If
        End Set
    End Property
    Public Property DateFormat() As String
        Get
            Return _curDateFormat
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDateFormat <> value Then
                _curDateFormat = value
                OnPropertyChanged("DateFormat")
            End If
        End Set
    End Property
    Public Property DataLength() As Nullable(Of Integer)
        Get
            Return _curDataLength
        End Get
        Set(ByVal value As Nullable(Of Integer))
            If _curDataLength.HasValue And value.HasValue Then
                If _curDataLength.Value <> value.Value Then
                    _curDataLength = value
                    OnPropertyChanged("DataLength")
                End If
            ElseIf Not _curDataLength.HasValue And Not value.HasValue Then
                'Do nothing
            Else
                _curDataLength = value
                OnPropertyChanged("DataLength")
            End If
        End Set
    End Property
    Public Property Mandatory() As Boolean
        Get
            Return _curMandatory
        End Get
        Set(ByVal value As Boolean)
            If _curMandatory <> value Then
                _curMandatory = value
                OnPropertyChanged("Mandatory")
            End If
        End Set
    End Property

    Public Property CarriageReturn() As Boolean
        Get
            Return _curCarriageReturn
        End Get
        Set(ByVal value As Boolean)
            If _curCarriageReturn <> value Then
                _curCarriageReturn = value
                OnPropertyChanged("CarriageReturn")
            End If
        End Set
    End Property

    Public Property DefaultValue() As String
        Get
            Return _curDefaultValue
        End Get
        Set(ByVal value As String)
            'If Not value Is Nothing Then value = value.Trim
            If _curDefaultValue <> value Then
                _curDefaultValue = value
                OnPropertyChanged("DefaultValue")
            End If
        End Set
    End Property

    Public Property IncludeDecimal() As String
        Get
            Return _curIncludeDecimal
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curIncludeDecimal <> value Then
                _curIncludeDecimal = value
                OnPropertyChanged("IncludeDecimal")
            End If
        End Set
    End Property
    Public Property DecimalSeparator() As String
        Get
            Return _curDecimalSeparator
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDecimalSeparator <> value Then
                _curDecimalSeparator = value
                OnPropertyChanged("DecimalSeparator")
            End If
        End Set
    End Property
   
    Public Property DataSample() As String
        Get
            Return _curDataSample
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDataSample <> value Then
                _curDataSample = value
                OnPropertyChanged("DataSample")
            End If
        End Set
    End Property
    Public Property MapSeparator() As String
        Get
            Return _curMapSeparator
        End Get
        Set(ByVal value As String)
            If _curMapSeparator <> value Then
                _curMapSeparator = value
                OnPropertyChanged("MapSeparator")
            End If
        End Set
    End Property
    Public Property Header() As Boolean
        Get
            Return _curHeader
        End Get
        Set(ByVal value As Boolean)
            If _curHeader <> value Then
                _curHeader = value
                OnPropertyChanged("Header")
            End If
        End Set
    End Property
    Public Property Detail() As String
        Get
            Return _curDetail
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then value = value.Trim
            If _curDetail <> value Then
                _curDetail = value
                OnPropertyChanged("Detail")
            End If
        End Set
    End Property
    Public Property Trailer() As Boolean
        Get
            Return _curTrailer
        End Get
        Set(ByVal value As Boolean)
            If _curTrailer <> value Then
                _curTrailer = value
                OnPropertyChanged("Trailer")
            End If
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _curCreatedBy
        End Get
    End Property
    Public ReadOnly Property CreatedDate() As DateTime
        Get
            Return _curCreatedDate
        End Get
    End Property
    Public ReadOnly Property ModifiedBy() As String
        Get
            Return _curModifiedBy
        End Get
    End Property
    Public ReadOnly Property ModifiedDate() As DateTime
        Get
            Return _curModifiedDate
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedBy() As String
        Get
            Return _oriModifiedBy
        End Get
    End Property
    Public ReadOnly Property OriginalModifiedDate() As DateTime
        Get
            Return _oriModifiedDate
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Apply the changes made to this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub ApplyEdit()
        _oriPosition = _curPosition
        _oriFieldName = _curFieldName
        _oriDataType = _curDataType
        _oriDateFormat = _curDateFormat
        _oriDataLength = _curDataLength
        _oriMandatory = _curMandatory

        _oriCarriageReturn = _curCarriageReturn

        _oriDefaultValue = _curDefaultValue
        _oriIncludeDecimal = _curIncludeDecimal
        _oriDecimalSeparator = _curDecimalSeparator
        _oriDataSample = _curDataSample
        _oriMapSeparator = _curMapSeparator
        _oriHeader = _curHeader
        _oriDetail = _curDetail
        _oriTrailer = _curTrailer

    End Sub
    ''' <summary>
    ''' Begin making changes to the fields of this object
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub BeginEdit()
        _oldPosition = _curPosition
        _oldFieldName = _curFieldName
        _oldDataType = _curDataType
        _oldDateFormat = _curDateFormat
        _oldDataLength = _curDataLength
        _oldMandatory = _curMandatory

        _oldCarriageReturn = _curCarriageReturn

        _oldDefaultValue = _curDefaultValue
        _oriIncludeDecimal = _curIncludeDecimal
        _oriDecimalSeparator = _curDecimalSeparator
        _oldDataSample = _curDataSample
        _oldMapSeparator = _curMapSeparator
        _oldHeader = _curHeader
        _oldDetail = _curDetail
        _oldTrailer = _curTrailer
    End Sub
    ''' <summary>
    ''' Cancel the changes made to the fields
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CancelEdit()
        _curPosition = _oldPosition
        _curFieldName = _oldFieldName
        _curDataType = _oldDataType
        _curDateFormat = _oldDateFormat
        _curDataLength = _oldDataLength
        _curMandatory = _oldMandatory

        _curCarriageReturn = _oldCarriageReturn

        _curDefaultValue = _oldDefaultValue
        _curIncludeDecimal = _oriIncludeDecimal
        _curDecimalSeparator = _oriDecimalSeparator
        _curDataSample = _oldDataSample
        _curMapSeparator = _oldMapSeparator
        _curHeader = _oldHeader
        _curDetail = _oldDetail
        _curTrailer = _oldTrailer
    End Sub

#Region " Serialization "
    Protected Overrides Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            MyBase.ReadXml(reader)
            _curPosition = ReadXMLElement(reader, "_curPosition")
            _oldPosition = ReadXMLElement(reader, "_oldPosition")
            _oriPosition = ReadXMLElement(reader, "_oriPosition")
            _curFieldName = ReadXMLElement(reader, "_curFieldName")
            _oldFieldName = ReadXMLElement(reader, "_oldFieldName")
            _oriFieldName = ReadXMLElement(reader, "_oriFieldName")
            _curDataType = ReadXMLElement(reader, "_curDataType")
            _oldDataType = ReadXMLElement(reader, "_oldDataType")
            _oriDataType = ReadXMLElement(reader, "_oriDataType")
            _curDateFormat = ReadXMLElement(reader, "_curDateFormat")
            _oldDateFormat = ReadXMLElement(reader, "_oldDateFormat")
            _oriDateFormat = ReadXMLElement(reader, "_oriDateFormat")
            _curDataLength = ReadXMLElement(reader, "_curDataLength")
            _oldDataLength = ReadXMLElement(reader, "_oldDataLength")
            _oriDataLength = ReadXMLElement(reader, "_oriDataLength")
            _curMandatory = ReadXMLElement(reader, "_curMandatory")
            _oldMandatory = ReadXMLElement(reader, "_oldMandatory")
            _oriMandatory = ReadXMLElement(reader, "_oriMandatory")

            Dim temp As String
            temp = ReadXMLElement(reader, "_curCarriageReturn")
            If temp Is Nothing Then
                _curCarriageReturn = False
            Else
                _curCarriageReturn = temp
            End If
            temp = ReadXMLElement(reader, "_oldCarriageReturn")
            If temp Is Nothing Then
                _oldCarriageReturn = False
            Else
                _oldCarriageReturn = temp
            End If
            temp = ReadXMLElement(reader, "_oriCarriageReturn")
            If temp Is Nothing Then
                _oriCarriageReturn = False
            Else
                _oriCarriageReturn = temp
            End If

            _curDefaultValue = ReadXMLElement(reader, "_curDefaultValue")
            _oldDefaultValue = ReadXMLElement(reader, "_oldDefaultValue")
            _oriDefaultValue = ReadXMLElement(reader, "_oriDefaultValue")

            _curIncludeDecimal = ReadXMLElement(reader, "_curIncludeDecimal")
            _oldIncludeDecimal = ReadXMLElement(reader, "_oldIncludeDecimal")
            _oriIncludeDecimal = ReadXMLElement(reader, "_oriIncludeDecimal")

            _curDecimalSeparator = ReadXMLElement(reader, "_curDecimalSeparator")
            _oldDecimalSeparator = ReadXMLElement(reader, "_oldDecimalSeparator")
            _oriDecimalSeparator = ReadXMLElement(reader, "_oriDecimalSeparator")

            _curDataSample = ReadXMLElement(reader, "_curDataSample")
            _oldDataSample = ReadXMLElement(reader, "_oldDataSample")
            _oriDataSample = ReadXMLElement(reader, "_oriDataSample")
            _curMapSeparator = ReadXMLElement(reader, "_curMapSeparator")
            _oldMapSeparator = ReadXMLElement(reader, "_oldMapSeparator")
            _oriMapSeparator = ReadXMLElement(reader, "_oriMapSeparator")
            _curHeader = ReadXMLElement(reader, "_curHeader")
            _oldHeader = ReadXMLElement(reader, "_oldHeader")
            _oriHeader = ReadXMLElement(reader, "_oriHeader")
            _curDetail = ReadXMLElement(reader, "_curDetail")
            _oldDetail = ReadXMLElement(reader, "_oldDetail")
            _oriDetail = ReadXMLElement(reader, "_oriDetail")
            _curTrailer = ReadXMLElement(reader, "_curTrailer")
            _oldTrailer = ReadXMLElement(reader, "_oldTrailer")
            _oriTrailer = ReadXMLElement(reader, "_oriTrailer")
            reader.ReadEndElement()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        MyBase.WriteXml(writer)
        WriteXmlElement(writer, "_curPosition", _curPosition)
        WriteXmlElement(writer, "_oldPosition", _oldPosition)
        WriteXmlElement(writer, "_oriPosition", _oriPosition)
        WriteXmlElement(writer, "_curFieldName", _curFieldName)
        WriteXmlElement(writer, "_oldFieldName", _oldFieldName)
        WriteXmlElement(writer, "_oriFieldName", _oriFieldName)
        WriteXmlElement(writer, "_curDataType", _curDataType)
        WriteXmlElement(writer, "_oldDataType", _oldDataType)
        WriteXmlElement(writer, "_oriDataType", _oriDataType)
        WriteXmlElement(writer, "_curDateFormat", _curDateFormat)
        WriteXmlElement(writer, "_oldDateFormat", _oldDateFormat)
        WriteXmlElement(writer, "_oriDateFormat", _oriDateFormat)
        WriteXmlElement(writer, "_curDataLength", _curDataLength)
        WriteXmlElement(writer, "_oldDataLength", _oldDataLength)
        WriteXmlElement(writer, "_oriDataLength", _oriDataLength)
        WriteXmlElement(writer, "_curMandatory", _curMandatory)
        WriteXmlElement(writer, "_oldMandatory", _oldMandatory)
        WriteXmlElement(writer, "_oriMandatory", _oriMandatory)

        WriteXmlElement(writer, "_curCarriageReturn", _curCarriageReturn)
        WriteXmlElement(writer, "_oldCarriageReturn", _oldCarriageReturn)
        WriteXmlElement(writer, "_oriCarriageReturn", _oriCarriageReturn)

        WriteXmlElement(writer, "_curDefaultValue", _curDefaultValue)
        WriteXmlElement(writer, "_oldDefaultValue", _oldDefaultValue)
        WriteXmlElement(writer, "_oriDefaultValue", _oriDefaultValue)

        WriteXmlElement(writer, "_curIncludeDecimal", _curIncludeDecimal)
        WriteXmlElement(writer, "_oldIncludeDecimal", _oldIncludeDecimal)
        WriteXmlElement(writer, "_oriIncludeDecimal", _oriIncludeDecimal)

        WriteXmlElement(writer, "_curDecimalSeparator", _curDecimalSeparator)
        WriteXmlElement(writer, "_oldDecimalSeparator", _oldDecimalSeparator)
        WriteXmlElement(writer, "_oriDecimalSeparator", _oriDecimalSeparator)

        WriteXmlElement(writer, "_curDataSample", _curDataSample)
        WriteXmlElement(writer, "_oldDataSample", _oldDataSample)
        WriteXmlElement(writer, "_oriDataSample", _oriDataSample)
        WriteXmlElement(writer, "_curMapSeparator", _curMapSeparator)
        WriteXmlElement(writer, "_oldMapSeparator", _oldMapSeparator)
        WriteXmlElement(writer, "_oriMapSeparator", _oriMapSeparator)
        WriteXmlElement(writer, "_curHeader", _curHeader)
        WriteXmlElement(writer, "_oldHeader", _oldHeader)
        WriteXmlElement(writer, "_oriHeader", _oriHeader)
        WriteXmlElement(writer, "_curDetail", _curDetail)
        WriteXmlElement(writer, "_oldDetail", _oldDetail)
        WriteXmlElement(writer, "_oriDetail", _oriDetail)
        WriteXmlElement(writer, "_curTrailer", _curTrailer)
        WriteXmlElement(writer, "_oldTrailer", _oldTrailer)
        WriteXmlElement(writer, "_oriTrailer", _oriTrailer)
    End Sub
#End Region
#Region "  Save and Loading  "
    ''' <summary>
    ''' Saves this object to given file
    ''' </summary>
    ''' <param name="filename">Name of File</param>
    ''' <remarks></remarks>
    Public Sub SaveToFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to save child object")
        End If
        Dim serializer As New XmlSerializer(GetType(MasterTemplateFixedDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()
    End Sub
    ''' <summary>
    ''' Loads this object from given file
    ''' </summary>
    ''' <param name="filename">Name of file</param>
    ''' <remarks></remarks>
    Public Sub LoadFromFile(ByVal filename As String)
        If IsChild Then
            Throw New Exception("Unable to load child object")
        End If
        Dim objE As MasterTemplateFixedDetail
        Dim serializer As New XmlSerializer(GetType(MasterTemplateFixedDetail))
        Dim stream As FileStream = File.Open(filename, FileMode.Open)
        objE = DirectCast(serializer.Deserialize(stream), MasterTemplateFixedDetail)
        stream.Close()
        Clone(objE)
        Validate()
    End Sub
    Protected Overrides Sub Clone(ByVal obj As BusinessBase)
        MyBase.Clone(obj)
        Dim objE As MasterTemplateFixedDetail = DirectCast(obj, MasterTemplateFixedDetail)
        _curPosition = objE._curPosition
        _oldPosition = objE._oldPosition
        _oriPosition = objE._oriPosition
        _curFieldName = objE._curFieldName
        _oldFieldName = objE._oldFieldName
        _oriFieldName = objE._oriFieldName
        _curDataType = objE._curDataType
        _oldDataType = objE._oldDataType
        _oriDataType = objE._oriDataType
        _curDateFormat = objE._curDateFormat
        _oldDateFormat = objE._oldDateFormat
        _oriDateFormat = objE._oriDateFormat
        _curDataLength = objE._curDataLength
        _oldDataLength = objE._oldDataLength
        _oriDataLength = objE._oriDataLength
        _curMandatory = objE._curMandatory
        _oldMandatory = objE._oldMandatory
        _oriMandatory = objE._oriMandatory

        _curCarriageReturn = objE._curCarriageReturn
        _oldCarriageReturn = objE._oldCarriageReturn
        _oriCarriageReturn = objE._oriCarriageReturn

        _curDefaultValue = objE._curDefaultValue
        _oldDefaultValue = objE._oldDefaultValue
        _oriDefaultValue = objE._oriDefaultValue

        _curIncludeDecimal = objE._curIncludeDecimal
        _oldIncludeDecimal = objE._oldIncludeDecimal
        _oriIncludeDecimal = objE._oriIncludeDecimal
        _curDecimalSeparator = objE._curDecimalSeparator
        _oldDecimalSeparator = objE._oldDecimalSeparator
        _oriDecimalSeparator = objE._oriDecimalSeparator

        _curDataSample = objE._curDataSample
        _oldDataSample = objE._oldDataSample
        _oriDataSample = objE._oriDataSample
        _curMapSeparator = objE._curMapSeparator
        _oldMapSeparator = objE._oldMapSeparator
        _oriMapSeparator = objE._oriMapSeparator
        _curHeader = objE._curHeader
        _oldHeader = objE._oldHeader
        _oriHeader = objE._oriHeader
        _curDetail = objE._curDetail
        _oldDetail = objE._oldDetail
        _oriDetail = objE._oriDetail
        _curTrailer = objE._curTrailer
        _oldTrailer = objE._oldTrailer
        _oriTrailer = objE._oriTrailer
    End Sub
#End Region
End Class

''' <summary>
''' Collection of Master Template Fixed for detail
''' </summary>
''' <remarks></remarks>
Partial Public Class MasterTemplateFixedDetailCollection
    Inherits BusinessBaseCollection(Of MasterTemplateFixedDetail)
    Implements System.Xml.Serialization.IXmlSerializable

    Private _bndListView As BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixedDetail)

    Public Sub New()
        _bndListView = New BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixedDetail)(Me)
    End Sub

    Public ReadOnly Property DefaultView() As BTMU.MAGIC.Common.BindingListView(Of MasterTemplateFixedDetail)
        Get
            Return _bndListView
        End Get
    End Property

    Friend Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function
    'Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
    '    Dim child As MasterTemplateFixedDetail
    '    reader.ReadStartElement("MasterTemplateFixedDetailCollection")
    '    While reader.LocalName = "MasterTemplateFixedDetail"
    '        child = New MasterTemplateFixedDetail
    '        DirectCast(child, IXmlSerializable).ReadXml(reader)
    '        Me.Add(child)
    '    End While
    '    If Me.Count > 0 Then reader.ReadEndElement()
    'End Sub
    Friend Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As MasterTemplateFixedDetail
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement("MasterTemplateFixedDetailCollection")
        While reader.NodeType = System.Xml.XmlNodeType.Whitespace
            reader.Skip()
        End While
        While reader.LocalName = "MasterTemplateFixedDetail"
            child = New MasterTemplateFixedDetail
            DirectCast(child, IXmlSerializable).ReadXml(reader)
            Me.Add(child)
            While reader.NodeType = System.Xml.XmlNodeType.Whitespace
                reader.Skip()
            End While
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub

    Friend Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As MasterTemplateFixedDetail
        writer.WriteStartElement("MasterTemplateFixedDetailCollection")
        For Each child In Me
            writer.WriteStartElement("MasterTemplateFixedDetail")
            DirectCast(child, IXmlSerializable).WriteXml(writer)
            writer.WriteEndElement()
        Next
        writer.WriteEndElement()
    End Sub




End Class
