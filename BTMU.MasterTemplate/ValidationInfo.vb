Imports System
Imports System.IO
Imports System.Xml.Serialization
Imports BTMU.MAGIC.Common

Partial Public Class MasterTemplateNonFixedDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub OnPropertyChanged(ByVal propertyName As String)
        MyBase.OnPropertyChanged(propertyName)
        If propertyName = "DataType" Then
            OnPropertyChanged("DateFormat")
            OnPropertyChanged("DataSample")
            OnPropertyChanged("DefaultValue")
        ElseIf propertyName = "DataLength" Then
            OnPropertyChanged("DataSample")
            OnPropertyChanged("DefaultValue")
            'ElseIf propertyName = "DateFormat" Then
            'OnPropertyChanged("DataLength")
        End If
    End Sub
    Protected Overrides Sub Initialize()
        Dim DataLengthMinValue As Integer = 1

        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FieldName", "Field Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DataType", "Data Type"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DataLength", "Data Length"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Detail", "Detail"))

        'Field Name must begin with an alphabet
        ValidationEngine.AddValidationRule(AddressOf E01000040, New Common.ValidationArgs("FieldName", "Field Name"))

        'If Data Type = "DateTime" then Date Format is required.
        ValidationEngine.AddValidationRule(AddressOf E01000050, New Common.ValidationArgs("DateFormat", "Date Format"))
        'If Data Type <> "DateTime" then Date Format should be empty.
        ValidationEngine.AddValidationRule(AddressOf E01000060, New Common.ValidationArgs("DateFormat", "Date Format"))
        'Data Length should be greater than 0..
        ValidationEngine.AddValidationRule(AddressOf E01000070, New Common.ValidationArgs("DataLength", "Data Length"))

        'If Data Type = "DateTime" and Date Format has value, Data Length should same as date format. 
        'ValidationEngine.AddValidationRule(AddressOf E01000090, New Common.ValidationArgs("DataLength", "Data Length"))

        'Each of the Default value should not exceed the Data Length
        ValidationEngine.AddValidationRule(AddressOf E01000100, New Common.ValidationArgs("DefaultValue", "Default Value"))
        ' Each of the Default value should be the same as the DataType.
        ValidationEngine.AddValidationRule(AddressOf E01000120, New Common.ValidationArgs("DefaultValue", "Default Value"))

        'Each of the Data Sample should not exceed the Data Length
        ValidationEngine.AddValidationRule(AddressOf E01000100, New Common.ValidationArgs("DataSample", "Data Sample"))
        ' Each of the Default value should be the same as the DataType.
        ValidationEngine.AddValidationRule(AddressOf E01000120, New Common.ValidationArgs("DataSample", "Data Sample"))

        '(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("DefaultValue", "[;@^]+?", "Default Value", "(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','", True))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("DataSample", "[;@^]+?", "Data Sample", "(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','", True))

        ValidationEngine.Validate()
    End Sub

    'Field Name must begin with an alphabet
    Public Function E01000040(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Not IsNothingOrEmptyString(value) And Not (UCase(value.Substring(0, 1)) >= "A" And UCase(value.Substring(0, 1)) <= "Z") Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000040"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type = "DateTime" then Date Format is required.
    Public Function E01000050(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.DataType = "DateTime" And IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000050"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type <> "DateTime" then Date Format should be empty.
    Public Function E01000060(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            If Me.DataType <> "DateTime" And Not IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000060"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'Data Length should be greater than 0.
    Public Function E01000070(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = DataLength
            If Not IsNothingOrEmptyString(value) And value <= 0 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000070"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'remove by kaler on 20090827
    'If Data Type = "DateTime" and Date Format has value, Data Length should same as date format.
    'Public Function E01000090(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
    '    Try
    '        Dim value As String = DataLength
    '        If Me.DataType = "DateTime" AndAlso Me.DateFormat.ToString.Trim <> "" AndAlso value <> Me.DateFormat.Length Then
    '            e.Description = _
    '                        String.Format(MsgReader.GetString("E01000090"), ValidationArgs.GetFriendlyPropertyName(e))
    '            Return False
    '        Else
    '            Return True
    '        End If
    '    Catch ex As Exception
    '        Return True
    '    End Try
    'End Function
    'end remove

    'Each of the Data Sample should not exceed the Data Length
    Public Function E01000100(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim curValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            'found a bug on 04 Aug 2011
            'Data Samples for Temp fields are separated by a "|" not by a ","

            If Not IsNothingOrEmptyString(curValue) Then
                Dim valueArr() As String = CStr(CallByName(target, e.PropertyName, CallType.Get)).Split(",")

                For Each value As String In valueArr
                    If Not IsNothingOrEmptyString(value) And Me.DataLength.HasValue Then
                        If value.Trim.Length > Me.DataLength.Value Then
                            e.Description = _
                                        String.Format(MsgReader.GetString("E01000100"), ValidationArgs.GetFriendlyPropertyName(e))
                            Return False
                        End If
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            Return True
        End Try

    End Function

    'Each of the Default value should be the same as the DataType.
    Public Function E01000120(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim curValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            If Not IsNothingOrEmptyString(curValue) Then
                Dim dataType As String = CStr(CallByName(target, "DataType", CallType.Get))
                Dim dateFormat As String = CStr(CallByName(target, "DateFormat", CallType.Get))
                Dim valueArr() As String = curValue.Split(",")

                For Each value As String In valueArr
                    If dataType = "DateTime" And Not IsNothingOrEmptyString(value) Then
                        If Not IsDateDataType(value.Trim, dateFormat, False) Then
                            e.Description = _
                                        String.Format(MsgReader.GetString("E01000120"), ValidationArgs.GetFriendlyPropertyName(e))
                            Return False
                        End If

                    ElseIf dataType = "Number" And Not IsNothingOrEmptyString(value) Then
                        If Not IsNumeric(value) Then
                            e.Description = _
                                        String.Format(MsgReader.GetString("E01000120"), ValidationArgs.GetFriendlyPropertyName(e))
                            Return False
                        End If
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            Return True
        End Try

    End Function
End Class

Partial Public Class MasterTemplateNonFixed
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        '.;!@#$%^&*()-_+=/\?'|{}:[]<>`~ --> EnclosureCharacter
        '.,;!@#$%^&*+=/\?'|:<>`~ --> File Name
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("TemplateName", "Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Description", "Description"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("CustomerOutputExtension", "Output Extension"))

        ValidationEngine.AddValidationRule(AddressOf E01000010, New Common.ValidationArgs("TemplateName", "Template Name"))
        ValidationEngine.AddValidationRule(AddressOf E01000030, New Common.ValidationArgs("CustomerOutputExtension", "Output Extension"))

        ValidationEngine.Validate()
    End Sub

    Public Function E01000010(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

    Public Function E01000030(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "(", ")"c, "-"c, "_"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, "{"c, "}"c, ":"c, "["c, "]"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

End Class

Partial Public Class MasterTemplateNonFixedDetailCollection

    Protected Overrides Sub OnListChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)
        'Dim modifiedItem As MasterTemplateNonFixedDetail
        'Dim item As MasterTemplateNonFixedDetail
        'If e.NewIndex < Me.Items.Count Then
        '    modifiedItem = Me.Items(e.NewIndex)

        '    For Each item In MyBase.Items
        '        If Not item Is modifiedItem Then
        '            If item.FieldName = modifiedItem.FieldName Then

        '                Throw New Exception("Duplicate field name")
        '            End If
        '        End If
        '    Next
        '    MyBase.OnListChanged(e)
        'End If
    End Sub

End Class

Partial Public Class MasterTemplateFixed
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub Initialize()
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("TemplateName", "Template Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Description", "Description"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("CustomerOutputExtension", "Output Extension"))
        ValidationEngine.AddValidationRule(AddressOf E01010010, New Common.ValidationArgs("TemplateName", "Template Name"))
        ValidationEngine.AddValidationRule(AddressOf E01010030, New Common.ValidationArgs("CustomerOutputExtension", "Output Extension"))
        ValidationEngine.AddValidationRule(AddressOf E01010040, New Common.ValidationArgs("DelimiterCharacter", "Delimiter Character"))

        ValidationEngine.Validate()
    End Sub

    Public Function E01010010(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, ":"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

    Public Function E01010030(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            Dim isFound As Integer
            isFound = value.IndexOfAny(New Char() {"."c, ";"c, "!"c, ","c, "@"c, "#"c, "$"c, "%"c, "^"c, "&"c, "*"c, "(", ")"c, "-"c, "_"c, "+"c, "="c, "/"c, "\"c, "?"c, "'"c, "|"c, "{"c, "}"c, ":"c, "["c, "]"c, "<"c, ">"c, "`"c, "~"c, """"c})
            If Not IsNothingOrEmptyString(value) AndAlso isFound <> -1 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01000010"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try

    End Function

    'If Delimiter is selected, then the Delimiter should be provided.
    Public Function E01010040(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            'If value = "True" AndAlso IsNothingOrEmptyString(Me.DelimiterCharacter) Then
            If Me.Delimiter = True AndAlso IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010040"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

End Class

Partial Public Class MasterTemplateFixedDetail
    Inherits BTMU.MAGIC.Common.BusinessBase

    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    Protected Overrides Sub OnPropertyChanged(ByVal propertyName As String)
        MyBase.OnPropertyChanged(propertyName)
        If propertyName = "DataType" Then
            OnPropertyChanged("DateFormat")
            OnPropertyChanged("DataSample")
            OnPropertyChanged("DefaultValue")
            OnPropertyChanged("IncludeDecimal")
            OnPropertyChanged("DecimalSeparator")
        ElseIf propertyName = "DataLength" Then
            OnPropertyChanged("DataSample")
            OnPropertyChanged("DefaultValue")
            'ElseIf propertyName = "DateFormat" Then
            'OnPropertyChanged("DataLength")
        ElseIf propertyName = "IncludeDecimal" Then
            OnPropertyChanged("DecimalSeparator")
        ElseIf propertyName = "FieldName" Then
            OnPropertyChanged("DefaultValue")
        End If
    End Sub

    Protected Overrides Sub Initialize()
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("FieldName", "Field Name"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DataType", "Data Type"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("DataLength", "Data Length"))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.StringRequired, New Common.ValidationArgs("Detail", "Detail"))

        'Field Name must begin with an alphabet
        ValidationEngine.AddValidationRule(AddressOf E01010050, New Common.ValidationArgs("FieldName", "Field Name"))
        'If Data Type = "DateTime" then Date Format is required.
        ValidationEngine.AddValidationRule(AddressOf E01010060, New Common.ValidationArgs("DateFormat", "Date Format"))
        'If Data Type <> "DateTime" then Date Format should be empty.
        ValidationEngine.AddValidationRule(AddressOf E01010070, New Common.ValidationArgs("DateFormat", "Date Format"))
        'Data Length should be greater than 0..
        ValidationEngine.AddValidationRule(AddressOf E01010080, New Common.ValidationArgs("DataLength", "Data Length"))
        ' Each of the Default value should be the same as the DataType.

        'If Data Type = "DateTime" and Date Format has value, Data Length should same as date format. 
        'ValidationEngine.AddValidationRule(AddressOf E01000090, New Common.ValidationArgs("DataLength", "Data Length"))

        ValidationEngine.AddValidationRule(AddressOf E01010120, New Common.ValidationArgs("DefaultValue", "Default Value"))
        ' Each of the Data Sample should be the same as the DataType.
        ValidationEngine.AddValidationRule(AddressOf E01010120, New Common.ValidationArgs("DataSample", "Data Sample"))

        '(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("DefaultValue", "[;@^]+?", "Default Value", "(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','", True))
        ValidationEngine.AddValidationRule(AddressOf Common.CommonRules.RegExMatch, New Common.RegExValidationArgs("DataSample", "[;@^]+?", "Data Sample", "(;, @,^) cannot be used as delimiter for '{0}'. Please key in ','", True))

        'If the Field Name is NOT "Currency" and Data Type is NOT "Currency" and NOT "Number" then each of the Default Value should not exceed data length.
        ValidationEngine.AddValidationRule(AddressOf E01010090, New Common.ValidationArgs("DefaultValue", "Default Value"))
        'If the Field Name is NOT "Currency" and Data Type is either "Currency" or "Number" then each of the Default Value must be the same as Data Length.
        ValidationEngine.AddValidationRule(AddressOf E01010100, New Common.ValidationArgs("DefaultValue", "Default Value"))

        ValidationEngine.AddValidationRule(AddressOf E01010140, New Common.ValidationArgs("IncludeDecimal", "Include Decimal"))
        ValidationEngine.AddValidationRule(AddressOf E01010150, New Common.ValidationArgs("IncludeDecimal", "Include Decimal"))
        ValidationEngine.AddValidationRule(AddressOf E01010160, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))
        ValidationEngine.AddValidationRule(AddressOf E01010170, New Common.ValidationArgs("DecimalSeparator", "Decimal Separator"))

        'If the Field Name is NOT "Currency" and Data Type is NOT "Currency" and NOT "Number" then each of the Data Sample should not exceed data length.
        ValidationEngine.AddValidationRule(AddressOf E01010090, New Common.ValidationArgs("DataSample", "Data Sample"))
        'If the Field Name is NOT "Currency" and Data Type is either "Currency" or "Number" then each of the Data Sample must be the same as Data Length.
        ValidationEngine.AddValidationRule(AddressOf E01010100, New Common.ValidationArgs("DataSample", "Data Sample"))
        'Field name, Data type and Data Length should be filled in before before filling Data sample.
        ValidationEngine.AddValidationRule(AddressOf E01010210, New Common.ValidationArgs("DataSample", "Data Sample"))


        ValidationEngine.Validate()
    End Sub


    Public Function E01010050(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            'value.IndexOfAny(".", ";", "!", ",", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "/", "\", "?", "'", "|", "{", "}", ":", "[", "]", "<", ">", "`", "~", """",

            If Not IsNothingOrEmptyString(value) And Not (UCase(value.Substring(0, 1)) >= "A" And UCase(value.Substring(0, 1)) <= "Z") Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010050"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type = "DateTime" then Date Format is required.
    Public Function E01010060(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.DataType = "DateTime" And IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010060"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type <> "DateTime" then Date Format should be empty.
    Public Function E01010070(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            If Me.DataType <> "DateTime" And Not IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010070"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'Data Length should be greater than 0.
    Public Function E01010080(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
            If Not IsNothingOrEmptyString(value) And value <= 0 Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010080"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type = "DateTime" and Date Format has value, Data Length should same as date format.
    'Public Function E01000090(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
    '    Try
    '        Dim value As String = DataLength
    '        If Me.DataType = "DateTime" AndAlso Me.DateFormat.ToString.Trim <> "" AndAlso value <> Me.DateFormat.Length Then
    '            e.Description = _
    '                        String.Format(MsgReader.GetString("E01000090"), ValidationArgs.GetFriendlyPropertyName(e))
    '            Return False
    '        Else
    '            Return True
    '        End If
    '    Catch ex As Exception
    '        Return True
    '    End Try
    'End Function

    'Each of the Default value should be the same as the DataType.
    Public Function E01010120(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Dim CurValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        Try

            If Not IsNothingOrEmptyString(CurValue) Then
                Dim valueArr() As String = CurValue.Split(",")
                Dim dataType As String = CStr(CallByName(target, "DataType", CallType.Get))
                Dim dateFormat As String = CStr(CallByName(target, "DateFormat", CallType.Get))
                Dim fieldName As String = CStr(CallByName(target, "FieldName", CallType.Get))

                If UCase(fieldName) <> "CURRENCY" Then
                    For Each value As String In valueArr
                        If dataType = "DateTime" And Not IsNothingOrEmptyString(value) Then
                            If Not IsDateDataType(value.Trim, dateFormat, False) Then
                                e.Description = _
                                            String.Format(MsgReader.GetString("E01010120"), ValidationArgs.GetFriendlyPropertyName(e))
                                Return False
                            End If

                        ElseIf (dataType = "Number" Or dataType = "Currency") And Not IsNothingOrEmptyString(value) Then
                            If Not IsNumeric(value) Then
                                e.Description = _
                                            String.Format(MsgReader.GetString("E01010120"), ValidationArgs.GetFriendlyPropertyName(e))
                                Return False
                            End If
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            Return True
        End Try

    End Function

    'If the Field Name is NOT "Currency" and Data Type is NOT "Currency" and NOT "Number" then each of the Default Value should not exceed data length.
    Public Function E01010090(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Dim CurValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

        Try
            Dim valueArr() As String = CurValue.Split(",")

            If Not IsNothingOrEmptyString(CurValue) Then
                For Each value As String In valueArr
                    If UCase(Me.FieldName) <> "CURRENCY" And _
                                    UCase(Me.DataType) <> "CURRENCY" And _
                                    UCase(Me.DataType) <> "NUMBER" And _
                                    Me.DataLength.HasValue And _
                                    value.Trim <> "" And _
                                    value.Length > Me.DataLength.Value Then
                        e.Description = String.Format(MsgReader.GetString("E01010090"), ValidationArgs.GetFriendlyPropertyName(e))
                        Return False
                        'Else
                        'Return True
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If the Field Name is NOT "Currency" and Data Type is either "Currency" or "Number" then each of the Default Value must be the same as Data Length.
    Public Function E01010100(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Dim CurValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        Try

            If Not IsNothingOrEmptyString(CurValue) Then
                Dim valueArr() As String = CurValue.Split(",")

                For Each value As String In valueArr
                    If UCase(Me.FieldName) <> "CURRENCY" And _
                        (UCase(Me.DataType) = "CURRENCY" Or UCase(Me.DataType) = "NUMBER") And _
                        Me.DataLength.HasValue And _
                        value.Trim <> "" And _
                        value.Length <> Me.DataLength.Value Then

                        e.Description = String.Format(MsgReader.GetString("E01010100"), ValidationArgs.GetFriendlyPropertyName(e))
                        Return False
                        'Else
                        'Return True
                    End If
                Next
            End If
            Return True

        Catch ex As Exception
            Return True
        End Try
    End Function

    'If the Field Name is "Currency" and Output Format <> "SOAPPL" and if any of the Default Value contains "-", then whatever to the left of the "-" should not exceed the DataLength and whatever to the right of "-" should be numeric. 
    Public Function E01010130(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Dim CurValue As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        Try

            If Not IsNothingOrEmptyString(CurValue) Then
                Dim valueArr() As String = CurValue.Split(",")
                Dim valueD() As String

                For Each value As String In valueArr
                    If value.IndexOfAny("-") <> -1 Then
                        valueD = value.Split("-")

                        If Me.DataLength.Value < valueD(0).Length Then
                            e.Description = String.Format(MsgReader.GetString("E01010120"), ValidationArgs.GetFriendlyPropertyName(e))
                            Return False
                        End If

                        If Not IsNumeric(valueD(1)) Then
                            e.Description = String.Format(MsgReader.GetString("E01010120"), ValidationArgs.GetFriendlyPropertyName(e))
                            Return False
                        End If
                    End If

                Next
            End If
            Return True
        Catch ex As Exception
            Return True
        End Try
    End Function


    'If Data Type = "Currency" then Include Decimal is required.
    Public Function E01010140(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.DataType = "Currency" And IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010140"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type <> "Currency" then Include Decimal should be empty.
    Public Function E01010150(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.DataType <> "Currency" And Not IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010150"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Data Type <> "Currency" then Decimal Separator should be empty
    Public Function E01010160(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.DataType <> "Currency" And Not IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010160"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    'If Include Decimal is "Yes" then the Decimal Separator should be empty.
    Public Function E01010170(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If Me.IncludeDecimal = "Yes" And Not IsNothingOrEmptyString(value) Then
                e.Description = _
                            String.Format(MsgReader.GetString("E01010170"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function


    'Field name, Data type and Data Length should be filled in before filling Data sample.
    Public Function E01010210(ByVal target As Object, ByVal e As ValidationArgs) As Boolean
        Try
            Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))

            If IsNothingOrEmptyString(FieldName) And _
                IsNothingOrEmptyString(DataType) And _
                Not Me.DataLength.HasValue And _
                Not IsNothingOrEmptyString(value) Then
                e.Description = _
                                            String.Format(MsgReader.GetString("E01010210"), ValidationArgs.GetFriendlyPropertyName(e))
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return True
        End Try
    End Function
End Class