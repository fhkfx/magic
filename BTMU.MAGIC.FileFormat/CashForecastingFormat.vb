'Cash Forecasting Import Format (BTMU-Hong Kong)

Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the CashForecasting Format
''' </summary>
''' <remarks></remarks>
Public Class CashForecastingFormat
    Inherits BaseFileFormat

    Private MandatoryFields() As String = {"Date", "Customer ID", "D/C", "Bank Name", "Branch Name", "Account Type" _
                    , "Account No", "CCY", "Amount", "Forecast Type"}

    Private Shared _amountFieldName As String = "Amount"
    ' editable fields
    Private Shared _editableFields() As String = {"Date", "Customer ID", "D/C", "Bank Name", "Branch Name", "Account Type" _
                    , "Account No", "CCY", "Amount", "Forecast Type", "Detail Information"}

    Private Shared _autoTrimmableFields() As String = {"Date", "Customer ID", "D/C", "Bank Name", "Branch Name", "Account Type" _
                    , "Account No", "CCY", "Amount", "Forecast Type", "Detail Information"}

    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Date", "Customer ID", "D/C", "Bank Name", "Branch Name", "Account Type" _
                    , "Account No", "CCY", "Amount", "Forecast Type", "Detail Information"}


    Public Shared Function OutputFormatString() As String
        Return "CashForecasting"
    End Function

#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "CashForecasting"
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Date") = ValueDate.ToString(TblBankFields("Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Date: {0}", ex.Message))
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As System.Collections.Generic.List(Of String), ByVal _referenceFields As System.Collections.Generic.List(Of String), ByVal _appendText As System.Collections.Generic.List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Amount"))
            If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Amount")

            _consolidatedRecordCount = _consolidatedData.Rows.Count

            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim headertext As String = String.Empty

        Dim objMasterTemplate As MasterTemplateNonFixed  ' Non Fixed Type Master Template

        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        'Prepare header (only field names)
        For Each col As DataColumn In _PreviewTable.Columns
            If Not col.ColumnName.ToUpper.Equals("CONSOLIDATE FIELD") Then
                headertext &= """" & col.ColumnName() & ""","
            End If
        Next
        'remove the last comma
        headertext = headertext.Remove(headertext.LastIndexOf(","), 1)

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In TblBankFields.Keys
                    sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)
                rowNo += 1

            Next

            'append header
            sText = headertext & vbNewLine & sText
            Return SaveTextToFile(sText, _outputFile)

            'Catch fex As FormatException
            '    Throw New MagicException(String.Format("Record: {0} Field: 'Amount of Invoice' has invalid value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    Protected Overrides Sub GenerateOutputFormat()
        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Format Amount 
                If _previewRecord("Amount").ToString <> String.Empty Then

                    'Output accepts Dollar value
                    'Convert to Dollars if the source value is Cents
                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Format Amount 
                If _previewRecord("Amount").ToString <> String.Empty Then

                    'Output accepts Dollar value
                    'Convert to Dollars if the source value is Cents
                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub Validate()

        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim _previewTableCopy As New DataTable
        Dim prevTransRef As String = String.Empty
        Dim tempId As String = String.Empty
        Dim tempCur As String = String.Empty

        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)

        rowNo = 0

        Try
            ' Validate the fields of each record and add file format errors to _validationerrors object
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'Field #1 - Date
                Dim _ValueDate As Date
                If _previewRecord("Date").ToString().Trim <> String.Empty Then
                    If HelperModule.IsDateDataType(_previewRecord("Date").ToString().Trim, "", TblBankFields("Date").DateFormat, _ValueDate, True) Then
                        'Cannot be a past date
                        If _ValueDate < Date.Today Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Date").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Value Date' cannot be a past date.", rowNo + 1)
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        'Cannot fall on Saturday or Sunday. 
                        If Weekday(_ValueDate) = 1 Or Weekday(_ValueDate) = 7 Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Date").Ordinal + 1
                            validationError.Description = String.Format(MsgReader.GetString("E09020030"), "Record", rowNo + 1, "Value Date")
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    Else
                        'Must be a valid Date value
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be a valid date.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If
                End If

                'Field #2 - Customer ID 
                If _previewRecord("Customer ID").ToString.Trim <> String.Empty Then
                    'For uniqueness checking
                    If rowNo = 0 Then tempId = _previewRecord("Customer ID").ToString.Trim

                    'must be C####### format.
                    If _previewRecord("Customer ID").ToString.Length <> 8 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Customer ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Customer ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Customer ID' must start with 'C' and followed by 7-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                    'must start with 'C'
                    If _previewRecord("Customer ID").ToString.Length = 8 And _
                        Not _previewRecord("Customer ID").ToString.StartsWith("C", StringComparison.InvariantCultureIgnoreCase) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Customer ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Customer ID' must start with 'C' and followed by 7-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                        'End If

                        'must followed by 7 digits
                    ElseIf _previewRecord("Customer ID").ToString.Length = 8 And _
                            regNonNumeric.IsMatch(_previewRecord("Customer ID").ToString.Substring(1)) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Customer ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Customer ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Customer ID' must start with 'C' and followed by 7-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Check for uniqueness in the whole file
                    If Not tempId.Equals(_previewRecord("Customer ID").ToString.Trim) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Customer ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Customer ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Customer ID' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #3 D/C
                If _previewRecord("D/C").ToString.Trim <> String.Empty Then
                    If (Not _previewRecord("D/C").ToString.Equals("D") And _
                        Not _previewRecord("D/C").ToString.Equals("C")) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "D/C"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("D/C").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'D/C' is not a valid 'D/C'.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #4 Bank Name
                If _previewRecord("Bank Name").ToString.Trim = String.Empty Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Bank Name"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Bank Name").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Bank Name")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Field #5 Branch Name
                If _previewRecord("Branch Name").ToString.Trim = String.Empty Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Branch Name"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Branch Name").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Branch Name")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Field #6 Account Type must be one of the default values in master template
                If _previewRecord("Account Type").ToString <> String.Empty Then
                    If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Account Type").ToString

                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Account Type").DefaultValue) Then
                        If TblBankFields("Account Type").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Account Type").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Account Type").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Account Type"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Account Type").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Account Type' is not a valid Account Type.", rowNo + 1)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                ' Field #7 Account No
                If _previewRecord("Account No").ToString.Trim <> String.Empty Then
                    'length must be exactly 10 or 11 digits
                    If _previewRecord("Account No").ToString.Length <> 10 And _previewRecord("Account No").ToString.Length <> 11 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Account No"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Account No").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050041"), "Record", rowNo + 1, "Account No", "10 or 11")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'account number must be numeric
                    If regNonNumeric.IsMatch(_previewRecord("Account No").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Account No"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Account No").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Account No")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #8 CCY : Must be one of default values in master template
                If _previewRecord("CCY").ToString <> String.Empty Then
                    If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("CCY").ToString

                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("CCY").DefaultValue) Then
                        If TblBankFields("CCY").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("CCY").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("CCY").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "CCY"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("CCY").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'CCY' is not a valid CCY.", rowNo + 1)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                ' Field #9 Amount
                Dim amount As Decimal
                If _previewRecord("Amount").ToString.Trim <> String.Empty And _
                          Not Decimal.TryParse(_previewRecord("Amount").ToString, amount) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Amount"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Amount").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Amount")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Field #10 Forecast Type
                If _previewRecord("Forecast Type").ToString.Trim <> String.Empty Then
                    If (Not _previewRecord("Forecast Type").ToString.Equals("1") And _
                        Not _previewRecord("Forecast Type").ToString.Equals("2") And _
                        Not _previewRecord("Forecast Type").ToString.Equals("3")) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Forecast Type"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Forecast Type").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Forecast Type' is not a valid 'Forecast Type'.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #11 set Detail Information to blank always
                _previewRecord("Detail Information") = String.Empty

                ' Validation - Do not allow unpermitted characters
                Dim regAllow As New Regex("[^0-9A-Za-z/?(),.:'+\-\s]", RegexOptions.Compiled)

                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Date", "Customer ID", "D/C", "Bank Name", "Branch Name", "Account Type" _
                    , "Account No", "CCY", "Amount", "Forecast Type"

                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(col).ToString) Then

                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If

                        Case Else
                            'Do Nothing
                    End Select
                Next

                rowNo += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Validation : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#End Region


End Class

