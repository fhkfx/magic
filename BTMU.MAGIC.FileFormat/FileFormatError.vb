
''' <summary>
''' Encapsulates the details of Validation Error araising from Transaction Records prior to File Conversion
''' </summary>
''' <remarks></remarks>
Public Class FileFormatError

#Region "Private Properties"

    Private _recordno As Int32
    Private _columnordinalno As Int32
    Private _columnname As String
    Private _description As String

#End Region

#Region "Public Properties"

    ''' <summary>
    ''' Record Number of the Transaction Records that has validation errors
    ''' </summary>
    ''' <value>Record No.</value>
    ''' <returns>Returns the Record No.</returns>
    ''' <remarks></remarks>
    Public Property RecordNo() As Int32
        Get
            Return _recordno
        End Get
        Set(ByVal value As Int32)
            _recordno = value
        End Set
    End Property

    ''' <summary>
    ''' Column Number of a Transaction Record that has validation error
    ''' </summary>
    ''' <value>Column ordinal No.</value>
    ''' <returns>Returns the Column oridinal No.</returns>
    ''' <remarks></remarks>
    Public Property ColumnOrdinalNo() As Int32
        Get
            Return _columnordinalno
        End Get
        Set(ByVal value As Int32)
            _columnordinalno = value
        End Set
    End Property

    ''' <summary>
    ''' Column Name of a field in a Transaction Record that has validation error
    ''' </summary>
    ''' <value>Column Name</value>
    ''' <returns>Returns Column Name</returns>
    ''' <remarks></remarks>
    Public Property ColumnName() As String
        Get
            Return _columnname
        End Get
        Set(ByVal value As String)
            _columnname = value
        End Set
    End Property

    ''' <summary>
    ''' Description of Validation Error
    ''' </summary>
    ''' <value>Error Description</value>
    ''' <returns>Returns Error Description</returns>
    ''' <remarks></remarks>
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

#End Region

End Class

''' <summary>
''' Collection of Validation Errors
''' </summary>
''' <remarks></remarks>
Public Class FileFormatErrorCollection
    Inherits System.ComponentModel.BindingList(Of FileFormatError)

End Class
