Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions
Imports System.IO

''' <summary>
''' Encapsulates the OMAKASE II output Format:
''' No I-records contain in the source file.
''' </summary>
Public Class OMAKASEFormatII
    Inherits BaseFileFormat

    'Target Data Tables
    Private _tblPayment As DataTable

    'Source Data Tables
    Private _tblPaymentSrc As DataTable

    'Added for Different Date value checking and A/C no.
    '*************************************
    Private Shared _IsSameDateValue As Boolean = True
    

#Region "Local Constants"

    'In this format, editable fields, group by fields and reference fields are same
    Private Shared _editableFields() As String = { _
                                "File Type-P", "Seq. No.-P", "Transaction Type", "Beneficiary Bank Branch IFSC Code" _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary A/C No.", "Beneficiary Name" _
                                , "Transaction Amount", "Remark / Sender to Receiver Information", "Payee Name-1", "Payee Name-2", "Notice By", "Beneficiary Fax Area Code" _
                                , "Applicant Mobile No", "Applicant E-mail Address", "Bene Address 1 / Payee Address 1", "Bene Address 2 / Payee Address 2", "Bene Address 3 / Payable At" _
                                , "Customer Reference 1 / Detail of Payment 1", "Customer Reference 2 / Detail of Payment 2", "VAT Amount-P", "Discount Amount", "Invoice Amount-P" _
                                , "Commission Charge To", "Mailing Name", "Mailing Address 1", "Mailing Address 2", "Mailing Address 3", "Reserve-P" _
                                }

    Private Shared _amountFieldName As String = "Transaction Amount"

    Private Shared _autoTrimmableFields() As String = { _
                                "Contact Person", "Reserve-H" _
 _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary Name", "Remark / Sender to Receiver Information" _
                                , "Bene Address 1 / Payee Address 1", "Bene Address 2 / Payee Address 2", "Bene Address 3 / Payable At", "Customer Reference 1 / Detail of Payment 1" _
                                , "Customer Reference 2 / Detail of Payment 2", "Mailing Name" _
                                , "Mailing Address 1", "Mailing Address 2", "Mailing Address 3", "Reserve-P" _
                                }

    Private Shared _validgroupbyFields() As String = { _
                                "File Type-P", "Seq. No.-P", "Transaction Type", "Beneficiary Bank Branch IFSC Code" _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary A/C No.", "Beneficiary Name" _
                                , "Transaction Amount", "Remark / Sender to Receiver Information", "Payee Name-1", "Payee Name-2", "Notice By", "Beneficiary Fax Area Code" _
                                , "Applicant Mobile No", "Applicant E-mail Address", "Bene Address 1 / Payee Address 1", "Bene Address 2 / Payee Address 2", "Bene Address 3 / Payable At" _
                                , "Customer Reference 1 / Detail of Payment 1", "Customer Reference 2 / Detail of Payment 2", "VAT Amount-P", "Discount Amount", "Invoice Amount-P" _
                                , "Commission Charge To", "Mailing Name", "Mailing Address 1", "Mailing Address 2", "Mailing Address 3", "Reserve-P" _
                                }

    Private Shared _headerfields() As String = {"File Type-H", "Account No", "Value Date", "No. of Record", "Total Amount", "Debit Type", "Fax Area Code", "Fax No.", "E-mail Address", "Contact Person", "Customer ID", "Batch ID", "Reserve-H"}

    'Fields: Payment Record
    Private Shared _fieldsPaymentRecord() As String = {"File Type-P", "Seq. No.-P", "Transaction Type", "Beneficiary Bank Branch IFSC Code" _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary A/C No.", "Beneficiary Name" _
                                , "Transaction Amount", "Remark / Sender to Receiver Information", "Payee Name-1", "Payee Name-2", "Notice By", "Beneficiary Fax Area Code" _
                                , "Applicant Mobile No", "Applicant E-mail Address", "Bene Address 1 / Payee Address 1", "Bene Address 2 / Payee Address 2", "Bene Address 3 / Payable At" _
                                , "Customer Reference 1 / Detail of Payment 1", "Customer Reference 2 / Detail of Payment 2", "VAT Amount-P", "Discount Amount", "Invoice Amount-P" _
                                , "Commission Charge To", "Mailing Name", "Mailing Address 1", "Mailing Address 2", "Mailing Address 3", "Reserve-P" _
                                }
   

#End Region

    Public Shared ReadOnly Property IsSameDateValue() As Boolean
        Get
            Return _IsSameDateValue
        End Get
    End Property

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    'Has more than one amount fields  
    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return ""
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "Mr.Omakase India II"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

#End Region

    Public Shared Function OutputFormatString() As String
        Return "Mr.Omakase India II"
    End Function

    Public Sub New()
        SetupPTable()
    End Sub

#Region "Overriden Methods"


    Protected Overrides Sub GenerateHeader()

        _Header = String.Empty

        For Each row As DataRow In _PreviewTable.Rows

            For Each headerfld As String In _headerfields
                _Header &= row(headerfld).ToString()
                _Header &= IIf(_Header = String.Empty, "", "|")
            Next

            If _Header.EndsWith("|") Then _Header = _Header.Substring(0, _Header.Length - 1)

            Exit For

        Next

        _Header &= vbCrLf

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
                Exit For
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim strField As String = String.Empty
        Dim rowindex As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim payeeNames As New List(Of String)
        'replace colons with a space in (1st, 36th, 71st, 106th, 141st, 176th) positions
        Dim ReplaceCharLocs() As Int32 = {0, 35, 70, 105, 140, 175}
        Dim remark As String


        For Each row As DataRow In _PreviewTable.Rows

            'Format Fields: Header '''''''''''''''''''''''''''''''''''''''''''''''''''''
            If rowindex = 0 Then
                If Not IsNothingOrEmptyString(row("Contact Person").ToString()) Then
                    row("Contact Person") = row("Contact Person").ToString().ToUpper()
                End If

                If IsNothingOrEmptyString(row("No. of Record").ToString()) Then
                    row("No. of Record") = _PreviewTable.Rows.Count
                End If
                'Check for consolidations()
                If _consolidatedRecordCount > 0 AndAlso _consolidatedRecordCount <= row("No. of Record") Then
                    row("No. of Record") = _consolidatedRecordCount
                End If

                If row("Total Amount") <> "" AndAlso Decimal.TryParse(row("Total Amount").ToString(), Nothing) Then
                    'row("Total Amount") = Convert.ToDecimal(row("Total Amount")).ToString("#.00")
                    row("Total Amount") = FormatAmount(Convert.ToDecimal(row("Total Amount").ToString))
                End If

                'If Debit Type is blank, set '0' by default
                If IsNothingOrEmptyString(row("Debit Type").ToString()) Then
                    'row("Total Amount") = Convert.ToDecimal(row("Total Amount")).ToString("#.00")
                    row("Debit Type") = "0"
                End If

            End If


            'if transtype=2 and tramsamount <100000 is okay
            'no need to check
            '******************
            'If row("Transaction Type").ToString() = "2" AndAlso Val(row("Transaction Amount")) > 0.0 Then
            '    row("Transaction Type") = "2"
            'End If

            ''Format Fields: Payment Record '''''''''''''''''''''''''''''''''''''''''''''''''''''
            'If row("File Type-P").ToString() = "P" Then

            If row("Transaction Amount") <> "" AndAlso Decimal.TryParse(row("Transaction Amount").ToString(), Nothing) Then
                'Remove hyphen and comma
                row("Transaction Amount") = row("Transaction Amount").ToString().Replace("-", "").Replace(",", "")
                'format two decimal places
                row("Transaction Amount") = FormatAmount(Convert.ToDecimal(row("Transaction Amount").ToString))
            End If

            If row("VAT Amount-P") <> "" AndAlso Decimal.TryParse(row("VAT Amount-P").ToString(), Nothing) Then
                'format two decimal places
                row("VAT Amount-P") = FormatAmount(Convert.ToDecimal(row("VAT Amount-P").ToString))
            Else
                row("VAT Amount-P") = "0.00"
            End If

            If row("Discount Amount") <> "" AndAlso Decimal.TryParse(row("Discount Amount").ToString(), Nothing) Then
                'format two decimal places
                row("Discount Amount") = FormatAmount(Convert.ToDecimal(row("Discount Amount").ToString))
            Else
                row("Discount Amount") = "0.00"
            End If

            If row("Invoice Amount-P") <> "" AndAlso Decimal.TryParse(row("Invoice Amount-P").ToString(), Nothing) Then
                'format two decimal places
                row("Invoice Amount-P") = FormatAmount(Convert.ToDecimal(row("Invoice Amount-P").ToString))
            Else
                row("Invoice Amount-P") = "0.00"
            End If

            Select Case row("Transaction Type").ToString()
                '#-#-#
               
                Case "5", "6", "7"
                    row("Beneficiary Bank Branch IFSC Code") = ""
                    row("Beneficiary Bank Name") = ""
                    row("Beneficiary Branch Name") = ""
                    row("Beneficiary A/C No.") = ""
                    row("Beneficiary Name") = ""
                    'modified @ 09/11/2010
                    'change request after UAT
                    'row("Mailing Name") = ""
                Case Else
                    row("Payee Name-1") = ""
                    row("Payee Name-2") = ""
            End Select


            If IsNothingOrEmptyString(row("Notice By")) Then
                row("Notice By") = "01"
            End If

            If IsNothingOrEmptyString(row("Commission Charge To")) Then
                row("Commission Charge To") = "A"
            End If

            'Payee Name-1 exceeds 80 chars, the exceeding chars shall be copied to payee name-2
            'change request @ 14/09/2010
            'no more required
            'If row("Payee Name-1").ToString().Length > 80 Then
            '    payeeNames = SplitPayeeName(row("Payee Name-1").ToString(), 80)
            '    row("Payee Name-1") = payeeNames(0)
            '    row("Payee Name-2") = payeeNames(1)
            'End If

            'change request
            Select Case row("Transaction Type").ToString()
                '#-#-#
                Case "1", "2", "A"
                    'replace colon/hyphen with a space in first position
                    If Not IsNothingOrEmptyString(row("Beneficiary Name")) Then
                        If (row("Beneficiary Name").ToString().StartsWith(":") OrElse row("Beneficiary Name").ToString().StartsWith("-")) Then
                            row("Beneficiary Name") = " " + row("Beneficiary Name").ToString().Remove(0, 1)
                        End If

                        'replace non-permitted characters with a space
                        row("Beneficiary Name") = ReplaceNonPermitCharWithSpace(row("Beneficiary Name").ToString())
                    End If

                    If Not IsNothingOrEmptyString(row("Remark / Sender to Receiver Information")) Then
                        remark = row("Remark / Sender to Receiver Information").ToString()
                        For Each index As Int32 In ReplaceCharLocs

                            If index < remark.Length Then
                                'replace with a space
                                If remark(index) = ":" OrElse remark(index) = "-" Then
                                    remark = remark.Remove(index, 1)
                                    remark = remark.Insert(index, " ")
                                End If

                            End If
                        Next
                        row("Remark / Sender to Receiver Information") = remark

                        'replace non-permitted characters with a space
                        row("Remark / Sender to Receiver Information") = ReplaceNonPermitCharWithSpace(row("Remark / Sender to Receiver Information").ToString())
                    End If

                    If Not IsNothingOrEmptyString(row("Bene Address 1 / Payee Address 1")) Then
                        If (row("Bene Address 1 / Payee Address 1").ToString().StartsWith(":") OrElse row("Bene Address 1 / Payee Address 1").ToString().StartsWith("-")) Then
                            row("Bene Address 1 / Payee Address 1") = " " + row("Bene Address 1 / Payee Address 1").ToString().Remove(0, 1)
                        End If

                        'replace non-permitted characters with a space
                        row("Bene Address 1 / Payee Address 1") = ReplaceNonPermitCharWithSpace(row("Bene Address 1 / Payee Address 1").ToString())
                    End If

                    If Not IsNothingOrEmptyString(row("Bene Address 2 / Payee Address 2")) Then
                        If (row("Bene Address 2 / Payee Address 2").ToString().StartsWith(":") OrElse row("Bene Address 2 / Payee Address 2").ToString().StartsWith("-")) Then
                            row("Bene Address 2 / Payee Address 2") = " " + row("Bene Address 2 / Payee Address 2").ToString().Remove(0, 1)
                        End If

                        'replace non-permitted characters with a space
                        row("Bene Address 2 / Payee Address 2") = ReplaceNonPermitCharWithSpace(row("Bene Address 2 / Payee Address 2").ToString())
                    End If

                    If Not IsNothingOrEmptyString(row("Bene Address 3 / Payable At")) Then
                        If (row("Bene Address 3 / Payable At").ToString().StartsWith(":") OrElse row("Bene Address 3 / Payable At").ToString().StartsWith("-")) Then
                            row("Bene Address 3 / Payable At") = " " + row("Bene Address 3 / Payable At").ToString().Remove(0, 1)
                        End If

                        'replace non-permitted characters with a space
                        row("Bene Address 3 / Payable At") = ReplaceNonPermitCharWithSpace(row("Bene Address 3 / Payable At").ToString())
                    End If


                    'Except from NEFT Transaction Type
                    If Not row("Transaction Type").ToString().Equals("2") Then
                        If Not IsNothingOrEmptyString(row("Customer Reference 1 / Detail of Payment 1")) Then
                            If (row("Customer Reference 1 / Detail of Payment 1").ToString().StartsWith(":") OrElse row("Customer Reference 1 / Detail of Payment 1").ToString().StartsWith("-")) Then
                                row("Customer Reference 1 / Detail of Payment 1") = " " + row("Customer Reference 1 / Detail of Payment 1").ToString().Remove(0, 1)
                            End If

                            'replace non-permitted characters with a space
                            row("Customer Reference 1 / Detail of Payment 1") = ReplaceNonPermitCharWithSpace(row("Customer Reference 1 / Detail of Payment 1").ToString())
                        End If

                        If Not IsNothingOrEmptyString(row("Customer Reference 2 / Detail of Payment 2")) Then
                            If (row("Customer Reference 2 / Detail of Payment 2").ToString().StartsWith(":") OrElse row("Customer Reference 2 / Detail of Payment 2").ToString().StartsWith("-")) Then
                                row("Customer Reference 2 / Detail of Payment 2") = " " + row("Customer Reference 2 / Detail of Payment 2").ToString().Remove(0, 1)
                            End If

                            'replace non-permitted characters with a space
                            row("Customer Reference 2 / Detail of Payment 2") = ReplaceNonPermitCharWithSpace(row("Customer Reference 2 / Detail of Payment 2").ToString())
                        End If
                    End If
                Case Else
                    'nothing
            End Select


            'replace colon with a space in first position
            If Not IsNothingOrEmptyString(row("Applicant Mobile No")) AndAlso (row("Applicant Mobile No").ToString().StartsWith(":") OrElse row("Applicant Mobile No").ToString().StartsWith("-")) Then
                row("Applicant Mobile No") = " " + row("Applicant Mobile No").ToString().Remove(0, 1)
            End If

            If Not IsNothingOrEmptyString(row("Applicant E-mail Address")) AndAlso (row("Applicant E-mail Address").ToString().StartsWith(":") OrElse row("Applicant E-mail Address").ToString().StartsWith("-")) Then
                row("Applicant E-mail Address") = " " + row("Applicant E-mail Address").ToString().Remove(0, 1)
            End If

            'In the event of both fields are mapped and available, Email address should be outputted
            'And Applicant Mobile No will be blank.
            '**********************************
            If Not IsNothingOrEmptyString(row("Applicant Mobile No")) AndAlso Not IsNothingOrEmptyString(row("Applicant E-mail Address")) Then
                row("Applicant Mobile No") = ""
            End If

            
            'Confirmed by BTMU (Casey), it's not necessary to validate
            'If Not IsNothingOrEmptyString(row("Mailing Name")) AndAlso (row("Mailing Name").ToString().StartsWith(":") OrElse row("Mailing Name").ToString().StartsWith("-")) Then
            '    row("Mailing Name") = " " + row("Mailing Name").ToString().Remove(0, 1)
            'End If

            'If Not IsNothingOrEmptyString(row("Mailing Address1")) AndAlso (row("Mailing Address1").ToString().StartsWith(":") OrElse row("Mailing Address1").ToString().StartsWith("-")) Then
            '    row("Mailing Address1") = " " + row("Mailing Address1").ToString().Remove(0, 1)
            'End If

            'If Not IsNothingOrEmptyString(row("Mailing Address2")) AndAlso (row("Mailing Address2").ToString().StartsWith(":") OrElse row("Mailing Address2").ToString().StartsWith("-")) Then
            '    row("Mailing Address2") = " " + row("Mailing Address2").ToString().Remove(0, 1)
            'End If

            'If Not IsNothingOrEmptyString(row("Mailing Address3")) AndAlso (row("Mailing Address3").ToString().StartsWith(":") OrElse row("Mailing Address3").ToString().StartsWith("-")) Then
            '    row("Mailing Address3") = " " + row("Mailing Address3").ToString().Remove(0, 1)
            'End If


            rowindex += 1
        Next

    End Sub

    ''' <summary>
    ''' #10. Fill Sector Selection 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub BeneAddressStringManipulation()

        Dim strField As String = String.Empty
        Dim rowindex As Int32 = 0
        

        For Each row As DataRow In _PreviewTable.Rows

            '*******************************
            'Change Request for Auto shifting 
            'mapping logic for bene address 1/2/3
            'After UAT, modified by Kay @ 20/10/2010
            'due to some scenarios are not covered in the last CR, revise the logic accordingly during Mar 2011
            'final modification made on 16 Mar 2011
            '*******************************
            'Declare to store BeneAdd1 value
            Dim strBene As String

            Dim len_bene1, len_bene2, len_bene3 As Integer
            If Not IsNothingOrEmptyString(row("Transaction Type")) Then
                strBene = row("Bene Address 1 / Payee Address 1").ToString()

                len_bene1 = TblBankFields("Bene Address 1 / Payee Address 1").DataLength.Value
                len_bene2 = TblBankFields("Bene Address 2 / Payee Address 2").DataLength.Value
                len_bene3 = TblBankFields("Bene Address 3 / Payable At").DataLength.Value


                Select Case row("Transaction Type").ToString()
                    Case "1", "2", "3", "4", "A"
                        'Rule-3
                        'If BeneAdd1 length is > 70 , distribute the value to bene1,2,and 3 fields (each-35 chars)
                        If strBene.Length > (len_bene1 + len_bene2) Then
                            row("Bene Address 1 / Payee Address 1") = strBene.Substring(0, len_bene1)
                            row("Bene Address 2 / Payee Address 2") = strBene.Substring(len_bene1, len_bene2)
                            row("Bene Address 3 / Payable At") = strBene.Substring(len_bene1 + len_bene2)
                        ElseIf strBene.Length > len_bene1 Then
                            'Rule-2
                            ''If BeneAdd1 length is > 35, distribute the value to bene1,2,and append bene3 data to bene2 if bene3 length  is < 35; bene3 will resume from the end of bene2 val.
                            row("Bene Address 1 / Payee Address 1") = strBene.Substring(0, len_bene1)
                            row("Bene Address 2 / Payee Address 2") = strBene.Substring(len_bene1)
                            row("Bene Address 3 / Payable At") = ""
                        Else
                            'Rule-1
                            'If BeneAdd1 length <= 35
                            row("Bene Address 1 / Payee Address 1") = strBene
                            row("Bene Address 2 / Payee Address 2") = ""
                            row("Bene Address 3 / Payable At") = ""
                        End If

                    Case Else
                        'Rule-5
                        'if bene1 length is > 35, distribute the value to bene1 and bene2 only
                        If strBene.Length > len_bene1 Then
                            row("Bene Address 1 / Payee Address 1") = strBene.Substring(0, len_bene1)
                            row("Bene Address 2 / Payee Address 2") = strBene.Substring(len_bene1)
                        Else
                            'Rule-4
                            'If BeneAdd1 length < 35
                            row("Bene Address 1 / Payee Address 1") = strBene
                            row("Bene Address 2 / Payee Address 2") = ""
                        End If

                End Select
            End If

        Next
    End Sub


    Private Function SplitPayeeName(ByVal VenName As String, ByVal StringLength As Integer) As List(Of String)
        Dim strSplit() As String
        Dim PayeeNames As New List(Of String)
        Dim PayeeName2StartPosition As Integer

        PayeeNames.Add("")
        PayeeNames.Add("")


        strSplit = VenName.Split(" ")
        For i As Integer = 0 To strSplit.Length - 1
            If strSplit(i).Length <= StringLength Then
                If PayeeNames(0) = String.Empty Then
                    PayeeNames(0) = strSplit(i)
                Else
                    If String.Concat(PayeeNames(0), " ", strSplit(i)).Length <= StringLength Then
                        PayeeNames(0) = String.Concat(PayeeNames(0), " ", strSplit(i))
                    Else
                        PayeeName2StartPosition = i
                        Exit For
                    End If
                End If
            End If
        Next

        For i As Integer = PayeeName2StartPosition To strSplit.Length - 1
            If PayeeNames(1) = String.Empty Then
                PayeeNames(1) = strSplit(i)
            Else
                PayeeNames(1) = String.Concat(PayeeNames(1), " ", strSplit(i))
            End If
        Next
        Return PayeeNames
    End Function
    'Validates Mandatory & Conditional Mandatory Fields.
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0

        Try

            'Validate Mandatory Header Fields
            For Each headerfld As String In New String() {"File Type-H", "Account No", "Value Date", "No. of Record", "Total Amount", "Customer ID"}

                If IsNothingOrEmptyString(_PreviewTable.Rows(0)(headerfld).ToString()) Then
                    AddValidationError(headerfld, _PreviewTable.Columns(headerfld).Ordinal + 1 _
                                       , String.Format(MsgReader.GetString("E09030160"), "Header", "", headerfld) _
                                       , 0, True)
                End If

            Next

            For Each row As DataRow In _PreviewTable.Rows

                'Validate Mandatory Payment Fields 
                For Each _column As String In New String() {"File Type-P", "Seq. No.-P", "Transaction Type", "Transaction Amount", "Notice By", "Bene Address 1 / Payee Address 1", "Customer Reference 1 / Detail of Payment 1", "VAT Amount-P", "Discount Amount", "Invoice Amount-P", "Commission Charge To"}

                    If IsNothingOrEmptyString(row(_column).ToString()) Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                            , RowNumber + 1, True)
                    End If

                Next

                'Payment Field 4, 7, 8 and 26 are required when Field 3 is '1', '2', '3' , '4' or 'A' 

                Select Case row("Transaction Type").ToString()
                    Case "1", "2", "3", "4", "A"
                        If IsNothingOrEmptyString(row("Beneficiary Bank Branch IFSC Code").ToString()) Then
                            AddValidationError("Beneficiary Bank Branch IFSC Code", _PreviewTable.Columns("Beneficiary Bank Branch IFSC Code").Ordinal + 1 _
                                             , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Bank Branch IFSC Code", "'Transaction Type' is '1', '2', '3' , '4' or 'A'") _
                                             , RowNumber + 1, True)
                        End If

                        If IsNothingOrEmptyString(row("Beneficiary A/C No.").ToString()) Then
                            AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary A/C No.", "'Transaction Type' is '1', '2', '3' , '4' or 'A'") _
                                            , RowNumber + 1, True)
                        End If

                        If IsNothingOrEmptyString(row("Beneficiary Name").ToString()) Then
                            AddValidationError("Beneficiary Name", _PreviewTable.Columns("Beneficiary Name").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Name", "'Transaction Type' is '1', '2', '3', '4' or 'A'") _
                                                , RowNumber + 1, True)
                        End If
                        'modified @ 09/11/2010
                        'change request after UAT
                        'If IsNothingOrEmptyString(row("Mailing Name").ToString()) Then
                        '    AddValidationError("Mailing Name", _PreviewTable.Columns("Mailing Name").Ordinal + 1 _
                        '                        , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Mailing Name", "'Transaction Type' is '1', '2', '3', '4' or 'A'") _
                        '                        , RowNumber + 1, True)
                        'End If

                    Case "5", "6", "7"
                        If IsNothingOrEmptyString(row("Payee Name-1").ToString()) Then
                            AddValidationError("Payee Name-1", _PreviewTable.Columns("Payee Name-1").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Payee Name-1", "'Transaction Type' is '5', '6' or '7'") _
                                             , RowNumber + 1, True)
                        End If

                        If IsNothingOrEmptyString(row("Bene Address 3 / Payable At").ToString()) Then
                            AddValidationError("Bene Address 3 / Payable At", _PreviewTable.Columns("Bene Address 3 / Payable At").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Bene Address 3 / Payable At", "'Transaction Type' is '5', '6' or '7'") _
                                             , RowNumber + 1, True)
                        End If
                End Select

                'Payment Field either 15 or 16 is required when Field 3 is '2' or 'A'
                'Both 15 & 16  can't be existed together
                If row("Transaction Type").ToString() = "2" OrElse row("Transaction Type").ToString() = "A" Then
                    If IsNothingOrEmptyString(row("Applicant Mobile No").ToString()) AndAlso IsNothingOrEmptyString(row("Applicant E-mail Address").ToString()) Then
                        AddValidationError("Applicant Mobile No", _PreviewTable.Columns("Applicant Mobile No").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09060030"), "Record", RowNumber + 1, "Applicant Mobile No", "Applicant E-mail Address", "'Transaction Type' is '2' or 'A'") _
                                            , RowNumber + 1, True)
                    End If

                    If Not IsNothingOrEmptyString(row("Applicant Mobile No").ToString()) AndAlso Not IsNothingOrEmptyString(row("Applicant E-mail Address").ToString()) Then
                        AddValidationError("Applicant Mobile No", _PreviewTable.Columns("Applicant Mobile No").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09060050"), "Record", RowNumber + 1, "Applicant Mobile No", "Applicant E-mail Address", "'Transaction Type' is '2' or 'A'") _
                                            , RowNumber + 1, True)
                    End If
                End If


                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type, Conditional Mandatory and Custom Validations 
    '(other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()


        Dim SetupTransAmt As Decimal
        Dim found As Boolean = False
        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim strField As String = String.Empty
        Dim _ValueDate As Date
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
        Dim regAllowNum As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim regEmail As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled)
        Dim _compareDate As String = ""
        Dim _compareACNo As String = ""
        Dim regAllowSpecialAlphaNum As New Regex("[^0-9A-Za-z., -]", RegexOptions.Compiled)

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                For Each col As String In TblBankFields.Keys
                    If IsNothingOrEmptyString(_previewRecord(col)) Then _previewRecord(col) = ""
                    If _previewRecord(col) Is DBNull.Value Then _previewRecord(col) = ""
                Next

                '########
                'Get Bank Sample Value for Transaction Amount Filtering
                'There should be set up only one value 
                '########
                If Not TblBankFields("Transaction Amount").BankSampleValue Is Nothing Then
                    Decimal.TryParse(TblBankFields("Transaction Amount").BankSampleValue, SetupTransAmt)
                End If


                If RowNumber = 0 Then

                    'Validate Header Records
                    '#1. File Type is incorrect. It must be 'H'.
                    If (_previewRecord("File Type-H") & "").ToString().ToUpper() <> "H" Then
                        AddValidationError("File Type-H", _PreviewTable.Columns("File Type-H").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Header", "", "File Type-H", "'H'") _
                                            , 0, True)
                    End If

                    If _previewRecord("Account No") <> "" Then
                        If _previewRecord("Account No").ToString().Length <> 19 Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09050040"), "Header", "", "Account No", 19) _
                                        , 0, True)
                        End If
                        If regAllowAlphaNum.IsMatch(_previewRecord("Account No").ToString()) Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060020"), "Header", "", "Account No") _
                                                    , 0, True)
                        End If
                        If Not _previewRecord("Account No").ToString().EndsWith("INR") Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060011"), "Header", "", "Account No") _
                                                    , 0, True)
                        End If
                        'For A/C Number comparison
                        _compareACNo = _previewRecord("Account No").ToString()
                    End If

                    If _previewRecord("Value Date") <> "" Then
                        If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), TblBankFields("Value Date").DateFormat, True) Then
                            _ValueDate = DateTime.ParseExact(_previewRecord("Value Date").ToString(), TblBankFields("Value Date").DateFormat.Replace("YY", "yy").Replace("DD", "dd"), Nothing)
                            '_value date must be between current date and current date + 21 days
                            If _ValueDate < Date.Today OrElse _ValueDate > Date.Today.AddDays(21) Then
                                AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09030072"), "Header", "", "Value Date"), 0, True)
                            End If
                        Else
                            AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Header", "", "Value Date"), 0, True)
                        End If

                        'For Date Value comparison
                        _compareDate = _previewRecord("Value Date").ToString()

                    End If

                    Select Case _previewRecord("Debit Type").ToString.Trim
                        Case "0", "1"
                        Case Else
                            AddValidationError("Debit Type", _PreviewTable.Columns("Debit Type").Ordinal + 1, String.Format(MsgReader.GetString("E09060040"), "Header", "", "Debit Type"), 0, True)
                    End Select

                    If _previewRecord("Fax Area Code") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Fax Area Code").ToString()) Then
                            AddValidationError("Fax Area Code", _PreviewTable.Columns("Fax Area Code").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Fax Area Code") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("Fax No.") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Fax No.").ToString()) Then
                            AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Fax No.") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("E-mail Address") <> "" Then 'valid internet email address
                        If Not regEmail.IsMatch(_previewRecord("E-mail Address").ToString()) Then
                            AddValidationError("E-mail Address", _PreviewTable.Columns("E-mail Address").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030022"), "Header", "", "E-mail Address") _
                                                    , 0, True)
                        Else
                            'check double @@ in Email address
                            If (_previewRecord("E-mail Address").ToString().IndexOf(",") > 1 OrElse _previewRecord("E-mail Address").ToString().IndexOf(";") > 1) Then
                            Else
                                If _previewRecord("E-mail Address").ToString().Split("@").Length > 2 Then 'OrElse _previewRecord("E-mail Address").ToString().IndexOf("@@") > 0
                                    AddValidationError("E-mail Address", _PreviewTable.Columns("E-mail Address").Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09030022"), "Header", "", "E-mail Address") _
                                                            , 0, True)
                                End If
                            End If
                        End If

                    End If
                    If _previewRecord("Contact Person") <> "" Then 'alphanumeric
                        If regAllowAlphaNum.IsMatch(_previewRecord("Contact Person").ToString().Replace(" ", "")) Then
                            AddValidationError("Contact Person", _PreviewTable.Columns("Contact Person").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060020"), "Header", "", "Contact Person") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("Customer ID") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Customer ID").ToString()) Then
                            AddValidationError("Customer ID", _PreviewTable.Columns("Customer ID").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Customer ID") _
                                                    , 0, True)
                        End If
                    End If

                    'Total Amount should not contain more than 2 decimal places in the header
                    If _previewRecord("Total Amount") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("Total Amount"), dAmount) Then
                            AddValidationError(strField, _PreviewTable.Columns("Total Amount").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Total Amount") _
                                            , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Total Amount", _PreviewTable.Columns("Total Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Header", "", "Total Amount") _
                                                        , RowNumber + 1, True)
                                End If
                            End If

                        End If
                    End If

                    ''#2. First Record must be P Record
                    'If Not IsPRecord Then
                    '    AddValidationError("File Type-P", _PreviewTable.Columns("File Type-P").Ordinal + 1 _
                    '                        , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Type-P", "'P'") _
                    '                        , RowNumber + 1, True)
                    'End If
                End If


                'Start Payment Record Fields validation
                '#1. 'File Type-P' is incorrect. It must be 'P'.
                If _previewRecord("File Type-P").ToString() <> "" AndAlso _previewRecord("File Type-P").ToString().ToUpper() <> "P" Then
                    AddValidationError("File Type-P", _PreviewTable.Columns("File Type-P").Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Type-P", "'P'") _
                                        , RowNumber + 1, True)
                End If

                '#3. "Transaction Type" must be one of   1, 2, 3, 4, 5, 6, 7, A
                If _previewRecord("Transaction Type") <> "" Then
                    Select Case _previewRecord("Transaction Type")
                        Case "1", "2", "3", "4", "5", "6", "7", "A"
                            'valid data
                        Case Else
                            AddValidationError("Transaction Type", _PreviewTable.Columns("Transaction Type").Ordinal + 1 _
                                                                              , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Transaction Type") _
                                                                              , RowNumber + 1, True)
                    End Select

                End If

                '#4. "Beneficiary Bank Branch IFSC Code"
                If _previewRecord("Transaction Type") <> "" Then
                    If _previewRecord("Transaction Type") = "3" Or _previewRecord("Transaction Type") = "4" Then
                        If _previewRecord("Beneficiary Bank Branch IFSC Code") = "" OrElse (Not _previewRecord("Beneficiary Bank Branch IFSC Code").ToString().StartsWith("BOTM")) Then
                            AddValidationError("Beneficiary Bank Branch IFSC Code", _PreviewTable.Columns("Beneficiary Bank Branch IFSC Code").Ordinal + 1 _
                                                                                                              , String.Format(MsgReader.GetString("E09050061"), "Record", RowNumber + 1, "Beneficiary Bank Branch IFSC Code", "Transaction Type is '3' or '4'") _
                                                                                                              , RowNumber + 1, True)
                        End If
                    End If
                End If

                'change request
                'Check alphanumeric
                If regAllowAlphaNum.IsMatch(_previewRecord("Beneficiary Bank Branch IFSC Code").ToString()) Then
                    AddValidationError("Beneficiary Bank Branch IFSC Code", _PreviewTable.Columns("Beneficiary Bank Branch IFSC Code").Ordinal + 1 _
                                                                                                      , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Beneficiary Bank Branch IFSC Code") _
                                                                                                      , RowNumber + 1, True)
                End If

                '#7. Beneficiary A/C No. must be alphanumeric
                If _previewRecord("Beneficiary A/C No.") <> "" Then
                    Select Case _previewRecord("Transaction Type")
                        'change request
                        'during UAT
                        'modified by Kay @ 19 Oct 2010
                        'to check first position is hyphen, if it's found, raise error!
                        Case "2"
                            If regAllowSpecialAlphaNum.IsMatch(_previewRecord("Beneficiary A/C No.").ToString()) Then
                                AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060021"), "Record", RowNumber + 1, "Beneficiary A/C No.", "'Transaction Type' is 2") _
                                                    , RowNumber + 1, True)
                            End If

                            If _previewRecord("Beneficiary A/C No.").ToString().StartsWith("-") Then
                                AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060022"), "Record", RowNumber + 1, "Beneficiary A/C No.", "'Transaction Type' is 2") _
                                                    , RowNumber + 1, True)
                            End If
                        Case "1", "3", "4", "A"
                            If regAllowAlphaNum.IsMatch(_previewRecord("Beneficiary A/C No.").ToString()) Then
                                AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Beneficiary A/C No.") _
                                                    , RowNumber + 1, True)
                            End If
                        Case Else
                            'nothing
                    End Select

                    'For TransType is Book, then the value must not exceed 6
                    'UAT change request, to validate for transaction type-4 too.
                    'updated by kay @ 20/10/2010
                    If (_previewRecord("Transaction Type") = "3" OrElse _previewRecord("Transaction Type") = "4") AndAlso _previewRecord("Beneficiary A/C No.").ToString.Length > 6 Then
                        AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09020040"), "Record", RowNumber + 1, "Beneficiary A/C No.", "6 digits") _
                                        , RowNumber + 1, True)

                    End If

                End If
                '#-#-#

                '#9. Transaction Amount Must be numeric
                If _previewRecord("Transaction Amount") <> "" Then 'numbers only
                    If Not Decimal.TryParse(_previewRecord("Transaction Amount").ToString(), dAmount) Then
                        AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Transaction Amount"), RowNumber + 1, True)

                    Else
                        'If dAmount <= 0 Then 'Should not less than or equal to zero                             
                        '    AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1 _
                        '                        , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Transaction Amount") _
                        '                        , RowNumber + 1, True)
                        'End If
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Transaction Amount") _
                                                    , RowNumber + 1, True)
                            End If
                        End If

                        'Check when Transaction Type is 'RTGS'
                        If _previewRecord("Transaction Type") = "1" AndAlso dAmount < SetupTransAmt Then
                            AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1 _
                                                                               , String.Format(MsgReader.GetString("E09050073"), "Record", RowNumber + 1, "Transaction Amount", SetupTransAmt, "'Transaction Type' is '1'") _
                                                                               , RowNumber + 1, True)
                        End If
                    End If
                End If

                '#13. "Notice By" must be one of   01, 02, 03, 04,
                If _previewRecord("Notice By") <> "" Then
                    Select Case _previewRecord("Notice By").ToString()
                        Case "01", "02", "03", "04"
                            'valid data
                        Case Else
                            AddValidationError("Notice By", _PreviewTable.Columns("Notice By").Ordinal + 1 _
                                                                              , String.Format(MsgReader.GetString("E09030101"), "Record", RowNumber + 1, _previewRecord("Notice By").ToString(), "Notice By") _
                                                                              , RowNumber + 1, True)
                    End Select
                End If

                '#14. Beneficiary Fax Area Code must be numeric
                If regAllowNum.IsMatch(_previewRecord("Beneficiary Fax Area Code").ToString()) Then
                    AddValidationError("Beneficiary Fax Area Code", _PreviewTable.Columns("Beneficiary Fax Area Code").Ordinal + 1 _
                                     , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Beneficiary Fax Area Code") _
                                     , RowNumber + 1, True)
                End If

                '#15. Applicant Mobile No must be numeric
                If regAllowNum.IsMatch(_previewRecord("Applicant Mobile No").ToString()) Then
                    AddValidationError("Applicant Mobile No", _PreviewTable.Columns("Applicant Mobile No").Ordinal + 1 _
                                     , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Applicant Mobile No") _
                                     , RowNumber + 1, True)
                End If

                'Change request
                'must be exactly 10 digits
                If Not IsNothingOrEmptyString(_previewRecord("Applicant Mobile No")) AndAlso _previewRecord("Applicant Mobile No").ToString().Length <> 10 Then
                    AddValidationError("Applicant Mobile No", _PreviewTable.Columns("Applicant Mobile No").Ordinal + 1 _
                                , String.Format(MsgReader.GetString("E09050041"), "Record", RowNumber + 1, "Applicant Mobile No", 10) _
                                , RowNumber + 1, True)
                End If


                '#16. Applicant E-mail Address
                If _previewRecord("Applicant E-mail Address") <> "" Then 'valid internet email address
                    If Not regEmail.IsMatch(_previewRecord("Applicant E-mail Address").ToString()) Then
                        AddValidationError("Applicant E-mail Address", _PreviewTable.Columns("Applicant E-mail Address").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "Applicant E-mail Address") _
                                                , RowNumber + 1, True)
                    End If

                    'check double/triple @ signs in email address
                    If _previewRecord("Applicant E-mail Address").ToString().Split("@").Length > 2 OrElse _previewRecord("Applicant E-mail Address").ToString().IndexOf("@@") > 0 Then
                        AddValidationError("Applicant E-mail Address", _PreviewTable.Columns("Applicant E-mail Address").Ordinal + 1 _
                                         , String.Format(MsgReader.GetString("E09030023"), "Record", RowNumber + 1, "Applicant E-mail Address") _
                                          , RowNumber + 1, True)
                    End If

                    'during UAT, Requested by BTMU to check with ';' and '@' too.
                    If _previewRecord("Applicant E-mail Address").ToString().Split(",").Length > 1 OrElse _previewRecord("Applicant E-mail Address").ToString().Split(";").Length > 1 Then
                        AddValidationError("Applicant E-mail Address", _PreviewTable.Columns("Applicant E-mail Address").Ordinal + 1 _
                                         , String.Format(MsgReader.GetString("E09030023"), "Record", RowNumber + 1, "Applicant E-mail Address") _
                                          , RowNumber + 1, True)
                    End If
                End If

                '#22. VAT Amount-P Must be numeric
                If _previewRecord("VAT Amount-P") <> "" Then 'numbers only
                    If Not Decimal.TryParse(_previewRecord("VAT Amount-P").ToString(), dAmount) Then
                        AddValidationError("VAT Amount-P", _PreviewTable.Columns("VAT Amount-P").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "VAT Amount-P") _
                                            , RowNumber + 1, True)
                    Else
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("VAT Amount-P", _PreviewTable.Columns("VAT Amount-P").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "VAT Amount-P") _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#23. Discount Amount Must be numeric
                If _previewRecord("Discount Amount") <> "" Then 'numbers only
                    If Not Decimal.TryParse(_previewRecord("Discount Amount").ToString(), dAmount) Then
                        AddValidationError("Discount Amount", _PreviewTable.Columns("Discount Amount").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Discount Amount") _
                                            , RowNumber + 1, True)
                    Else
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Discount Amount", _PreviewTable.Columns("Discount Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Discount Amount") _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#24. Invoice Amount-P Must be numeric
                If _previewRecord("Invoice Amount-P") <> "" Then 'numbers only
                    If Not Decimal.TryParse(_previewRecord("Invoice Amount-P").ToString(), dAmount) Then
                        AddValidationError("Invoice Amount-P", _PreviewTable.Columns("Invoice Amount-P").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Invoice Amount-P") _
                                                , RowNumber + 1, True)
                    Else
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Invoice Amount-P", _PreviewTable.Columns("Invoice Amount-P").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Invoice Amount-P") _
                                                    , RowNumber + 1, True)
                            End If
                        End If

                    End If
                End If


                '#25. "Commission Charge To" must be A or B
                If _previewRecord("Commission Charge To") <> "" Then
                    Select Case _previewRecord("Commission Charge To")
                        Case "A", "B"
                            'valid data
                        Case Else
                            AddValidationError("Commission Charge To)", _PreviewTable.Columns("Commission Charge To").Ordinal + 1 _
                                                                              , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Commission Charge To") _
                                                                              , RowNumber + 1, True)
                    End Select
                End If

                '### Check for | character in any of the fields
                For Each _column As String In TblBankFields.Keys
                    If Not IsNothingOrEmptyString(_previewRecord(_column)) Then
                        If _previewRecord(_column).ToString.Contains("|") Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09020100"), IIf(TblBankFields(_column).Header, "Header", "Record") _
                                                , RowNumber + 1, _column), RowNumber + 1, True)
                        End If
                    End If
                Next

                'Added for Diff A/C NO checking
                '*************************************
                If Not _previewRecord("Account No").ToString.Equals(_compareACNo) Then
                    AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060012"), "Record", RowNumber + 1, "Account No") _
                                                    , RowNumber + 1, True)
                End If

                'Added for Diff Value Date checking
                '*************************************
                If Not _previewRecord("Value Date").ToString.Equals(_compareDate) Then
                    _IsSameDateValue = False
                    'Throw New System.Exception(String.Format(MsgReader.GetString("E04000281"), rowNo + 1, col.ColumnName, ErrorPrefix))               
                End If

                RowNumber = RowNumber + 1

            Next



        Catch ex As Exception
            Throw New MagicException("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        If _PreviewTable.Rows.Count = 0 Then Exit Sub

        Dim totalAmount As Decimal = 0
        Dim _rowIndex As Integer = 0
        Dim _out As Decimal

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Transaction Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Transaction Amount"))
            If Not Decimal.TryParse(_row("Transaction Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Transaction Amount"))
            'If _row("Discount Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Discount Amount"))
            'If Not Decimal.TryParse(_row("Discount Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Discount Amount"))


            ' Take the absolute value of Transaction Amount 
            If Not IsNothingOrEmptyString(_row("Transaction Amount")) Then
                If Decimal.TryParse(_row("Transaction Amount"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Transaction Amount") = (Math.Abs(_out) / 100).ToString
                    Else
                        _row("Transaction Amount") = Math.Abs(_out).ToString
                    End If
                End If
            End If

            '#-discount amount
            If Not IsNothingOrEmptyString(_row("Discount Amount")) Then
                If Decimal.TryParse(_row("Discount Amount"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Discount Amount") = (_out / 100).ToString
                    Else
                        _row("Discount Amount") = (_out).ToString
                    End If
                End If
            End If

            '#-VAT amount-P
            If Not IsNothingOrEmptyString(_row("VAT Amount-P")) Then
                If Decimal.TryParse(_row("VAT Amount-P"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("VAT Amount-P") = (_out / 100).ToString
                    Else
                        _row("VAT Amount-P") = (_out).ToString
                    End If
                End If
            End If

            '#Invoice amount-P
            If Not IsNothingOrEmptyString(_row("Invoice Amount-P")) Then
                If Decimal.TryParse(_row("Invoice Amount-P"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Invoice Amount-P") = (_out / 100).ToString
                    Else
                        _row("Invoice Amount-P") = (_out).ToString
                    End If
                End If
            End If
            '#-#-#

            'calculate total amount
            totalAmount += Convert.ToDecimal(_row("Transaction Amount").ToString)

        Next

        '#3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        ''#4. Preserve Header Fields
        'Dim headervalues As New List(Of String)
        'If _PreviewTable.Rows.Count > 0 Then
        '    For Each headerfld As String In _headerfields
        '        headervalues.Add(_PreviewTable.Rows(0)(headerfld).ToString())
        '    Next
        'End If


        Try

            Dim _cnt As Int32 = 1
            'Join: Payment and Invoice Tables => Preview Table           
            'JoinPaymentRecords()

            'Restore the Header Fields
            'If _PreviewTable.Rows.Count > 0 Then
            '    _cnt = 0
            '    For Each headerfld As String In _headerfields
            '        _PreviewTable.Rows(0)(headerfld) = headervalues(_cnt)
            '        _cnt += 1
            '    Next
            'End If

            'Update Sequence No.
            'Format : 0000#

            _cnt = 1
            For Each row As DataRow In _PreviewTable.Rows
                row("Seq. No.-P") = _cnt.ToString("0000#")
                _cnt += 1
                'update total amount
                row("Total Amount") = FormatAmount(totalAmount)
            Next


            'This is to format the calculated Amount fields
            FormatOutput()

        Catch ex As Exception
            Throw New MagicException("Error while generating output records: " & ex.Message.ToString)
        End Try

    End Sub
    Protected Overrides Sub GenerateOutputFormat1()

        If _PreviewTable.Rows.Count = 0 Then Exit Sub

        Dim totalAmount As Decimal = 0
        Dim _rowIndex As Integer = 0
        Dim _out As Decimal

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Transaction Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Transaction Amount"))
            If Not Decimal.TryParse(_row("Transaction Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Transaction Amount"))
            'If _row("Discount Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Discount Amount"))
            'If Not Decimal.TryParse(_row("Discount Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Discount Amount"))


            ' Take the absolute value of Transaction Amount 
            If Not IsNothingOrEmptyString(_row("Transaction Amount")) Then
                If Decimal.TryParse(_row("Transaction Amount"), _out) Then
                    'it's a bug found during UAT
                    'everytime checks and repeat again
                    'commented by Kay @ 12 Oct 2010
                    'If _DataModel.IsAmountInCents Then
                    '    _row("Transaction Amount") = (Math.Abs(_out) / 100).ToString
                    'Else
                    _row("Transaction Amount") = Math.Abs(_out).ToString
                    'End If
                End If
            End If

            '#-discount amount
            If Not IsNothingOrEmptyString(_row("Discount Amount")) Then
                If Decimal.TryParse(_row("Discount Amount"), _out) Then
                    'If _DataModel.IsAmountInCents Then
                    '    _row("Discount Amount") = (_out / 100).ToString
                    'Else
                    _row("Discount Amount") = (_out).ToString
                    'End If
                End If
            End If

            '#-VAT amount-P
            If Not IsNothingOrEmptyString(_row("VAT Amount-P")) Then
                If Decimal.TryParse(_row("VAT Amount-P"), _out) Then
                    'If _DataModel.IsAmountInCents Then
                    '    _row("VAT Amount-P") = (_out / 100).ToString
                    'Else
                    _row("VAT Amount-P") = (_out).ToString
                    'End If
                End If
            End If

            '#Invoice amount-P
            If Not IsNothingOrEmptyString(_row("Invoice Amount-P")) Then
                If Decimal.TryParse(_row("Invoice Amount-P"), _out) Then
                    'If _DataModel.IsAmountInCents Then
                    '    _row("Invoice Amount-P") = (_out / 100).ToString
                    'Else
                    _row("Invoice Amount-P") = (_out).ToString
                    'End If
                End If
            End If
            '#-#-#

            'calculate total amount
            totalAmount += Convert.ToDecimal(_row("Transaction Amount").ToString)

        Next

        '#3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        ''#4. Preserve Header Fields
        'Dim headervalues As New List(Of String)
        'If _PreviewTable.Rows.Count > 0 Then
        '    For Each headerfld As String In _headerfields
        '        headervalues.Add(_PreviewTable.Rows(0)(headerfld).ToString())
        '    Next
        'End If


        Try

            Dim _cnt As Int32 = 1
            'Join: Payment and Invoice Tables => Preview Table           
            'JoinPaymentRecords()

            'Restore the Header Fields
            'If _PreviewTable.Rows.Count > 0 Then
            '    _cnt = 0
            '    For Each headerfld As String In _headerfields
            '        _PreviewTable.Rows(0)(headerfld) = headervalues(_cnt)
            '        _cnt += 1
            '    Next
            'End If

            'Update Sequence No.
            'Format : 0000#

            _cnt = 1
            For Each row As DataRow In _PreviewTable.Rows
                row("Seq. No.-P") = _cnt.ToString("0000#")
                _cnt += 1
                'update total amount
                row("Total Amount") = FormatAmount(totalAmount)
            Next


            'This is to format the calculated Amount fields
            FormatOutput()

        Catch ex As Exception
            Throw New MagicException("Error while generating output records: " & ex.Message.ToString)
        End Try

    End Sub

    'Consolidation is not applicable to this file format
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        If GroupByFields.Count = 0 Then Exit Sub

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If

            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        '''''''''' End of Previous Argument Setting '''''''''''''


        Dim filterString As String = String.Empty
        Dim filterList As New List(Of String)
        Dim transAmt As Decimal = 0
        Dim referenceField(1) As String
        Dim rowIndex As Integer = 0
        Dim totalAmount As Decimal

        Try

            _consolidatedDataView = _PreviewTable.DefaultView ' DataView to apply rowfilter
            _consolidatedData = New DataTable ' DataTable to hold only consolidated records
            ' Create the columns for the Consolidated DataTable
            For Each _column As DataColumn In _PreviewTable.Columns
                _consolidatedData.Columns.Add(_column.ColumnName, GetType(String))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '1. Filter out unique rows for given Filter Group
            For Each _row As DataRow In _PreviewTable.Rows

                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_row(_field).ToString()))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For

                If filterList.IndexOf(filterString) = -1 Then
                    filterList.Add(filterString)
                    filterList.Sort()
                End If

            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next


            '2. Set of fields to be aggregated/consolidated while applying the Grouping
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            totalAmount = 0
            For Each _filter As String In filterList
                _consolidatedDataView.RowFilter = String.Empty
                _consolidatedDataView.RowFilter = _filter

                If _consolidatedDataView.Count > 0 Then

                    ' Create the reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1) <> "" Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next

                    transAmt = 0
                    For Each _viewRow As DataRowView In _consolidatedDataView
                        transAmt += Convert.ToDecimal(_viewRow(_amountFieldName).ToString)
                        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                            If _referenceFields(intI, 1).Trim.Length <> 0 Then
                                referenceField(intI) = referenceField(intI) & _viewRow(_referenceFields(intI, 1).ToString) & ","
                            End If
                        Next
                    Next
                End If

                Dim consolidatedRow As DataRow
                consolidatedRow = _consolidatedData.NewRow
                _consolidatedDataView.Item(0)(_amountFieldName) = transAmt.ToString

                For Each _column As String In TblBankFields.Keys
                    consolidatedRow(_column) = _consolidatedDataView(0)(_column)
                    consolidatedRow(_amountFieldName) = transAmt.ToString

                Next

                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
                        consolidatedRow(_referenceFields(intI, 1)) = referenceField(intI).Substring(0, referenceField(intI).Length - 1)
                    End If
                Next

                _consolidatedData.Rows.Add(consolidatedRow)
                totalAmount += transAmt
            Next

            '_totalRecordCount = _PreviewTable.Rows.Count
            'If _consolidatedData.Rows.Count > 0 Then
            '    _consolidatedRecordCount = _consolidatedData.Rows.Count
            '    _PreviewTable.Rows.Clear()
            '    _PreviewTable = _consolidatedData.Copy
            'Else
            '    _consolidatedRecordCount = 0
            'End If

            '***************************************************
            'Added for Header Record
            '***************************************************
            _totalRecordCount = _PreviewTable.Rows.Count
            If _consolidatedData.Rows.Count > 0 Then

                ' Change the Header Field Values to the record No : 1 of the source file
                _consolidatedData.Rows(0)("File Type-H") = _PreviewTable.Rows(0)("File Type-H").ToString
                _consolidatedData.Rows(0)("Account No") = _PreviewTable.Rows(0)("Account No").ToString
                _consolidatedData.Rows(0)("Value Date") = _PreviewTable.Rows(0)("Value Date").ToString
                _consolidatedData.Rows(0)("No. of Record") = _consolidatedData.Rows.Count
                _consolidatedData.Rows(0)("Total Amount") = FormatAmount(totalAmount)
                _consolidatedData.Rows(0)("Debit Type") = _PreviewTable.Rows(0)("Debit Type").ToString
                _consolidatedData.Rows(0)("Fax Area Code") = _PreviewTable.Rows(0)("Fax Area Code").ToString
                _consolidatedData.Rows(0)("Fax No.") = _PreviewTable.Rows(0)("Fax No.").ToString
                _consolidatedData.Rows(0)("E-mail Address") = _PreviewTable.Rows(0)("E-mail Address").ToString
                _consolidatedData.Rows(0)("Contact Person") = _PreviewTable.Rows(0)("Contact Person").ToString
                _consolidatedData.Rows(0)("Customer ID") = _PreviewTable.Rows(0)("Customer ID").ToString
                _consolidatedData.Rows(0)("Batch ID") = _PreviewTable.Rows(0)("Batch ID").ToString
                _consolidatedData.Rows(0)("Reserve-H") = _PreviewTable.Rows(0)("Reserve-H").ToString
                _consolidatedRecordCount = _consolidatedData.Rows.Count

                'Modified by Kay @ 12 Oct 2010
                'During UAT, BTMU requested to reorder Seq no. after consolidation
                'Regenerate seq. no.
                rowIndex = 1
                For Each row As DataRow In _consolidatedData.Rows
                    row("Seq. No.-P") = rowIndex.ToString("0000#")
                    rowIndex += 1
                Next

                _PreviewTable.Rows.Clear()
                _PreviewTable = _consolidatedData.Copy

            Else
                _consolidatedRecordCount = 0
            End If



            ValidationErrors.Clear()
            GenerateHeader()
            GenerateTrailer()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()

        Catch ex As Exception
            Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
        End Try

    End Sub

    'The output file consists of
    '1)	All Fields are separated by |, no enclosure characters
    '2)	One header and one-to-many Payment Records 

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sTaxAmts As String = String.Empty
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim omakaseOutput2FileName As String = String.Empty

        Dim objMasterTemplate As MasterTemplateNonFixed ' Non Fixed Type Master Template
        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            '1. File Format Type
            GenerateHeader()
            sText = _Header

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                '2. Payment Record
                If _previewRecord("File Type-P").ToString() = "P" Then

                    If _previewRecord("Transaction Amount").ToString() <> "" Then
                        If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                            _previewRecord("Transaction Amount") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Transaction Amount"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("Transaction Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Transaction Amount"))), 2).ToString("############0.00")
                        End If
                    End If

                    For Each _column As String In _fieldsPaymentRecord

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", "|") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", "|") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                End If

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Dim encoutputfile As String = _outputFile
            'Rename the file name
            If objMasterTemplate.EncryptCustomerOutputFile Then
                If _outputFile.Length > objMasterTemplate.CustomerOutputExtension.Length Then
                    _outputFile = _outputFile.Substring(0, _outputFile.Length - (objMasterTemplate.CustomerOutputExtension.Length + 1)) & "FYI." & objMasterTemplate.CustomerOutputExtension
                Else
                    _outputFile = "FYI." & objMasterTemplate.CustomerOutputExtension
                End If
            End If
            'Temporarily save in Plain Format 
            SaveTextToFile(sText, _outputFile)


            'Encrypt with GnuPg 1.4.7 
            'Save the encrypted Copy
            If objMasterTemplate.EncryptCustomerOutputFile Then
                ' EncryptOmakase(GnuPg147, GnuPubKey, argInputFile, argOutputFile)
                EncryptOmakase(GnuPG147, _outputFile, encoutputfile)

                'Modified by Kay @ 07 Oct 2010
                'During UAT, BTMU asked to generate a plain text file if Encrypt Output file is unchecked.
                'So move the below codes into If condition statement
                Dim outFileInfo As New FileInfo(_outputFile)
                If outFileInfo.Exists Then outFileInfo.Delete()

            End If

            Return True

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Amount Field has invalid numeric value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    Private Function FormatAmount(ByVal amount As Decimal) As String

        Dim formattedAmount As String = String.Empty

        'Amount fields should not be formatted if there are more than 2 decimal places.
        'Total Amount
        'Transaction Amount
        'VAT Amount-P
        'Discount Amount
        'Invoice Amount-P


        If amount.ToString().IndexOf(".") > 0 Then 'Should not format if  more than 2 decimal places
            If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                formattedAmount = (amount).ToString("#0.00")
            Else
                formattedAmount = amount
            End If
        Else
            formattedAmount = (amount).ToString("#0.00")
        End If

        Return formattedAmount

    End Function
#End Region

#Region "Helper Methods"


    'Initialize Payment/Tax/Invoice Tables
    Private Sub SetupPTable()

        _tblPayment = New DataTable("TPayment")
        _tblPayment.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsPaymentRecord
            _tblPayment.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

    End Sub

    'Private Sub ReplaceWith(ByVal TextVal As String, ByVal FindBy As String, ByVal ReplaceBy As String, Optional ByVal PositionVal As Short = 0)

    '    Dim strSplit() As String
    '    Dim PayeeNames As New List(Of String)
    '    Dim PayeeName2StartPosition As Integer

    '    PayeeNames.Add("")
    '    PayeeNames.Add("")


    '    strSplit = VenName.Split(" ")
    '    For i As Integer = 0 To strSplit.Length - 1
    '        If strSplit(i).Length <= StringLength Then
    '            If PayeeNames(0) = String.Empty Then
    '                PayeeNames(0) = strSplit(i)
    '            Else
    '                If String.Concat(PayeeNames(0), " ", strSplit(i)).Length <= StringLength Then
    '                    PayeeNames(0) = String.Concat(PayeeNames(0), " ", strSplit(i))
    '                Else
    '                    PayeeName2StartPosition = i
    '                    Exit For
    '                End If
    '            End If
    '        End If
    '    Next

    '    For i As Integer = PayeeName2StartPosition To strSplit.Length - 1
    '        If PayeeNames(1) = String.Empty Then
    '            PayeeNames(1) = strSplit(i)
    '        Else
    '            PayeeNames(1) = String.Concat(PayeeNames(1), " ", strSplit(i))
    '        End If
    '    Next
    '    'Return PayeeNames
    'End Sub

    'gpg --output c:\OMAKASE20100105133416.txt -er OmakaseIndia  c:\OMAKASE20100105133416FYI.txt
    Private Sub EncryptOmakase(ByVal GnuPg147 As String, ByVal argInputFile As String, _
                                ByVal argOutputFile As String _
                                )
        Dim homedirectory As String = GnuPg147.Replace("\gpg.exe", "")
        'to generate ASCII output, modified by kay @ 07 Oct 2010 (added --armor)
        Dim argSign As String = " --output """ & argOutputFile & """ -er OmakaseIndia  --armor """ & argInputFile & """"

        argSign = "--homedir """ & homedirectory & """" & argSign
        Dim proc As Process


        proc = Process.Start(GnuPg147, argSign)
        proc.WaitForExit()
        If proc.ExitCode <> 0 Then Throw New Exception("Failed to encrypt output file!")
        proc.Close()
        proc.Dispose()


    End Sub

    Private Function ReplaceNonPermitCharWithSpace(ByVal fieldstr As String) As String
        'non-permitted characters
        '{	}	!	@	$	#	%	^	&	*	=	~	<	>	;	�	\	[	]	|	_
        Dim NonPermitChars() As Char = {Chr(123), Chr(125), Chr(33), Chr(64), Chr(36), Chr(35), Chr(37), _
                                        Chr(94), Chr(38), Chr(42), Chr(61), Chr(126), Chr(60), Chr(62), _
                                        Chr(59), Chr(34), Chr(92), Chr(91), Chr(93), Chr(124), Chr(95)}
        Dim tmpstr As String = String.Empty

        tmpstr = fieldstr

        'For i As Int32 = 0 To tmpstr.Length
        For j As Int16 = 0 To NonPermitChars.Length - 1
            'If tmpstr(i).Equals(NonPermitChars(j)) Then
            'replace it with a space
            tmpstr = tmpstr.Replace(NonPermitChars(j), Chr(32))
            'End If
        Next
        'Next

        ReplaceNonPermitCharWithSpace = tmpstr

    End Function


#End Region

End Class