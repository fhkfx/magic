Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the iFTSFormat
''' </summary>
''' <remarks></remarks>
Public Class iFTSFormat
    Inherits BaseFileFormat

    Private Shared _amountFieldName As String = "Gross Amount"

    'In this format, editable fields, group by fields and reference fields are same
    Private Shared _editableFields() As String = {"Beneficiary Name", "Account No.", "Bank Code" _
                    , "Branch Code", "Less Charge", "Service Type", "Gross Amount" _
                    , "Fax Notice", "Fax No.", "Message To Beneficiary", "W/H Tax No." _
                    , "Name (TH)", "Address (TH)", "Tax ID", "No. in the form", "Tax Form" _
                    , "The Payer of Income", "Other Payer of Income", "Type of Income" _
                    , "Other Type of Income", "Date", "Amount Paid", "WHT Amount"}

    Private Shared _autoTrimmableFields() As String = {"Beneficiary Name", "Message To Beneficiary" _
                    , "W/H Tax No.", "Name (TH)", "Address (TH)", "Other Payer of Income", "Other Type of Income"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iFTS"
        End Get
    End Property

#End Region

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Public Shared Function OutputFormatString() As String
        Return "iFTS"
    End Function

#Region "Overriden Methods"

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        For Each row As DataRow In _PreviewTable.Rows

            If row("Account No.") <> "" Then row("Account No.") = row("Account No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "").Trim()

            If row("Bank Code").ToString.Trim <> "" Then row("Bank Code") = row("Bank Code").ToString().PadLeft(TblBankFields("Bank Code").DataLength.Value, "0")

            If row("Branch Code").ToString.Trim <> "" Then row("Branch Code") = row("Branch Code").ToString().PadLeft(TblBankFields("Branch Code").DataLength.Value, "0")

            If row("Service Type").ToString.Trim <> "" Then row("Service Type") = row("Service Type").ToString().PadLeft(TblBankFields("Service Type").DataLength.Value, "0")

            If row("Fax No.") <> "" Then row("Fax No.") = row("Fax No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "").Trim()

            If row("No. in the form") <> "" Then row("No. in the form") = row("No. in the form").ToString().Replace(",", "").Replace(".", "")

            'Amount fields will be formatted without thousand separator
            If row("Gross Amount") <> "" AndAlso Decimal.TryParse(row("Gross Amount").ToString(), Nothing) Then
                row("Gross Amount") = (Convert.ToDecimal(row("Gross Amount"))).ToString("#0.00")
            End If


            If row("Amount Paid") <> "" AndAlso Decimal.TryParse(row("Amount Paid").ToString(), Nothing) Then
                row("Amount Paid") = (Convert.ToDecimal(row("Amount Paid"))).ToString("#0.00")
            End If


            If row("WHT Amount") <> "" AndAlso Decimal.TryParse(row("WHT Amount").ToString(), Nothing) Then
                row("WHT Amount") = (Convert.ToDecimal(row("WHT Amount"))).ToString("#0.00")
            End If

        Next

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Date") = ValueDate.ToString("yyMMdd", HelperModule.enUSCulture)

            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Validates Mandatory Fields. Field #11-23 are optional, if none of them have value
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0
        Dim WHTaxFields As String = ""
        Dim colIdx As Int32 = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'Determine if the fields 11 to 23 need to be validated
                WHTaxFields = ""
                For colIdx = 10 To 22
                    WHTaxFields = WHTaxFields & _previewRecord(colIdx).ToString()
                Next

                colIdx = 0

                For Each _column As String In TblBankFields.Keys

                    If WHTaxFields = "" AndAlso colIdx >= 10 Then Continue For

                    If (TblBankFields(_column).Mandatory OrElse IsMandatoryTaxField(_column)) AndAlso IsNothingOrEmptyString(_previewRecord(_column).ToString()) Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1, String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column), RowNumber + 1, True)
                    End If

                    colIdx += 1

                Next ' Iteration for each column of table 

                RowNumber += 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New Exception("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        For Each row As DataRow In _PreviewTable.Rows

            If row("Gross Amount") <> "" AndAlso Decimal.TryParse(row("Gross Amount").ToString(), Nothing) Then
                If _DataModel.IsAmountInCents Then row("Gross Amount") = (Convert.ToDecimal(row("Gross Amount")) / 100).ToString("#0.00")
            End If

            If row("Amount Paid") <> "" AndAlso Decimal.TryParse(row("Amount Paid").ToString(), Nothing) Then
                If _DataModel.IsAmountInCents Then row("Amount Paid") = (Convert.ToDecimal(row("Amount Paid")) / 100).ToString("#0.00")
            End If

            If row("WHT Amount") <> "" AndAlso Decimal.TryParse(row("WHT Amount").ToString(), Nothing) Then
                If _DataModel.IsAmountInCents Then row("WHT Amount") = (Convert.ToDecimal(row("WHT Amount")) / 100).ToString("#0.00")
            End If

        Next

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim pattern As String = "[^0-9\s]"
        Dim regAllow As New Regex(pattern, RegexOptions.Compiled)
        Dim regAllow2 As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim _ValueDate As Date
        Dim found As Boolean = False

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                ' #1. Account No. should only contain numeric values
                If _previewRecord("Account No.") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Account No.").ToString()) Then
                        AddValidationError("Account No.", _PreviewTable.Columns("Account No.").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Account No."), RowNumber + 1, True)
                    End If
                End If

                '#2. Less Charge - either "BEN" or "OUR"
                If _previewRecord("Less Charge") <> "" Then
                    If Not (_previewRecord("Less Charge").ToString().ToUpper() = "BEN" _
                        Or _previewRecord("Less Charge").ToString().ToUpper() = "OUR") Then
                        AddValidationError("Less Charge", _PreviewTable.Columns("Less Charge").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Less Charge"), RowNumber + 1, True)
                    End If
                End If

                '#3. Service Type - must be one of {"01", "02", "03", "04", "05", "06", "07", "59"}
                If _previewRecord("Service Type") <> "" Then
                    found = False
                    For Each _validSvcType As String In New String() {"01", "02", "03", "04", "05", "06", "07", "59"}
                        If _previewRecord("Service Type").ToString() = _validSvcType Then
                            found = True
                            Exit For
                        End If
                    Next
                    If Not found Then
                        Dim validationError As New FileFormatError()
                        AddValidationError("Service Type", _PreviewTable.Columns("Service Type").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Service Type"), RowNumber + 1, True)
                    End If
                End If

                '#4. Gross Amount
                If _previewRecord("Gross Amount") <> "" Then
                    If Decimal.TryParse(_previewRecord("Gross Amount"), dAmount) Then
                        'If Not IsConsolidated AndAlso dAmount <= 0 Then
                        '    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1)
                        'End If

                    Else
                        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1, True)
                    End If
                End If

                '#5. Fax Notice - Must be one of these ('F', 'N' or blank)
                If _previewRecord("Fax Notice") <> "" Then
                    If Not (_previewRecord("Fax Notice").ToString().ToUpper() = "F" _
                        Or _previewRecord("Fax Notice").ToString().ToUpper() = "N") Then
                        AddValidationError("Fax Notice", _PreviewTable.Columns("Fax Notice").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Fax Notice"), RowNumber + 1, True)
                    End If
                End If

                '#6. Fax No.
                If _previewRecord("Fax Notice") <> "" AndAlso _previewRecord("Fax Notice").ToString().ToUpper() = "F" Then
                    If IsNothingOrEmptyString(_previewRecord("Fax No.")) Then
                        'mandatory field is missing
                        AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Fax No.", "'Fax Notice' is 'F' or 'f'"), RowNumber + 1, True)
                    End If
                End If
                If Not IsNothingOrEmptyString(_previewRecord("Fax No.")) Then
                    If _previewRecord("Fax No.").ToString.Length < 8 OrElse _previewRecord("Fax No.").ToString.Length > 10 Then
                        'length error
                        AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1, String.Format(MsgReader.GetString("E09050020"), "Record", RowNumber + 1, "Fax No."), RowNumber + 1, True)
                    End If
                    If regAllow2.IsMatch(_previewRecord("Fax No.").ToString()) Then 'numeric values only
                        AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1, String.Format(MsgReader.GetString("E09050030"), "Record", RowNumber + 1, "Fax No."), RowNumber + 1, True)
                    End If
                End If


                '#7. Tax ID - Allow numeric values only
                If _previewRecord("Tax ID") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Tax ID").ToString()) Then
                        AddValidationError("Tax ID", _PreviewTable.Columns("Tax ID").Ordinal + 1, String.Format(MsgReader.GetString("E09050030"), "Record", RowNumber + 1, "Tax ID"), RowNumber + 1, True)
                    End If

                    If _previewRecord("Tax ID").ToString().Length <> 10 Then
                        AddValidationError("Tax ID", _PreviewTable.Columns("Tax ID").Ordinal + 1, String.Format(MsgReader.GetString("E09050040"), "Record", RowNumber + 1, "Tax ID", 10), RowNumber + 1, True)
                    End If
                End If

                '#8. No. in the form - Positive integer from 1 to 9999999999
                If _previewRecord("No. in the form") <> "" Then
                    If Decimal.TryParse(_previewRecord("No. in the form").ToString(), dAmount) Then
                        If dAmount < 1 OrElse dAmount > 9999999999D Then
                            AddValidationError("No. in the form", _PreviewTable.Columns("No. in the form").Ordinal + 1, String.Format(MsgReader.GetString("E09050050"), "Record", RowNumber + 1, "No. in the form", "1", "9999999999"), RowNumber + 1, True)
                        End If
                    Else
                        'Not a number
                        AddValidationError("No. in the form", _PreviewTable.Columns("No. in the form").Ordinal + 1, String.Format(MsgReader.GetString("E09050030"), "Record", RowNumber + 1, "No. in the form"), RowNumber + 1, True)
                    End If
                End If

                '#9. Tax Form - Must be one of these ('1','2','3','4','5','6','7')
                If _previewRecord("Tax Form") <> "" Then
                    If Decimal.TryParse(_previewRecord("Tax Form").ToString(), dAmount) Then
                        If dAmount > 7 OrElse dAmount < 1 Then
                            AddValidationError("Tax Form", _PreviewTable.Columns("Tax Form").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Tax Form"), RowNumber + 1, True)
                        End If
                    Else
                        AddValidationError("Tax Form", _PreviewTable.Columns("Tax Form").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Tax Form"), RowNumber + 1, True)
                    End If
                End If

                '#10. The Payer of Income - Must be one of these values: ('1', '2', '3', '4') 
                ' and  If its Then '4' Other Payer of Income is mandatory
                If Not IsNothingOrEmptyString(_previewRecord("The Payer of Income")) Then

                    If Decimal.TryParse(_previewRecord("The Payer of Income").ToString(), dAmount) Then
                        If dAmount > 4 OrElse dAmount < 1 Then
                            AddValidationError("The Payer of Income", _PreviewTable.Columns("The Payer of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "The Payer of Income"), RowNumber + 1, True)
                        End If
                    Else
                        AddValidationError("The Payer of Income", _PreviewTable.Columns("The Payer of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "The Payer of Income"), RowNumber + 1, True)
                    End If

                    If _previewRecord("The Payer of Income").ToString() = "4" Then
                        If IsNothingOrEmptyString(_previewRecord("Other Payer of Income")) Then
                            AddValidationError("Other Payer of Income", _PreviewTable.Columns("Other Payer of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Other Payer of Income", "'The Payer of Income' is '4'"), RowNumber + 1, True)
                        End If

                    Else
                        'Not a number
                        If _previewRecord("Other Payer of Income").ToString() <> "" Then
                            AddValidationError("Other Payer of Income", _PreviewTable.Columns("Other Payer of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09050060"), "Record", RowNumber + 1, "Other Payer of Income", "'The Payer of Income' is not '4'"), RowNumber + 1, True)
                        End If
                    End If

                End If

                '#11. Type of Income - Must be one of these values ('1','2','3','41','42','43','44','45','46','47','51','6')
                'If its '6' Other Type of Income is mandatory
                If Not IsNothingOrEmptyString(_previewRecord("Type of Income")) Then
                    found = False
                    For Each _validSvcType As String In New String() {"1", "2", "3", "41", "42", "43", "44", "45", "46", "47", "51", "6"}
                        If _previewRecord("Type of Income").ToString() = _validSvcType Then
                            found = True
                            Exit For
                        End If
                    Next

                    If Not found Then
                        AddValidationError("Type of Income", _PreviewTable.Columns("Type of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Type of Income"), RowNumber + 1, True)
                    End If

                    If _previewRecord("Type of Income").ToString() = "6" Then
                        If IsNothingOrEmptyString(_previewRecord("Other Type of Income")) Then
                            AddValidationError("Other Type of Income", _PreviewTable.Columns("Other Type of Income").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Other Type of Income", "'Type of Income' is '6'"), RowNumber + 1, True)
                        End If
                    End If
                End If


                '#12. Date - must be valid date
                If _previewRecord("Date").ToString() <> "" Then
                    If Not HelperModule.IsDateDataType(_previewRecord("Date").ToString(), "", "yyMMdd", _ValueDate, True) Then
                        AddValidationError("Date", _PreviewTable.Columns("Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Date"), RowNumber + 1, True)
                    End If
                End If


                '#13. WHT Amount <= Amount Paid
                If _previewRecord("Amount Paid") <> "" AndAlso Decimal.TryParse(_previewRecord("Amount Paid").ToString(), Nothing) AndAlso _previewRecord("WHT Amount") <> "" AndAlso Decimal.TryParse(_previewRecord("WHT Amount").ToString(), Nothing) Then
                    If Decimal.Parse(_previewRecord("Amount Paid")) < Decimal.Parse(_previewRecord("WHT Amount")) Then
                        AddValidationError("WHT Amount", _PreviewTable.Columns("WHT Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030120"), "Record", RowNumber + 1, "WHT Amount", "Amount Paid"), RowNumber + 1, True)
                    End If
                Else

                    If _previewRecord("Amount Paid") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("Amount Paid").ToString(), Nothing) Then
                            AddValidationError("Amount Paid", _PreviewTable.Columns("Amount Paid").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Amount Paid"), RowNumber + 1, True)
                        End If
                    End If

                    If _previewRecord("WHT Amount") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("WHT Amount").ToString(), Nothing) Then
                            AddValidationError("WHT Amount", _PreviewTable.Columns("WHT Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "WHT Amount"), RowNumber + 1, True)
                        End If
                    End If

                End If

                '#14. Bank Code - should be a valid numeric value
                If _previewRecord("Bank Code") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Bank Code").ToString()) Then
                        AddValidationError("Bank Code", _PreviewTable.Columns("Bank Code").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Bank Code"), RowNumber + 1, True)
                    End If
                End If

                '#15. Branch Code - should be a valid numeric value
                If _previewRecord("Branch Code") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Branch Code").ToString()) Then
                        AddValidationError("Branch Code", _PreviewTable.Columns("Branch Code").Ordinal + 1, String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Branch Code"), RowNumber + 1, True)
                    End If
                End If

                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Gross Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Gross Amount"))
            If Not Decimal.TryParse(_row("Gross Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Gross Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Gross Amount")

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        Dim objMasterTemplate As MasterTemplateNonFixed  ' Non Fixed Type Master Template

        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In TblBankFields.Keys

                    If _column.Equals("Gross Amount", StringComparison.InvariantCultureIgnoreCase) Then
                        If _previewRecord(_column).ToString.StartsWith("-") Then
                            _previewRecord(_column) = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(_column))), 2).ToString("############0.00")
                        Else
                            _previewRecord(_column) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(_column))), 2).ToString("############0.00")
                        End If
                    End If

                    sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            sText = vbNewLine & sText

            Return SaveTextToFile(sText, _outputFile)

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Field: 'Gross Amount' has invalid value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If

            End If
            If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                _ErrorAtMandatoryField = True
            End If
            RowNumber += 1

        Next

    End Sub

#End Region

    Private Function IsMandatoryTaxField(ByVal strColumn As String) As Boolean

        For Each _column As String In New String() {"W/H Tax No.", "Name (TH)", "Address (TH)", "Tax ID", "Tax Form", "Type of Income", "Date", "Amount Paid", "WHT Amount"}
            If _column.Equals(strColumn, StringComparison.InvariantCultureIgnoreCase) Then Return True
        Next

        Return False

    End Function


End Class