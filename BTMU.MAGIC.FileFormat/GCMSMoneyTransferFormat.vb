
Public Interface IFileFormat

    Property ConvertedData() As DataTable
    Function ValidateData() As FileFormatErrorCollection

End Interface


Public Class GCMSMoneyTransferFileFormat
    Implements IFileFormat


#Region " Private properties "

    Private _settlementacno As DataColumn
    Private _valuedate As DataColumn
    Private _customerreference As DataColumn
    Private _sectorselection As DataColumn
    Private _currency As DataColumn
    Private _remittanceamount As DataColumn
    Private _exchangemethod As DataColumn
    Private _contractnumber As DataColumn
    Private _intermediarybank As DataColumn
    Private _intermediarybankmastercode As DataColumn
    Private _beneficiarybank As DataColumn
    Private _beneficiarybankmastercode As DataColumn
    Private _beneficiaryacno As DataColumn
    Private _beneficiaryname As DataColumn
    Private _messagetobeneficiary As DataColumn
    Private _purposeofremittance As DataColumn
    Private _informationtoremittingbank As DataColumn
    Private _bankcharges As DataColumn
    Private _chargesaccountno As DataColumn
    Private _temp1 As DataColumn
    Private _temp2 As DataColumn
    Private _temp3 As DataColumn
    Private _localcurrency As DataColumn
    Private _localcountry As DataColumn

    Private _gcmsmoneytransfertable As New DataTable("GCMSMoneyTransferTable")

#End Region


    Public Property ConvertedData() As DataTable Implements IFileFormat.ConvertedData
        Get
            Return _gcmsmoneytransfertable
        End Get
        Set(ByVal value As DataTable)
            _gcmsmoneytransfertable = value
        End Set
    End Property



    Public Sub New()

        _settlementacno = New DataColumn("Settlement A/C No", GetType(String)) ' "Settlement A/C No.", 34, "Text", True, 1, DataColumnType.Detail)

        _valuedate = New DataColumn("Value Date", GetType(Date)) ' "Value Date.", 6, "Date", True, 2, DataColumnType.Detail)

        _customerreference = New DataColumn("Customer Reference", GetType(String)) ', "Customer Reference", 16, "Text", True, 3, DataColumnType.Detail)

        _sectorselection = New DataColumn("Sector Selection", GetType(String))  ', "Sector Selection.", 13, "Text", True, 4, DataColumnType.Detail)

        _currency = New DataColumn("Currency", GetType(String)) ', "Currency.", 3, "Text", True, 5, DataColumnType.Detail)

        _remittanceamount = New DataColumn("Remittance Amount", GetType(Decimal)) ', "Remittance Amount.", 15, "Number", True, 6, DataColumnType.Detail)

        _exchangemethod = New DataColumn("Exchange Method", GetType(String)) ', "Exchange Method.", 4, "Text", True, 7, DataColumnType.Detail)

        _contractnumber = New DataColumn("Contract Number", GetType(String)) ', "Contract Number.", 15, "Text", False, 8, DataColumnType.Detail)

        _intermediarybank = New DataColumn("Intermediary Bank", GetType(String)) ', "Intermediary Bank.", 140, "Text", False, 9, DataColumnType.Detail)
        _intermediarybankmastercode = New DataColumn("Intermediary Bank Master Code", GetType(String)) ', "Intermediary Bank Master Code.", 8, "Text", False, 10, DataColumnType.Detail)
        _beneficiarybank = New DataColumn("Beneficiary Bank", GetType(String)) ' "Beneficiary Bank.",, 140, "Text", False, 11, DataColumnType.Detail)
        _beneficiarybankmastercode = New DataColumn("Beneficiary Bank Master Code", GetType(String)) ', "Beneficiary Bank Master Code.", 8, "Text", False, 12, DataColumnType.Detail)
        _beneficiaryacno = New DataColumn("Beneficiary A/C No", GetType(String)) ', "Beneficiary A/C No.", 34, "Text", True, 13, DataColumnType.Detail)
        _beneficiaryname = New DataColumn("Beneficiary Name", GetType(String)) ', "Beneficiary Name.", 140, "Text", True, 14, DataColumnType.Detail)
        _messagetobeneficiary = New DataColumn("Message To Beneficiary", GetType(String)) ', "Message To Beneficiary.", 140, "Text", False, 15, DataColumnType.Detail)
        _purposeofremittance = New DataColumn("Purpose of Remittance", GetType(String)) ', "Purpose of Remittance.", 22, "Text", False, 16, DataColumnType.Detail)
        _informationtoremittingbank = New DataColumn("Information To Remitting Bank", GetType(String)) ', "Information To Remitting Bank.", 105, "Text", False, 17, DataColumnType.Detail)

        _bankcharges = New DataColumn("Bank Charges", GetType(String)) ', "Bank Charges.", 3, "Text", True, 18, DataColumnType.Detail)
        _chargesaccountno = New DataColumn("Charges Account Number", GetType(String)) ', "Charges Account Number.", 34, "Text", False, 19, DataColumnType.Detail)

        _temp1 = New DataColumn("Temp1", GetType(String)) ', "Temp1.", 100, "Text", False, 20, DataColumnType.Detail)
        _temp2 = New DataColumn("Temp2", GetType(String)) ', "Temp2.", 100, "Text", False, 21, DataColumnType.Detail)
        _temp3 = New DataColumn("Temp3", GetType(String)) ', "Temp3.", 100, "Text", False, 22, DataColumnType.Detail)

        _localcurrency = New DataColumn("Local Currency", GetType(String)) ', "Local Currency.", 50, "Text", False, 23, DataColumnType.Detail)
        _localcountry = New DataColumn("Local Country", GetType(String)) ', "Local Country.", 50, "Text", False, 24, DataColumnType.Detail)

        'Add all fields to table
        _gcmsmoneytransfertable.Columns.Add(_settlementacno)
        _gcmsmoneytransfertable.Columns.Add(_valuedate)
        _gcmsmoneytransfertable.Columns.Add(_customerreference)
        _gcmsmoneytransfertable.Columns.Add(_sectorselection)
        _gcmsmoneytransfertable.Columns.Add(_currency)
        _gcmsmoneytransfertable.Columns.Add(_remittanceamount)
        _gcmsmoneytransfertable.Columns.Add(_exchangemethod)
        _gcmsmoneytransfertable.Columns.Add(_contractnumber)
        _gcmsmoneytransfertable.Columns.Add(_intermediarybank)
        _gcmsmoneytransfertable.Columns.Add(_intermediarybankmastercode)
        _gcmsmoneytransfertable.Columns.Add(_beneficiarybank)
        _gcmsmoneytransfertable.Columns.Add(_beneficiarybankmastercode)
        _gcmsmoneytransfertable.Columns.Add(_beneficiaryacno)
        _gcmsmoneytransfertable.Columns.Add(_beneficiaryname)
        _gcmsmoneytransfertable.Columns.Add(_messagetobeneficiary)
        _gcmsmoneytransfertable.Columns.Add(_purposeofremittance)
        _gcmsmoneytransfertable.Columns.Add(_informationtoremittingbank)
        _gcmsmoneytransfertable.Columns.Add(_bankcharges)
        _gcmsmoneytransfertable.Columns.Add(_chargesaccountno)
        _gcmsmoneytransfertable.Columns.Add(_temp1)
        _gcmsmoneytransfertable.Columns.Add(_temp2)
        _gcmsmoneytransfertable.Columns.Add(_temp3)
        _gcmsmoneytransfertable.Columns.Add(_localcurrency)
        _gcmsmoneytransfertable.Columns.Add(_localcountry)

    End Sub

    Public Function ValidateData() As FileFormatErrorCollection Implements IFileFormat.ValidateData

        Dim Errors As New FileFormatErrorCollection()

        For Each row As DataRow In _gcmsmoneytransfertable.Rows

            ' validate the fields of this record and add file format errors to Errors object

        Next

        Return Errors

    End Function

End Class


'Public Function GCMSGeneration(ByVal sInputSource As String)
'    Dim i As Integer
'    Dim bFieldExchangeMethodExist As Boolean
'    Dim bFieldContractNoExist As Boolean
'    bFieldExchangeMethodExist = False
'    bFieldContractNoExist = False

'    For i = 0 To frmConversion.rsConvert.Fields.count - 1
'        If UCase(Trim(frmConversion.rsConvert.Fields(i).Name)) = UCase("Exchange Method") Then
'            bFieldExchangeMethodExist = True
'        End If
'        If UCase(Trim(frmConversion.rsConvert.Fields(i).Name)) = UCase("Contract Number") Then
'            bFieldContractNoExist = True
'        End If
'    Next

'    frmConversion.rsConvert.MoveFirst()
'    While Not frmConversion.rsConvert.EOF
'        'Fill Field Exchange Method
'        If bFieldExchangeMethodExist = True And bFieldContractNoExist = True Then
'            If UCase(Mid(Trim(frmConversion.rsConvert.Fields("Settlement Account No")), Len(Trim(frmConversion.rsConvert.Fields("Settlement Account No"))) - 2, 3)) = UCase(Trim(frmConversion.rsConvert.Fields("Currency"))) Then
'                frmConversion.rsConvert.Fields("Exchange Method") = ""
'            Else
'                If Trim(frmConversion.rsConvert.Fields("Contract Number")) = "" Then
'                    frmConversion.rsConvert.Fields("Exchange Method") = "Spot"
'                Else
'                    frmConversion.rsConvert.Fields("Exchange Method") = "Cont"
'                End If
'            End If

'            If UCase(Trim(frmConversion.rsConvert.Fields("Bank Charges"))) = UCase("Ben") Then
'                frmConversion.rsConvert.Fields("Charges Account Number") = ""
'            End If

'            If frmConversion.rsConvert.Fields("Exchange Method") = "" Or frmConversion.rsConvert.Fields("Exchange Method") = "Spot" Then
'                frmConversion.rsConvert.Fields("Contract Number") = ""
'            End If
'        End If
'        'Remittance Amount
'        If frmConversion.rsConvert.Fields("Remittance Amount") <> "" Then
'            If frmConversion.optCent Then
'                frmConversion.rsConvert.Fields("Remittance Amount") = CCur(frmConversion.rsConvert.Fields("Remittance Amount") / 100)
'            End If
'            'Remittance Amount Format
'            Select Case Trim(frmConversion.rsConvert.Fields("Currency"))
'                Case "TND", "KWD"
'                    frmConversion.rsConvert.Fields("Remittance Amount") = Format(Round(frmConversion.rsConvert.Fields("Remittance Amount"), 3), "###########0.000")

'                Case "TWD", "JPY", "WON"
'                    frmConversion.rsConvert.Fields("Remittance Amount") = Format(Round(frmConversion.rsConvert.Fields("Remittance Amount")), "###########0")

'                Case Else
'                    frmConversion.rsConvert.Fields("Remittance Amount") = Format(Round(frmConversion.rsConvert.Fields("Remittance Amount"), 2), "###########0.00")
'            End Select
'        End If
'        'SWIFT Character Validation
'        For i = 0 To 18
'            If i <> 1 And i <> 5 Then
'                frmConversion.rsConvert.Fields(i) = SWIFTCharacterValidation(frmConversion.rsConvert.Fields(i))
'            End If
'        Next

'        frmConversion.rsConvert.Update()
'        frmConversion.rsConvert.MoveNext()
'    End While
'End Function

'Public Function GCMSValidations(ByVal sInputSource As String, ByVal iBaris As Integer, ByVal strError As String, ByVal iCtrError As Integer)
'    iBaris = 1
'    frmConversion.rsConvert.MoveFirst()
'    While Not frmConversion.rsConvert.EOF
'        'Beneficiary Bank
'        If Mid(frmConversion.rsConvert.Fields(10), 1, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(10), 1, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(10), 36, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(10), 36, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(10), 71, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(10), 71, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(10), 106, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(10), 106, 1) = ":" Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(10).Name & "'" & vbTab & " cannot contain hyphen[-] or colon[:] at positions 1/36/71/106." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 11) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Beneficiary Name
'        If Mid(frmConversion.rsConvert.Fields(13), 1, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(13), 1, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(13), 36, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(13), 36, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(13), 71, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(13), 71, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(13), 106, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(13), 106, 1) = ":" Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(13).Name & "'" & vbTab & " cannot contain hyphen[-] or colon[:] at positions 1/36/71/106." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 14) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Message to Beneficiary
'        If Mid(frmConversion.rsConvert.Fields(14), 1, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(14), 1, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(14), 36, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(14), 36, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(14), 71, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(14), 71, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(14), 106, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(14), 106, 1) = ":" Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(14).Name & "'" & vbTab & " cannot contain hyphen[-] or colon[:] at positions 1/36/71/106." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 15) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Information to Remitting Bank
'        If Mid(frmConversion.rsConvert.Fields(16), 1, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(16), 1, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(16), 36, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(16), 36, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(16), 71, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(16), 71, 1) = ":" Or _
'            Mid(frmConversion.rsConvert.Fields(16), 106, 1) = "-" Or Mid(frmConversion.rsConvert.Fields(16), 106, 1) = ":" Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(16).Name & "'" & vbTab & " cannot contain hyphen[-] or colon[:] at positions 1/36/71/106." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 17) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Purpose of Remittance and Information to Remitting Bank
'        If frmConversion.rsConvert.Fields(15) <> "" And Len(frmConversion.rsConvert.Fields(16)) > 70 Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(16).Name & "'" & vbTab & " cannot contain more than 70 characters when 'Purpose of Remittance' is given." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 17) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Charges Account No.
'        If (UCase(frmConversion.rsConvert.Fields(17)) = UCase("OUR") Or UCase(frmConversion.rsConvert.Fields(17)) = UCase("SHA")) And frmConversion.rsConvert.Fields(18) = frmConversion.rsConvert.Fields(0) Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(18).Name & "'" & vbTab & " cannot be same as the 'Settlement Account No.' when 'Bank Charges' is 'OUR' or 'SHA'." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 19) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If
'        'Remittance Amount
'        If Trim(frmConversion.rsConvert.Fields("Remittance Amount")) <> "" And Val(Trim(frmConversion.rsConvert.Fields("Remittance Amount"))) <= 0 Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields("Remittance Amount").Name & "'" & vbTab & " should not be zero or negative." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 6) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            '    frmConversion.bErrorMandatotyField = True
'        End If
'        'Remittance Amount
'        If Trim(frmConversion.rsConvert.Fields("Remittance Amount")) <> "" And Len(Trim(frmConversion.rsConvert.Fields("Remittance Amount"))) > GetFieldLength("Remittance Amount") Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields("Remittance Amount").Name & "'" & vbTab & " exceeds the specified length." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 6) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            '    frmConversion.bErrorMandatotyField = True
'        End If
'        'Value Date
'        On Error GoTo ValueDateError
'        Dim dtValueDate
'        dtValueDate = DateSerial(CInt(Mid(frmConversion.rsConvert.Fields(1), 1, 2)), CInt(Mid(frmConversion.rsConvert.Fields(1), 3, 2)), CInt(Mid(frmConversion.rsConvert.Fields(1), 5, 2)))
'        If DateDiff("d", Now(), dtValueDate) < 0 Or DateDiff("d", Now(), dtValueDate) > 21 Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(1).Name & "'" & vbTab & " should be equal to or greater than today and less than 21 calendar days from today's date." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 2) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'        End If

'ValueDateError:
'        If Err.Number = 13 Then
'            strError = strError & "Line: " & iBaris & vbTab & " Field: '" & frmConversion.rsConvert.Fields(1).Name & "'" & vbTab & " should be a valid date." & vbCrLf
'            frmPreviewTransaction.CommonFlexGrid.Cell(flexcpBackColor, iBaris, 2) = RGB(77, 236, 72)
'            iCtrError = iCtrError + 1
'            frmConversion.bErrorMandatotyField = True
'            Err.Clear()
'        End If

'        iBaris = iBaris + 1
'        frmConversion.rsConvert.MoveNext()
'    End While
'End Function



 