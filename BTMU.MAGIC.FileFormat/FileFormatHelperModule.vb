Imports BTMU.MAGIC.CommonTemplate

Module FileFormatHelperModule

    Private _previewDataTable As DataTable

    ' Reference Field Setting
    ' Filter Field Setting
    ' Format Source Data

#Region " Format Source Data "

    Public Function FormatSourceData(ByVal sourceDataTable As DataTable, ByVal objCommonTemplate As CommonTemplateExcelDetail) As DataTable
        Try
            _previewDataTable = New DataTable
            ' Create the columns for the Preview Table
            For Each bankField As MapBankField In objCommonTemplate.MapBankFields
                Dim dupBankField As New MapBankField()
                bankField.CopyTo(dupBankField)
                _previewDataTable.Columns.Add(New DataColumn(dupBankField.BankField, GetType(String)))
            Next

            ' Apply Mapping and Generate the Source records for preview
            Mapping(sourceDataTable, objCommonTemplate)

            ' Apply Translation
            Translation(sourceDataTable, objCommonTemplate)

            ' Apply Calculation
            Calculation(sourceDataTable.DefaultView, objCommonTemplate)

            ' Apply String Manipulation
            StringManipulation(sourceDataTable.DefaultView, objCommonTemplate)

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while Translating Bank Fields!, Error Details : {0}", ex.Message))
        End Try

        Return _previewDataTable

    End Function
#End Region

#Region " Apply Mapping "

    Public Sub Mapping(ByVal sourceDatatable As DataTable, ByVal objCommonTemplateExcel As CommonTemplate.CommonTemplateExcelDetail)
        Dim newFormatRow As DataRow
        For Each sourceDataRow As DataRow In sourceDatatable.Rows
            newFormatRow = _previewDataTable.NewRow

            For Each bankField As MapBankField In objCommonTemplateExcel.MapBankFields
                For Each sourceField As MapSourceField In bankField.MappedSourceFields
                    sourceField.SourceFieldValue = sourceDataRow(sourceField.SourceFieldName).ToString
                Next
                newFormatRow(bankField.BankField) = bankField.MappedValues
            Next
            _previewDataTable.Rows.Add(newFormatRow)
        Next
    End Sub

    
    Public Sub Mapping(ByVal sourceDataTable As DataTable, ByVal objCommonTemplateText As CommonTemplateTextDetail)
        Dim newFormatRow As DataRow

        For Each sourceDataRow As DataRow In sourceDataTable.Rows
            newFormatRow = _previewDataTable.NewRow

            For Each bankField As MapBankField In objCommonTemplateText.MapBankFields
                For Each sourceField As MapSourceField In bankField.MappedSourceFields
                    sourceField.SourceFieldValue = sourceDataRow(sourceField.SourceFieldName).ToString
                Next
                newFormatRow(bankField.BankField) = bankField.MappedValues
            Next
            _previewDataTable.Rows.Add(newFormatRow)
        Next
    End Sub

#End Region

#Region " Apply Translation "

    Public Sub Translation(ByVal sourceDataTable As DataTable, ByVal objCommonTemplateText As CommonTemplateTextDetail)
        Try
            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each translatedBankField As TranslatorSetting In objCommonTemplateText.TranslatorSettings

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If _previewRecord(translatedBankField.BankFieldName) = translatedBankField.CustomerValue Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while Translating Bank Fields!, Error Details : {0}", ex.Message))
        End Try
    End Sub

   
    Public Sub Translation(ByVal sourceDataTable As DataTable, ByVal objCommonTemplateExcel As CommonTemplateExcelDetail)
        Try
            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each translatedBankField As TranslatorSetting In objCommonTemplateExcel.TranslatorSettings

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If _previewRecord(translatedBankField.BankFieldName) = translatedBankField.CustomerValue Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while Translating Bank Fields!, Error Details : {0}", ex.Message))
        End Try

    End Sub

#End Region

#Region " Apply Calculation "

   
    Public Sub Calculation(ByVal sourceDataTable As DataView, ByVal objCommonTemplateText As CommonTemplateTextDetail)
        Try

            Dim _calvalue As Decimal = 0, rowIndex As Int32 = 0

            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each calculatedBankField As CalculatedField In objCommonTemplateText.CalculatedFields

                    If calculatedBankField.Operator1 = "+" Then
                        _calvalue = System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand1)) _
                                    + System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand2))
                    Else
                        _calvalue = System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand1)) _
                                    - System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand2))
                    End If
                    If calculatedBankField.Operator2 = "+" Then
                        _calvalue = _calvalue + System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "-" Then
                        _calvalue = _calvalue - System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand3))
                    End If

                    _previewRecord(calculatedBankField.BankFieldName) = _calvalue.ToString()

                Next

                rowIndex = rowIndex + 1
            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while processing Calculation Settings!, Error Details : {0}", ex.Message))
        End Try

    End Sub

  
    Public Sub Calculation(ByVal sourceDataTable As DataView, ByVal objCommonTemplateExcel As CommonTemplateExcelDetail)
        Try

            Dim _calvalue As Decimal = 0, rowIndex As Int32 = 0

            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each calculatedBankField As CalculatedField In objCommonTemplateExcel.CalculatedFields

                    If calculatedBankField.Operator1 = "+" Then
                        _calvalue = System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand1)) _
                                    + System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand2))
                    Else
                        _calvalue = System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand1)) _
                                    - System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand2))
                    End If
                    If calculatedBankField.Operator2 = "+" Then
                        _calvalue = _calvalue + System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "-" Then
                        _calvalue = _calvalue - System.Convert.ToDecimal(sourceDataTable(rowIndex)(calculatedBankField.Operand3))
                    End If

                    _previewRecord(calculatedBankField.BankFieldName) = _calvalue.ToString()

                Next

                rowIndex = rowIndex + 1
            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while processing Calculation Settings!, Error Details : {0}", ex.Message))
        End Try

    End Sub

#End Region

#Region " String Manipulation "

 
    Public Sub StringManipulation(ByVal sourceDataTable As DataView, ByVal objCommonTemplateText As CommonTemplateTextDetail)
        Try
            Dim _svalue As String

            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each manipulatedBankField As StringManipulation In objCommonTemplateText.StringManipulations

                    _svalue = _previewRecord(manipulatedBankField.BankFieldName)

                    If _svalue = String.Empty Then Continue For

                    Select Case manipulatedBankField.FunctionName

                        Case "LEFT"
                            _svalue = _svalue.Substring(0, manipulatedBankField.NumberOfCharacters.Value)
                        Case "RIGHT"
                            _svalue = _svalue.Substring(_svalue.Length - (manipulatedBankField.NumberOfCharacters.Value - 1), manipulatedBankField.NumberOfCharacters.Value)
                        Case "SUBSTRING"
                            _svalue = _svalue.Substring(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)
                        Case "REMOVE"
                            _svalue = _svalue.Remove(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)

                    End Select

                    _previewRecord(manipulatedBankField.BankFieldName) = _svalue

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while processing String Manipulation Settings!, Error Details : {0}", ex.Message))
        End Try
    End Sub

   
    Public Sub StringManipulation(ByVal sourceDataTable As DataView, ByVal objCommonTemplateExcel As CommonTemplateExcelDetail)
        Try
            Dim _svalue As String

            For Each _previewRecord As DataRow In _previewDataTable.Rows

                For Each manipulatedBankField As StringManipulation In objCommonTemplateExcel.StringManipulations

                    _svalue = _previewRecord(manipulatedBankField.BankFieldName)

                    If _svalue = String.Empty Then Continue For

                    Select Case manipulatedBankField.FunctionName

                        Case "LEFT"
                            _svalue = _svalue.Substring(0, manipulatedBankField.NumberOfCharacters.Value)
                        Case "RIGHT"
                            _svalue = _svalue.Substring(_svalue.Length - (manipulatedBankField.NumberOfCharacters.Value - 1), manipulatedBankField.NumberOfCharacters.Value)
                        Case "SUBSTRING"
                            _svalue = _svalue.Substring(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)
                        Case "REMOVE"
                            _svalue = _svalue.Remove(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)

                    End Select

                    _previewRecord(manipulatedBankField.BankFieldName) = _svalue

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error occurred while processing String Manipulation Settings!, Error Details : {0}", ex.Message))
        End Try
    End Sub

#End Region

#Region " Get Default Value "
    ''' <summary>
    ''' Get Default Value - Common Template Excel
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDefaultValue(ByVal bankfieldname As String, ByVal objCommonTemplate As CommonTemplateExcelDetail) As String
        Dim defaultValue As String = String.Empty
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                defaultValue = IIf(bankField.DefaultValue Is Nothing, "", bankField.DefaultValue.ToString)
                Exit For
            End If
        Next
        Return defaultValue
    End Function

    ''' <summary>
    ''' Get Default Value - Common Template Text
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDefaultValue(ByVal bankfieldname As String, ByVal objCommonTemplate As CommonTemplateTextDetail) As String
        Dim defaultValue As String = String.Empty
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                defaultValue = IIf(bankField.DefaultValue Is Nothing, "", bankField.DefaultValue.ToString)
                Exit For
            End If
        Next
        Return defaultValue
    End Function

    ''' <summary>
    ''' Get Default Value - SWIFT Template
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDefaultValue(ByVal bankfieldname As String, ByVal objCommonTemplate As SwiftTemplate) As String
        Dim defaultValue As String = String.Empty
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                defaultValue = IIf(bankField.DefaultValue Is Nothing, "", bankField.DefaultValue.ToString)
                Exit For
            End If
        Next
        Return defaultValue
    End Function

#End Region

#Region " Get Data Length "

    ''' <summary>
    ''' Get Data Length - Common Template Excel
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDataLength(ByVal bankfieldname As String, ByVal objCommonTemplate As CommonTemplateExcelDetail) As Integer
        Dim dataLength As Integer = 0
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                dataLength = bankField.DataLength.Value
                Exit For
            End If
        Next
        Return dataLength
    End Function

    ''' <summary>
    ''' Get Data Length - Common Template Text
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDataLength(ByVal bankfieldname As String, ByVal objCommonTemplate As CommonTemplateTextDetail) As Integer
        Dim dataLength As Integer = 0
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                dataLength = bankField.DataLength.Value
                Exit For
            End If
        Next
        Return dataLength
    End Function

    ''' <summary>
    ''' Get Data Length - SWIFT Template
    ''' </summary>
    ''' <param name="bankfieldname"></param>
    ''' <param name="objCommonTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBankFieldDataLength(ByVal bankfieldname As String, ByVal objCommonTemplate As SwiftTemplate) As Integer
        Dim dataLength As Integer = 0
        For Each bankField As MapBankField In objCommonTemplate.MapBankFields
            If bankField.BankField.Trim.ToUpper = bankfieldname.ToUpper Then
                dataLength = bankField.DataLength.Value
                Exit For
            End If
        Next
        Return dataLength
    End Function

    Public Function IsAmountInDollars(ByVal objCommonTemplate As CommonTemplateExcelDetail) As Boolean
        Dim retValue As Boolean = False
        If objCommonTemplate.IsAmountInDollars Then retValue = True
        Return retValue
    End Function

    Public Function IsAmountInDollars(ByVal objCommonTemplate As CommonTemplateTextDetail) As Boolean
        Dim retValue As Boolean = False
        If objCommonTemplate.IsAmountInDollars Then retValue = True
        Return retValue
    End Function

    Public Function GetDateFormat(ByVal fieldName As String, ByVal objCommonTemplate As CommonTemplateExcelDetail) As String
        Dim retvalue As String = String.Empty
        For Each bankfield As MapBankField In objCommonTemplate.MapBankFields
            If bankfield.BankField.Trim.ToUpper = fieldName.Trim.ToUpper Then
                retvalue = bankfield.DateFormat
            End If
        Next
        Return retvalue
    End Function

#End Region

End Module
