'************************************************CREATED**********************************
'SRS No                     Date Modified   Description
'--------------------------------------------------------------------------------------------------
'SRS/BTMU/2010-0018         01/Apr/2010     Add Sector Selection Logic for India
'**************************************************************************************************
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the GCMS Plus Credit Data Format
''' </summary>
''' <remarks></remarks>
Public Class GCMSPlusCreditDataFormat
    Inherits BaseFileFormat

    Private MandatoryFields() As String = {"Bank Name", "Currency", "Account No", "Account Name", "Booking Date", _
                                 "Value Date", "Debit Credit", "Amount", "Customer Reference", _
                                 "Detail Information", "Bank Reference"}

    Private Shared _amountFieldName As String = "Amount"

    Private Shared _editableFields() As String = {"Record Type", "Bank Name", "Branch Name", "Account Type", "Currency", _
                            "Account No", "Account Name", "Booking Date", "Value Date", "Transaction Type Name", _
                            "Debit Credit", "Amount", "Customer Reference", "Detail Information", "Bank Reference"}

    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Record Type", "Bank Name", "Branch Name", "Account Type", "Currency", _
                            "Account No", "Account Name", "Booking Date", "Value Date", "Transaction Type Name", _
                            "Debit Credit", "Amount", "Customer Reference", "Detail Information", "Bank Reference", "Consolidate Field"}

    Private Shared _autoTrimmableFields() As String = {"Record Type", "Bank Name", "Branch Name", "Account Type", _
                            "Account Name", "Transaction Type Name", _
                            "Debit Credit", "Customer Reference", "Detail Information", "Bank Reference"}
    Private Shared _splitFile As Boolean = False

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "GCMSPlusCreditData"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

#End Region

    Public Shared Property SplitFile() As Boolean
        Get
            Return _splitFile
        End Get
        Set(ByVal value As Boolean)
            _splitFile = value
        End Set
    End Property


    Public Shared Function OutputFormatString() As String
        Return "GCMSPlusCreditData"
    End Function

#Region "Overriden Methods"

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = -1
        Dim NameExist, AddressExist, CountryExist As Boolean
        NameExist = AddressExist = CountryExist = False
        Dim temp, expandedCountryName As String
        Dim pos As Int16
        'For Specific country validations
        Dim ListCountry() As String
        ListCountry = "".Split("|")
        expandedCountryName = ""
        If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
            If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                expandedCountryName = ListCountry(0).ToUpper()
                System.Array.Sort(ListCountry)
            End If
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Customer Reference
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Customer Reference").ToString() <> "" AndAlso _
                    (_previewRecord("Customer Reference").ToString().StartsWith(":") Or _
                    _previewRecord("Customer Reference").ToString().StartsWith("-")) Then

                    _previewRecord("Customer Reference") = " " + _previewRecord("Customer Reference").ToString().Substring(1)
                End If

                '# Format Amount 
                If _previewRecord("Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If

                    Select Case _previewRecord("Currency").ToString().ToUpper()

                        Case "TND", "KWD", "BHD", "OMR"
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0.000")

                        Case "TWD", "JPY", "KRW", "VND", "CLP"
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0.00")

                    End Select

                End If

                '#2. Replace '@' to 'AT' and '&' to 'AND' Non SWIFT Characters (except Option fields)
                '#CR-2 Replace hyphen and colon @ poisitons 1/36/71/106 (except Option fields)
                '#fixed @ 06 Sep 2011 by Kay
                'For i As Int32 = 0 To 19
                '    _previewRecord(i) = _previewRecord(i).ToString().Replace("&", "AND")
                '    _previewRecord(i) = _previewRecord(i).ToString().Replace("@", "AT")
                '    'do not remove hyphen from non-text datatype fields (2,6,10,12)
                '    Select Case i
                '        Case 1, 5, 9, 11, 18
                '            'skip
                '        Case Else
                '            _previewRecord(i) = RemoveHyphenColon(_previewRecord(i).ToString())
                '    End Select

                'Next

                ''#3. Remove Unpermitted Characters (Change Request: fixed on 05 Sepc 2011)
                ''"CUSTOMER REFERENCE"
                '_previewRecord(2) = RemoveGCMSPlusInvalidCharacters(_previewRecord(2).ToString())

                ''"FORWARD CONTRACT NUMBER", "INTERMEDIARY BANK/BRANCH/ADDRESS", "INTERMEDIARY BANK MASTER CODE", _
                ''"BENEFICIARY BANK/BRANCH/ADDRESS", "BENEFICIARY BANK MASTER CODE", "BENEFICIARY ACCOUNT NO", _
                ''"BENEFICIARY NAME/ADDRESS", "MESSAGE TO BENEFICIARY", "PURPOSE OF REMITTANCE", "INFORMATION TO REMITTING BANK"
                'For i As Int32 = 7 To 16
                '    _previewRecord(i) = RemoveGCMSPlusInvalidCharacters(_previewRecord(i).ToString())
                'Next

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1
        Dim NameExist, AddressExist, CountryExist As Boolean
        NameExist = AddressExist = CountryExist = False
        Dim expandedCountryName As String
        Dim pos As Int16
        'For Specific country validations
        Dim ListCountry() As String
        ListCountry = "".Split("|")
        expandedCountryName = ""
        If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
            If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                expandedCountryName = ListCountry(0).ToUpper()
                System.Array.Sort(ListCountry)
            End If
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Customer Reference
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Customer Reference").ToString() <> "" AndAlso _
                    (_previewRecord("Customer Reference").ToString().StartsWith(":") Or _
                    _previewRecord("Customer Reference").ToString().StartsWith("-")) Then
                    _previewRecord("Customer Reference") = " " + _previewRecord("Customer Reference").ToString().Substring(1)
                End If

                ''"Beneficiary Account No" (Change request after release)
                ''fixed @ 04th Aug 2011
                ''If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                'If _previewRecord("Beneficiary Account No").ToString() <> "" AndAlso _
                '    (_previewRecord("Beneficiary Account No").ToString().StartsWith(":") Or _
                '    _previewRecord("Beneficiary Account No").ToString().StartsWith("-")) Then

                '    _previewRecord("Beneficiary Account No") = " " + _previewRecord("Beneficiary Account No").ToString().Substring(1)
                'End If

                ''# Replace '@' to 'AT' and '&' to 'AND' Non SWIFT Characters
                'For i As Int32 = 0 To 19
                '    _previewRecord(i) = _previewRecord(i).ToString().Replace("&", "AND")
                '    _previewRecord(i) = _previewRecord(i).ToString().Replace("@", "AT")
                '    'do not remove hyphen from non-text datatype fields (2,6,10,12,19)
                '    Select Case i
                '        Case 1, 5, 9, 11, 18
                '            'skip
                '        Case Else
                '            _previewRecord(i) = RemoveHyphenColon(_previewRecord(i).ToString())
                '    End Select

                'Next


                ''# Remove Unpermitted Characters (Change Request: fixed on 05 Sepc 2011)
                ''"CUSTOMER REFERENCE"
                '_previewRecord(2) = RemoveGCMSPlusInvalidCharacters(_previewRecord(2).ToString())

                ''"FORWARD CONTRACT NUMBER", "INTERMEDIARY BANK/BRANCH/ADDRESS", "INTERMEDIARY BANK MASTER CODE", _
                ''"BENEFICIARY BANK/BRANCH/ADDRESS", "BENEFICIARY BANK MASTER CODE", "BENEFICIARY ACCOUNT NO", _
                ''"BENEFICIARY NAME/ADDRESS", "MESSAGE TO BENEFICIARY", "PURPOSE OF REMITTANCE", "INFORMATION TO REMITTING BANK"
                'For i As Int32 = 7 To 16
                '    _previewRecord(i) = RemoveGCMSPlusInvalidCharacters(_previewRecord(i).ToString())
                'Next

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Data Type and Custom Validations other than Data Length and Mandatory Field Validations
    Protected Overrides Sub Validate()

        Try
            Dim regEmail As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled)
            Dim emails() As String
            Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
            Dim RowNumber As Int32 = 0
            Dim _fieldValue, expandedCountryName As String
            'For Specific country validations
            Dim ListCountry() As String
            Dim i As Integer
            Dim isValidCurrency, isValidSettlementACNo As Boolean


            ListCountry = "".Split("|")
            expandedCountryName = ""
            If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
                If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                    ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                    expandedCountryName = ListCountry(0).ToUpper()
                    System.Array.Sort(ListCountry)
                End If
            End If


            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'reset value for 'Exchange Method' and 'Forward Contract Number' validations
                isValidCurrency = True
                isValidSettlementACNo = True


                '#2. Value Date
                'Dim _ValueDate As Date
                'If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then

                '    If _ValueDate < Date.Today Or _ValueDate >= Date.Today.AddDays(21) Then

                '        Dim validationError As New FileFormatError()
                '        validationError.ColumnName = "Value Date"
                '        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                '        validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be equal to or greater than today and less than 21 calendar days from today's date.", RowNumber + 1)
                '        validationError.RecordNo = RowNumber + 1
                '        _ValidationErrors.Add(validationError)
                '        _ErrorAtMandatoryField = True

                '    End If

                'Else

                '    Dim validationError As New FileFormatError()
                '    validationError.ColumnName = "Value Date"
                '    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                '    validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be a valid date.", RowNumber + 1)
                '    validationError.RecordNo = RowNumber + 1
                '    _ValidationErrors.Add(validationError)
                '    _ErrorAtMandatoryField = True

                'End If

                '#3. Customer Reference
                If _previewRecord("Customer Reference").ToString() <> "" Then
                    _fieldValue = _previewRecord("Customer Reference").ToString()
                    If _fieldValue(0) = "/" Or _fieldValue(_fieldValue.Length - 1) = "/" Then
                        AddValidationError("Customer Reference", _PreviewTable.Columns("Customer Reference").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not start or end with a single slash(/).", RowNumber + 1, "Customer Reference") _
                                           , RowNumber + 1, True)
                    End If

                    If _fieldValue.Contains("//") Then
                        AddValidationError("Customer Reference", _PreviewTable.Columns("Customer Reference").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Customer Reference") _
                                           , RowNumber + 1, True)
                    End If

                End If

                '#5. Currency : Must be one of the default values
                If _previewRecord("Currency").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid 'Currency'.", RowNumber + 1)
                                validationError.RecordNo = RowNumber + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                                'set value for 'Exchange Method' and 'Forward Contract No' checking
                                isValidCurrency = False
                            End If
                        End If
                    End If
                End If

                'For all fields
                'Validate unpermitted characters
                Dim regAllow As New Regex("[^0-9A-Za-z/?(),.'+:\-\s]", RegexOptions.Compiled)
                For Each col As String In TblBankFields.Keys
                    'No need to validate for these fields (TMEP FIELDS)
                    'CR #2: replace with a space for certain fields (start from customer ref)
                    Select Case col.ToUpper
                        Case "Record Type", "Bank Name", "Branch Name", _
                            "Account Name", "Transaction Type Name", _
                            "Debit Credit", "Customer Reference", "Detail Information", "Bank Reference"
                            Continue For
                    End Select

                    If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(col).ToString) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = col
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid character.", RowNumber + 1, col)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                Next

                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validation : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' #10. Fill Sector Selection 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GenerateSectorSelection()

        Dim ListTemp1() As String, ListTemp2() As String
        Dim ListCountry() As String, ListLocalCurrency() As String
        Dim matchCurrency As Boolean, matchCountry As Boolean, tem1 As Boolean, tem2 As Boolean
        Dim expandedCountryName As String = ""

        Try
            If ((TblBankFields.ContainsKey("Temp1") Or TblBankFields.ContainsKey("Temp2")) _
                    And TblBankFields.ContainsKey("Payment Type/Sector Selection")) Then

                ListTemp1 = "".Split("|")
                ListTemp2 = "".Split("|")
                ListCountry = "".Split("|")
                ListLocalCurrency = "".Split("|")

                If TblBankFields.ContainsKey("Temp1") Then 'Get "Temp1" Data Sample
                    If Not TblBankFields("Temp1").BankSampleValue Is Nothing Then

                        ListTemp1 = TblBankFields("Temp1").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp1)
                    End If
                End If

                If TblBankFields.ContainsKey("Temp2") Then 'Get "Temp2" Data Sample
                    If Not TblBankFields("Temp2").BankSampleValue Is Nothing Then
                        ListTemp2 = TblBankFields("Temp2").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp2)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
                    If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                        ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                        expandedCountryName = ListCountry(0).ToUpper()
                        System.Array.Sort(ListCountry)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Currency") Then 'Get "Currency" Data Sample
                    If Not TblBankFields("Local Currency").BankSampleValue Is Nothing Then
                        ListLocalCurrency = TblBankFields("Local Currency").BankSampleValue.Split("|")
                        System.Array.Sort(ListLocalCurrency)
                    End If
                End If

                Select Case expandedCountryName

                    Case "INDONESIA", "SINGAPORE", "MALAYSIA", "THAILAND", "AUSTRALIA", "PHILIPPINES"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            If _previewRecord("Payment Type/Sector Selection").ToString() = "" Then
                                tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                                tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                                If tem1 And tem2 Then
                                    _previewRecord("Payment Type/Sector Selection") = "Book Transfer"
                                Else
                                    matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0)
                                    matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                    _previewRecord("Payment Type/Sector Selection") = IIf(matchCurrency And matchCountry, "Domestic", "International")
                                End If
                            End If
                        Next


                    Case "VIETNAM"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            If _previewRecord("Payment Type/Sector Selection").ToString() = "" Then
                                tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                                tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                                If tem1 And tem2 Then
                                    _previewRecord("Payment Type/Sector Selection") = "Book Transfer"
                                Else
                                    matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                    _previewRecord("Payment Type/Sector Selection") = IIf(matchCountry, "Domestic", "International")
                                End If
                            End If
                        Next

                End Select

            End If ' end of Sector Selection Exists?

            'If _MasterTemplateList.IsFixed Then FormatSequenceNo()

        Catch ex As Exception
            Throw New Exception("Error on generating Sector Selection : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub FormatOutput()

        Dim RowNumber As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If TblBankFields.ContainsKey("Currency") Then
                    If _previewRecord("Amount").ToString.Trim <> String.Empty And _
                       IsNumeric(_previewRecord("Amount").ToString.Trim) Then
                        Select Case _previewRecord("Currency").ToString.ToUpper
                            Case "TND", "KWD", "BHD", "OMR"
                                _previewRecord("Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Amount").ToString), 3).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                _previewRecord("Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Amount").ToString), 0).ToString("###########0")
                            Case Else
                                _previewRecord("Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Amount").ToString), 2).ToString("###########0.00")
                        End Select
                    End If
                End If

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Conversion : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Generates File in GCMS Plus Format
    ''' </summary>
    ''' <param name="_userName">User ID that requested for File Generation</param>
    ''' <param name="_sourceFile">Name of Source File</param>
    ''' <param name="_outputFile">Name of Output File</param>
    ''' <param name="_objMaster">Master Template</param>
    ''' <returns>Boolean value determining whether the Generation of File is successful</returns>
    ''' <remarks></remarks>
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim splitter, terminator As Integer
        Dim enclosureCharacter As String
        Dim tempTable As New DataTable
        Dim i As Integer
        Try

            ' GCMS Plus format is Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New Exception("Invalid master Template")
            End If


            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                enclosureCharacter = ""
            Else
                enclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            If _splitFile And _outputFile.Contains("_") Then
                splitter = _outputFile.Substring(_outputFile.IndexOf("_") + 1, 1)
                terminator = IIf(_PreviewTable.Rows.Count < (splitter * 1000), _PreviewTable.Rows.Count, (splitter * 1000))
            Else
                terminator = _PreviewTable.Rows.Count
            End If


            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If rowNo < (splitter * 1000) - 1000 Then
                    'skip
                Else
                    line = ""
                    i = 0
                    For Each _column As String In TblBankFields.Keys

                        If TblBankFields(_column).Detail.ToUpper <> "NO" Then

                            'If Not (_column = "Temp1".ToUpper Or _
                            '        _column.ToUpper = "Temp2".ToUpper Or _
                            '        _column.ToUpper = "Temp3".ToUpper Or _
                            '        _column.ToUpper = "Local Country".ToUpper Or _
                            '        _column.ToUpper = "Local Currency".ToUpper) Then

                            'make sure to print out only the first 30 fields
                            If i < 30 Then
                                If _column.ToUpper = "Remittance Amount".ToUpper Then

                                    Dim amount As Decimal
                                    amount = Math.Abs(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString))
                                    Select Case _previewRecord("Currency").ToString.ToUpper
                                        Case "TND", "KWD", "BHD", "OMR"
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 3).ToString("###########0.000")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount, 3).ToString("###########0.000")
                                            End If
                                        Case "TWD", "JPY", "KRW", "VND", "CLP"
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount).ToString("###########0")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount).ToString("###########0")
                                            End If
                                        Case Else
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 2).ToString("###########0.00")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount, 2).ToString("###########0.00")
                                            End If
                                    End Select

                                End If

                                'added for consolidate field checking
                                If (Not _column.Equals("Consolidate Field")) Then
                                    If enclosureCharacter.Trim = String.Empty Then
                                        If line = "" Then
                                            line &= _previewRecord(_column).ToString
                                        Else
                                            line = line & "," & _previewRecord(_column).ToString
                                        End If
                                    Else
                                        If line = "" Then
                                            line = line & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                        Else
                                            line = line & "," & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                        End If
                                    End If
                                End If

                            End If

                        End If

                        'increase count
                        i += 1
                    Next
                    'End of Columns looping

                    If rowNo <> terminator - 1 Then
                        text &= line & vbNewLine
                    Else
                        text &= line
                    End If

                End If
                'End of printing out data rows checking

                rowNo += 1

                If rowNo > terminator - 1 Then Exit For
            Next

            Return SaveTextToFile(text, _outputFile)

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    ''' <summary>
    ''' Generates Consolidated Data
    ''' </summary>
    ''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    ''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    ''' <param name="AppendText">Text to append with Reference Fields</param>
    ''' <remarks></remarks>
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        If GroupByFields.Count = 0 Then Exit Sub

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If

            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''


        Dim filterString As String = String.Empty
        Dim filterList As New List(Of String)
        Dim remittanceAmt As Decimal = 0
        Dim referenceField(1) As String
        Dim rowIndex As Integer = 0

        Try

            _consolidatedDataView = _PreviewTable.DefaultView ' DataView to apply rowfilter
            _consolidatedData = New DataTable ' DataTable to hold only consolidated records
            ' Create the columns for the Consolidated DataTable
            For Each _column As DataColumn In _PreviewTable.Columns
                _consolidatedData.Columns.Add(_column.ColumnName, GetType(String))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '1. Filter out unique rows for given Filter Group
            For Each _row As DataRow In _PreviewTable.Rows

                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_row(_field).ToString()))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For

                If filterList.IndexOf(filterString) = -1 Then
                    filterList.Add(filterString)
                    filterList.Sort()
                End If

            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next


            '2. Set of fields to be aggregated/consolidated while applying the Grouping
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            For Each _filter As String In filterList

                _consolidatedDataView.RowFilter = String.Empty
                _consolidatedDataView.RowFilter = _filter

                If _consolidatedDataView.Count > 0 Then

                    ' Create the reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1) <> "" Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next
                    remittanceAmt = 0

                    For Each _viewRow As DataRowView In _consolidatedDataView
                        remittanceAmt += Convert.ToDecimal(_viewRow(_amountFieldName).ToString)
                        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                            If _referenceFields(intI, 1).Trim.Length <> 0 Then
                                referenceField(intI) = referenceField(intI) & _viewRow(_referenceFields(intI, 1).ToString) & ","
                            End If
                        Next
                    Next
                End If

                Dim consolidatedRow As DataRow
                consolidatedRow = _consolidatedData.NewRow
                _consolidatedDataView.Item(0)(_amountFieldName) = remittanceAmt.ToString

                For Each _column As String In TblBankFields.Keys
                    consolidatedRow(_column) = _consolidatedDataView(0)(_column)
                    consolidatedRow(_amountFieldName) = remittanceAmt.ToString
                    '#2. Format Remittance Amount 
                    If consolidatedRow(_amountFieldName).ToString <> String.Empty Then
                        Select Case consolidatedRow("Currency").ToString().ToUpper()
                            Case "TND", "KWD", "BHD", "OMR"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0")
                            Case Else
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.00")
                        End Select

                    End If
                Next

                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
                        consolidatedRow(_referenceFields(intI, 1)) = referenceField(intI).Substring(0, referenceField(intI).Length - 1)
                    End If
                Next

                _consolidatedData.Rows.Add(consolidatedRow)

            Next

            _totalRecordCount = _PreviewTable.Rows.Count
            If _consolidatedData.Rows.Count > 0 Then
                _consolidatedRecordCount = _consolidatedData.Rows.Count
                _PreviewTable.Rows.Clear()
                _PreviewTable = _consolidatedData.Copy
            Else
                _consolidatedRecordCount = 0
            End If
            ValidationErrors.Clear()
            GenerateHeader()
            GenerateTrailer()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()

        Catch ex As Exception
            Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
        End Try

    End Sub


    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If
                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If

            End If

            RowNumber += 1

        Next

    End Sub


#End Region

    'Added for Settlement A/C enhancement for OVS
    Private Function IsValidSettlementAC(ByVal cur As String) As Boolean
        Dim isFound As Boolean = False
        If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
            If TblBankFields("Currency").DefaultValue.Length > 0 Then
                Dim defaultCurrency() As String
                defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                For Each val As String In defaultCurrency
                    If cur.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                        isFound = True
                        Exit For
                    End If
                Next
            End If
        End If
        Return isFound

    End Function

    Private Function IsValidIBANAccount(ByVal AcNo As String) As Boolean
        Dim isValid As Boolean = False
        Dim copyAC As String
        Dim strAcNo As System.Text.StringBuilder
        Dim i As Integer = 0
        Dim initLetter As Char = Chr(65)


        '1. Check that the total IBAN length is correct as per the country. If not, the IBAN is invalid. 
        '2. Move the four initial characters to the end of the string. 
        '3. Replace each letter in the string with two digits, thereby expanding the string, where A=10, B=11, ..., Z=35. 
        '4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97.  

        '1. Ignore

        '2. move first 4 chars to the end of string
        copyAC = AcNo.Substring(4) + AcNo.Substring(0, 4)

        '3.Replace
        strAcNo = New System.Text.StringBuilder(copyAC)
        For i = 10 To 35
            strAcNo.Replace(initLetter.ToString(), i.ToString())
            initLetter = Chr(Asc(initLetter) + 1)
        Next

        '4. Check modulus
        Dim tempinteger As Long
        Dim tempstr As String
        Dim divstr As String
        tempstr = strAcNo.ToString() 'Convert_to_iban_numbers(myaccount)
        Do Until Len(tempstr) <= 5
            divstr = Left(tempstr, 5)
            If Len(tempstr) > 5 Then
                tempstr = Right(tempstr, Len(tempstr) - 5)
            End If
            tempinteger = Format(divstr)
            tempstr = Str(tempinteger Mod 97) + tempstr
        Loop
        tempinteger = Format(tempstr)
        If (tempinteger Mod 97) = 1 Then
            isValid = True
        Else
            isValid = False
        End If

        Return isValid

    End Function

    'Only for Malaysia template
    'Remittance Amount limit should be setup in Master template's Data Sample field
    Private Function isPurposeRequired(ByVal cur As String, ByVal Amt As Double, ByVal sampleAmt As Double) As Boolean

        If cur.Equals("MYR") AndAlso Convert.ToDouble(Amt) > sampleAmt Then
            isPurposeRequired = True
        End If

        isPurposeRequired = False
    End Function

    Private Sub PadTrailingSpaces(ByRef src As String, ByVal max_len As Integer)
        Dim i As Integer = 0
        Dim diffLen As Integer

        diffLen = max_len - src.Length
        For i = 1 To diffLen
            src = src + " "
        Next

    End Sub

    Function validate_iban(ByVal myaccount As String) As Boolean
        Dim tempinteger As Long
        Dim tempstr As String
        Dim divstr As String
        tempstr = myaccount 'Convert_to_iban_numbers(myaccount)
        Do Until Len(tempstr) <= 5
            divstr = Left(tempstr, 5)
            If Len(tempstr) > 5 Then
                tempstr = Right(tempstr, Len(tempstr) - 5)
            End If
            tempinteger = Format(divstr)
            tempstr = Str(tempinteger Mod 97) + tempstr
        Loop
        tempinteger = Format(tempstr)
        If (tempinteger Mod 97) = 1 Then
            validate_iban = True
        Else
            validate_iban = False
        End If
    End Function

End Class

 

 