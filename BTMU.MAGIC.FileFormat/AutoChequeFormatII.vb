'Dummy container to hold the output string of AutoCheque ACMS which is a variant of AutoCheque with no header generated
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the RTMS-AutoCheque Format
''' </summary>
''' <remarks></remarks>
Public Class AutoChequeFormatII
    Inherits HKRTMSFormat
    'Change request during Development time
    'to take out the field: Cheque Number from the output/spec

    Private Shared _editableFields() As String = {"Group ID", "Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Security Group", "Instruction Remarks", "Identifier", "Counter Party Name", "Bank Number" _
                    , "Branch Number", "Account Number", "Transaction Amount", "Our Reference", "Advice Template" _
                    , "Counter Party Address", "Transaction Remarks", "AutoCheque Advice Information", "Delivery Option"}

    Private Shared _autoTrimmableFields() As String = {"Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Security Group", "Instruction Remarks", "Identifier", "Bank Number" _
                    , "Branch Number", "Account Number", "Our Reference", "Advice Template", "Counter Party Address", "Transaction Remarks" _
                    , "AutoCheque Advice Information", "Delivery Option"}


    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Group ID", "Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Security Group", "Instruction Remarks", "Identifier", "Counter Party Name", "Bank Number" _
                    , "Branch Number", "Account Number", "Transaction Amount", "Our Reference", "Advice Template" _
                    , "Counter Party Address", "Transaction Remarks", "AutoCheque Advice Information", "Delivery Option"}


    Public Shared Function OutputFormatString() As String
        Return "AutoCheque ACMS"
    End Function

#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "AutoCheque ACMS"
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As System.Collections.Generic.List(Of String), ByVal _referenceFields As System.Collections.Generic.List(Of String), ByVal _appendText As System.Collections.Generic.List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?        
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row(_amountFieldName) Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, _amountFieldName))
            If Not Decimal.TryParse(_row(_amountFieldName).ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, _amountFieldName))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, _amountFieldName)

            _consolidatedRecordCount = _consolidatedData.Rows.Count

            For Each row As DataRow In _consolidatedData.Rows
                Dim sentences As String() = row.Item("AutoCheque Advice Information").ToString.Split(New Char() {","c})
                Dim wordList As New List(Of String)

                For Each sentence As String In sentences
                    Dim sentenceTemp As String = sentence.Replace(vbCrLf, " ").Replace(vbLf, " ") & ","
                    Dim words As String() = sentenceTemp.Split(" ")
                    For Each word As String In words
                        wordList.Add(word)
                    Next
                Next

                Dim lines As New List(Of String)
                Dim line As String = String.Empty
                Dim isFirst As Boolean = True
                For Each word As String In wordList
                    If ((line.Length + word.Length) <= 80 - 1) Then
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            line = line & " " & word
                        End If
                    Else
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            lines.Add(line)
                            line = word
                        End If
                    End If
                Next
                lines.Add(line)

                If lines.Count > 25 Then
                    For i As Integer = lines.Count - 1 To 25 Step -1
                        lines.RemoveAt(i)
                    Next
                End If
                Dim output As String = String.Join(vbLf, lines.ToArray())
                If output.Length > 2 Then
                    output = output.Substring(0, output.Length - 1)
                End If
                row.Item("AutoCheque Advice Information") = output
            Next

            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub GenerateOutputFormat()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Convert to all Capital letters
                For Each col As String In TblBankFields.Keys
                    If _previewRecord(col).ToString.Trim().Length > 0 Then
                        _previewRecord(col) = _previewRecord(col).ToString().ToUpper

                    End If
                Next

                '#8. Identifier (set running number.)
                If _previewRecord("Identifier").ToString().Trim = String.Empty Then
                    seqNo += 1
                    _previewRecord("Identifier") = "A" + seqNo.ToString("0000000")
                End If

                '#10. Bank Number (always set blank)
                If _previewRecord("Bank Number").ToString.Trim <> String.Empty Then
                    _previewRecord("Bank Number") = String.Empty
                End If

                    '#11. Branch Number (always set blank)
                    If _previewRecord("Branch Number").ToString.Trim <> String.Empty Then
                        _previewRecord("Branch Number") = String.Empty
                    End If

                '#12. Account Number (always set blank)
                If _previewRecord("Account Number").ToString.Trim <> String.Empty Then
                    _previewRecord("Account Number") = String.Empty
                End If


                '''''''''''''''''''''Modified by Jacky on 2019-04-03''''''''''''''''''''
                'Purpose : Transaction Amount should support cent function
                '#13. Transaction Amount formatting
                If _previewRecord("Transaction Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()) / 100
                    End If

                    _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()).ToString("########0.00")

                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                '#13. Transaction Amount formatting
                'If _previewRecord("Transaction Amount").ToString <> String.Empty Then

                '    If Not _DataModel.IsAmountInDollars Then
                '        _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()) / 100
                '    End If

                '    _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()).ToString("########0.00")

                'End If

                '#16. Counter Party Address (Auto Shifting) - check position 60/120/180
                ' 2014-4-24, commented by FHK: disable auto-shifting for this column
                '' 2014-6-11, break the address into lines by LF first, and then apply auto-shifting for each line, and finally combine the lines again
                ' 2014-6-17, if there are LF, leave it as-is, otherwise apply apply auto-shifting
                Dim tempstr As String = String.Empty
                Dim pos As Int16 = 0
                Dim strbld As New System.Text.StringBuilder

                ' 2014-6-11 added by FHK chun (modified on 2014-6-17):

                '''''''''''''''''''''''''''''''''''modified by Jacky on 2018-11-01''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'Dim strAddrLine() As String = _previewRecord("Counter Party Address").ToString().Split(vbLf)
                'If strAddrLine.Length = 1 Then
                'Dim strLine As String = _previewRecord("Counter Party Address").ToString()
                ''For Each strLine As String In strAddrLine
                'Dim n As Int16 = 1
                'While strLine.Length > 60 * n
                ''check for 60n-th position whether it's space or not
                'tempstr = strLine.Substring(0, 60 * n)
                'If Not tempstr(60 * n - 1).Equals(" ") Then
                'pos = tempstr.LastIndexOf(" ")
                'If pos > 0 Then
                ''get first half
                'Dim strbld2 As New System.Text.StringBuilder(strLine.Substring(0, pos))
                ''second half
                'tempstr = strLine.Substring(pos + 1)

                ''fill trailing spaces
                'For i As Integer = 1 To 60 * n - pos
                'strbld2.Append(" ")
                'Next

                '   strbld2.Append(tempstr)
                '  strLine = strbld2.ToString()
                ' End If
                'End If
                '
                '               n += 1
                '              End While

                '             If strbld.Length = 0 Then
                'strbld.Append(strLine)
                'Else
                'strbld.Append(vbLf).Append(strLine)
                'End If
                ''Next
                '_previewRecord("Counter Party Address") = strbld.ToString()
                'End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'If _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 180 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 120th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 120)
                '    If Not tempstr(119).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 120 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 180th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 180)
                '    If Not tempstr(179).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 180 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'ElseIf _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 120 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 120th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 120)
                '    If Not tempstr(119).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 120 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'ElseIf _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 60 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'End If

                '#17. Transaction Remarks (Auto Shifting) - check position 70th
                If _previewRecord("Transaction Remarks").ToString <> String.Empty And _previewRecord("Transaction Remarks").ToString().Length > 70 Then
                    tempstr = _previewRecord("Transaction Remarks").ToString.Substring(0, 70)
                    If Not tempstr(69).Equals(" ") Then
                        pos = tempstr.LastIndexOf(" ")
                        If pos > 0 Then
                            'get first half
                            strbld = New System.Text.StringBuilder(_previewRecord("Transaction Remarks").ToString().Substring(0, pos))
                            'second half
                            tempstr = _previewRecord("Transaction Remarks").ToString().Substring(pos + 1)

                            'fill trailing spaces
                            For i As Integer = 1 To 70 - pos
                                strbld.Append(" ")
                            Next

                            strbld.Append(tempstr)
                            _previewRecord("Transaction Remarks") = strbld.ToString()
                        End If

                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Transaction Amount"))
            'Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Transaction"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Convert to all Capital letters
                For Each col As String In TblBankFields.Keys
                    If _previewRecord(col).ToString.Trim().Length > 0 Then
                        _previewRecord(col) = _previewRecord(col).ToString().ToUpper

                    End If
                Next

                ''#8. Identifier (set running number.)
                'If _previewRecord("Identifier").ToString().Trim = String.Empty Then
                '    seqNo += 1
                '    _previewRecord("Identifier") = "A" + seqNo.ToString("000000")
                'End If

                '#10. Bank Number (always set blank)
                If _previewRecord("Bank Number").ToString.Trim <> String.Empty Then
                    _previewRecord("Bank Number") = String.Empty
                End If

                '#11. Branch Number (always set blank)
                If _previewRecord("Branch Number").ToString.Trim <> String.Empty Then
                    _previewRecord("Branch Number") = String.Empty
                End If

                '#12. Account Number (always set blank)
                If _previewRecord("Account Number").ToString.Trim <> String.Empty Then
                    _previewRecord("Account Number") = String.Empty
                End If

                '#13. Transaction Amount
                'If _previewRecord("Transaction Amount").ToString <> String.Empty Then
                '    _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()).ToString("########0.00")
                'End If

                '#16. Counter Party Address (Auto Shifting) - check position 60/120/180 
                ' 2014-4-24, commented by FHK: disable auto-shifting for this column
                '' 2014-6-11, break the address into lines by LF first, and then apply auto-shifting for each line, and finally combine the lines again
                ' 2014-6-17, if there are LF, leave it as-is, otherwise apply apply auto-shifting
                Dim tempstr As String = String.Empty
                Dim pos As Int16 = 0
                Dim strbld As New System.Text.StringBuilder

                ' 2014-6-11 added by FHK chun (modified on 2014-6-17):
                ''''''''''''''''''''''''modified by jacky''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Dim Counter_Party_Address = _previewRecord("Counter Party Address").ToString
                'If (Counter_Party_Address.ToString.Contains(vbCrLf) Or Counter_Party_Address.ToString.Contains(vbCr) Or Counter_Party_Address.ToString.Contains(vbLf)) Then
                'Counter_Party_Address = Counter_Party_Address.ToString.Replace(vbCrLf, " ").Replace(vbCr, " ").Replace(vbLf, " ")
                'End If
                'If _previewRecord("Counter Party Address").ToString <> String.Empty Then
                'Dim strAddrLine() As String = Counter_Party_Address.ToString().Split(vbLf)
                'If strAddrLine.Length = 1 Then
                'Dim strLine As String = Counter_Party_Address.ToString()
                If _previewRecord("Counter Party Address").ToString <> String.Empty Then
                    Dim strAddrLine() As String = _previewRecord("Counter Party Address").ToString().Split(vbLf)
                    If strAddrLine.Length = 1 Then
                        Dim strLine As String = _previewRecord("Counter Party Address").ToString()
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'For Each strLine As String In strAddrLine
                        Dim n As Int16 = 1
                        While strLine.Length > 60 * n
                            'check for 60n-th position whether it's space or not
                            tempstr = strLine.Substring(0, 60 * n)
                            If Not tempstr(60 * n - 1).Equals(" ") Then
                                pos = tempstr.LastIndexOf(" ")
                                If pos > 0 Then
                                    'get first half
                                    Dim strbld2 As New System.Text.StringBuilder(strLine.Substring(0, pos))
                                    'second half
                                    tempstr = strLine.Substring(pos + 1)

                                    'fill trailing spaces
                                    For i As Integer = 1 To 60 * n - pos
                                        strbld2.Append(" ")
                                    Next

                                    strbld2.Append(tempstr)
                                    strLine = strbld2.ToString()
                                End If
                            End If

                            n += 1
                        End While

                        If strbld.Length = 0 Then
                            strbld.Append(strLine)
                        Else
                            strbld.Append(vbLf).Append(strLine)
                        End If
                        'Next
                        _previewRecord("Counter Party Address") = strbld.ToString()
                    End If
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'If _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 180 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 120th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 120)
                '    If Not tempstr(119).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 120 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 180th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 180)
                '    If Not tempstr(179).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 180 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'ElseIf _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 120 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                '    'check for 120th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 120)
                '    If Not tempstr(119).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 120 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'ElseIf _previewRecord("Counter Party Address").ToString <> String.Empty And _previewRecord("Counter Party Address").ToString().Length > 60 Then
                '    'check for 60th position whether it's space or not
                '    tempstr = _previewRecord("Counter Party Address").ToString.Substring(0, 60)
                '    If Not tempstr(59).Equals(" ") Then
                '        pos = tempstr.LastIndexOf(" ")
                '        If pos > 0 Then
                '            'get first half
                '            strbld = New System.Text.StringBuilder(_previewRecord("Counter Party Address").ToString().Substring(0, pos))
                '            'second half
                '            tempstr = _previewRecord("Counter Party Address").ToString().Substring(pos + 1)

                '            'fill trailing spaces
                '            For i As Integer = 1 To 60 - pos
                '                strbld.Append(" ")
                '            Next

                '            strbld.Append(tempstr)
                '            _previewRecord("Counter Party Address") = strbld.ToString()
                '        End If
                '    End If

                'End If

                '#17. Transaction Remarks (Auto Shifting) - check position 70th
                If _previewRecord("Transaction Remarks").ToString <> String.Empty And _previewRecord("Transaction Remarks").ToString().Length > 70 Then
                    tempstr = _previewRecord("Transaction Remarks").ToString.Substring(0, 70)
                    If Not tempstr(69).Equals(" ") Then
                        pos = tempstr.LastIndexOf(" ")
                        If pos > 0 Then
                            'get first half
                            strbld = New System.Text.StringBuilder(_previewRecord("Transaction Remarks").ToString().Substring(0, pos))
                            'second half
                            tempstr = _previewRecord("Transaction Remarks").ToString().Substring(pos + 1)

                            'fill trailing spaces
                            For i As Integer = 1 To 70 - pos
                                strbld.Append(" ")
                            Next
                            strbld.Append(tempstr)
                            _previewRecord("Transaction Remarks") = strbld.ToString()
                        End If

                    End If
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'added by fujitsu on 2013/03/21
    Protected Overrides Sub Validate()
        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim _previewTableCopy As New DataTable
        Dim prevID As String = String.Empty
        Dim tempId As String = String.Empty
        Dim tempDate As Date
        Dim tempCur As String = String.Empty
        Dim tempPAN As String = String.Empty
        Dim tempPRef As String = String.Empty
        Dim tempInstID As String = String.Empty
        Dim tempInstrRemark As String = String.Empty


        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim regNonAlphanum As New Regex("[^0-9a-zA-Z]", RegexOptions.Compiled)
        Dim regUnpermittedChar As New Regex("[""]", RegexOptions.Compiled)  ' disallow double quote only
        rowNo = 0
        '''''''''''Modified by Jacky on 2019-04-08'''''''''''''''''''''''''''''
        'Purpose : Security group accept underline and space
        Dim regNonAlphanumAndUnderLineAndSpace As New Regex("[^0-9a-zA-Z_ ]", RegexOptions.Compiled)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Try
            ' Validate the fields of each record and add file format errors to _validationerrors object
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'Field #1 - Group ID 
                If _previewRecord("Group ID").ToString.Trim <> String.Empty Then
                    'For uniqueness checking
                    If rowNo = 0 Then tempId = _previewRecord("Group ID").ToString.Trim

                    'must be D### format.
                    If _previewRecord("Group ID").ToString.Length < 4 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                    'must start with 'D'
                    If _previewRecord("Group ID").ToString.Length = 4 And _
                        Not _previewRecord("Group ID").ToString.StartsWith("D", StringComparison.InvariantCultureIgnoreCase) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                        'End If

                        'must followed by 3 digits
                    ElseIf _previewRecord("Group ID").ToString.Length = 4 And _
                            regNonNumeric.IsMatch(_previewRecord("Group ID").ToString.Substring(1)) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Check for uniqueness in the whole file
                    If Not tempId.Equals(_previewRecord("Group ID").ToString.Trim) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                Else
                    If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                'Field #2 - Payment Date
                Dim _ValueDate As Date
                If _previewRecord("Payment Date").ToString().Trim <> String.Empty Then
                    ''''''''''''''''''''Modified by Jacky on 2019-01-09''''''''''''''''''''
                    'Purpose : Avoid control panel setting problem
                    If Not InStr(_previewRecord("Payment Date").ToString(), "/") Then
                        _previewRecord("Payment Date") = _previewRecord("Payment Date").ToString().Replace("-", "/").Replace(".", "/")
                    End If
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If HelperModule.IsDateDataType(_previewRecord("Payment Date").ToString(), "", TblBankFields("Payment Date").DateFormat, _ValueDate, True) Then

                        'For uniqueness checking
                        If rowNo = 0 Or tempDate.ToString().Equals("01/01/0001 12:00:00 AM") Then tempDate = _ValueDate

                        'Cannot be a past date
                        '''''Modified by Jacky on 2019-01-16'''''''''
                        'Purpose : AutoCheque ACMS now accept today value
                        'If _ValueDate <= Date.Today Then
                        If _ValueDate < Date.Today Then
                            '''''''''''''''''''''''''''''''''''''''''''
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Payment Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                            '''''''''''''''''Modified by Jacky on 2019-01-16'''''''''''''''''
                            'Purpose : AutoCheque ACMS now accept today value
                            'validationError.Description = String.Format("Record: {0} Field: 'Payment Date' cannot be a past date or today's date.", rowNo + 1)
                            validationError.Description = String.Format("Record: {0} Field: 'Payment Date' cannot be a past date.", rowNo + 1)
                            '''''''''''''''''''''''''''''''''''''''''''''
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        'Cannot fall on Saturday or Sunday. 
                        If Weekday(_ValueDate) = 1 Or Weekday(_ValueDate) = 7 Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Payment Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                            validationError.Description = String.Format(MsgReader.GetString("E09020030"), "Record", rowNo + 1, "Payment Date")
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        '20180906 Samuel Comment out
                        'Check for uniqueness in the whole file
                        'If Not tempDate.ToString().Equals("01/01/0001 12:00:00 AM") And tempDate <> _ValueDate Then
                        '    formatError = New FileFormatError
                        '    With formatError
                        '        .ColumnName = "Payment Date"
                        '        .ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                        '        .Description = String.Format("Record: {0} Field: 'Payment Date' must be same for all records", rowNo + 1)
                        '        .RecordNo = rowNo + 1
                        '    End With
                        '    _ValidationErrors.Add(formatError)
                        '    _ErrorAtMandatoryField = True
                        'End If

                    Else
                        'Must be a valid Date value
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Payment Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Payment Date' should be a valid date.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #3 - Currency : Must be one of default values in master template
                If _previewRecord("Currency").ToString <> String.Empty Then
                    If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Currency").ToString

                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid Currency.", rowNo + 1)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If

                    '20180906 Samuel Comment out
                    'Check for uniqueness in the whole file
                    'If _previewRecord("Currency").ToString.Trim <> String.Empty And _
                    '    Not tempCur.Equals(_previewRecord("Currency").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Currency"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                    '        .Description = String.Format("Record: {0} Field: 'Currency' must be same for all records", rowNo + 1)
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If

                Else
                    If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Currency"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Currency' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #4 - Principal Account Number (must be numeric)
                If _previewRecord("Principal Account Number").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempPAN = String.Empty Then tempPAN = _previewRecord("Principal Account Number").ToString

                    If regNonNumeric.IsMatch(_previewRecord("Principal Account Number").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Account Number").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Principal Account Number")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    '20180906 Samuel Comment out
                    'Check for uniqueness in the whole file
                    'If _previewRecord("Principal Account Number").ToString.Trim <> String.Empty And _
                    '        Not tempPAN.Equals(_previewRecord("Principal Account Number").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Principal Account Number"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Principal Account Number").Ordinal + 1
                    '        .Description = String.Format("Record: {0} Field: 'Principal Account Number' must be same for all records", rowNo + 1)
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If

                Else
                    If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Account Number").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Principal Account Number' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #5 - Principal Reference
                If _previewRecord("Principal Reference").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempPRef = String.Empty Then tempPRef = _previewRecord("Principal Reference").ToString

                    If ContainsISOButDoubleQuote(_previewRecord("Principal Reference").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Reference"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Reference").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Principal Reference")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    '20180906 Samuel Comment out
                    'Check for uniqueness in the whole file
                    'If _previewRecord("Principal Reference").ToString.Trim <> String.Empty And _
                    '        Not tempPRef.Equals(_previewRecord("Principal Reference").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Principal Reference"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Principal Reference").Ordinal + 1
                    '        .Description = String.Format("Record: {0} Field: 'Principal Reference' must be same for all records", rowNo + 1)
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                End If

                ' Field #6 - Security Group
                ' 20180906 samuel modified
                If _previewRecord("Security Group").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempInstID = String.Empty Then tempInstID = _previewRecord("Security Group").ToString
                    'check length (CR during Dev time)
                    'If _previewRecord("Security Group").ToString.Length <> 15 Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Security Group"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Security Group").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09050040"), "Record", rowNo + 1, "Security Group", "15")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'check alphanumeric
                    '20180906 samuel modified
                    If regNonAlphanumAndUnderLineAndSpace.IsMatch(_previewRecord("Security Group").ToString) Then
                        If _previewRecord("Security Group").ToString <> "N/A" And _previewRecord("Security Group").ToString <> "n/a" Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Security Group"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Security Group").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Security Group")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                    'end remarked by fhk on 12-aug-2015

                    '20180906 Samuel Comment out
                    'Check for uniqueness in the whole file
                    'If _previewRecord("Security Group").ToString.Trim <> String.Empty And _
                    '        Not tempInstID.Equals(_previewRecord("Security Group").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Security Group"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Security Group").Ordinal + 1
                    '        .Description = String.Format("Record: {0} Field: 'Security Group' must be same for all records", rowNo + 1)
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                End If

                ' Field #7 - Instruction Remarks
                If _previewRecord("Instruction Remarks").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempInstrRemark = String.Empty Then tempInstrRemark = _previewRecord("Instruction Remarks").ToString()

                    '20180906 Samuel change to ISO
                    If ContainsInvlidISO(_previewRecord("Instruction Remarks").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Instruction Remarks"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Instruction Remarks").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Instruction Remarks")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    '20180906 Samuel comment out
                    'Check for uniqueness in the whole file
                    'If _previewRecord("Instruction Remarks").ToString.Trim <> String.Empty And _
                    '        Not tempInstrRemark.Equals(_previewRecord("Instruction Remarks").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Instruction Remarks"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Instruction Remarks").Ordinal + 1
                    '        .Description = String.Format("Record: {0} Field: 'Instruction Remarks' must be same for all records", rowNo + 1)
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                End If

                'Field #8 - Identifier
                If _previewRecord("Identifier").ToString.Trim <> String.Empty Then

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'check alphanumeric
                    'If regNonAlphanum.IsMatch(_previewRecord("Identifier").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Identifier"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Identifier")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                    'end remarked by fhk on 12-aug-2015

                    '20180906 samuel added
                    'check ISO
                    If ContainsInvlidISO(_previewRecord("Identifier").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Identifier"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Identifier")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    Dim isFound As Boolean = False
                    Dim sub_rno As Integer = -1
                    'check uniqueness in each record
                    For Each _subRec As DataRow In _PreviewTable.Rows
                        sub_rno += 1
                        'skip for the same record
                        If sub_rno = rowNo Then Continue For

                        If _previewRecord("Identifier").ToString.Equals(_subRec("Identifier").ToString, StringComparison.InvariantCultureIgnoreCase) Then
                            isFound = True
                            Exit For
                        End If

                    Next

                    If isFound Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Identifier"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Identifier' must be unique.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                Else
                    ''''''''''''''''''modified by Jacky on 2018-11-02'''''''''''''''''''''''''''''''
                    'If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                    'formatError = New FileFormatError
                    'With formatError
                    '.ColumnName = "Identifier"
                    '.ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                    '.Description = String.Format("Record: {0} Field: 'Identifier' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                    '.RecordNo = rowNo + 1
                    'End With
                    '_ValidationErrors.Add(formatError)
                    '_ErrorAtMandatoryField = True
                    'End If
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If

                ' Field #9 - Counter Party Name
                'CR @ 07/10/2011 > from alphanum checking to unpermitted char checking.

                If _previewRecord("Counter Party Name").ToString.Trim <> String.Empty Then
                    If ContainsISOButDoubleQuoteAndMore(_previewRecord("Counter Party Name").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Counter Party Name"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Counter Party Name").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Counter Party Name")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                Else
                    ''''''''''''''''''modified by Jacky on 2018-11-02'''''''''''''''''''''''''''''''
                    'If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                    'formatError = New FileFormatError
                    'With formatError
                    '.ColumnName = "Counter Party Name"
                    '.ColumnOrdinalNo = _PreviewTable.Columns("Counter Party Name").Ordinal + 1
                    '.Description = String.Format("Record: {0} Field: 'Counter Party Name' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                    '.RecordNo = rowNo + 1
                    'End With
                    '_ValidationErrors.Add(formatError)
                    '_ErrorAtMandatoryField = True
                    'End If
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If

                '' Field #10 - Bank Number
                'If _previewRecord("Bank Number").ToString.Trim <> String.Empty Then
                '    'must be numeric
                '    If regNonNumeric.IsMatch(_previewRecord("Bank Number").ToString) Then
                '        formatError = New FileFormatError
                '        With formatError
                '            .ColumnName = "Bank Number"
                '            .ColumnOrdinalNo = _PreviewTable.Columns("Bank Number").Ordinal + 1
                '            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Bank Number")
                '            .RecordNo = rowNo + 1
                '        End With
                '        _ValidationErrors.Add(formatError)
                '        _ErrorAtMandatoryField = True
                '    End If

                'End If

                '' Field #11 - Branch Number
                'If _previewRecord("Branch Number").ToString.Trim <> String.Empty Then
                '    'must be numeric
                '    If regNonNumeric.IsMatch(_previewRecord("Branch Number").ToString) Then
                '        formatError = New FileFormatError
                '        With formatError
                '            .ColumnName = "Branch Number"
                '            .ColumnOrdinalNo = _PreviewTable.Columns("Branch Number").Ordinal + 1
                '            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Branch Number")
                '            .RecordNo = rowNo + 1
                '        End With
                '        _ValidationErrors.Add(formatError)
                '        _ErrorAtMandatoryField = True
                '    End If

                'End If

                ' Field #12 - Account Number
                If _previewRecord("Account Number").ToString.Trim <> String.Empty Then
                    'must be numeric
                    If regNonNumeric.IsMatch(_previewRecord("Account Number").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Account Number").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Account Number")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #13 - Transaction Amount
                Dim amount As Decimal
                'Amount must be positive
                If _previewRecord("Transaction Amount").ToString().Trim <> String.Empty Then
                    If Decimal.TryParse(_previewRecord("Transaction Amount").ToString, amount) Then
                        If amount <= 0 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Transaction Amount"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09050080"), "Record", rowNo + 1, "Transaction Amount")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If

                        Select Case _previewRecord("Currency").ToString.ToUpper
                            Case "KWD"
                                'If no. of decimal places <= 3, then format to 3 decimal, else prompt error message
                                If NumberOfDecimalPlaces(_previewRecord("Transaction Amount").ToString) <= 3 Then
                                    If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                                        _previewRecord("Transaction Amount") = "-" & amount.ToString("###########0.000")
                                    Else
                                        _previewRecord("Transaction Amount") = amount.ToString("###########0.000")
                                    End If
                                Else
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, three decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            Case "JPY", "KRW", "IDR", "ARS"
                                'If no. of decimal places is not equal to 0, prompt error message
                                If regNonNumeric.IsMatch(_previewRecord("Transaction Amount").ToString) Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, no decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            Case Else
                                'If no. of decimal places <= 2, then format to 2 decimal, else prompt error message
                                If NumberOfDecimalPlaces(_previewRecord("Transaction Amount").ToString) <= 2 Then
                                    If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                                        _previewRecord("Transaction Amount") = "-" & amount.ToString("###########0.00")
                                    Else
                                        _previewRecord("Transaction Amount") = amount.ToString("###########0.00")
                                    End If
                                Else
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, two decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If

                        End Select

                    End If
                Else
                    If _previewRecord("Template Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Transaction Amount"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Transaction Amount' must be mandatory as 'Template Name' is not provided", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #14 - Our Reference
                If _previewRecord("Our Reference").ToString.Trim <> String.Empty And _
                    ContainsISOButDoubleQuoteAndMore(_previewRecord("Our Reference").ToString) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Our Reference"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Our Reference").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Our Reference")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Field #15 - Advice Template
                'remarked by fhk on 12-aug-2015, allow special characters
                'If _previewRecord("Advice Template").ToString.Trim <> String.Empty And _
                '    regNonAlphanum.IsMatch(_previewRecord("Advice Template").ToString) Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Advice Template"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Advice Template").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Advice Template")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'end remarked by fhk on 12-aug-2015

                ' Field #15 - Template Name
                'remarked by fhk on 20180906, samuel
                If _previewRecord("Template Name").ToString.Trim <> String.Empty And _
                    ContainsInvlidISO(_previewRecord("Template Name").ToString) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Template Name"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Template Name").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Template Name")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' added by fujitsu chun 2014-6-17: add checking
                ' Field #16 - Counter Party Address - length error if has more than 1 line (by line-feeds) and anyline with length > 60
                If _previewRecord("Counter Party Address").ToString.Trim <> String.Empty Then
                    Dim bError As Boolean = False
                    Dim Counter_Party_Address_length = 240

                    If (_previewRecord("Counter Party Address").ToString.Contains(vbCrLf) Or _previewRecord("Counter Party Address").ToString.Contains(vbCr) Or _previewRecord("Counter Party Address").ToString.Contains(vbLf)) Then
                        Counter_Party_Address_length = 243
                    End If

                    If (_previewRecord("Counter Party Address").ToString.Length <= Counter_Party_Address_length) Then
                        If (_previewRecord("Counter Party Address").ToString.Contains(vbCrLf) Or _previewRecord("Counter Party Address").ToString.Contains(vbCr) Or _previewRecord("Counter Party Address").ToString.Contains(vbLf)) Then
                            Dim strAddrLine() As String = _previewRecord("Counter Party Address").ToString().Split(vbLf)

                            If strAddrLine.Length > 4 Then
                                bError = True

                            ElseIf strAddrLine.Length > 1 Then
                                Dim i As Int16 = 0
                                Dim CountMaxiumLine As Int16 = 0
                                Dim totalCount = 0

                                While (Not bError) AndAlso i < strAddrLine.Length
                                    If strAddrLine(i).Length > 60 Then
                                        Dim CountUntilLower As Int16 = strAddrLine(i).Length
                                        'bError = True
                                        'Else
                                        While 60 <= CountUntilLower
                                            CountUntilLower = CountUntilLower - 60
                                            CountMaxiumLine += 1
                                        End While
                                        i += 1
                                    Else
                                        i += 1
                                    End If

                                    'If CountMaxiumLine <> 0 Then
                                    '    totalCount = CountMaxiumLine + strAddrLine.Length
                                    'End If

                                    'If totalCount > 4 Then
                                    '  bError = True
                                    'End If

                                End While
                                If CountMaxiumLine <> 0 Then
                                    totalCount = CountMaxiumLine + strAddrLine.Length
                                End If

                                'The total line must not exceed 4
                                If totalCount > 4 Then
                                    bError = True
                                End If
                            End If
                        End If
                    Else
                        bError = True
                    End If

                    If bError Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Counter Party Address"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Counter Party Address").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "Counter Party Address", "4 lines of 60 characters")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #17 - Transaction Remarks
                '''''modified by Jacky'''''''''''''''''
                ''''''''''add check length of Transaction Remarks and autocheque advice info''''''''''''''''''
                '''''''''''''''''''''''''''Transaction Remarks'''''''''''''''''''''''''''''''
                If _previewRecord("Transaction Remarks").ToString.Trim <> String.Empty Then
                    Dim bError As Boolean = False
                    If (_previewRecord("Transaction Remarks").ToString.Length <= 140) Then
                        If (_previewRecord("Transaction Remarks").ToString.Contains(vbCrLf) Or _previewRecord("Transaction Remarks").ToString.Contains(vbCr) Or _previewRecord("Transaction Remarks").ToString.Contains(vbLf)) Then
                            Dim strAddrLine() As String = _previewRecord("Transaction Remarks").ToString().Split(vbLf)

                            If strAddrLine.Length > 2 Then
                                bError = True
                            ElseIf strAddrLine.Length >= 1 Then
                                Dim i As Int16 = 0
                                Dim CountMaxiumLine As Int16 = 0
                                Dim totalCount = 0

                                While (Not bError) AndAlso i < strAddrLine.Length
                                    If strAddrLine(i).Length > 70 Then
                                        bError = True
                                    Else
                                        i += 1
                                    End If
                                End While

                            End If
                        End If
                    Else
                        bError = True
                    End If

                    If bError Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Transaction Remarks"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Remarks").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "Transaction Remarks", "2 lines of 70 characters")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If
                ''''''''''''''''''''''''''auto cheque info'''''''''''''''''''
                If _previewRecord("AutoCheque Advice Information").ToString.Trim <> String.Empty Then
                    Dim bError As Boolean = False
                    If (_previewRecord("AutoCheque Advice Information").ToString.Length <= 2000) Then
                        If (_previewRecord("AutoCheque Advice Information").ToString.Contains(vbCrLf) Or _previewRecord("AutoCheque Advice Information").ToString.Contains(vbCr) Or _previewRecord("AutoCheque Advice Information").ToString.Contains(vbLf)) Then
                            Dim strAddrLine() As String = _previewRecord("AutoCheque Advice Information").ToString().Split(vbLf)

                            If strAddrLine.Length > 25 Then
                                bError = True
                            ElseIf strAddrLine.Length > 1 Then
                                Dim i As Int16 = 0
                                Dim CountMaxiumLine As Int16 = 0
                                Dim totalCount = 0

                                While (Not bError) AndAlso i < strAddrLine.Length
                                    If strAddrLine(i).Length > 80 Then
                                        Dim CountUntilLower As Int16 = strAddrLine(i).Length
                                        'bError = True
                                        'Else
                                        While 80 <= CountUntilLower
                                            CountUntilLower = CountUntilLower - 80
                                            CountMaxiumLine += 1
                                        End While
                                        i += 1
                                    Else
                                        i += 1
                                    End If

                                    'If CountMaxiumLine <> 0 Then
                                    '  totalCount = CountMaxiumLine + strAddrLine.Length
                                    'End If

                                    'If totalCount > 25 Then
                                    '    bError = True
                                    'End If

                                End While
                                If CountMaxiumLine <> 0 Then
                                    totalCount = CountMaxiumLine + strAddrLine.Length
                                End If

                                'The total line must not exceed 25
                                If totalCount > 25 Then
                                    bError = True
                                End If
                            End If
                        End If
                    Else
                        bError = True
                    End If

                    If bError Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "AutoCheque Advice Information"
                            .ColumnOrdinalNo = _PreviewTable.Columns("AutoCheque Advice Information").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "AutoCheque Advice Information", "25 lines of 80 characters")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ''''''''''changed ContainsISOButDoubleQuote to ContainsISOButDoubleQuote1'''''''''''''''''''''''''
                If _previewRecord("Transaction Remarks").ToString.Trim <> String.Empty And _
                    ContainsISOButDoubleQuote1(_previewRecord("Transaction Remarks").ToString) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Transaction Remarks"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Remarks").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Transaction Remarks")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                rowNo += 1
            Next

        Catch ex As Exception
            Throw New Exception("Error on Validation : " & ex.Message.ToString)
        End Try

        CustomRTMSValidate()

    End Sub

    Protected Overrides Sub CustomRTMSValidate()

        Dim rowNo As Int32 = 0
        'changed by fujitsu on 2013/03/21
        'Dim regUnpermittedChar As New Regex("[^0-9a-zA-Z/?(),.:'+\-\s]", RegexOptions.Compiled)
        Dim regUnpermittedChar As New Regex("[""]", RegexOptions.Compiled)  ' disallow doublq quote only
        'end change
        Dim formatError As FileFormatError

        ''''''''''''''''modified by Jacky''''''''''''''''''''''''''''
        'ContainsISOButDoubleQuoteAndMore(_previewRecord("Counter Party Address").ToString) Then

        'ContainsISOButDoubleQuoteAndMore(_previewRecord("AutoCheque Advice Information").ToString) Then
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        For Each _previewRecord As DataRow In _PreviewTable.Rows
            ' Field #16 - Counter Party Address
            If _previewRecord("Counter Party Address").ToString.Trim <> String.Empty And _
                ContainsISOButDoubleQuoteAndMore1(_previewRecord("Counter Party Address").ToString) Then
                formatError = New FileFormatError
                With formatError
                    .ColumnName = "Counter Party Address"
                    .ColumnOrdinalNo = _PreviewTable.Columns("Counter Party Address").Ordinal + 1
                    .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Counter Party Address")
                    .RecordNo = rowNo + 1
                End With
                _ValidationErrors.Add(formatError)
                _ErrorAtMandatoryField = True
            End If

            ' Field #18 - AutoCheque Advice Information
            If _previewRecord("AutoCheque Advice Information").ToString.Trim <> String.Empty And _
                ContainsISOButDoubleQuoteAndMore1(_previewRecord("AutoCheque Advice Information").ToString) Then
                formatError = New FileFormatError
                With formatError
                    .ColumnName = "AutoCheque Advice Information"
                    .ColumnOrdinalNo = _PreviewTable.Columns("AutoCheque Advice Information").Ordinal + 1
                    .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "AutoCheque Advice Information")
                    .RecordNo = rowNo + 1
                End With
                _ValidationErrors.Add(formatError)
                _ErrorAtMandatoryField = True
            End If

            ' Field #19 - Delivery Option : Must be one of default values in master template
            If _previewRecord("Delivery Option").ToString <> String.Empty Then
                Dim isFound As Boolean = False
                If Not IsNothingOrEmptyString(TblBankFields("Delivery Option").DefaultValue) Then
                    If TblBankFields("Delivery Option").DefaultValue.Length > 0 Then
                        Dim defaultCurrency() As String
                        defaultCurrency = TblBankFields("Delivery Option").DefaultValue.Split(",")
                        For Each val As String In defaultCurrency
                            If _previewRecord("Delivery Option").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                isFound = True
                                Exit For
                            End If
                        Next

                        If Not isFound Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Delivery Option"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Delivery Option").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Delivery Option' is not a valid Delivery Option.", rowNo + 1)
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

            Else
                'If DO is not specified and Counter Party Address is available, set 'BENE'
                If _previewRecord("Counter Party Address").ToString.Trim <> String.Empty Then
                    _previewRecord("Delivery Option") = "BENE"
                Else
                    'Otherwise set 'BANK'
                    _previewRecord("Delivery Option") = "BANK"
                End If

            End If

            rowNo += 1

        Next

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Function ContainsInvlidISO(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) < 32 Or Convert.ToInt32(chr) > 126 Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function ContainsISOButDoubleQuote(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Then
                If Convert.ToInt32(chr) = 34 Then Return True ' Do not allow double quote(")
            Else
                Return True
            End If
        Next
        Return False
    End Function
    Private Function ContainsISOButDoubleQuote1(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Or (Convert.ToInt32(chr) = 10 Or Convert.ToInt32(chr) = 13) Or (Convert.ToInt32(chr) = 13 And Convert.ToInt32(chr) = 10) Then
                If Convert.ToInt32(chr) = 34 Then Return True ' Do not allow double quote(")
            Else
                Return True
            End If
        Next
        Return False
    End Function

    Private Function ContainsISOButDoubleQuoteAndMore(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Then
                If Convert.ToInt32(chr) = 124 Or Convert.ToInt32(chr) = 34 Or Convert.ToInt32(chr) = 91 Or Convert.ToInt32(chr) = 92 Or Convert.ToInt32(chr) = 93 Or Convert.ToInt32(chr) = 94 Then Return True ' Do not allow these characters (|"\[]^)
            Else
                Return True
            End If
        Next
        Return False
    End Function
    Private Function ContainsISOButDoubleQuoteAndMore1(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            ''''''''modified by jacky''''''''''''''''''''
            'changed if case from Convert.ToInt32(chr) >= 32 to Convert.ToInt32(chr) >= 10  on 2018-10-26
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Or (Convert.ToInt32(chr) = 10 Or Convert.ToInt32(chr) = 13) Or (Convert.ToInt32(chr) = 13 And Convert.ToInt32(chr) = 10) Then
                If Convert.ToInt32(chr) = 124 Or Convert.ToInt32(chr) = 34 Or Convert.ToInt32(chr) = 91 Or Convert.ToInt32(chr) = 92 Or Convert.ToInt32(chr) = 93 Or Convert.ToInt32(chr) = 94 Then Return True ' Do not allow these characters (|"\[]^)
            Else
                Return True
            End If
        Next
        Return False
    End Function

    Private Function NumberOfDecimalPlaces(ByVal textvalue As String) As Integer
        Dim indexOfDecimalPoint As Integer = textvalue.IndexOf(".")
        If indexOfDecimalPoint = -1 Then
            Return 0
        Else
            Return textvalue.Substring(indexOfDecimalPoint + 1).Length
        End If

    End Function

#End Region


End Class

