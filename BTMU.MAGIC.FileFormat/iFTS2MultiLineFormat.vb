'************************************************CHANGE REQUESTS**********************************'
'SRS No                     Date Modified   Description
'-------------------------------------------------------------------------------------------------'
'SRS/BTMU/2010-0025         01/Apr/2010     Add Email Validations
'                                           Email Format : local-part@domain-part
'                                           1.  Local-part can use the letters a-z and A-Z, 
'                                               digits 0-9, Hyphen(-), underscore(_) and dot(.)
'                                           2.  Domain-part can use the letters a-z and A-z,
'                                               digits 0-9, Hyphen(-) and dot(.)
'                                           3.  Dot(.) cannot be first or last character in  
'                                               local-partor domain-part
'                                           4.  Dot(.) cannot appear two or more times 
'                                               consecutively
'                                           5.  Allow multiple email ids separated by semicolon(;)
'                                           6.  If Comma is used, replace it with semicolon
'                                           7.  Remove the space between email ids
'*************************************************************************************************'
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions


''' <summary>
''' Encapsulates the iFTS2 MultiLine output Format:
''' Header Record
''' Payment Record
''' WHTax Record-1
''' ....
''' WHTax Record-N
''' Invoice Record-1
''' ....
''' Invoice Record-N
''' </summary>
Public Class iFTS2MultiLineFormat
    Inherits BaseFileFormat

    'Target Data Tables
    Private _tblPayment As DataTable
    Private _tblTax As DataTable
    Private _tblInvoice As DataTable

    'Source Data Tables
    Private _tblPaymentSrc As DataTable
    Private _tblTaxSrc As DataTable
    Private _tblInvoiceSrc As DataTable

#Region "Local Constants"

    Private Shared _amountFieldName As String = "Gross Amount"

    'Only P-Records can be filtered/grouped/referenced
    Private Shared _validgroupbyFields() As String = {"Record Type-P", "Beneficiary Name", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type" _
                                                        , "Gross Amount", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary", "WHT Delivery Method" _
                                                        , "Consolidate Field"}

    Private Shared _editableFields() As String = { _
                                                    "Record Type-P", "Beneficiary Name", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type" _
                                                    , "Gross Amount", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary", "WHT Delivery Method" _
 _
                                                    , "Record Type-W", "W/H Tax No.", "Name", "Address", "Tax ID/ID Card No.", "No. (in the form)", "Tax Form" _
                                                    , "The Payer of Income", "The Payer of Income Description", "Type of Income (1)", "Type of Income (1) Description" _
                                                    , "Date (1)", "Amount Paid (1)", "Tax Rate (1)", "WHT Amount (1)", "Type of Income (2)", "Type of Income (2) Description" _
                                                    , "Date (2)", "Amount Paid (2)", "Tax Rate (2)", "WHT Amount (2)", "Type of Income (3)", "Type of Income (3) Description" _
                                                    , "Date (3)", "Amount Paid (3)", "Tax Rate (3)", "WHT Amount (3)" _
 _
                                                    , "Record Type-I", "Invoice No.", "Invoice Date", "Description", "Invoice Amount", "VAT", "Net Amount" _
                                                     }

    Private Shared _autoTrimmableFields() As String = { _
                                                    "Beneficiary Name", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary" _
                                                    , "W/H Tax No.", "Name", "Address", "No. (in the form)", "The Payer of Income Description" _
                                                    , "Type of Income (1) Description", "Type of Income (2) Description", "Type of Income (3) Description" _
                                                    , "Description"}

    'Fields: Payment Record
    Private Shared _fieldsPaymentRecord() As String = {"Record Type-P", "Beneficiary Name", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type" _
                                                        , "Gross Amount", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary", "WHT Delivery Method" _
                                                        , "Consolidate Field"}
    'Fields: Tax Record
    Private Shared _fieldsTaxRecord() As String = {"Record Type-W", "W/H Tax No.", "Name", "Address", "Tax ID/ID Card No.", "No. (in the form)", "Tax Form" _
                                                    , "The Payer of Income", "The Payer of Income Description", "Type of Income (1)", "Type of Income (1) Description" _
                                                    , "Date (1)", "Amount Paid (1)", "Tax Rate (1)", "WHT Amount (1)", "Type of Income (2)", "Type of Income (2) Description" _
                                                    , "Date (2)", "Amount Paid (2)", "Tax Rate (2)", "WHT Amount (2)", "Type of Income (3)", "Type of Income (3) Description" _
                                                    , "Date (3)", "Amount Paid (3)", "Tax Rate (3)", "WHT Amount (3)"}
    'Fields: Invoice Record
    Private Shared _fieldsInvoiceRecord() As String = {"Record Type-I", "Invoice No.", "Invoice Date", "Description", "Invoice Amount", "VAT", "Net Amount"}

#End Region

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iFTS-2 MultiLine"
        End Get
    End Property

#End Region

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Public Shared Function OutputFormatString() As String
        Return "iFTS-2 MultiLine"
    End Function

    Public Sub New()
        SetupPTITables()
    End Sub

#Region "Overriden Methods"

    ''' <summary>
    ''' Copy Source Values to BankFieds per Mapping and Add the record to "PreviewData" Data Table 
    ''' </summary>
    Protected Overrides Sub ReadAllSourceData()

        Try

            Try

                Dim obj As CommonTemplateTextDetail = _DataModel
                _tblPaymentSrc = obj._tblPaymentSrc.Copy()
                _tblTaxSrc = obj._tblTaxSrc.Copy()
                _tblInvoiceSrc = obj._tblInvoiceSrc.Copy()

            Catch ex As InvalidCastException
                Dim obj As CommonTemplateExcelDetail = _DataModel
                _tblPaymentSrc = obj._tblPaymentSrc.Copy()
                _tblTaxSrc = obj._tblTaxSrc.Copy()
                _tblInvoiceSrc = obj._tblInvoiceSrc.Copy()
            End Try

            Dim tmpDb As New DataSet()
            tmpDb.Tables.Add(_tblPaymentSrc)
            tmpDb.Tables.Add(_tblTaxSrc)
            tmpDb.Tables.Add(_tblInvoiceSrc)

            Dim RPaymentTax As New DataRelation("PaymentTax" _
                   , tmpDb.Tables(0).Columns("ID") _
                   , tmpDb.Tables(1).Columns("ID"))

            Dim RPaymentInvoice As New DataRelation("PaymentInvoice" _
              , tmpDb.Tables(0).Columns("ID") _
              , tmpDb.Tables(2).Columns("ID"))

            tmpDb.Relations.Add(RPaymentTax)
            tmpDb.Relations.Add(RPaymentInvoice)

            'Copy Source Data to Preview Table based on Map Settings
            Dim newrecord As DataRow

            For Each row As DataRow In tmpDb.Tables(0).Rows

                '#1. Copy Source Data to Payment Bank Fields
                For Each BankField As MapBankField In TblBankFields.Values

                    If Array.IndexOf(_fieldsPaymentRecord, BankField.BankField) = -1 Then Continue For

                    For Each srcfld As MapSourceField In BankField.MappedSourceFields

                        If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                            srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                        ElseIf _tblTaxSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                            Dim taxrecords() As DataRow = row.GetChildRows("PaymentTax")
                            If taxrecords.Length > 0 Then srcfld.SourceFieldValue = taxrecords(0)(srcfld.SourceFieldName)
                        ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                            Dim invoicerecords() As DataRow = row.GetChildRows("PaymentInvoice")
                            If invoicerecords.Length > 0 Then srcfld.SourceFieldValue = invoicerecords(0)(srcfld.SourceFieldName)
                        End If

                    Next

                Next

                newrecord = _PreviewTable.NewRow()

                newrecord("File Format Type") = "F2"
                ' Add Payment Record
                For Each column As DataColumn In _tblPayment.Columns
                    If column.ColumnName = "ID" Then Continue For                    
                    newrecord(column.ColumnName) = TblBankFields(column.ColumnName).MappedValues
                Next
                _PreviewTable.Rows.Add(newrecord)


                '#2. Copy Source Data to WHTax Bank Fields
                'In case Source Fields of Invoice Data have been mapped to Tax Bank Fields,
                ' value of the first row will be taken
                For Each taxrow As DataRow In row.GetChildRows("PaymentTax")


                    For Each BankField As MapBankField In TblBankFields.Values

                        If Array.IndexOf(_fieldsTaxRecord, BankField.BankField) = -1 Then Continue For

                        For Each srcfld As MapSourceField In BankField.MappedSourceFields

                            If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                            ElseIf _tblTaxSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(taxrow(srcfld.SourceFieldName))
                            ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                Dim invoicerecords() As DataRow = row.GetChildRows("PaymentInvoice")
                                If invoicerecords.Length > 0 Then srcfld.SourceFieldValue = invoicerecords(0)(srcfld.SourceFieldName)
                            End If

                        Next

                    Next

                    newrecord = _PreviewTable.NewRow()
                    ' Add WHTax Record
                    For Each column As DataColumn In _tblTax.Columns
                        If column.ColumnName = "ID" Then Continue For
                        newrecord(column.ColumnName) = TblBankFields(column.ColumnName).MappedValues
                    Next
                    _PreviewTable.Rows.Add(newrecord)

                Next

                '#3. Copy Source Data to Invoice Bank Fields
                'In case Source Fields of Tax Data have been mapped to Invoice Bank Fields,
                ' value of the first row will be taken
                For Each invoicerow As DataRow In row.GetChildRows("PaymentInvoice")

                    For Each BankField As MapBankField In TblBankFields.Values

                        If Array.IndexOf(_fieldsInvoiceRecord, BankField.BankField) = -1 Then Continue For

                        For Each srcfld As MapSourceField In BankField.MappedSourceFields

                            If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                            ElseIf _tblTaxSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                Dim taxrecords() As DataRow = row.GetChildRows("PaymentTax")
                                If taxrecords.Length > 0 Then srcfld.SourceFieldValue = taxrecords(0)(srcfld.SourceFieldName)
                            ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(invoicerow(srcfld.SourceFieldName))
                            End If

                        Next

                    Next

                    newrecord = _PreviewTable.NewRow()
                    ' Add Invoice Record
                    For Each column As DataColumn In _tblInvoice.Columns
                        If column.ColumnName = "ID" Then Continue For
                        newrecord(column.ColumnName) = TblBankFields(column.ColumnName).MappedValues
                    Next
                    _PreviewTable.Rows.Add(newrecord)

                Next

            Next

            ' Clean up
            tmpDb.Relations.Remove("PaymentTax")
            tmpDb.Relations.Remove("PaymentInvoice")

            'tmpDb.Tables.RemoveAt(0)
            'tmpDb.Tables.RemoveAt(0)
            'tmpDb.Tables.RemoveAt(0)
            tmpDb.Dispose()

            ' To replace dbnull valuse with empty string
            For Each row As DataRow In _PreviewTable.Rows
                For Each col As DataColumn In _PreviewTable.Columns
                    If row(col) Is DBNull.Value Then row(col) = ""
                Next
            Next
   
        Catch ex As InvalidCastException
            Throw New MagicException("Unsupported Common Template for iFTS-2 MultiLine Format!")
        Catch ex As Exception
            Throw New MagicException("Error reading Source Data(ReadAllSourceData): " & ex.Message.ToString)
        End Try

    End Sub

    

    ''' <summary>
    ''' Translate Bank Fields
    ''' Translation defined on Payment Bank Field is applicable to Payment Records.
    ''' Similarly the Translation defined on WHTax and Invoice Bank Fields are applicable to respective records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyTranslation()

        Dim pid As String = ""
        Dim wid As String = ""
        Dim iid As String = ""

        Try

            Dim obj As CommonTemplateTextDetail = _DataModel
            pid = IIf(IsNothingOrEmptyString(obj.PIndicator), "", obj.PIndicator)
            wid = IIf(IsNothingOrEmptyString(obj.WIndicator), "", obj.WIndicator)
            iid = IIf(IsNothingOrEmptyString(obj.IIndicator), "", obj.IIndicator)
        Catch ex As InvalidCastException
            Dim obj As CommonTemplateExcelDetail = _DataModel
            pid = IIf(IsNothingOrEmptyString(obj.PIndicator), "", obj.PIndicator)
            wid = IIf(IsNothingOrEmptyString(obj.WIndicator), "", obj.WIndicator)
            iid = IIf(IsNothingOrEmptyString(obj.IIndicator), "", obj.IIndicator)
        End Try


        Try

            For Each translatedBankField As TranslatorSetting In _DataModel.TranslatorSettings

                For Each _previewRecord As DataRow In _PreviewTable.Rows

                    If Array.IndexOf(_fieldsPaymentRecord, translatedBankField.BankFieldName) <> -1 Then
                        If Not pid.Equals(_previewRecord("Record Type-P").ToString, StringComparison.InvariantCultureIgnoreCase) _
                           AndAlso Not "P".Equals(_previewRecord("Record Type-P").ToString, StringComparison.InvariantCultureIgnoreCase) Then Continue For
                    ElseIf Array.IndexOf(_fieldsTaxRecord, translatedBankField.BankFieldName) <> -1 Then
                        If Not wid.Equals(_previewRecord("Record Type-W").ToString, StringComparison.InvariantCultureIgnoreCase) _
                           AndAlso Not "W".Equals(_previewRecord("Record Type-W").ToString, StringComparison.InvariantCultureIgnoreCase) Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord, translatedBankField.BankFieldName) <> -1 Then
                        If Not iid.Equals(_previewRecord("Record Type-I").ToString, StringComparison.InvariantCultureIgnoreCase) _
                           AndAlso Not "I".Equals(_previewRecord("Record Type-I").ToString, StringComparison.InvariantCultureIgnoreCase) Then Continue For
                    End If

                    'Is BankField a Tax Field? and Is there a Tax Record? Then Apply Translation
                    If Array.IndexOf(_fieldsTaxRecord, translatedBankField.BankFieldName) <> -1 Then

                        If translatedBankField.BankFieldName.Equals("Type of Income (2)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 2) Then Continue For
                        End If


                        If translatedBankField.BankFieldName.Equals("Type of Income (3)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 3) Then Continue For
                        End If

                    End If

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If translatedBankField.CustomerValue.Equals(_previewRecord(translatedBankField.BankFieldName).ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Translation Settings(ApplyTranslation) : {0}", ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply Lookup
    ''' Applicable only for Payment Records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyLookup()

        Dim _rowindex As Int32 = 0

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                If _previewRecord("Record Type-P") Is DBNull.Value Then Continue For
                If _previewRecord("Record Type-P") <> "P" Then Continue For

                For Each lookupBankField As LookupSetting In _DataModel.LookupSettings

                    If Array.IndexOf(_fieldsPaymentRecord, lookupBankField.BankFieldName) = -1 Then Continue For

                    _previewRecord(lookupBankField.BankFieldName) = GetLookupValue(lookupBankField.TableName, _tblPaymentSrc.Rows(_rowindex)(lookupBankField.SourceFieldName), lookupBankField.LookupValue.Split("-")(0), lookupBankField.LookupKey.Split("-")(0))
                Next

                _rowindex += 1

            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error applying Lookup Settings(ApplyLookup) at Recod: {0} Error: {1}", _rowindex, ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply Calculation 
    ''' Applicable only for Payment Records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyCalculation()

        Dim _calvalue As Decimal = 0, rowIndex As Int32 = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                If _previewRecord("Record Type-P") Is DBNull.Value Then Continue For
                If _previewRecord("Record Type-P") <> "P" Then Continue For

                For Each calculatedBankField As CalculatedField In _DataModel.CalculatedFields

                    If Array.IndexOf(_fieldsPaymentRecord, calculatedBankField.BankFieldName) = -1 Then Continue For

                    If calculatedBankField.Operator1 = "+" Then
                        _calvalue = System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand1)) _
                                    + System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand2))
                    Else
                        _calvalue = System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand1)) _
                                    - System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand2))
                    End If

                    If calculatedBankField.Operator2 = "+" Then
                        _calvalue = _calvalue + System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "-" Then
                        _calvalue = _calvalue - System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand3))
                    End If

                    _previewRecord(calculatedBankField.BankFieldName) = _calvalue.ToString()

                Next

                rowIndex = rowIndex + 1
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error applying Calculation Settings(ApplyCalculation) at Recod: {0} Error: {1}", rowIndex, ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply String Manipulation
    ''' String Manipulation defined on Payment Bank Field is applicable to Payment Records.
    ''' Similarly the String Manipulation defined on WHTax and Invoice Bank Fields are applicable to respective records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyStringManipulation()

        Try
            Dim _svalue As String

            For Each manipulatedBankField As StringManipulation In _DataModel.StringManipulations

                For Each _previewRecord As DataRow In _PreviewTable.Rows

                    If Array.IndexOf(_fieldsPaymentRecord, manipulatedBankField.BankFieldName) <> -1 Then
                        If _previewRecord("Record Type-P") Is DBNull.Value Then Continue For
                        If _previewRecord("Record Type-P") <> "P" Then Continue For
                    ElseIf Array.IndexOf(_fieldsTaxRecord, manipulatedBankField.BankFieldName) <> -1 Then
                        If _previewRecord("Record Type-W") Is DBNull.Value Then Continue For
                        If _previewRecord("Record Type-W") <> "W" Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord, manipulatedBankField.BankFieldName) <> -1 Then
                        If _previewRecord("Record Type-I") Is DBNull.Value Then Continue For
                        If _previewRecord("Record Type-I") <> "I" Then Continue For
                    End If

                    _svalue = _previewRecord(manipulatedBankField.BankFieldName)

                    If _svalue = String.Empty Then Continue For

                    Select Case manipulatedBankField.FunctionName

                        Case "LEFT"
                            _svalue = Left(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "RIGHT"
                            _svalue = Right(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "SUBSTRING"
                            _svalue = Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value)
                        Case "REMOVE"
                            ''''''''''Modified by jacky on 2019-07-15'''''''''
                            'Purpose: Replace function should only do once
                            '_svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "")
                            _svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "", , 1)
                            ''''''''''''''''''''''''''''''''''''''''''
                        Case "REMOVE LAST"
                            If _svalue.Length > manipulatedBankField.NumberOfCharacters.Value Then
                                _svalue = _svalue.Substring(0, _svalue.Length - manipulatedBankField.NumberOfCharacters.Value)
                            Else
                                _svalue = ""
                            End If

                    End Select

                    _previewRecord(manipulatedBankField.BankFieldName) = _svalue

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying String Manipulation Settings(ApplyStringManipulation) : {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub GenerateHeader()

        _Header = _PreviewTable.Rows(0)("File Format Type").ToString()

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim strField As String = String.Empty
        Dim strFieldAmtPaid As String = String.Empty
        Dim strFieldTxRate As String = String.Empty
        Dim strFieldWHTAmt As String = String.Empty

        For Each row As DataRow In _PreviewTable.Rows
            

            'Validate Fields: Payment Record '''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row("Record Type-P").ToString() = "P" Then

                If Not IsNothingOrEmptyString(row("Bank Charge")) Then
                    row("Bank Charge") = row("Bank Charge").ToString.ToUpper
                End If

                If Not IsNothingOrEmptyString(row("Service Type")) Then
                    row("Service Type") = row("Service Type").ToString.PadLeft(TblBankFields("Service Type").DataLength.Value, "0")
                End If

                'new spec. requirement
                If IsNothingOrEmptyString(row("WHT Delivery Method")) Then
                    row("WHT Delivery Method") = "C"
                Else
                    row("WHT Delivery Method") = row("WHT Delivery Method").ToString.ToUpper.Replace("""", "")
                End If

                If row("Account No.") <> "" Then
                    row("Account No.") = row("Account No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "")
                End If

                If row("Gross Amount") <> "" AndAlso Decimal.TryParse(row("Gross Amount").ToString(), Nothing) Then
                    row("Gross Amount") = row("Gross Amount").ToString.Replace(_DataModel.ThousandSeparator, "")
                End If


                If row("Fax No.") <> "" Then
                    row("Fax No.") = row("Fax No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "")
                End If

                'SRS/BTMU/2010-0025
                If row("E-mail Address") <> "" Then
                    row("E-mail Address") = row("E-mail Address").ToString.Replace(",", ";")
                    Dim mailids As New System.Text.StringBuilder
                    For Each mailid As String In row("E-mail Address").ToString().Split(";")
                        mailids.Append(mailid.Trim)
                        mailids.Append(";")
                    Next
                    row("E-mail Address") = mailids.ToString.Substring(0, mailids.ToString.Length - 1) ' to remove the ; at the end
                End If

                'Validate Fields: Tax Record ''''''''''''''''''''''''''''''''''''''''''''''''''
            ElseIf row("Record Type-W").ToString() = "W" Then

                For _cnt As Int32 = 1 To 3
                    strField = String.Format("Amount Paid ({0})", _cnt)
                    If row(strField) <> "" AndAlso Decimal.TryParse(row(strField).ToString(), Nothing) Then
                        row(strField) = row(strField).ToString.Replace(_DataModel.ThousandSeparator, "")
                    End If
                Next

                For _cnt As Int32 = 1 To 3

                    strFieldAmtPaid = String.Format("Amount Paid ({0})", _cnt)
                    strFieldTxRate = String.Format("Tax Rate ({0})", _cnt)
                    strFieldWHTAmt = String.Format("WHT Amount ({0})", _cnt)

                    If row(strFieldTxRate) <> "" AndAlso Decimal.TryParse(row(strFieldAmtPaid).ToString(), Nothing) AndAlso Decimal.TryParse(row(strFieldTxRate).ToString(), Nothing) Then
                        row(strFieldWHTAmt) = ((Decimal.Parse(row(strFieldAmtPaid)) * Decimal.Parse(row(strFieldTxRate))) / 100).ToString()
                    End If
                Next

                'Validate Fields: Invoice Record '''''''''''''''''''''''''''''''''''''''''''''''
            ElseIf row("Record Type-I").ToString() = "I" Then


            End If

        Next

    End Sub

    'Validates Mandatory Fields.
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0
        Dim IsPRecord, IsTRecord, IsIRecord As Boolean

        Try

            'File Format Type is Required
            If IsNothingOrEmptyString(_PreviewTable.Rows(0)("File Format Type")) Then
                AddValidationError("File Format Type", _PreviewTable.Columns("File Format Type").Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09030160"), "Header", "", "File Format Type") _
                                   , RowNumber + 1, True)
            End If

            For Each row As DataRow In _PreviewTable.Rows

                IdentifyRecordType(row, IsPRecord, IsTRecord, IsIRecord)

                If IsPRecord Then 'Validate Fields: Payment Record  

                    For Each _column As String In New String() {"Record Type-P", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type", "Gross Amount"}

                        If IsNothingOrEmptyString(row(_column)) Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                                , RowNumber + 1, True)
                        End If

                    Next

                ElseIf IsTRecord Then 'Validate Fields: Tax Record 

                    For Each _column As String In New String() {"Record Type-W", "W/H Tax No.", "Name", "Address", "Tax ID/ID Card No.", "Tax Form", "Type of Income (1)", "Amount Paid (1)", "WHT Amount (1)", "WHT Amount (2)", "WHT Amount (3)", "Amount Paid (2)", "Amount Paid (3)"}

                        If IsNothingOrEmptyString(row(_column)) Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                                , RowNumber + 1, True)
                        End If

                    Next

                ElseIf IsIRecord Then 'Validate Fields: Invoice Record

                    For Each _column As String In New String() {"Record Type-I", "Invoice No.", "Invoice Date", "Net Amount"}

                        If IsNothingOrEmptyString(row(_column)) Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                                , RowNumber + 1, True)
                        End If

                    Next

                End If

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type, Conditional Mandatory and Custom Validations 
    '(other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim IsPRecord, IsTRecord, IsIRecord As Boolean
        Dim found As Boolean = False
        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim strField As String = String.Empty
        Dim _ValueDate As Date
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
        Dim regAllow2 As New Regex("[^0-9]", RegexOptions.Compiled)
        'SRS/BTMU/2010-0025
        'Dim regEmail As New System.Text.RegularExpressions.Regex("^[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$", System.Text.RegularExpressions.RegexOptions.Compiled Or System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim regEmail As New System.Text.RegularExpressions.Regex("^[a-z0-9_-]+(\.[a-z0-9_-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$", System.Text.RegularExpressions.RegexOptions.Compiled Or System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                IdentifyRecordType(_previewRecord, IsPRecord, IsTRecord, IsIRecord)

                If (Not (IsPRecord Or IsTRecord Or IsIRecord)) _
                    OrElse (IsPRecord AndAlso (IsTRecord Or IsIRecord)) _
                    OrElse (IsTRecord AndAlso (IsPRecord Or IsIRecord)) _
                    OrElse (IsIRecord AndAlso (IsPRecord Or IsTRecord)) Then
                    AddValidationError("", 1, String.Format(MsgReader.GetString("E09030220"), RowNumber + 1) _
                                        , RowNumber + 1, True)
                    Continue For
                End If

                If RowNumber = 0 Then

                    '#1. File Format Type' is incorrect. It must be 'F2'.
                    If (_previewRecord("File Format Type") & "").ToString().ToUpper() <> "F2" Then
                        AddValidationError("File Format Type", _PreviewTable.Columns("File Format Type").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Format Type", "'F2'") _
                                            , RowNumber + 1, True)
                    End If

                    '#2. First Record must be P Record
                    If Not IsPRecord Then
                        AddValidationError("Record Type-P", _PreviewTable.Columns("Record Type-P").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-P", "'P'") _
                                            , RowNumber + 1, True)
                    End If
                End If


                If IsPRecord Then 'Validate Fields: Payment Record

                    '#3. 'Record Type-P' is incorrect. It must be 'P'.
                    If _previewRecord("Record Type-P").ToString() <> "" AndAlso _previewRecord("Record Type-P").ToString().ToUpper() <> "P" Then
                        AddValidationError("Record Type-P", _PreviewTable.Columns("Record Type-P").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-P", "'P'") _
                                            , RowNumber + 1, True)
                    End If

                    '#4. Account No. Must be numeric
                    If _previewRecord("Account No.") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("Account No.").ToString()) Then
                            'If Not Decimal.TryParse(_previewRecord("Account No.").ToString(), Nothing) Then
                            AddValidationError("Account No.", _PreviewTable.Columns("Account No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Account No.") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    '#5. Bank Code Must be numeric
                    If _previewRecord("Bank Code") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("Bank Code").ToString()) Then
                            'If Not Decimal.TryParse(_previewRecord("Bank Code").ToString(), Nothing) Then
                            AddValidationError("Bank Code", _PreviewTable.Columns("Bank Code").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Bank Code") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    '#6. Branch Code Must be numeric
                    If _previewRecord("Branch Code") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("Branch Code").ToString()) Then
                            'If Not Decimal.TryParse(_previewRecord("Branch Code").ToString(), Nothing) Then
                            AddValidationError("Branch Code", _PreviewTable.Columns("Branch Code").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Branch Code") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    '#7. Bank Charge - either "BEN" or "OUR"
                    If _previewRecord("Bank Charge") <> "" Then

                        If Not (_previewRecord("Bank Charge").ToString().ToUpper() = "BEN" _
                            Or _previewRecord("Bank Charge").ToString().ToUpper() = "OUR") Then
                            AddValidationError("Bank Charge", _PreviewTable.Columns("Bank Charge").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Bank Charge") _
                                                , RowNumber + 1, True)
                        End If

                    End If

                    '#8. Service Type - must be one of {"01", "02", "03", "04", "05", "06", "07", "59"}
                    If _previewRecord("Service Type") <> "" Then
                        found = False
                        For Each _validSvcType As String In New String() {"01", "02", "03", "04", "05", "06", "07", "59"}
                            If _previewRecord("Service Type").ToString() = _validSvcType Then
                                found = True
                                Exit For
                            End If
                        Next
                        If Not found Then
                            AddValidationError("Service Type", _PreviewTable.Columns("Service Type").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Service Type") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#9. Gross Amount

                    If _previewRecord("Gross Amount") <> "" Then

                        If Decimal.TryParse(_previewRecord("Gross Amount"), dAmount) Then

                            If IsConsolidated Then
                                'If IsNegativePaymentOption Then
                                '    If dAmount >= 0 Then
                                '        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1, True)
                                '    End If
                                'Else
                                '    If dAmount <= 0 Then
                                '        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1, True)
                                '    End If
                                'End If
                            Else
                                'If dAmount <= 0 Then 'Should not be negative or 0
                                '    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                '                        , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Gross Amount") _
                                '                        , RowNumber + 1, True)
                                'End If
                            End If

                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Gross Amount") _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Gross Amount", 13) _
                                                        , RowNumber + 1, True)
                                End If
                            Else
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Length > 13 Then
                                    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Gross Amount", 13) _
                                                        , RowNumber + 1, True)
                                End If
                            End If

                        Else
                            AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Gross Amount") _
                                                , RowNumber + 1, True)
                        End If

                    End If

                    '#10. Fax No. - Must be numeric
                    If _previewRecord("Fax No.") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("Fax No.").ToString()) Then 'numeric values only
                            'If Not Decimal.TryParse(_previewRecord("Fax No.").ToString(), Nothing) Then
                            AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Fax No.") _
                                                , RowNumber + 1, True)
                        End If

                    End If

                    '#11. - WHT Delivery Method - must be one of ("C", "R", "M")
                    If _previewRecord("WHT Delivery Method") <> "" Then

                        If _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "C" _
                           AndAlso _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "R" _
                           AndAlso _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "M" Then
                            AddValidationError("WHT Delivery Method", _PreviewTable.Columns("WHT Delivery Method").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "WHT Delivery Method") _
                                                , RowNumber + 1, True)
                        End If

                    End If

                    '#11.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                    For Each _field As String In New String() {"Beneficiary Name", "E-mail Address", "Attention", "Message to Beneficiary"}
                        If _previewRecord(_field) <> "" Then
                            If Not IsValidiFTS2Text(_previewRecord(_field).ToString()) Then
                                AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Next

                    '#11.a. Multiple Email addresses separated by comma (;) are allowed
                    If _previewRecord("E-mail Address") <> "" Then

                        For Each mail As String In _previewRecord("E-mail Address").ToString().Split(";")
                            If Not regEmail.IsMatch(mail) Then
                                AddValidationError("E-mail Address", _PreviewTable.Columns("E-mail Address").Ordinal + 1 _
                                                     , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "E-mail Address") _
                                                     , RowNumber + 1, True)
                                Exit For
                            End If
                        Next

                    End If

                    'For Each _field As String In New String() {"E-mail Address", "Attention", "Message to Beneficiary", "W/H Tax No."}
                    '    If _previewRecord(_field) <> "" Then
                    '        If ContainsInvalidCharacters(_previewRecord(_field).ToString) Then
                    '            AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                    '                                                           , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                    '                                                            , RowNumber + 1, True)
                    '        End If
                    '    End If
                    'Next

                ElseIf IsTRecord Then 'Validate Fields: Tax Record

                    '#12. 'Record Type-W' is incorrect. It must be 'W'.
                    If _previewRecord("Record Type-W").ToString() <> "" AndAlso _previewRecord("Record Type-W").ToString().ToUpper() <> "W" Then
                        AddValidationError("Record Type-W", _PreviewTable.Columns("Record Type-W").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-W", "'W'") _
                                            , RowNumber + 1, True)
                    End If

                    '#13. Tax ID/ID Card No. - Must be numeric and 10 or 13 digits in length
                    If _previewRecord("Tax ID/ID Card No.") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("Tax ID/ID Card No.").ToString()) Then 'numeric values only
                            'If Not Decimal.TryParse(_previewRecord("Tax ID/ID Card No.").ToString(), Nothing) Then
                            AddValidationError("Tax ID/ID Card No.", _PreviewTable.Columns("Tax ID/ID Card No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Tax ID/ID Card No.") _
                                                , RowNumber + 1, True)
                        End If

                        If _previewRecord("Tax ID/ID Card No.").ToString().Length <> 10 AndAlso _previewRecord("Tax ID/ID Card No.").ToString().Length <> 13 Then
                            AddValidationError("Tax ID/ID Card No.", _PreviewTable.Columns("Tax ID/ID Card No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09060010"), "Record", RowNumber + 1, "Tax ID/ID Card No.") _
                                                , RowNumber + 1, True)
                        End If

                    End If

                    '#14. No. (in the form) - Must be numeric
                    If _previewRecord("No. (in the form)") <> "" Then
                        If regAllow2.IsMatch(_previewRecord("No. (in the form)").ToString()) Then 'numeric values only
                            'If Not Decimal.TryParse(_previewRecord("No. (in the form)").ToString(), Nothing) Then
                            AddValidationError("No. (in the form)", _PreviewTable.Columns("No. (in the form)").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "No. (in the form)") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    '#15. Tax Form - Must be numeric and can have value between 1 and 7
                    If _previewRecord("Tax Form") <> "" Then

                        If Decimal.TryParse(_previewRecord("Tax Form").ToString(), dAmount) Then
                            If dAmount < 1 OrElse dAmount > 7 Then
                                AddValidationError("Tax Form", _PreviewTable.Columns("Tax Form").Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Tax Form") _
                                                   , RowNumber + 1, True)
                            End If
                        Else
                            AddValidationError("Tax Form", _PreviewTable.Columns("Tax Form").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Tax Form") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    '#16. The Payer of Income - Must be numeric and can have value between 1 and 4
                    If _previewRecord("The Payer of Income") <> "" Then

                        If Decimal.TryParse(_previewRecord("The Payer of Income").ToString(), dAmount) Then
                            If dAmount < 1 OrElse dAmount > 4 Then
                                AddValidationError("The Payer of Income", _PreviewTable.Columns("The Payer of Income").Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "The Payer of Income") _
                                                   , RowNumber + 1, True)
                            End If

                            If dAmount = 4 Then
                                If IsNothingOrEmptyString(_previewRecord("The Payer of Income Description")) Then
                                    AddValidationError("The Payer of Income Description", _PreviewTable.Columns("The Payer of Income Description").Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "The Payer of Income Description", "'The Payer of Income' is '4'") _
                                                      , RowNumber + 1, True)
                                End If
                            Else
                                If Not IsNothingOrEmptyString(_previewRecord("The Payer of Income Description")) Then
                                    AddValidationError("The Payer of Income Description", _PreviewTable.Columns("The Payer of Income Description").Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09050011"), "Record", RowNumber + 1, "The Payer of Income Description", "'The Payer of Income' is not '4'") _
                                                      , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError("The Payer of Income", _PreviewTable.Columns("The Payer of Income").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "The Payer of Income") _
                                               , RowNumber + 1, True)
                        End If

                    End If

                    For _cnt As Int32 = 1 To 3

                        '#17. Type of Income - Must be numeric and can have one of:(1, 2, 3, 41, 42, 43, 44, 45, 46, 47, 51, 52, 61, or 62)
                        strField = String.Format("Type of Income ({0})", _cnt)
                        If _previewRecord(strField) <> "" Then

                            If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then

                                If _cnt = 1 Then
                                    'Type of Income (1), (2), (3) cannot be duplicated
                                    'compare with Type of Income (1) with (2)
                                    If _previewRecord("Type of Income (2)") <> "" Then
                                        If Decimal.TryParse(_previewRecord("Type of Income (2)"), Nothing) Then

                                            'compare (1) with (2)
                                            If dAmount = Decimal.Parse(_previewRecord("Type of Income (2)")) Then
                                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                      , RowNumber + 1, True)
                                            End If
                                            If Decimal.TryParse(_previewRecord("Type of Income (3)"), Nothing) Then
                                                'compare (1) with (3)
                                                If dAmount = Decimal.Parse(_previewRecord("Type of Income (3)")) Then
                                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                          , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                          , RowNumber + 1, True)
                                                End If
                                                'compare (2) with (3)
                                                If Decimal.Parse(_previewRecord("Type of Income (2)")) = Decimal.Parse(_previewRecord("Type of Income (3)")) Then
                                                    AddValidationError("Type of Income (2)", _PreviewTable.Columns("Type of Income (2)").Ordinal + 1 _
                                                          , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, "Type of Income (2)") _
                                                          , RowNumber + 1, True)
                                                End If
                                            End If

                                        End If
                                    End If
                                    If _previewRecord("Type of Income (3)") <> "" Then
                                        If Decimal.TryParse(_previewRecord("Type of Income (3)"), Nothing) Then
                                            'compare (1) with (3)
                                            If dAmount = Decimal.Parse(_previewRecord("Type of Income (3)")) Then
                                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                      , RowNumber + 1, True)
                                            End If
                                        End If
                                    End If


                                End If

                                found = False
                                For Each _value As Int32 In New Int32() {1, 2, 3, 41, 42, 43, 44, 45, 46, 47, 51, 52, 61, 62}
                                    If dAmount = _value Then
                                        found = True
                                        Exit For
                                    End If
                                Next
                                If Not found Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                       , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                       , RowNumber + 1, True)
                                End If

                                Select Case _previewRecord(strField).ToString()
                                    Case "51", "52", "61", "62"
                                        'If _previewRecord("Tax Form") = "4" OrElse _previewRecord("Tax Form") = "7" Then
                                        If IsNothingOrEmptyString(_previewRecord(String.Format("Type of Income ({0}) Description", _cnt))) Then
                                            AddValidationError(String.Format("Type of Income ({0}) Description", _cnt), _PreviewTable.Columns(String.Format("Type of Income ({0}) Description", _cnt)).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, String.Format("Type of Income ({0}) Description", _cnt)) _
                                                   , RowNumber + 1, True)
                                        End If
                                        'End If
                                    Case Else
                                        If _previewRecord("Tax Form") = "4" OrElse _previewRecord("Tax Form") = "7" Then
                                            If IsNothingOrEmptyString(_previewRecord(String.Format("Type of Income ({0}) Description", _cnt))) Then
                                                AddValidationError(String.Format("Type of Income ({0}) Description", _cnt), _PreviewTable.Columns(String.Format("Type of Income ({0}) Description", _cnt)).Ordinal + 1 _
                                                       , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, String.Format("Type of Income ({0}) Description", _cnt)) _
                                                       , RowNumber + 1, True)
                                            End If
                                        End If
                                End Select

                                '##########################################################
                                'strField = String.Format("Amount Paid ({0})", _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                      , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, strField) _
                                '                      , RowNumber + 1, True)
                                'End If

                                ''#20.Tax Rate 
                                ''20.1 If 'WHT Amount (2)' is blank Then 'Tax Rate (2)' is mandatory.
                                'strField = String.Format("WHT Amount ({0})", _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    strField = String.Format("Tax Rate ({0})", _cnt)
                                '    If _previewRecord(strField).ToString.Trim = "" Then
                                '        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                          , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, strField, String.Format("WHT Amount ({0}) is blank", _cnt)) _
                                '                          , RowNumber + 1, True)
                                '    End If
                                'End If

                                '#21.WHT Amount
                                '21.1 If 'Tax Rate (2)' is blank Then 'WHT Amount (2)' is mandatory.
                                'strField = String.Format("Tax Rate ({0})", _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    strField = String.Format("WHT Amount ({0})", _cnt)
                                '    If _previewRecord(strField).ToString.Trim = "" Then
                                '        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                          , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, strField, String.Format("Tax Rate ({0}) is blank", _cnt)) _
                                '                          , RowNumber + 1, True)
                                '    Else

                                '    End If
                                'End If

                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                   , RowNumber + 1, True)
                            End If ' if decimal.tryparse()...end if

                        End If ' if _previewRecord(strField) <> "" ... end if

                        '20.2 Tax Rate must be numeric and within the range 0.01 and 100.00.
                        strField = String.Format("Tax Rate ({0})", _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField), dAmount) Then
                                If dAmount < 0.01 OrElse dAmount > 100.0 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                  , String.Format(MsgReader.GetString("E09050050"), "Record", RowNumber + 1, strField, "0.01", "100.00") _
                                                  , RowNumber + 1, True)
                                End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed  3  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 3 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 3) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else ' if no decimal separator
                                    'Integral part of the amount should not exceed  3  
                                    If dAmount.ToString().Length > 3 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 3) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If
                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                              , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                              , RowNumber + 1, True)
                            End If
                        End If



                        '21.2 WHT Amount must be numeric and cannot be <= 0 
                        strField = String.Format("WHT Amount ({0})", _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField), dAmount) Then

                                'Must be <= Amount Paid
                                If Decimal.TryParse(_previewRecord(String.Format("Amount Paid ({0})", _cnt)), Nothing) Then
                                    If dAmount > Decimal.Parse(_previewRecord(String.Format("Amount Paid ({0})", _cnt))) Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09020201"), "Record", RowNumber + 1, strField) _
                                        , RowNumber + 1, True)
                                    End If
                                End If

                                'If dAmount <= 0 Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                  , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, strField) _
                                '                  , RowNumber + 1, True)
                                'End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed  13  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else ' if no decimal separator
                                    'Integral part of the amount should not exceed  13  
                                    If dAmount.ToString().Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If
                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                              , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                              , RowNumber + 1, True)
                            End If
                        End If

                        '#18. Date - must be valid date and can be either in yymmdd or ymmdd format
                        strField = String.Format("Date ({0})", _cnt)
                        If _previewRecord(strField) <> "" Then
                            If (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", TblBankFields(strField).DateFormat, _ValueDate, True)) _
                              AndAlso (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", "yMMdd", _ValueDate, True)) Then

                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, strField) _
                                                       , RowNumber + 1, True)
                            End If
                        End If

                        '#19. Amount Paid - Must be numeric and cannot be <=0 
                        strField = String.Format("Amount Paid ({0})", _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then

                                'If dAmount <= 0 Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                      , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, strField) _
                                '                      , RowNumber + 1, True)
                                'End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed 13  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else 'if no decimal separator
                                    'Integral part of the amount should not exceed 13  
                                    If dAmount.ToString().Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If

                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                                    , RowNumber + 1, True)
                            End If
                        End If


                    Next ' for cnt...next

                    '#21.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                    For Each _field As String In New String() {"W/H Tax No.", "Name", "Address", "The Payer of Income Description", "Type of Income (1) Description", "Type of Income (2) Description", "Type of Income (3) Description"}
                        If _previewRecord(_field) <> "" Then
                            If Not IsValidiFTS2Text(_previewRecord(_field).ToString()) Then
                                AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Next

                ElseIf IsIRecord Then 'Validate Fields: Invoice Record

                    '#22. 'Record Type-I' is incorrect. It must be 'I'.
                    If _previewRecord("Record Type-I").ToString() <> "" AndAlso _previewRecord("Record Type-I").ToString().ToUpper() <> "I" Then
                        AddValidationError("Record Type-I", _PreviewTable.Columns("Record Type-I").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-I", "'I'") _
                                            , RowNumber + 1, True)
                    End If

                    '#23. Invoice No. - must be alphanumeric
                    If _previewRecord("Invoice No.") <> "" Then
                        If regAllowAlphaNum.IsMatch(_previewRecord("Invoice No.").ToString()) Then
                            AddValidationError("Invoice No.", _PreviewTable.Columns("Invoice No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Invoice No.") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#24. Invoice Date - must be valid date and can be either in yymmdd or ymmdd format
                    If _previewRecord("Invoice Date") <> "" Then
                        If (Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), "", TblBankFields("Invoice Date").DateFormat, _ValueDate, True)) _
                          AndAlso (Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), "", "yMMdd", _ValueDate, True)) Then
                            AddValidationError("Invoice Date", _PreviewTable.Columns("Invoice Date").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Invoice Date") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#25. Invoice Amount - must be numeric
                    If _previewRecord("Invoice Amount") <> "" Then
                        If Decimal.TryParse(_previewRecord("Invoice Amount"), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Invoice Amount") _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                    AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Invoice Amount", 13) _
                                                        , RowNumber + 1, True)
                                End If
                            Else 'if no decimal separatot
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Length > 13 Then
                                    AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Invoice Amount", 13) _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Invoice Amount") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#26. VAT - must be numeric
                    If _previewRecord("VAT") <> "" Then
                        If Decimal.TryParse(_previewRecord("VAT"), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("VAT", _PreviewTable.Columns("VAT").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "VAT") _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                    AddValidationError("VAT", _PreviewTable.Columns("VAT").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "VAT", 13) _
                                                        , RowNumber + 1, True)
                                End If
                            Else
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Length > 13 Then
                                    AddValidationError("VAT", _PreviewTable.Columns("VAT").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "VAT", 13) _
                                                        , RowNumber + 1, True)
                                End If

                            End If
                        Else
                            AddValidationError("VAT", _PreviewTable.Columns("VAT").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "VAT") _
                                            , RowNumber + 1, True)
                        End If
                    End If

                    '#27. Net Amount - must be numeric
                    If _previewRecord("Net Amount") <> "" Then
                        If Decimal.TryParse(_previewRecord("Net Amount"), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Net Amount", _PreviewTable.Columns("Net Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Net Amount") _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 14  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 14 Then
                                    AddValidationError("Net Amount", _PreviewTable.Columns("Net Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Net Amount", 14) _
                                                        , RowNumber + 1, True)
                                End If
                            Else
                                'Integral part of the amount should not exceed 14  
                                If dAmount.ToString().Length > 14 Then
                                    AddValidationError("Net Amount", _PreviewTable.Columns("Net Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Net Amount", 14) _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError("Net Amount", _PreviewTable.Columns("Net Amount").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Net Amount") _
                                            , RowNumber + 1, True)
                        End If
                    End If

                    '#27.a.1 VAT <= INVOICE AMOUNT
                    If Decimal.TryParse(_previewRecord("Invoice Amount"), Nothing) AndAlso Decimal.TryParse(_previewRecord("VAT"), Nothing) Then
                        If Decimal.Parse(_previewRecord("VAT")) > Decimal.Parse(_previewRecord("Invoice Amount")) Then
                            AddValidationError("VAT", _PreviewTable.Columns("VAT").Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09020199"), "Record", RowNumber + 1, "VAT") _
                                   , RowNumber + 1, True)
                        End If
                    End If
                    '#27.a.2. NET AMOUNT <= INVOICE AMOUNT
                    If Decimal.TryParse(_previewRecord("Invoice Amount"), Nothing) AndAlso Decimal.TryParse(_previewRecord("Net Amount"), Nothing) Then
                        If Decimal.Parse(_previewRecord("Invoice Amount")) > Decimal.Parse(_previewRecord("Net Amount")) Then
                            AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09020200"), "Record", RowNumber + 1, "Invoice Amount") _
                                   , RowNumber + 1, True)
                        End If
                    End If
                    '#27.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                    For Each _field As String In New String() {"Invoice No.", "Description"}
                        If _previewRecord(_field) <> "" Then
                            If Not IsValidiFTS2Text(_previewRecord(_field).ToString()) Then
                                AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                                , RowNumber + 1, True)
                            End If
                        End If
                    Next

                End If
                For Each col As DataColumn In _PreviewTable.Columns
                    If Not IsNothingOrEmptyString(_previewRecord(col)) Then
                        If _previewRecord(col).ToString.Contains("""") Then
                            AddValidationError(col.ColumnName, _PreviewTable.Columns(col.ColumnName).Ordinal + 1 _
                                                                           , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, col.ColumnName) _
                                                                            , RowNumber + 1, True)

                        End If
                    End If
                Next
                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()
        If _DataModel.IsAmountInCents Then
            For Each row As DataRow In _PreviewTable.Rows

                row("Gross Amount") = (Convert.ToDecimal(row("Gross Amount")) / 100).ToString()
                If Not IsNothingOrEmptyString(row("Amount Paid (1)")) Then
                    row("Amount Paid (1)") = (Convert.ToDecimal(row("Amount Paid (1)")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("Amount Paid (2)")) Then
                    row("Amount Paid (2)") = (Convert.ToDecimal(row("Amount Paid (2)")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("Amount Paid (3)")) Then
                    row("Amount Paid (3)") = (Convert.ToDecimal(row("Amount Paid (3)")) / 100).ToString()
                End If

                'TODO : Remove the commented code after UAT
                '' ''If Not IsNothingOrEmptyString(row("Tax Rate (1)")) Then
                '' ''    row("Tax Rate (1)") = (Convert.ToDecimal(row("Tax Rate (1)")) / 100).ToString()
                '' ''End If

                '' ''If Not IsNothingOrEmptyString(row("Tax Rate (2)")) Then
                '' ''    row("Tax Rate (2)") = (Convert.ToDecimal(row("Tax Rate (2)")) / 100).ToString()
                '' ''End If

                '' ''If Not IsNothingOrEmptyString(row("Tax Rate (3)")) Then
                '' ''    row("Tax Rate (3)") = (Convert.ToDecimal(row("Tax Rate (3)")) / 100).ToString()
                '' ''End If

                If Not IsNothingOrEmptyString(row("WHT Amount (1)")) Then
                    row("WHT Amount (1)") = (Convert.ToDecimal(row("WHT Amount (1)")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("WHT Amount (2)")) Then
                    row("WHT Amount (2)") = (Convert.ToDecimal(row("WHT Amount (2)")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("WHT Amount (3)")) Then
                    row("WHT Amount (3)") = (Convert.ToDecimal(row("WHT Amount (3)")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("Invoice Amount")) Then
                    row("Invoice Amount") = (Convert.ToDecimal(row("Invoice Amount")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("VAT")) Then
                    row("VAT") = (Convert.ToDecimal(row("VAT")) / 100).ToString()
                End If

                If Not IsNothingOrEmptyString(row("Net Amount")) Then
                    row("Net Amount") = (Convert.ToDecimal(row("Net Amount")) / 100).ToString()
                End If
            Next
        End If

        _totalRecordCount = _tblPayment.Rows.Count
        Dim dv As DataView = _PreviewTable.DefaultView
        dv.RowFilter = "[Record Type-P]='P'"
        _totalRecordCount = dv.Count
        'dv.Dispose()

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Record Type-P").ToString = "P" AndAlso _row("Gross Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Gross Amount"))
            If _row("Record Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Gross Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Gross Amount"))
        Next

        '#2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '#3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        '#4. Preserve File Format Type
        Dim _fileformattype As String = _PreviewTable.Rows(0)("File Format Type").ToString()

        Try
            'Fork: Preview Table => Payment, Tax and Invoice Tables
            ForkPaymentRecords()

            dsPreview.Tables.Add(_tblPayment.Copy())
            dhelper.ds = dsPreview
            _totalRecordCount = _tblPayment.Rows.Count

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto5("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Gross Amount", IsCheckedSumAmount)

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            're-order the records back to it's original sorting
            Dim dv As DataView = _consolidatedData.DefaultView
            Try
                dv.Sort = "ID"
            Catch ex As Exception
            End Try
            _consolidatedData = dv.ToTable(_consolidatedData.TableName)

            'Remove consolidated field
            _consolidatedData.Columns.Remove("Consolidate Field")

            _tblPayment = _consolidatedData.Copy()

            'Join: Payment, Tax and Invoice Tables => Preview Table
            JoinPaymentRecords()

            'Restore File Format Type
            RestoreFileFormatType(_fileformattype)

            ValidationErrors.Clear()
            FormatOutput()
            GenerateHeader()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()

        Catch ex As Exception
            Throw New MagicException("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    'The output file consists of
    '1)	File Format Record (mandatory)
    '2)	3 Header Records (mandatory)- empty lines
    '3)	Payment Record (mandatory)
    '4)	Multiple WHT Records (optional)
    '5)	Multiple Invoice Record (optional)
    '�	iFTS-2 output file format will be a text file with all fields enclosed within double quotes and separated by comma. 
    '�	Empty data fields will be separated by comma without double quotes
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sTaxAmts As String = String.Empty
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        Dim objMasterTemplate As MasterTemplateNonFixed ' Non Fixed Type Master Template
        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            '1. File Format Type
            sText = _Header & vbNewLine
            '2. Three empty Header Rows
            sText &= vbNewLine & vbNewLine & vbNewLine

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                '3. Payment Record
                If _previewRecord("Record Type-P").ToString() = "P" Then

                    If _previewRecord("Gross Amount").ToString() <> "" Then
                        If _previewRecord("Gross Amount").ToString.StartsWith("-") Then
                            _previewRecord("Gross Amount") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Gross Amount"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("Gross Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Gross Amount"))), 2).ToString("############0.00")
                        End If
                    End If

                    For Each _column As String In _fieldsPaymentRecord
                        'To exclude Consolidation Field
                        If (Not _column.Equals("Consolidate Field")) Then
                            If _previewRecord(_column).ToString() = "" Then
                                sLine = sLine & IIf(sLine = "", "", ",") & _previewRecord(_column).ToString()
                            Else
                                sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                            End If
                        End If

                    Next

                    '4. Tax Records
                ElseIf _previewRecord("Record Type-W").ToString() = "W" Then

                    For _cnt As Int32 = 1 To 3

                        sTaxAmts = String.Format("Amount Paid ({0})", _cnt)
                        If _previewRecord(sTaxAmts).ToString() <> "" Then
                            If _previewRecord(sTaxAmts).ToString.StartsWith("-") Then
                                _previewRecord(sTaxAmts) = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("############0.00")
                            Else
                                _previewRecord(sTaxAmts) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("############0.00")
                            End If
                        End If

                        sTaxAmts = String.Format("Tax Rate ({0})", _cnt)
                        If _previewRecord(sTaxAmts).ToString() <> "" Then
                            If _previewRecord(sTaxAmts).ToString.StartsWith("-") Then
                                _previewRecord(sTaxAmts) = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("##0.00")
                            Else
                                _previewRecord(sTaxAmts) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("##0.00")
                            End If
                        End If

                        sTaxAmts = String.Format("WHT Amount ({0})", _cnt)
                        If _previewRecord(sTaxAmts).ToString() <> "" Then
                            If _previewRecord(sTaxAmts).ToString.StartsWith("-") Then
                                _previewRecord(sTaxAmts) = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("############0.00")
                            Else
                                _previewRecord(sTaxAmts) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sTaxAmts))), 2).ToString("############0.00")
                            End If

                        End If

                    Next

                    For Each _column As String In _fieldsTaxRecord

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", ",") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                    '5. Invoice Records
                ElseIf _previewRecord("Record Type-I").ToString() = "I" Then

                    If _previewRecord("Invoice Amount").ToString() <> "" Then
                        If _previewRecord("Invoice Amount").ToString.StartsWith("-") Then
                            _previewRecord("Invoice Amount") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Invoice Amount"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("Invoice Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Invoice Amount"))), 2).ToString("############0.00")
                        End If
                    End If

                    If _previewRecord("VAT").ToString() <> "" Then
                        If _previewRecord("VAT").ToString.StartsWith("-") Then
                            _previewRecord("VAT") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("VAT"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("VAT") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("VAT"))), 2).ToString("############0.00")
                        End If

                    End If

                    If _previewRecord("Net Amount").ToString() <> "" Then
                        If _previewRecord("Net Amount").ToString.StartsWith("-") Then
                            _previewRecord("Net Amount") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Net Amount"))), 2).ToString("#############0.00")
                        Else
                            _previewRecord("Net Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Net Amount"))), 2).ToString("#############0.00")
                        End If

                    End If

                    For Each _column As String In _fieldsInvoiceRecord

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", ",") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                End If

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Return SaveTextToFile(sText, _outputFile) '.Replace("iFTS2MultiLineFormat", "iFTS2")

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Amount Field has invalid numeric value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If

            End If
            If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                _ErrorAtMandatoryField = True
            End If
            RowNumber += 1

        Next

    End Sub

#End Region

#Region "Helper Methods"

    'Permitted Special Characters in iFTS-2 File Format despite Eng/Thai alphabets
    ' @	#	%	&	*	(	)	_	-	=	+	;	:	"	'	<
    '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
    Private Function IsValidiFTS2Text(ByVal textValue As String) As Boolean

        If IsNothingOrEmptyString(textValue) Then Return True

        textValue = textValue.Trim()

        Dim thaiEncoding As System.Text.Encoding = System.Text.Encoding.GetEncoding(874)
        '33 - 126 English chars: A-Za-z0-9 and special characters 
        '161 - 251 Thai Charas: Alphabets and Numbers
        For Each b As Byte In thaiEncoding.GetBytes(textValue)
            If (b > 32 AndAlso b < 127) OrElse (b > 160 AndAlso b < 252) Then
                'its english/thai alphabets/numbers/special chars
            Else
                ' non english/thai character
                'Return False ' If allow only ansi encoded thai characters, return false
                Return True
            End If

        Next

        Return True

    End Function
    Private Function ContainsInvalidCharacters(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in iFTS-2 File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Then
                If Convert.ToInt32(chr) = 34 Then Return True ' Do not allow double quote(")
            Else
                Return True
            End If
        Next
        Return False
    End Function
    'Initialize Payment/Tax/Invoice Tables
    Private Sub SetupPTITables()

        _tblPayment = New DataTable("TPayment")
        _tblPayment.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsPaymentRecord
            If _column = "ID" Then Continue For
            _tblPayment.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

        _tblTax = New DataTable("TTax")
        _tblTax.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsTaxRecord
            If _column = "ID" Then Continue For
            _tblTax.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

        _tblInvoice = New DataTable("TInvoice")
        _tblInvoice.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsInvoiceRecord
            If _column = "ID" Then Continue For
            _tblInvoice.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

    End Sub

    'Fork: Preview Table => Payment, Tax and Invoice Tables
    'PreviewTable is expected to have Payment, WHTax and Invoice Records on different rows as given below:
    ' P x x x x 
    '          W x x x x 
    '          W x x x x
    '                   I x x x
    Sub ForkPaymentRecords()

        On Error Resume Next

        '#1. Clear Payment Tables
        _tblPayment.Rows.Clear()
        _tblTax.Rows.Clear()
        _tblInvoice.Rows.Clear()

        '#2. Populate Payment Tables
        Dim id As Integer = 0
        Dim _NewRecord As DataRow

        For Each row As DataRow In _PreviewTable.Rows

            If row("Record Type-P").ToString() = "P" Then

                id += 1
                _NewRecord = _tblPayment.NewRow()
                _NewRecord("ID") = id.ToString()
                For Each _column As DataColumn In _tblPayment.Columns
                    If _column.ColumnName = "ID" Then Continue For
                    _NewRecord(_column.ColumnName) = row(_column.ColumnName)
                Next
                'Added for Consolidate field
                _NewRecord("Consolidate Field") = row("Consolidate Field")

                _tblPayment.Rows.Add(_NewRecord)

            ElseIf row("Record Type-W").ToString() = "W" Then

                _NewRecord = _tblTax.NewRow()
                _NewRecord("ID") = id.ToString()
                For Each _column As DataColumn In _tblTax.Columns
                    If _column.ColumnName = "ID" Then Continue For
                    _NewRecord(_column.ColumnName) = row(_column.ColumnName)
                Next
                _tblTax.Rows.Add(_NewRecord)

            ElseIf row("Record Type-I").ToString() = "I" Then

                _NewRecord = _tblInvoice.NewRow()
                _NewRecord("ID") = id.ToString()
                For Each _column As DataColumn In _tblInvoice.Columns
                    If _column.ColumnName = "ID" Then Continue For
                    _NewRecord(_column.ColumnName) = row(_column.ColumnName)
                Next
                _tblInvoice.Rows.Add(_NewRecord)

            End If

        Next

    End Sub

    'Join: Payment, Tax and Invoice Tables => Preview Table
    Sub JoinPaymentRecords()

        '#1. Clear Preview Table
        _PreviewTable.Rows.Clear()

        '#2. Populate Preview Table
        Dim newrecord As DataRow

        For Each row As DataRow In _tblPayment.Rows

            '#2.a. Add Payment Record
            newrecord = _PreviewTable.NewRow()
            For Each column As DataColumn In _tblPayment.Columns
                If column.ColumnName = "ID" OrElse column.ColumnName = "IDs" Then Continue For
                newrecord(column.ColumnName) = row(column.ColumnName)
            Next
            _PreviewTable.Rows.Add(newrecord)

            '#2.b. Add Tax Record
            Dim dv As DataView = _tblTax.DefaultView
            dv.RowFilter = "ID in (" & row("IDs").ToString() & ")"
            For Each taxrow As DataRowView In dv

                newrecord = _PreviewTable.NewRow()
                For Each column As DataColumn In _tblTax.Columns
                    If column.ColumnName = "ID" OrElse column.ColumnName = "IDs" Then Continue For
                    newrecord(column.ColumnName) = taxrow(column.ColumnName).ToString()
                Next
                _PreviewTable.Rows.Add(newrecord)

            Next

            '#2.c. Add Invoice Record
            dv = _tblInvoice.DefaultView
            dv.RowFilter = "ID in (" & row("IDs").ToString() & ")"
            For Each invoicerow As DataRowView In dv

                newrecord = _PreviewTable.NewRow()
                For Each column As DataColumn In _tblInvoice.Columns
                    If column.ColumnName = "ID" OrElse column.ColumnName = "IDs" Then Continue For
                    newrecord(column.ColumnName) = invoicerow(column.ColumnName).ToString()
                Next
                _PreviewTable.Rows.Add(newrecord)

            Next

        Next

        If _tblPayment.Columns.IndexOf("IDs") <> -1 Then _tblPayment.Columns.Remove("IDs")

        ' To replace dbnull valuse with empty string
        For Each row As DataRow In _PreviewTable.Rows
            For Each col As DataColumn In _PreviewTable.Columns
                If row(col) Is DBNull.Value Then row(col) = ""
            Next
        Next

    End Sub

    'Identifies the given Row as Payment/WHTax/Invoice Record
    Sub IdentifyRecordType(ByVal row As DataRow, ByRef IsPRecord As Boolean _
                            , ByRef IsTRecord As Boolean, ByRef IsIRecord As Boolean)
        IsPRecord = False
        IsTRecord = False
        IsIRecord = False

        For Each _pField As String In _fieldsPaymentRecord

            If IsNothingOrEmptyString(row(_pField)) Then Continue For
            IsPRecord = True
            Exit For

        Next

        For Each _tField As String In _fieldsTaxRecord

            If IsNothingOrEmptyString(row(_tField)) Then Continue For
            IsTRecord = True
            Exit For

        Next

        For Each _iField As String In _fieldsInvoiceRecord

            If IsNothingOrEmptyString(row(_iField)) Then Continue For
            IsIRecord = True
            Exit For

        Next

    End Sub

    Sub RestoreFileFormatType(ByVal _fileformattype As String)

        For Each row As DataRow In _PreviewTable.Rows
            row("File Format Type") = _fileformattype
            Exit For ' Sufficient to set it for the very first record
        Next

    End Sub

    Private Function DoTypeofIncomeFieldsHaveValue(ByVal row As DataRow, ByVal isetid As Int32) As Boolean

        For Each field As String In New String() {"Type of Income (" & isetid.ToString() & ")", "Type of Income (" & isetid.ToString() & ") Description", "Date (" & isetid.ToString() & ")", "Amount Paid (" & isetid.ToString() & ")", "Tax Rate (" & isetid.ToString() & ")", "WHT Amount (" & isetid.ToString() & ")"}
            'if any one of them have value
            If Not IsNothingOrEmptyString(row(field)) Then Return True
        Next

        Return False

    End Function

#End Region

End Class