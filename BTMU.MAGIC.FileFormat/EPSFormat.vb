'************************************************CHANGE REQUESTS**********************************'
'SRS No             Date Modified       Description
'-------------------------------------------------------------------------------------------------'
'SRS/BTMU/2010-0026 01/Apr/2010         Modified the ValidateDataLength function since the data 
'                                       length was not validated for Remitter and WHT fields
'SRS/BTMU/2010-0033 26/Apr/2010         Bank Field Value should not be trimmed.
'*************************************************************************************************'
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate
Imports System.io
Imports System.Text.RegularExpressions


''' <summary>
''' Encapsulates the EPSFormat
''' </summary>
''' <remarks></remarks>
Public Class EPSFormat
    Inherits BaseFileFormat

    Private Const RECORD_TYPE_H As String = "0"
    Private Const REEL_SEQUENCE As String = "01"
    Private Const FINANCIAL_INSTITUITON As String = "WBC"
    Private Const RECORD_TYPE_D As String = "1"
    Private Const WHT As String = "00000000"
    Private Const RECORD_TYPE_T As String = "7"
    Private Const FILLER_1 As String = "999-999"

    Private Shared _amountFieldName As String = "Amount"

    Private Shared _editableFields() As String = {"Record Type D", "BSB", "ACNO", "Indicator" _
                        , "Transaction Code", "Amount", "Beneficiary", "Lodgement Reference" _
                        , "Settlement Account BSB", "Settlement Account", "Settlement Account Suffix", "Remitter", "WHT"}

    Private Shared _autoTrimmableFields() As String = {"Beneficiary", "Lodgement Reference" _
                       , "Remitter", "Blank Field H1", "Blank Field H2", "Blank Field H3" _
                       , "Blank Field T1", "Blank Field T2", "Blank Field T3"}

    'In EPS format, group by fields and reference fields are same
    Private Shared _validGroupByFields() As String = {"Record Type H", "Blank Field H1", "Reel Sequence", "Financial Institution" _
                        , "Blank Field H2", "Customer Name", "APCA ID", "Description of file", "Value Date", "Blank Field H3" _
                        , "Record Type D", "BSB", "ACNO", "Indicator", "Transaction Code", "Amount", "Beneficiary", "Lodgement Reference" _
                        , "Settlement Account BSB", "Settlement Account", "Settlement Account Suffix", "Remitter", "WHT" _
                        , "Record Type T", "Filler 1", "Blank Field T1", "Total Amount", "Credit Amount", "Debit Amount", "Blank Field T2" _
                        , "Record Count", "Blank Field T3"}

    Protected indicatorList As New List(Of String)
    Protected transactionCodeList As New List(Of String)


    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

#Region "Overriden Properties"



    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "EPS"
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

#End Region

    Public Shared Function OutputFormatString() As String
        Return "EPS"
    End Function

#Region "Overriden Methods"

    ''' <summary>
    ''' Validates Data Length of Certain Fields
    ''' Error Code : E09020050
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ValidateDataLength()

        Try

            Dim RowNumber As Int32 = 0

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each _column As String In TblBankFields.Keys

                    'Need to skip Validating Data Length for Header / Trailer Fields at the Detail Rows
                    If RowNumber > 0 AndAlso "No".Equals(TblBankFields(_column).Detail, StringComparison.InvariantCultureIgnoreCase) Then Continue For

                    'Need to skip Validating Data Length for Header / Trailer Fields at the Header/Trailer Rows
                    If _PreviewTable.Columns(_column).Ordinal < 10 OrElse _PreviewTable.Columns(_column).Ordinal > 22 Then Continue For

                    If "Currency".Equals(TblBankFields(_column).DataType, StringComparison.InvariantCultureIgnoreCase) _
                        And "No".Equals(TblBankFields(_column).IncludeDecimal, StringComparison.InvariantCultureIgnoreCase) Then

                        If _previewRecord(_column).ToString().Length > _
                                (TblBankFields(_column).DataLength.Value + _
                                IIf(_DataModel.DecimalSeparator = "No Separator", 0, 1) + _
                                GetDecimalLengthForCurrency(_previewRecord("Currency").ToString())) Then

                            Dim validationError As New FileFormatError()
                            If TblBankFields(_column).Header Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            ElseIf TblBankFields(_column).Trailer Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            Else ' If detail
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = RowNumber + 1
                            End If
                            validationError.ColumnName = _column
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1
                            _ValidationErrors.Add(validationError)

                        End If

                    Else

                        If _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value Then

                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = _column
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1
                            If TblBankFields(_column).Header Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            ElseIf TblBankFields(_column).Trailer Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            Else ' If detail
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = RowNumber + 1
                            End If

                            _ValidationErrors.Add(validationError)
                            If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True

                        End If

                    End If

                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Data Length: " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' EPS is fixed width file format.
    ''' Adjust the Width of Numeric and Currency Values so as 
    ''' to fit the Decimal Length in FixedWidthMaster Template
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Function FixNumericData() As Integer

        Try

            Dim sNumber As String, sDecimal As String, iDecimalLength As Integer = 0
            For Each _BankField As MapBankField In TblBankFields.Values
                Select Case _BankField.DataType.ToUpper
                    Case "NUMBER" ' a. Numeric Value Size adjustment
                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            If _previewRecord(_BankField.BankField).ToString().Length < _BankField.DataLength.Value Then
                                ' For Preview, Amount field need not be padded with zero.
                                If _BankField.BankField.ToUpper() = "Amount".ToUpper() Then
                                    If _previewRecord(_BankField.BankField).ToString.Trim = String.Empty Then
                                        _previewRecord(_BankField.BankField) = 0
                                    End If
                                    _previewRecord(_BankField.BankField) = _previewRecord(_BankField.BankField).ToString().PadLeft(_BankField.DataLength.Value, " ")
                                Else
                                    _previewRecord(_BankField.BankField) = _previewRecord(_BankField.BankField).ToString().PadLeft(_BankField.DataLength.Value, "0")
                                End If
                            End If
                            'Following line is for All other Output Formats of Fixed width Master Template
                            '_previewRecord(_BankField.BankField) = _previewRecord(_BankField.BankField).ToString().PadLeft(_BankField.DataLength.Value, "0")
                        Next
                    Case "CURRENCY"  'b. Currency adjustment
                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            Dim numbers() As String = _previewRecord(_BankField.BankField).ToString().Split(".")
                            If numbers.Length = 2 Then
                                sNumber = numbers(0)
                                sDecimal = numbers(1)
                            Else
                                sNumber = _previewRecord(_BankField.BankField).ToString()
                                sDecimal = ""
                            End If
                            'Get Decimal Length from Default Value Field  
                            iDecimalLength = GetDecimalLengthForCurrency(_previewRecord("Currency").ToString())
                            If iDecimalLength = 0 Then iDecimalLength = 2
                            If sDecimal.Length < iDecimalLength Then
                                sDecimal = sDecimal.PadRight(iDecimalLength, "0")
                            ElseIf sDecimal.Length > iDecimalLength Then
                                If MsgBox("Error, Decimal length is greater than decimal data length in master template." _
                                    & Chr(13) & "Do you want to cut or repair customer data ?" _
                                    & Chr(13) & Chr(13) & "Click 'Yes', if you want to cut customer data." _
                                    & Chr(13) & "Click 'No', if you want to repair customer data.", _
                                    vbQuestion & vbYesNo, "Generate Common Transaction Template") = vbYes Then
                                    sDecimal = sDecimal.Substring(0, iDecimalLength)
                                Else
                                    Return -1
                                End If
                            End If

                            If _BankField.IncludeDecimal = "Yes" Then
                                sNumber = sNumber & sDecimal
                                If _BankField.DataLength.HasValue Then
                                    sNumber = sNumber.PadLeft(_BankField.DataLength.Value, "0")
                                End If
                                _previewRecord(_BankField.BankField) = sNumber
                            ElseIf _BankField.IncludeDecimal = "No" Then
                                If _BankField.DataLength.HasValue Then
                                    sNumber = sNumber.PadLeft(_BankField.DataLength.Value, "0")
                                End If
                                _previewRecord(_BankField.BankField) = sNumber & _BankField.DecimalSeparator & sDecimal
                            End If
                        Next
                End Select

            Next

        Catch ex As Exception
            Throw New Exception("Error on validating numeric data : " & ex.Message.ToString)
        End Try

        Return 0

    End Function

    ''' <summary>
    ''' Generates the field data as per EPS format 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GenerateOutputFormat()

        Dim totalAmount As Decimal = 0
        Dim _rowIndex As Integer = 0

        Try
            'Is Data is valid for Consolidation?
            For Each _row As DataRow In _PreviewTable.Rows
                _rowIndex += 1
                If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "Amount"))
                If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "Amount"))
                'If Convert.ToDecimal(_row("Amount").ToString) <= 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "Amount", "0"))
            Next
            _rowIndex = 0
            For Each _row As DataRow In _PreviewTable.Rows

                ' Generate - Amount : If the Amount is in Dollars, multiply by 100
                If _DataModel.IsAmountInDollars Then
                    If _row("Amount") = 0 Then
                        _row("Amount") = 0
                    Else
                        _row("Amount") = (Convert.ToDecimal(_row("Amount").ToString) * 100).ToString("##########")
                    End If
                End If


                ' Generate - Total Amount : Sum of each individual Amount

                totalAmount += Convert.ToDecimal(_row("Amount").ToString)

                ' Generate - Indicator : If empty the default as blank
                If IsNothingOrEmptyString(_row("Indicator").ToString) Then
                    _row("Indicator") = " "
                Else
                    _row("Indicator") = _row("Indicator").ToString.Trim.ToUpper
                End If

                ' Generate - ACNO : If the data exceeds the maximum length, then edit out hyphen
                If _row("ACNO").ToString.Length > TblBankFields("ACNO").DataLength.Value Then
                    _row("ACNO") = _row("ACNO").ToString.Trim.Replace("-", "")
                End If
                _rowIndex += 1
            Next

            _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########")
            _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########")
            _PreviewTable.Rows(0)("Record Count") = _PreviewTable.Rows.Count.ToString


            '    ' Format - Total Amount 
            '    If totalAmount < 0 Then
            '        totalAmount = Math.Abs(totalAmount)
            '        _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value - 1, "0")
            '        _PreviewTable.Rows(0)("Total Amount") = "-" & _PreviewTable.Rows(0)("Total Amount").ToString

            '        ' Format - Credit Amount
            '        _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value - 1, "0")
            '        _PreviewTable.Rows(0)("Credit Amount") = "-" & _PreviewTable.Rows(0)("Credit Amount").ToString

            '    Else
            '        _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value, "0")
            '        ' Format - Credit Amount
            '        _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value, "0")

            '    End If



            '    ' Format - Debit Amount
            '    _PreviewTable.Rows(0)("Debit Amount") = "0000000000"

            '    ' Format - Record Count
            '    _PreviewTable.Rows(0)("Record Count") = _PreviewTable.Rows.Count.ToString.PadLeft(TblBankFields("Record Count").DataLength.Value, "0")
        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch mex As MagicException
            Throw New MagicException(mex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub GenerateOutputFormat1()

        Dim totalAmount As Decimal = 0
        Dim _rowIndex As Integer = 0

        Try
            'Is Data is valid for Consolidation?
            For Each _row As DataRow In _PreviewTable.Rows
                _rowIndex += 1
                If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "Amount"))
                If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "Amount"))
                'If Convert.ToDecimal(_row("Amount").ToString) <= 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "Amount", "0"))
            Next
            _rowIndex = 0
            For Each _row As DataRow In _PreviewTable.Rows
                If _row("Amount") = 0 Then
                    _row("Amount") = 0
                Else
                    _row("Amount") = (Convert.ToDecimal(_row("Amount").ToString)).ToString("##########")
                End If
                '_row("Amount") = (Convert.ToDecimal(_row("Amount").ToString)).ToString("##########")
                ' Generate - Total Amount : Sum of each individual Amount
                totalAmount += Convert.ToDecimal(_row("Amount").ToString)

                ' Generate - Indicator : If empty the default as blank
                If IsNothingOrEmptyString(_row("Indicator").ToString) Then
                    _row("Indicator") = " "
                Else
                    _row("Indicator") = _row("Indicator").ToString.Trim.ToUpper
                End If

                ' Generate - ACNO : If the data exceeds the maximum length, then edit out hyphen
                If _row("ACNO").ToString.Length > TblBankFields("ACNO").DataLength.Value Then
                    _row("ACNO") = _row("ACNO").ToString.Trim.Replace("-", "")
                End If
                _rowIndex += 1
            Next

            _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########")
            _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########")
            _PreviewTable.Rows(0)("Record Count") = _PreviewTable.Rows.Count.ToString

            '' Format - Total Amount 
            'If totalAmount < 0 Then
            '    totalAmount = Math.Abs(totalAmount)
            '    _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value - 1, "0")
            '    _PreviewTable.Rows(0)("Total Amount") = "-" & _PreviewTable.Rows(0)("Total Amount").ToString

            '    ' Format - Credit Amount
            '    _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value - 1, "0")
            '    _PreviewTable.Rows(0)("Credit Amount") = "-" & _PreviewTable.Rows(0)("Credit Amount").ToString

            'Else
            '    _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value, "0")
            '    ' Format - Credit Amount
            '    _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value, "0")

            'End If

            '' Format - Debit Amount
            '_PreviewTable.Rows(0)("Debit Amount") = "0000000000"

            '' Format - Record Count
            '_PreviewTable.Rows(0)("Record Count") = _PreviewTable.Rows.Count.ToString.PadLeft(TblBankFields("Record Count").DataLength.Value, "0")
        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount"))
        Catch mex As MagicException
            Throw New MagicException(mex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)

            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub FormatOutput()
        Dim bsb As Decimal
        'SRS/BTMU/2010-0033
        For Each _previewRecord As DataRow In _PreviewTable.Rows

            _previewRecord("Blank Field H1") = _previewRecord("Blank Field H1").ToString().PadRight(TblBankFields("Blank Field H1").DataLength.Value, " ")
            _previewRecord("Blank Field H2") = _previewRecord("Blank Field H2").ToString().PadRight(TblBankFields("Blank Field H2").DataLength.Value, " ")
            _previewRecord("Customer Name") = _previewRecord("Customer Name").ToString().PadRight(TblBankFields("Customer Name").DataLength.Value, " ")
            _previewRecord("APCA ID") = _previewRecord("APCA ID").ToString().Trim.PadLeft(TblBankFields("APCA ID").DataLength.Value, "0")
            _previewRecord("Description of file") = _previewRecord("Description of file").ToString().PadRight(TblBankFields("Description of file").DataLength.Value, " ")
            _previewRecord("Blank Field H3") = _previewRecord("Blank Field H3").ToString().PadRight(TblBankFields("Blank Field H3").DataLength.Value, " ")
            _previewRecord("ACNO") = _previewRecord("ACNO").ToString().PadLeft(TblBankFields("ACNO").DataLength.Value, " ")
            _previewRecord("Beneficiary") = _previewRecord("Beneficiary").ToString().PadRight(TblBankFields("Beneficiary").DataLength.Value, " ")
            _previewRecord("Lodgement Reference") = _previewRecord("Lodgement Reference").ToString().PadRight(TblBankFields("Lodgement Reference").DataLength.Value, " ")
            _previewRecord("Settlement Account") = _previewRecord("Settlement Account").ToString().PadRight(TblBankFields("Settlement Account").DataLength.Value, " ")
            _previewRecord("Remitter") = _previewRecord("Remitter").ToString().PadRight(TblBankFields("Remitter").DataLength.Value, " ")
            _previewRecord("Blank Field T1") = _previewRecord("Blank Field T1").ToString().PadRight(TblBankFields("Blank Field T1").DataLength.Value, " ")
            _previewRecord("Blank Field T2") = _previewRecord("Blank Field T2").ToString().PadRight(TblBankFields("Blank Field T2").DataLength.Value, " ")
            _previewRecord("Record Count") = _previewRecord("Record Count").ToString().Trim.PadLeft(TblBankFields("Record Count").DataLength.Value, "0")
            _previewRecord("Blank Field T3") = _previewRecord("Blank Field T3").ToString().PadRight(TblBankFields("Blank Field T3").DataLength.Value, " ")
            ' Generate - Indicator : If empty the default as blank
            If IsNothingOrEmptyString(_previewRecord("Indicator").ToString) Then
                _previewRecord("Indicator") = " "
            Else
                _previewRecord("Indicator") = _previewRecord("Indicator").ToString.Trim.ToUpper
            End If
            '***************************************************************************************************'
            ' Added on      : 28-Oct-2009
            ' Added By      : Meera
            ' Requested By  : Sydney Branch
            ' Request       : If hyphen is omitted in Settlement Account BSB or BSB, then add hyphen at the 4th character

            If TblBankFields.ContainsKey("Settlement Account BSB") Then
                If Decimal.TryParse(_previewRecord("Settlement Account BSB").ToString, bsb) And _previewRecord("Settlement Account BSB").ToString.Length > 3 Then
                    _previewRecord("Settlement Account BSB") = String.Format("{0}-{1}", _previewRecord("Settlement Account BSB").ToString.Substring(0, 3), _previewRecord("Settlement Account BSB").ToString.Substring(3))
                End If
            End If

            If Decimal.TryParse(_previewRecord("BSB").ToString, bsb) And _previewRecord("BSB").ToString.Length > 3 Then
                _previewRecord("BSB") = String.Format("{0}-{1}", _previewRecord("BSB").ToString.Substring(0, 3), _previewRecord("BSB").ToString.Substring(3))
            End If
            '***************************************************************************************************'
        Next

        Dim totalAmount As Decimal
        totalAmount = Convert.ToDecimal(_PreviewTable.Rows(0)("Total Amount").ToString)

        If totalAmount < 0 Then

            totalAmount = Math.Abs(totalAmount)
            _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value, "0")
            _PreviewTable.Rows(0)("Total Amount") = "-" & _PreviewTable.Rows(0)("Total Amount").ToString

            _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value, "0")
            _PreviewTable.Rows(0)("Credit Amount") = "-" & _PreviewTable.Rows(0)("Credit Amount").ToString

        Else

            _PreviewTable.Rows(0)("Total Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Total Amount").DataLength.Value, "0")
            _PreviewTable.Rows(0)("Credit Amount") = totalAmount.ToString("##########").PadLeft(TblBankFields("Credit Amount").DataLength.Value, "0")

        End If

        _PreviewTable.Rows(0)("Debit Amount") = "0000000000"

    End Sub

    Protected Overrides Sub GenerateHeader()

        Dim formatError As FileFormatError

        Try

            _Header = String.Empty

            For Each bankField As MapBankField In TblBankFields.Values

                If bankField.Header Then
                    If _PreviewTable.Rows(0)(bankField.BankField).ToString.Length > TblBankFields(bankField.BankField).DataLength.Value Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = bankField.BankField
                            .ColumnOrdinalNo = _PreviewTable.Columns(bankField.BankField).Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020050"), "Header ", "", bankField.BankField, TblBankFields(bankField.BankField).DataLength.Value)
                            .RecordNo = 0
                        End With
                        _ValidationErrors.Add(formatError)
                        Select Case bankField.BankField
                            Case "Customer Name", "APCA ID", "Description of file"
                                _ErrorAtMandatoryField = True
                        End Select
                    End If
                    _Header += _PreviewTable.Rows(0)(bankField.BankField).ToString()
                End If

            Next

        Catch ex As Exception
            Throw New Exception("Error on generating Header Record : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub GenerateTrailer()

        Dim formatError As FileFormatError

        Try

            _Trailer = String.Empty

            For Each bankfield As MapBankField In TblBankFields.Values

                If bankfield.Trailer Then
                    If _PreviewTable.Rows(0)(bankfield.BankField).ToString.Length > TblBankFields(bankfield.BankField).DataLength.Value Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = bankfield.BankField
                            .ColumnOrdinalNo = _PreviewTable.Columns(bankfield.BankField).Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020050"), "Trailer ", "", bankfield.BankField, TblBankFields(bankfield.BankField).DataLength.Value)
                            .RecordNo = 0
                        End With
                        _ValidationErrors.Add(formatError)
                        Select Case bankfield.BankField
                            Case "Total Amount", "Credit Amount"
                                _ErrorAtMandatoryField = True
                        End Select
                    End If
                    _Trailer += _PreviewTable.Rows(0)(bankfield.BankField).ToString()
                End If

            Next

        Catch ex As Exception
            Throw New Exception("Error on Conversion(GenerateTrailer) : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub Validate()

        Dim hyphenPosition As Integer = 4
        Dim settlementAccount As String = String.Empty

        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim totalAmount As Decimal
        Dim _previewTableCopy As New DataTable

        Dim regACNO As New Regex("[^0-9\-]", RegexOptions.Compiled)
        Dim regSettlement As New Regex("[^0-9\-]", RegexOptions.Compiled)
        Dim regBSB As New Regex("^\d{3}-\d{3}", RegexOptions.Compiled)

        Dim account As Decimal

        totalAmount = 0

        rowNo = 0

        If TblBankFields.ContainsKey("Indicator") AndAlso indicatorList.Count = 0 Then
            indicatorList.AddRange(TblBankFields("Indicator").DefaultValue.Split(","))
        End If

        If TblBankFields.ContainsKey("Transaction Code") AndAlso transactionCodeList.Count = 0 Then
            transactionCodeList.AddRange(TblBankFields("Transaction Code").DefaultValue.Split(","))
        End If

        Try
            ' Validate the fields of each record and add file format errors to _validationerrors object
            For Each _row As DataRow In _PreviewTable.Rows
                ' Header and Trailer Field Validations
                If rowNo = 0 Then
                    ' ****************************Header Fields Validations *****************************'
                    ' Validation - Record Type H : Must be 0
                    If _row("Record Type H").ToString.Trim = String.Empty Or _
                            _row("Record Type H").ToString.Trim <> RECORD_TYPE_H Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Record Type H"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Record Type H").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030050"), "Header", "", "Record Type H", RECORD_TYPE_H)
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Blank Field H1 : Must be blank
                    If _row("Blank Field H1").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field H1"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field H1").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Header", "", "Blank Field H1")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Reel Sequence : Must be 01
                    If _row("Reel Sequence").ToString.Trim = String.Empty Or _
                    _row("Reel Sequence").ToString.Trim <> REEL_SEQUENCE Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Reel Sequence"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Reel Sequence").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030050"), "Header", "", "Reel Sequence", REEL_SEQUENCE)
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Financial Institution : Must be WBC
                    If _row("Financial Institution").ToString.Trim = String.Empty Or _
                    _row("Financial Institution").ToString.Trim <> FINANCIAL_INSTITUITON Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Financial Institution"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Financial Institution").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030050"), "Header", "", "Financial Institution", FINANCIAL_INSTITUITON)
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Blank Field H2 : Must be blank
                    If _row("Blank Field H2").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field H2"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field H2").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Header", "", "Blank Field H2")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Customer Name : Must not be blank
                    If _row("Customer Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Customer Name"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Customer Name").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030010"), "Header", "", "Customer Name")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - APCA ID : Must be numeric
                    If _row("APCA ID").ToString.Trim = String.Empty Or _
                    Not IsNumeric(_row("APCA ID").ToString.Trim) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "APCA ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("APCA ID").Ordinal + 1
                            .Description = String.Format("Header : " & vbTab & MsgReader.GetString("E09030020"), "Header", "", "APCA ID")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Description Of File : Must not be blank
                    If _row("Description Of file").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Description Of file"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Description Of file").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030010"), "Header", "", "Description Of file")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    If Not IsDateDataType(_row("Value Date").ToString, TblBankFields("Value Date").DateFormat, False) Then
                        ' Validation - Value Date : Should be ddMMyy format 
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Value Date"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030060"), "Header", "", "Value Date", TblBankFields("Value Date").DateFormat)
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    Else
                        ' Validation - Value Date : Must be greater than or equal to Today's date. 
                        Dim _ValueDate As Date
                        If IsDateDataType(_row("Value Date").ToString, "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then
                            If Date.Compare(_ValueDate, Date.Today) < 0 Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "Value Date"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09030070"), "Header", "", "Value Date")
                                    .RecordNo = rowNo
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If

                        ' Validation - Value Date : Must not be Saturday or Sunday. 
                        If Weekday(_ValueDate) = 1 Or Weekday(_ValueDate) = 7 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Value Date"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09020030"), "Header", "", "Value Date")
                                .RecordNo = rowNo
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If

                    ' Validation - Blank Field H3 : Must be blank
                    If _row("Blank Field H3").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field H3"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field H3").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Header", "", "Blank Field H3")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' ****************************Trailer Fields Validations *****************************'
                    ' Validation - Record Type T : Must be 7
                    If _row("Record Type T").ToString.Trim = String.Empty Or _
                                            _row("Record Type T").ToString.Trim <> RECORD_TYPE_T Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Record Type T"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Record Type T").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030050"), "Trailer", "", "Record Type T", "7")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Filler 1 : Must be 999-999
                    If _row("Filler 1").ToString.Trim = String.Empty Or _
                                            _row("Filler 1").ToString.Trim <> FILLER_1 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Filler 1"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Filler 1").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030050"), "Trailer", "", "Filler 1", "999-999")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Blank Field T1 : Must be blank
                    If _row("Blank Field T1").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field T1"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field T1").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Trailer", "", "Blank Field T1")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Total Amount : Must be > 0
                    'If Val(_row("Total Amount").ToString.Trim) <= 0 Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Total Amount"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Total Amount").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09030110"), "Trailer", "", "Total Amount", "0")
                    '        .RecordNo = rowNo
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If



                    ' Validation - Total Amount : Must be <= 9999999999
                    If Val(_row("Total Amount").ToString.Trim) > 9999999999 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Total Amount"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Total Amount").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030120"), "Trailer", "", "Total Amount", "9999999999")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Credit Amount : Must be > 0
                    'If Val(_row("Credit Amount").ToString.Trim) <= 0 Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Credit Amount"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Credit Amount").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09030110"), "Trailer", "", "Credit Amount", "0")
                    '        .RecordNo = rowNo
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If

                    ' Validation - Credit Amount : Must be <= 9999999999
                    If Val(_row("Credit Amount").ToString.Trim) > 9999999999 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Credit Amount"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Credit Amount").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030120"), "Trailer", "", "Credit Amount", "9999999999")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - Debit Amount : Must be 0
                    If _row("Debit Amount").ToString.Trim = "" Or _
                        Val(_row("Debit Amount").ToString.Trim) <> 0 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Debit Amount"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Debit Amount").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030140"), "Trailer", "", "Debit Amount", "0")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Blank Field T2 : Must be blank
                    If _row("Blank Field T2").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field T2"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field T2").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Trailer", "", "Blank Field T2")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Record Count : Must be < 999999
                    If Val(_row("Record Count").ToString.Trim) > 999999 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Record Count"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Record Count").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020040"), "Trailer", "", "Record Count", "999999")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If

                    ' Validation - Blank Field T3 : Must be blank
                    If _row("Blank Field T3").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Blank Field T3"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Blank Field T3").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030040"), "Trailer", "", "Blank Field T3")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                    End If
                End If


                ' ****************************Detail Fields Validations *****************************'
                ' Validation - Record Type D : Must be 1
                If _row("Record Type D").ToString.Trim = String.Empty Or _
                    _row("Record Type D").ToString.Trim <> RECORD_TYPE_D Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Record Type D"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Record Type D").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030050"), "Record", rowNo + 1, "Record Type D", RECORD_TYPE_D)
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - BSB : Should be of format '3digits-3digits'
                If Not regBSB.IsMatch(_row("BSB").ToString.Trim) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "BSB"
                        .ColumnOrdinalNo = _PreviewTable.Columns("BSB").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030090"), "Record", rowNo + 1, "BSB")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - ACNO : Numeric and Hyphen are allowed
                'Dim regACNO As New Regex("[0-9" & Regex.Escape("-") & "]")

                If regACNO.IsMatch(_row("ACNO").ToString.Trim) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "ACNO"
                        .ColumnOrdinalNo = _PreviewTable.Columns("ACNO").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030100"), "Record", rowNo + 1, "ACNO")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Indicator : Must be one of these values (N,W,X,Y or space)
                If indicatorList.IndexOf(_row("Indicator").ToString.ToUpper) = -1 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Indicator"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Indicator").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030100"), "Record", rowNo + 1, "Indicator")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Transaction Code : Must be one of these values (50,51,52,53,54,55,56 and 57)
                'If transactionCodeList.IndexOf(_row("Transaction Code")) = -1 Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Transaction Code"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Code").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09030100"), "Record ", rowNo + 1, "Transaction Code")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                ' Validation - Transaction Code : Must be one of these values (50, 53)
                If Not (_row("Transaction Code").ToString.Trim = "50" Or _row("Transaction Code").ToString.Trim = "53") Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Transaction Code"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Code").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030100"), "Record", rowNo + 1, "Transaction Code")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Amount : Must be > 0
                Dim amount As Decimal
                If IsConsolidated Then
                    If _row("Amount").ToString.Trim <> String.Empty And _
                           Decimal.TryParse(_row("Amount").ToString, amount) Then
                        'If IsNegativePaymentOption Then
                        '    If Val(_row("Amount").ToString.Trim) >= 0 Then
                        '        formatError = New FileFormatError
                        '        With formatError
                        '            .ColumnName = "Amount"
                        '            .ColumnOrdinalNo = _PreviewTable.Columns("Amount").Ordinal + 1
                        '            .Description = String.Format("Record: {0}, Field: '{1}' should not be zero or positive. Please check the payment option selected or there may be invalid data in your source file.", rowNo + 1, "Amount")
                        '            .RecordNo = rowNo + 1
                        '        End With
                        '        _ValidationErrors.Add(formatError)
                        '        _ErrorAtMandatoryField = True
                        '    End If
                        'Else
                        '    If Val(_row("Amount").ToString.Trim) <= 0 Then
                        '        formatError = New FileFormatError
                        '        With formatError
                        '            .ColumnName = "Amount"
                        '            .ColumnOrdinalNo = _PreviewTable.Columns("Amount").Ordinal + 1
                        '            .Description = String.Format("Record: {0}, Field: '{1}' should not be zero or negative. Please check the payment option selected or there may be invalid data in your source file.", rowNo + 1, "Amount")
                        '            .RecordNo = rowNo + 1
                        '        End With
                        '        _ValidationErrors.Add(formatError)
                        '        _ErrorAtMandatoryField = True
                        '    End If
                        'End If
                    End If
                Else
                    'If Val(_row("Amount").ToString.Trim) <= 0 Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Amount"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Amount").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09030110"), "Record", rowNo + 1, "Amount", "0")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                End If


                ' Validation - Amount : Must be <= 9999999999
                If Val(_row("Amount").ToString.Trim) > 9999999999 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Amount"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Amount").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030120"), "Record", rowNo + 1, "Amount", "9999999999")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Beneficiary : Must not be blank
                If _row("Beneficiary").ToString.Trim = String.Empty Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Beneficiary"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030010"), "Record", rowNo + 1, "Beneficiary")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Lodgement Reference : Must not be blank
                If _row("Lodgement Reference").ToString.Trim = String.Empty Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Lodgement Reference"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Lodgement Reference").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030010"), "Record", rowNo + 1, "Lodgement Reference")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                '*******************************************************************************************'
                ' Added On       :   28-Oct-2009
                ' Added By       :   Meera
                ' Requested By   :   Sydney Branch

                ' The following validation for Settlement Account is commented to incorporate the
                ' new requirement to split the Settlement Account into 3 fields - Settlement Account BSB,
                ' Settlement Account and Settlement Account Suffix 

                ' '' '' Validation - Settlement Account : Numeric and Hyphen are allowed
                '' ''If regSettlement.IsMatch(_row("Settlement Account").ToString.Trim) Then
                '' ''    formatError = New FileFormatError
                '' ''    With formatError
                '' ''        .ColumnName = "Settlement Account"
                '' ''        .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account").Ordinal + 1
                '' ''        .Description = String.Format(MsgReader.GetString("E09030100"), "Record", rowNo + 1, "Settlement Account")
                '' ''        .RecordNo = rowNo + 1
                '' ''    End With
                '' ''    _ValidationErrors.Add(formatError)
                '' ''    _ErrorAtMandatoryField = True
                '' ''End If

                ' '' '' Validation - Settlement Account : Character position 84 must be a hyphen
                '' ''settlementAccount = _row("Settlement Account").ToString
                '' ''If hyphenPosition < settlementAccount.Length Then
                '' ''    If settlementAccount(hyphenPosition - 1) <> "-" Then
                '' ''        formatError = New FileFormatError
                '' ''        With formatError
                '' ''            .ColumnName = "Settlement Account"
                '' ''            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account").Ordinal + 1
                '' ''            .Description = String.Format(MsgReader.GetString("E09030170"), "Record", rowNo + 1, "Settlement Account", hyphenPosition.ToString, "hyphen")
                '' ''            .RecordNo = rowNo + 1
                '' ''        End With
                '' ''        _ValidationErrors.Add(formatError)
                '' ''        _ErrorAtMandatoryField = True
                '' ''    End If
                '' ''End If

                ' Validation - Settlement Account BSB : Must be in the format 3digits-3digits
                If TblBankFields.ContainsKey("Settlement Account BSB") Then
                    If Not IsNothingOrEmptyString(_row("Settlement Account BSB")) Then
                        If Not regBSB.IsMatch(_row("Settlement Account BSB").ToString.Trim) Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Settlement Account BSB"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account BSB").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09030090"), "Record", rowNo + 1, "Settlement Account BSB")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

                ' Validation - Settlement Account : Must be numeric
                If Not IsNothingOrEmptyString(_row("Settlement Account")) Then
                    If Not Decimal.TryParse(_row("Settlement Account").ToString.Replace("-", ""), account) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Settlement Account"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Settlement Account")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If
                

                ' Validation - Settlement Account Suffix : Must be numeric
                If TblBankFields.ContainsKey("Settlement Account Suffix") Then
                    If Not IsNothingOrEmptyString(_row("Settlement Account Suffix")) Then
                        If Not Decimal.TryParse(_row("Settlement Account Suffix").ToString, account) Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Settlement Account Suffix"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account Suffix").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Settlement Account Suffix")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If
                '*******************************************************************************************'

                ' Validation - Remitter : Must be atleast 2 characters
                If _row("Remitter").ToString.Trim = String.Empty Or _row("Remitter").ToString.Trim.Length < 2 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Remitter"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Remitter").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030130"), "Record", rowNo + 1, "Remitter", "two")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - WHT : Must be 00000000
                If _row("WHT").ToString.Trim = String.Empty Or _row("WHT").ToString.Trim <> WHT Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "WHT"
                        .ColumnOrdinalNo = _PreviewTable.Columns("WHT").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030050"), "Record", rowNo + 1, "WHT", WHT)
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Do not allow unicode characters
                Dim pattern As String = "[^0-9A-Za-z\s" & Regex.Escape("@#%&*()_+;:""'<>,.?/~`!$^[{}|\") & "\-\=\]]"

                Dim regAllow As New Regex(pattern, RegexOptions.Compiled)

                For Each col As String In TblBankFields.Keys

                    If _row(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_row(col).ToString) Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = col
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: '{1}' contains invalid character.", rowNo + 1, col)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                Next

                rowNo += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Validation : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Generates Consolidated Data
    ''' </summary>
    ''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    ''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    ''' <param name="AppendText">Text to append with Reference Fields</param>
    ''' <remarks></remarks>
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        If GroupByFields.Count = 0 Then Exit Sub

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(GroupByFields, ReferenceFields)

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If
            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''


        Dim filterString As String = String.Empty
        Dim filterList As New List(Of String)
        Dim remittanceAmt As Decimal = 0
        Dim referenceField(1) As String
        Dim totalAmount As Decimal

        'Is Data is valid for Consolidation?
        Dim _rowIndex As Integer = 0
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "Amount"))
            If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "Amount"))
            'If Convert.ToDecimal(_row("Amount").ToString) <= 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "Amount", "0"))
        Next


        Try
            _consolidatedDataView = _PreviewTable.DefaultView ' DataView to apply rowfilter
            _consolidatedData = New DataTable ' DataTable to hold only consolidated records
            ' Create the columns for the Consolidated DataTable
            For Each _column As DataColumn In _PreviewTable.Columns
                _consolidatedData.Columns.Add(_column.ColumnName, GetType(String))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            'For Each _field As String In GroupByFields
            '    If Not TblBankFields.ContainsKey(_field) Then
            '        Throw New Exception("Group by field not defined in Master Template")
            '    End If
            'Next

            '1. Filter out unique rows for given Filter Group
            For Each _row As DataRow In _PreviewTable.Rows

                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_row(_field).ToString()))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For

                If filterList.IndexOf(filterString) = -1 Then
                    filterList.Add(filterString)
                    filterList.Sort()
                End If

            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next

            'For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
            '    If _referenceFields(intI, 1).Trim.Length <> 0 Then
            '        If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
            '            Throw New Exception("Reference field not defined in Master Template")
            '        End If
            '    End If
            'Next

            totalAmount = 0
            For Each _filter As String In filterList
                _consolidatedDataView.RowFilter = String.Empty
                _consolidatedDataView.RowFilter = _filter
                If _consolidatedDataView.Count > 0 Then
                    ' Create the reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next
                    remittanceAmt = 0
                    For Each _viewRow As DataRowView In _consolidatedDataView
                        remittanceAmt += Convert.ToDecimal(_viewRow(_amountFieldName).ToString)
                        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                            If _referenceFields(intI, 1).Trim.Length <> 0 Then
                                referenceField(intI) = referenceField(intI) & _viewRow(_referenceFields(intI, 1).ToString) & ","
                            End If
                        Next
                    Next

                    Dim consolidatedRow As DataRow
                    consolidatedRow = _consolidatedData.NewRow
                    _consolidatedDataView.Item(0)(_amountFieldName) = remittanceAmt.ToString
                    For Each _column As String In TblBankFields.Keys
                        consolidatedRow(_column) = _consolidatedDataView(0)(_column)
                        consolidatedRow(_amountFieldName) = remittanceAmt.ToString
                    Next
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            consolidatedRow(_referenceFields(intI, 1)) = referenceField(intI).Substring(0, referenceField(intI).Length - 1)
                        End If
                    Next
                    _consolidatedData.Rows.Add(consolidatedRow)
                    totalAmount += remittanceAmt
                End If
            Next

            _totalRecordCount = _PreviewTable.Rows.Count
            If _consolidatedData.Rows.Count > 0 Then

                ' Change the Header Field Values to the record No : 1 of the source file
                _consolidatedData.Rows(0)("Record Type H") = _PreviewTable.Rows(0)("Record Type H").ToString
                _consolidatedData.Rows(0)("Blank Field H1") = _PreviewTable.Rows(0)("Blank Field H1").ToString
                _consolidatedData.Rows(0)("Reel Sequence") = _PreviewTable.Rows(0)("Reel Sequence").ToString
                _consolidatedData.Rows(0)("Financial Institution") = _PreviewTable.Rows(0)("Financial Institution").ToString
                _consolidatedData.Rows(0)("Blank Field H2") = _PreviewTable.Rows(0)("Blank Field H2").ToString
                _consolidatedData.Rows(0)("Customer Name") = _PreviewTable.Rows(0)("Customer Name").ToString
                _consolidatedData.Rows(0)("APCA ID") = _PreviewTable.Rows(0)("APCA ID").ToString
                _consolidatedData.Rows(0)("Description of file") = _PreviewTable.Rows(0)("Description of file").ToString
                _consolidatedData.Rows(0)("Value Date") = _PreviewTable.Rows(0)("Value Date").ToString
                _consolidatedData.Rows(0)("Blank Field H3") = _PreviewTable.Rows(0)("Blank Field H3").ToString
                _consolidatedData.Rows(0)("Total Amount") = totalAmount.ToString("##########")
                _consolidatedData.Rows(0)("Credit Amount") = totalAmount.ToString("##########")


                _consolidatedData.Rows(0)("Record Count") = _consolidatedData.Rows.Count
                _consolidatedRecordCount = _consolidatedData.Rows.Count
                _PreviewTable.Rows.Clear()
                _PreviewTable = _consolidatedData.Copy
            Else
                _consolidatedRecordCount = 0
            End If
            ValidationErrors.Clear()

            FormatOutput()
            GenerateHeader()
            GenerateTrailer()

            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()

        Catch mex As MagicException
            Throw New MagicException(mex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on Consolidatiing records : " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal objMaster As Object) As Boolean  ' SaveFile

        'Dim delimiter As String
        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Try
            ' EPS format is Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateFixed
            If objMaster.GetType Is GetType(MasterTemplateFixed) Then
                objMasterTemplate = CType(objMaster, MasterTemplateFixed)
            Else
                Throw New Exception("Invalid Master Template")
            End If

            ' Get the Delimiter, Enclosure character etc. No idea from where?
            If Header <> String.Empty Then
                text &= Header & vbNewLine
            End If

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                line = ""
                For Each _column As String In TblBankFields.Keys
                    If TblBankFields(_column).Detail.ToUpper <> "NO" Then
                        If _column.ToUpper = "Amount".ToUpper Then
                            If _previewRecord(_column).ToString.StartsWith("-") Then
                                _previewRecord(_column) = System.Math.Abs(Convert.ToDecimal(_previewRecord(_column).ToString())).ToString("##########")

                                _previewRecord(_column) = "-" & _previewRecord(_column).ToString().PadLeft(TblBankFields(_column).DataLength.Value, "0")
                            Else
                                _previewRecord(_column) = System.Math.Abs(Convert.ToDecimal(_previewRecord(_column).ToString())).ToString("##########")

                                _previewRecord(_column) = _previewRecord(_column).ToString().PadLeft(TblBankFields(_column).DataLength.Value, "0")
                            End If
                            
                        End If
                        If TblBankFields(_column).DataType = "Number" Or _
                            TblBankFields(_column).DataType = "Currency" Then
                            line &= _previewRecord(_column).ToString
                        Else
                            line &= _previewRecord(_column).ToString.PadLeft(TblBankFields(_column).DataLength.Value, " ")
                        End If

                    End If
                Next
                If objMasterTemplate.NewLine Then
                    If rowNo < _PreviewTable.Rows.Count - 1 Then
                        text = text & line & vbNewLine
                    Else
                        text = text & line
                    End If
                Else
                    If rowNo < _PreviewTable.Rows.Count - 1 Then
                        text = text & line & objMasterTemplate.DelimiterCharacter
                    Else
                        text = text & line
                    End If
                End If
                rowNo += 1
            Next
            If Trailer <> String.Empty Then
                text &= vbNewLine & Trailer
            End If

            'Encrypt using GnuPG Encryption Standard
            If objMasterTemplate.EncryptCustomerOutputFile Then
                text = HelperModule.EncrypteClientFileUsingGnuPGWrapper(text, GnuPG_Home)
            End If

            Return SaveTextToFile(text, _outputFile)

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

#End Region

End Class
