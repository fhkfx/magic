'************************************************CHANGE REQUESTS**********************************'
'SRS No                     Date Modified   Description
'-------------------------------------------------------------------------------------------------'
'SRS/BTMU/2010-0025         01/Apr/2010     Add Email Validations
'                                           Email Format : local-part@domain-part
'                                           1.  Local-part can use the letters a-z and A-Z, 
'                                               digits 0-9, Hyphen(-), underscore(_) and dot(.)
'                                           2.  Domain-part can use the letters a-z and A-z,
'                                               digits 0-9, Hyphen(-) and dot(.)
'                                           3.  Dot(.) cannot be first or last character in  
'                                               local-partor domain-part
'                                           4.  Dot(.) cannot appear two or more times 
'                                               consecutively
'                                           5.  Allow multiple email ids separated by semicolon(;)
'                                           6.  If Comma is used, replace it with semicolon
'                                           7.  Remove the space between email ids
'*************************************************************************************************'
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions


''' <summary>
''' Encapsulates the iFTS2 MultiLine output Format:
''' Header Record
''' Payment Record WHTax Rec#1 WHTAX Rec#2 WHTax Rec#3 Invoice Rec#1 Invoice Rec#2 Invoice Rec#3
''' </summary>
Public Class iFTS2SingleLineFormat
    Inherits BaseFileFormat

#Region "Local Constants"

    Private Shared _amountFieldName As String = "Gross Amount"

    'Only P-Records can be filtered/grouped/referenced
    Private Shared _validgroupbyFields() As String = {"Record Type-P", "Beneficiary Name", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type" _
                                                        , "Gross Amount", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary", "WHT Delivery Method" _
                                                        , "Consolidate Field"}

    Private Shared _editableFields() As String = { _
                                                    "Record Type-P", "Beneficiary Name", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type" _
                                                    , "Gross Amount", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary", "WHT Delivery Method" _
 _
                                                    , "W1-Record Type-W", "W1-W/H Tax No.", "W1-Name", "W1-Address", "W1-Tax ID/ID Card No.", "W1-No. (in the form)", "W1-Tax Form" _
                                                    , "W1-The Payer of Income", "W1-The Payer of Income Description ", "W1-Type of Income (1)", "W1-Type of Income (1) Description" _
                                                    , "W1-Date (1)", "W1-Amount Paid (1)", "W1-Tax Rate (1)", "W1-WHT Amount (1)", "W1-Type of Income (2)", "W1-Type of Income (2) Description" _
                                                    , "W1-Date (2)", "W1-Amount Paid (2)", "W1-Tax Rate (2)", "W1-WHT Amount (2)", "W1-Type of Income (3)", "W1-Type of Income (3) Description" _
                                                    , "W1-Date (3)", "W1-Amount Paid (3)", "W1-Tax Rate (3)", "W1-WHT Amount (3)" _
                                                    , "W2-Record Type-W", "W2-W/H Tax No.", "W2-Name", "W2-Address", "W2-Tax ID/ID Card No.", "W2-No. (in the form)", "W2-Tax Form" _
                                                    , "W2-The Payer of Income", "W2-The Payer of Income Description ", "W2-Type of Income (1)", "W2-Type of Income (1) Description" _
                                                    , "W2-Date (1)", "W2-Amount Paid (1)", "W2-Tax Rate (1)", "W2-WHT Amount (1)", "W2-Type of Income (2)", "W2-Type of Income (2) Description" _
                                                    , "W2-Date (2)", "W2-Amount Paid (2)", "W2-Tax Rate (2)", "W2-WHT Amount (2)", "W2-Type of Income (3)", "W2-Type of Income (3) Description" _
                                                    , "W2-Date (3)", "W2-Amount Paid (3)", "W2-Tax Rate (3)", "W2-WHT Amount (3)" _
                                                    , "W3-Record Type-W", "W3-W/H Tax No.", "W3-Name", "W3-Address", "W3-Tax ID/ID Card No.", "W3-No. (in the form)", "W3-Tax Form" _
                                                    , "W3-The Payer of Income", "W3-The Payer of Income Description ", "W3-Type of Income (1)", "W3-Type of Income (1) Description" _
                                                    , "W3-Date (1)", "W3-Amount Paid (1)", "W3-Tax Rate (1)", "W3-WHT Amount (1)", "W3-Type of Income (2)", "W3-Type of Income (2) Description" _
                                                    , "W3-Date (2)", "W3-Amount Paid (2)", "W3-Tax Rate (2)", "W3-WHT Amount (2)", "W3-Type of Income (3)", "W3-Type of Income (3) Description" _
                                                    , "W3-Date (3)", "W3-Amount Paid (3)", "W3-Tax Rate (3)", "W3-WHT Amount (3)" _
 _
                                                    , "I1-Record Type-I", "I1-Invoice No.", "I1-Invoice Date", "I1-Description", "I1-Invoice Amount", "I1-VAT", "I1-Net Amount" _
                                                    , "I2-Record Type-I", "I2-Invoice No.", "I2-Invoice Date", "I2-Description", "I2-Invoice Amount", "I2-VAT", "I2-Net Amount" _
                                                    , "I3-Record Type-I", "I3-Invoice No.", "I3-Invoice Date", "I3-Description", "I3-Invoice Amount", "I3-VAT", "I3-Net Amount" _
                                                     }

    Private Shared _autoTrimmableFields() As String = { _
                                                    "Beneficiary Name", "Fax No.", "E-mail Address", "Attention", "Message to Beneficiary" _
                                                    , "W1-W/H Tax No.", "W2-W/H Tax No.", "W3-W/H Tax No.", "W1-Name", "W2-Name", "W3-Name", "W1-Address", "W2-Address", "W3-Address" _
                                                    , "W1-No. (in the form)", "W2-No. (in the form)", "W3-No. (in the form)", "W1-The Payer of Income Description" _
                                                    , "W2-The Payer of Income Description", "W3-The Payer of Income Description", "W1-Type of Income (1) Description" _
                                                    , "W2-Type of Income (1) Description", "W3-Type of Income (1) Description", "W1-Type of Income (2) Description" _
                                                    , "W2-Type of Income (2) Description", "W3-Type of Income (2) Description", "W1-Type of Income (3) Description" _
                                                    , "W2-Type of Income (3) Description", "W3-Type of Income (3) Description", "I1-Description", "I2-Description" _
                                                    , "I3-Description"}

    'Fields: Tax Record
    Private Shared _fieldsTaxRecord1() As String = {"W1-Record Type-W", "W1-W/H Tax No.", "W1-Name", "W1-Address", "W1-Tax ID/ID Card No.", "W1-No. (in the form)", "W1-Tax Form" _
                                                    , "W1-The Payer of Income", "W1-The Payer of Income Description ", "W1-Type of Income (1)", "W1-Type of Income (1) Description" _
                                                    , "W1-Date (1)", "W1-Amount Paid (1)", "W1-Tax Rate (1)", "W1-WHT Amount (1)", "W1-Type of Income (2)", "W1-Type of Income (2) Description" _
                                                    , "W1-Date (2)", "W1-Amount Paid (2)", "W1-Tax Rate (2)", "W1-WHT Amount (2)", "W1-Type of Income (3)", "W1-Type of Income (3) Description" _
                                                    , "W1-Date (3)", "W1-Amount Paid (3)", "W1-Tax Rate (3)", "W1-WHT Amount (3)"}

    Private Shared _fieldsTaxRecord2() As String = {"W2-Record Type-W", "W2-W/H Tax No.", "W2-Name", "W2-Address", "W2-Tax ID/ID Card No.", "W2-No. (in the form)", "W2-Tax Form" _
                                                   , "W2-The Payer of Income", "W2-The Payer of Income Description ", "W2-Type of Income (1)", "W2-Type of Income (1) Description" _
                                                   , "W2-Date (1)", "W2-Amount Paid (1)", "W2-Tax Rate (1)", "W2-WHT Amount (1)", "W2-Type of Income (2)", "W2-Type of Income (2) Description" _
                                                   , "W2-Date (2)", "W2-Amount Paid (2)", "W2-Tax Rate (2)", "W2-WHT Amount (2)", "W2-Type of Income (3)", "W2-Type of Income (3) Description" _
                                                   , "W2-Date (3)", "W2-Amount Paid (3)", "W2-Tax Rate (3)", "W2-WHT Amount (3)"}

    Private Shared _fieldsTaxRecord3() As String = {"W3-Record Type-W", "W3-W/H Tax No.", "W3-Name", "W3-Address", "W3-Tax ID/ID Card No.", "W3-No. (in the form)", "W3-Tax Form" _
                                                   , "W3-The Payer of Income", "W3-The Payer of Income Description ", "W3-Type of Income (1)", "W3-Type of Income (1) Description" _
                                                   , "W3-Date (1)", "W3-Amount Paid (1)", "W3-Tax Rate (1)", "W3-WHT Amount (1)", "W3-Type of Income (2)", "W3-Type of Income (2) Description" _
                                                   , "W3-Date (2)", "W3-Amount Paid (2)", "W3-Tax Rate (2)", "W3-WHT Amount (2)", "W3-Type of Income (3)", "W3-Type of Income (3) Description" _
                                                   , "W3-Date (3)", "W3-Amount Paid (3)", "W3-Tax Rate (3)", "W3-WHT Amount (3)"}

    'Fields: Invoice Record
    Private Shared _fieldsInvoiceRecord1() As String = {"I1-Record Type-I", "I1-Invoice No.", "I1-Invoice Date", "I1-Description", "I1-Invoice Amount", "I1-VAT", "I1-Net Amount"}

    Private Shared _fieldsInvoiceRecord2() As String = {"I2-Record Type-I", "I2-Invoice No.", "I2-Invoice Date", "I2-Description", "I2-Invoice Amount", "I2-VAT", "I2-Net Amount"}

    Private Shared _fieldsInvoiceRecord3() As String = {"I3-Record Type-I", "I3-Invoice No.", "I3-Invoice Date", "I3-Description", "I3-Invoice Amount", "I3-VAT", "I3-Net Amount"}


#End Region

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iFTS-2 SingleLine"
        End Get
    End Property

#End Region

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Public Shared Function OutputFormatString() As String
        Return "iFTS-2 SingleLine"
    End Function


#Region "Overriden Methods"

    Protected Overrides Sub ApplyTranslation()

        Dim recordExists As Boolean = False

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each translatedBankField As TranslatorSetting In _DataModel.TranslatorSettings

                    'Is BankField a Tax Field? and Is there a Tax Record? Then Apply Translation
                    If Array.IndexOf(_fieldsTaxRecord1, translatedBankField.BankFieldName) <> -1 Then
                        TaxRecordExists(_previewRecord, recordExists, 1)
                        If Not recordExists Then Continue For


                        If translatedBankField.BankFieldName.Equals("W1-Type of Income (2)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 1, 2) Then Continue For
                        End If


                        If translatedBankField.BankFieldName.Equals("W1-Type of Income (3)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 1, 3) Then Continue For
                        End If

                    ElseIf Array.IndexOf(_fieldsTaxRecord2, translatedBankField.BankFieldName) <> -1 Then

                        TaxRecordExists(_previewRecord, recordExists, 2)
                        If Not recordExists Then Continue For

                        If translatedBankField.BankFieldName.Equals("W2-Type of Income (2)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 2, 2) Then Continue For
                        End If


                        If translatedBankField.BankFieldName.Equals("W2-Type of Income (3)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 2, 3) Then Continue For
                        End If


                    ElseIf Array.IndexOf(_fieldsTaxRecord3, translatedBankField.BankFieldName) <> -1 Then
                        TaxRecordExists(_previewRecord, recordExists, 3)
                        If Not recordExists Then Continue For

                        If translatedBankField.BankFieldName.Equals("W3-Type of Income (2)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 3, 2) Then Continue For
                        End If


                        If translatedBankField.BankFieldName.Equals("W3-Type of Income (3)", StringComparison.InvariantCultureIgnoreCase) Then
                            If Not DoTypeofIncomeFieldsHaveValue(_previewRecord, 3, 3) Then Continue For
                        End If


                        'Is BankField a Tax Field? and Is there a Tax Record? Then Apply Translation
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord1, translatedBankField.BankFieldName) <> -1 Then
                        InvoiceRecordExists(_previewRecord, recordExists, 1)
                        If Not recordExists Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord2, translatedBankField.BankFieldName) <> -1 Then
                        InvoiceRecordExists(_previewRecord, recordExists, 2)
                        If Not recordExists Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord3, translatedBankField.BankFieldName) <> -1 Then
                        InvoiceRecordExists(_previewRecord, recordExists, 3)
                        If Not recordExists Then Continue For
                    End If

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If translatedBankField.CustomerValue.Equals(_previewRecord(translatedBankField.BankFieldName).ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Translation Settings(ApplyTranslation) : {0}", ex.Message))
        End Try
    End Sub



    Protected Overrides Sub GenerateHeader()

        If _PreviewTable.Rows.Count > 0 Then
            _Header = _PreviewTable.Rows(0)("File Format Type").ToString()
        Else
            _Header = ""
        End If

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim strField As String = String.Empty
        Dim strFieldAmtPaid As String = String.Empty
        Dim strFieldTxRate As String = String.Empty
        Dim strFieldWHTAmt As String = String.Empty

        For Each row As DataRow In _PreviewTable.Rows

            

            'Validate Fields: Payment Record '''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row("Record Type-P").ToString() = "P" Then

                If Not IsNothingOrEmptyString(row("Bank Charge")) Then
                    row("Bank Charge") = row("Bank Charge").ToString.ToUpper
                End If

                If Not IsNothingOrEmptyString(row("Service Type")) Then
                    row("Service Type") = row("Service Type").ToString.PadLeft(TblBankFields("Service Type").DataLength.Value, "0")
                End If

                If Not IsNothingOrEmptyString(row("WHT Delivery Method")) Then
                    row("WHT Delivery Method") = row("WHT Delivery Method").ToString.ToUpper.Replace("""", "")
                End If

                'new spec. requirement
                If IsNothingOrEmptyString(row("WHT Delivery Method")) Then
                    row("WHT Delivery Method") = "C"
                Else
                    row("WHT Delivery Method") = row("WHT Delivery Method").ToString.ToUpper.Replace("""", "")
                End If

                If row("Account No.") <> "" Then
                    row("Account No.") = row("Account No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "").Trim()
                End If

                If row("Gross Amount") <> "" AndAlso Decimal.TryParse(row("Gross Amount").ToString(), Nothing) Then
                    row("Gross Amount") = row("Gross Amount").ToString.Replace(_DataModel.ThousandSeparator, "")
                End If

                If row("Fax No.") <> "" Then
                    row("Fax No.") = row("Fax No.").ToString().Replace("(", "").Replace(")", "").Replace("-", "").Trim()
                End If

                'SRS/BTMU/2010-0025
                If row("E-mail Address") <> "" Then
                    row("E-mail Address") = row("E-mail Address").ToString.Replace(",", ";")
                    Dim mailids As New System.Text.StringBuilder
                    For Each mailid As String In row("E-mail Address").ToString().Split(";")
                        mailids.Append(mailid.Trim)
                        mailids.Append(";")
                    Next
                    row("E-mail Address") = mailids.ToString.Substring(0, mailids.ToString.Length - 1) ' to remove the ; at the end
                End If
            End If

            'Validate Fields: Tax Record ''''''''''''''''''''''''''''''''''''''''''''''''''
            For _cnt As Int32 = 1 To 3
                If Not IsNothingOrEmptyString(row(String.Format("W{0}-Record Type-W", _cnt))) Then
                    row(String.Format("W{0}-Record Type-W", _cnt)) = row(String.Format("W{0}-Record Type-W", _cnt)).ToString.ToUpper
                End If
               
            Next

            For _rcnt As Int32 = 1 To 3
                For _cnt As Int32 = 1 To 3
                    strFieldAmtPaid = String.Format("W{0}-Amount Paid ({1})", _rcnt, _cnt)
                    strFieldTxRate = String.Format("W{0}-Tax Rate ({1})", _rcnt, _cnt)
                    strFieldWHTAmt = String.Format("W{0}-WHT Amount ({1})", _rcnt, _cnt)

                    If row(strFieldAmtPaid).ToString() <> "" Then
                        row(strFieldAmtPaid) = row(strFieldAmtPaid).ToString.Replace(_DataModel.ThousandSeparator, "")
                    End If

                    If row(strFieldTxRate) <> "" AndAlso Decimal.TryParse(row(strFieldAmtPaid).ToString(), Nothing) AndAlso Decimal.TryParse(row(strFieldTxRate).ToString(), Nothing) Then
                        row(strFieldWHTAmt) = ((Decimal.Parse(row(strFieldAmtPaid)) * Decimal.Parse(row(strFieldTxRate))) / 100).ToString("#.00")
                    End If
                Next
            Next

        Next

    End Sub

    'Validates Mandatory Fields.
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0
        Dim tRecordExists, iRecordExists As Boolean
        Dim _setcnt As Int32 = 0

        Try

            'File Format Type is Required
            If IsNothingOrEmptyString(_PreviewTable.Rows(0)("File Format Type").ToString()) Then
                AddValidationError("File Format Type", _PreviewTable.Columns("File Format Type").Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09030160"), "Header", "", "File Format Type") _
                                   , RowNumber + 1, True)
            End If

            For Each row As DataRow In _PreviewTable.Rows



                'Validate Fields: Payment Record  
                For Each _column As String In New String() {"Record Type-P", "Account No.", "Bank Code", "Branch Code", "Bank Charge", "Service Type", "Gross Amount"}

                    If IsNothingOrEmptyString(row(_column).ToString()) Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                            , RowNumber + 1, True)
                    End If

                Next

                'Validate Fields: Tax Record 

                For Each _column As String In New String() {"W1-Record Type-W", "W1-W/H Tax No.", "W1-Name", "W1-Address", "W1-Tax ID/ID Card No.", "W1-Tax Form", "W1-Type of Income (1)", "W1-Amount Paid (1)", "W1-WHT Amount (1)", "W1-Amount Paid (2)", "W1-WHT Amount (2)", "W1-Amount Paid (3)", "W1-WHT Amount (3)", "W2-Record Type-W", "W2-W/H Tax No.", "W2-Name", "W2-Address", "W2-Tax ID/ID Card No.", "W2-Tax Form", "W2-Type of Income (1)", "W2-Amount Paid (1)", "W2-WHT Amount (1)", "W2-Amount Paid (2)", "W2-WHT Amount (2)", "W2-Amount Paid (3)", "W2-WHT Amount (3)", "W3-Record Type-W", "W3-W/H Tax No.", "W3-Name", "W3-Address", "W3-Tax ID/ID Card No.", "W3-Tax Form", "W3-Type of Income (1)", "W3-Amount Paid (1)", "W3-WHT Amount (1)", "W3-Amount Paid (2)", "W3-WHT Amount (2)", "W3-Amount Paid (3)", "W3-WHT Amount (3)"}

                    If _column.StartsWith("W1") Then _setcnt = 1
                    If _column.StartsWith("W2") Then _setcnt = 2
                    If _column.StartsWith("W3") Then _setcnt = 3

                    TaxRecordExists(row, tRecordExists, _setcnt)
                    If Not tRecordExists Then Continue For

                    If IsNothingOrEmptyString(row(_column).ToString()) Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                            , RowNumber + 1, True)
                    End If

                Next


                'Validate Fields: Invoice Record

                For Each _column As String In New String() {"I1-Record Type-I", "I1-Invoice No.", "I1-Invoice Date", "I1-Net Amount", "I2-Record Type-I", "I2-Invoice No.", "I2-Invoice Date", "I2-Net Amount", "I3-Record Type-I", "I3-Invoice No.", "I3-Invoice Date", "I3-Net Amount"}
                    If _column.StartsWith("I1") Then _setcnt = 1
                    If _column.StartsWith("I2") Then _setcnt = 2
                    If _column.StartsWith("I3") Then _setcnt = 3

                    InvoiceRecordExists(row, iRecordExists, _setcnt)
                    If Not iRecordExists Then Continue For

                    If IsNothingOrEmptyString(row(_column).ToString()) Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                            , RowNumber + 1, True)
                    End If

                Next



                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type, Conditional Mandatory and Custom Validations 
    '(other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim tRecordExists, iRecordExists As Boolean
        Dim found As Boolean = False
        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim strField As String = String.Empty
        Dim _ValueDate As Date
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
        Dim regAllow2 As New Regex("[^0-9]", RegexOptions.Compiled)
        'SRS/BTMU/2010-0025
        'Dim regEmail As New System.Text.RegularExpressions.Regex("^[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$", System.Text.RegularExpressions.RegexOptions.Compiled Or System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim regEmail As New System.Text.RegularExpressions.Regex("^[a-z0-9_-]+(\.[a-z0-9_-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$", System.Text.RegularExpressions.RegexOptions.Compiled Or System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                '#1. File Format Type' is incorrect. It must be 'F2'.
                If RowNumber = 0 Then
                    If (_previewRecord("File Format Type") & "").ToString().ToUpper() <> "F2" Then
                        AddValidationError("File Format Type", _PreviewTable.Columns("File Format Type").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Header", "", "File Format Type", "'F2'") _
                                            , 0, True)
                    End If
                End If

                'Validate Fields: Payment Record
                '#3. 'Record Type-P' is incorrect. It must be 'P'.
                If _previewRecord("Record Type-P").ToString() <> "" AndAlso _previewRecord("Record Type-P").ToString().ToUpper() <> "P" Then
                    AddValidationError("Record Type-P", _PreviewTable.Columns("Record Type-P").Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-P", "'P'") _
                                        , RowNumber + 1, True)
                End If

                '#4. Account No. Must be numeric
                If _previewRecord("Account No.") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Account No.").ToString()) Then
                        'If Not Decimal.TryParse(_previewRecord("Account No.").ToString(), Nothing) Then
                        AddValidationError("Account No.", _PreviewTable.Columns("Account No.").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Account No.") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#5. Bank Code Must be numeric
                If _previewRecord("Bank Code") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Bank Code").ToString()) Then
                        'If Not Decimal.TryParse(_previewRecord("Bank Code").ToString(), Nothing) Then
                        AddValidationError("Bank Code", _PreviewTable.Columns("Bank Code").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Bank Code") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#6. Branch Code Must be numeric
                If _previewRecord("Branch Code") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Branch Code").ToString()) Then
                        'If Not Decimal.TryParse(_previewRecord("Branch Code").ToString(), Nothing) Then
                        AddValidationError("Branch Code", _PreviewTable.Columns("Branch Code").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Branch Code") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#7. Bank Charge - either "BEN" or "OUR"
                If _previewRecord("Bank Charge") <> "" Then
                    If Not (_previewRecord("Bank Charge").ToString().ToUpper() = "BEN" _
                        Or _previewRecord("Bank Charge").ToString().ToUpper() = "OUR") Then
                        AddValidationError("Bank Charge", _PreviewTable.Columns("Bank Charge").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Bank Charge") _
                                            , RowNumber + 1, True)
                    End If
                End If

                '#8. Service Type - must be one of {"01", "02", "03", "04", "05", "06", "07", "59"}
                If _previewRecord("Service Type") <> "" Then
                    found = False
                    For Each _validSvcType As String In New String() {"01", "02", "03", "04", "05", "06", "07", "59"}
                        If _previewRecord("Service Type").ToString() = _validSvcType Then
                            found = True
                            Exit For
                        End If
                    Next
                    If Not found Then
                        AddValidationError("Service Type", _PreviewTable.Columns("Service Type").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Service Type") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#9. Gross Amount
                If _previewRecord("Gross Amount") <> "" Then
                    If Decimal.TryParse(_previewRecord("Gross Amount"), dAmount) Then
                        If IsConsolidated Then
                            'If IsNegativePaymentOption Then
                            '    If dAmount >= 0 Then
                            '        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1, True)
                            '    End If
                            'Else
                            '    If dAmount <= 0 Then
                            '        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, "Gross Amount"), RowNumber + 1, True)
                            '    End If
                            'End If
                        Else
                            'If dAmount <= 0 Then 'Should not be negative or 0
                            '    AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                            '                        , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Gross Amount") _
                            '                        , RowNumber + 1, True)
                            'End If
                        End If


                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Gross Amount") _
                                                    , RowNumber + 1, True)
                            End If
                            'Integral part of the amount should not exceed 13  
                            If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Gross Amount", 13) _
                                                    , RowNumber + 1, True)
                            End If
                        Else
                            'Integral part of the amount should not exceed 13  
                            If dAmount.ToString().Length > 13 Then
                                AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, "Gross Amount", 13) _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Else
                        AddValidationError("Gross Amount", _PreviewTable.Columns("Gross Amount").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Gross Amount") _
                                            , RowNumber + 1, True)
                    End If
                End If

                '#10. Fax No. - Must be numeric
                If _previewRecord("Fax No.") <> "" Then
                    If regAllow2.IsMatch(_previewRecord("Fax No.").ToString()) Then 'numeric values only
                        'If Not Decimal.TryParse(_previewRecord("Fax No.").ToString(), Nothing) Then
                        AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Fax No.") _
                                            , RowNumber + 1, True)
                    End If
                End If

                '#11. - WHT Delivery Method - must be one of ("C", "R", "M")
                If _previewRecord("WHT Delivery Method") <> "" Then
                    If _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "C" _
                       AndAlso _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "R" _
                       AndAlso _previewRecord("WHT Delivery Method").ToString().ToUpper() <> "M" Then
                        AddValidationError("WHT Delivery Method", _PreviewTable.Columns("WHT Delivery Method").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "WHT Delivery Method") _
                                            , RowNumber + 1, True)
                    End If
                End If

                '#11.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                For Each _field As String In New String() {"Beneficiary Name", "E-mail Address", "Attention", "Message to Beneficiary"}
                    If _previewRecord(_field) <> "" Then
                        If Not IsValidiFTS2Text(_previewRecord(_field).ToString()) Then
                            AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                                , RowNumber + 1, True)
                        End If
                    End If
                Next

                '#11.a. Multiple Email addresses separated by comma (;) are allowed
                If _previewRecord("E-mail Address") <> "" Then

                    For Each mail As String In _previewRecord("E-mail Address").ToString().Split(";")
                        If Not regEmail.IsMatch(mail) Then
                            AddValidationError("E-mail Address", _PreviewTable.Columns("E-mail Address").Ordinal + 1 _
                                                 , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "E-mail Address") _
                                                 , RowNumber + 1, True)
                            Exit For
                        End If
                    Next

                End If

                If IsNothingOrEmptyString(_previewRecord("Attention")) Then _previewRecord("Attention") = "Finance and Accounting Manager"

                'Validate Fields: Tax Record
                For _tRecCount As Int32 = 1 To 3
                    TaxRecordExists(_previewRecord, tRecordExists, _tRecCount)
                    If Not tRecordExists Then Continue For

                    '#12. 'Record Type-W' is incorrect. It must be 'W'.
                    strField = String.Format("W{0}-Record Type-W", _tRecCount)
                    If _previewRecord(strField).ToString() <> "" AndAlso _previewRecord(strField).ToString().ToUpper() <> "W" Then
                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, strField, "'W'") _
                                            , RowNumber + 1, True)
                    End If

                    '#13. Tax ID/ID Card No. - Must be numeric and 10 or 13 digits in length
                    strField = String.Format("W{0}-Tax ID/ID Card No.", _tRecCount)
                    If _previewRecord(strField) <> "" Then
                        If regAllow2.IsMatch(_previewRecord(strField).ToString()) Then 'numeric values only
                            'If Not Decimal.TryParse(_previewRecord(strField).ToString(), Nothing) Then
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                                , RowNumber + 1, True)
                        End If

                        If _previewRecord(strField).ToString().Length <> 10 AndAlso _previewRecord(strField).ToString().Length <> 13 Then
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09060010"), "Record", RowNumber + 1, strField) _
                                                , RowNumber + 1, True)
                        End If
                    End If

                    '#14. No. (in the form) - Must be numeric
                    strField = String.Format("W{0}-No. (in the form)", _tRecCount)
                    If _previewRecord(strField) <> "" Then
                        If regAllow2.IsMatch(_previewRecord(strField).ToString()) Then 'numeric values only
                            'If Not Decimal.TryParse(_previewRecord(strField).ToString(), Nothing) Then
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#15. Tax Form - Must be numeric and can have value between 1 and 7
                    strField = String.Format("W{0}-Tax Form", _tRecCount)
                    If _previewRecord(strField) <> "" Then
                        If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then
                            If dAmount < 1 OrElse dAmount > 7 Then
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                   , RowNumber + 1, True)
                            End If
                        Else
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#16. The Payer of Income - Must be numeric and can have value between 1 and 4
                    strField = String.Format("W{0}-The Payer of Income", _tRecCount)
                    If _previewRecord(strField) <> "" Then
                        If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then
                            If dAmount < 1 OrElse dAmount > 4 Then
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                   , RowNumber + 1, True)
                            End If

                            If dAmount = 4 Then
                                Dim _col As String = String.Format("W{0}-The Payer of Income Description", _tRecCount)
                                If _previewRecord(_col).ToString.Trim = "" Then
                                    AddValidationError(_col, _PreviewTable.Columns(_col).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, _col, "'" & strField & "' is '4'") _
                                                      , RowNumber + 1, True)
                                End If
                            Else
                                Dim _col As String = String.Format("W{0}-The Payer of Income Description", _tRecCount)
                                If Not IsNothingOrEmptyString(_previewRecord(_col)) Then
                                    AddValidationError(_col, _PreviewTable.Columns(_col).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09050011"), "Record", RowNumber + 1, _col, "'" & strField & "' is not '4'") _
                                                      , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    For _cnt As Int32 = 1 To 3
                        '#17. Type of Income - Must be numeric and can have one of:(1, 2, 3, 41, 42, 43, 44, 45, 46, 47, 51, 52, 61, or 62)
                        strField = String.Format("W{0}-Type of Income ({1})", _tRecCount, _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then

                                '###################################
                                If _cnt = 1 Then
                                    'Type of Income (1), (2), (3) cannot be duplicated
                                    'compare with Type of Income (1) with (2)
                                    If _previewRecord(String.Format("W{0}-Type of Income (2)", _tRecCount)) <> "" Then
                                        If Decimal.TryParse(_previewRecord(String.Format("W{0}-Type of Income (2)", _tRecCount)), Nothing) Then

                                            'compare (1) with (2)
                                            If dAmount = Decimal.Parse(_previewRecord(String.Format("W{0}-Type of Income (2)", _tRecCount))) Then
                                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                      , RowNumber + 1, True)
                                            End If
                                            If Decimal.TryParse(_previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount)), Nothing) Then
                                                'compare (1) with (3)
                                                If dAmount = Decimal.Parse(_previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount))) Then
                                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                          , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                          , RowNumber + 1, True)
                                                End If
                                                'compare (2) with (3)
                                                If Decimal.Parse(_previewRecord(String.Format("W{0}-Type of Income (2)", _tRecCount))) = Decimal.Parse(_previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount))) Then
                                                    AddValidationError(String.Format("W{0}-Type of Income (2)", _tRecCount), _PreviewTable.Columns(String.Format("W{0}-Type of Income (2)", _tRecCount)).Ordinal + 1 _
                                                          , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, String.Format("W{0}-Type of Income (2)", _tRecCount)) _
                                                          , RowNumber + 1, True)
                                                End If
                                            End If

                                        End If
                                    End If
                                    If _previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount)) <> "" Then
                                        If Decimal.TryParse(_previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount)), Nothing) Then
                                            'compare (1) with (3)
                                            If dAmount = Decimal.Parse(_previewRecord(String.Format("W{0}-Type of Income (3)", _tRecCount))) Then
                                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                      , String.Format(MsgReader.GetString("E09020202"), "Record", RowNumber + 1, strField) _
                                                      , RowNumber + 1, True)
                                            End If
                                        End If
                                    End If


                                End If
                                '###################################
                                found = False
                                For Each _value As Int32 In New Int32() {1, 2, 3, 41, 42, 43, 44, 45, 46, 47, 51, 52, 61, 62}
                                    If dAmount = _value Then
                                        found = True
                                        Exit For
                                    End If
                                Next
                                If Not found Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                       , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                       , RowNumber + 1, True)
                                End If
                                Select Case _previewRecord(strField).ToString()
                                    Case "51", "52", "61", "62"
                                        'If _previewRecord(String.Format("W{0}-Tax Form", _tRecCount)) = "4" OrElse _previewRecord(String.Format("W{0}-Tax Form", _tRecCount)) = "7" Then
                                        If IsNothingOrEmptyString(_previewRecord(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt))) Then
                                            AddValidationError(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt), _PreviewTable.Columns(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt)).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt)) _
                                                   , RowNumber + 1, True)
                                        End If
                                        'End If
                                    Case Else
                                        If _previewRecord(String.Format("W{0}-Tax Form", _tRecCount)) = "4" OrElse _previewRecord(String.Format("W{0}-Tax Form", _tRecCount)) = "7" Then
                                            If IsNothingOrEmptyString(_previewRecord(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt))) Then
                                                AddValidationError(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt), _PreviewTable.Columns(String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt)).Ordinal + 1 _
                                                       , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, String.Format("W{0}-Type of Income ({1}) Description", _tRecCount, _cnt)) _
                                                       , RowNumber + 1, True)
                                            End If
                                        End If
                                End Select
                                '#################################
                                'strField = String.Format("W{0}-Amount Paid ({1})", _tRecCount, _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                      , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, strField) _
                                '                      , RowNumber + 1, True)
                                'End If

                                ''#20.Tax Rate 
                                ''20.1 If 'WHT Amount (1)' is blank Then 'Tax Rate (1)' is mandatory.
                                'strField = String.Format("W{0}-WHT Amount ({1})", _tRecCount, _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    strField = String.Format("W{0}-Tax Rate ({1})", _tRecCount, _cnt)
                                '    If _previewRecord(strField).ToString.Trim = "" Then
                                '        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                          , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, strField, String.Format("W{0}-WHT Amount ({1}) is blank", _tRecCount, _cnt)) _
                                '                          , RowNumber + 1, True)
                                '    End If
                                'End If

                                ''#21.WHT Amount
                                ''21.1 If 'Tax Rate (1)' is blank Then 'WHT Amount (1)' is mandatory.
                                'strField = String.Format("W{0}-Tax Rate ({1})", _tRecCount, _cnt)
                                'If _previewRecord(strField).ToString.Trim = "" Then
                                '    strField = String.Format("W{0}-WHT Amount ({1})", _tRecCount, _cnt)
                                '    If _previewRecord(strField).ToString.Trim = "" Then
                                '        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                          , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, strField, String.Format("W{0}-Tax Rate ({1}) is blank", _tRecCount, _cnt)) _
                                '                          , RowNumber + 1, True)
                                '    End If
                                'End If
                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                   , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, strField) _
                                                   , RowNumber + 1, True)
                            End If ' if decimal.tryparse()....end if
                        End If 'if _previewRecord()... end if


                        '20.2 Tax Rate must be numeric and within the range 0.01 and 100.00.
                        strField = String.Format("W{0}-Tax Rate ({1})", _tRecCount, _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField), dAmount) Then
                                If dAmount < 0.01 OrElse dAmount > 100.0 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                  , String.Format(MsgReader.GetString("E09050050"), "Record", RowNumber + 1, strField, "0.01", "100.00") _
                                                  , RowNumber + 1, True)
                                End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed  3  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 3 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 3) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else ' If no Decimal Separator
                                    'Integral part of the amount should not exceed  3  
                                    If dAmount.ToString().Length > 3 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 3) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If

                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                              , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                              , RowNumber + 1, True)
                            End If
                        End If



                        '20.1 WHT Amount must be numeric and cannot be <= 0 
                        strField = String.Format("W{0}-WHT Amount ({1})", _tRecCount, _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField), dAmount) Then

                                'Must be <= Amount Paid
                                If Decimal.TryParse(_previewRecord(String.Format("W{0}-Amount Paid ({1})", _tRecCount, _cnt)), Nothing) Then
                                    If dAmount > Decimal.Parse(_previewRecord(String.Format("W{0}-Amount Paid ({1})", _tRecCount, _cnt))) Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09020201"), "Record", RowNumber + 1, strField) _
                                        , RowNumber + 1, True)
                                    End If
                                End If

                                'If dAmount <= 0 Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                  , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, strField) _
                                '                  , RowNumber + 1, True)
                                'End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed  13  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else ' if no decimal separator
                                    'Integral part of the amount should not exceed  13  
                                    If dAmount.ToString().Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If
                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                              , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                              , RowNumber + 1, True)
                            End If
                        End If
                        '#18. Date - must be valid date and can be either in yymmdd or ymmdd format
                        strField = String.Format("W{0}-Date ({1})", _tRecCount, _cnt)
                        If _previewRecord(strField) <> "" Then
                            If (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", TblBankFields(strField).DateFormat, _ValueDate, True)) _
                              AndAlso (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", "yMMdd", _ValueDate, True)) Then

                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, strField) _
                                                       , RowNumber + 1, True)
                            End If
                        End If

                        '#19. Amount Paid - Must be numeric and cannot be <=0 
                        strField = String.Format("W{0}-Amount Paid ({1})", _tRecCount, _cnt)
                        If _previewRecord(strField) <> "" Then
                            If Decimal.TryParse(_previewRecord(strField).ToString(), dAmount) Then

                                'If dAmount <= 0 Then
                                '    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                '                      , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, strField) _
                                '                      , RowNumber + 1, True)
                                'End If
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                            , RowNumber + 1, True)
                                    End If
                                    'Integral part of the amount should not exceed 13  
                                    If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                Else ' if no decimal separator
                                    'Integral part of the amount should not exceed 13  
                                    If dAmount.ToString().Length > 13 Then
                                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                            , RowNumber + 1, True)
                                    End If
                                End If

                            Else
                                AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                                    , RowNumber + 1, True)
                            End If
                        End If




                    Next ' for _cnt ...next

                Next ' for _trecCount ...next

                '#21.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                For Each _field As String In New String() {"W1-W/H Tax No.", "W1-Name", "W1-Address", "W1-The Payer of Income Description", "W1-Type of Income (1) Description", "W1-Type of Income (2) Description", "W1-Type of Income (3) Description", "W2-W/H Tax No.", "W2-Name", "W2-Address", "W2-The Payer of Income Description", "W2-Type of Income (1) Description", "W2-Type of Income (2) Description", "W2-Type of Income (3) Description", "W3-W/H Tax No.", "W3-Name", "W3-Address", "W3-The Payer of Income Description", "W3-Type of Income (1) Description", "W3-Type of Income (2) Description", "W3-Type of Income (3) Description"}
                    If _previewRecord(_field) <> "" Then
                        If Not IsValidiFTS2Text(_previewRecord(_field).ToString()) Then
                            AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                                , RowNumber + 1, True)
                        End If
                    End If
                Next

                'For Each _field As String In New String() {"W1-W/H Tax No.", "W2-W/H Tax No.", "W3-W/H Tax No."}
                '    If _previewRecord(_field) <> "" Then
                '        If ContainsInvalidCharacters(_previewRecord(_field).ToString) Then
                '            AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                '                                                           , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                '                                                            , RowNumber + 1, True)
                '        End If
                '    End If
                'Next



                'Validate Fields: Invoice Record
                For _iRecCount As Int32 = 1 To 3

                    InvoiceRecordExists(_previewRecord, iRecordExists, _iRecCount)

                    If Not iRecordExists Then Continue For

                    '#22. 'Record Type-I' is incorrect. It must be 'I'.
                    strField = String.Format("I{0}-Record Type-I", _iRecCount)
                    If _previewRecord(strField).ToString() <> "" AndAlso _previewRecord(strField).ToString().ToUpper() <> "I" Then
                        AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, strField, "'I'") _
                                            , RowNumber + 1, True)
                    End If

                    '#23. Invoice No. - must be alphanumeric
                    strField = String.Format("I{0}-Invoice No.", _iRecCount)
                    If _previewRecord(strField) <> "" Then
                        If regAllowAlphaNum.IsMatch(_previewRecord(strField).ToString()) Then
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#24. Invoice Date - must be valid date and can be either in yymmdd or ymmdd format
                    strField = String.Format("I{0}-Invoice Date", _iRecCount)
                    If _previewRecord(strField) <> "" Then
                        If (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", TblBankFields(strField).DateFormat, _ValueDate, True)) _
                          AndAlso (Not HelperModule.IsDateDataType(_previewRecord(strField).ToString(), "", "yMMdd", _ValueDate, True)) Then
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#25. Invoice Amount - must be numeric
                    strField = String.Format("I{0}-Invoice Amount", _iRecCount)
                    If _previewRecord(strField) <> "" Then
                        If Decimal.TryParse(_previewRecord(strField), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                        , RowNumber + 1, True)
                                End If
                            Else ' if no decimal separator
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Length > 13 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#26. VAT - must be numeric
                    strField = String.Format("I{0}-VAT", _iRecCount)
                    If _previewRecord(strField) <> "" Then
                        If Decimal.TryParse(_previewRecord(strField), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 13 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                        , RowNumber + 1, True)
                                End If
                            Else ' if no decimal separator
                                'Integral part of the amount should not exceed 13  
                                If dAmount.ToString().Length > 13 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 13) _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                            , RowNumber + 1, True)
                        End If
                    End If

                    '#27. Net Amount - must be numeric
                    strField = String.Format("I{0}-Net Amount", _iRecCount)
                    If _previewRecord(strField) <> "" Then
                        If Decimal.TryParse(_previewRecord(strField), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, strField) _
                                                        , RowNumber + 1, True)
                                End If
                                'Integral part of the amount should not exceed 14  
                                If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 14 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 14) _
                                                        , RowNumber + 1, True)
                                End If
                            Else 'if no decimal separator
                                'Integral part of the amount should not exceed 14  
                                If dAmount.ToString().Length > 14 Then
                                    AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050071"), "Record", RowNumber + 1, strField, 14) _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        Else
                            AddValidationError(strField, _PreviewTable.Columns(strField).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, strField) _
                                            , RowNumber + 1, True)
                        End If
                    End If


                    '#27.a.1 VAT <= INVOICE AMOUNT
                    Dim netamountfield As String = String.Format("I{0}-Net Amount", _iRecCount)
                    Dim vatamountfield As String = String.Format("I{0}-VAT", _iRecCount)
                    Dim invoiceamountfield As String = String.Format("I{0}-Invoice Amount", _iRecCount)

                    If Decimal.TryParse(_previewRecord(invoiceamountfield), Nothing) AndAlso Decimal.TryParse(_previewRecord(vatamountfield), Nothing) Then
                        If Decimal.Parse(_previewRecord(vatamountfield)) > Decimal.Parse(_previewRecord(invoiceamountfield)) Then
                            AddValidationError(vatamountfield, _PreviewTable.Columns(vatamountfield).Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09020199"), "Record", RowNumber + 1, vatamountfield) _
                                   , RowNumber + 1, True)
                        End If
                    End If

                    '#27.a.2. NET AMOUNT <= INVOICE AMOUNT
                    If Decimal.TryParse(_previewRecord(invoiceamountfield), Nothing) AndAlso Decimal.TryParse(_previewRecord(netamountfield), Nothing) Then
                        If Decimal.Parse(_previewRecord(invoiceamountfield)) > Decimal.Parse(_previewRecord(netamountfield)) Then
                            AddValidationError(invoiceamountfield, _PreviewTable.Columns(invoiceamountfield).Ordinal + 1 _
                                   , String.Format(MsgReader.GetString("E09020200"), "Record", RowNumber + 1, invoiceamountfield) _
                                   , RowNumber + 1, True)
                        End If
                    End If

                Next

                '#27.a. Validate that if the Text Value is a valid iFTS2 Text value containing English/Thai chars
                For Each _field As String In New String() {"I1-Invoice No.", "I1-Description", "I2-Invoice No.", "I2-Description", "I3-Invoice No.", "I3-Description"}

                    If _previewRecord(_field) = "" Then Continue For

                    If IsValidiFTS2Text(_previewRecord(_field).ToString()) Then Continue For

                    AddValidationError(_field, _PreviewTable.Columns(_field).Ordinal + 1 _
                                    , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _field) _
                                    , RowNumber + 1, True)
                Next

                For Each col As DataColumn In _PreviewTable.Columns
                    If Not IsNothingOrEmptyString(_previewRecord(col)) Then
                        If _previewRecord(col).ToString.Contains("""") Then
                            AddValidationError(col.ColumnName, _PreviewTable.Columns(col.ColumnName).Ordinal + 1 _
                                                                           , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, col.ColumnName) _
                                                                            , RowNumber + 1, True)

                        End If
                    End If
                Next
                RowNumber = RowNumber + 1
            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If

            End If
            If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                _ErrorAtMandatoryField = True
            End If
            RowNumber += 1

        Next

    End Sub
    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()
        Dim strField As String = ""
        For Each row As DataRow In _PreviewTable.Rows
            If _DataModel.IsAmountInCents Then
                row("Gross Amount") = (Convert.ToDecimal(row("Gross Amount")) / 100).ToString()
                For cnt As Integer = 1 To 3
                    strField = String.Format("W{0}-Amount Paid (1)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    'TODO : Remove the commented code after UAT
                    '' ''strField = String.Format("W{0}-Tax Rate (1)", cnt)
                    '' ''If Not IsNothingOrEmptyString(row(strField)) Then
                    '' ''    row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    '' ''End If

                    strField = String.Format("W{0}-WHT Amount (1)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    strField = String.Format("W{0}-Amount Paid (2)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    '' ''strField = String.Format("W{0}-Tax Rate (2)", cnt)
                    '' ''If Not IsNothingOrEmptyString(row(strField)) Then
                    '' ''    row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    '' ''End If

                    strField = String.Format("W{0}-WHT Amount (2)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    strField = String.Format("W{0}-Amount Paid (3)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    '' ''strField = String.Format("W{0}-Tax Rate (3)", cnt)
                    '' ''If Not IsNothingOrEmptyString(row(strField)) Then
                    '' ''    row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    '' ''End If

                    strField = String.Format("W{0}-WHT Amount (3)", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    strField = String.Format("I{0}-Invoice Amount", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    strField = String.Format("I{0}-VAT", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If

                    strField = String.Format("I{0}-Net Amount", cnt)
                    If Not IsNothingOrEmptyString(row(strField)) Then
                        row(strField) = (Convert.ToDecimal(row(strField)) / 100).ToString()
                    End If
                Next

            End If
        Next
    
    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        '*****************************************
        ' BTMU-MAGIC enhancement made in Apr 2011.
        ' allow to read same payment with different invoice information in multi-line
        ' so that the output after consolidation will be only one Payment record and multiple invoices.
        '*****************************************

        If GroupByFields.Count = 0 Then Exit Sub
        Dim _rowIndex As Integer = 0

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Record Type-P").ToString = "P" AndAlso _row("Gross Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Gross Amount"))
            If _row("Record Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Gross Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Gross Amount"))
        Next

        '#2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(GroupByFields, ReferenceFields)

        ''#3. Consolidate Records and apply validations on
        'Dim dhelper As New DataSetHelper()
        'Dim dsPreview As New DataSet("PreviewDataSet")

        'Try
        '    dsPreview.Tables.Add(_PreviewTable.Copy())
        '    dhelper.ds = dsPreview

        '    Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
        '                                            , _groupByFields, _referenceFields, _appendText, "Gross Amount")

        '    _consolidatedRecordCount = _consolidatedData.Rows.Count
        '    _PreviewTable.Rows.Clear()

        '    Dim fmtTblView As DataView = _consolidatedData.DefaultView
        '    _PreviewTable = fmtTblView.ToTable()


        '************** Start of Custom Consolidation logic (refer to VPS logic) ********************
        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If
            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''

        Dim sourceDataView As DataView
        Dim filterString As String
        Dim totalGrossAmt As Decimal
        Dim referenceField(1) As String
        Dim P_rec_no As Int32 = 0
        Dim p_grp_no As Int32 = 0

        Try           

            '2.1 Fixed quick conversion field name for group by fields
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '2.2 Fixed quick conversion field name for reference fields
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next

            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            sourceDataView = _PreviewTable.DefaultView
            sourceDataView.AllowEdit = True            
            'add new column called IsConsolidated
            If _PreviewTable.Columns.IndexOf("IsConsolidated") < 0 Then
                _PreviewTable.Columns.Add(New DataColumn("IsConsolidated", GetType(Boolean)))
                _PreviewTable.Columns("IsConsolidated").ReadOnly = False

            End If


            'add new column called P_rec_no to trace the consolidated P-records  
            If _PreviewTable.Columns.IndexOf("P_rec_no") < 0 Then
                _PreviewTable.Columns.Add(New DataColumn("P_rec_no", GetType(Integer)))
                _PreviewTable.Columns("P_rec_no").ReadOnly = False
            End If

            'add new column called P_grp_no to trace the consolidated P-groups
            If _PreviewTable.Columns.IndexOf("P_grp_no") < 0 Then
                _PreviewTable.Columns.Add(New DataColumn("P_grp_no", GetType(Integer)))
                _PreviewTable.Columns("P_grp_no").ReadOnly = False
            End If

            'Fill value false to IsConsolidated column
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("IsConsolidated") = False
            Next

            'start looping for grouping
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                If _previewRecord("IsConsolidated") Then Continue For
                filterString = "1=1"

                For Each _field As String In _groupByFields
                    If IsNothingOrEmptyString(_field) Then Continue For

                    'prepare filterstring with group by fields
                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_previewRecord(_field).ToString().ToUpper))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For


                sourceDataView.RowFilter = String.Empty
                sourceDataView.RowFilter = filterString
                'P_rec_no = sourceDataView.Count
                p_grp_no += 1
                P_rec_no = 0
                totalGrossAmt = 0
                For Each _row As DataRowView In sourceDataView
                    P_rec_no += 1

                    _row.BeginEdit()
                    'prepare data
                    _row("P_grp_no") = p_grp_no
                    _row("P_rec_no") = P_rec_no

                    ' Create Reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next

                    If _row(AmountFieldName).ToString <> "" And _row(AmountFieldName) <> 0 Then
                        _row(AmountFieldName) = Convert.ToDecimal(_row(AmountFieldName).ToString)
                    End If

                    If IsCheckedSumAmount Then
                        'sum up the amount value                    
                        totalGrossAmt = totalGrossAmt + Convert.ToDecimal(_row(AmountFieldName).ToString)
                    End If

                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            referenceField(intI) = referenceField(intI) & _row(_referenceFields(intI, 1).ToString)
                            _row(_referenceFields(intI, 1).ToString) = referenceField(intI)
                        End If
                    Next

                    _row("IsConsolidated") = True
                    _row.EndEdit()

                Next

                ''update amount after consolidation is done
                If IsCheckedSumAmount Then
                    For Each _row As DataRowView In sourceDataView
                        _row.BeginEdit()
                        _row(AmountFieldName) = totalGrossAmt
                        _row.EndEdit()
                    Next
                End If
                sourceDataView.RowFilter = String.Empty
            Next


            Dim dv As DataView
            dv = _PreviewTable.DefaultView
            dv.Sort = "P_grp_no ASC, P_rec_no ASC"
            Dim dt As DataTable
            dt = dv.ToTable()

            'remove extra columns added manually
            'for system use only
            'dt.Columns.Remove("P_rec_no")
            'dt.Columns.Remove("IsConsolidated")
            _PreviewTable = dt

            dt.Dispose()


            ValidationErrors.Clear()
            FormatOutput()
            GenerateHeader()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()

        Catch ex As Exception
            Throw New MagicException("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    'The output file consists of
    '1)	File Format Record (mandatory)
    '2)	3 Header Records (mandatory)- empty lines
    '3)	Payment Record (mandatory)
    '4)	Multiple WHT Records (optional)
    '5)	Multiple Invoice Record (optional)
    '�	iFTS-2 output file format will be a text file with all fields enclosed within double quotes and separated by comma. 
    '�	Empty data fields will be separated by comma without double quotes
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sAmount As String = String.Empty
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim wstr As String = ""
        Dim istr As String = ""
        Dim P_grp_no = 0

        Dim objMasterTemplate As MasterTemplateNonFixed ' Non Fixed Type Master Template
        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            '1. File Format Type
            sText = _Header & vbNewLine
            '2. Three empty Header Rows
            sText &= vbNewLine & vbNewLine & vbNewLine

            'to check group
            'add new column called P_rec_no to trace the consolidated P-records  
            If _PreviewTable.Columns.IndexOf("P_rec_no") < 0 Then
                _PreviewTable.Columns.Add(New DataColumn("P_rec_no", GetType(Integer)))
                _PreviewTable.Columns("P_rec_no").ReadOnly = False
            End If

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                '3. Payment Record
                If _previewRecord("Gross Amount").ToString() <> "" Then
                    _previewRecord("Gross Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Gross Amount"))), 2).ToString("############0.00")
                End If

                
                'only for consolidated transactions
                'if P-records are same, and consolidated is true, it will be skipped
                If IsConsolidated AndAlso Not _previewRecord("P_rec_no").ToString().Equals("1") Then
                    'If Not _previewRecord("P_rec_no").ToString().Equals("1") Then
                    'skip to print out p-record 
                    'skip to print out w-record too
                    'Else
                    'End If

                Else

                    For _column As Int32 = 1 To 13

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", ",") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                    '4. Tax Records
                    For _cnt As Int32 = 1 To 3

                        sAmount = String.Format("W{0}-Amount Paid ({0})", _cnt)
                        If _previewRecord(sAmount).ToString() <> "" Then
                            _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("############0.00")
                        End If

                        sAmount = String.Format("W{0}-Tax Rate ({0})", _cnt)
                        If _previewRecord(sAmount).ToString() <> "" Then
                            _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("##0.00")
                        End If

                        sAmount = String.Format("W{0}-WHT Amount ({0})", _cnt)
                        If _previewRecord(sAmount).ToString() <> "" Then
                            _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("############0.00")
                        End If
                    Next

                    sLine &= vbNewLine
                    wstr = ""

                    'W1 fields
                    For _column As Int32 = 14 To 40
                        If _previewRecord(_column).ToString <> "" Then
                            wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 40, "", ",")
                        Else
                            wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 40, "", ",")
                        End If
                    Next

                    If wstr.Replace(",", "").Length > 0 Then
                        sLine &= wstr & vbNewLine
                    End If

                    wstr = ""

                    'End If
                    'commented by Kay for testing
                    'W2 fields
                    For _column As Int32 = 41 To 67
                        If _previewRecord(_column).ToString <> "" Then
                            wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 67, "", ",")
                        Else
                            wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 67, "", ",")
                        End If
                    Next

                    If wstr.Replace(",", "").Length > 0 Then
                        sLine &= wstr & vbNewLine
                    End If

                    wstr = ""
                    'W3 fields
                    For _column As Int32 = 68 To 94
                        If _previewRecord(_column).ToString <> "" Then
                            wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 94, "", ",")
                        Else
                            wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 94, "", ",")
                        End If
                    Next

                    If wstr.Replace(",", "").Length > 0 Then
                        sLine &= wstr & vbNewLine
                    End If

                End If
                'move to here testing

                '5. Invoice Records
                For _cnt As Int32 = 1 To 3

                    sAmount = String.Format("I{0}-Invoice Amount", _cnt)
                    If _previewRecord(sAmount).ToString() <> "" Then
                        _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("############0.00")
                    End If

                    sAmount = String.Format("I{0}-VAT", _cnt)
                    If _previewRecord(sAmount).ToString() <> "" Then
                        _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("############0.00")
                    End If

                    sAmount = String.Format("I{0}-Net Amount", _cnt)
                    If _previewRecord(sAmount).ToString() <> "" Then
                        _previewRecord(sAmount) = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord(sAmount))), 2).ToString("#############0.00")
                    End If

                Next

                wstr = ""
                'I-1 Fields
                For _column As Int32 = 95 To 101
                    If _previewRecord(_column).ToString <> "" Then
                        wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 101, "", ",")
                    Else
                        wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 101, "", ",")
                    End If
                Next

                If wstr.Replace(",", "").Length > 0 Then
                    sLine &= wstr & vbNewLine
                End If

                'I-2 Fields
                wstr = ""
                For _column As Int32 = 102 To 108
                    If _previewRecord(_column).ToString <> "" Then
                        wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 108, "", ",")
                    Else
                        wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 107, "", ",")
                    End If
                Next

                If wstr.Replace(",", "").Length > 0 Then
                    sLine &= wstr & vbNewLine
                End If

                'I-3 Fields
                wstr = ""
                For _column As Int32 = 109 To 115
                    If _previewRecord(_column).ToString <> "" Then
                        wstr = wstr & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter & IIf(_column = 115, "", ",")
                    Else
                        wstr = wstr & _previewRecord(_column).ToString() & IIf(_column = 115, "", ",")
                    End If
                Next

                If wstr.Replace(",", "").Length > 0 Then
                    sLine &= wstr & vbNewLine
                End If

                sText = sText & sLine

                rowNo += 1

            Next

            If sText.Length > 0 AndAlso sText(sText.Length - 1) = vbNewLine Then sText = sText.Remove(sText.Length - 1)
            Return SaveTextToFile(sText, _outputFile) '.Replace("iFTS2SingleLineFormat", "iFTS2")

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Amount Field has invalid numeric value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

#End Region

#Region "Helper Methods"

    'Permitted Special Characters in iFTS-2 File Format despite Eng/Thai alphabets
    ' @	#	%	&	*	(	)	_	-	=	+	;	:	"	'	<
    '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
    Private Function IsValidiFTS2Text(ByVal textValue As String) As Boolean

        If IsNothingOrEmptyString(textValue) Then Return True

        textValue = textValue.Trim()

        Dim thaiEncoding As System.Text.Encoding = System.Text.Encoding.GetEncoding(874)
        '33 - 126 English chars: A-Za-z0-9 and special characters 
        '161 - 251 Thai Charas: Alphabets and Numbers
        For Each b As Byte In thaiEncoding.GetBytes(textValue)
            If (b > 32 AndAlso b < 127) OrElse (b > 160 AndAlso b < 252) Then
                'its english/thai alphabets/numbers/special chars
            Else
                ' non english/thai character
                'Return False ' If allow only ansi encoded thai characters, return false
                Return True
            End If

        Next

        Return True

    End Function

    Private Function ContainsInvalidCharacters(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in iFTS-2 File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) >= 32 AndAlso Convert.ToInt32(chr) <= 126 Then
                If Convert.ToInt32(chr) = 34 Then Return True ' Do not allow double quote(")
            Else
                Return True
            End If
        Next
        Return False
    End Function
    Sub TaxRecordExists(ByVal row As DataRow, ByRef TRecordExists As Boolean, ByVal TaxSetNo As Int32)

        TRecordExists = False
        Dim colSt As Int32 = 0
        Dim colEd As Int32 = 0
        Select Case TaxSetNo
            Case 1
                colSt = 14
                colEd = 40
            Case 2
                colSt = 41
                colEd = 67
            Case 3
                colSt = 68
                colEd = 94
            Case Else
                Exit Sub
        End Select

        For col As Int32 = colSt To colEd

            If IsNothingOrEmptyString(row(col)) Then Continue For
            TRecordExists = True
            Exit For

        Next

     
    End Sub

    Sub InvoiceRecordExists(ByVal row As DataRow, ByRef IRecordExists As Boolean, ByVal InvoiceSetNo As Int32)


        IRecordExists = False
        Dim colSt As Int32 = 0
        Dim colEd As Int32 = 0
        Select Case InvoiceSetNo
            Case 1
                colSt = 95
                colEd = 101
            Case 2
                colSt = 102
                colEd = 108
            Case 3
                colSt = 109
                colEd = 115
            Case Else
                Exit Sub
        End Select

        For col As Int32 = colSt To colEd

            If IsNothingOrEmptyString(row(col)) Then Continue For
            IRecordExists = True
            Exit For

        Next

    End Sub

    Private Function DoTypeofIncomeFieldsHaveValue(ByVal row As DataRow, ByVal wsetid As Int32, ByVal isetid As Int32) As Boolean

        For Each field As String In New String() {"W" & wsetid.ToString() & "-Type of Income (" & isetid.ToString() & ")", "W" & wsetid.ToString() & "-Type of Income (" & isetid.ToString() & ") Description", "W" & wsetid.ToString() & "-Date (" & isetid.ToString() & ")", "W" & wsetid.ToString() & "-Amount Paid (" & isetid.ToString() & ")", "W" & wsetid.ToString() & "-Tax Rate (" & isetid.ToString() & ")", "W" & wsetid.ToString() & "-WHT Amount (" & isetid.ToString() & ")"}
            'if any one of them have value
            If Not IsNothingOrEmptyString(row(field)) Then Return True
        Next

        Return False

    End Function


#End Region

End Class