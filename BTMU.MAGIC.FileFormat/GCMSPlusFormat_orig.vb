'************************************************CREATED**********************************
'SRS No                     Date Modified   Description
'--------------------------------------------------------------------------------------------------
'SRS/BTMU/2010-0018         01/Apr/2010     Add Sector Selection Logic for India
'**************************************************************************************************
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the GCMS Plus Format
''' </summary>
''' <remarks></remarks>
Public Class GCMSPlusFormat
    Inherits BaseFileFormat

    Private nationalClearingCodes() As String = {"AT", "AU", "BL", "CC", "CH", "CP", "ES", "FW", "GR", "HK", "IE", "IN", "IT", "NZ", "PL", "PT", "RU", "SC", "SW"}

    Private MandatoryFields() As String = {"Settlement Account No", "Value Date", "Payment Type/Sector Selection", _
                                 "Currency", "Remittance Amount", "Beneficiary Account No", "Beneficiary Name/Address", _
                                 "Bank Charges"}

    Private Shared _amountFieldName As String = "Remittance Amount"

    Private Shared _editableFields() As String = {"Settlement Account No", "Value Date", _
          "Customer Reference", "Payment Type/Sector Selection", "Currency", "Remittance Amount", _
          "Exchange Method", "Forward Contract Number", "Intermediary Bank/Branch/Address", _
          "Intermediary Bank Master Code", "Beneficiary Bank/Branch/Address", "Beneficiary Bank Master Code", _
          "Beneficiary Account No", "Beneficiary Name/Address", "Message To Beneficiary", "Purpose of Remittance", _
          "Information To Remitting Bank", "Bank Charges", "Charges Account Number", "IBAN Flag", "Option 1", _
          "Option 2", "Option 3", "Option 4", "Option 5", "Option 6", "Option 7", "Option 8", "Option 9", "Option 10"}

    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Settlement Account No", "Value Date", _
          "Customer Reference", "Payment Type/Sector Selection", "Currency", "Remittance Amount", _
          "Exchange Method", "Forward Contract Number", "Intermediary Bank/Branch/Address", _
          "Intermediary Bank Master Code", "Beneficiary Bank/Branch/Address", "Beneficiary Bank Master Code", _
          "Beneficiary Account No", "Beneficiary Name/Address", "Message To Beneficiary", "Purpose of Remittance", _
          "Information To Remitting Bank", "Bank Charges", "Charges Account Number", "IBAN Flag", "Option 1", _
          "Option 2", "Option 3", "Option 4", "Option 5", "Option 6", "Option 7", "Option 8", "Option 9", "Option 10", "Consolidate Field"}

    Private Shared _autoTrimmableFields() As String = {"Customer Reference", "Intermediary Bank/Branch/Address", "Intermediary Bank Master Code", _
            "Beneficiary Bank/Branch/Address", "Beneficiary Bank Master Code", "Beneficiary Name/Address", _
            "Message To Beneficiary", "Purpose of Remittance", "Information To Remitting Bank", "IBAN Flag", "Option 1", _
            "Option 2", "Option 3", "Option 4", "Option 5", "Option 6", "Option 7", "Option 8", "Option 9", "Option 10"}
    Private Shared _splitFile As Boolean = False

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "GCMSPlus"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

#End Region

    Public Shared Property SplitFile() As Boolean
        Get
            Return _splitFile
        End Get
        Set(ByVal value As Boolean)
            _splitFile = value
        End Set
    End Property


    Public Shared Function OutputFormatString() As String
        Return "GCMSPlus"
    End Function

#Region "Overriden Methods"

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = -1
        Dim NameExist, AddressExist, CountryExist As Boolean
        NameExist = AddressExist = CountryExist = False
        Dim temp, expandedCountryName As String
        Dim pos As Int16
        'For Specific country validations
        Dim ListCountry() As String
        ListCountry = "".Split("|")
        expandedCountryName = ""
        If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
            If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                expandedCountryName = ListCountry(0).ToUpper()
                System.Array.Sort(ListCountry)
            End If
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Customer Reference
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Customer Reference").ToString() <> "" AndAlso _
                        (_previewRecord("Customer Reference").ToString().StartsWith(":") Or _
                        _previewRecord("Customer Reference").ToString().StartsWith("-")) Then

                    _previewRecord("Customer Reference") = " " + _previewRecord("Customer Reference").ToString().Substring(1)
                End If

                '#Fill Fields Exchange Method, and Forward Contract Number              
                'added for GCMS-OVS settlement account no. format checking.
                'GCMS smile > currency code is end of the settlement account
                'GCMS OVS > currency code is position 5th to 7th (index starts from one )
                If _previewRecord.Table.Columns.Contains("Exchange Method") And _previewRecord.Table.Columns.Contains("Settlement Account No") Then
                    If _previewRecord("Exchange Method").ToString = "" Then
                        If Not _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                        AndAlso _previewRecord("Settlement Account No").ToString().Length < 7 Then
                            'Do nothing
                        Else
                            'match currencies (either OVS or SMILE)
                            If _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                                OrElse _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then
                                _previewRecord("Exchange Method") = ""
                            Else
                                _previewRecord("Exchange Method") = IIf(_previewRecord("Forward Contract Number").ToString() = "", "SPOT", "CONT")
                            End If

                        End If
                    End If
                End If

                'If _previewRecord("Exchange Method").ToString() = "" Or _previewRecord("Exchange Method").ToString().Equals("Spot", StringComparison.CurrentCultureIgnoreCase) Then
                '    _previewRecord("Forward Contract Number") = ""
                'End If


                '********************************
                'Start Auto String Manipulation for Intermediary Bank/Branch/Address
                '********************************
                'clear variables
                NameExist = False
                AddressExist = False
                CountryExist = False

                'Check SWIFT is empty not empty and any of temp fields has value (to concatenate swift code to bank name field)
                If _previewRecord("Intm SWIFT BIC").ToString.Trim <> "" AndAlso (_previewRecord("Intermediary Bank Name").ToString <> "" OrElse _
                    _previewRecord("Intermediary Bank Address").ToString <> "" OrElse _previewRecord("Intermediary Bank Country").ToString <> "") Then
                    _previewRecord("Intermediary Bank Name") = _previewRecord("Intm SWIFT BIC").ToString + " " + _previewRecord("Intermediary Bank Name").ToString()
                End If


                If _previewRecord("Intermediary Bank Name").ToString <> "" Then
                    NameExist = True

                    If _previewRecord("Intermediary Bank Name").ToString.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Intermediary Bank Name"), 34)

                    Else

                        temp = _previewRecord("Intermediary Bank Name").ToString().Substring(0, 35)
                        'shift the word to right if it's split at position 35th
                        If Not temp(34).Equals(" ") Then
                            pos = temp.LastIndexOf(" ")
                            temp = _previewRecord("Intermediary Bank Name").ToString().Substring(pos + 1)

                            'update the bank fields
                            _previewRecord("Intermediary Bank Name") = _previewRecord("Intermediary Bank Name").ToString().Substring(0, pos + 1)
                            'pad spaces in the end
                            PadTrailingSpaces(_previewRecord("Intermediary Bank Name"), 34)
                            _previewRecord("Intermediary Bank Address") = temp + " " + _previewRecord("Intermediary Bank Address")
                        Else
                            'update Address first
                            _previewRecord("Intermediary Bank Address") = _previewRecord("Intermediary Bank Name").ToString().Substring(35) + " " + _previewRecord("Intermediary Bank Address")
                            _previewRecord("Intermediary Bank Name") = _previewRecord("Intermediary Bank Name").ToString().Substring(0, 35)

                        End If
                    End If

                End If

                If _previewRecord("Intermediary Bank Address").ToString <> "" Then
                    AddressExist = True
                    If _previewRecord("Intermediary Bank Address").ToString.Length < 70 Then
                        PadTrailingSpaces(_previewRecord("Intermediary Bank Address"), 69)
                    Else
                        'trim up to 70 characters automatically
                        _previewRecord("Intermediary Bank Address") = _previewRecord("Intermediary Bank Address").ToString.Substring(0, 69)
                    End If
                End If

                If _previewRecord("Intermediary Bank Country").ToString <> "" Then
                    CountryExist = True
                    If _previewRecord("Intermediary Bank Country").ToString.Trim.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Intermediary Bank Country"), 35)
                    Else
                        _previewRecord("Intermediary Bank Country") = _previewRecord("Intermediary Bank Country").ToString.Substring(0, 35)
                    End If
                End If

                'Check temporary fields are empty or not
                If NameExist Then
                    _previewRecord("Intermediary Bank/Branch/Address") = _previewRecord("Intermediary Bank Name").ToString + " "
                    If AddressExist Then
                        _previewRecord("Intermediary Bank/Branch/Address") = _previewRecord("Intermediary Bank/Branch/Address").ToString + _previewRecord("Intermediary Bank Address").ToString + " "
                    Else
                        PadTrailingSpaces(_previewRecord("Intermediary Bank/Branch/Address"), 105)
                    End If
                    If CountryExist Then
                        _previewRecord("Intermediary Bank/Branch/Address") = _previewRecord("Intermediary Bank/Branch/Address").ToString + _previewRecord("Intermediary Bank Country").ToString
                    Else
                        PadTrailingSpaces(_previewRecord("Intermediary Bank/Branch/Address"), 140)
                    End If
                ElseIf _previewRecord("Intermediary Bank/Branch/Address").ToString <> "" Then
                    If _previewRecord("Intm SWIFT BIC").ToString <> "" Then
                        _previewRecord("Intermediary Bank/Branch/Address") = _previewRecord("Intm SWIFT BIC").ToString + " " + _previewRecord("Intermediary Bank/Branch/Address").ToString
                    Else
                        _previewRecord("Intermediary Bank/Branch/Address") = _previewRecord("Intermediary Bank/Branch/Address").ToString
                    End If

                Else
                    If _previewRecord("Intm SWIFT BIC").ToString <> "" Then
                        'Option-A
                        _previewRecord("Intermediary Bank/Branch/Address") = "A:" + _previewRecord("Intm SWIFT BIC").ToString
                    ElseIf _previewRecord("Intm National Clearing Code").ToString <> "" Then
                        _previewRecord("Intermediary Bank/Branch/Address") = "C:" + _previewRecord("Intm National Clearing Code").ToString
                    End If
                End If
                'End Auto String Manipulation for Intermediary Bank/Branch/Address
                '********************************

                'Replace hyphen or colon at positions 1/36/71/106
                Dim replaceValue As New System.Text.StringBuilder(_previewRecord("Intermediary Bank/Branch/Address").ToString())
                For Each index As Int32 In ReplaceCharLocs
                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                            replaceValue(index) = " "
                        End If

                    End If
                Next
                _previewRecord("Intermediary Bank/Branch/Address") = replaceValue


                '********************************
                'Start Auto String Manipulation for Beneficiary Bank/Branch/Address
                '********************************
                'clear variables
                NameExist = False
                AddressExist = False
                CountryExist = False

                'Check SWIFT is empty not empty and any of temp fields has value (to concatenate swift code to bank name field)
                If _previewRecord("Bene SWIFT BIC").ToString.Trim <> "" AndAlso (_previewRecord("Bene Bank Name").ToString <> "" OrElse _
                    _previewRecord("Bene Bank Address").ToString <> "" OrElse _previewRecord("Bene Bank Country").ToString <> "") Then
                    _previewRecord("Bene Bank Name") = _previewRecord("Bene SWIFT BIC").ToString + " " + _previewRecord("Bene Bank Name").ToString()
                End If

                If _previewRecord("Bene Bank Name").ToString <> "" Then
                    NameExist = True
                    If _previewRecord("Bene Bank Name").ToString.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Bene Bank Name"), 34)
                    Else
                        temp = _previewRecord("Bene Bank Name").ToString().Substring(0, 35)
                        'shift the word to right if it's split at position 35th
                        If Not temp(34).Equals(" ") Then
                            pos = temp.LastIndexOf(" ")
                            temp = _previewRecord("Bene Bank Name").ToString().Substring(pos + 1)

                            'update the bank fields
                            _previewRecord("Bene Bank Name") = _previewRecord("Bene Bank Name").ToString().Substring(0, pos + 1)
                            'pad spaces in the end
                            PadTrailingSpaces(_previewRecord("Bene Bank Name"), 34)
                            _previewRecord("Bene Bank Address") = temp + " " + _previewRecord("Bene Bank Address")
                        Else
                            'update Address first
                            _previewRecord("Bene Bank Address") = _previewRecord("Bene Bank Name").ToString().Substring(35) + " " + _previewRecord("Bene Bank Address")
                            _previewRecord("Bene Bank Name") = _previewRecord("Bene Bank Name").ToString().Substring(0, 35)

                        End If
                    End If
                End If

                If _previewRecord("Bene Bank Address").ToString <> "" Then
                    AddressExist = True
                    If _previewRecord("Bene Bank Address").ToString.Length < 70 Then
                        PadTrailingSpaces(_previewRecord("Bene Bank Address"), 69)
                    Else
                        'trim up to 70 characters automatically
                        _previewRecord("Bene Bank Address") = _previewRecord("Bene Bank Address").ToString.Substring(0, 69)
                    End If
                End If

                If _previewRecord("Bene Bank Country").ToString <> "" Then
                    CountryExist = True
                    If _previewRecord("Bene Bank Country").ToString.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Bene Bank Country"), 35)
                    Else
                        _previewRecord("Bene Bank Country") = _previewRecord("Bene Bank Country").ToString.Substring(0, 35)
                    End If
                End If

                If NameExist Then
                    _previewRecord("Beneficiary Bank/Branch/Address") = _previewRecord("Bene Bank Name").ToString + " "
                    If AddressExist Then
                        _previewRecord("Beneficiary Bank/Branch/Address") = _previewRecord("Beneficiary Bank/Branch/Address").ToString + _previewRecord("Bene Bank Address").ToString + " "
                    Else

                        PadTrailingSpaces(_previewRecord("Beneficiary Bank/Branch/Address"), 105)
                    End If
                    If CountryExist Then
                        _previewRecord("Beneficiary Bank/Branch/Address") = _previewRecord("Beneficiary Bank/Branch/Address").ToString + _previewRecord("Bene Bank Country").ToString
                    Else
                        PadTrailingSpaces(_previewRecord("Beneficiary Bank/Branch/Address"), 140)
                    End If
                ElseIf _previewRecord("Beneficiary Bank/Branch/Address").ToString <> "" Then
                    If _previewRecord("Bene SWIFT BIC").ToString <> "" Then
                        _previewRecord("Beneficiary Bank/Branch/Address") = _previewRecord("Bene SWIFT BIC").ToString + " " + _previewRecord("Beneficiary Bank/Branch/Address").ToString
                    Else
                        _previewRecord("Beneficiary Bank/Branch/Address") = _previewRecord("Beneficiary Bank/Branch/Address").ToString
                    End If

                Else
                    If _previewRecord("Bene SWIFT BIC").ToString <> "" Then
                        'Option-A
                        _previewRecord("Beneficiary Bank/Branch/Address") = "A:" + _previewRecord("Bene SWIFT BIC").ToString
                    ElseIf _previewRecord("Bene National Clearing Code").ToString <> "" Then
                        _previewRecord("Beneficiary Bank/Branch/Address") = "C:" + _previewRecord("Bene National Clearing Code").ToString
                    End If
                End If
                'End Auto String Manipulation for Beneficiary Bank/Branch/Address
                '********************************

                'Replace hyphen or colon at positions 1/36/71/106
                replaceValue = New System.Text.StringBuilder(_previewRecord("Beneficiary Bank/Branch/Address").ToString())
                For Each index As Int32 In ReplaceCharLocs
                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                            replaceValue(index) = " "
                        End If

                    End If
                Next
                _previewRecord("Beneficiary Bank/Branch/Address") = replaceValue


                '********************************
                'Start Auto String Manipulation for Beneficiary Name/Address
                '********************************
                'clear variables
                NameExist = False
                AddressExist = False
                CountryExist = False
                If _previewRecord("Bene Name").ToString <> "" Then
                    NameExist = True
                    If _previewRecord("Bene Name").ToString.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Bene Name"), 34)
                    Else
                        temp = _previewRecord("Bene Name").ToString().Substring(0, 35)
                        'shift the word to right if it's split at position 35th
                        If Not temp(34).Equals(" ") Then
                            pos = temp.LastIndexOf(" ")
                            temp = _previewRecord("Bene Name").ToString().Substring(pos + 1)

                            'update the bank fields
                            _previewRecord("Bene Name") = _previewRecord("Bene Name").ToString().Substring(0, pos + 1)
                            'pad spaces in the end                            
                            PadTrailingSpaces(_previewRecord("Bene Name"), 34)
                            _previewRecord("Bene Address") = temp + " " + _previewRecord("Bene Address")
                        Else
                            'update Address first
                            _previewRecord("Bene Address") = _previewRecord("Bene Name").ToString().Substring(35) + " " + _previewRecord("Bene Address")
                            _previewRecord("Bene Name") = _previewRecord("Bene Name").ToString().Substring(0, 35)

                        End If
                    End If
                End If

                If _previewRecord("Bene Address").ToString <> "" Then
                    AddressExist = True
                    If _previewRecord("Bene Address").ToString.Length < 70 Then

                        PadTrailingSpaces(_previewRecord("Bene Address"), 69)
                    Else
                        'trim up to 70 characters automatically
                        _previewRecord("Bene Address") = _previewRecord("Bene Address").ToString.Substring(0, 69)
                    End If
                End If

                If _previewRecord("Bene Country").ToString <> "" Then
                    CountryExist = True
                    If _previewRecord("Bene Country").ToString.Length < 35 Then
                        PadTrailingSpaces(_previewRecord("Bene Country"), 35)
                    Else
                        _previewRecord("Bene Country") = _previewRecord("Bene Country").ToString.Substring(0, 35)
                    End If
                End If

                If NameExist Then
                    _previewRecord("Beneficiary Name/Address") = _previewRecord("Bene Name").ToString + " "
                    If AddressExist Then
                        _previewRecord("Beneficiary Name/Address") = _previewRecord("Beneficiary Name/Address") + _previewRecord("Bene Address").ToString + " "
                    Else
                        PadTrailingSpaces(_previewRecord("Beneficiary Name/Address"), 105)
                    End If
                    If CountryExist Then
                        _previewRecord("Beneficiary Name/Address") = _previewRecord("Beneficiary Name/Address") + _previewRecord("Bene Country").ToString
                    Else
                        PadTrailingSpaces(_previewRecord("Beneficiary Name/Address"), 140)
                    End If
                End If
                'End Auto String Manipulation for Beneficiary Bank/Branch/Address
                '********************************

                'Replace hyphen or colon at positions 1/36/71/106
                If _previewRecord("Beneficiary Name/Address").ToString() <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Beneficiary Name/Address").ToString())
                    For Each index As Int32 In ReplaceCharLocs
                        If index < replaceValue.Length Then
                            If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                                replaceValue(index) = " "
                            End If

                        End If
                    Next
                    _previewRecord("Beneficiary Name/Address") = replaceValue
                End If

                'Message To Beneficiary
                'Replace hyphen or colon at positions 1/36/71/106
                If _previewRecord("Message To Beneficiary").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Message To Beneficiary").ToString())
                    For Each index As Int32 In ReplaceCharLocs
                        If index < replaceValue.Length Then
                            If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                                replaceValue(index) = " "
                            End If

                        End If
                    Next
                    _previewRecord("Message To Beneficiary") = replaceValue
                End If

                'Purpose of Remittance (only for 1st position)
                If _previewRecord("Purpose of Remittance").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Purpose of Remittance").ToString())
                    If replaceValue(0) = "-" Or replaceValue(0) = ":" Then
                        replaceValue(0) = " "
                    End If
                    _previewRecord("Purpose of Remittance") = replaceValue
                End If

                'Information To Remitting Bank
                'Replace hyphen or colon at positions 1/36/71
                If _previewRecord("Information To Remitting Bank").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Information To Remitting Bank").ToString())
                    If expandedCountryName.ToUpper().Contains("INDONESIA") Then
                        'Position 27 and 62 only
                        If replaceValue.Length >= 27 Then
                            If replaceValue(26) = "-" Or replaceValue(26) = ":" Then replaceValue(26) = " "
                        End If

                        If replaceValue.Length >= 62 Then
                            If replaceValue(61) = "-" Or replaceValue(61) = ":" Then replaceValue(61) = " "
                        End If
                    Else
                        'Position 1 and 36 and 71 only
                        If replaceValue(0) = "-" Or replaceValue(0) = ":" Then
                            replaceValue(0) = " "
                        End If
                        If replaceValue.Length >= 36 Then
                            If replaceValue(35) = "-" Or replaceValue(35) = ":" Then replaceValue(35) = " "
                        End If
                        If replaceValue.Length >= 71 Then
                            If replaceValue(70) = "-" Or replaceValue(70) = ":" Then replaceValue(70) = " "
                        End If
                    End If
                    _previewRecord("Information To Remitting Bank") = replaceValue
                End If

                'Charges Account Number
                If _previewRecord("Bank Charges").ToString().Equals("Ben", StringComparison.CurrentCultureIgnoreCase) Or _
                            _previewRecord("Bank Charges").ToString().Equals("Beneficiary", StringComparison.CurrentCultureIgnoreCase) Then
                    _previewRecord("Charges Account Number") = ""

                ElseIf _previewRecord("Bank Charges").ToString().Equals("SHA", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("Share", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("OUR", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("Applicant", StringComparison.CurrentCultureIgnoreCase) Then

                    _previewRecord("Charges Account Number") = IIf(_previewRecord("Charges Account Number").ToString().Equals(_previewRecord("Settlement Account No").ToString, _
                                StringComparison.CurrentCultureIgnoreCase), "", _previewRecord("Charges Account Number").ToString)
                End If


                '# Format Remittance Amount 
                If _previewRecord("Remittance Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()) / 100
                    End If

                    Select Case _previewRecord("Currency").ToString().ToUpper()

                        Case "TND", "KWD", "BHD", "OMR"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.000")

                        Case "TWD", "JPY", "KRW", "VND", "CLP"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.00")

                    End Select

                End If

                '"Beneficiary Account No" (Change request after release)
                'fixed @ 04th Aug 2011
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Beneficiary Account No").ToString() <> "" AndAlso _
                    (_previewRecord("Beneficiary Account No").ToString().StartsWith(":") Or _
                    _previewRecord("Beneficiary Account No").ToString().StartsWith("-")) Then

                    _previewRecord("Beneficiary Account No") = " " + _previewRecord("Beneficiary Account No").ToString().Substring(1)
                End If

                '#1. 'Option 9' field: replace ',' with ';'
                If _previewRecord("Option 9").ToString.Contains(",") Then
                    _previewRecord("Option 9") = _previewRecord("Option 9").ToString.Replace(",", ";")
                End If


                '#2. Replace '@' to 'AT' and '&' to 'AND' Non SWIFT Characters (except Option fields)
                '#CR-2 Replace hyphen and colon @ poisitons 1/36/71/106 (except Option fields)
                '#fixed @ 06 Sep 2011 by Kay
                For i As Int32 = 0 To 19
                    _previewRecord(i) = _previewRecord(i).ToString().Replace("&", "AND")
                    _previewRecord(i) = _previewRecord(i).ToString().Replace("@", "AT")
                    'do not remove hyphen from non-text datatype fields (2,6,10,12)
                    Select Case i
                        Case 1, 5, 9, 11, 18
                            'skip
                        Case Else
                            _previewRecord(i) = RemoveHyphenColon(_previewRecord(i).ToString())
                    End Select

                Next

                '#3. Remove Unpermitted Characters (Change Request: fixed on 05 Sepc 2011)
                '"CUSTOMER REFERENCE"
                _previewRecord(2) = RemoveGCMSPlusInvalidCharacters(_previewRecord(2).ToString())

                '"FORWARD CONTRACT NUMBER", "INTERMEDIARY BANK/BRANCH/ADDRESS", "INTERMEDIARY BANK MASTER CODE", _
                '"BENEFICIARY BANK/BRANCH/ADDRESS", "BENEFICIARY BANK MASTER CODE", "BENEFICIARY ACCOUNT NO", _
                '"BENEFICIARY NAME/ADDRESS", "MESSAGE TO BENEFICIARY", "PURPOSE OF REMITTANCE", "INFORMATION TO REMITTING BANK"
                For i As Int32 = 7 To 16
                    _previewRecord(i) = RemoveGCMSPlusInvalidCharacters(_previewRecord(i).ToString())
                Next




            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1
        Dim NameExist, AddressExist, CountryExist As Boolean
        NameExist = AddressExist = CountryExist = False
        Dim expandedCountryName As String
        Dim pos As Int16
        'For Specific country validations
        Dim ListCountry() As String
        ListCountry = "".Split("|")
        expandedCountryName = ""
        If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
            If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                expandedCountryName = ListCountry(0).ToUpper()
                System.Array.Sort(ListCountry)
            End If
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Customer Reference
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Customer Reference").ToString() <> "" AndAlso _
                    (_previewRecord("Customer Reference").ToString().StartsWith(":") Or _
                    _previewRecord("Customer Reference").ToString().StartsWith("-")) Then
                    _previewRecord("Customer Reference") = " " + _previewRecord("Customer Reference").ToString().Substring(1)
                End If

                '#Fill Fields Exchange Method, and Forward Contract Number              
                'added for GCMS-OVS settlement account no. format checking.
                'GCMS smile > currency code is end of the settlement account
                'GCMS OVS > currency code is position 5th to 7th (index starts from one )
                If _previewRecord("Exchange Method").ToString = "" Then
                    If Not _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                    AndAlso _previewRecord("Settlement Account No").ToString().Length < 7 Then
                        'Do nothing
                    Else
                        'match currencies (either OVS or SMILE)
                        If _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                            OrElse _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then
                            _previewRecord("Exchange Method") = ""
                        Else
                            _previewRecord("Exchange Method") = IIf(_previewRecord("Forward Contract Number").ToString() = "", "SPOT", "CONT")
                        End If

                    End If
                End If


                'Replace hyphen or colon at positions 1/36/71/106
                Dim replaceValue As New System.Text.StringBuilder(_previewRecord("Intermediary Bank/Branch/Address").ToString())
                For Each index As Int32 In ReplaceCharLocs
                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                            replaceValue(index) = " "
                        End If

                    End If
                Next
                _previewRecord("Intermediary Bank/Branch/Address") = replaceValue


                'Replace hyphen or colon at positions 1/36/71/106
                replaceValue = New System.Text.StringBuilder(_previewRecord("Beneficiary Bank/Branch/Address").ToString())
                For Each index As Int32 In ReplaceCharLocs
                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                            replaceValue(index) = " "
                        End If

                    End If
                Next
                _previewRecord("Beneficiary Bank/Branch/Address") = replaceValue



                'Replace hyphen or colon at positions 1/36/71/106
                If _previewRecord("Beneficiary Name/Address").ToString() <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Beneficiary Name/Address").ToString())
                    For Each index As Int32 In ReplaceCharLocs
                        If index < replaceValue.Length Then
                            If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                                replaceValue(index) = " "
                            End If

                        End If
                    Next
                    _previewRecord("Beneficiary Name/Address") = replaceValue
                End If

                'Message To Beneficiary
                'Replace hyphen or colon at positions 1/36/71/106
                If _previewRecord("Message To Beneficiary").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Message To Beneficiary").ToString())
                    For Each index As Int32 In ReplaceCharLocs
                        If index < replaceValue.Length Then
                            If replaceValue(index) = "-" Or replaceValue(index) = ":" Then
                                replaceValue(index) = " "
                            End If

                        End If
                    Next
                    _previewRecord("Message To Beneficiary") = replaceValue
                End If

                'Purpose of Remittance (only for 1st position)
                If _previewRecord("Purpose of Remittance").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Purpose of Remittance").ToString())
                    If replaceValue(0) = "-" Or replaceValue(0) = ":" Then
                        replaceValue(0) = " "
                    End If
                    _previewRecord("Purpose of Remittance") = replaceValue
                End If

                'Information To Remitting Bank
                'Replace hyphen or colon at positions 1/36/71/106
                If _previewRecord("Information To Remitting Bank").ToString <> "" Then
                    replaceValue = New System.Text.StringBuilder(_previewRecord("Information To Remitting Bank").ToString())
                    If expandedCountryName.ToUpper().Contains("INDONESIA") Then
                        'Position 27 and 62 only
                        If replaceValue.Length >= 27 Then
                            If replaceValue(26) = "-" Or replaceValue(26) = ":" Then replaceValue(26) = " "
                        End If

                        If replaceValue.Length >= 62 Then
                            If replaceValue(61) = "-" Or replaceValue(61) = ":" Then replaceValue(61) = " "
                        End If
                    Else
                        'Position 1 and 36 and 71 only
                        If replaceValue(0) = "-" Or replaceValue(0) = ":" Then
                            replaceValue(0) = " "
                        End If
                        If replaceValue.Length >= 36 Then
                            If replaceValue(35) = "-" Or replaceValue(35) = ":" Then replaceValue(35) = " "
                        End If
                        If replaceValue.Length >= 71 Then
                            If replaceValue(70) = "-" Or replaceValue(70) = ":" Then replaceValue(70) = " "
                        End If
                    End If
                    _previewRecord("Information To Remitting Bank") = replaceValue
                End If

                'Charges Account Number
                If _previewRecord("Bank Charges").ToString().Equals("Ben", StringComparison.CurrentCultureIgnoreCase) Or _
                            _previewRecord("Bank Charges").ToString().Equals("Beneficiary", StringComparison.CurrentCultureIgnoreCase) Then
                    _previewRecord("Charges Account Number") = ""

                ElseIf _previewRecord("Bank Charges").ToString().Equals("SHA", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("Share", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("OUR", StringComparison.CurrentCultureIgnoreCase) Or _
                        _previewRecord("Bank Charges").ToString().Equals("Applicant", StringComparison.CurrentCultureIgnoreCase) Then

                    _previewRecord("Charges Account Number") = IIf(_previewRecord("Charges Account Number").ToString().Equals(_previewRecord("Settlement Account No").ToString, _
                                StringComparison.CurrentCultureIgnoreCase), "", _previewRecord("Charges Account Number").ToString)
                End If



                '"Beneficiary Account No" (Change request after release)
                'fixed @ 04th Aug 2011
                'If hyphen(-) or colon(:) found at 1st position, MAGIC will replace it with a space.
                If _previewRecord("Beneficiary Account No").ToString() <> "" AndAlso _
                    (_previewRecord("Beneficiary Account No").ToString().StartsWith(":") Or _
                    _previewRecord("Beneficiary Account No").ToString().StartsWith("-")) Then

                    _previewRecord("Beneficiary Account No") = " " + _previewRecord("Beneficiary Account No").ToString().Substring(1)
                End If

                '# 'Option 9' field: replace ',' with ';'
                If _previewRecord("Option 9").ToString.Contains(",") Then
                    _previewRecord("Option 9") = _previewRecord("Option 9").ToString.Replace(",", ";")
                End If


                '# Replace '@' to 'AT' and '&' to 'AND' Non SWIFT Characters
                For i As Int32 = 0 To 19
                    _previewRecord(i) = _previewRecord(i).ToString().Replace("&", "AND")
                    _previewRecord(i) = _previewRecord(i).ToString().Replace("@", "AT")
                    'do not remove hyphen from non-text datatype fields (2,6,10,12,19)
                    Select Case i
                        Case 1, 5, 9, 11, 18
                            'skip
                        Case Else
                            _previewRecord(i) = RemoveHyphenColon(_previewRecord(i).ToString())
                    End Select

                Next


                '# Remove Unpermitted Characters (Change Request: fixed on 05 Sepc 2011)
                '"CUSTOMER REFERENCE"
                _previewRecord(2) = RemoveGCMSPlusInvalidCharacters(_previewRecord(2).ToString())

                '"FORWARD CONTRACT NUMBER", "INTERMEDIARY BANK/BRANCH/ADDRESS", "INTERMEDIARY BANK MASTER CODE", _
                '"BENEFICIARY BANK/BRANCH/ADDRESS", "BENEFICIARY BANK MASTER CODE", "BENEFICIARY ACCOUNT NO", _
                '"BENEFICIARY NAME/ADDRESS", "MESSAGE TO BENEFICIARY", "PURPOSE OF REMITTANCE", "INFORMATION TO REMITTING BANK"
                For i As Int32 = 7 To 16
                    _previewRecord(i) = RemoveGCMSPlusInvalidCharacters(_previewRecord(i).ToString())
                Next

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Data Type and Custom Validations other than Data Length and Mandatory Field Validations
    Protected Overrides Sub Validate()

        Try
            Dim regEmail As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled)
            Dim emails() As String
            Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
            Dim RowNumber As Int32 = 0
            Dim _fieldValue, expandedCountryName As String
            'For Specific country validations
            Dim ListCountry() As String
            Dim i As Integer
            Dim isValidCurrency, isValidSettlementACNo As Boolean


            ListCountry = "".Split("|")
            expandedCountryName = ""
            If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
                If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                    ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                    expandedCountryName = ListCountry(0).ToUpper()
                    System.Array.Sort(ListCountry)
                End If
            End If


            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'reset value for 'Exchange Method' and 'Forward Contract Number' validations
                isValidCurrency = True
                isValidSettlementACNo = True

                '#1. Settlement A/C No.
                If _previewRecord("Settlement Account No").ToString() <> "" Then
                    _fieldValue = _previewRecord("Settlement Account No").ToString()
                    If regAllowAlphaNum.IsMatch(_previewRecord("Settlement Account No").ToString()) Then
                        AddValidationError("Settlement Account No", _PreviewTable.Columns("Settlement Account No").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Settlement Account No") _
                                           , RowNumber + 1, True)
                    End If

                    If _fieldValue(0) = ":" Or _fieldValue(0) = "-" Then
                        AddValidationError("Settlement Account No", _PreviewTable.Columns("Settlement Account No").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not start with Colon(:) or hyphen(-).", RowNumber + 1, "Settlement Account No") _
                                           , RowNumber + 1, True)
                    End If
                    'Settlement A/C currency is valid or not (check against the default currency value list)
                    'If _previewRecord("Settlement Account No").ToString.Length < 7 Then
                    '    Dim validationError As New FileFormatError()
                    '    validationError.ColumnName = "Settlement Account No"
                    '    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account No").Ordinal + 1
                    '    validationError.Description = String.Format("Record: {0} Field: 'Settlement Account No' is not a valid 'Settlement Account No'. Data length is too short to verify the Currency Code.", RowNumber + 1)
                    '    validationError.RecordNo = RowNumber + 1
                    '    _ValidationErrors.Add(validationError)
                    '    _ErrorAtMandatoryField = True
                    '    isValidSettlementACNo = False
                    'Else
                    '    'Check for SMILE first
                    '    'Check for OVS again
                    '    'OVS needs to check the currency code at positions 5-7, so minimum length required is 7
                    '    If Not IsValidSettlementAC(_previewRecord("Settlement Account No").ToString.Substring(_previewRecord("Settlement Account No").ToString.Length - 3, 3)) _
                    '        AndAlso Not IsValidSettlementAC(_previewRecord("Settlement Account No").ToString.Substring(4, 3)) Then

                    '        Dim validationError As New FileFormatError()
                    '        validationError.ColumnName = "Settlement Account No"
                    '        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account No").Ordinal + 1
                    '        validationError.Description = String.Format("Record: {0} Field: 'Settlement Account No' is not a valid 'Settlement Account No'. Currency Code in 'Settlement Account No' is invalid.", RowNumber + 1)
                    '        validationError.RecordNo = RowNumber + 1
                    '        _ValidationErrors.Add(validationError)
                    '        _ErrorAtMandatoryField = True
                    '        isValidSettlementACNo = False
                    '    End If
                    'End If

                End If

                '#2. Value Date
                Dim _ValueDate As Date
                If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then

                    If _ValueDate < Date.Today Or _ValueDate >= Date.Today.AddDays(21) Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Value Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be equal to or greater than today and less than 21 calendar days from today's date.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                Else

                    Dim validationError As New FileFormatError()
                    validationError.ColumnName = "Value Date"
                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                    validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be a valid date.", RowNumber + 1)
                    validationError.RecordNo = RowNumber + 1
                    _ValidationErrors.Add(validationError)
                    _ErrorAtMandatoryField = True

                End If

                '#3. Customer Reference
                If _previewRecord("Customer Reference").ToString() <> "" Then
                    _fieldValue = _previewRecord("Customer Reference").ToString()
                    If _fieldValue(0) = "/" Or _fieldValue(_fieldValue.Length - 1) = "/" Then
                        AddValidationError("Customer Reference", _PreviewTable.Columns("Customer Reference").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not start or end with a single slash(/).", RowNumber + 1, "Customer Reference") _
                                           , RowNumber + 1, True)
                    End If

                    If _fieldValue.Contains("//") Then
                        AddValidationError("Customer Reference", _PreviewTable.Columns("Customer Reference").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Customer Reference") _
                                           , RowNumber + 1, True)
                    End If

                    If _fieldValue(0) = ":" Or _fieldValue(0) = "-" Then
                        AddValidationError("Customer Reference", _PreviewTable.Columns("Customer Reference").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not start with Colon(:) or hyphen(-).", RowNumber + 1, "Customer Reference") _
                                           , RowNumber + 1, True)
                    End If

                End If

                '#4. Payment Type/Sector Selection : Must be one of these values(Book Transfer, Domestic, International)
                If _previewRecord("Payment Type/Sector Selection").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Payment Type/Sector Selection").DefaultValue) Then
                        If TblBankFields("Payment Type/Sector Selection").DefaultValue.Length > 0 Then
                            Dim defaultSector() As String
                            defaultSector = TblBankFields("Payment Type/Sector Selection").DefaultValue.Split(",")
                            For Each val As String In defaultSector
                                If _previewRecord("Payment Type/Sector Selection").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Payment Type/Sector Selection"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Type/Sector Selection").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Payment Type/Sector Selection is not a valid 'Payment Type/Sector Selection'.", RowNumber + 1)
                                validationError.RecordNo = RowNumber + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                '#5. Currency : Must be one of the default values
                If _previewRecord("Currency").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid 'Currency'.", RowNumber + 1)
                                validationError.RecordNo = RowNumber + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                                'set value for 'Exchange Method' and 'Forward Contract No' checking
                                isValidCurrency = False
                            End If
                        End If
                    End If
                End If

                '#6. Remittance Amount 
                If Not IsConsolidated AndAlso _previewRecord("Remittance Amount").ToString() <> "" _
                                        AndAlso Val(_previewRecord("Remittance Amount").ToString()) <= 0 Then

                    Dim validationError As New FileFormatError()
                    validationError.ColumnName = "Remittance Amount"
                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Remittance Amount").Ordinal + 1
                    validationError.Description = String.Format("Record: {0} Field: 'Remittance Amount' should not be zero or negative.", RowNumber + 1)
                    validationError.RecordNo = RowNumber + 1
                    _ValidationErrors.Add(validationError)

                End If

                '#7. Exchange Method 
                If _previewRecord("Exchange Method").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Exchange Method").DefaultValue) Then
                        If TblBankFields("Exchange Method").DefaultValue.Length > 0 Then
                            Dim exchangeMethods() As String
                            exchangeMethods = TblBankFields("Exchange Method").DefaultValue.Split(",")
                            For Each val As String In exchangeMethods
                                If _previewRecord("Exchange Method").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Exchange Method", _PreviewTable.Columns("Exchange Method").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Exchange Method' is not a valid 'Exchange Method'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            Else
                                'FORMAT THE VALUE TO ALL CAPS
                                _previewRecord("Exchange Method") = _previewRecord("Exchange Method").ToString.ToUpper()
                            End If
                        End If
                    End If
                End If

                '#8. Exchange Method & Forward Contract Number
                If Not isValidCurrency Then 'And Not isValidSettlementACNo Then
                    'AddValidationError("Exchange Method", _PreviewTable.Columns("Exchange Method").Ordinal + 1 _
                    '                                                           , String.Format("Record: {0} Both values of 'Settlement Account No' and 'Currency' are invalid. Validation to 'Exchange method' and 'Forward Contract Number' cannot be performed.", RowNumber + 1) _
                    '                                                           , RowNumber + 1, True)
                    AddValidationError("Exchange Method", _PreviewTable.Columns("Exchange Method").Ordinal + 1 _
                                                                               , String.Format("Record: {0} Value of 'Currency' is invalid. Validation to 'Exchange method' and 'Forward Contract Number' cannot be performed.", RowNumber + 1) _
                                                                               , RowNumber + 1, True)
                Else
                    'If _previewRecord("Settlement Account No").ToString.Length < 7 Then

                    If _previewRecord("Forward Contract Number").ToString <> "" And _
                                   Not _previewRecord("Exchange Method").ToString.Equals("Cont", StringComparison.CurrentCultureIgnoreCase) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Exchange Method"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be cont.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                    If _previewRecord("Forward Contract Number").ToString() = "" And _
                        Not _previewRecord("Exchange Method").ToString.Equals("SPOT", StringComparison.CurrentCultureIgnoreCase) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Exchange Method"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be spot.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                    If _previewRecord("Forward Contract Number").ToString() <> "" Then
                        _fieldValue = _previewRecord("Forward Contract Number").ToString()
                        If _fieldValue(0) = "/" Or _fieldValue(_fieldValue.Length - 1) = "/" Then
                            AddValidationError("Forward Contract Number", _PreviewTable.Columns("Forward Contract Number").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' must not start or end with a single slash(/).", RowNumber + 1, "Forward Contract Number") _
                                               , RowNumber + 1, True)
                        End If

                        If _fieldValue.Contains("//") Then
                            AddValidationError("Forward Contract Number", _PreviewTable.Columns("Forward Contract Number").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Forward Contract Number") _
                                               , RowNumber + 1, True)
                        End If
                    End If
                End If

                '#9. Intermediary Bank/Branch/Address
                If _previewRecord("Intermediary Bank/Branch/Address").ToString() <> "" Then
                    _fieldValue = _previewRecord("Intermediary Bank/Branch/Address").ToString()
                    If _fieldValue.Contains("//") Then
                        AddValidationError("Intermediary Bank/Branch/Address", _PreviewTable.Columns("Intermediary Bank/Branch/Address").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Intermediary Bank/Branch/Address") _
                                           , RowNumber + 1, True)
                    End If

                    If _previewRecord("Intermediary Bank Name").ToString <> "" Then
                        If _previewRecord("Intermediary Bank Country").ToString = "" Then
                            AddValidationError("Intermediary Bank/Branch/Address", _PreviewTable.Columns("Intermediary Bank/Branch/Address").Ordinal + 1 _
                                                                       , String.Format("Record: {0} Field: '{1}' Intermediary Bank Country must be mapped.", RowNumber + 1, "Intermediary Bank/Branch/Address") _
                                                                       , RowNumber + 1, True)
                        End If
                    End If

                    'check swift code length
                    If _previewRecord("Intermediary Bank/Branch/Address").ToString.StartsWith("A:") Then
                        _fieldValue = _previewRecord("Intermediary Bank/Branch/Address").ToString()
                        If _fieldValue.Length <> 10 AndAlso _fieldValue.Length <> 13 Then
                            AddValidationError("Intermediary Bank/Branch/Address", _PreviewTable.Columns("Intermediary Bank/Branch/Address").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' SWIFT code must be exactly 8 or 11 characters.", RowNumber + 1, "Intermediary Bank/Branch/Address") _
                                               , RowNumber + 1, True)
                        End If
                    ElseIf _previewRecord("Intermediary Bank/Branch/Address").ToString.StartsWith("C:") Then
                        Dim nationalClearingCode As String = _previewRecord("Intermediary Bank/Branch/Address").ToString.Substring(2, 2)
                        Dim found As Boolean = False
                        For Each temp As String In nationalClearingCodes
                            If nationalClearingCode = temp Then
                                found = True
                            End If
                        Next
                        If Not found Then
                            AddValidationError("Intermediary Bank/Branch/Address", _PreviewTable.Columns("Intermediary Bank/Branch/Address").Ordinal + 1 _
                                                                           , String.Format("Record: {0} Field: '{1}' Invalid National Clearing Code.", RowNumber + 1, "Intermediary Bank/Branch/Address") _
                                                                           , RowNumber + 1, True)
                        End If
                    End If


                End If



                '#10. Intermediary Bank Master Code
                If _previewRecord("Intermediary Bank Master Code").ToString() <> "" Then
                    'check invalid char
                    If Not IsNumeric(_previewRecord("Intermediary Bank Master Code").ToString) Or Val(_previewRecord("Intermediary Bank Master Code").ToString) < 0 Then
                        AddValidationError("Intermediary Bank Master Code", _PreviewTable.Columns("Intermediary Bank Master Code").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must be numeric characters.", RowNumber + 1, "Intermediary Bank Master Code") _
                                           , RowNumber + 1, True)
                    End If
                    'check length
                    If _previewRecord("Intermediary Bank Master Code").Length <> 8 Then
                        AddValidationError("Intermediary Bank Master Code", _PreviewTable.Columns("Intermediary Bank Master Code").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must be exactly 8 characters.", RowNumber + 1, "Intermediary Bank Master Code") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#11. Beneficiary Bank/Branch/Address
                If _previewRecord("Beneficiary Bank/Branch/Address").ToString() <> "" Then
                    _fieldValue = _previewRecord("Beneficiary Bank/Branch/Address").ToString()
                    If _fieldValue.Contains("//") Then
                        AddValidationError("Beneficiary Bank/Branch/Address", _PreviewTable.Columns("Beneficiary Bank/Branch/Address").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Beneficiary Bank/Branch/Address") _
                                           , RowNumber + 1, True)
                    End If

                    If _previewRecord("Bene Bank Name").ToString <> "" Then
                        If _previewRecord("Bene Bank Country").ToString = "" Then
                            AddValidationError("Beneficiary Bank/Branch/Address", _PreviewTable.Columns("Beneficiary Bank/Branch/Address").Ordinal + 1 _
                                                                       , String.Format("Record: {0} Field: '{1}' Bene Bank Country must be mapped.", RowNumber + 1, "Beneficiary Bank/Branch/Address") _
                                                                       , RowNumber + 1, True)
                        End If
                    End If

                    'check swift code length
                    If _previewRecord("Beneficiary Bank/Branch/Address").ToString().StartsWith("A:") Then
                        _fieldValue = _previewRecord("Beneficiary Bank/Branch/Address").ToString()
                        If _fieldValue.Length <> 10 AndAlso _fieldValue.Length <> 13 Then
                            AddValidationError("Beneficiary Bank/Branch/Address", _PreviewTable.Columns("Beneficiary Bank/Branch/Address").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' SWIFT code must be exactly 8 or 11 characters.", RowNumber + 1, "Beneficiary Bank/Branch/Address") _
                                               , RowNumber + 1, True)
                        End If
                    ElseIf _previewRecord("Beneficiary Bank/Branch/Address").ToString().StartsWith("C:") Then
                        Dim nationalClearingCode As String = _previewRecord("Beneficiary Bank/Branch/Address").ToString.Substring(2, 2)
                        Dim found As Boolean = False
                        For Each temp As String In nationalClearingCodes
                            If nationalClearingCode = temp Then
                                found = True
                            End If
                        Next
                        If Not found Then
                            AddValidationError("Beneficiary Bank/Branch/Address", _PreviewTable.Columns("Beneficiary Bank/Branch/Address").Ordinal + 1 _
                                                                           , String.Format("Record: {0} Field: '{1}' Invalid National Clearing Code.", RowNumber + 1, "Beneficiary Bank/Branch/Address") _
                                                                           , RowNumber + 1, True)
                        End If
                    End If
                End If


                '#12. Beneficiary Bank Master Code
                If _previewRecord("Beneficiary Bank Master Code").ToString() <> "" Then
                    'check invalid char
                    If Not IsNumeric(_previewRecord("Beneficiary Bank Master Code").ToString) Or Val(_previewRecord("Beneficiary Bank Master Code").ToString) < 0 Then
                        AddValidationError("Beneficiary Bank Master Code", _PreviewTable.Columns("Beneficiary Bank Master Code").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must be numeric characters.", RowNumber + 1, "Beneficiary Bank Master Code") _
                                           , RowNumber + 1, True)
                    End If

                    If _previewRecord("Beneficiary Bank Master Code").Length <> 8 Then
                        AddValidationError("Beneficiary Bank Master Code", _PreviewTable.Columns("Beneficiary Bank Master Code").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must be exactly 8 characters.", RowNumber + 1, "Beneficiary Bank Master Code") _
                                           , RowNumber + 1, True)
                    End If
                End If

                '#13. Beneficiary Account No
                If _previewRecord("Beneficiary Account No").ToString() <> "" Then
                    If regAllowAlphaNum.IsMatch(_previewRecord("Beneficiary Account No").ToString()) Then
                        AddValidationError("Settlement Account No", _PreviewTable.Columns("Beneficiary Account No").Ordinal + 1 _
                                           , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Beneficiary Account No") _
                                           , RowNumber + 1, True)
                    End If

                    If _previewRecord("IBAN Flag").ToString() <> "" And _previewRecord("IBAN Flag").ToString = "1" Then

                        If _previewRecord("Beneficiary Account No").ToString().Length < 15 OrElse _previewRecord("Beneficiary Account No").ToString().Length > 31 Then
                            AddValidationError("Beneficiary Account No", _PreviewTable.Columns("Beneficiary Account No").Ordinal + 1 _
                                              , String.Format("Record: {0} Field: '{1}' length is invalid for 'IBAN Account No' checking.", RowNumber + 1, "Beneficiary Account No") _
                                              , RowNumber + 1, True)
                        ElseIf Not IsValidIBANAccount(_previewRecord("Beneficiary Account No").ToString()) Then
                            AddValidationError("Beneficiary Account No", _PreviewTable.Columns("Beneficiary Account No").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' must be a valid IBAN Account No when 'IBAN Flag' is 1.", RowNumber + 1, "Beneficiary Account No") _
                                               , RowNumber + 1, True)
                        End If
                    End If
                End If

                '#14. Beneficiary Name/Address
                If _previewRecord("Beneficiary Name/Address").ToString() <> "" Then
                    _fieldValue = _previewRecord("Beneficiary Name/Address").ToString()
                    If _fieldValue.Contains("//") Then
                        AddValidationError("Beneficiary Name/Address", _PreviewTable.Columns("Beneficiary Name/Address").Ordinal + 1 _
                                           , String.Format("Record: {0} Field: '{1}' must not contain double slashes(//).", RowNumber + 1, "Beneficiary Name/Address") _
                                           , RowNumber + 1, True)
                    End If

                    If _previewRecord("Bene Name").ToString <> "" Then
                        If _previewRecord("Bene Country").ToString = "" Then
                            AddValidationError("Beneficiary Name/Address", _PreviewTable.Columns("Beneficiary Name/Address").Ordinal + 1 _
                                                                       , String.Format("Record: {0} Field: '{1}' Bene Country must be mapped.", RowNumber + 1, "Beneficiary Name/Address") _
                                                                       , RowNumber + 1, True)
                        End If
                    End If
                End If

                '#15. Message To Beneficiary

                '#16. Purpose of Remittance (Malaysia only)
                'no more required (confirmed by casey, winson @ 14.6.2011)
                'If IsNothingOrEmptyString(_previewRecord("Purpose of Remittance").ToString()) AndAlso OutputFileName.Contains("MALAYSIA") Then
                '    Dim sampleAmount As Double
                '    sampleAmount = Convert.ToDouble(TblBankFields("Remittance Amount").BankSampleValue.ToString())
                '    If isPurposeRequired(_previewRecord("Currency").ToString(), Convert.ToDouble(_previewRecord("Remittance Amount").ToString()), sampleAmount) Then
                '        AddValidationError("Purpose of Remittance", _PreviewTable.Columns("Purpose of Remittance").Ordinal + 1 _
                '                           , String.Format("Record: {0} Field: '{1}' is required when Remittance Amount is greater than '{2}'MYR.", RowNumber + 1, "Purpose of Remittance", sampleAmount) _
                '                           , RowNumber + 1, True)
                '    End If
                'End If

                '#17. Information To Remitting Bank                
                If _previewRecord("Information To Remitting Bank").ToString() <> "" Then
                    _fieldValue = _previewRecord("Information To Remitting Bank").ToString()

                    'For Indonesia
                    If expandedCountryName.ToUpper().Contains("INDONESIA") Then
                        If Not IsNothingOrEmptyString(_previewRecord("Purpose of Remittance")) And _fieldValue.Length > 61 Then
                            AddValidationError("Information To Remitting Bank", _PreviewTable.Columns("Information To Remitting Bank").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' cannot contain more than 61 characters when 'Purpose of Remittance' is given.", RowNumber + 1, "Information To Remitting Bank") _
                                               , RowNumber + 1, True)
                        ElseIf IsNothingOrEmptyString(_previewRecord("Purpose of Remittance")) And _fieldValue.Length > 96 Then
                            AddValidationError("Information To Remitting Bank", _PreviewTable.Columns("Information To Remitting Bank").Ordinal + 1 _
                                               , String.Format("Record: {0} Field: '{1}' cannot contain more than 96 characters.", RowNumber + 1, "Information To Remitting Bank") _
                                               , RowNumber + 1, True)
                        End If

                    Else
                        'For Other Countries
                        If Not IsNothingOrEmptyString(_previewRecord("Purpose of Remittance")) Then
                            '�Information To Remitting Bank� cannot contain more than 70 characters when �Purpose of Remittance� is filled
                            If _fieldValue.Length > 70 Then
                                AddValidationError("Information To Remitting Bank", _PreviewTable.Columns("Information To Remitting Bank").Ordinal + 1 _
                                                   , String.Format("Record: {0} Field: '{1}' cannot contain more than 70 characters when 'Purpose of Remittance' is given.", RowNumber + 1, "Information To Remitting Bank") _
                                                   , RowNumber + 1, True)
                            End If

                        Else
                            '�Information To Remitting Bank� cannot contain more than 105 characters when �Purpose of Remittance� is NOT filled.
                            If _fieldValue.Length > 105 Then
                                AddValidationError("Information To Remitting Bank", _PreviewTable.Columns("Information To Remitting Bank").Ordinal + 1 _
                                                   , String.Format("Record: {0} Field: '{1}' cannot contain more than 105 characters.", RowNumber + 1, "Information To Remitting Bank") _
                                                   , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#18. "Bank Charges" 
                If _previewRecord("Bank Charges").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Bank Charges").DefaultValue) Then
                        If TblBankFields("Bank Charges").DefaultValue.Length > 0 Then
                            Dim temp() As String
                            temp = TblBankFields("Bank Charges").DefaultValue.Split(",")
                            For Each val As String In temp
                                If _previewRecord("Bank Charges").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Bank Charges", _PreviewTable.Columns("Bank Charges").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Bank Charges' is not a valid 'Bank Charges'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            End If
                        End If
                    End If

                    If expandedCountryName.ToUpper().Contains("INDONESIA") And _previewRecord("Payment Type/Sector Selection").ToString() <> "" Then
                        Select Case _previewRecord("Payment Type/Sector Selection").ToString().ToUpper()
                            Case "BOOK TRANSFER", "DOMESTIC", "1", "2"
                                If Not _previewRecord("Bank Charges").ToString().ToUpper().Equals("OUR") AndAlso Not _previewRecord("Bank Charges").ToString().ToUpper().Equals("APPLICANT") Then
                                    AddValidationError("Bank Charges", _PreviewTable.Columns("Bank Charges").Ordinal + 1 _
                                                   , String.Format("Record: {0} Field: '{1}' must be 'OUR' or 'Applicant' when 'Payment Type/Sector Selection' is 'Book Transfer' or 'Domestic'.", RowNumber + 1, "Bank Charges") _
                                                   , RowNumber + 1, True)
                                End If

                            Case Else

                        End Select
                    End If
                End If

                '#19. "Charges Account Number"
                If regAllowAlphaNum.IsMatch(_previewRecord("Charges Account Number").ToString()) Then
                    AddValidationError("Charges Account Number", _PreviewTable.Columns("Charges Account Number").Ordinal + 1 _
                                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Charges Account Number") _
                                       , RowNumber + 1, True)
                End If

                If (_previewRecord("Bank Charges").ToString().Equals("BEN", StringComparison.CurrentCultureIgnoreCase) _
                    Or _previewRecord("Bank Charges").ToString().Equals("Beneficiary", StringComparison.CurrentCultureIgnoreCase) _
                    ) And _previewRecord("Charges Account Number").ToString() <> "" Then

                    Dim validationError As New FileFormatError()
                    validationError.ColumnName = "Charges Account Number"
                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Charges Account Number").Ordinal + 1
                    validationError.Description = String.Format("Record: {0}, Field: 'Charges Account Number' should be blank if 'Bank Charges' is 'BEN'.", RowNumber + 1)
                    validationError.RecordNo = RowNumber + 1
                    _ValidationErrors.Add(validationError)
                    _ErrorAtMandatoryField = True
                End If

                '#20. "IBAN Flag"
                If _previewRecord("IBAN Flag").ToString() <> "" And _previewRecord("IBAN Flag").ToString() <> "1" Then
                    AddValidationError("IBAN Flag", _PreviewTable.Columns("IBAN Flag").Ordinal + 1 _
                                       , String.Format("Record: {0} Field: '{1}' is not a valid '{1}'.", RowNumber + 1, "IBAN Flag") _
                                       , RowNumber + 1, True)
                End If

                '#21. "Option 1" 
                If _previewRecord("Option 1").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Option 1").DefaultValue) Then
                        If TblBankFields("Option 1").DefaultValue.Length > 0 Then
                            Dim temp() As String
                            temp = TblBankFields("Option 1").DefaultValue.Split(",")
                            For Each val As String In temp
                                If _previewRecord("Option 1").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Option 1", _PreviewTable.Columns("Option 1").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Option 1' is not a valid 'Option 1'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#22. "Option 2" 
                If _previewRecord("Option 2").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Option 2").DefaultValue) Then
                        If TblBankFields("Option 2").DefaultValue.Length > 0 Then
                            Dim temp() As String
                            temp = TblBankFields("Option 2").DefaultValue.Split(",")
                            For Each val As String In temp
                                If _previewRecord("Option 2").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Option 2", _PreviewTable.Columns("Option 2").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Option 2' is not a valid 'Option 2'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#23. "Option 3" 
                If _previewRecord("Option 3").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Option 3").DefaultValue) Then
                        If TblBankFields("Option 3").DefaultValue.Length > 0 Then
                            Dim temp() As String
                            temp = TblBankFields("Option 3").DefaultValue.Split(",")
                            For Each val As String In temp
                                If _previewRecord("Option 3").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Option 3", _PreviewTable.Columns("Option 3").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Option 3' is not a valid 'Option 3'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#24. "Option 4" 
                If _previewRecord("Option 4").ToString() <> "" Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Option 4").DefaultValue) Then
                        If TblBankFields("Option 4").DefaultValue.Length > 0 Then
                            Dim temp() As String
                            temp = TblBankFields("Option 4").DefaultValue.Split(",")
                            For Each val As String In temp
                                If _previewRecord("Option 4").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                AddValidationError("Option 4", _PreviewTable.Columns("Option 4").Ordinal + 1 _
                                                           , String.Format("Record: {0} Field: 'Option 4' is not a valid 'Option 4'.", RowNumber + 1) _
                                                           , RowNumber + 1, True)
                            End If
                        End If
                    End If
                End If

                '#29. Option 9(E-mail Address)
                If _previewRecord("Option 9") <> "" Then 'valid internet email address                  

                    'check double/triple @ signs in email address
                    If _previewRecord("Option 9").ToString().Split(";").Length > 2 Or _previewRecord("Option 9").ToString().Split(",").Length > 2 Then
                        AddValidationError("Option 9", _PreviewTable.Columns("Option 9").Ordinal + 1 _
                                         , String.Format("Record: {0} Field: '{1}' should not include more than two email addresses.", RowNumber + 1, "Option 9") _
                                          , RowNumber + 1, True)
                    End If

                    If _previewRecord("Option 9").ToString().Split(";").Length > 1 Or _previewRecord("Option 9").ToString().Split(",").Length > 1 Then
                        'split emails to check individual validation
                        emails = _previewRecord("Option 9").ToString().Split(";")
                        For i = 0 To emails.Length - 1
                            If Not regEmail.IsMatch(emails(i)) OrElse emails(i).IndexOf("@@") > 0 Then
                                AddValidationError("Option 9", _PreviewTable.Columns("Option 9").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "Option 9") _
                                                        , RowNumber + 1, True)
                                Exit For
                            End If
                        Next

                    Else
                        If Not regEmail.IsMatch(_previewRecord("Option 9").ToString()) OrElse _previewRecord("Option 9").ToString().IndexOf("@@") > 0 Then
                            AddValidationError("Option 9", _PreviewTable.Columns("Option 9").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "Option 9") _
                                                    , RowNumber + 1, True)
                        End If

                    End If
                End If

                'For all fields
                'Validate unpermitted characters
                Dim regAllow As New Regex("[^0-9A-Za-z/?(),.'+:\-\s]", RegexOptions.Compiled)
                For Each col As String In TblBankFields.Keys
                    'No need to validate for these fields (TMEP FIELDS)
                    'CR #2: replace with a space for certain fields (start from customer ref)
                    '"BENEFICIARY NAME/ADDRESS", "MESSAGE TO BENEFICIARY", "PURPOSE OF REMITTANCE", "CUSTOMER REFERENCE",
                    Select Case col.ToUpper
                        Case "TEMP1", "TEMP2", "LOCAL COUNTRY", "LOCAL CURRENCY", "CONSOLIDATE FIELD", "OPTION 9", _
                             "BENE BANK NAME", "BENE BANK ADDRESS", "BENE BANK COUNTRY", "INTERMEDIARY BANK NAME", _
                             "INTERMEDIARY BANK ADDRESS", "INTERMEDIARY BANK COUNTRY", "BENE NAME", "BENE ADDRESS", _
                             "BENE COUNTRY", "BENE SWIFT BIC", "INTM SWIFT BIC", _
                             "FORWARD CONTRACT NUMBER", "INTERMEDIARY BANK/BRANCH/ADDRESS", "INTERMEDIARY BANK MASTER CODE", _
                             "BENEFICIARY BANK/BRANCH/ADDRESS", "BENEFICIARY BANK MASTER CODE", "BENEFICIARY ACCOUNT NO", _
                             "INFORMATION TO REMITTING BANK"
                            Continue For
                    End Select

                    If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(col).ToString) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = col
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid character.", RowNumber + 1, col)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                Next

                RowNumber = RowNumber + 1

            Next

        Catch ex As OverflowException
            Throw New Exception("Error on IBAN Validation : " & ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on validation : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' #10. Fill Sector Selection 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GenerateSectorSelection()

        Dim ListTemp1() As String, ListTemp2() As String
        Dim ListCountry() As String, ListLocalCurrency() As String
        Dim matchCurrency As Boolean, matchCountry As Boolean, tem1 As Boolean, tem2 As Boolean
        Dim expandedCountryName As String = ""

        Try
            If ((TblBankFields.ContainsKey("Temp1") Or TblBankFields.ContainsKey("Temp2")) _
                    And TblBankFields.ContainsKey("Payment Type/Sector Selection")) Then

                ListTemp1 = "".Split("|")
                ListTemp2 = "".Split("|")
                ListCountry = "".Split("|")
                ListLocalCurrency = "".Split("|")

                If TblBankFields.ContainsKey("Temp1") Then 'Get "Temp1" Data Sample
                    If Not TblBankFields("Temp1").BankSampleValue Is Nothing Then

                        ListTemp1 = TblBankFields("Temp1").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp1)
                    End If
                End If

                If TblBankFields.ContainsKey("Temp2") Then 'Get "Temp2" Data Sample
                    If Not TblBankFields("Temp2").BankSampleValue Is Nothing Then
                        ListTemp2 = TblBankFields("Temp2").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp2)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
                    If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                        ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                        expandedCountryName = ListCountry(0).ToUpper()
                        System.Array.Sort(ListCountry)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Currency") Then 'Get "Currency" Data Sample
                    If Not TblBankFields("Local Currency").BankSampleValue Is Nothing Then
                        ListLocalCurrency = TblBankFields("Local Currency").BankSampleValue.Split("|")
                        System.Array.Sort(ListLocalCurrency)
                    End If
                End If

                Select Case expandedCountryName

                    Case "INDONESIA", "SINGAPORE", "MALAYSIA", "THAILAND", "AUSTRALIA", "PHILIPPINES"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            If _previewRecord("Payment Type/Sector Selection").ToString() = "" Then
                                tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                                tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                                If tem1 And tem2 Then
                                    _previewRecord("Payment Type/Sector Selection") = "Book Transfer"
                                Else
                                    matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0)
                                    matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                    _previewRecord("Payment Type/Sector Selection") = IIf(matchCurrency And matchCountry, "Domestic", "International")
                                End If
                            End If
                        Next


                    Case "VIETNAM"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            If _previewRecord("Payment Type/Sector Selection").ToString() = "" Then
                                tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                                tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                                If tem1 And tem2 Then
                                    _previewRecord("Payment Type/Sector Selection") = "Book Transfer"
                                Else
                                    matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                    _previewRecord("Payment Type/Sector Selection") = IIf(matchCountry, "Domestic", "International")
                                End If
                            End If
                        Next

                End Select

            End If ' end of Sector Selection Exists?

            'If _MasterTemplateList.IsFixed Then FormatSequenceNo()

        Catch ex As Exception
            Throw New Exception("Error on generating Sector Selection : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub FormatOutput()

        Dim RowNumber As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If TblBankFields.ContainsKey("Currency") Then
                    If _previewRecord("Remittance Amount").ToString.Trim <> String.Empty And _
                       IsNumeric(_previewRecord("Remittance Amount").ToString.Trim) Then
                        Select Case _previewRecord("Currency").ToString.ToUpper
                            Case "TND", "KWD", "BHD", "OMR"
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 3).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 0).ToString("###########0")
                            Case Else
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 2).ToString("###########0.00")
                        End Select
                    End If
                End If

                'Payment Type/Sector Selection
                If _previewRecord("Payment Type/Sector Selection").ToString.Equals("Book Transfer", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Payment Type/Sector Selection") = "Book Transfer"

                ElseIf _previewRecord("Payment Type/Sector Selection").ToString.Equals("Domestic", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Payment Type/Sector Selection") = "Domestic"

                ElseIf _previewRecord("Payment Type/Sector Selection").ToString.Equals("International", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Payment Type/Sector Selection") = "International"
                End If

                'Exchange Method
                If _previewRecord("Exchange Method").ToString.Equals("SPOT", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Exchange Method") = "SPOT"

                ElseIf _previewRecord("Exchange Method").ToString.Equals("CONT", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Exchange Method") = "CONT"
                End If

                'Bank Charges
                If _previewRecord("Bank Charges").ToString.Equals("SHA", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "SHA"

                ElseIf _previewRecord("Bank Charges").ToString.Equals("Share", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "Share"

                ElseIf _previewRecord("Bank Charges").ToString.Equals("OUR", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "OUR"

                ElseIf _previewRecord("Bank Charges").ToString.Equals("Applicant", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "Applicant"

                ElseIf _previewRecord("Bank Charges").ToString.Equals("BEN", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "BEN"

                ElseIf _previewRecord("Bank Charges").ToString.Equals("Beneficiary", StringComparison.InvariantCultureIgnoreCase) Then
                    _previewRecord("Bank Charges") = "Beneficiary"

                End If

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Conversion : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Generates File in GCMS Plus Format
    ''' </summary>
    ''' <param name="_userName">User ID that requested for File Generation</param>
    ''' <param name="_sourceFile">Name of Source File</param>
    ''' <param name="_outputFile">Name of Output File</param>
    ''' <param name="_objMaster">Master Template</param>
    ''' <returns>Boolean value determining whether the Generation of File is successful</returns>
    ''' <remarks></remarks>
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim splitter, terminator As Integer
        Dim enclosureCharacter As String
        Dim tempTable As New DataTable
        Dim i As Integer
        Try

            ' GCMS Plus format is Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New Exception("Invalid master Template")
            End If


            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                enclosureCharacter = ""
            Else
                enclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            If _splitFile And _outputFile.Contains("_") Then
                splitter = _outputFile.Substring(_outputFile.IndexOf("_") + 1, 1)
                terminator = IIf(_PreviewTable.Rows.Count < (splitter * 1000), _PreviewTable.Rows.Count, (splitter * 1000))
            Else
                terminator = _PreviewTable.Rows.Count
            End If


            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If rowNo < (splitter * 1000) - 1000 Then
                    'skip
                Else
                    line = ""
                    i = 0
                    For Each _column As String In TblBankFields.Keys

                        If TblBankFields(_column).Detail.ToUpper <> "NO" Then

                            'If Not (_column = "Temp1".ToUpper Or _
                            '        _column.ToUpper = "Temp2".ToUpper Or _
                            '        _column.ToUpper = "Temp3".ToUpper Or _
                            '        _column.ToUpper = "Local Country".ToUpper Or _
                            '        _column.ToUpper = "Local Currency".ToUpper) Then

                            'make sure to print out only the first 30 fields
                            If i < 30 Then
                                If _column.ToUpper = "Remittance Amount".ToUpper Then

                                    Dim amount As Decimal
                                    amount = Math.Abs(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString))
                                    Select Case _previewRecord("Currency").ToString.ToUpper
                                        Case "TND", "KWD", "BHD", "OMR"
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 3).ToString("###########0.000")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount, 3).ToString("###########0.000")
                                            End If
                                        Case "TWD", "JPY", "KRW", "VND", "CLP"
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount).ToString("###########0")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount).ToString("###########0")
                                            End If
                                        Case Else
                                            If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                                _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 2).ToString("###########0.00")
                                            Else
                                                _previewRecord("Remittance Amount") = Math.Round(amount, 2).ToString("###########0.00")
                                            End If
                                    End Select

                                End If

                                'added for consolidate field checking
                                If (Not _column.Equals("Consolidate Field")) Then
                                    If enclosureCharacter.Trim = String.Empty Then
                                        If line = "" Then
                                            line &= _previewRecord(_column).ToString
                                        Else
                                            line = line & "," & _previewRecord(_column).ToString
                                        End If
                                    Else
                                        If line = "" Then
                                            line = line & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                        Else
                                            line = line & "," & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                        End If
                                    End If
                                End If

                            End If

                        End If

                        'increase count
                        i += 1
                    Next
                    'End of Columns looping

                    If rowNo <> terminator - 1 Then
                        text &= line & vbNewLine
                    Else
                        text &= line
                    End If

                End If
                'End of printing out data rows checking

                rowNo += 1

                If rowNo > terminator - 1 Then Exit For
            Next

            Return SaveTextToFile(text, _outputFile)

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    ''' <summary>
    ''' Generates Consolidated Data
    ''' </summary>
    ''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    ''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    ''' <param name="AppendText">Text to append with Reference Fields</param>
    ''' <remarks></remarks>
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        If GroupByFields.Count = 0 Then Exit Sub

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If

            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''


        Dim filterString As String = String.Empty
        Dim filterList As New List(Of String)
        Dim remittanceAmt As Decimal = 0
        Dim referenceField(1) As String
        Dim rowIndex As Integer = 0

        Try

            _consolidatedDataView = _PreviewTable.DefaultView ' DataView to apply rowfilter
            _consolidatedData = New DataTable ' DataTable to hold only consolidated records
            ' Create the columns for the Consolidated DataTable
            For Each _column As DataColumn In _PreviewTable.Columns
                _consolidatedData.Columns.Add(_column.ColumnName, GetType(String))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '1. Filter out unique rows for given Filter Group
            For Each _row As DataRow In _PreviewTable.Rows

                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_row(_field).ToString()))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For

                If filterList.IndexOf(filterString) = -1 Then
                    filterList.Add(filterString)
                    filterList.Sort()
                End If

            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next


            '2. Set of fields to be aggregated/consolidated while applying the Grouping
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            For Each _filter As String In filterList

                _consolidatedDataView.RowFilter = String.Empty
                _consolidatedDataView.RowFilter = _filter

                If _consolidatedDataView.Count > 0 Then

                    ' Create the reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1) <> "" Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next
                    remittanceAmt = 0

                    For Each _viewRow As DataRowView In _consolidatedDataView
                        remittanceAmt += Convert.ToDecimal(_viewRow(_amountFieldName).ToString)
                        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                            If _referenceFields(intI, 1).Trim.Length <> 0 Then
                                referenceField(intI) = referenceField(intI) & _viewRow(_referenceFields(intI, 1).ToString) & ","
                            End If
                        Next
                    Next
                End If

                Dim consolidatedRow As DataRow
                consolidatedRow = _consolidatedData.NewRow
                _consolidatedDataView.Item(0)(_amountFieldName) = remittanceAmt.ToString

                For Each _column As String In TblBankFields.Keys
                    consolidatedRow(_column) = _consolidatedDataView(0)(_column)
                    consolidatedRow(_amountFieldName) = remittanceAmt.ToString
                    '#2. Format Remittance Amount 
                    If consolidatedRow(_amountFieldName).ToString <> String.Empty Then
                        Select Case consolidatedRow("Currency").ToString().ToUpper()
                            Case "TND", "KWD", "BHD", "OMR"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0")
                            Case Else
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.00")
                        End Select

                    End If
                Next

                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
                        consolidatedRow(_referenceFields(intI, 1)) = referenceField(intI).Substring(0, referenceField(intI).Length - 1)
                    End If
                Next

                _consolidatedData.Rows.Add(consolidatedRow)

            Next

            _totalRecordCount = _PreviewTable.Rows.Count
            If _consolidatedData.Rows.Count > 0 Then
                _consolidatedRecordCount = _consolidatedData.Rows.Count
                _PreviewTable.Rows.Clear()
                _PreviewTable = _consolidatedData.Copy
            Else
                _consolidatedRecordCount = 0
            End If
            ValidationErrors.Clear()
            GenerateHeader()
            GenerateTrailer()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()

        Catch ex As Exception
            Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
        End Try

    End Sub


    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If
                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If

            End If

            RowNumber += 1

        Next

    End Sub


#End Region

    'Added for Settlement A/C enhancement for OVS
    Private Function IsValidSettlementAC(ByVal cur As String) As Boolean
        Dim isFound As Boolean = False
        If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
            If TblBankFields("Currency").DefaultValue.Length > 0 Then
                Dim defaultCurrency() As String
                defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                For Each val As String In defaultCurrency
                    If cur.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                        isFound = True
                        Exit For
                    End If
                Next
            End If
        End If
        Return isFound

    End Function

    Private Function IsValidIBANAccount(ByVal AcNo As String) As Boolean
        Dim isValid As Boolean = False
        Dim copyAC As String
        Dim strAcNo As System.Text.StringBuilder
        Dim i As Integer = 0
        Dim initLetter As Char = Chr(65)


        '1. Check that the total IBAN length is correct as per the country. If not, the IBAN is invalid. 
        '2. Move the four initial characters to the end of the string. 
        '3. Replace each letter in the string with two digits, thereby expanding the string, where A=10, B=11, ..., Z=35. 
        '4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97.  

        '1. Ignore

        '2. move first 4 chars to the end of string
        copyAC = AcNo.Substring(4) + AcNo.Substring(0, 4)

        '3.Replace
        strAcNo = New System.Text.StringBuilder(copyAC)
        For i = 10 To 35
            strAcNo.Replace(initLetter.ToString(), i.ToString())
            initLetter = Chr(Asc(initLetter) + 1)
        Next

        '4. Check modulus
        Dim tempinteger As Long
        Dim tempstr As String
        Dim divstr As String
        tempstr = strAcNo.ToString() 'Convert_to_iban_numbers(myaccount)
        Do Until Len(tempstr) <= 5
            divstr = Left(tempstr, 5)
            If Len(tempstr) > 5 Then
                tempstr = Right(tempstr, Len(tempstr) - 5)
            End If
            tempinteger = Format(divstr)
            tempstr = Str(tempinteger Mod 97) + tempstr
        Loop
        tempinteger = Format(tempstr)
        If (tempinteger Mod 97) = 1 Then
            isValid = True
        Else
            isValid = False
        End If

        Return isValid

    End Function

    'Only for Malaysia template
    'Remittance Amount limit should be setup in Master template's Data Sample field
    Private Function isPurposeRequired(ByVal cur As String, ByVal Amt As Double, ByVal sampleAmt As Double) As Boolean

        If cur.Equals("MYR") AndAlso Convert.ToDouble(Amt) > sampleAmt Then
            isPurposeRequired = True
        End If

        isPurposeRequired = False
    End Function

    Private Sub PadTrailingSpaces(ByRef src As String, ByVal max_len As Integer)
        Dim i As Integer = 0
        Dim diffLen As Integer

        diffLen = max_len - src.Length
        For i = 1 To diffLen
            src = src + " "
        Next

    End Sub

    Function validate_iban(ByVal myaccount As String) As Boolean
        Dim tempinteger As Long
        Dim tempstr As String
        Dim divstr As String
        tempstr = myaccount 'Convert_to_iban_numbers(myaccount)
        Do Until Len(tempstr) <= 5
            divstr = Left(tempstr, 5)
            If Len(tempstr) > 5 Then
                tempstr = Right(tempstr, Len(tempstr) - 5)
            End If
            tempinteger = Format(divstr)
            tempstr = Str(tempinteger Mod 97) + tempstr
        Loop
        tempinteger = Format(tempstr)
        If (tempinteger Mod 97) = 1 Then
            validate_iban = True
        Else
            validate_iban = False
        End If
    End Function

End Class

 

 