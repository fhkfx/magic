''' <summary>
''' Encapsulates the Specific Features of OiRTMS RM Format
''' </summary>
''' <remarks></remarks>
Public Class OiRTMSRMFormat
    Inherits iRTMSFormat

    Private Shared _editableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", _
                                             "Value Date", "Account No. with BTMU", "Currency", "Amount", _
                                             "Beneficiary Name 1", "Beneficiary Name 2", "Beneficiary Attention", "Beneficiary Contact", _
                                             "Beneficiary Address 1", "Beneficiary Address 2", "Beneficiary Address 3", "Beneficiary Country", _
                                             "Beneficiary Account No.", "Beneficiary Bank Name", "Beneficiary Bank Code", _
                                             "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                             "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                             "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                             "Intermediary Bank Name", "Intermediary Bank Branch Name", _
                                             "Intermediary Bank Address 1", "Intermediary Bank Address 2", "Intermediary Bank Address 3", _
                                             "Intermediary Bank Country", "Intermediary Bank BIC", _
                                             "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                             "Customer Reference", "Charge Code", "Contract No", "Delivery Mode", "Pre-Advice Flag", _
                                             "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                             "Account Currency"}

    Private Shared _autoTrimmableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", _
                                                   "Currency", _
                                                   "Beneficiary Name 1", "Beneficiary Name 2", "Beneficiary Attention", "Beneficiary Contact", _
                                                   "Beneficiary Address 1", "Beneficiary Address 2", "Beneficiary Address 3", "Beneficiary Country", _
                                                   "Beneficiary Bank Name", "Beneficiary Bank Code", _
                                                   "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                                   "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                                   "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                   "Intermediary Bank Name", "Intermediary Bank Branch Name", _
                                                   "Intermediary Bank Address 1", "Intermediary Bank Address 2", "Intermediary Bank Address 3", _
                                                   "Intermediary Bank Country", "Intermediary Bank BIC", _
                                                   "Message to Beneficiary", "Message to Bank", _
                                                   "Customer Reference", "Charge Code", "Contract No", "Delivery Mode", "Pre-Advice Flag", _
                                                   "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice"}
#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "OiRTMSRM"
        End Get
    End Property

#End Region

    Public Shared ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Public Shared ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Public Shared Function OutputFormatString() As String
        Return "OiRTMSRM"
    End Function

End Class