Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the CWSFormat
''' </summary>
''' <remarks></remarks>
Public Class CWSFormat
    Inherits BaseFileFormat

    'Amount field is summed up while grouping records
    Private Shared _amountFieldName As String = "Invoice Amount"

    Private Shared _validgroupbyFields() As String = {"Beneficiary Code", "Payee Name"}

    Private Shared _validreferenceFields() As String = {"Invoice No."}

    Private Shared _editableFields() As String = {"Invoice No.", "Invoice Date"}

    Private Shared _autoTrimmableFields() As String = {"Record Type (H)", "Beneficiary Code", "Payee Name" _
                    , "Invoice No.", "Record Type (D)", "Record Type (T)"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "CWS"
        End Get
    End Property

#End Region


    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validreferenceFields
        End Get
    End Property
    Public Shared Function OutputFormatString() As String
        Return "CWS"
    End Function

#Region "Overriden Methods"

    Protected Overrides Sub GenerateHeader()

        _Header = String.Empty

        For Each bankField As MapBankField In TblBankFields.Values

            If bankField.Header Then

                If _Header <> "" Then _Header += ","

                _Header += _PreviewTable.Rows(0)(bankField.BankField).ToString()

            End If

        Next

    End Sub

    Protected Overrides Sub GenerateTrailer()

        _Trailer = String.Empty

        For Each bankfield As MapBankField In TblBankFields.Values

            If bankfield.Trailer Then

                If _Trailer <> "" Then _Trailer += ","

                _Trailer += _PreviewTable.Rows(0)(bankfield.BankField).ToString()

            End If

        Next

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        'Dim _rowIndex As Integer = 1

        For Each row As DataRow In _PreviewTable.Rows

            '   _previewRecord("No") = _rowIndex 'No - Sequence Number starts from 1

            If Decimal.TryParse(row("Invoice Amount").ToString(), Nothing) Then
                'row("Invoice Amount") = Convert.ToDecimal(row("Invoice Amount")).ToString("#0.00")
                row("Invoice Amount") = FormatAmount(Convert.ToDecimal(row("Invoice Amount").ToString))
            End If

            '  _rowIndex += 1

        Next

    End Sub

    Private Sub FormatAmount()

        For Each row As DataRow In _PreviewTable.Rows

            If row("Invoice Amount") <> "" AndAlso Decimal.TryParse(row("Invoice Amount").ToString(), Nothing) Then

                If _DataModel.IsAmountInCents Then row("Invoice Amount") = (Convert.ToDecimal(row("Invoice Amount")) / 100)

            End If

        Next

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        FormatAmount()

        Dim TotalChequeAmount As Decimal = 0
        Dim RefIDDate As String = Now.ToString("yyyyMMdd")
        Dim nxtRowIdx As Integer = 0
        Dim fmtRefID As String = String.Empty

        Dim dsPreview As New DataSet("PreviewDataSet")
        dsPreview.Tables.Add(_PreviewTable.Copy())
        Dim dhelper As New DataSetHelper(dsPreview)

        Dim tbloutput As DataTable = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0), _
            "Beneficiary Code, Payee Name, count{Beneficiary Code}=TotalInvPerDetail, sum{Invoice Amount}=ChequeAmt", _
            "", "Beneficiary Code, Payee Name", False, "ChequeAmt", TotalChequeAmount)

      

        If tbloutput.Rows.Count > 0 Then
            _PreviewTable.Rows(nxtRowIdx)("Total No. of Checks (H)") = tbloutput.Rows(tbloutput.Rows.Count - 1)("S.No.").ToString()
            _PreviewTable.Rows(nxtRowIdx)("Total Check Amount (H)") = FormatAmount(TotalChequeAmount.ToString)
            _PreviewTable.Rows(nxtRowIdx)("Total No. of Checks (T)") = tbloutput.Rows(tbloutput.Rows.Count - 1)("S.No.").ToString()
            _PreviewTable.Rows(nxtRowIdx)("Total Check Amount (T)") = FormatAmount(TotalChequeAmount.ToString)
        End If


        Dim intI As Int32 = 0
        Dim intJ As Int32 = 0
        Dim BCode As String = ""
        Dim PName As String = ""


        For intJ = 0 To _PreviewTable.Rows.Count - 1

            BCode = _PreviewTable.Rows(intJ)("Beneficiary Code").ToString
            PName = _PreviewTable.Rows(intJ)("Payee Name").ToString

            For intI = 0 To tbloutput.Rows.Count - 1
                If BCode = tbloutput.Rows(intI)("Beneficiary Code").ToString AndAlso _
                PName = tbloutput.Rows(intI)("Payee Name").ToString Then

                    _PreviewTable.Rows(intJ)("Total Invoice No.") = tbloutput.Rows(intI)("TotalInvPerDetail").ToString
                    _PreviewTable.Rows(intJ)("Check Amount") = FormatAmount(Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt").ToString))
                    _PreviewTable.Rows(intJ)("Total Invoice Amount") = FormatAmount(Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt").ToString))
                    _PreviewTable.Rows(intJ)("Customer Code (T)") = _PreviewTable.Rows(intJ)("Customer Code (H)")

                End If

            Next

        Next
        
        Dim Counter As Int32 = 1
        For intI = 0 To _PreviewTable.Rows.Count - 1
            If _PreviewTable.Rows(intI)("Reference ID (D)") <> "" Then Continue For
            BCode = _PreviewTable.Rows(intI)("Beneficiary Code").ToString
            PName = _PreviewTable.Rows(intI)("Payee Name").ToString
            fmtRefID = String.Format("{0}{1:00000000000000000}", RefIDDate, Counter)
            Counter += 1
            _PreviewTable.Rows(intI)("Reference ID (D)") = fmtRefID
            _PreviewTable.Rows(intI)("Reference ID (I)") = fmtRefID
            For intJ = intI + 1 To _PreviewTable.Rows.Count - 1

                If _PreviewTable.Rows(intJ)("Reference ID (D)") <> "" Then Continue For
                If BCode = _PreviewTable.Rows(intJ)("Beneficiary Code").ToString AndAlso _
                               PName = _PreviewTable.Rows(intJ)("Payee Name").ToString Then
                    _PreviewTable.Rows(intJ)("Reference ID (D)") = fmtRefID
                    _PreviewTable.Rows(intJ)("Reference ID (I)") = fmtRefID
                End If
            Next
        Next


        '''''''''''''''''''''''''''''''''''''''''''''''''
        Dim sortedPreviewTbl As DataView = _PreviewTable.DefaultView
        sortedPreviewTbl.Sort = "[Reference ID (D)] ASC"
        _PreviewTable = sortedPreviewTbl.ToTable("PreviewData")

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Try

            Dim RowNumber As Int32 = 0
            Dim dAmount As Decimal = 0

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                '#1. Value Date 
                Dim _ValueDate As Date
                If Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), "", TblBankFields("Invoice Date").DateFormat, _ValueDate, True) Then
                    AddValidationError("Invoice Date", _PreviewTable.Columns("Invoice Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Invoice Date"), RowNumber + 1, True)
                End If

                '#2. Check Amount
                If _previewRecord("Check Amount") <> "" Then
                    If Decimal.TryParse(_previewRecord("Check Amount"), dAmount) Then
                        'If dAmount <= 0 Then
                        '    AddValidationError("Check Amount", _PreviewTable.Columns("Check Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Check Amount"), RowNumber + 1, True)
                        'End If
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Check Amount", _PreviewTable.Columns("Check Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Check Amount") _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Else
                        AddValidationError("Check Amount", _PreviewTable.Columns("Check Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Check Amount"), RowNumber + 1, True)
                    End If
                End If

                '#2. Total Invoice No.
                If _previewRecord("Total Invoice No.") <> "" Then
                    If Not Decimal.TryParse(_previewRecord("Total Invoice No."), dAmount) Then
                        AddValidationError("Total Invoice No.", _PreviewTable.Columns("Total Invoice No.").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Total Invoice No."), RowNumber + 1, True)
                    End If
                End If

                '#3. Total Invoice Amount
                If _previewRecord("Total Invoice Amount") <> "" Then
                    If Decimal.TryParse(_previewRecord("Total Invoice Amount"), dAmount) Then
                        'If dAmount <= 0 Then
                        '    AddValidationError("Total Invoice Amount", _PreviewTable.Columns("Total Invoice Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Total Invoice Amount"), RowNumber + 1, True)
                        'End If
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Total Invoice Amount", _PreviewTable.Columns("Total Invoice Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Total Invoice Amount") _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Else
                        AddValidationError("Total Invoice Amount", _PreviewTable.Columns("Total Invoice Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Total Invoice Amount"), RowNumber + 1, True)
                    End If
                End If

                '#4. Invoice Amount
                If _previewRecord("Invoice Amount") <> "" Then
                    If Decimal.TryParse(_previewRecord("Invoice Amount"), dAmount) Then
                        If dAmount < 0 Then
                            AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09020070"), "Record", RowNumber + 1, "Invoice Amount"), RowNumber + 1, True)
                        End If
                        If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Invoice Amount") _
                                                    , RowNumber + 1, True)
                            End If
                        End If
                    Else
                        AddValidationError("Invoice Amount", _PreviewTable.Columns("Invoice Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Invoice Amount"), RowNumber + 1, True)
                    End If
                End If

                '5. Verify if the characters in values are in the range of Valid CWS Characters
                ' The allowed characters for any non-numeric fields are a-z, A-Z, 0-9,
                ' period(.) and space character( ).

                Dim pattern As String = "[^a-zA-Z0-9" & Regex.Escape(".") & "\-\s]"
                Dim regAllow As New Regex(pattern, RegexOptions.Compiled)

                For Each charField As String In New String() {"Customer Code (H)", "Customer Code (T)", "Beneficiary Code", "Payee Name", "Invoice No."}

                    If _previewRecord(charField).ToString().Trim().Length > 0 Then
                        If regAllow.IsMatch(_previewRecord(charField).ToString().Trim()) OrElse _previewRecord(charField).ToString.Contains(vbTab) Then

                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = charField
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(charField).Ordinal + 1

                            If TblBankFields(charField).Header AndAlso RowNumber = 0 Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020100"), "Header", "", charField)
                                validationError.RecordNo = 0
                            Else
                                validationError.Description = String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, charField)
                                validationError.RecordNo = RowNumber + 1
                            End If

                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True

                        End If
                    End If

                Next

                '6. The comma (,) delimiter is not allowed in any field value
                ' excluding  the field "Invoice Amount"
                pattern = ","
                regAllow = New Regex(pattern, RegexOptions.Compiled)

                For Each charField As String In TblBankFields.Keys

                    If charField = "Invoice Amount" Then Continue For

                    If _previewRecord(charField).ToString().Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(charField).ToString().Trim()) Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = charField
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(charField).Ordinal + 1

                        If TblBankFields(charField).Header AndAlso RowNumber = 0 Then
                            validationError.Description = String.Format(MsgReader.GetString("E09020101"), "Header", "", charField)
                            validationError.RecordNo = 0
                        Else
                            validationError.Description = String.Format(MsgReader.GetString("E09020101"), "Record", RowNumber + 1, charField)
                            validationError.RecordNo = RowNumber + 1
                        End If

                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                Next

                'Validate Total Check Amount (H) and Total Check Amount (T)
                If RowNumber = 0 Then
                    If _previewRecord("Total Check Amount (H)") <> "" Then
                        If Decimal.TryParse(_previewRecord("Total Check Amount (H)"), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Total Check Amount (H)", _PreviewTable.Columns("Total Check Amount (H)").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Header", "", "Total Check Amount (H)") _
                                                        , RowNumber, True)
                                End If
                            End If
                        Else
                            AddValidationError("Total Check Amount (H)", _PreviewTable.Columns("Total Check Amount (H)").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Header", "", "Total Check Amount (H)"), RowNumber, True)
                        End If
                    End If
                    If _previewRecord("Total Check Amount (T)") <> "" Then
                        If Decimal.TryParse(_previewRecord("Total Check Amount (T)"), dAmount) Then
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Total Check Amount (T)", _PreviewTable.Columns("Total Check Amount (T)").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Trailer", "", "Total Check Amount (T)") _
                                                        , RowNumber, True)
                                End If
                            End If
                        Else
                            AddValidationError("Total Check Amount (T)", _PreviewTable.Columns("Total Check Amount (T)").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Trailer", "", "Total Check Amount (T)"), RowNumber, True)
                        End If
                    End If
                End If
                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validation: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0
        Dim _groupbys As String = ""
        Dim _invoiceAmount As Decimal = 0
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")
        Dim TotalChequeAmount As Decimal = 0
        Dim RefIDDate As String = Now.ToString("yyyyMMdd")
        Dim nxtRowIdx As Integer = 0
        Dim fmtRefID As String = String.Empty
        Dim fmtCheqAmt As Decimal = 0

        '1. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        For Each _groupby As String In _groupByFields
            If _groupbys = "" Then
                _groupbys = _groupby
            Else
                _groupbys &= "," & _groupby
            End If
        Next


        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview


            If (_isNegativePaymentOption AndAlso _paymentOption = "+") Or (_isPositivePaymentOption AndAlso _paymentOption = "-") Then
                For Each row As DataRow In _PreviewTable.Rows
                    If row("Invoice Amount") <> "" AndAlso Decimal.TryParse(row("Invoice Amount").ToString(), _invoiceAmount) Then
                        row("Invoice Amount") = (_invoiceAmount * -1).ToString()
                    End If
                Next
            End If

            Dim tbloutput As DataTable = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0), _
                        IIf(_groupbys.Contains("Beneficiary Code"), " Beneficiary Code, ", "") & _
                        IIf(_groupbys.Contains("Payee Name"), " Payee Name, ", "") & _
                        "count{Beneficiary Code}=TotalInvPerDetail, sum{Invoice Amount}=ChequeAmt", _
                        "", _groupbys, False, "ChequeAmt", TotalChequeAmount)

            If tbloutput.Rows.Count > 0 Then
                _PreviewTable.Rows(nxtRowIdx)("Total No. of Checks (H)") = tbloutput.Rows(tbloutput.Rows.Count - 1)("S.No.").ToString()
                _PreviewTable.Rows(nxtRowIdx)("Total Check Amount (H)") = TotalChequeAmount.ToString("#0.00")
                _PreviewTable.Rows(nxtRowIdx)("Total No. of Checks (T)") = tbloutput.Rows(tbloutput.Rows.Count - 1)("S.No.").ToString()
                _PreviewTable.Rows(nxtRowIdx)("Total Check Amount (T)") = TotalChequeAmount.ToString("#0.00")
            End If


            Dim intI As Int32 = 0
            Dim intJ As Int32 = 0
            Dim BCode As String = ""
            Dim PName As String = ""

            For intJ = 0 To _PreviewTable.Rows.Count - 1

                BCode = _PreviewTable.Rows(intJ)("Beneficiary Code").ToString
                PName = _PreviewTable.Rows(intJ)("Payee Name").ToString

                For intI = 0 To tbloutput.Rows.Count - 1

                    If _groupbys.Contains("Beneficiary Code") AndAlso _groupbys.Contains("Payee Name") Then
                        If BCode = tbloutput.Rows(intI)("Beneficiary Code").ToString AndAlso _
                    PName = tbloutput.Rows(intI)("Payee Name").ToString Then

                            _PreviewTable.Rows(intJ)("Total Invoice No.") = tbloutput.Rows(intI)("TotalInvPerDetail").ToString
                            _PreviewTable.Rows(intJ)("Check Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Total Invoice Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Customer Code (T)") = _PreviewTable.Rows(intJ)("Customer Code (H)")

                            If _referenceFields.Count = 1 AndAlso _appendText.Count = 1 Then _PreviewTable.Rows(intJ)("Invoice No.") = _appendText.Item(0) & _PreviewTable.Rows(intJ)("Invoice No.").ToString

                        End If
                    ElseIf _groupbys.Contains("Beneficiary Code") Then
                        If BCode = tbloutput.Rows(intI)("Beneficiary Code").ToString Then

                            _PreviewTable.Rows(intJ)("Total Invoice No.") = tbloutput.Rows(intI)("TotalInvPerDetail").ToString
                            _PreviewTable.Rows(intJ)("Check Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Total Invoice Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Customer Code (T)") = _PreviewTable.Rows(intJ)("Customer Code (H)")

                            If _referenceFields.Count = 1 AndAlso _appendText.Count = 1 Then _PreviewTable.Rows(intJ)("Invoice No.") = _appendText.Item(0) & _PreviewTable.Rows(intJ)("Invoice No.").ToString

                        End If
                    ElseIf _groupbys.Contains("Payee Name") Then
                        If PName = tbloutput.Rows(intI)("Payee Name").ToString Then

                            _PreviewTable.Rows(intJ)("Total Invoice No.") = tbloutput.Rows(intI)("TotalInvPerDetail").ToString
                            _PreviewTable.Rows(intJ)("Check Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Total Invoice Amount") = Convert.ToDecimal(tbloutput.Rows(intI)("ChequeAmt")).ToString("#0.00")
                            _PreviewTable.Rows(intJ)("Customer Code (T)") = _PreviewTable.Rows(intJ)("Customer Code (H)")

                            If _referenceFields.Count = 1 AndAlso _appendText.Count = 1 Then _PreviewTable.Rows(intJ)("Invoice No.") = _appendText.Item(0) & _PreviewTable.Rows(intJ)("Invoice No.").ToString

                        End If
                    End If

                Next

            Next


            For Each row As DataRow In _PreviewTable.Rows
                row("Reference ID (D)") = ""
                row("Reference ID (I)") = ""
            Next
            Dim Counter As Int32 = 1
            For intI = 0 To _PreviewTable.Rows.Count - 1
                If _PreviewTable.Rows(intI)("Reference ID (D)") <> "" Then Continue For
                BCode = _PreviewTable.Rows(intI)("Beneficiary Code").ToString
                PName = _PreviewTable.Rows(intI)("Payee Name").ToString
                fmtRefID = String.Format("{0}{1:00000000000000000}", RefIDDate, Counter)
                Counter += 1
                _PreviewTable.Rows(intI)("Reference ID (D)") = fmtRefID
                _PreviewTable.Rows(intI)("Reference ID (I)") = fmtRefID
                For intJ = intI + 1 To _PreviewTable.Rows.Count - 1

                    If _PreviewTable.Rows(intJ)("Reference ID (D)") <> "" Then Continue For

                    If _groupbys.Contains("Beneficiary Code") AndAlso _groupbys.Contains("Payee Name") Then
                        If BCode = _PreviewTable.Rows(intJ)("Beneficiary Code").ToString AndAlso _
                                       PName = _PreviewTable.Rows(intJ)("Payee Name").ToString Then
                            _PreviewTable.Rows(intJ)("Reference ID (D)") = fmtRefID
                            _PreviewTable.Rows(intJ)("Reference ID (I)") = fmtRefID
                        End If
                    ElseIf _groupbys.Contains("Beneficiary Code") Then
                        If BCode = _PreviewTable.Rows(intJ)("Beneficiary Code").ToString Then
                            _PreviewTable.Rows(intJ)("Reference ID (D)") = fmtRefID
                            _PreviewTable.Rows(intJ)("Reference ID (I)") = fmtRefID
                        End If
                    ElseIf _groupbys.Contains("Payee Name") Then
                        If PName = _PreviewTable.Rows(intJ)("Payee Name").ToString Then
                            _PreviewTable.Rows(intJ)("Reference ID (D)") = fmtRefID
                            _PreviewTable.Rows(intJ)("Reference ID (I)") = fmtRefID
                        End If
                    End If



                Next
            Next

            Dim sortedPreviewTbl As DataView = _PreviewTable.DefaultView
            sortedPreviewTbl.Sort = "[Reference ID (D)] ASC"
            _PreviewTable = sortedPreviewTbl.ToTable("PreviewData")

            ValidationErrors.Clear()
            FormatOutput()
            GenerateHeader()
            GenerateTrailer()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            _totalRecordCount = _PreviewTable.Rows.Count
            _consolidatedRecordCount = _PreviewTable.Rows.Count

        Catch ex As Exception
            Throw New Exception("Error on Consolidating records: " & ex.Message.ToString)
        End Try


    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty

        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim colNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim sReferenceID As String = String.Empty

        Try
            ' CWS uses Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New MagicException("Invalid master Template")
            End If

            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                sEnclosureCharacter = ""
            Else
                sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            GenerateHeader()
            If _Header <> "" Then sText = sText & _Header & vbNewLine

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""
                colNo = 0

                For Each _column As String In TblBankFields.Keys

                    'Ignore Header/Trailer Columns
                    If colNo < 4 Or colNo > 14 Then
                        colNo += 1
                        Continue For
                    End If

                    If sReferenceID <> _previewRecord("Reference ID (D)").ToString() Then

                        sLine = sLine & IIf(sLine = "", "", IIf(colNo = 11, "", ",")) & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter

                        If colNo = 10 Then sLine = sLine & vbNewLine

                    Else

                        If colNo > 10 Then
                            sLine = sLine & IIf(sLine = "", "", IIf(colNo = 11, "", ",")) & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    End If

                    colNo += 1

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

                sReferenceID = _previewRecord("Reference ID (D)")

            Next

            GenerateTrailer()
            If _Trailer <> String.Empty Then sText = sText & vbNewLine & _Trailer

            Return SaveTextToFile(sText, _outputFile)

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    Private Function FormatAmount(ByVal amount As Decimal) As String

        Dim formattedAmount As String = String.Empty

        'Amount fields should not be formatted if there are more than 2 decimal places.
        'Total Check Amount(H)
        'Check Amount
        'Total Invoice Amount
        'Invoice Amount
        'Total Check Amount(T)


        If amount.ToString().IndexOf(".") > 0 Then 'Should not format if  more than 2 decimal places
            If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                formattedAmount = (amount).ToString("#0.00")
            Else
                formattedAmount = amount
            End If
        Else
            formattedAmount = (amount).ToString("#0.00")
        End If

        Return formattedAmount

    End Function
#End Region

End Class