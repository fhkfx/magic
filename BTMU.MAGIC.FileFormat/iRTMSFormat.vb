Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Common Features of iRTMS Formats
''' </summary>
''' <remarks></remarks>
Public MustInherit Class iRTMSFormat
    Inherits BaseFileFormat

    Private Shared _amountFieldName As String = "Amount"
#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    Protected Overrides Sub ValidateDataLength()

        Try
            Dim RowNumber As Int32 = 0
            Dim adviceRecords As New List(Of String)
            Dim adviceRecord As String = ""
            Dim intI As Integer = 0


            For Each _previewRecord As DataRow In _PreviewTable.Rows

                adviceRecords.Clear()

                For Each _column As String In TblBankFields.Keys

                    If _column = "Account Currency" Then Continue For

                    If _column = "Transaction Advice" AndAlso (Not IsNothingOrEmptyString(_previewRecord(_column))) Then

                        If _DataModel.IsAdviceRecordForEveryRow Then
                            adviceRecords.Add(_previewRecord(_column).ToString)
                        ElseIf _DataModel.IsAdviceRecordAfterChar Then
                            adviceRecord = _previewRecord(_column).ToString
                            While adviceRecord.Length > _DataModel.AdviceRecordChar
                                adviceRecords.Add(adviceRecord.Substring(0, _DataModel.AdviceRecordChar))
                                adviceRecord = adviceRecord.Remove(0, _DataModel.AdviceRecordChar)
                            End While
                            If adviceRecord.Length > 0 Then adviceRecords.Add(adviceRecord)
                        ElseIf _DataModel.IsAdviceRecordDelimited Then
                            adviceRecords.AddRange(_previewRecord(_column).ToString.Split(_DataModel.AdviceRecordDelimiter))
                        End If

                        For Each adviceRecord In adviceRecords
                            If adviceRecord.Length > TblBankFields(_column).DataLength.Value Then
                                AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1, String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value), RowNumber + 1)
                            End If
                        Next
                    Else
                        'modified during UAT for iRTMS Enhancment
                        'If the amount value doesn't have decimal places, the maximum data lenght is 12.
                        'If the amount has decimal places, max data length is 15. (12,2)
                        '@15/03/2011 by Kay

                        If _column.Equals(AmountFieldName) And Not _previewRecord(_column).ToString().Contains(".") Then
                            'length-3= decimal point + 2 decimal places
                            If _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value - 3 Then
                                AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1, String.Format(MsgReader.GetString("E09020051"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value - 3), RowNumber + 1)
                            End If

                        ElseIf _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value Then

                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1, String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value), RowNumber + 1)

                            'Certain Fileformats shall not set the mandatoryerror flag when their Amount Value exceeds predefined size
                            If Not _column.Equals(AmountFieldName, StringComparison.InvariantCultureIgnoreCase) Then
                                If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True
                            End If

                        End If

                        End If

                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Data Length: " & ex.Message.ToString)
        End Try

    End Sub

    'Validations Common to all O/iRTMSFormats + Overridable CustomiRTMSValidate()
    Protected Overrides Sub Validate()

        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim _ValueDate As Date

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            '1 Value Date must be valid date
            If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then

                '1. Value Date must be >= to Today's Date.
                If _ValueDate.CompareTo(Date.Today) < 0 Then

                    AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1 _
                    , String.Format(MsgReader.GetString("E09030070"), "Record", RowNumber + 1, "Value Date") _
                    , RowNumber + 1, True)

                End If

            Else

                AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1 _
                , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Value Date") _
                , RowNumber + 1, True)

            End If


            '2. Amount : Must be numeric
            If _previewRecord(_amountFieldName) <> "" Then
                If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then
                    'If Not IsConsolidated AndAlso dAmount <= 0 Then
                    '    AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1)
                    'End If

                    'Added this format checking for iRTMS-RM enhancement
                    'IDR currency code allows 2 decimal places
                    'changes made @ 09/03/2011
                    If Not IsNothingOrEmptyString(_previewRecord("Currency")) And FileFormatName.ToUpper() = "iRTMSRM".ToUpper() Then
                        Select Case _previewRecord("Currency")
                            Case "JPY", "KRW", "TWD"
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have decimal places
                                    AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050072"), "Record", RowNumber + 1, "Amount") _
                                                        , RowNumber + 1, True)

                                End If
                            Case Else
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Amount") _
                                                            , RowNumber + 1, True)
                                    End If
                                End If
                        End Select
                    Else
                        Select Case _previewRecord("Currency")
                            Case "JPY", "IDR"
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have decimal places
                                    AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050072"), "Record", RowNumber + 1, "Amount") _
                                                        , RowNumber + 1, True)

                                End If
                            Case Else
                                If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                    If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                        AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1 _
                                                            , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Amount") _
                                                            , RowNumber + 1, True)
                                    End If
                                End If
                        End Select
                    End If

                Else
                    AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                End If
            End If

            '3. Field value should be an ascii value
            For Each _column As String In TblBankFields.Keys

                If ContainsNonASCIIChar(_previewRecord(_column)) Then

                    AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                    , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, _column) _
                    , RowNumber + 1, True)

                End If
            Next


            RowNumber += 1

        Next

        'Call Additional O/iRTMS Validations, if any, implemented by derivatives
        CustomiRTMSValidate()

    End Sub

    Protected Overridable Sub CustomiRTMSValidate()
        'O/iRTMS Derivative Formats can perform additional validations by overriding this method
    End Sub

    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub FormatOutput()

        Dim _amount As Decimal

        For Each _previewrecord As DataRow In _PreviewTable.Rows

            If Not Decimal.TryParse(_previewrecord("Amount").ToString, _amount) Then Continue For
            If _DataModel.ThousandSeparator <> "" Then _previewrecord("Amount") = _previewrecord("Amount").ToString.Replace(_DataModel.ThousandSeparator, "")
            _previewrecord("Amount") = FormatAmount(_previewrecord("Currency"), _amount)

        Next

    End Sub

    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = 0

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                _rowIndex += 1
                If Not IsNothingOrEmptyString(_previewRecord("Amount")) Then
                    If Decimal.TryParse(_previewRecord("Amount").ToString(), Nothing) Then
                        If _DataModel.IsAmountInCents Then _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex, " Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex, " Amount"))
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        ' Regarding "Sum up Remittance Amount" Checkbox in Consolidation Screen.
        ' If Checked, the individual amounts will be summed up during consolidation.
        ' If unchecked, the amount taken for the consolidated record will be the amount of the last transaction record in each group.
        ' Change request for uncheck case: since magic version 3.14, amount value will be taken from the "first" row (btmu-casey-requested @ 27 Apr 2011)
        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0
        Dim _consolidatedData As DataTable

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Amount"))
            If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            If IsCheckedSumAmount Then
                _consolidatedData = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                                    , _groupByFields, _referenceFields, _appendText, "Amount")
            Else
                _consolidatedData = dhelper.SelectGroupByInto4("output", dsPreview.Tables(0) _
                                                                    , _groupByFields, _referenceFields, _appendText, "Amount")
            End If

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView

            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            GenerateHeader()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()

        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sLine As String = "" ' Store one line of data
        Dim sText As String = "" ' Store the entire file content
        Dim colNo As Integer = 0
        Dim adviceRecords As New List(Of String)
        Dim adviceRecord As String = ""
        Dim objMasterTemplate As MasterTemplateNonFixed
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If


        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            adviceRecords.Clear()
            colNo = 0
            sLine = ""

            For Each _column As String In TblBankFields.Keys

                ' Take the absolute value of Amount
                If _column = "Amount" Then
                    '_previewRecord(_column) = Math.Abs(Convert.ToDecimal(_previewRecord(_column).ToString))
                    _previewRecord(_column) = FormatAmount(_previewRecord("Currency"), Convert.ToDecimal(_previewRecord(_column).ToString))
                End If

                If _column = "Account Currency" Then Continue For ' Account Currency will not be output to file.

                ' This section creates the Transaction Records
                If TblBankFields(_column).Detail = "Transaction" Then

                    'To exclude Consolidation Field
                    If (Not _column.Equals("Consolidate Field")) Then
                        sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString & sEnclosureCharacter
                    End If

                ElseIf TblBankFields(_column).Detail = "Advice" Then

                    If _column = "Serial No. AR" AndAlso Not IsNothingOrEmptyString(_previewRecord("Transaction Advice")) Then

                        ' This section creates the Advice Records
                        If _DataModel.IsAdviceRecordForEveryRow Then
                            adviceRecords.Add(_previewRecord("Transaction Advice").ToString)
                        ElseIf _DataModel.IsAdviceRecordAfterChar Then
                            adviceRecord = _previewRecord("Transaction Advice").ToString
                            While adviceRecord.Length > _DataModel.AdviceRecordChar
                                adviceRecords.Add(adviceRecord.Substring(0, _DataModel.AdviceRecordChar))
                                adviceRecord = adviceRecord.Remove(0, _DataModel.AdviceRecordChar)
                            End While
                            If adviceRecord.Length > 0 Then adviceRecords.Add(adviceRecord)
                        ElseIf _DataModel.IsAdviceRecordDelimited Then
                            adviceRecords.AddRange(_previewRecord("Transaction Advice").ToString.Split(_DataModel.AdviceRecordDelimiter))
                        End If

                        For Each adviceRecord In adviceRecords

                            sLine = sLine & vbNewLine

                            'Bug found in generating Advice records (no. of columns to be hidden should be 4)
                            For col As Integer = colNo To _PreviewTable.Columns.Count - 4 '3
                                sLine = sLine & IIf(col = colNo, "", ",") & sEnclosureCharacter & _previewRecord(col).ToString & sEnclosureCharacter

                            Next
                            sLine = sLine & "," & sEnclosureCharacter & adviceRecord & sEnclosureCharacter

                        Next

                    End If

                End If

                colNo += 1

            Next

            sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)
            rowNo += 1

        Next

        Return SaveTextToFile(sText, _outputFile)

    End Function


    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If

                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If

            End If

            RowNumber += 1

        Next

    End Sub


#End Region

#Region "Helper Methods"

    Private Function FormatAmount(ByVal currency As String, ByVal amount As Decimal) As String

        Dim formattedAmount As String = String.Empty

        'Added this format checking for iRTMS-RM enhancement
        'IDR currency code allows 2 decimal places
        'changes made @ 09/03/2011
        If FileFormatName.ToUpper() = "iRTMSRM".ToUpper() Then
            Select Case currency
                Case "JPY", "KRW", "TWD"

                    If amount.ToString().IndexOf(".") = 0 Then 'Should not have decimal places
                        formattedAmount = (amount).ToString("###########0")
                    Else
                        formattedAmount = amount
                    End If

                Case Else
                    If amount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                        If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                            formattedAmount = (amount).ToString("###########0.00")
                        Else
                            formattedAmount = amount
                        End If
                    Else
                        formattedAmount = (amount).ToString("###########0.00")
                    End If

            End Select
        Else
            Select Case currency
                Case "JPY", "IDR"

                    If amount.ToString().IndexOf(".") = 0 Then 'Should not have decimal places
                        formattedAmount = (amount).ToString("###########0")
                    Else
                        formattedAmount = amount
                    End If

                Case Else
                    If amount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                        If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                            formattedAmount = (amount).ToString("###########0.00")
                        Else
                            formattedAmount = amount
                        End If
                    Else
                        formattedAmount = (amount).ToString("###########0.00")
                    End If

            End Select
        End If

        Return formattedAmount

    End Function

    'Protected Function ContainsNonASCIIChar(ByVal fieldValue As String) As Boolean

    '    If IsNothingOrEmptyString(fieldValue) Then Return False

    '    For Each eachChar As Char In fieldValue
    '        If Convert.ToInt32(eachChar) > 255 Then Return True
    '    Next

    '    Return False

    'End Function

#End Region

End Class