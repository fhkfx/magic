Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions
Imports System.IO

''' <summary>
''' Encapsulates the FPS Format:
''' </summary>
Public Class FPSFormat
    Inherits BaseFileFormat

    'Target Data Tables
    Private _tblPayment As DataTable

    'Source Data Tables
    Private _tblPaymentSrc As DataTable

    'Added for Different Date value checking and A/C no.
    '*************************************
    Private Shared _IsSameDateValue As Boolean = True


#Region "Local Constants"

    'In this format, editable fields, group by fields and reference fields are same
    Private Shared _editableFields() As String = {"Beneficiary / Debtor ID", "Counter Party Name", "Bank Number", "Account Number" _
                                            , "Transaction Amount", "Our Reference", "Message to Beneficiary", "Email Address", "Notification Detail"}

    Private Shared _amountFieldName As String = "Transaction Amount"

    Private Shared _autoTrimmableFields() As String = {"Beneficiary / Debtor ID", "Counter Party Name", "Bank Number", "Account Number" _
                                            , "Transaction Amount", "Our Reference", "Message to Beneficiary", "Email Address", "Notification Detail"}

    Private Shared _validgroupbyFields() As String = {"Beneficiary / Debtor ID", "Counter Party Name", "Bank Number", "Account Number" _
                                            , "Transaction Amount", "Our Reference", "Message to Beneficiary", "Email Address", "Notification Detail"}

    Private Shared _headerfields() As String = {"Payment Mode", "Payment Date", "Currency", "Principal Account Number", "Principal Reference", "Instruction Remarks", "Purpose of Transfer", "Charges Borne By", "Charge Account", "Security Group", "Template Name"}

    'Fields: Body
    Private Shared _fieldsBody() As String = {"Beneficiary / Debtor ID", "Counter Party Name", "Bank Number", "Account Number" _
                                           , "Transaction Amount", "Our Reference", "Message to Beneficiary", "Email Address", "Notification Detail"}

#End Region

    Public Shared ReadOnly Property IsSameDateValue() As Boolean
        Get
            Return _IsSameDateValue
        End Get
    End Property

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    'Has more than one amount fields  
    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "FPS"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

#End Region

    Public Shared Function OutputFormatString() As String
        Return "FPS"
    End Function

#Region "Overriden Methods"


    Protected Overrides Sub GenerateHeader()

        _Header = String.Empty
        Dim strDoubleQuote As String = """"

        For Each row As DataRow In _PreviewTable.Rows

            For Each headerfld As String In _headerfields
                _Header = _Header & strDoubleQuote & row(headerfld).ToString() & strDoubleQuote
                '_Header &= IIf(_Header = strDoubleQuote & strDoubleQuote, "", ",")
                _Header = _Header & ","
            Next

            If _Header.EndsWith(",") Then _Header = _Header.Substring(0, _Header.Length - 1)

            Exit For

        Next

        _Header &= vbCrLf

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Payment Date") = ValueDate.ToString(TblBankFields("Payment Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
                Exit For
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim strField As String = String.Empty
        Dim rowindex As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim payeeNames As New List(Of String)
        'replace colons with a space in (1st, 36th, 71st, 106th, 141st, 176th) positions
        Dim ReplaceCharLocs() As Int32 = {0, 35, 70, 105, 140, 175}
        Dim remark As String
        Dim seqNo As Long = 0

        For Each row As DataRow In _PreviewTable.Rows

            'pad with leading zeros
            If _PreviewTable.Rows(rowindex)("Bank Number").ToString.Trim <> String.Empty Then
                _PreviewTable.Rows(rowindex)("Bank Number") = _PreviewTable.Rows(rowindex)("Bank Number").ToString.Trim.PadLeft(TblBankFields("Bank Number").DataLength.Value, "0")
            End If

            '#1 Beneficiary / Debtor ID (set running number.)
            If _PreviewTable.Rows(rowindex)("Beneficiary / Debtor ID").ToString.Trim = String.Empty Then
                seqNo += 1
                _PreviewTable.Rows(rowindex)("Beneficiary / Debtor ID") = "A" + seqNo.ToString("0000000")
            End If

            rowindex += 1
        Next

    End Sub

    'Validates Mandatory & Conditional Mandatory Fields.
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0
        Dim hasTemplateName As Boolean = False

        Try

            'Validate Mandatory Header Fields
            For Each headerfld As String In New String() {"Payment Date"}
                ''''''''''''''''''''''''''''''''modified by Jacky on 2018-11-01'''''''''''''''''''''''''''
                'If IsNothingOrEmptyString(_PreviewTable.Rows(0)(headerfld).ToString()) Then
                'AddValidationError(headerfld, _PreviewTable.Columns(headerfld).Ordinal + 1 _
                '                , String.Format(MsgReader.GetString("E09030160"), "Header", "", headerfld) _
                '              , 0, True)
                'End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Next

            'Check if Template Name is available, for conditional mapping control
            If _PreviewTable.Rows(0)("Template Name").ToString <> String.Empty Then
                hasTemplateName = True
            End If

            'Validate Conditionally Mandatory Header Fields
            For Each headerfld As String In New String() {"Payment Mode", "Currency", "Principal Account Number", "Purpose of Transfer"}

                If IsNothingOrEmptyString(_PreviewTable.Rows(0)(headerfld).ToString()) And Not hasTemplateName Then
                    AddValidationError(headerfld, _PreviewTable.Columns(headerfld).Ordinal + 1 _
                                       , String.Format(MsgReader.GetString("E09050010"), "Header", _PreviewTable.Columns(headerfld).Ordinal + 1, headerfld, "'Template Name' is not provided") _
                                       , 0, True)
                End If

            Next

            For Each row As DataRow In _PreviewTable.Rows

                'Validate Conditionally Mandatory Body Fields 
                ''''''''''''''''''''''''''''''modified by Jacky on 2018-11-02'''''''''''''''''''''''''
                'For Each _column As String In New String() {"Beneficiary / Debtor ID", "Counter Party Name", "Bank Number", "Account Number", "Transaction Amount"}
                For Each _column As String In New String() {"Transaction Amount"}
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If IsNothingOrEmptyString(row(_column).ToString()) And Not hasTemplateName Then
                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", _PreviewTable.Columns(_column).Ordinal + 1, _column, "'Template Name' is not provided") _
                                            , RowNumber + 1, True)
                    End If

                Next
                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type, Conditional Mandatory and Custom Validations 
    '(other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim found As Boolean = False
        Dim dAmount As Decimal = 0
        Dim strField As String = String.Empty
        Dim _compareDate As String = ""
        Dim _compareACNo As String = ""
        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim _previewTableCopy As New DataTable
        Dim prevTransRef As String = String.Empty
        Dim tempId As String = String.Empty
        Dim tempCur As String = String.Empty
        Dim currencyHeader As String = String.Empty

        Dim regNonAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
        Dim regNonAlpha As New Regex("[^A-Za-z]", RegexOptions.Compiled)
        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim regEmail As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled)
        Dim regUnpermittedChar As New Regex("[""|\\\[\]\^]", RegexOptions.Compiled)  ' disallow special characters : | " \ [ ] ^
        '''''''''''Modified by Jacky on 2019-04-08'''''''''''''''''''''''''''''
        'Purpose : Security group accept underline and space
        Dim regNonAlphanumAndUnderLineAndSpace As New Regex("[^0-9a-zA-Z_ ]", RegexOptions.Compiled)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                For Each col As String In TblBankFields.Keys
                    If IsNothingOrEmptyString(_previewRecord(col)) Then _previewRecord(col) = ""
                    If _previewRecord(col) Is DBNull.Value Then _previewRecord(col) = ""
                Next

                If rowNo = 0 Then

                    'Validate Header Records
                    '#1. Payment Mode must be one of the default values in master template
                    If _previewRecord("Payment Mode").ToString <> String.Empty Then
                        If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Payment Mode").ToString

                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Payment Mode").DefaultValue) Then
                            If TblBankFields("Payment Mode").DefaultValue.Length > 0 Then
                                Dim defaultCurrency() As String
                                defaultCurrency = TblBankFields("Payment Mode").DefaultValue.Split(",")
                                For Each val As String In defaultCurrency
                                    If _previewRecord("Payment Mode").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Payment Mode"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Mode").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Payment Mode' is not a valid Payment Mode.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    ''''''''''''''''''''Modified by Jacky on 2019-01-09''''''''''''''''''''
                    'Purpose : Avoid control panel setting problem
                    'Field #2 - Payment Date
                    If _previewRecord("Payment Date").ToString().Trim <> String.Empty Then
                        If Not InStr(_previewRecord("Payment Date").ToString(), "/") Then
                            _previewRecord("Payment Date") = _previewRecord("Payment Date").ToString().Replace("-", "/").Replace(".", "/")
                        End If
                    End If
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    '#3. Currency - if the field is presented, it should be same as that of Principal Account (TBC where to confirm the currency of Principal Account)
                    If _previewRecord("Currency").ToString <> String.Empty Then
                        If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Currency").ToString
                        currencyHeader = _previewRecord("Currency").ToString

                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                            If TblBankFields("Currency").DefaultValue.Length > 0 Then
                                Dim defaultCurrency() As String
                                defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                                For Each val As String In defaultCurrency
                                    If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Currency"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid Currency.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    '#7. Purpose of Transfer must be one of the default values in master template
                    If _previewRecord("Purpose of Transfer").ToString <> String.Empty Then
                        If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Purpose of Transfer").ToString

                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Purpose of Transfer").DefaultValue) Then
                            If TblBankFields("Purpose of Transfer").DefaultValue.Length > 0 Then
                                Dim defaultCurrency() As String
                                defaultCurrency = TblBankFields("Purpose of Transfer").DefaultValue.Split(",")
                                For Each val As String In defaultCurrency
                                    If _previewRecord("Purpose of Transfer").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Purpose of Transfer"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Purpose of Transfer").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Purpose of Transfer' is not a valid Purpose of Transfer.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    '#8. Charges Borne By must be one of the default values in master template
                    If _previewRecord("Charges Borne By").ToString <> String.Empty Then
                        If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Charges Borne By").ToString

                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Charges Borne By").DefaultValue) Then
                            If TblBankFields("Charges Borne By").DefaultValue.Length > 0 Then
                                Dim defaultCurrency() As String
                                defaultCurrency = TblBankFields("Charges Borne By").DefaultValue.Split(",")
                                For Each val As String In defaultCurrency
                                    If _previewRecord("Charges Borne By").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Charges Borne By"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Charges Borne By").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Charges Borne By' is not a valid Charges Borne By.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    'Field #10 - Security Group
                    If _previewRecord("Security Group").ToString.Trim <> String.Empty Then
                        If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Security Group").ToString

                        If regNonAlphanumAndUnderLineAndSpace.IsMatch(_previewRecord("Security Group").ToString) Then
                            If _previewRecord("Security Group").ToString <> "N/A" And _previewRecord("Security Group").ToString <> "n/a" Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "Security Group"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("Security Group").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Security Group")
                                    .RecordNo = rowNo + 1
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If

                End If


                'Start Body Record Fields validation
                '#1. Beneficiary / Debtor ID must be unique in the whole file
                If _previewRecord("Beneficiary / Debtor ID").ToString() <> "" Then
                    'For uniqueness checking
                    Dim isFound As Boolean = False
                    Dim sub_rno As Integer = -1
                    'check uniqueness in each record
                    For Each _subRec As DataRow In _PreviewTable.Rows
                        sub_rno += 1
                        'skip for the same record
                        If sub_rno = rowNo Then Continue For

                        If _previewRecord("Beneficiary / Debtor ID").ToString.Equals(_subRec("Beneficiary / Debtor ID").ToString) Then
                            isFound = True
                            Exit For
                        End If
                    Next

                    If isFound Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary / Debtor ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary / Debtor ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Beneficiary / Debtor ID' must be unique.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    If regUnpermittedChar.IsMatch(_previewRecord("Beneficiary / Debtor ID").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary / Debtor ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary / Debtor ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Beneficiary / Debtor ID' contains invalid characters.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ''''''''''''''''''''Modified by Jacky on 2019-01-09''''''''''''''''''''
                'Purpose : Avoid control panel setting problem
                'Field #2 - Payment Date
                If _previewRecord("Payment Date").ToString().Trim <> String.Empty Then
                    If Not InStr(_previewRecord("Payment Date").ToString(), "/") Then
                        _previewRecord("Payment Date") = _previewRecord("Payment Date").ToString().Replace("-", "/").Replace(".", "/")
                    End If
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                '#5. Transaction Amount should be positive
                Dim amount As Decimal
                If _previewRecord("Transaction Amount").ToString() <> "" Then
                    If Decimal.TryParse(_previewRecord("Transaction Amount").ToString, amount) Then
                        If amount <= 0 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Transaction Amount"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09050080"), "Record", rowNo + 1, "Transaction Amount")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                        '#5. Transaction Amount should not be larger than the maximum limit for Faster Payment (TBC)

                        'Format Transaction Amount Output
                        Select Case currencyHeader.ToUpper
                            Case "KWD"
                                'If no. of decimal places <= 3, then format to 3 decimal, else prompt error message
                                If NumberOfDecimalPlaces(_previewRecord("Transaction Amount").ToString) <= 3 Then
                                    If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                                        _previewRecord("Transaction Amount") = "-" & amount.ToString("###########0.000")
                                    Else
                                        _previewRecord("Transaction Amount") = amount.ToString("###########0.000")
                                    End If
                                Else
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, three decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            Case "JPY", "KRW", "IDR", "ARS"
                                'If no. of decimal places is not equal to 0, prompt error message
                                If regNonNumeric.IsMatch(_previewRecord("Transaction Amount").ToString) Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, no decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            Case Else
                                'If no. of decimal places <= 2, then format to 2 decimal, else prompt error message
                                If NumberOfDecimalPlaces(_previewRecord("Transaction Amount").ToString) <= 2 Then
                                    If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                                        _previewRecord("Transaction Amount") = "-" & amount.ToString("###########0.00")
                                    Else
                                        _previewRecord("Transaction Amount") = amount.ToString("###########0.00")
                                    End If
                                Else
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Transaction Amount"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid format, two decimal places required.", rowNo + 1)
                                    validationError.RecordNo = rowNo + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If

                        End Select


                    Else
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Transaction Amount"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Transaction Amount' is not a valid Transaction Amount.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                '#8. Email Address - multiple email addresses are supported by semicolon
                If _previewRecord("Email Address").ToString <> String.Empty Then
                    If _previewRecord("Email Address").ToString.StartsWith(";") Or _previewRecord("Email Address").ToString.EndsWith(";") Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Email Address"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Email Address").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Email Address' Semicolon(;) should not be inputted as first or last character of this field", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    Else
                        Dim isValid As Boolean = True
                        Dim emailAddress() As String
                        emailAddress = _previewRecord("Email Address").ToString.Split(";")
                        For Each val As String In emailAddress
                            If Not regEmail.IsMatch(val) Then
                                isValid = False
                                Exit For
                            End If
                        Next

                        If Not isValid Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Email Address"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Email Address").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Email Address' is not a valid Email Address.", rowNo + 1)
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

                'Field#9 Notification Detail
                '''''''''''''''''''''''''''modified by Jacky on 2018-10-29'''''''''''''''''''''''''''
                If _previewRecord("Notification Detail").ToString.Trim <> String.Empty Then
                    Dim bError As Boolean = False

                    If _previewRecord("Notification Detail").ToString.Length <= 5000 Then
                        If _previewRecord("Notification Detail").ToString.Contains(vbCrLf) Or _previewRecord("Notification Detail").ToString.Contains(vbCr) Or _previewRecord("Notification Detail").ToString.Contains(vbLf) Then
                            Dim strAddrLine() As String = _previewRecord("Notification Detail").ToString().Split(vbLf)
                            If strAddrLine.Length > 5000 Then
                                bError = True
                            ElseIf strAddrLine.Length > 1 Then
                                Dim i As Int16 = 0
                                Dim CountMaxiumLine As Int16 = 0
                                Dim totalCount = 0

                                While (Not bError) AndAlso i < strAddrLine.Length
                                    If strAddrLine(i).Length > 80 Then
                                        Dim CountUntilLower As Int16 = strAddrLine(i).Length
                                        'bError = True
                                        'Else
                                        While 80 < CountUntilLower
                                            CountUntilLower = CountUntilLower - 80
                                            CountMaxiumLine += 1
                                        End While
                                        i += 1
                                    Else
                                        i += 1
                                    End If

                                    ''If CountMaxiumLine <> 0 Then
                                    ''  totalCount = CountMaxiumLine + strAddrLine.Length
                                    ''End If

                                    ''If totalCount > 5000 Then
                                    ''  bError = True
                                    ''End If
                                End While
                                If CountMaxiumLine <> 0 Then
                                    totalCount = CountMaxiumLine + strAddrLine.Length
                                End If

                                'The total line must not exceed 5000
                                If totalCount > 5000 Then
                                    bError = True
                                End If
                            End If
                        End If
                    Else
                        bError = True
                    End If

                    If bError Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Notification Detail"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Notification Detail").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "Notification Detail", "5000 lines of 80 characters")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'ISO characters checking
                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Payment Mode", "Currenecy", "Principal Reference", "Template Name", "Beneficiary / Debtor ID", "Counter Party Name" _
                        , "Our Reference", "Message to Beneficiary", "Email Address", "Instruction Remarks"

                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso ContainsInvlidISO(_previewRecord(col).ToString) Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If
                            '''''''''''''''''''''''''Modified by Jacky on 2018-10-30''''''''''''''''''''''''''''''''''''
                            'ContainsInvlidISO method change to ContainsInvlidISO1 method
                        Case "Notification Detail"
                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso ContainsInvlidISO1(_previewRecord(col).ToString) Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Case Else
                            'Do Nothing
                    End Select
                Next

                'AlphaNumeric checking
                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Account Number"
                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regNonAlphaNum.IsMatch(_previewRecord(col).ToString) Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If

                        Case Else
                            'Do Nothing
                    End Select
                Next

                'Alphabet checking
                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Purpose of Transfer", "Charges Borne By"
                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regNonAlpha.IsMatch(_previewRecord(col).ToString) Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If

                        Case Else
                            'Do Nothing
                    End Select
                Next

                'numeric checking
                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Principal Account Number", "Charge Account", "Bank Number"
                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regNonNumeric.IsMatch(_previewRecord(col).ToString) Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If

                        Case Else
                            'Do Nothing
                    End Select
                Next

                rowNo += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()
        'N.A. for this format
        '''''''''''''''''''''Modified by Jacky on 2019-04-03''''''''''''''''''''
        Dim _rowIndex As Integer = 0
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            'Purpose : Transaction Amount should support cent function
            '#13. Transaction Amount formatting
            If _row("Transaction Amount").ToString <> String.Empty Then

                If Not _DataModel.IsAmountInDollars Then
                    _row("Transaction Amount") = Convert.ToDecimal(_row("Transaction Amount").ToString()) / 100
                End If

                _row("Transaction Amount") = Convert.ToDecimal(_row("Transaction Amount").ToString()).ToString("########0.00")

            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Next
    End Sub
    Protected Overrides Sub GenerateOutputFormat1()
        'N.A. for this format
    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As System.Collections.Generic.List(Of String), ByVal _referenceFields As System.Collections.Generic.List(Of String), ByVal _appendText As System.Collections.Generic.List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Transaction Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Transaction Amount"))
            If Not Decimal.TryParse(_row("Transaction Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Transaction Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Transaction Amount")

            _consolidatedRecordCount = _consolidatedData.Rows.Count

            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    'The output file consists of
    '1)	All Fields are separated by |, no enclosure characters
    '2)	One header and one-to-many Payment Records 

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sTaxAmts As String = String.Empty
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim omakaseOutput2FileName As String = String.Empty

        Dim objMasterTemplate As MasterTemplateNonFixed ' Non Fixed Type Master Template
        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            '1. File Format Type
            GenerateHeader()
            sText = _Header

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In _fieldsBody

                    If _previewRecord(_column).ToString() = "" Then
                        sLine = sLine & IIf(sLine = "", "", ",") & _previewRecord(_column).ToString()
                    Else
                        sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                    End If

                Next
                
                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Dim encoutputfile As String = _outputFile
            'Rename the file name
            If objMasterTemplate.EncryptCustomerOutputFile Then
                If _outputFile.Length > objMasterTemplate.CustomerOutputExtension.Length Then
                    _outputFile = _outputFile.Substring(0, _outputFile.Length - (objMasterTemplate.CustomerOutputExtension.Length + 1)) & "FYI." & objMasterTemplate.CustomerOutputExtension
                Else
                    _outputFile = "FYI." & objMasterTemplate.CustomerOutputExtension
                End If
            End If
            'Temporarily save in Plain Format 
            SaveTextToFile(sText, _outputFile)


            'Encrypt with GnuPg 1.4.7 
            'Save the encrypted Copy
            If objMasterTemplate.EncryptCustomerOutputFile Then
                ' EncryptOmakase(GnuPg147, GnuPubKey, argInputFile, argOutputFile)
                EncryptFPS(GnuPG147, _outputFile, encoutputfile)

                'Modified by Kay @ 07 Oct 2010
                'During UAT, BTMU asked to generate a plain text file if Encrypt Output file is unchecked.
                'So move the below codes into If condition statement
                Dim outFileInfo As New FileInfo(_outputFile)
                If outFileInfo.Exists Then outFileInfo.Delete()

            End If

            Return True

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Amount Field has invalid numeric value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    Private Function FormatAmount(ByVal amount As Decimal) As String

        Dim formattedAmount As String = String.Empty

        'Amount fields should not be formatted if there are more than 2 decimal places.
        'Total Amount
        'Transaction Amount
        'VAT Amount-P
        'Discount Amount
        'Invoice Amount-P


        If amount.ToString().IndexOf(".") > 0 Then 'Should not format if  more than 2 decimal places
            If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                formattedAmount = (amount).ToString("#0.00")
            Else
                formattedAmount = amount
            End If
        Else
            formattedAmount = (amount).ToString("#0.00")
        End If

        Return formattedAmount

    End Function
#End Region

#Region "Helper Methods"
    Private Sub EncryptFPS(ByVal GnuPg147 As String, ByVal argInputFile As String, _
                                ByVal argOutputFile As String _
                                )
        Dim homedirectory As String = GnuPg147.Replace("\gpg.exe", "")
        'to generate ASCII output, modified by kay @ 07 Oct 2010 (added --armor)
        Dim argSign As String = " --output """ & argOutputFile & """ -er OmakaseIndia  --armor """ & argInputFile & """"

        argSign = "--homedir """ & homedirectory & """" & argSign
        Dim proc As Process


        proc = Process.Start(GnuPg147, argSign)
        proc.WaitForExit()
        If proc.ExitCode <> 0 Then Throw New Exception("Failed to encrypt output file!")
        proc.Close()
        proc.Dispose()


    End Sub

    Private Function ReplaceNonPermitCharWithSpace(ByVal fieldstr As String) As String
        'non-permitted characters
        '{	}	!	@	$	#	%	^	&	*	=	~	<	>	;	�	\	[	]	|	_
        Dim NonPermitChars() As Char = {Chr(123), Chr(125), Chr(33), Chr(64), Chr(36), Chr(35), Chr(37), _
                                        Chr(94), Chr(38), Chr(42), Chr(61), Chr(126), Chr(60), Chr(62), _
                                        Chr(59), Chr(34), Chr(92), Chr(91), Chr(93), Chr(124), Chr(95)}
        Dim tmpstr As String = String.Empty

        tmpstr = fieldstr

        'For i As Int32 = 0 To tmpstr.Length
        For j As Int16 = 0 To NonPermitChars.Length - 1
            'If tmpstr(i).Equals(NonPermitChars(j)) Then
            'replace it with a space
            tmpstr = tmpstr.Replace(NonPermitChars(j), Chr(32))
            'End If
        Next
        'Next

        ReplaceNonPermitCharWithSpace = tmpstr

    End Function

    Private Function ContainsInvlidISO(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) < 32 Or Convert.ToInt32(chr) > 126 Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Function ContainsInvlidISO1(ByVal textvalue As String) As Boolean
        'Permitted Special Characters in AutoCheque ACMS File Format despite Eng/Thai alphabets
        ' @	#	%	&	*	(	)	_	-	=	+	;	:		'	<
        '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
        If IsNothingOrEmptyString(textvalue) Then Return True

        textvalue = textvalue.Trim()
        For Each chr As Char In textvalue
            If Convert.ToInt32(chr) < 32 Or Convert.ToInt32(chr) > 126 Then
                ''''''''''''''''''''modified by Jacky on 2018-10-29'''''''''''''''''''''
                If (Convert.ToInt32(chr) = 10 Or Convert.ToInt32(chr) = 13) Then
                    Return False
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return True
            End If
        Next
        Return False
    End Function

    Private Function NumberOfDecimalPlaces(ByVal textvalue As String) As Integer
        Dim indexOfDecimalPoint As Integer = textvalue.IndexOf(".")
        If indexOfDecimalPoint = -1 Then
            Return 0
        Else
            Return textvalue.Substring(indexOfDecimalPoint + 1).Length
        End If

    End Function

#End Region

End Class
