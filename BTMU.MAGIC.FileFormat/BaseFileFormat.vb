Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.IO

''' <summary>
''' This class Abstracts the common File Format behaviors
''' </summary>
''' <remarks></remarks>
Public MustInherit Class BaseFileFormat

    Protected dbConnectionString As String
    Private _intermediaryFileName As String
    Private _outputFileName As String
    Private _saveLocation As String
    Public Shared GnuPG_Home As String 'init it @ the main program
    Public Shared GnuPG147 As String 'init it @ the main program
    Public Shared GnuPG147_PubKey As String 'init it @ the main program

#Region "Protected Members"

    'For Consolidation and Quick Conversion
    Protected _isConsolidated As Boolean
    Protected _paymentOption As String ' This is used only for VPS and CWS file formats.The value can be "+" or "-"
    Protected _isPositivePaymentOption As Boolean
    Protected _isNegativePaymentOption As Boolean
    Protected _isQuickConversion As Boolean
    Protected _isCheckedSumAmount As Boolean
    Protected _sourceFormat As String
    Protected _masterTemplateName As String
    Protected _commonTemplateName As String
    Protected _definitionFile As String
    Protected _sourceFileName As String
    Protected _worksheetName As String
    Protected _EliminatedChar As String
    Protected _valueDate As Date
    Protected _useValueDate As Boolean
    Protected _transactionEndRow As Integer
    Protected _removeRowsFromEnd As Integer

    'For Input 
    Protected _SourceData As DataView
    Protected _DataModel As ICommonTemplate
    Protected _MasterTemplateList As MasterTemplateList

    'For Ouput  
    Protected _Header As String
    Protected _Trailer As String
    Protected _PreviewTable As DataTable
    Protected _consolidatedDataView As DataView
    Protected _consolidatedData As DataTable
    Protected _ValidationErrors As New FileFormatErrorCollection()
    Protected _ErrorAtMandatoryField As Boolean

    Protected _totalRecordCount As Integer
    Protected _consolidatedRecordCount As Integer

    ' Default Value of Currency and Decimal Length could be in this format JPY-2,SGD,MYR-3
    Protected TblDecimalLengthForCurrency As New Dictionary(Of String, Int32)

#End Region

#Region "Public Property"

    Public Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)

    ''' <summary>
    ''' Dictionary of Bank Field Names and their details
    ''' </summary>
    ''' <remarks></remarks>
    Public TblBankFields As New Dictionary(Of String, MapBankField)(25)

    ''' <summary>
    ''' This property is to Get/Set Source Format Name
    ''' </summary>
    ''' <value>Source Format Name</value>
    ''' <returns>Returns Source Format Name</returns>
    ''' <remarks></remarks>
    Public Property SourceFormat() As String
        Get
            Return _sourceFormat
        End Get
        Set(ByVal value As String)
            _sourceFormat = value
        End Set
    End Property


    ''' <summary>
    ''' This Property is to Get/Set Common Template Name
    ''' </summary>
    ''' <value>Common Template Name</value>
    ''' <returns>Returns Common Template Name</returns>
    ''' <remarks></remarks>
    Public Property CommonTemplateName() As String
        Get
            Return _commonTemplateName
        End Get
        Set(ByVal value As String)
            _commonTemplateName = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Master Template Name
    ''' </summary>
    ''' <value>Master Template Name</value>
    ''' <returns>Returns Master Template Name</returns>
    ''' <remarks></remarks>
    Public Property MasterTemplateName() As String
        Get
            Return _masterTemplateName
        End Get
        Set(ByVal value As String)
            _masterTemplateName = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Source File Name
    ''' </summary>
    ''' <value>Source File Name</value>
    ''' <returns>Returns Source File Name</returns>
    ''' <remarks></remarks>
    Public Property SourceFileName() As String
        Get
            Return _sourceFileName
        End Get
        Set(ByVal value As String)
            _sourceFileName = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Worksheet Name
    ''' </summary>
    ''' <value>Worksheet Name</value>
    ''' <returns>Returns Worksheet Name</returns>
    ''' <remarks></remarks>
    Public Property WorksheetName() As String
        Get
            Return _worksheetName
        End Get
        Set(ByVal value As String)
            _worksheetName = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Intermediary FileName 
    ''' </summary>
    ''' <value>Intermediary FileName</value>
    ''' <returns>Returns Intermediary FileName</returns>
    ''' <remarks></remarks>
    Public Property IntermediaryFileName() As String
        Get
            Return _intermediaryFileName
        End Get
        Set(ByVal value As String)
            _intermediaryFileName = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Transaction End Row 
    ''' </summary>
    ''' <value>Transaction End Row</value>
    ''' <returns>Returns Transaction End Row</returns>
    ''' <remarks></remarks>
    Public Property TransactionEndRow() As Integer
        Get
            Return _transactionEndRow
        End Get
        Set(ByVal value As Integer)
            _transactionEndRow = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set Transaction End Row 
    ''' </summary>
    ''' <value>Transaction End Row</value>
    ''' <returns>Returns Transaction End Row</returns>
    ''' <remarks></remarks>
    Public Property RemoveRowsFromEnd() As Integer
        Get
            Return _removeRowsFromEnd
        End Get
        Set(ByVal value As Integer)
            _removeRowsFromEnd = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Character of Elimination
    ''' </summary>
    ''' <value>Eliminated Character</value>
    ''' <returns>Returns Eliminated Character</returns>
    ''' <remarks></remarks>
    Public Property EliminatedCharacter() As String
        Get
            Return _EliminatedChar
        End Get
        Set(ByVal value As String)
            _EliminatedChar = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Location of Converted Output File
    ''' </summary>
    ''' <value>Save Location</value>
    ''' <returns>Returns Save Location</returns>
    ''' <remarks></remarks>
    Public Property SaveLocation() As String
        Get
            Return _saveLocation
        End Get
        Set(ByVal value As String)
            _saveLocation = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Output File Name
    ''' </summary>
    ''' <value>Output FileName</value>
    ''' <returns>Returns Output FileName</returns>
    ''' <remarks></remarks>
    Public Property OutputFileName() As String
        Get
            Return _outputFileName
        End Get
        Set(ByVal value As String)
            _outputFileName = value
        End Set
    End Property

    ''' <summary>
    ''' Determines whether to use the current date as the Value Date
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>Returns a boolean value indicating whether to use the current date as the value date</returns>
    ''' <remarks></remarks>
    Public Property UseValueDate() As Boolean
        Get
            Return _useValueDate
        End Get
        Set(ByVal value As Boolean)
            _useValueDate = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Value Date
    ''' </summary>
    ''' <value>Value Date</value>
    ''' <returns>Returns Value Date</returns>
    ''' <remarks></remarks>
    Public Property ValueDate() As Date
        Get
            Return _valueDate
        End Get
        Set(ByVal value As Date)
            _valueDate = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Definition File
    ''' </summary>
    ''' <value>Definition File</value>
    ''' <returns>Returns Definition File</returns>
    ''' <remarks></remarks>
    Public Property DefinitionFile() As String
        Get
            Return _definitionFile
        End Get
        Set(ByVal value As String)
            _definitionFile = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Definition File
    ''' </summary>
    ''' <value>Definition File</value>
    ''' <returns>Returns Definition File</returns>
    ''' <remarks></remarks>
    Public Property IsConsolidated() As Boolean
        Get
            Return _isConsolidated
        End Get
        Set(ByVal value As Boolean)
            _isConsolidated = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get/Set the Payment Option
    ''' </summary>
    ''' <value>Payment Option</value>
    ''' <returns>Returns Payment Option</returns>
    ''' <remarks></remarks>
    Public Property PaymentOption() As String
        Get
            Return _paymentOption
        End Get
        Set(ByVal value As String)
            _paymentOption = value
        End Set
    End Property

    ''' <summary>
    ''' Determines whether the Payment Option is positive(+) 
    ''' </summary>
    ''' <value>boolean </value>
    ''' <returns>Returns a boolean value indicating the + Payment Option</returns>
    ''' <remarks></remarks>
    Public Property IsPositivePaymentOption() As Boolean
        Get
            Return _isPositivePaymentOption
        End Get
        Set(ByVal value As Boolean)
            _isPositivePaymentOption = value
        End Set
    End Property

    ''' <summary>
    ''' Determines whether the Payment Option is negative(-) 
    ''' </summary>
    ''' <value>boolean </value>
    ''' <returns>Returns a boolean value indicating the - Payment Option</returns>
    ''' <remarks></remarks>
    Public Property IsNegativePaymentOption() As Boolean
        Get
            Return _isNegativePaymentOption
        End Get
        Set(ByVal value As Boolean)
            _isNegativePaymentOption = value
        End Set
    End Property

    ''' <summary>
    ''' Determines whether Summing up of Amount is required
    ''' </summary>
    ''' <value>boolean </value>
    ''' <returns>Returns a boolean value indicating whether Summing up of amount is required</returns>
    ''' <remarks></remarks>
    Public Property IsCheckedSumAmount() As Boolean
        Get
            Return _isCheckedSumAmount
        End Get
        Set(ByVal value As Boolean)
            _isCheckedSumAmount = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Set/Get Preview Records
    ''' </summary>
    ''' <value>Data Table</value>
    ''' <returns>Returns a Data Table of Preview Transaction Records</returns>
    ''' <remarks></remarks>
    Public Property PreviewDataTable() As DataTable
        Get
            Return _PreviewTable
        End Get
        Set(ByVal value As DataTable)
            _PreviewTable = value
        End Set
    End Property

    ''' <summary>
    ''' This Property is to Get a table of Consolidated Data
    ''' </summary>
    ''' <value>Data Table</value>
    ''' <returns>Returns a Data Table</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ConsolidatedData() As DataTable
        Get
            Return _consolidatedData
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get Header Value
    ''' </summary>
    ''' <value>Header</value>
    ''' <returns>Returns Header</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Header() As String
        Get
            Return _Header
        End Get

    End Property

    ''' <summary>
    ''' This Property is to Get Trailer Value
    ''' </summary>
    ''' <value>Trailer</value>
    ''' <returns>Returns Trailer</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Trailer() As String
        Get
            Return _Trailer
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the Total Number of Errors
    ''' </summary>
    ''' <value>Number of Errors</value>
    ''' <returns>Returns the Number of Errors</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TotalErrors() As Int32
        Get
            Return _ValidationErrors.Count
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the Total Number of Records
    ''' </summary>
    ''' <value>Number of Records</value>
    ''' <returns>Returns the Number of Records</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TotalRecords() As Int32
        Get
            'Return _PreviewTable.Rows.Count
            Return _totalRecordCount
        End Get
    End Property

    ''' <summary>
    ''' This Property is to Get the Total Number of Consolidated Records
    ''' </summary>
    ''' <value>Number of Consolidated Records</value>
    ''' <returns>Returns the Number of Consolidated Records</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ConsolidatedRecords() As Int32
        Get
            Return _consolidatedRecordCount
        End Get
    End Property

    ''' <summary>
    ''' Determines whether there is an error at any of the mandatory fields fail to comply Validations
    ''' </summary>
    ''' <value>boolean</value>
    ''' <returns>Returns a boolean value indicating whether any of the mandatory fields fail to comply Validations</returns>
    ''' <remarks></remarks>
    Public Property IsErrorWithMandatoryFields() As Boolean
        Get
            Return _ErrorAtMandatoryField
        End Get
        Set(ByVal value As Boolean)
            _ErrorAtMandatoryField = value
        End Set
    End Property

    ''' <summary>
    ''' Collection of Validation Errors
    ''' </summary>
    ''' <value>Validation Error</value>
    ''' <returns>Returns a list of Validation Errors</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ValidationErrors() As FileFormatErrorCollection
        Get
            Return _ValidationErrors
        End Get
    End Property

    ''' <summary>
    ''' List of Bank Fields valid for Group By
    ''' </summary>
    ''' <value>Bank Field</value>
    ''' <returns>Array of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GroupByFields() As String()
        Get
            Return ValidGroupByFields
        End Get
    End Property

    ''' <summary>
    ''' List of Bank Fields valid for Reference
    ''' </summary>
    ''' <value>Bank Field</value>
    ''' <returns>Array of Bank Fields</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ReferenceFields() As String()
        Get
            Return ValidReferenceFields
        End Get
    End Property

#End Region

#Region " Generic Methods "

    'Amount field is summed up while grouping records
    Protected MustOverride ReadOnly Property AmountFieldName() As String

    Protected MustOverride ReadOnly Property FileFormatName() As String

    Protected Overridable Sub ApplyValueDate()

    End Sub

    Protected MustOverride Sub GenerateOutputFormat()

    Protected MustOverride Sub Validate()

    Protected MustOverride ReadOnly Property AutoTrimmableFields() As String()

    Public MustOverride Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _masterTemplate As Object) As Boolean

    Public MustOverride Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

    'Can be implemented by FileFormat to format preview data
    Protected Overridable Sub FormatOutput()

    End Sub

    Protected Overridable ReadOnly Property ValidGroupByFields() As String()
        Get
            Dim _validGrpByFields(TblBankFields.Keys.Count - 1) As String
            TblBankFields.Keys.CopyTo(_validGrpByFields, 0)
            Return _validGrpByFields
        End Get
    End Property

    Protected Overridable ReadOnly Property ValidReferenceFields() As String()
        Get
            Return ValidGroupByFields
        End Get
    End Property

    'Can be implemented by FileFormat that recomputes amount value after consolidation/edit/update
    Protected Overridable Sub GenerateOutputFormat1()

    End Sub

    'Can be implemented by FileFormat that Generates Header
    Protected Overridable Sub GenerateHeader()

    End Sub

    'Can be implemented by FileFormat that Generates Trailer
    Protected Overridable Sub GenerateTrailer()

    End Sub

    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overridable Sub ConversionValidation()

    End Sub

#End Region

#Region "Public Methods"

    ''' <summary>
    ''' Generates transaction records for preview
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template</param>
    ''' <param name="ObjMasterTemplateList">Master Template</param>
    ''' <param name="dbConnectionString">Database Connection String</param>
    ''' <remarks></remarks>
    Public Sub DoPreview(ByRef ObjDataModel As ICommonTemplate _
                        , ByRef ObjMasterTemplateList As MasterTemplateList _
                        , ByVal dbConnectionString As String)

        'The old System defaults "Dollar" for Amount Setting 
        ObjDataModel.IsAmountInDollars = IIf(ObjDataModel.IsAmountInCents, False, True)

        Me.dbConnectionString = dbConnectionString
        _SourceData = ObjDataModel.FilteredSourceData
        _DataModel = ObjDataModel
        _MasterTemplateList = ObjMasterTemplateList
        _PreviewTable = New DataTable("PreviewData")
        _ValidationErrors.Clear()

        CopyMapping() 'Copy SourceField - BankField Mapping
        PopulateDecimalLengthForCurrency() 'Capture Decimal Length defined for different currencies
        ReadAllSourceData() 'Read-in Source Values to "PreviewData" Data Table as per Mapping 

        'Throw exception if there are no data for preview
        If _PreviewTable.Rows.Count = 0 Then Throw New MagicException("There is no Source data for preview.")

        ' Translate any Dbnull value or nothing to empty string
        TranslateDBNullToEmptyString()

        ApplyTranslation()  'Translation
        ApplyLookup() 'Lookup
        ApplyCalculation() 'Calculation
        ApplyStringManipulation() 'String Manipulation

        'added for Omakase II string manipulation 
        'modified by Kay @ 16 Mar 2011
        BeneAddressStringManipulation()

        ' Translate any Dbnull value or nothing to empty string
        TranslateDBNullToEmptyString()


        'Fix Numeric Data Length for Fixed Master Template 
        If FixNumericData() = -1 Then Exit Sub 'No need to proceed any further as user doesnt want to!

        'Sector Selection - implemented only by GCMSFileFormat. It also formats the SequenceNo  
        GenerateSectorSelection()

        'If UseValueDate options is specified in Quick/Manual Conversion
        If UseValueDate Then ApplyValueDate()

        _totalRecordCount = _PreviewTable.Rows.Count
        _consolidatedRecordCount = 0

        GenerateOutputFormat() 'Invokes the Specific Output format 

        'Throw exception if there are no data for preview
        If _PreviewTable.Rows.Count = 0 Then Throw New MagicException("There is no converted data for Preview.")

        ' Translate any Dbnull value or nothing to empty string
        TranslateDBNullToEmptyString()

        If _EliminatedChar <> String.Empty Then RemoveEliminatedCharacter()

        FormatOutput()

        'Invokes the Header/Trailer for the Specific Output Format 
        GenerateHeader()
        GenerateTrailer()

        ValidateDataLength()
        ValidateMandatoryFields()
        Validate() 'Custom validation implemented by FileFormat 



    End Sub

    ''' <summary>
    ''' Converts the Transaction Records to the Target Format
    ''' </summary>
    ''' <param name="ObjDataModel">Common Template</param>
    ''' <param name="ObjMasterTemplateList">Master Template</param>
    ''' <remarks></remarks>
    Public Sub DoConvert(ByRef ObjDataModel As ICommonTemplate _
                        , ByRef ObjMasterTemplateList As MasterTemplateList)

        'The old System defaults "Dollar" for Amount Setting 
        ObjDataModel.IsAmountInDollars = IIf(ObjDataModel.IsAmountInCents, False, True)

        _DataModel = ObjDataModel
        _MasterTemplateList = ObjMasterTemplateList
        _ErrorAtMandatoryField = False ' Clear all the mandatory validation errors
        _ValidationErrors.Clear()

        ' Translate any Dbnull value or nothing to empty string
        TranslateDBNullToEmptyString()

        'update the sector selection logic just in case user has updated
        'one of the editable fields that govern the logic to compute sector selection
        GenerateSectorSelection()

        ' Recalculate the total amount in case the user edited the Amount in Preview grid.
        GenerateOutputFormat1()

        ' Translate any Dbnull value or nothing to empty string
        TranslateDBNullToEmptyString()

        If _EliminatedChar <> String.Empty Then RemoveEliminatedCharacter()

        FormatOutput()
        GenerateHeader()
        GenerateTrailer()
        ValidateDataLength()
        ValidateMandatoryFields()
        Validate()
        ConversionValidation()

        Dim test As String
        test = "!23"

        _totalRecordCount = _PreviewTable.Rows.Count
        If Not _isConsolidated Then _consolidatedRecordCount = 0

    End Sub


    ''' <summary>
    ''' Saves the Converted Transaction records to a file
    ''' </summary>
    ''' <param name="strData">Converted Transaction records</param>
    ''' <param name="fileName">output file name</param>
    ''' <returns>Returns a boolean value indicating whether the file generation is successful</returns>
    ''' <remarks></remarks>
    Public Function SaveTextToFile(ByRef strData As String, ByVal fileName As String) As Boolean

        'edited by fhk on 18-sep-2015, convert text file to UTF-8 with BOM format
        Dim utf8WithBom As New System.Text.UTF8Encoding(True)
        'File.WriteAllText(fileName, strData)
        File.WriteAllText(fileName, strData, utf8WithBom)
        'end by fhk on 18-sep-2015

        Return System.IO.File.Exists(fileName)

    End Function

    ''' <summary>
    ''' Trims the leading/Trailing spaces in the transaction data
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub TrimData()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                For Each _column As String In AutoTrimmableFields
                    If _previewRecord(_column).ToString.Length > TblBankFields(_column).DataLength.Value Then
                        _previewRecord(_column) = _previewRecord(_column).ToString.Substring(0, TblBankFields(_column).DataLength.Value)
                    End If
                Next
            Next

        Catch ex As Exception
            Throw New MagicException("Error while Trimming Trimmable Fields: " & ex.Message.ToString)
        End Try
        _ValidationErrors.Clear()
        ValidateDataLength()
        ValidateMandatoryFields()
        Validate()

    End Sub

    ''' <summary>
    ''' Writes the Activities to Activity Log
    ''' </summary>
    ''' <param name="_userName">User ID of Application User that performs an activity</param>
    ''' <param name="_sourceFile">Name of the Source File</param>
    ''' <param name="_outputFile">Name of the Output File</param>
    ''' <remarks></remarks>
    Public Sub WriteActivityLog(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String)
        Try

            Dim currencyAmt As New SortedList(Of String, Decimal)
            Dim currency As New List(Of String)
            Dim previewCurrency As DataView

            'modified for CARROT Customer data format (Changed by FHK 2013-03-05)
            If FileFormatName.Equals(CarrotCustomerDataFormat.OutputFormatString, StringComparison.InvariantCultureIgnoreCase) Then
                'no currency and amount available
                HelperModule.Log_ConvertSourceFile(_userName, _sourceFile, _outputFile, _PreviewTable.Rows.Count)

            Else
                If TblBankFields.ContainsKey("Currency") Then
                    currency.AddRange(TblBankFields("Currency").DefaultValue.Split(","))
                    For Each _currency As String In currency
                        currencyAmt.Add(_currency, 0)
                    Next

                    previewCurrency = _PreviewTable.DefaultView
                    Dim _totalAmount As Decimal
                    For Each _currency As String In currency
                        _totalAmount = 0
                        previewCurrency.RowFilter = "Currency='" & _currency & "'"
                        If previewCurrency.Count > 0 Then
                            For Each _row As DataRowView In previewCurrency
                                _totalAmount += Convert.ToDecimal(_row(AmountFieldName).ToString)
                                currencyAmt(_currency) = _totalAmount
                            Next
                        End If

                    Next
                    HelperModule.Log_ConvertSourceFile(_userName, _sourceFile, _outputFile, _PreviewTable.Rows.Count, currencyAmt)
                Else
                    HelperModule.Log_ConvertSourceFile(_userName, _sourceFile, _outputFile, _PreviewTable.Rows.Count)
                End If
            End If

        Catch ex As Exception
            Throw New Exception("Error writing to Activity Log(WriteActivityLog) : " & ex.Message.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' Applies the Prevalidation conditions on the Transaction data prior to conversion
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ValidateBeforeConversion()

        Try
            ' Validation - Common Template Name : Cannot be empty
            If IsNothingOrEmptyString(CommonTemplateName) Then
                Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Common Template Name"))
            End If
            ' Validation - Source File Name : Cannot be empty
            If IsNothingOrEmptyString(SourceFileName) Then
                Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Source File Name"))
            End If

            ' Validation - Value Date : Should not be less than today
            If UseValueDate Then
                If IsDate(ValueDate) Then
                    'If Date.Compare(Convert.ToDateTime(ValueDate), Date.Today) < 0 Then
                    If ValueDate < Date.Today Then
                        Throw New MagicException(String.Format(MsgReader.GetString("E03000010"), "Value Date"))
                    End If
                Else
                    Throw New MagicException(String.Format(MsgReader.GetString("E03000010"), "Value Date"))
                End If
            End If

            ' Validation - Save Location : Cannot be empty
            If IsNothingOrEmptyString(SaveLocation) Then
                Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Save Location"))
            End If

            Select Case SourceFormat.ToUpper
                Case "TXT"
                    If Not IsNumeric(RemoveRowsFromEnd) Then
                        Throw New MagicException("Please enter a valid Transaction End Row")
                    End If
                    ' Validation - Remove Rows from End : Should be greater than 0
                    If RemoveRowsFromEnd < 0 Then
                        Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Remove Rows from End"))
                    End If
                Case "EXCEL"
                    ' Validation - Worksheet Name : Cannot be empty
                    If IsNothingOrEmptyString(WorksheetName) Then
                        Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Worksheet Name"))
                    End If
                    If Not IsNumeric(TransactionEndRow) Then
                        Throw New MagicException("Please enter a valid Transaction End Row")
                    End If
                    ' Validation - Transaction End Row : Should be greater than 0
                    If TransactionEndRow < 0 Then
                        Throw New MagicException(String.Format(MsgReader.GetString("E00000050"), "Transaction ends at Row"))
                    End If
                Case "SWIFT"
            End Select

            'edited by fhk on 20-oct-2015, check source file extension, convert for non-EXCEL file only: not allow ".XLS", ".XLSX"
            Dim fileExtension As String = ""
            fileExtension = System.IO.Path.GetExtension(SourceFileName)

            'added by fhk on 28-aug-2015, convert non-unicode format text file from Big5 to UTF-8 with BOM for TXT, SWIFT type
            'If SourceFormat.ToUpper <> "EXCEL" Then
            If SourceFormat.ToUpper <> "EXCEL" And fileExtension.ToUpper <> ".XLS" And fileExtension.ToUpper <> ".XLSX" Then
                'end by fhk on 20-oct-2015
                Dim sEncoding As System.Text.Encoding = GetFileEncoding(SourceFileName)

                'convert encoding if file is non-unicode format, default ANSI type is 874 from GetFileEncoding
                If sEncoding.Equals(System.Text.Encoding.GetEncoding(874)) Then
                    'backup original source file before conversion, delete if backup .org file exists
                    Dim fileSource As String = SourceFileName
                    Dim fileBackup As String = fileSource + ".org"
                    If File.Exists(fileBackup) Then
                        Try
                            File.Delete(fileBackup)
                        Catch ex As Exception
                            File.SetAttributes(fileBackup, FileAttributes.Normal)
                            File.Delete(fileBackup)
                        End Try
                    End If

                    File.Copy(fileSource, fileBackup)

                    Dim sFile As StreamReader = New StreamReader(fileSource, System.Text.Encoding.GetEncoding("Big5"))
                    Dim sText As String = sFile.ReadToEnd()
                    sFile.Close()

                    Dim utf8WithBom As New System.Text.UTF8Encoding(True)
                    Dim sWriter As New StreamWriter(fileSource, False, utf8WithBom)
                    sWriter.Write(sText)
                    sWriter.Close()
                End If
            End If
            'end by fhk on 28-aug-2015
            
        Catch ex As MagicException
            Throw New MagicException(ex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on Conversion Validation(ValidateBeforeConversion) : " & ex.Message.ToString)
        End Try

    End Sub

#End Region

#Region "Shared Methods"

    Public Shared Function GetEditableFields(ByVal FileFormatName As String) As String()

        Select Case FileFormatName

            Case EPSFormat.OutputFormatString
                Return EPSFormat.EditableFields
            Case VPSFormat.OutputFormatString
                Return VPSFormat.EditableFields
            Case iFTSFormat.OutputFormatString
                Return iFTSFormat.EditableFields
            Case iFTS2MultiLineFormat.OutputFormatString
                Return iFTS2MultiLineFormat.EditableFields
            Case iFTS2SingleLineFormat.OutputFormatString()
                Return iFTS2SingleLineFormat.EditableFields
                'Case iFTS2GenericFormat.OutputFormatString
                '    Return iFTS2GenericFormat.EditableFields
            Case OMAKASEFormat.OutputFormatString
                Return OMAKASEFormat.EditableFields
            Case CWSFormat.OutputFormatString
                Return CWSFormat.EditableFields
            Case BULKVNFormat.OutputFormatString
                Return BULKVNFormat.EditableFields
            Case GCMSFormat.OutputFormatString
                Return GCMSFormat.EditableFields

            Case iRTMSCIFormat.OutputFormatString
                Return iRTMSCIFormat.EditableFields
            Case iRTMSGCFormat.OutputFormatString
                Return iRTMSGCFormat.EditableFields
            Case iRTMSGDFormat.OutputFormatString
                Return iRTMSGDFormat.EditableFields
            Case iRTMSRMFormat.OutputFormatString
                Return iRTMSRMFormat.EditableFields

            Case OiRTMSCIFormat.OutputFormatString
                Return OiRTMSCIFormat.EditableFields
            Case OiRTMSGCFormat.OutputFormatString
                Return OiRTMSGCFormat.EditableFields
            Case OiRTMSGDFormat.OutputFormatString
                Return OiRTMSGDFormat.EditableFields
            Case OiRTMSRMFormat.OutputFormatString
                Return OiRTMSRMFormat.EditableFields

            Case OMAKASEFormatII.OutputFormatString
                Return OMAKASEFormatII.EditableFields
            Case GCMSPlusFormat.OutputFormatString
                Return GCMSPlusFormat.EditableFields

                'MAGIC HK Enhancement
            Case FundsTransferInstructionFormat.OutputFormatString
                Return FundsTransferInstructionFormat.EditableFields
            Case AutoDebitFormat.OutputFormatString
                Return AutoDebitFormat.EditableFields
            Case AutoChequeFormat.OutputFormatString
                Return AutoChequeFormat.EditableFields

                '06062018 FTI II and AutoCheque ACMS with no header (FHK Maggie)
            Case AutoChequeFormatII.OutputFormatString
                Return AutoChequeFormatII.EditableFields
            Case FundsTransferInstructionFormatII.OutputFormatString
                Return FundsTransferInstructionFormatII.EditableFields

                'CARROT (Added by FHK 2013-03-05)
            Case CarrotCustomerDataFormat.OutputFormatString
                Return CarrotCustomerDataFormat.EditableFields
            Case CarrotInvoiceDataFormat.OutputFormatString
                Return CarrotInvoiceDataFormat.EditableFields

            Case GCMSPlusCreditDataFormat.OutputFormatString
                Return GCMSPlusCreditDataFormat.EditableFields

                'CFC (Added by FHK Maggie 2018-07-05)
            Case CashForecastingFormat.OutputFormatString
                Return CashForecastingFormat.EditableFields

                'FPS (Added by FHK Maggie 2018-07-18)
            Case FPSFormat.OutputFormatString
                Return FPSFormat.EditableFields

            Case Else
                Throw New MagicException("Undefined Output File Format! Valid Output File Formats: EPS, VPS, iFTS, CWS, BULKVN, CARROTCUSTOMER, CARROTINVOICE, GCMS, GCMS Plus, iFTS-2 SingleLine, iFTS-2 MultiLine, Mr.Omakase India , Mr.Omakase India II , iRTMSCI, iRTMSGC, iRTMSGD, iRTMSRM, OiRTMSCI, OiRTMSGC, OiRTMSGD, OiRTMSRM, Funds Transfer Instruction, Funds Transfer Instruction II, AutoDebit, AutoCheque, AutoCheque ACMS, Cash Forecasting, FPS")

        End Select

    End Function


    Public Shared Function CreateFileFormat(ByVal FileFormatName As String) As BaseFileFormat

        Select Case FileFormatName
            Case EPSFormat.OutputFormatString
                Return New EPSFormat()
            Case VPSFormat.OutputFormatString
                Return New VPSFormat()
            Case iFTSFormat.OutputFormatString
                Return New iFTSFormat()

            Case iFTS2SingleLineFormat.OutputFormatString
                Return New iFTS2SingleLineFormat()
            Case iFTS2MultiLineFormat.OutputFormatString
                Return New iFTS2MultiLineFormat()
            Case OMAKASEFormat.OutputFormatString
                Return New OMAKASEFormat()
            Case CWSFormat.OutputFormatString
                Return New CWSFormat()
            Case BULKVNFormat.OutputFormatString
                Return New BULKVNFormat()
            Case GCMSFormat.OutputFormatString
                Return New GCMSFormat()
            Case iRTMSCIFormat.OutputFormatString
                Return New iRTMSCIFormat()
            Case iRTMSGCFormat.OutputFormatString
                Return New iRTMSGCFormat()
            Case iRTMSGDFormat.OutputFormatString
                Return New iRTMSGDFormat()
            Case iRTMSRMFormat.OutputFormatString
                Return New iRTMSRMFormat()

            Case OiRTMSCIFormat.OutputFormatString
                Return New OiRTMSCIFormat()
            Case OiRTMSGCFormat.OutputFormatString
                Return New OiRTMSGCFormat()
            Case OiRTMSGDFormat.OutputFormatString
                Return New OiRTMSGDFormat()
            Case OiRTMSRMFormat.OutputFormatString
                Return New OiRTMSRMFormat()

            Case OMAKASEFormatII.OutputFormatString
                Return New OMAKASEFormatII()

            Case GCMSPlusFormat.OutputFormatString
                Return New GCMSPlusFormat()
                'Funds Transfer Instruction Format (Hong Kong)
            Case FundsTransferInstructionFormat.OutputFormatString
                Return New FundsTransferInstructionFormat()
                'AutoDebit (HK)
            Case AutoDebitFormat.OutputFormatString
                Return New AutoDebitFormat()
                'AutoCheque (HK)
            Case AutoChequeFormat.OutputFormatString
                Return New AutoChequeFormat()

                '06062018 FTI II and AutoCheque ACMS with no header (FHK Maggie)
            Case AutoChequeFormatII.OutputFormatString
                Return New AutoChequeFormatII()
            Case FundsTransferInstructionFormatII.OutputFormatString
                Return New FundsTransferInstructionFormatII()

                'CARROT (Added by FHK 2013-03-05)
            Case CarrotCustomerDataFormat.OutputFormatString
                Return New CarrotCustomerDataFormat()
            Case CarrotInvoiceDataFormat.OutputFormatString
                Return New CarrotInvoiceDataFormat()

            Case GCMSPlusCreditDataFormat.OutputFormatString
                Return New GCMSPlusCreditDataFormat()

                'CFC (Added by FHK Maggie 2018-07-05)
            Case CashForecastingFormat.OutputFormatString
                Return New CashForecastingFormat()

                'FPS (Added by FHK Maggie 2018-07-18)
            Case FPSFormat.OutputFormatString
                Return New FPSFormat()

            Case Else
                Throw New MagicException("Undefined Output File Format! Valid Output File Formats: EPS, VPS, iFTS, CWS, BULKVN, CARROTCUSTOMER, CARROTINVOICE, GCMS, GCMS Plus, iFTS-2 SingleLine, iFTS-2 MultiLine, Mr.Omakase India, Mr.Omakase India II, iRTMSCI, iRTMSGC, iRTMSGD, iRTMSRM, OiRTMSCI, OiRTMSGC, OiRTMSGD, OiRTMSRM, Funds Transfer Instruction, Funds Transfer Instruction II, AutoDebit, AutoCheque, AutoCheque ACMS, Cash Forecasting, FPS")
        End Select

    End Function

#End Region

#Region "Private and Protected Methods"

    ''' <summary>
    '''#1. Copy Mapping - Duplicate the BankFields in DataModel, So as Not to Impact  DataModel by any chance
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CopyMapping()

        Try
            TblBankFields.Clear()

            For Each BankField As MapBankField In _DataModel.MapBankFields

                Dim dupBankField As New MapBankField()
                BankField.CopyTo(dupBankField)
                TblBankFields.Add(dupBankField.BankField, dupBankField)
                _PreviewTable.Columns.Add(New DataColumn(dupBankField.BankField, GetType(String)))

            Next

            'Sync these properties of BankFields (Common Template & Master Template)
            '1. DataLength
            '2. DefaultValue
            '3. DataSample
            '4. Mandatory
            '5. Map Separator
            '6. Include Decimal
            '7. Decimal Separator
            '8. Header
            '9. Detail
            '10. Trailer
            Dim _mstTmpUtility As New MasterTemplateSharedFunction

            If _MasterTemplateList.IsFixed Then

                Dim objmastertmp As New MasterTemplateFixed()
                objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
                   Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, _MasterTemplateList.TemplateName) _
                    , _mstTmpUtility.MasterTemplateFileExtension))

                For Each _masterFixedDetail As MasterTemplateFixedDetail In objmastertmp.MasterTemplateFixedDetailCollection

                    TblBankFields(_masterFixedDetail.FieldName).DefaultValue = _masterFixedDetail.DefaultValue
                    TblBankFields(_masterFixedDetail.FieldName).DataLength = _masterFixedDetail.DataLength
                    TblBankFields(_masterFixedDetail.FieldName).Mandatory = _masterFixedDetail.Mandatory
                    'TblBankFields(_masterFixedDetail.FieldName).MapSeparator = _masterFixedDetail.MapSeparator
                    TblBankFields(_masterFixedDetail.FieldName).IncludeDecimal = _masterFixedDetail.IncludeDecimal
                    TblBankFields(_masterFixedDetail.FieldName).DecimalSeparator = _masterFixedDetail.DecimalSeparator
                    TblBankFields(_masterFixedDetail.FieldName).Header = _masterFixedDetail.Header
                    TblBankFields(_masterFixedDetail.FieldName).Detail = _masterFixedDetail.Detail
                    TblBankFields(_masterFixedDetail.FieldName).Trailer = _masterFixedDetail.Trailer

                    If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                        TblBankFields(_masterFixedDetail.FieldName).BankSampleValue = ""
                    Else
                        TblBankFields(_masterFixedDetail.FieldName).BankSampleValue = _masterFixedDetail.DataSample
                    End If

                Next

            Else

                Dim objmastertmp As New MasterTemplateNonFixed()
                objmastertmp = objmastertmp.LoadFromFile2(String.Format("{0}{1}", _
                   Path.Combine(_mstTmpUtility.MasterTemplateViewFolder, _MasterTemplateList.TemplateName) _
                    , _mstTmpUtility.MasterTemplateFileExtension))

                'modified @ 7/6/2011
                'Add for consolidate field
                If TblBankFields.Count = objmastertmp.MasterTemplateNonFixedDetailCollection.Count - 1 Then
                    TblBankFields.Add("Consolidate Field", New MapBankField())
                    _PreviewTable.Columns.Add(New DataColumn("Consolidate Field", GetType(String)))
                End If


                For Each _masterFixedDetail As MasterTemplateNonFixedDetail In objmastertmp.MasterTemplateNonFixedDetailCollection

                    TblBankFields(_masterFixedDetail.FieldName).DefaultValue = _masterFixedDetail.DefaultValue
                    TblBankFields(_masterFixedDetail.FieldName).DataLength = _masterFixedDetail.DataLength
                    TblBankFields(_masterFixedDetail.FieldName).Mandatory = _masterFixedDetail.Mandatory
                    'TblBankFields(_masterFixedDetail.FieldName).MapSeparator = _masterFixedDetail.MapSeparator
                    TblBankFields(_masterFixedDetail.FieldName).Header = _masterFixedDetail.Header
                    TblBankFields(_masterFixedDetail.FieldName).Detail = _masterFixedDetail.Detail
                    TblBankFields(_masterFixedDetail.FieldName).Trailer = _masterFixedDetail.Trailer

                    If IsNothingOrEmptyString(_masterFixedDetail.DataSample) Then
                        TblBankFields(_masterFixedDetail.FieldName).BankSampleValue = ""
                    Else
                        TblBankFields(_masterFixedDetail.FieldName).BankSampleValue = _masterFixedDetail.DataSample
                    End If
                Next

            End If
        Catch exp As KeyNotFoundException
            Throw New Exception("Error on Mapping(CopyMapping) : " & vbCrLf & "Common Template or Master Template used is not compatible with this version. Please check the followings:" _
                                    & vbCrLf & "1.) Ensure the program version is later than 3.13." _
                                    & vbCrLf & "2.) Ensure the 'Consolidate Field' field is defined in master template." _
                                    & vbCrLf & "3.) Ensure the'Consolidate Field' is listed at the end of bank fields list in Common template mapping.")
        Catch ex As Exception
            Throw New Exception("Error on Mapping(CopyMapping) : " & ex.Message.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' #2. Read-in Source Values to BankFieds per Mapping and Add the record to "PreviewData" Data Table 
    ''' iFTS-2 MultiLine and OMAKASE File Formats have different implementations for this function!
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ReadAllSourceData()

        Try
            Dim NewPreviewRecord As DataRow

            For Each sourcerecord As DataRowView In _SourceData

                NewPreviewRecord = _PreviewTable.NewRow()

                For Each BankField As MapBankField In TblBankFields.Values
                    For Each SourceField As MapSourceField In BankField.MappedSourceFields
                        SourceField.SourceFieldValue = System.Convert.ToString(sourcerecord(SourceField.SourceFieldName))
                    Next
                Next


                For Each _column As String In TblBankFields.Keys

                    NewPreviewRecord(_column) = TblBankFields(_column).MappedValues

                Next

                _PreviewTable.Rows.Add(NewPreviewRecord)

            Next 'Next data row from Source Data Table 
        Catch ex As Exception
            Throw New Exception("Error reading Source Data(ReadAllSourceData) : " & ex.Message.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' #4. Translation - Translate Bank Fields
    ''' No Clear Input from Customer on how they've been practising the Translation
    ''' especially when BankFields have more than one Customer Fields Mapped! 
    ''' Are they translating the Combined Source Field values of BankField or One of the Source Fields?
    ''' Current Logic: Tranlate the Combined Source Field Values of a Bank Field to Target Value (Bank Value)
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ApplyTranslation()

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each translatedBankField As TranslatorSetting In _DataModel.TranslatorSettings

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If translatedBankField.CustomerValue.Equals(_previewRecord(translatedBankField.BankFieldName).ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Translation Settings(ApplyTranslation) : {0}", ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' #5. Apply Lookup
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ApplyLookup()

        Try
            Dim _rowindex As Int32 = 0
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each lookupBankField As LookupSetting In _DataModel.LookupSettings
                    _previewRecord(lookupBankField.BankFieldName) = GetLookupValue(lookupBankField.TableName, _SourceData.Item(_rowindex)(lookupBankField.SourceFieldName), lookupBankField.LookupValue.Split("-")(0), lookupBankField.LookupKey.Split("-")(0))
                    If Not IsNothingOrEmptyString(lookupBankField.TableName2) Then
                        _previewRecord(lookupBankField.BankFieldName) = _previewRecord(lookupBankField.BankFieldName) & GetLookupValue(lookupBankField.TableName2, _SourceData.Item(_rowindex)(lookupBankField.SourceFieldName), lookupBankField.LookupValue2.Split("-")(0), lookupBankField.LookupKey2.Split("-")(0))
                    End If
                    If Not IsNothingOrEmptyString(lookupBankField.TableName3) Then
                        _previewRecord(lookupBankField.BankFieldName) = _previewRecord(lookupBankField.BankFieldName) & GetLookupValue(lookupBankField.TableName3, _SourceData.Item(_rowindex)(lookupBankField.SourceFieldName), lookupBankField.LookupValue3.Split("-")(0), lookupBankField.LookupKey3.Split("-")(0))
                    End If
                Next
                _rowindex += 1
            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Lookup Settings(ApplyLookup) : {0}", ex.Message))
        End Try

    End Sub

    Protected Function GetLookupValue(ByVal tableName As String, ByVal conditionValue As String, ByVal selectColumn As String, ByVal conditionColumn As String) As String

        If tableName.Trim() = "" Then Throw New MagicException(" Table Name is Empty!, Please ensure there exists a Lookup Table")
        If selectColumn.Trim() = "" Then Throw New MagicException(" Lookup Value Column Name is empty!, Please specify a lookup Value Column Name")
        If conditionColumn.Trim() = "" Then Throw New MagicException(" Lookup Key Column name is empty!, Please specify a Lookup Key Column Name")

        Dim strQuery = String.Format(" SELECT [{0}] FROM [{1}] WHERE [{2}] = '{3}' " _
                                , selectColumn, tableName, conditionColumn, conditionValue)

        Return HelperModule.QueryTable(strQuery, dbConnectionString)

    End Function

    ''' <summary>
    '''  #6. Apply Calculation 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ApplyCalculation()

        Dim _calvalue As Decimal = 0, rowIndex As Int32 = 0

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each calculatedBankField As CalculatedField In _DataModel.CalculatedFields

                    If calculatedBankField.Operator1 = "+" Then
                        _calvalue = System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand1)) _
                                    + System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand2))
                    ElseIf calculatedBankField.Operator1 = "-" Then
                        _calvalue = System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand1)) _
                                    - System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand2))
                    ElseIf calculatedBankField.Operator1 = "*" Then
                        _calvalue = System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand1)) _
                                    * System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand2))
                    Else
                        _calvalue = System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand1)) _
                                    / System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand2))
                    End If
                    If calculatedBankField.Operator2 = "+" Then
                        _calvalue = _calvalue + System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "-" Then
                        _calvalue = _calvalue - System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "*" Then
                        _calvalue = _calvalue * System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "/" Then
                        _calvalue = _calvalue / System.Convert.ToDecimal(_SourceData(rowIndex)(calculatedBankField.Operand3))
                    End If

                    _previewRecord(calculatedBankField.BankFieldName) = _calvalue.ToString()

                Next

                rowIndex = rowIndex + 1
            Next
        Catch fex As FormatException
            Throw New Exception(String.Format("Error applying Calculation Settings(ApplyCalculation) at Row no: {0}, The value is not a number !", rowIndex))
        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Calculation Settings(ApplyCalculation) : {0}", ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' #7. Apply String Manipulation
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ApplyStringManipulation()

        Try
            Dim _svalue As String

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each manipulatedBankField As StringManipulation In _DataModel.StringManipulations

                    _svalue = _previewRecord(manipulatedBankField.BankFieldName)

                    If _svalue = String.Empty Then Continue For

                    Select Case manipulatedBankField.FunctionName
                        ''''''''''''''''''''''''''modified by Jacky on 2018-11-07''''''''''''''''''''''
                        'added : '
                        Case "LEFT"
                            _svalue = Left(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "RIGHT"
                            _svalue = Right(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "SUBSTRING"
                            _svalue = Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value)
                        Case "REMOVE"
                            ''''''''''Modified by jacky on 2019-07-15'''''''''
                            'Purpose: Replace function should only do once
                            '_svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "")
                            _svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "", , 1)
                            ''''''''''''''''''''''''''''''''
                        Case "REMOVE LAST"
                            If _svalue.Length > manipulatedBankField.NumberOfCharacters.Value Then
                                _svalue = _svalue.Substring(0, _svalue.Length - manipulatedBankField.NumberOfCharacters.Value)
                            Else
                                _svalue = ""
                            End If
                        Case "TRIM"
                            _svalue = _svalue.Trim
                        Case "LEFT & TRIM"
                            _svalue = Left(_svalue, manipulatedBankField.NumberOfCharacters.Value).Trim
                        Case "RIGHT & TRIM"
                            _svalue = Right(_svalue, manipulatedBankField.NumberOfCharacters.Value).Trim
                        Case "SUBSTRING & TRIM"
                            _svalue = Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value).Trim
                        Case "REMOVE & TRIM"
                            ''''''''''Modified by jacky on 2019-07-15'''''''''
                            'Purpose: Replace function should only do once
                            '_svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "")
                            _svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "", , 1).Trim
                            '''''''''''''''''''''''''''''''''''
                        Case "REMOVE LAST & TRIM"
                            If _svalue.Length > manipulatedBankField.NumberOfCharacters.Value Then
                                _svalue = _svalue.Substring(0, _svalue.Length - manipulatedBankField.NumberOfCharacters.Value).Trim
                            Else
                                _svalue = ""
                            End If

                    End Select

                    _previewRecord(manipulatedBankField.BankFieldName) = _svalue

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying String Manipulation Settings(ApplyStringManipulation) : {0}", ex.Message))
        End Try

    End Sub

    Private Sub TranslateDBNullToEmptyString()

        For Each row As DataRow In _PreviewTable.Rows

            For Each col As DataColumn In _PreviewTable.Columns
                If IsNothingOrEmptyString(row(col.ColumnName)) Then
                    row(col.ColumnName) = String.Empty
                ElseIf row(col.ColumnName) Is DBNull.Value Then
                    row(col.ColumnName) = String.Empty
                End If
            Next

        Next

    End Sub


    ''' <summary>
    ''' #8. If Master Template is a Fixed Width Master Template, 'Adjust the Width of 
    ''' Numeric and Currency Values so as to fit the Decimal Length in FixedWidthMaster Template
    ''' - Different Output Formats have their own rules to get the data comply by 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Function FixNumericData() As Integer
        Return 0
    End Function

    ''' <summary>
    ''' #9. Fill Sector Selection - Valid for GCMS Format only
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub GenerateSectorSelection()

    End Sub

    ''' <summary>
    ''' #10. Auto String manipulation for Omakase II format only
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub BeneAddressStringManipulation()

    End Sub

    'Eliminate Given Characters, if given
    Private Sub RemoveEliminatedCharacter()

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            For Each _column As String In TblBankFields.Keys
                If _previewRecord(_column) <> "" Then _previewRecord(_column) = _previewRecord(_column).ToString().Replace(_EliminatedChar, "")
            Next

        Next

    End Sub

    ''' <summary>
    ''' Validates Mandatory Fields
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ValidateMandatoryFields()

        Try
            Dim RowNumber As Int32 = 0

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each _column As String In TblBankFields.Keys

                    If FileFormatName = "EPS" And _column = "Indicator" Then Continue For

                    If TblBankFields(_column).Mandatory AndAlso IsNothingOrEmptyString(_previewRecord(_column).ToString()) Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = _column
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1

                        If TblBankFields(_column).Header AndAlso RowNumber = 0 Then
                            validationError.Description = String.Format(MsgReader.GetString("E09030160"), "Header", "", _column)
                        ElseIf TblBankFields(_column).Trailer AndAlso ((RowNumber + 1) = _PreviewTable.Rows.Count) Then
                            validationError.Description = String.Format(MsgReader.GetString("E09030160"), "Trailer", "", _column)
                        Else ' all records other than first and last
                            validationError.Description = String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column)
                        End If

                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Validates Data Length of Certain Fields
    ''' Error Code : E09020050
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub ValidateDataLength()

        ' This is for File Formats using Non Fixed Master Template: CWS, BULKVN, GCMS, iRTMS, iFTS,iFTS-2 SingleLine, iFTS-2 MultiLine, OMAKASE I & II,GCMS Plus, HK-RTMS
        Try
            Dim RowNumber As Int32 = 0
            Dim i As Integer
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                i = 0
                For Each _column As String In TblBankFields.Keys

                    If _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value Then

                        'HK-RTMS Formats has conditional length validation for some fields
                        'Funds Transfer Instruction Import
                        If _MasterTemplateList.OutputFormat.Equals(FundsTransferInstructionFormat.OutputFormatString) And _
                                                   (_column.Equals("Beneficiary Account")) Then
                            'skip length vailation
                            Continue For
                        End If
                        'Auto Debit Import
                        If _MasterTemplateList.OutputFormat.Equals(AutoDebitFormat.OutputFormatString) And _
                                                   (_column.Equals("Account Number")) Then
                            'skip length vailation
                            Continue For
                        End If



                        'GCMS Plus does not require length checking for temp fields
                        If _MasterTemplateList.OutputFormat.Equals(GCMSPlusFormat.OutputFormatString) And _
                            ((i >= 35 And i < 44) OrElse _column.Equals("Information To Remitting Bank")) Then
                            'no validation required
                        Else

                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = _column
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1

                            If TblBankFields(_column).Header AndAlso RowNumber = 0 Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            ElseIf TblBankFields(_column).Trailer AndAlso ((RowNumber + 1) = _PreviewTable.Rows.Count) Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            Else ' If detail record
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = RowNumber + 1
                            End If

                            _ValidationErrors.Add(validationError)

                            'Certain Fileformats shall not set the mandatoryerror flag when their Amount Value exceeds predefined size
                            If _column.Equals(AmountFieldName, StringComparison.InvariantCultureIgnoreCase) Then

                                Select Case FileFormatName
                                    Case GCMSFormat.OutputFormatString, BULKVNFormat.OutputFormatString, iFTSFormat.OutputFormatString
                                        'Ignore _erroratmandatoryfield
                                    Case Else
                                        If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True
                                End Select

                            Else
                                If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True
                            End If

                        End If
                    End If
                    i += 1
                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Data Length: " & ex.Message.ToString)
        End Try

    End Sub



    Protected Function IsAutoTrimmable(ByVal field As String) As Boolean

        For Each _trimmablefield As String In AutoTrimmableFields
            If _trimmablefield.Equals(field, StringComparison.InvariantCultureIgnoreCase) Then Return True
        Next

        Return False

    End Function

    ''' <summary>
    ''' Default Value of Currency and Decimal Length could be in this format JPY-2,SGD,MYR-3
    ''' The TblDecimalLengthForCurrency will have Currency and Decimal Length for the given common template
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateDecimalLengthForCurrency()

        If Not TblBankFields.ContainsKey("Currency") Then Exit Sub

        For Each currency As String In TblBankFields("Currency").DefaultValue.Split(",")

            Dim values() As String = currency.Split("-")
            If values.Length = 2 Then
                Try
                    TblDecimalLengthForCurrency.Add(values(0), Convert.ToInt32(values(1)))
                Catch ex As Exception
                    'dont bother
                End Try
            End If

        Next

    End Sub

    Private Function IsUnique(ByRef values As List(Of String)) As Boolean

        If values.Count <= 1 Then Return True

        For idx As Integer = 0 To values.Count - 2
            For jdx As Integer = idx + 1 To values.Count - 1
                If values(idx).Equals(values(jdx), StringComparison.InvariantCultureIgnoreCase) Then Return False
            Next
        Next

        Return True

    End Function

    Private Function IsValidGroupByField(ByVal afield As String) As Boolean

        For Each gfield As String In ValidGroupByFields

            If gfield.Equals(afield, StringComparison.InvariantCultureIgnoreCase) Then Return True

        Next

        Return False

    End Function

    Private Function IsValidReferenceField(ByVal afield As String) As Boolean

        For Each gfield As String In ValidReferenceFields

            If gfield.Equals(afield, StringComparison.InvariantCultureIgnoreCase) Then Return True

        Next

        Return False

    End Function

#End Region

    ' Quick Conversion Setup File might have incorrect Group By and Reference Fields 
    Protected Sub ValidateGroupByAndReferenceFields(ByRef _groupBys As List(Of String), ByRef _ReferenceBys As List(Of String))

        Dim _field As String = ""

        '1. Are Group By/Reference Fields unique
        If Not IsUnique(_groupBys) Then Throw New MagicException("Duplicate Group By Fields were found!, Group By Fields must be unique.")

        If Not IsUnique(_ReferenceBys) Then Throw New MagicException("Duplicate Reference Fields were found!, Reference Fields must be unique.")

        '2. Are Group By/Reference Fields valid for the specific FileFormat
        For Each _field In _groupBys
            If Not IsValidGroupByField(_field) Then Throw New MagicException(String.Format("Group by field ""{0}"" is not a valid Group By Field for ""{1}""", _field, FileFormatName))
        Next

        For Each _field In _ReferenceBys
            If Not IsValidReferenceField(_field) Then Throw New MagicException(String.Format("Reference field ""{0}"" is not a valid Reference Field for ""{1}""", _field, FileFormatName))
        Next

    End Sub

    Protected Function GetDecimalLengthForCurrency(ByVal CurrencyCode As String) As Int32

        If TblDecimalLengthForCurrency.ContainsKey(CurrencyCode) Then
            Return TblDecimalLengthForCurrency(CurrencyCode)
        Else
            Return 0
        End If

    End Function

    Protected Sub AddValidationError(ByVal strField As String, ByVal intFieldPosition As Int32, ByVal strErrorDesc As String, ByVal intRecordNo As Int32, Optional ByVal IsErrorAtMandatoryField As Boolean = False)

        Dim validationError As New FileFormatError()
        validationError.ColumnName = strField
        validationError.ColumnOrdinalNo = intFieldPosition
        validationError.Description = strErrorDesc
        validationError.RecordNo = intRecordNo
        _ValidationErrors.Add(validationError)
        If IsErrorAtMandatoryField Then _ErrorAtMandatoryField = True

    End Sub

#Region "Friend Members"

    Friend Shared NonSWIFTCharacters() As Char = { _
             Chr(33), Chr(34), Chr(35), Chr(36), Chr(37), Chr(38), Chr(42), Chr(59), Chr(60) _
             , Chr(61), Chr(62), Chr(91), Chr(92), Chr(93), Chr(94), Chr(95), Chr(96), Chr(123) _
             , Chr(124), Chr(125), Chr(126), Chr(127) _
         }

    'Locations where : or - are not expected
    Friend Shared ReplaceCharLocs() As Int32 = {0, 35, 70, 105}

    Friend Shared Function RemoveNonSWIFTCharacters(ByVal sFieldValue As String) As String

        sFieldValue = sFieldValue.Replace("&", "AND")
        sFieldValue = sFieldValue.Replace("@", "AT")

        For Each nonSwiftChar As Char In NonSWIFTCharacters
            sFieldValue = sFieldValue.Replace(nonSwiftChar, " ")
        Next

        Dim replaceValue As New System.Text.StringBuilder(sFieldValue)

        For Each index As Int32 In ReplaceCharLocs

            If index < replaceValue.Length Then
                If replaceValue(index) = "-" Or replaceValue(index) = ":" Then replaceValue(index) = " "
            End If

        Next

        sFieldValue = replaceValue.ToString()
        replaceValue = Nothing

        Return sFieldValue

    End Function
    Friend Shared Function RemoveGCMSPlusInvalidCharacters(ByVal sFieldValue As String) As String
        'Dim regAllow As New Regex("[^0-9A-Za-z/?(),.'+:\-\s]", RegexOptions.Compiled)

        'If any unpermitted characters were found, MAGIC will replace it with a space.
        For Each nonSwiftChar As Char In NonSWIFTCharacters
            sFieldValue = sFieldValue.Replace(nonSwiftChar, " ")
        Next


        Return sFieldValue

    End Function

    Friend Shared Function RemoveHyphenColon(ByVal sFieldValue As String) As String

        'Hyphen (-) and Colon (:), if present at positions 1/36/71/106 will be replaced by a space for all text data type fields.
        Dim replaceValue As New System.Text.StringBuilder(sFieldValue)
        For Each index As Int32 In ReplaceCharLocs

            If index < replaceValue.Length Then
                If replaceValue(index) = "-" Or replaceValue(index) = ":" Then replaceValue(index) = " "
            End If

        Next
        sFieldValue = replaceValue.ToString()
        replaceValue = Nothing

        Return sFieldValue

    End Function

    Friend Shared Function RemoveIRTMSNonSWIFTCharacters(ByVal sFieldValue As String) As String
        'created for O/iRTMS-RM enhancement @ 08/03/2011
        ' iRTMS doesn't need to check '-' and ':' for all fields
        sFieldValue = sFieldValue.Replace("&", "AND")
        sFieldValue = sFieldValue.Replace("@", "AT")

        For Each nonSwiftChar As Char In NonSWIFTCharacters
            sFieldValue = sFieldValue.Replace(nonSwiftChar, " ")
        Next

        Return sFieldValue

    End Function

#End Region

End Class