'************************************************CHANGE REQUESTS**********************************
'SRS No                     Date Modified   Description
'--------------------------------------------------------------------------------------------------
'SRS/BTMU/2010-0018         01/Apr/2010     Add Sector Selection Logic for India
'**************************************************************************************************
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the GCMS Format
''' </summary>
''' <remarks></remarks>
Public Class GCMSFormat
    Inherits BaseFileFormat

    Private MandatoryFields() As String = {"Beneficiary Bank", "Beneficiary Name", "Message To Beneficiary", _
                                "Information To Remitting Bank"}

    Private Shared _amountFieldName As String = "Remittance Amount"

    Private Shared _editableFields() As String = {"Settlement Account No", "Value Date", _
          "Customer Reference", "Sector Selection", "Currency", "Remittance Amount", _
          "Exchange Method", "Contract Number", "Intermediary Bank/Branch", _
          "Intermediary Bank Master Code", "Beneficiary Bank", "Beneficiary Bank Master Code", _
          "Beneficiary A/C No", "Beneficiary Name", "Message To Beneficiary", "Purpose of Remittance", _
          "Information To Remitting Bank", "Bank Charges", "Charges Account Number"}

    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Settlement Account No", "Value Date", _
            "Customer Reference", "Sector Selection", "Currency", "Remittance Amount", _
            "Exchange Method", "Contract Number", "Intermediary Bank/Branch", _
            "Intermediary Bank Master Code", "Beneficiary Bank", "Beneficiary Bank Master Code", _
            "Beneficiary A/C No", "Beneficiary Name", "Message To Beneficiary", "Purpose of Remittance", _
            "Information To Remitting Bank", "Bank Charges", "Charges Account Number", "Consolidate Field"}

    Private Shared _autoTrimmableFields() As String = {"Value Date", "Customer Reference", "Sector Selection", _
            "Currency", "Exchange Method", "Contract Number", "Intermediary Bank/Branch", _
            "Intermediary Bank Master Code", "Beneficiary Bank", "Beneficiary Bank Master Code", _
            "Beneficiary Name", "Message To Beneficiary", "Purpose of Remittance", "Local Country", _
            "Information To Remitting Bank", "Bank Charges"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "GCMS"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

#End Region


    Public Shared Function OutputFormatString() As String
        Return "GCMS"
    End Function

#Region "Overriden Methods"

    ''' <summary>
    ''' For "Sector Selection" = "Domestic" make up "Sequence No" = "A01XXXXXX", A01000001, A01000002,..
    ''' For "Sector Selection" = "Book Transfer" make up "Sequence No" = "A02XXXXXX", A02000001, A02000002...
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FormatSequenceNo()
        Try

            Dim iNoDomestic As Integer = 1 ' SequenceNo for Sector Selection = "Domestic" 
            Dim iNoBookTransfer As Integer = 1 ' SequenceNo for Sector Selection = "BookTransfer" 
            Dim _counter As Int32 = 0 'Loop control

            If TblBankFields.ContainsKey("Sector Selection") Then

                While _counter < _PreviewTable.Rows.Count

                    If _PreviewTable.Rows(_counter)("Sector Selection").ToString() = "International" Then

                        MsgBox("There is [Sector Selection] = 'International'. " & _
                            "So the row which contains [Sector Selection] = 'International' will not be previewed or converted.", vbExclamation, "Generate Common Transaction Template")

                    End If

                    Select Case _PreviewTable.Rows(_counter)("Sector Selection").ToString()

                        Case "Domestic"

                            If TblBankFields.ContainsKey("FT Beneficiary Account No") Then
                                _PreviewTable.Rows(_counter)("FT Beneficiary Account No") = ""
                            End If

                            If TblBankFields.ContainsKey("Sequence No") Then
                                _PreviewTable.Rows(_counter)("Sequence No") = "A01" & iNoDomestic.ToString().PadLeft(6, "0")
                                iNoDomestic = iNoDomestic + 1
                            End If

                        Case "Book Transfer"

                            If TblBankFields.ContainsKey("LLG Beneficiary Account No") Then
                                _PreviewTable.Rows(_counter)("LLG Beneficiary Account No") = ""
                            End If

                            If TblBankFields.ContainsKey("Sequence No") Then
                                _PreviewTable.Rows(_counter)("Sequence No") = "A02" & iNoBookTransfer.ToString().PadLeft(6, "0")
                                iNoBookTransfer = iNoBookTransfer + 1
                            End If

                        Case Else

                            ' Delete this row of preview data
                            _PreviewTable.Rows.RemoveAt(_counter)
                            _counter = _counter - 1

                    End Select

                    _counter = _counter + 1

                End While

            End If ' end of if sector selection exists?

        Catch ex As Exception
            Throw New Exception("Error on formatting Sequence No : " & ex.Message.ToString)
        End Try

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Fill Fields Exchange Method, Charges Account Number, and Contract Number
                If TblBankFields.ContainsKey("Exchange Method") And TblBankFields.ContainsKey("Contract Number") Then

                    'Exchange Method Field (settlement account currency and transaction currency are same)
                    'added for GCMS-OVS settlement account no. format checking.
                    'GCMS smile > currency code is end of the settlement account
                    'GCMS OVS > currency code is position 5th to 7th (index starts from one )

                    'Fixed during UAT (check length < 7 for both of OVS & Smile GCMS)
                    'confirmed by Casey on phone @ 13th May 2011
                    If Not _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                        AndAlso _previewRecord("Settlement Account No").ToString().Length < 7 Then

                        'Do nothing
                    Else

                        If _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                            OrElse _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then
                            _previewRecord("Exchange Method") = ""
                        Else
                            _previewRecord("Exchange Method") = IIf(_previewRecord("Contract Number").ToString() = "", "Spot", "Cont")
                        End If

                        If _previewRecord("Bank Charges").ToString().Equals("Ben", StringComparison.CurrentCultureIgnoreCase) Then
                            _previewRecord("Charges Account Number") = ""
                        End If

                        If _previewRecord("Exchange Method").ToString() = "" Or _previewRecord("Exchange Method").ToString().Equals("Spot", StringComparison.CurrentCultureIgnoreCase) Then
                            _previewRecord("Contract Number") = ""
                        End If

                    End If

                End If


                '#2. Format Remittance Amount 
                If _previewRecord("Remittance Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()) / 100
                    End If

                    Select Case _previewRecord("Currency").ToString().ToUpper()

                        Case "TND", "KWD"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.000")

                        Case "TWD", "JPY", "KRW", "VND"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.00")

                    End Select

                End If


                '#3. Remove Non SWIFT Characters
                For i As Int32 = 0 To 18
                    If i <> 1 And i <> 5 Then
                        _previewRecord(i) = RemoveNonSWIFTCharacters(_previewRecord(i).ToString())
                    End If
                Next

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Fill Fields Exchange Method, Charges Account Number, and Contract Number
                If TblBankFields.ContainsKey("Exchange Method") And TblBankFields.ContainsKey("Contract Number") Then

                    'Exchange Method Field (settlement account currency and transaction currency are same)
                    'added for GCMS-OVS settlement account no. format checking.
                    'GCMS smile > currency code is end of the settlement account
                    'GCMS OVS > currency code is position 5th to 7th (index starts from one )

                    'Fixed during UAT (check length < 7 for both of OVS & Smile GCMS)
                    'confirmed by Casey on phone @ 13th May 2011
                    If Not _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                        AndAlso _previewRecord("Settlement Account No").ToString().Length < 7 Then

                        'Do nothing
                    Else
                        If _previewRecord("Settlement Account No").ToString().EndsWith(_previewRecord("Currency").ToString(), StringComparison.CurrentCultureIgnoreCase) _
                            OrElse _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then

                            _previewRecord("Exchange Method") = ""
                        Else
                            _previewRecord("Exchange Method") = IIf(_previewRecord("Contract Number").ToString() = "", "Spot", "Cont")
                        End If

                        If _previewRecord("Bank Charges").ToString().Equals("Ben", StringComparison.CurrentCultureIgnoreCase) Then
                            _previewRecord("Charges Account Number") = ""
                        End If

                        If _previewRecord("Exchange Method").ToString() = "" Or _previewRecord("Exchange Method").ToString().Equals("Spot", StringComparison.CurrentCultureIgnoreCase) Then
                            _previewRecord("Contract Number") = ""
                        End If

                    End If

                End If


                '#2. Format Remittance Amount 
                If _previewRecord("Remittance Amount").ToString <> String.Empty Then

                    Select Case _previewRecord("Currency").ToString().ToUpper()

                        Case "TND", "KWD"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.000")

                        Case "TWD", "JPY", "KRW", "VND"
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Remittance Amount") = Convert.ToDecimal(_previewRecord("Remittance Amount").ToString()).ToString("###########0.00")

                    End Select

                End If


                '#3. Remove Non SWIFT Characters
                For i As Int32 = 0 To 18
                    If i <> 1 And i <> 5 Then
                        _previewRecord(i) = RemoveNonSWIFTCharacters(_previewRecord(i).ToString())
                    End If
                Next

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Remittance Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Data Type and Custom Validations other than Data Length and Mandatory Field Validations
    Protected Overrides Sub Validate()

        Try

            Dim RowNumber As Int32 = 0
            Dim _fieldValue As String

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                '[CR] During Settlement A/C for OVS enhancement
                '#0. Settlement A/C currency is valid or not (check against the default currency value list)
                If _previewRecord("Settlement Account No").ToString() <> ""  Then
                    'Fixed during UAT (check length < 7 for both of OVS & Smile GCMS)
                    'confirmed by Casey on phone @ 13th May 2011
                    If _previewRecord("Settlement Account No").ToString.Length < 7 Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Settlement Account No"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account No").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: Invalid Settlement Account No. Data length is too short to verify the Currency Code.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    Else

                        'Check for SMILE first
                        'Check for OVS again
                        'OVS needs to check the currency code at positions 5-7, so minimum length required is 7
                        If Not IsValidSettlementAC(_previewRecord("Settlement Account No").ToString.Substring(_previewRecord("Settlement Account No").ToString.Length - 3, 3)) _
                            AndAlso Not IsValidSettlementAC(_previewRecord("Settlement Account No").ToString.Substring(4, 3)) Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Settlement Account No"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account No").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: Invalid Settlement Account No Currency Code.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True

                        End If

                    End If
                End If

                    '#1. Validate Beneficiary Bank , Beneficiary Name , Message To Beneficiary , Information To Remitting Bank
                    For Each mandatoryField As String In MandatoryFields

                        _fieldValue = _previewRecord(mandatoryField).ToString()

                        For Each index As Int32 In ReplaceCharLocs

                            If index < _fieldValue.Length Then

                                If _fieldValue(index) = "-" Or _fieldValue(index) = ":" Then

                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = mandatoryField
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns(mandatoryField).Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: '{1}' cannot contain hyphen[-] or colon[:] at positions 1/36/71/106.", RowNumber + 1, mandatoryField)
                                    validationError.RecordNo = RowNumber + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True

                                End If

                            End If
                        Next

                    Next

                    '#2. Purpose of Remittance and Information to Remitting Bank
                    If _previewRecord("Purpose of Remittance").ToString() <> "" _
                        And _previewRecord("Information To Remitting Bank").ToString().Length > 70 Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Information To Remitting Bank"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Information To Remitting Bank").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Information To Remitting Bank' cannot contain more than 70 characters when 'Purpose of Remittance' is given.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                    '#3. Charges Account No. "Bank Charges"  "Charges Account Number"
                    If ( _
                        _previewRecord("Bank Charges").ToString().Equals("OUR", StringComparison.CurrentCultureIgnoreCase) _
                        Or _previewRecord("Bank Charges").ToString().Equals("SHA", StringComparison.CurrentCultureIgnoreCase) _
                        ) _
                        And _
                        _previewRecord("Charges Account Number").ToString().Equals(_previewRecord("Settlement Account No").ToString(), StringComparison.CurrentCultureIgnoreCase) Then

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Charges Account Number"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Charges Account Number").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Charges Account Number' cannot be same as the 'Settlement Account No.' when 'Bank Charges' is 'OUR' or 'SHA'.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                    '#4. Remittance Amount - Commented by Meera -- Need to know
                'If Not IsConsolidated AndAlso _previewRecord("Remittance Amount").ToString() <> "" _
                '                        AndAlso Val(_previewRecord("Remittance Amount").ToString()) <= 0 Then

                '    Dim validationError As New FileFormatError()
                '    validationError.ColumnName = "Remittance Amount"
                '    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Remittance Amount").Ordinal + 1
                '    validationError.Description = String.Format("Record: {0} Field: 'Remittance Amount' should not be zero or negative.", RowNumber + 1)
                '    validationError.RecordNo = RowNumber + 1
                '    _ValidationErrors.Add(validationError)

                'End If



                    '#5. Value Date
                    Dim _ValueDate As Date
                    If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then

                        If _ValueDate < Date.Today Or _ValueDate > Date.Today.AddDays(21) Then

                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Value Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be equal to or greater than today and less than 21 calendar days from today's date.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True

                        End If

                    Else

                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Value Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be a valid date.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If

                    '#6. Validation - Currency : Must be one of these values(SGD, IDR, JPY,USD, GBP, EUR,HKD, AUD, CHF,THB, VND, PHP,MYR, NZD, PGK)
                    If _previewRecord("Currency").ToString <> String.Empty Then
                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                            If TblBankFields("Currency").DefaultValue.Length > 0 Then
                                Dim defaultCurrency() As String
                                defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                                For Each val As String In defaultCurrency
                                    If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next
                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Currency"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid 'Currency'.", RowNumber + 1)
                                    validationError.RecordNo = RowNumber + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If


                    '#7. Validation - Sector Selection : Must be one of these values(Book Transfer, Domestic, International)
                    If _previewRecord("Sector Selection").ToString <> String.Empty Then
                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Sector Selection").DefaultValue) Then
                            If TblBankFields("Sector Selection").DefaultValue.Length > 0 Then
                                Dim defaultSector() As String
                                defaultSector = TblBankFields("Sector Selection").DefaultValue.Split(",")
                                For Each val As String In defaultSector
                                    If _previewRecord("Sector Selection").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next
                                If Not isFound Then
                                    Dim validationError As New FileFormatError()
                                    validationError.ColumnName = "Sector Selection"
                                    validationError.ColumnOrdinalNo = _PreviewTable.Columns("Sector Selection").Ordinal + 1
                                    validationError.Description = String.Format("Record: {0} Field: 'Sector Selection' is not a valid 'Sector Selection'.", RowNumber + 1)
                                    validationError.RecordNo = RowNumber + 1
                                    _ValidationErrors.Add(validationError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    'Additional Validations

                    ' Bank Charges
                    If _previewRecord("Bank Charges").ToString.ToUpper <> "BEN" And _
                       _previewRecord("Bank Charges").ToString.ToUpper <> "OUR" And _
                       _previewRecord("Bank Charges").ToString.ToUpper <> "SHA" Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Bank Charges"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Bank Charges").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Bank Charges' should be either 'BEN' or 'SHA' or 'OUR'.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                    If _previewRecord("Bank Charges").ToString.ToUpper = "BEN" And _
                       _previewRecord("Charges Account Number").ToString <> "" Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Charges Account Number"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Charges Account Number").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Charges Account Number' should be blank if 'Bank Charges' is 'BEN'.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                    _previewRecord("Bank Charges") = _previewRecord("Bank Charges").ToString.ToUpper

                'Fixed during UAT (check length < 7 for both of OVS & Smile GCMS)
                'confirmed by Casey on phone @ 13th May 2011
                If _previewRecord("Settlement Account No").ToString.Length < 7 Then
                    'Need to check Exchange Method and Contract No.
                    'assumed that a/c currency and transaction currency are different
                    If _previewRecord("Contract Number").ToString <> "" And _
                                             _previewRecord("Exchange Method").ToString.ToUpper <> "CONT" Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Exchange Method"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be 'Cont'.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                    If _previewRecord("Contract Number").ToString = "" And _
                       _previewRecord("Exchange Method").ToString.ToUpper <> "SPOT" Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Exchange Method"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                        validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be 'Spot'.", RowNumber + 1)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                Else

                    'Exchange Method Field (settlement account currency and transaction currency are same)
                    'added for GCMS-OVS settlement account no. format checking.
                    'GCMS smile > currency code is end of the settlement account
                    'GCMS OVS > currency code is position 5th to 7 th (index starts from one )
                    If _previewRecord("Settlement Account No").ToString.ToUpper.EndsWith(_previewRecord("Currency").ToString.ToUpper) _
                     Or _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then
                        If _previewRecord("Exchange Method").ToString <> "" Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Exchange Method"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                            validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be blank if the currency in 'Settlement A/C No.' is the same as the transaction currency.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                    'End If

                    'Exchange Method Field (settlement account currency and transaction currency do not match)
                    'added for GCMS-OVS settlement account no. format checking.
                    'GCMS smile > currency code is end of the settlement account
                    'GCMS OVS > currency code is position 5th to 7 th (index starts from one )
                    If Not _previewRecord("Settlement Account No").ToString.ToUpper.EndsWith(_previewRecord("Currency").ToString.ToUpper) _
                        And Not _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then

                        If _previewRecord("Contract Number").ToString <> "" And _
                           _previewRecord("Exchange Method").ToString.ToUpper <> "CONT" Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Exchange Method"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                            validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be 'Cont'.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                        If _previewRecord("Contract Number").ToString = "" And _
                           _previewRecord("Exchange Method").ToString.ToUpper <> "SPOT" Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Exchange Method"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                            validationError.Description = String.Format("Record: {0}, Field: 'Exchange Method' should be 'Spot'.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If


                    'Contract No Field
                    'added for GCMS-OVS settlement account no. format checking.
                    'GCMS smile > currency code is end of the settlement account
                    'GCMS OVS > currency code is position 5th to 7th (index starts from one )
                    If _previewRecord("Settlement Account No").ToString.ToUpper.EndsWith(_previewRecord("Currency").ToString.ToUpper) _
                        Or _previewRecord("Settlement Account No").ToString.Substring(4, 3).ToUpper.Equals(_previewRecord("Currency").ToString.ToUpper) Then
                        If _previewRecord("Contract Number").ToString <> "" Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Contract Number"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Contract Number").Ordinal + 1
                            validationError.Description = String.Format("Record: {0}, Field: 'Contract Number' should be blank if the currency in 'Settlement A/C No.' is the same as the transaction currency.", RowNumber + 1)
                            validationError.RecordNo = RowNumber + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If

                End If

                Dim regAllow As New Regex("[^0-9A-Za-z/?(),.'+:\-\s]", RegexOptions.Compiled)

                For Each col As String In TblBankFields.Keys

                    Select Case col.ToUpper
                        Case "TEMP1", "TEMP2", "TEMP3", "LOCAL COUNTRY", "LOCAL CURRENCY", "CONSOLIDATE FIELD"
                            Continue For
                    End Select


                    If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(col).ToString) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = col
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid character.", RowNumber + 1, col)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If

                Next

                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validation : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' #10. Fill Sector Selection 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GenerateSectorSelection()

        Dim ListTemp1() As String, ListTemp2() As String, ListTemp3() As String
        Dim ListCountry() As String, ListLocalCurrency() As String
        Dim matchCurrency As Boolean, matchCountry As Boolean, tem1 As Boolean, tem2 As Boolean
        Dim expandedCountryName As String = ""

        Try
            If ((TblBankFields.ContainsKey("Temp1") Or TblBankFields.ContainsKey("Temp2")) _
                    And TblBankFields.ContainsKey("Sector Selection")) Then

                ListTemp1 = "".Split("|")
                ListTemp2 = "".Split("|")
                ListTemp3 = "".Split("|")
                ListCountry = "".Split("|")
                ListLocalCurrency = "".Split("|")

                If TblBankFields.ContainsKey("Temp1") Then 'Get "Temp1" Data Sample
                    If Not TblBankFields("Temp1").BankSampleValue Is Nothing Then

                        ListTemp1 = TblBankFields("Temp1").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp1)
                    End If
                End If

                If TblBankFields.ContainsKey("Temp2") Then 'Get "Temp2" Data Sample
                    If Not TblBankFields("Temp2").BankSampleValue Is Nothing Then
                        ListTemp2 = TblBankFields("Temp2").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp2)
                    End If
                End If

                If TblBankFields.ContainsKey("Temp3") Then 'Get "Temp3" Data Sample
                    If Not TblBankFields("Temp3").BankSampleValue Is Nothing Then
                        ListTemp3 = TblBankFields("Temp3").BankSampleValue.Split("|")
                        System.Array.Sort(ListTemp3)
                    End If
                End If
                If TblBankFields.ContainsKey("Local Country") Then  'Get "Country" Data Sample
                    If Not TblBankFields("Local Country").BankSampleValue Is Nothing Then
                        ListCountry = TblBankFields("Local Country").BankSampleValue.Split("|")
                        expandedCountryName = ListCountry(0).ToUpper()
                        System.Array.Sort(ListCountry)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Currency") Then 'Get "Currency" Data Sample
                    If Not TblBankFields("Local Currency").BankSampleValue Is Nothing Then
                        ListLocalCurrency = TblBankFields("Local Currency").BankSampleValue.Split("|")
                        System.Array.Sort(ListLocalCurrency)
                    End If
                End If

                Select Case expandedCountryName

                    Case "INDONESIA"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                            tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                            If tem1 Or tem2 Then
                                _previewRecord("Sector Selection") = "Book Transfer"
                            Else
                                _previewRecord("Sector Selection") = IIf((BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0), "Domestic", "International")
                            End If

                        Next


                    Case "SINGAPORE", "MALAYSIA", "THAILAND", "AUSTRALIA"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                            tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                            If tem1 And tem2 Then
                                _previewRecord("Sector Selection") = "Book Transfer"
                            Else
                                matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0)
                                matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                _previewRecord("Sector Selection") = IIf(matchCurrency And matchCountry, "Domestic", "International")
                            End If

                        Next


                    Case "VIETNAM"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                            tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                            If tem1 And tem2 Then
                                _previewRecord("Sector Selection") = "Book Transfer"
                            Else
                                matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                matchCurrency = (BinarySearchCI(ListTemp3, _previewRecord("Temp3").ToString()) >= 0)
                                _previewRecord("Sector Selection") = IIf(matchCountry Or matchCurrency, "Domestic", "International")
                            End If

                        Next


                    Case "PHILIPPINES"

                        For Each _previewRecord As DataRow In _PreviewTable.Rows

                            tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)
                            tem2 = (BinarySearchCI(ListTemp2, _previewRecord("Temp2").ToString()) >= 0)

                            If tem1 And tem2 Then
                                _previewRecord("Sector Selection") = "Book Transfer"
                            Else
                                matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0)
                                matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                                _previewRecord("Sector Selection") = IIf(matchCountry And matchCurrency, "Domestic", "International")
                            End If

                        Next

                        'SRS/BTMU/2010-0018
                    Case "INDIA"
                        Dim branchCode As String = String.Empty
                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Currency").ToString()) >= 0)
                            matchCountry = (BinarySearchCI(ListCountry, _previewRecord("Local Country").ToString()) >= 0)
                            tem1 = (BinarySearchCI(ListTemp1, _previewRecord("Temp1").ToString()) >= 0)

                            If Not matchCurrency Then
                                _previewRecord("Sector Selection") = "International"
                                Continue For
                            End If

                            If Not matchCountry Then
                                _previewRecord("Sector Selection") = "International"
                                Continue For
                            End If

                            If Not tem1 Then
                                _previewRecord("Sector Selection") = "Domestic"
                                Continue For
                            End If


                            ' Get the first 3 characters from the Settlement Account No
                            If Not IsNothingOrEmptyString(_previewRecord("Settlement Account No")) Then
                                If _previewRecord("Settlement Account No").ToString.Trim.StartsWith("611") Then
                                    If String.Equals(_previewRecord("Temp2").ToString(), "New Delhi", StringComparison.InvariantCultureIgnoreCase) Then
                                        _previewRecord("Sector Selection") = "Book Transfer"
                                    Else
                                        _previewRecord("Sector Selection") = "Domestic"
                                    End If
                                ElseIf _previewRecord("Settlement Account No").ToString.Trim.StartsWith("612") Then
                                    If String.Equals(_previewRecord("Temp2").ToString(), "Mumbai", StringComparison.InvariantCultureIgnoreCase) Then
                                        _previewRecord("Sector Selection") = "Book Transfer"
                                    Else
                                        _previewRecord("Sector Selection") = "Domestic"
                                    End If
                                ElseIf _previewRecord("Settlement Account No").ToString.Trim.StartsWith("613") Then
                                    If String.Equals(_previewRecord("Temp2").ToString(), "Chennai", StringComparison.InvariantCultureIgnoreCase) Then
                                        _previewRecord("Sector Selection") = "Book Transfer"
                                    Else
                                        _previewRecord("Sector Selection") = "Domestic"
                                    End If
                                End If
                            End If
                        Next
                End Select

            End If ' end of Sector Selection Exists?

            If _MasterTemplateList.IsFixed Then FormatSequenceNo()

        Catch ex As Exception
            Throw New Exception("Error on generating Sector Selection : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub FormatOutput()

        Dim RowNumber As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If TblBankFields.ContainsKey("Currency") Then
                    If _previewRecord("Remittance Amount").ToString.Trim <> String.Empty And _
                       IsNumeric(_previewRecord("Remittance Amount").ToString.Trim) Then
                        Select Case _previewRecord("Currency").ToString.ToUpper
                            Case "TND", "KWD"
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 3).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND"
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 0).ToString("###########0")
                            Case Else
                                _previewRecord("Remittance Amount") = Math.Round(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString), 2).ToString("###########0.00")
                        End Select
                    End If
                End If


                If _previewRecord("Exchange Method").ToString.ToUpper = "SPOT" Then _previewRecord("Exchange Method") = "Spot"
                If _previewRecord("Exchange Method").ToString.ToUpper = "CONT" Then _previewRecord("Exchange Method") = "Cont"

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Conversion : " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Generates File in GCMS Format
    ''' </summary>
    ''' <param name="_userName">User ID that requested for File Generation</param>
    ''' <param name="_sourceFile">Name of Source File</param>
    ''' <param name="_outputFile">Name of Output File</param>
    ''' <param name="_objMaster">Master Template</param>
    ''' <returns>Boolean value determining whether the Generation of File is successful</returns>
    ''' <remarks></remarks>
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim enclosureCharacter As String

        Try

            ' GCMS format is Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New Exception("Invalid master Template")
            End If


            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                enclosureCharacter = ""
            Else
                enclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                line = ""

                For Each _column As String In TblBankFields.Keys

                    If TblBankFields(_column).Detail.ToUpper <> "NO" Then

                        If Not (_column.ToUpper = "Temp1".ToUpper Or _
                                _column.ToUpper = "Temp2".ToUpper Or _
                                _column.ToUpper = "Temp3".ToUpper Or _
                                _column.ToUpper = "Local Country".ToUpper Or _
                                _column.ToUpper = "Local Currency".ToUpper) Then

                            If _column.ToUpper = "Remittance Amount".ToUpper Then

                                Dim amount As Decimal
                                amount = Math.Abs(Convert.ToDecimal(_previewRecord("Remittance Amount").ToString))
                                Select Case _previewRecord("Currency").ToString.ToUpper
                                    Case "TND", "KWD"
                                        If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                            _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 3).ToString("###########0.000")
                                        Else
                                            _previewRecord("Remittance Amount") = Math.Round(amount, 3).ToString("###########0.000")
                                        End If
                                    Case "TWD", "JPY", "KRW", "VND"
                                        If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                            _previewRecord("Remittance Amount") = "-" & Math.Round(amount).ToString("###########0")
                                        Else
                                            _previewRecord("Remittance Amount") = Math.Round(amount).ToString("###########0")
                                        End If
                                    Case Else
                                        If _previewRecord("Remittance Amount").ToString.StartsWith("-") Then
                                            _previewRecord("Remittance Amount") = "-" & Math.Round(amount, 2).ToString("###########0.00")
                                        Else
                                            _previewRecord("Remittance Amount") = Math.Round(amount, 2).ToString("###########0.00")
                                        End If
                                End Select

                                ' Added On 25 Aug 2009
                                'New Requirement from Client
                                If _previewRecord("Remittance Amount").ToString.Trim <> String.Empty And _
                                                                                 IsNumeric(_previewRecord("Remittance Amount").ToString.Trim) Then

                                    _previewRecord("Remittance Amount") = Left(_previewRecord("Remittance Amount").ToString, 15)
                                    If _previewRecord("Remittance Amount").ToString().EndsWith(".") Then
                                        _previewRecord("Remittance Amount") = Left(_previewRecord("Remittance Amount").ToString(), 14)
                                    End If
                                End If

                            End If

                            'added for consolidate field checking
                            If (Not _column.Equals("Consolidate Field")) Then
                                If enclosureCharacter.Trim = String.Empty Then
                                    If line = "" Then
                                        line &= _previewRecord(_column).ToString
                                    Else
                                        line = line & "," & _previewRecord(_column).ToString
                                    End If
                                Else
                                    If line = "" Then
                                        line = line & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                    Else
                                        line = line & "," & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                    End If
                                End If
                            End If

                        End If

                    End If
                Next

                If rowNo <> _PreviewTable.Rows.Count - 1 Then
                    text &= line & vbNewLine
                Else
                    text &= line
                End If

                rowNo += 1

            Next

            Return SaveTextToFile(text, _outputFile)

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    ''' <summary>
    ''' Generates Consolidated Data
    ''' </summary>
    ''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    ''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    ''' <param name="AppendText">Text to append with Reference Fields</param>
    ''' <remarks></remarks>
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))

        If GroupByFields.Count = 0 Then Exit Sub

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If

            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''


        Dim filterString As String = String.Empty
        Dim filterList As New List(Of String)
        Dim remittanceAmt As Decimal = 0
        Dim referenceField(1) As String
        Dim rowIndex As Integer = 0

        Try

            _consolidatedDataView = _PreviewTable.DefaultView ' DataView to apply rowfilter
            _consolidatedData = New DataTable ' DataTable to hold only consolidated records
            ' Create the columns for the Consolidated DataTable
            For Each _column As DataColumn In _PreviewTable.Columns
                _consolidatedData.Columns.Add(_column.ColumnName, GetType(String))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '1. Filter out unique rows for given Filter Group
            For Each _row As DataRow In _PreviewTable.Rows

                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_row(_field).ToString()))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For

                If filterList.IndexOf(filterString) = -1 Then
                    filterList.Add(filterString)
                    filterList.Sort()
                End If

            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next


            '2. Set of fields to be aggregated/consolidated while applying the Grouping
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            For Each _filter As String In filterList

                _consolidatedDataView.RowFilter = String.Empty
                _consolidatedDataView.RowFilter = _filter

                If _consolidatedDataView.Count > 0 Then

                    ' Create the reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1) <> "" Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next
                    remittanceAmt = 0

                    For Each _viewRow As DataRowView In _consolidatedDataView
                        remittanceAmt += Convert.ToDecimal(_viewRow(_amountFieldName).ToString)
                        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                            If _referenceFields(intI, 1).Trim.Length <> 0 Then
                                referenceField(intI) = referenceField(intI) & _viewRow(_referenceFields(intI, 1).ToString) & ","
                            End If
                        Next
                    Next
                End If

                Dim consolidatedRow As DataRow
                consolidatedRow = _consolidatedData.NewRow
                _consolidatedDataView.Item(0)(_amountFieldName) = remittanceAmt.ToString

                For Each _column As String In TblBankFields.Keys
                    consolidatedRow(_column) = _consolidatedDataView(0)(_column)
                    consolidatedRow(_amountFieldName) = remittanceAmt.ToString
                    '#2. Format Remittance Amount 
                    If consolidatedRow(_amountFieldName).ToString <> String.Empty Then
                        Select Case consolidatedRow("Currency").ToString().ToUpper()
                            Case "TND", "KWD"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.000")
                            Case "TWD", "JPY", "KRW", "VND"
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0")
                            Case Else
                                consolidatedRow("Remittance Amount") = Convert.ToDecimal(consolidatedRow("Remittance Amount").ToString()).ToString("###########0.00")
                        End Select

                    End If
                Next

                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
                        consolidatedRow(_referenceFields(intI, 1)) = referenceField(intI).Substring(0, referenceField(intI).Length - 1)
                    End If
                Next

                _consolidatedData.Rows.Add(consolidatedRow)

            Next

            _totalRecordCount = _PreviewTable.Rows.Count
            If _consolidatedData.Rows.Count > 0 Then
                _consolidatedRecordCount = _consolidatedData.Rows.Count
                _PreviewTable.Rows.Clear()
                _PreviewTable = _consolidatedData.Copy
            Else
                _consolidatedRecordCount = 0
            End If
            ValidationErrors.Clear()
            GenerateHeader()
            GenerateTrailer()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()

        Catch ex As Exception
            Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
        End Try

    End Sub


    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If
                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If

            End If

            RowNumber += 1

        Next

    End Sub


#End Region

    'Added for Settlement A/C enhancement for OVS
    Private Function IsValidSettlementAC(ByVal cur As String) As Boolean
        Dim isFound As Boolean = False
        If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
            If TblBankFields("Currency").DefaultValue.Length > 0 Then
                Dim defaultCurrency() As String
                defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                For Each val As String In defaultCurrency
                    If cur.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                        isFound = True
                        Exit For
                    End If
                Next
            End If
        End If
        Return isFound

    End Function

    Public Sub New()

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

 

 