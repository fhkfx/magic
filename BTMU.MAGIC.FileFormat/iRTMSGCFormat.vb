Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Specific Features of iRTMS GC Format
''' </summary>
''' <remarks></remarks>
Public Class iRTMSGCFormat
    Inherits iRTMSFormat
    'In this format,  group by fields and reference fields are same

    Private Shared _validGroupByFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                              "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                              "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                              "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                              "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                              "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                              "Beneficiary Bank Country", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                              "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                              "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                              "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                              "Creditor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                              "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                              "Account Currency", "Consolidate Field"}

    Private Shared _editableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                                  "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                                  "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                  "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                                  "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                                  "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                                  "Beneficiary Bank Country", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                  "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                  "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                  "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                                  "Creditor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                                  "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                                  "Account Currency"}

    Private Shared _autoTrimmableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", _
                                                   "Currency", "Beneficiary Name 1", "Beneficiary Name 2", "Beneficiary Attention", _
                                                   "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                   "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Bank Name", _
                                                   "Beneficiary Bank Branch Name", "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", _
                                                   "Beneficiary Bank Address 3", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                   "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                   "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                   "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                                   "Creditor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                                   "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice"}
#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iRTMSGC"
        End Get
    End Property

#End Region

    Public Shared Function OutputFormatString() As String
        Return "iRTMSGC"
    End Function

End Class