Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

Public Class CarrotInvoiceDataFormat
    Inherits BaseFileFormat

    'Amount field is summed up while grouping records
    Private Shared _amountFieldName As String = "Amount of Invoice"

    Private Shared _validgroupbyFields() As String = {"Invoice Date", "Invoice No", "Customer Code", "Customer Name", "Currency", _
                                                        "Account No", "Due Date", "Amount of Invoice", "Note", "Consolidate Field"}

    Private Shared _validreferenceFields() As String = {"Invoice Date", "Invoice No", "Customer Code", "Customer Name", "Currency", _
                                                        "Account No", "Due Date", "Amount of Invoice", "Note"}

    Private Shared _editableFields() As String = {"Invoice Date", "Invoice No", "Customer Code", "Customer Name", "Currency", _
                                                        "Account No", "Due Date", "Amount of Invoice", "Note"}

    Private Shared _autoTrimmableFields() As String = {"Invoice No", "Currency", "Customer Code", "Customer Name", "Account No", "Note"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "CARROTInvoice"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validreferenceFields
        End Get
    End Property

#End Region


    Public Shared Function OutputFormatString() As String
        Return "CARROTInvoice"
    End Function

#Region "Overriden Methods"

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim RowNumber As Integer = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If TblBankFields.ContainsKey("Currency") And _previewRecord("Currency").ToString.Trim <> "" Then
                    If _previewRecord("Amount of Invoice").ToString.Trim <> String.Empty And _
                       IsNumeric(_previewRecord("Amount of Invoice").ToString) Then
                        Select Case _previewRecord("Currency").ToString.ToUpper
                            Case "BHD", "KWD", "OMR", "TND"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 3).ToString("##############0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 0).ToString("##############0")
                            Case Else
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 2).ToString("##############0.00")
                        End Select
                    End If
                End If

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Conversion : " & ex.Message.ToString)
        End Try
    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                'Carrot - Changed by fujitsu - 20/03/2013
                _previewRecord("Invoice Date") = ValueDate.ToString(TblBankFields("Invoice Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()
        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Format Amount 
                If _previewRecord("Amount of Invoice").ToString <> String.Empty Then

                    'Output accepts Dollar value
                    'Convert to Dollars if the source value is Cents
                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Amount of Invoice") = Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString()) / 100
                    End If

                    If _previewRecord("Currency").ToString.Trim <> "" Then
                        Select Case _previewRecord("Currency").ToString().ToUpper()
                            Case "BHD", "KWD", "OMR", "TND"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 3).ToString("##############0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 0).ToString("##############0")
                            Case Else
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 2).ToString("##############0.00")
                        End Select
                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount of Invoice"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount of Invoice"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                '#1. Format Amount 
                If _previewRecord("Amount of Invoice").ToString <> String.Empty Then

                    'Output accepts Dollar value
                    'Convert to Dollars if the source value is Cents
                    'No need (once is enough in GenerateOutputFormat())
                    'If Not _DataModel.IsAmountInDollars Then
                    '    _previewRecord("Amount of Invoice") = Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString()) / 100
                    'End If
                    If _previewRecord("Currency").ToString.Trim <> "" Then
                        Select Case _previewRecord("Currency").ToString().ToUpper()
                            Case "BHD", "KWD", "OMR", "TND"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 3).ToString("##############0.000")
                            Case "TWD", "JPY", "KRW", "VND", "CLP"
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 0).ToString("##############0")
                            Case Else
                                _previewRecord("Amount of Invoice") = Math.Round(Convert.ToDecimal(_previewRecord("Amount of Invoice").ToString), 2).ToString("##############0.00")
                        End Select
                    End If
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Amount of Invoice"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex + 1, "Amount of Invoice"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub ValidateDataLength()

        Try

            Dim RowNumber As Int32 = 0
            Dim validationError As FileFormatError

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each _column As String In TblBankFields.Keys

                    If _column.Equals("Amount of Invoice") Then
                        Continue For
                        'length validation by currency code is implemented in Vaidate()

                    ElseIf _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value Then

                        validationError = New FileFormatError()
                        validationError.ColumnName = _column
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1
                        validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                        validationError.RecordNo = RowNumber + 1
                        _ValidationErrors.Add(validationError)
                        If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True

                    End If

                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Data Length: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled) ' no need to check (confirmed)
        Dim _ValueDate As Date

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                '#1. Invoice Date - must be valid date
                If _previewRecord("Invoice Date").ToString() <> "" Then
                    'Carrot - Changed by fujitsu - 20/03/2013
                    If Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), "", TblBankFields("Invoice Date").DateFormat.Replace("D", "d").Replace("Y", "y"), _ValueDate, True) Then
                        AddValidationError("Invoice Date", _PreviewTable.Columns("Invoice Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Invoice Date"), RowNumber + 1, True)
                    End If
                End If

                '#2. Invoice No
                'If regAllowAlphaNum.IsMatch(_previewRecord("Invoice No").ToString()) Then
                '    AddValidationError("Invoice No", _PreviewTable.Columns("Invoice No").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Invoice No") _
                '                       , RowNumber + 1, True)
                'End If

                '#3. Customer Code
                'If regAllowAlphaNum.IsMatch(_previewRecord("Customer Code").ToString()) Then
                '    AddValidationError("Customer Code", _PreviewTable.Columns("Customer Code").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Customer Code") _
                '                       , RowNumber + 1, True)
                'End If

                '#4. Customer Name

                '#5. Currency
                If _previewRecord("Currency").ToString <> String.Empty Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid 'Currency'.", RowNumber + 1)
                                validationError.RecordNo = RowNumber + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                '#6. Account No
                'If regAllowAlphaNum.IsMatch(_previewRecord("Account No").ToString()) Then
                '    AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Account No") _
                '                       , RowNumber + 1, True)
                'End If

                '#7. Due Date - must be valid date
                If _previewRecord("Due Date").ToString() <> "" Then
                    'Carrot - Changed by fujitsu - 20/03/2013
                    If Not HelperModule.IsDateDataType(_previewRecord("Due Date").ToString(), "", TblBankFields("Due Date").DateFormat.Replace("D", "d").Replace("Y", "y"), _ValueDate, True) Then
                        AddValidationError("Due Date", _PreviewTable.Columns("Due Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Due Date"), RowNumber + 1, True)
                    End If
                End If

                '#8. Amount of Invoice
                If _previewRecord("Amount of Invoice") <> "" Then 'numbers only
                    'Must be numeric
                    If Not Decimal.TryParse(_previewRecord("Amount of Invoice").ToString(), dAmount) Then
                        AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Amount of Invoice"), RowNumber + 1, True)

                    Else
                        '********************************************************************************************
                        'Should not be less than or equal to zero  (only before consolidate records; fixed during UAT)
                        'Confirmed by Jackson/ Casey @ 20/12/2012
                        'If Not IsConsolidated AndAlso dAmount <= 0 Then
                        '    AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                        '                        , String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Amount of Invoice") _
                        '                        , RowNumber + 1, True)
                        'End If

                        'Should not greater than 999,999,999,999,999.999
                        If dAmount.ToString().IndexOf(".") > 0 Then
                            'Should not have more than 15 integers
                            If dAmount.ToString().Substring(0, dAmount.ToString().IndexOf(".")).Length > 15 Then
                                AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                                                    , String.Format("{0}: {1} Field: '{2}' should not include more than 15 integers.", "Record", RowNumber + 1, "Amount of Invoice") _
                                                    , RowNumber + 1, True)
                            End If
                            'Should not have more than 3 decimal places
                            If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 3 Then
                                AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                                                    , String.Format("{0}: {1} Field: '{2}' should not include more than 3 decimal places.", "Record", RowNumber + 1, "Amount of Invoice") _
                                                    , RowNumber + 1, True)
                            End If

                        End If

                        If _previewRecord("Currency").ToString.ToUpper = "VND" Or _previewRecord("Currency").ToString.ToUpper = "JPY" Or _
                            _previewRecord("Currency").ToString.ToUpper = "KRW" Or _previewRecord("Currency").ToString.ToUpper = "CLP" Or _
                             _previewRecord("Currency").ToString.ToUpper = "TWD" Then

                            If _previewRecord("Amount of Invoice").ToString.Length > 15 Then
                                AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                                                    , String.Format("{0}: {1} Field: '{2}' should not include more than 15 integers.", "Record", RowNumber + 1, "Amount of Invoice") _
                                                    , RowNumber + 1, True)
                            End If

                        End If


                        'If IsConsolidated Then
                        '    If IsNegativePaymentOption Then
                        '        If Val(_previewRecord("Amount of Invoice").ToString.Trim) >= 0 Then
                        '            AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                        '                    , String.Format("Record: {0}, Field: '{1}' should not be zero or positive. Please check the payment option selected or there may be invalid data in your source file.", RowNumber + 1, "Amount of Invoice") _
                        '                    , RowNumber + 1, True)
                        '        End If
                        '    Else
                        '        If Val(_previewRecord("Amount of Invoice").ToString.Trim) <= 0 Then
                        '            AddValidationError("Amount of Invoice", _PreviewTable.Columns("Amount of Invoice").Ordinal + 1 _
                        '                    , String.Format("Record: {0}, Field: '{1}' should not be zero or negative. Please check the payment option selected or there may be invalid data in your source file.", RowNumber + 1, "Amount of Invoice") _
                        '                    , RowNumber + 1, True)
                        '        End If
                        '    End If
                        'End If

                        End If
                End If

                '#9.Note

                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Amount of Invoice") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Amount of Invoice"))
            If Not Decimal.TryParse(_row("Amount of Invoice").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Amount of Invoice"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Amount of Invoice")

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        Dim objMasterTemplate As MasterTemplateNonFixed  ' Non Fixed Type Master Template

        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In TblBankFields.Keys
                    If _column.Equals("Consolidate Field", StringComparison.InvariantCultureIgnoreCase) Then Continue For

                    If _column.Equals("Amount of Invoice", StringComparison.InvariantCultureIgnoreCase) Then
                        'taking round() for all amount to be output as positive to cater -ve consolidated values output 
                        If _previewRecord(_column).ToString.StartsWith("-") Then
                            _previewRecord(_column) = "-" & Math.Abs(Convert.ToDecimal(_previewRecord(_column))).ToString() '(no formatting required)
                        Else
                            _previewRecord(_column) = Math.Abs(Convert.ToDecimal(_previewRecord(_column))).ToString() '(no formatting required)
                        End If
                    End If

                        sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Return SaveTextToFile(sText, _outputFile)

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Field: 'Amount of Invoice' has invalid value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

#End Region

End Class
