Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

Public Class CarrotCustomerDataFormat
    Inherits BaseFileFormat

    'Amount field is summed up while grouping records
    'In this format, there is no amount field defined
    Private Shared _amountFieldName As String = ""

    'No groupby and reference fields are required due to no amount field is defined
    Private Shared _validgroupbyFields() As String = {}
    Private Shared _validreferenceFields() As String = {}

    Private Shared _editableFields() As String = {"Customer Code", "Customer Name", "Account No", "Currency", _
                                                "Reconciliation Pattern ID", "Reconciliation Pattern Name", "Alias", "Affiliate", "Note"}

    Private Shared _autoTrimmableFields() As String = {"Customer Name", "Account No", "Currency", "Reconciliation Pattern ID", _
                                                    "Reconciliation Pattern Name", "Alias", "Affiliate", "Note"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "CARROTCustomer"
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validreferenceFields
        End Get
    End Property

#End Region


    Public Shared Function OutputFormatString() As String
        Return "CARROTCustomer"
    End Function

#Region "Overriden Methods"

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        'nothing to be done for this format

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()
        'not applicable for this format

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()
        'nothing to be done for this format

    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        'nothing to be done for this format

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled) 'confirmed not necessary
        'Dim _ValueDate As Date

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                '#1. Customer Code
                'If regAllowAlphaNum.IsMatch(_previewRecord("Customer Code").ToString()) Then
                '    AddValidationError("Customer Code", _PreviewTable.Columns("Customer Code").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Customer Code") _
                '                       , RowNumber + 1, True)
                'End If

                '#2. Customer Name

                '#3. Account No
                'If regAllowAlphaNum.IsMatch(_previewRecord("Account No").ToString()) Then
                '    AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Account No") _
                '                       , RowNumber + 1, True)
                'End If
                'Conditional mandatory checking
                If _previewRecord("Account No").ToString().ToString = String.Empty AndAlso _
                    (_previewRecord("Reconciliation Pattern ID").ToString <> String.Empty OrElse _
                    _previewRecord("Alias").ToString <> String.Empty) Then
                    AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                       , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Account No", "'Reconciliation Pattern ID' or 'Alias' is provided") _
                                       , RowNumber + 1, True)
                End If

                '#4. Currency
                If _previewRecord("Currency").ToString <> String.Empty Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next
                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid 'Currency'.", RowNumber + 1)
                                validationError.RecordNo = RowNumber + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                '#5. Reconciliation Pattern ID
                'If regAllowAlphaNum.IsMatch(_previewRecord("Reconciliation Pattern ID").ToString()) Then
                '    AddValidationError("Reconciliation Pattern ID", _PreviewTable.Columns("Reconciliation Pattern ID").Ordinal + 1 _
                '                       , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Reconciliation Pattern ID") _
                '                       , RowNumber + 1, True)
                'End If

                '#6. Reconciliation Pattern Name


                '#7. Alias

                '#8. Affiliate

                '#9.Note


                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        'N.A
        If _groupByFields.Count = 0 Then Exit Sub


    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        Dim objMasterTemplate As MasterTemplateNonFixed  ' Non Fixed Type Master Template

        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In TblBankFields.Keys
                    sLine = sLine & IIf(sLine = "", "", ",") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)
                rowNo += 1

            Next

            Return SaveTextToFile(sText, _outputFile)

            'Catch fex As FormatException
            '    Throw New MagicException(String.Format("Record: {0} Field: 'Amount of Invoice' has invalid value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        'Not applicable

    End Sub

#End Region

End Class
