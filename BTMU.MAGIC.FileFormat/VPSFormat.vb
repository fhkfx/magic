'------------------------------------------------CHANGE REQUESTS-----------------------------------
'SRS No                 Date Modified       Description
'--------------------------------------------------------------------------------------------------
'SRS/BTMU/2010-0027     09/Apr/2010         1.  No Consolidation by default
'                                           2.  Allow fields "DeliveryMode","VenCode","DocNum",
'                                               "VenName1","VenName2","VenAdd1","VenAdd2",
'                                               "VenAdd3","VenAdd4","InvNum" to be selected as
'                                               Group by fields in the Consolidation Screen
'                                           3.  Automatically list fields "VenCode","VenName1",
'                                               "VenName2","VenAdd1","VenAdd2","VenAdd3",
'                                               "VenAdd4" when user selects Yes to Consolidation
'SRS/BTMU/2010-0033     26/Apr/2010         1.  Bank Field Value should not be trimmed.
'--------------------------------------------------------------------------------------------------                                         
Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate
Imports System.Windows.Forms
Imports System.Text.RegularExpressions


''' <summary>
''' Encapsulates the VPS Format
''' </summary>
''' <remarks></remarks>
Public Class VPSFormat
    Inherits BaseFileFormat
 
    Private RECP_H As String '= "0000000"
    Private PAY_TYPE As String '= "VEN"
    Private PAY_MODE As String '= "CO"
    Private RECP_T As String '= "9999999"

    Private Shared _amountFieldName As String = "InvAmt"

    Private Shared _validgroupbyFields() As String = {"DeliveryMode", "VenCode", "DocNum", "VenName1", "VenName2", "VenAdd1", "VenAdd2", "VenAdd3", "VenAdd4", "InvNum"}

    Private Shared _validreferenceFields() As String = {"DocNum", "VenAdd1", "VenAdd2", "VenAdd3", "VenAdd4", "InvNum"}

    Private Shared _editableFields() As String = {"PayMode", "DeliveryMode", "DocNum", "VenAdd1", "VenAdd2", _
                    "VenAdd3", "VenAdd4", "InvNum"}

    Private Shared _autoTrimmableFields() As String = {"VenCode", "DocNum", "VenName1", "VenName2" _
        , "VenAdd1", "VenAdd2", "VenAdd3", "VenAdd4", "InvNum"}


#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "VPS"
        End Get
    End Property

#End Region

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validreferenceFields
        End Get
    End Property

    Public Shared Function OutputFormatString() As String
        Return "VPS"
    End Function


#Region "Overriden Methods"

    ''' <summary>
    ''' Validates Data Length of Certain Fields
    ''' Error Code : E09020050
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ValidateDataLength()

        Try

            Dim RowNumber As Int32 = 0

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                For Each _column As String In TblBankFields.Keys

                    'Need to skip Validating Data Length for Header / Trailer Fields at the Detail Rows
                    If RowNumber > 0 AndAlso "No".Equals(TblBankFields(_column).Detail, StringComparison.InvariantCultureIgnoreCase) Then Continue For

                    'Need to skip Validating Data Length for Header / Trailer Fields at the Header/Trailer Rows
                    If _PreviewTable.Columns(_column).Ordinal < 8 OrElse _PreviewTable.Columns(_column).Ordinal > 28 Then Continue For

                    If "Currency".Equals(TblBankFields(_column).DataType, StringComparison.InvariantCultureIgnoreCase) _
                        And "No".Equals(TblBankFields(_column).IncludeDecimal, StringComparison.InvariantCultureIgnoreCase) Then

                        If _previewRecord(_column).ToString().Length > _
                                (TblBankFields(_column).DataLength.Value + _
                                IIf(_DataModel.DecimalSeparator = "No Separator", 0, 1) + _
                                GetDecimalLengthForCurrency(_previewRecord("Currency").ToString())) Then

                            Dim validationError As New FileFormatError()
                            If TblBankFields(_column).Header Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            ElseIf TblBankFields(_column).Trailer Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            Else ' If detail
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = RowNumber + 1
                            End If
                            validationError.ColumnName = _column
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1
                            _ValidationErrors.Add(validationError)
                        End If
                    Else
                        If _previewRecord(_column).ToString().Length > TblBankFields(_column).DataLength.Value Then

                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = _column
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns(_column).Ordinal + 1
                            If TblBankFields(_column).Header Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            ElseIf TblBankFields(_column).Trailer Then
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = 0
                            Else ' If detail
                                validationError.Description = String.Format(MsgReader.GetString("E09020050"), "Record", RowNumber + 1, _column, TblBankFields(_column).DataLength.Value)
                                validationError.RecordNo = RowNumber + 1
                            End If

                            _ValidationErrors.Add(validationError)

                            If Not IsAutoTrimmable(_column) Then _ErrorAtMandatoryField = True

                        End If
                    End If

                Next ' Iteration for each column of table 

                RowNumber = RowNumber + 1

            Next ' Iteration for each Row of Table 

        Catch ex As Exception
            Throw New MagicException("Error on validating Data Length: " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' EPS is fixed width file format.
    ''' Adjust the Width of Numeric and Currency Values so as 
    ''' to fit the Decimal Length in FixedWidthMaster Template
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Function FixNumericData() As Integer
        Try
            Dim sNumber As String, sDecimal As String, iDecimalLength As Integer = 0

            For Each _BankField As MapBankField In TblBankFields.Values
                Select Case _BankField.DataType.ToUpper
                    Case "NUMBER" ' a. Numeric Value Size adjustment
                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            If _previewRecord(_BankField.BankField).ToString().Length < _BankField.DataLength.Value Then
                                If _BankField.BankField.ToUpper() <> "InvAmt".ToUpper() Then
                                    If _previewRecord(_BankField.BankField).ToString.Trim = String.Empty Then
                                        _previewRecord(_BankField.BankField) = 0
                                    End If
                                Else
                                    If _previewRecord(_BankField.BankField).ToString.Trim = String.Empty Then
                                        _previewRecord(_BankField.BankField) = 0
                                    End If
                                    '_previewRecord(_BankField.BankField) = 0
                                    '_previewRecord(_BankField.BankField) = _previewRecord(_BankField.BankField).ToString().PadLeft(_BankField.DataLength.Value, "0")
                                End If
                            End If
                            'Following line is for All other Output Formats of Fixed width Master Template
                            '_previewRecord(_BankField.BankField) = _previewRecord(_BankField.BankField).ToString().PadLeft(_BankField.DataLength.Value, "0")
                        Next
                    Case "CURRENCY"  'b. Currency adjustment
                        For Each _previewRecord As DataRow In _PreviewTable.Rows
                            Dim numbers() As String = _previewRecord(_BankField.BankField).ToString().Split(".")
                            If numbers.Length = 2 Then
                                sNumber = numbers(0)
                                sDecimal = numbers(1)
                            Else
                                sNumber = _previewRecord(_BankField.BankField).ToString()
                                sDecimal = ""
                            End If
                            'Get Decimal Length from Default Value Field  
                            iDecimalLength = GetDecimalLengthForCurrency(_previewRecord("Currency").ToString())
                            If iDecimalLength = 0 Then iDecimalLength = 2
                            If sDecimal.Length < iDecimalLength Then
                                sDecimal = sDecimal.PadRight(iDecimalLength, "0")
                            ElseIf sDecimal.Length > iDecimalLength Then
                                If MsgBox("Error, Decimal length is greater than decimal data length in master template." _
                                    & Chr(13) & "Do you want to cut or repair customer data ?" _
                                    & Chr(13) & Chr(13) & "Click 'Yes', if you want to cut customer data." _
                                    & Chr(13) & "Click 'No', if you want to repair customer data.", _
                                    vbQuestion & vbYesNo, "Generate Common Transaction Template") = vbYes Then
                                    sDecimal = sDecimal.Substring(0, iDecimalLength)
                                Else
                                    Return -1
                                End If
                            End If
                            If _BankField.IncludeDecimal = "Yes" Then
                                sNumber = sNumber & sDecimal
                                If _BankField.DataLength.HasValue Then
                                    sNumber = sNumber.PadLeft(_BankField.DataLength.Value, "0")
                                End If
                                _previewRecord(_BankField.BankField) = sNumber
                            ElseIf _BankField.IncludeDecimal = "No" Then
                                If _BankField.DataLength.HasValue Then
                                    sNumber = sNumber.PadLeft(_BankField.DataLength.Value, "0")
                                End If
                                _previewRecord(_BankField.BankField) = sNumber & _BankField.DecimalSeparator & sDecimal
                            End If
                        Next
                End Select
            Next
        Catch ex As Exception
            Throw New Exception("Error on validating numeric data : " & ex.Message.ToString)
        End Try
        Return 0
    End Function

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    'Protected Overrides Sub GenerateOutputFormat()
    '    Dim vendorList As New ArrayList

    '    Dim recp12 As Integer ' Total Number of Type 2 records for each vendor
    '    Dim recp13 As Integer ' Sequence Number allocated for each vendor
    '    Dim recp22 As Integer ' No:of Type 2 records for each vendor
    '    Dim recp23 As Integer ' Vendor Number
    '    Dim sourceDataView As DataView

    '    Dim filterString As String

    '    Dim totalInvAmt As Decimal
    '    Dim grandAmt As Decimal
    '    Dim rowIndex As Integer = 0

    '    Try

    '        ' Check if any of the rows have invalid numeric data
    '        Dim _rowIndex As Integer = 0
    '        For Each _row As DataRow In _PreviewTable.Rows
    '            _rowIndex += 1
    '            If _row("InvAmt") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "InvAmt"))
    '            If Not Decimal.TryParse(_row("InvAmt").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "InvAmt"))
    '            'If Convert.ToDecimal(_row("InvAmt").ToString) = 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "InvAmt", "0"))
    '        Next

    '        vendorList = GetVendorList()

    '        recp13 = 0
    '        sourceDataView = _PreviewTable.DefaultView
    '        sourceDataView.AllowEdit = True
    '        grandAmt = 0
    '        For Each vendor As Vendor In vendorList
    '            filterString = "VenCode = '" & vendor.venCode & "' AND VenName1 = '" & vendor.venName1.Replace("'", "''") & "' AND VenName2 = '" & vendor.venName2.Replace("'", "''") & "'"

    '            sourceDataView.RowFilter = filterString
    '            recp12 = sourceDataView.Count
    '            recp13 += 1
    '            recp22 = 0
    '            recp23 = recp13
    '            totalInvAmt = 0

    '            For Each _row As DataRowView In sourceDataView
    '                recp22 += 1

    '                _row.BeginEdit()
    '                _row("RECP1.1") = "1"
    '                _row("RECP1.2") = recp12.ToString
    '                _row("RECP1.3") = recp13.ToString
    '                _row("RECP2.1") = "2"
    '                _row("RECP2.2") = recp22.ToString
    '                _row("RECP2.3") = recp23.ToString

    '                Dim invAmt As Decimal

    '                If _DataModel.IsAmountInDollars Then

    '                    invAmt = (Convert.ToDecimal(_row("InvAmt").ToString) * 100).ToString()


    '                Else
    '                    invAmt = (Convert.ToDecimal(_row("InvAmt").ToString)).ToString()
    '                End If

    '                If invAmt >= 0 Then
    '                    _row("InvAmtSign") = "+"
    '                    'totalInvAmt += Convert.ToDecimal(_row("InvAmt").ToString)
    '                    'grandAmt += Convert.ToDecimal(_row("InvAmt").ToString)
    '                Else
    '                    _row("InvAmtSign") = "-"
    '                    'totalInvAmt -= Convert.ToDecimal(_row("InvAmt").ToString)
    '                    'grandAmt -= Convert.ToDecimal(_row("InvAmt").ToString)
    '                End If
    '                If _row("InvAmt") <> 0 Then
    '                    _row("InvAmt") = Math.Abs(invAmt).ToString("#############")
    '                Else
    '                    _row("InvAmt") = 0
    '                End If
    '                '_row("InvAmt") = _row("InvAmt").ToString.PadLeft(TblBankFields("InvAmt").DataLength.Value, "0")

    '                If _row("InvAmtSign").ToString = "-" Then
    '                    totalInvAmt -= Convert.ToDecimal(_row("InvAmt").ToString)
    '                    grandAmt -= Convert.ToDecimal(_row("InvAmt").ToString)
    '                ElseIf _row("InvAmtSign").ToString = "+" Then
    '                    totalInvAmt += Convert.ToDecimal(_row("InvAmt").ToString)
    '                    grandAmt += Convert.ToDecimal(_row("InvAmt").ToString)
    '                End If

    '                '' Format the PayDate in YYYY-MM-DD
    '                'Dim paydate As Date
    '                If IsDateDataType(_row("PayDate").ToString, TblBankFields("PayDate").DateFormat, False) Then
    '                    '_row("PayDate") = paydate.Year.ToString("0000") & "-" & paydate.Month.ToString("00") & "-" & paydate.Day.ToString("00")
    '                    _row("PayDate") = _row("PayDate").ToString.Substring(0, 4) & "-" & _row("PayDate").ToString.Substring(4, 2) & "-" & _row("PayDate").ToString.Substring(6, 2)
    '                End If

    '                _row.EndEdit()

    '            Next

    '            For Each _row As DataRowView In sourceDataView
    '                _row.BeginEdit()
    '                ' Update the total Inv Amount 
    '                If totalInvAmt >= 0 Then
    '                    _row("TotInvAmtSign") = "+"
    '                Else
    '                    _row("TotInvAmtSign") = "-"
    '                End If
    '                _row("TotInvAmt") = Math.Abs(totalInvAmt).ToString("#############")
    '                _row.EndEdit()
    '            Next
    '        Next

    '        ' Update the Grand Amount for the Header.Since the first row is taken as header, need to
    '        ' update only on the first row

    '        ' RECP (H)
    '        '_PreviewTable.Rows(0)(0) = RECP_H
    '        '_PreviewTable.Rows(0)(0) = _PreviewTable.Rows(0)(0).ToString.PadLeft(TblBankFields("RECP (H)").DataLength.Value, "0")
    '        '_PreviewTable.Rows(0)("ACNO") = _PreviewTable.Rows(0)("ACNO").ToString.PadLeft(TblBankFields("ACNO").DataLength.Value, "0")
    '        _PreviewTable.Rows(0)("GrandNo") = recp13.ToString
    '        If grandAmt >= 0 Then
    '            _PreviewTable.Rows(0)("GrandAmtSign") = "+"
    '        Else
    '            _PreviewTable.Rows(0)("GrandAmtSign") = "-"
    '        End If
    '        _PreviewTable.Rows(0)("GrandAmt") = Math.Abs(grandAmt).ToString("#############")
    '        _PreviewTable.DefaultView.RowFilter = String.Empty
    '        _PreviewTable.Columns.Add(New DataColumn("RECP23", GetType(Integer)))
    '        _PreviewTable.Columns.Add(New DataColumn("RECP22", GetType(Integer)))
    '        _PreviewTable.Columns("RECP23").Expression = "Convert([RECP2.3], 'System.Int32')"
    '        _PreviewTable.Columns("RECP22").Expression = "Convert([RECP2.2], 'System.Int32')"

    '        Dim dv As DataView
    '        dv = _PreviewTable.DefaultView
    '        dv.Sort = "RECP23 ASC, RECP22 ASC"
    '        Dim dt As DataTable
    '        dt = dv.ToTable()
    '        dt.Columns.Remove("RECP22")
    '        dt.Columns.Remove("RECP23")
    '        _PreviewTable = dt
    '        dt.Dispose()

    '        'Catch fex As FormatException
    '        '    throw new Exception(string.Format("Error on generating records: Record: {0} Field: 'Invoice Amount' - Not a valid numeric value", _rowIndex))
    '    Catch mex As MagicException
    '        Throw New MagicException(mex.Message.ToString)
    '    Catch ex As Exception
    '        Throw New Exception("Error on generating records : " & ex.Message.ToString)
    '    End Try
    'End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields

    'SRS/BTMU/2010-0027 
    'The above function is commented, which is the old logic
    Protected Overrides Sub GenerateOutputFormat()

        RECP_H = TblBankFields("RECP (H)").DefaultValue.ToString().Trim
        PAY_TYPE = TblBankFields("PayType").DefaultValue.ToString().Trim
        PAY_MODE = TblBankFields("PayMode").DefaultValue.ToString().Trim
        RECP_T = TblBankFields("RECP (T)").DefaultValue.ToString().Trim

        ' No Consolidation by default
        Dim recp13 As Integer ' Sequence Number allocated for each vendor
        Dim invAmt As Decimal
        Dim totalInvAmt As Decimal ' Total Invoice Amount for each Vendor
        Dim grandAmt As Decimal    ' Sum of Total Invoice Amount
        Try

            ' Check if any of the rows have invalid numeric data
            Dim _rowIndex As Integer = 0
            For Each _row As DataRow In _PreviewTable.Rows
                _rowIndex += 1
                If _row("InvAmt") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "InvAmt"))
                If Not Decimal.TryParse(_row("InvAmt").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "InvAmt"))
            Next

            recp13 = 0
            grandAmt = 0
            For Each row As DataRow In _PreviewTable.Rows
                recp13 += 1

                row("RECP1.1") = "1"
                row("RECP1.2") = "1"
                row("RECP1.3") = recp13.ToString
                row("RECP2.1") = "2"
                row("RECP2.2") = "1"
                row("RECP2.3") = recp13.ToString

                If _DataModel.IsAmountInDollars Then
                    invAmt = (Convert.ToDecimal(row("InvAmt").ToString) * 100).ToString()
                Else
                    invAmt = (Convert.ToDecimal(row("InvAmt").ToString)).ToString()
                End If

                If invAmt >= 0 Then
                    row("InvAmtSign") = "+"
                Else
                    row("InvAmtSign") = "-"
                End If

                If row("InvAmt") <> 0 Then
                    row("InvAmt") = Math.Abs(invAmt).ToString("#############")
                Else
                    row("InvAmt") = 0
                End If

                totalInvAmt = invAmt

                row("TotInvAmt") = Math.Abs(totalInvAmt).ToString("#############")

                If totalInvAmt >= 0 Then
                    row("TotInvAmtSign") = "+"
                Else
                    row("TotInvAmtSign") = "-"
                End If

                If row("InvAmtSign").ToString = "-" Then
                    grandAmt -= Convert.ToDecimal(row("InvAmt").ToString)
                ElseIf row("InvAmtSign").ToString = "+" Then
                    grandAmt += Convert.ToDecimal(row("InvAmt").ToString)
                End If

            Next
            _PreviewTable.Rows(0)("GrandNo") = recp13.ToString

            If grandAmt >= 0 Then
                _PreviewTable.Rows(0)("GrandAmtSign") = "+"
            Else
                _PreviewTable.Rows(0)("GrandAmtSign") = "-"
            End If
            _PreviewTable.Rows(0)("GrandAmt") = Math.Abs(grandAmt).ToString("#############")

        Catch mex As MagicException
            Throw New MagicException(mex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
        
    End Sub

    Protected Overrides Sub FormatOutput()
        Dim venName(1) As String
        'SRS/BTMU/2010-0033
        If IsNothingOrEmptyString(_PreviewTable.Rows(0)("RECP (H)").ToString) Then _PreviewTable.Rows(0)("RECP (H)") = RECP_H
        _PreviewTable.Rows(0)("RECP (H)") = _PreviewTable.Rows(0)("RECP (H)").ToString.Trim.PadLeft(TblBankFields("RECP (H)").DataLength.Value, "0")
        _PreviewTable.Rows(0)("ACNO") = _PreviewTable.Rows(0)("ACNO").ToString.Trim.PadLeft(TblBankFields("ACNO").DataLength.Value, "0")
        _PreviewTable.Rows(0)("Name") = _PreviewTable.Rows(0)("Name").ToString.PadRight(TblBankFields("Name").DataLength.Value, " ")
        If IsNothingOrEmptyString(_PreviewTable.Rows(0)("PayType").ToString) Then _PreviewTable.Rows(0)("PayType") = PAY_TYPE
        _PreviewTable.Rows(0)("PayType") = _PreviewTable.Rows(0)("PayType").ToString.PadRight(TblBankFields("PayType").DataLength.Value, " ")
        _PreviewTable.Rows(0)("GrandNo") = _PreviewTable.Rows(0)("GrandNo").ToString.Trim.PadLeft(TblBankFields("GrandNo").DataLength.Value, "0")
        _PreviewTable.Rows(0)("GrandAmt") = _PreviewTable.Rows(0)("GrandAmt").ToString.Trim.PadLeft(TblBankFields("GrandAmt").DataLength.Value, "0")

        If IsDateDataType(_PreviewTable.Rows(0)("PayDate").ToString, TblBankFields("PayDate").DateFormat, False) Then
            _PreviewTable.Rows(0)("PayDate") = _PreviewTable.Rows(0)("PayDate").ToString.Substring(0, 4) & "-" & _PreviewTable.Rows(0)("PayDate").ToString.Substring(4, 2) & "-" & _PreviewTable.Rows(0)("PayDate").ToString.Substring(6, 2)
        End If

        For Each _previewRecord As DataRow In _PreviewTable.Rows
            _previewRecord("RECP1.2") = _previewRecord("RECP1.2").ToString.Trim.PadLeft(TblBankFields("RECP1.2").DataLength.Value, "0")
            _previewRecord("RECP1.3") = _previewRecord("RECP1.3").ToString.Trim.PadLeft(TblBankFields("RECP1.3").DataLength.Value, "0")
            If IsNothingOrEmptyString(_previewRecord("PayMode").ToString) Then _previewRecord("PayMode") = PAY_MODE
            _previewRecord("PayMode") = _previewRecord("PayMode").ToString.PadRight(TblBankFields("PayMode").DataLength.Value, " ")
            _previewRecord("TotInvAmt") = _previewRecord("TotInvAmt").ToString.Trim.PadLeft(TblBankFields("TotInvAmt").DataLength.Value, "0")
            _previewRecord("VenCode") = _previewRecord("VenCode").ToString.PadRight(TblBankFields("VenCode").DataLength.Value, " ")
            _previewRecord("DocNum") = _previewRecord("DocNum").ToString.PadRight(TblBankFields("DocNum").DataLength.Value, " ")
            _previewRecord("VenName1") = _previewRecord("VenName1").ToString.PadRight(TblBankFields("VenName1").DataLength.Value, " ")
            _previewRecord("VenName2") = _previewRecord("VenName2").ToString.PadRight(TblBankFields("VenName2").DataLength.Value, " ")
            _previewRecord("VenAdd1") = _previewRecord("VenAdd1").ToString.PadRight(TblBankFields("VenAdd1").DataLength.Value, " ")
            _previewRecord("VenAdd2") = _previewRecord("VenAdd2").ToString.PadRight(TblBankFields("VenAdd2").DataLength.Value, " ")
            _previewRecord("VenAdd3") = _previewRecord("VenAdd3").ToString.PadRight(TblBankFields("VenAdd3").DataLength.Value, " ")
            _previewRecord("VenAdd4") = _previewRecord("VenAdd4").ToString.PadRight(TblBankFields("VenAdd4").DataLength.Value, " ")
            _previewRecord("RECP2.2") = _previewRecord("RECP2.2").ToString.Trim.PadLeft(TblBankFields("RECP2.2").DataLength.Value, "0")
            _previewRecord("RECP2.3") = _previewRecord("RECP2.3").ToString.Trim.PadLeft(TblBankFields("RECP2.3").DataLength.Value, "0")
            _previewRecord("InvAmt") = _previewRecord("InvAmt").ToString.Trim.PadLeft(TblBankFields("InvAmt").DataLength.Value, "0")
            _previewRecord("InvNum") = _previewRecord("InvNum").ToString.PadRight(TblBankFields("InvNum").DataLength.Value, " ")

            ' Generate VenName2 from VenName1
            ' If length of VenName1 > 35, then the remaining portion of the vendor Name
            ' starting with the word that exceeds the length should be displayed in VenName2
            If _previewRecord("VenName1").ToString.Length > TblBankFields("VenName1").DataLength.Value Then
                venName = SplitVenName(_previewRecord("VenName1").ToString, TblBankFields("VenName1").DataLength.Value)
                _previewRecord("VenName1") = venName(0)
                _previewRecord("VenName2") = venName(1)
            End If

        Next

        If IsNothingOrEmptyString(_PreviewTable.Rows(0)("RECP (T)").ToString) Then _PreviewTable.Rows(0)("RECP (T)") = RECP_T
        _PreviewTable.Rows(0)("RECP (T)") = _PreviewTable.Rows(0)("RECP (T)").ToString.Trim.PadLeft(TblBankFields("RECP (T)").DataLength.Value, "0")

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("PayDate") = ValueDate.ToString(TblBankFields("PayDate").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub GenerateHeader()
        Dim formatError As FileFormatError

        Try
            _Header = String.Empty
            For Each bankField As MapBankField In TblBankFields.Values
                If bankField.Header Then
                    If _PreviewTable.Rows(0)(bankField.BankField).ToString.Length > TblBankFields(bankField.BankField).DataLength.Value Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = bankField.BankField
                            .ColumnOrdinalNo = _PreviewTable.Columns(bankField.BankField).Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020050"), "Header", "", bankField.BankField, TblBankFields(bankField.BankField).DataLength.Value)
                            .RecordNo = 0
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                    _Header += _PreviewTable.Rows(0)(bankField.BankField).ToString
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Error on generating Header record : " & ex.Message.ToString)
        End Try

    End Sub

    Protected Overrides Sub GenerateTrailer()
        Dim formatError As FileFormatError

        Try
            _Trailer = String.Empty
            For Each bankfield As MapBankField In TblBankFields.Values
                If bankfield.Trailer Then
                    If _PreviewTable.Rows(0)(bankfield.BankField).ToString.Length > TblBankFields(bankfield.BankField).DataLength.Value Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = bankfield.BankField
                            .ColumnOrdinalNo = _PreviewTable.Columns(bankfield.BankField).Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020050"), "Trailer", "", bankfield.BankField, TblBankFields(bankfield.BankField).DataLength.Value)
                            .RecordNo = 0
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                    _Trailer += _PreviewTable.Rows(0)(bankfield.BankField).ToString()
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Error on generating Trailer record : " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()
        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim colNo As Integer = 0
        Dim totalAmount As Decimal

        totalAmount = 0
        Try
            rowNo = 0
            For Each _row As DataRow In _PreviewTable.Rows
                ' validate the fields of this record and add file format _validationerrors to _validationerrors object
                If rowNo = 0 Then
                    ' Header and Trailer Field Validations
                    ' ****************************Header Fields Validations *****************************'
                    ' UAT Added : 
                    ' Validation - RECP (H) : Must be 0000000
                    If _row("RECP (H)").ToString <> String.Empty Then
                        Dim defRecpH() As String
                        If Not IsNothingOrEmptyString(TblBankFields("RECP (H)").DefaultValue) Then
                            defRecpH = TblBankFields("RECP (H)").DefaultValue.Split(",")
                            RECP_H = defRecpH(0)
                            If Not _row("RECP (H)").ToString.Equals(RECP_H, StringComparison.InvariantCultureIgnoreCase) Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "RECP (H)"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("RECP (H)").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09030140"), "Header", "", "RECP (H)", RECP_H)
                                    .RecordNo = rowNo
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If

                    End If

                    ' Validation - PayType : Must be VEN
                    If _row("PayType").ToString <> String.Empty Then
                        Dim defPayType() As String
                        If Not IsNothingOrEmptyString(TblBankFields("PayType").DefaultValue) Then
                            defPayType = TblBankFields("PayType").DefaultValue.Split(",")
                            PAY_TYPE = defPayType(0)
                            If Not _row("PayType").ToString.Equals(PAY_TYPE, StringComparison.InvariantCultureIgnoreCase) Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "PayType"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("PayType").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09030140"), "Header", "", "PayType", PAY_TYPE)
                                    .RecordNo = rowNo
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If

                        End If

                    End If



                    ' Validation - ACNO : Must be numeric
                    If _row("ACNO").ToString.Trim <> String.Empty And Not IsNumeric(_row("ACNO").ToString.Trim) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "ACNO"
                            .ColumnOrdinalNo = _PreviewTable.Columns("ACNO").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020010"), "Header", "", "ACNO")
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - PayDate : Must be a valid date
                    Dim _paydate As Date
                    If Not Date.TryParse(_row("PayDate"), _paydate) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "PayDate"
                            .ColumnOrdinalNo = _PreviewTable.Columns("PayDate").Ordinal + 1
                            .Description = "Header" & vbTab & "Field : Pay Date must be a valid date."
                            .RecordNo = 0
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Validation - PayDate : Should be YYYY-MM-DD format 
                    If Not IsDateDataType(_row("PayDate").ToString, "YYYY-MM-DD", False) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "PayDate"
                            .ColumnOrdinalNo = _PreviewTable.Columns("PayDate").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030060"), "Header", "", "PayDate", TblBankFields("PayDate").DateFormat)
                            .RecordNo = rowNo
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    Else
                        ' Validation - PayDate : Must be greater than Today's date. 
                        If IsDateDataType(_row("PayDate").ToString, "-", "YYYYMMDD", _paydate, True) Then
                            If Date.Compare(_paydate, Date.Today) <= 0 Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "PayDate"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("PayDate").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09020020"), "Header", "", "PayDate")
                                    .RecordNo = rowNo
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If

                        ' Validation - PayDate : Must not be Saturday or Sunday. 
                        If Weekday(_paydate) = 1 Or Weekday(_paydate) = 7 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "PayDate"
                                .ColumnOrdinalNo = _PreviewTable.Columns("PayDate").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09020030"), "Header", "", "PayDate")
                                .RecordNo = rowNo
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                    ' Validation - RECP (T) : Must be 9999999
                    If _row("RECP (T)").ToString <> String.Empty Then
                        Dim defRecpT() As String
                        If Not IsNothingOrEmptyString(TblBankFields("RECP (T)").DefaultValue) Then
                            defRecpT = TblBankFields("RECP (T)").DefaultValue.Split(",")
                            RECP_T = defRecpT(0)
                            If Not _row("RECP (T)").ToString.Equals(RECP_T, StringComparison.InvariantCultureIgnoreCase) Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "RECP (T)"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("RECP (T)").Ordinal + 1
                                    .Description = String.Format(MsgReader.GetString("E09030140"), "Trailer", "", "RECP (T)", RECP_T)
                                    .RecordNo = rowNo
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                ' ****************************Detail Fields Validations *****************************'
                ' Validation - PayMode : Must be CO
                If _row("PayMode").ToString <> String.Empty Then
                    Dim defPayMode() As String
                    If Not IsNothingOrEmptyString(TblBankFields("PayMode").DefaultValue) Then
                        defPayMode = TblBankFields("PayMode").DefaultValue.Split(",")
                        PAY_MODE = defPayMode(0)
                        If Not _row("PayMode").ToString.Equals(PAY_MODE, StringComparison.InvariantCultureIgnoreCase) Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "PayMode"
                                .ColumnOrdinalNo = _PreviewTable.Columns("PayMode").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09030140"), "Record", rowNo + 1, "PayMode", PAY_MODE)
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

                ' Validation - TotInvAmt : Must be > 0
                If _row("TotInvAmtSign").ToString.Trim = String.Empty Or _
                    _row("TotInvAmtSign").ToString = "-" Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "TotInvAmt"
                        .ColumnOrdinalNo = _PreviewTable.Columns("TotInvAmt").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020070"), "Record", rowNo + 1, "TotInvAmt")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - VenAdd1 : Is mandatory if DeliveryMode is 0,1 or 2
                If _row("VenAdd1").ToString.Trim = String.Empty And _
                    (_row("DeliveryMode").ToString.Trim = "0" Or _
                     _row("DeliveryMode").ToString.Trim = "1" Or _
                     _row("DeliveryMode").ToString.Trim = "2") Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "VenAdd1"
                        .ColumnOrdinalNo = _PreviewTable.Columns("VenAdd1").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020060"), "Record", rowNo + 1, "VenAdd1")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - RECP1.2 : Must be < 999
                If _row("RECP1.2").ToString.Trim <> String.Empty And _
                    Val(_row("RECP1.2").ToString) > 999 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "RECP1.2"
                        .ColumnOrdinalNo = _PreviewTable.Columns("RECP1.2").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "RECP1.2", "999")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - RECP1.3 : Must be < 999
                If _row("RECP1.3").ToString.Trim <> String.Empty And _
                    Val(_row("RECP1.3").ToString) > 999 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "RECP1.3"
                        .ColumnOrdinalNo = _PreviewTable.Columns("RECP1.3").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "RECP1.3", "999")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - Delivery Mode : Must be one of these values ("",0,1,2,3,4,5,6,7)
                If _row("DeliveryMode").ToString <> String.Empty Then
                    If TblBankFields("DeliveryMode").DefaultValue.Length > 0 Then
                        Dim defaultValue() As String
                        Dim isFound As Boolean = False
                        defaultValue = TblBankFields("DeliveryMode").DefaultValue.Split(",")
                        For Each val As String In defaultValue
                            If _row("DeliveryMode").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                isFound = True
                                Exit For
                            End If
                        Next
                        If Not isFound Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "DeliveryMode"
                                .ColumnOrdinalNo = _PreviewTable.Columns("DeliveryMode").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09030100"), "Record", rowNo + 1, "DeliveryMode")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

                ' Validation - RECP2.2 : Must be < 999
                If _row("RECP2.2").ToString.Trim <> String.Empty And _
                    Val(_row("RECP2.2").ToString) > 999 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "RECP2.2"
                        .ColumnOrdinalNo = _PreviewTable.Columns("RECP2.2").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "RECP2.2", "999")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - RECP2.3 : Must be < 999
                If _row("RECP2.3").ToString.Trim <> String.Empty And _
                    Val(_row("RECP2.3").ToString) > 999 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "RECP2.3"
                        .ColumnOrdinalNo = _PreviewTable.Columns("RECP2.3").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "RECP2.3", "999")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - InvAmt : Must not exceed 13 digits
                If _row("InvAmt").ToString.Trim <> String.Empty And _
                    _row("InvAmt").ToString.Length > TblBankFields("InvAmt").DataLength.Value Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "InvAmt"
                        .ColumnOrdinalNo = _PreviewTable.Columns("InvAmt").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020050"), "Record", rowNo + 1, "InvAmt", TblBankFields("InvAmt").DataLength.Value)
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Validation - TotInvAmt : Must not exceed 13 digits
                If _row("TotInvAmt").ToString.Trim <> String.Empty And _
                     _row("TotInvAmt").ToString.Length > TblBankFields("TotInvAmt").DataLength.Value Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "TotInvAmt"
                        .ColumnOrdinalNo = _PreviewTable.Columns("TotInvAmt").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020050"), "Record", rowNo + 1, "TotInvAmt", TblBankFields("TotInvAmt").DataLength.Value)
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                '' Validation for Payment Date

                ' Validation - Do not allow unicode characters
                Dim pattern As String = "[^0-9A-Za-z\s" & Regex.Escape("@#%&*()_+;:""'<>,.?/~`!$^[{}|\") & "\-\=\]]"
                Dim regAllow As New Regex(pattern, RegexOptions.Compiled)
                For Each col As String In TblBankFields.Keys
                    If _row(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_row(col).ToString) Then
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = col
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid character.", rowNo + 1, col)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True
                    End If
                Next

                rowNo += 1
            Next
        Catch ex As Exception
            Throw New Exception("Error on validating records : " & ex.Message.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' Generates the Formatted File
    ''' </summary>
    ''' <param name="_userName">User that requested for File Generation</param>
    ''' <param name="_sourceFile">Name of Source File</param>
    ''' <param name="_outputFile">Name of Output file</param>
    ''' <param name="_objMaster">Master Template</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean
        'Dim delimiter As String
        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim venName1 As String ' Believe this is same as RECP2.3
        Dim colNo As Integer
        Try
            ' EPS format is Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateFixed
            If _objMaster.GetType Is GetType(MasterTemplateFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateFixed)
            Else
                Throw New Exception("Invalid Master Template")
            End If

            ' Get the Delimiter, Enclosure character etc. No idea from where?
            If Header <> String.Empty Then
                text &= Header & vbNewLine
            End If

            venName1 = String.Empty

            'Added on 18 Sept 2009 '''''''''''''''''''''''''''
            Dim linebrk As Boolean = False
            Dim gotFirstRowValue As Boolean = False
            ''''''''''''''''''''''''''' '''''''''''''''''''''''''''

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                line = ""
                colNo = 0
                linebrk = False

                'Added on 18 Sept 2009 '''''''''''''''''''''''''''
                If gotFirstRowValue Then
                    If venName1 = _previewRecord("RECP2.3").ToString Then
                        linebrk = True
                    Else
                        venName1 = _previewRecord("RECP2.3").ToString
                        linebrk = False
                    End If
                Else

                    gotFirstRowValue = True
                    venName1 = _previewRecord("RECP2.3").ToString
                End If

                ''''''''''''''''''''''''''' '''''''''''''''''''''''''''

                For Each _column As String In TblBankFields.Keys

                    If TblBankFields(_column).Detail.ToUpper <> "NO" Then '(colno < 8 and colno > 29)

                        If Not linebrk Then  'venName1 = _previewRecord("RECP2.3").ToString Then 'col 26
                            If TblBankFields(_column).DataType = "Number" Or _
                                TblBankFields(_column).DataType = "Currency" Then
                                line &= _previewRecord(_column).ToString
                            Else
                                line = line & _previewRecord(_column).ToString.PadRight(TblBankFields(_column).DataLength, " ")
                            End If
                            If colNo = 22 Then ' col 23
                                linebrk = True
                                line &= vbNewLine
                            End If
                        Else
                            'venName1 = _previewRecord("RECP2.3").ToString
                            ' VenName1 = RECP2.3
                            If colNo > 22 Then
                                If TblBankFields(_column).DataType = "Number" Or _
                                                            TblBankFields(_column).DataType = "Currency" Then
                                    line &= _previewRecord(_column).ToString
                                Else
                                    line = line & _previewRecord(_column).ToString.PadRight(TblBankFields(_column).DataLength, " ")
                                End If
                            End If
                        End If
                    End If
                    colNo += 1
                Next



                If objMasterTemplate.NewLine Then
                    If rowNo < _PreviewTable.Rows.Count - 1 Then
                        text = text & line & vbNewLine
                    Else
                        text = text & line
                    End If
                Else
                    If rowNo < _PreviewTable.Rows.Count - 1 Then
                        text = text & line & objMasterTemplate.DelimiterCharacter
                    Else
                        text = text & line
                    End If
                End If
                rowNo += 1
            Next

            If Trailer <> String.Empty Then
                text &= vbNewLine & Trailer
            End If

            If SaveTextToFile(text, _outputFile) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception("Error on file Generation : " & ex.Message.ToString)
        End Try
        ' Need to add code for encrption
    End Function

    '''' <summary>
    '''' Generates Consolidated Data
    '''' </summary>
    '''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    '''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    '''' <param name="AppendText">Text to append with Reference Fields</param>
    '''' <remarks></remarks>
    'Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))
    '    If GroupByFields.Count = 0 Then Exit Sub

    '    '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
    '    ValidateGroupByAndReferenceFields(GroupByFields, ReferenceFields)

    '    '''''''Dont like to disturb the Previous Argument Setting, so making it up here
    '    Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
    '    Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

    '    _referenceFields(0, 0) = ""
    '    _referenceFields(0, 1) = ""

    '    For idx As Integer = 0 To GroupByFields.Count - 1
    '        _groupByFields(idx) = GroupByFields(idx)
    '    Next

    '    For idx As Integer = 0 To ReferenceFields.Count - 1
    '        If idx < AppendText.Count Then
    '            _referenceFields(idx, 0) = AppendText(idx)
    '        Else
    '            _referenceFields(idx, 0) = ""
    '        End If
    '        _referenceFields(idx, 1) = ReferenceFields(idx)
    '    Next
    '    ''''''''' End of Previous Argument Setting '''''''''''''



    '    Dim vendorList As New ArrayList

    '    Dim recp12 As Integer ' Total Number of Type 2 records for each vendor
    '    Dim recp13 As Integer ' Sequence Number allocated for each vendor
    '    Dim recp22 As Integer ' No:of Type 2 records for each vendor
    '    Dim recp23 As Integer ' Vendor Number
    '    Dim sourceDataView As DataView

    '    Dim filterString As String

    '    Dim totalInvAmt As Decimal
    '    Dim grandAmt As Decimal

    '    Dim venCode As String
    '    Dim venName1 As String
    '    Dim venName2 As String

    '    Dim _vendor As Vendor
    '    Dim referenceField(1) As String
    '    Try
    '        ' Check if any of the rows have invalid numeric data
    '        Dim _rowIndex As Integer = 0
    '        For Each _row As DataRow In _PreviewTable.Rows
    '            _rowIndex += 1
    '            If _row("InvAmt") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "InvAmt"))
    '            If Not Decimal.TryParse(_row("InvAmt").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "InvAmt"))
    '            'If Convert.ToDecimal(_row("InvAmt").ToString) <= 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "InvAmt", "0"))
    '        Next

    '        '2. Fixed quick conversion field name
    '        For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
    '            If _groupByFields(intI).Trim.Length <> 0 Then
    '                For Each temp As String In TblBankFields.Keys
    '                    If temp.ToUpper = _groupByFields(intI).ToUpper Then
    '                        _groupByFields(intI) = temp
    '                    End If
    '                Next
    '            End If
    '        Next

    '        For Each _field As String In _groupByFields
    '            If Not TblBankFields.ContainsKey(_field) Then
    '                Throw New Exception("Group by field not defined in Master Template")
    '            End If
    '        Next

    '        '2. Fixed quick conversion field name
    '        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
    '            If _referenceFields(intI, 1).Trim.Length <> 0 Then
    '                For Each temp As String In TblBankFields.Keys
    '                    If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
    '                        _referenceFields(intI, 1) = temp
    '                    End If
    '                Next
    '            End If
    '        Next

    '        For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
    '            If _referenceFields(intI, 1).Trim.Length <> 0 Then
    '                If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
    '                    Throw New Exception("Reference field not defined in Master Template")
    '                End If
    '            End If
    '        Next

    '        Select Case _groupByFields.GetUpperBound(0)
    '            Case -1
    '                venCode = "VenCode"
    '                venName1 = "VenName1"
    '                venName2 = "VenName2"
    '            Case 0
    '                venCode = _groupByFields(0)
    '                venName1 = _groupByFields(0)
    '                venName2 = _groupByFields(0)
    '            Case 1
    '                venCode = _groupByFields(0)
    '                venName1 = _groupByFields(1)
    '                venName2 = _groupByFields(1)
    '            Case 2
    '                venCode = _groupByFields(0)
    '                venName1 = _groupByFields(1)
    '                venName2 = _groupByFields(2)
    '            Case Else
    '                Throw New Exception("Duplicate group by fields exists")
    '        End Select

    '        For Each _previewRecord As DataRow In _PreviewTable.Rows
    '            _vendor.venCode = _previewRecord(venCode).ToString
    '            _vendor.venName1 = _previewRecord(venName1).ToString
    '            _vendor.venName2 = _previewRecord(venName2).ToString
    '            If vendorList.IndexOf(_vendor) = -1 Then vendorList.Add(_vendor)
    '        Next

    '        recp13 = 0
    '        sourceDataView = _PreviewTable.DefaultView
    '        sourceDataView.AllowEdit = True
    '        grandAmt = 0
    '        For Each vendor As Vendor In vendorList
    '            filterString = "[" & venCode & "] = '" & vendor.venCode.Replace("'", "''") & "' AND [" & _
    '                                 venName1 & "] = '" & vendor.venName1.Replace("'", "''") & "' AND [" & _
    '                                 venName2 & "] = '" & vendor.venName2.Replace("'", "''") & "'"
    '            sourceDataView.RowFilter = String.Empty
    '            sourceDataView.RowFilter = filterString
    '            recp12 = sourceDataView.Count
    '            recp13 += 1
    '            recp22 = 0
    '            recp23 = recp13
    '            totalInvAmt = 0

    '            For Each _row As DataRowView In sourceDataView
    '                recp22 += 1

    '                _row.BeginEdit()
    '                _row("RECP1.1") = "1"
    '                _row("RECP1.2") = recp12.ToString.PadLeft(TblBankFields("RECP1.2").DataLength.Value, "0")
    '                _row("RECP1.3") = recp13.ToString.PadLeft(TblBankFields("RECP1.3").DataLength.Value, "0")
    '                _row("RECP2.1") = "2"
    '                _row("RECP2.2") = recp22.ToString.PadLeft(TblBankFields("RECP2.2").DataLength.Value, "0")
    '                _row("RECP2.3") = recp23.ToString.PadLeft(TblBankFields("RECP2.3").DataLength.Value, "0")

    '                ' Create Reference Fields
    '                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
    '                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
    '                        referenceField(intI) = _referenceFields(intI, 0).ToString
    '                    End If
    '                Next

    '                If _row("InvAmt").ToString <> "" Then
    '                    _row("InvAmt") = Convert.ToDecimal(_row("InvAmt").ToString)

    '                    If IsNegativePaymentOption And PaymentOption = "+" Then
    '                        If _row("InvAmtSign").ToString = "+" Then
    '                            _row("InvAmtSign") = "-"
    '                        ElseIf _row("InvAmtSign").ToString = "-" Then
    '                            _row("InvAmtSign") = "+"
    '                        End If
    '                    End If

    '                    If Not (IsNegativePaymentOption) And PaymentOption = "-" Then
    '                        If _row("InvAmtSign").ToString = "+" Then
    '                            _row("InvAmtSign") = "-"
    '                        ElseIf _row("InvAmtSign").ToString = "-" Then
    '                            _row("InvAmtSign") = "+"
    '                        End If
    '                    End If
    '                End If
    '                If _row("InvAmt") <> 0 Then
    '                    _row("InvAmt") = Math.Abs(Convert.ToDecimal(_row("InvAmt").ToString)).ToString("#############")
    '                Else
    '                    _row("InvAmt") = 0
    '                End If

    '                '_row("InvAmt") = Math.Abs(Convert.ToDecimal(_row("InvAmt").ToString)).ToString("##########")
    '                '_row("InvAmt") = _row("InvAmt").ToString("###########")
    '                _row("InvAmt") = _row("InvAmt").ToString.PadLeft(TblBankFields("InvAmt").DataLength.Value, "0")

    '                If _row("InvAmtSign").ToString = "-" Then
    '                    totalInvAmt = totalInvAmt - Convert.ToDecimal(_row("InvAmt").ToString)
    '                    grandAmt = grandAmt - Convert.ToDecimal(_row("InvAmt").ToString)
    '                ElseIf _row("InvAmtSign").ToString = "+" Then
    '                    totalInvAmt = totalInvAmt + Convert.ToDecimal(_row("InvAmt").ToString)
    '                    grandAmt = grandAmt + Convert.ToDecimal(_row("InvAmt").ToString)
    '                End If

    '                For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
    '                    If _referenceFields(intI, 1).Trim.Length <> 0 Then
    '                        referenceField(intI) = referenceField(intI) & _row(_referenceFields(intI, 1).ToString) & ","
    '                        '_row(_referenceFields(intI, 1).ToString) = referenceField(intI)
    '                    End If
    '                Next

    '                _row.EndEdit()

    '            Next

    '            For Each _row As DataRowView In sourceDataView
    '                _row.BeginEdit()
    '                _row("TotInvAmt") = totalInvAmt
    '                ' Update the total Inv Amount 
    '                If totalInvAmt >= 0 Then
    '                    _row("TotInvAmtSign") = "+"
    '                Else
    '                    _row("TotInvAmtSign") = "-"
    '                End If
    '                _row("TotInvAmt") = Math.Abs(totalInvAmt).ToString("#############").PadLeft(TblBankFields("TotInvAmt").DataLength.Value, "0")
    '                _row.EndEdit()
    '            Next
    '        Next

    '        ' Update the Grand Amount for the Header.Since the first row is taken as header, need to
    '        ' update only on the first row

    '        ' RECP (H)
    '        _PreviewTable.DefaultView.RowFilter = String.Empty
    '        _PreviewTable.Rows(0)("GrandNo") = recp13.ToString.PadLeft(TblBankFields("GrandNo").DataLength.Value, "0")
    '        If grandAmt >= 0 Then
    '            _PreviewTable.Rows(0)("GrandAmtSign") = "+"
    '        Else
    '            _PreviewTable.Rows(0)("GrandAmtSign") = "-"
    '        End If
    '        _PreviewTable.Rows(0)("GrandAmt") = Math.Abs(grandAmt).ToString.PadLeft(TblBankFields("GrandAmt").DataLength.Value, "0")
    '        _PreviewTable.DefaultView.RowFilter = String.Empty
    '        _PreviewTable.Columns.Add(New DataColumn("RECP23", GetType(Integer)))
    '        _PreviewTable.Columns.Add(New DataColumn("RECP22", GetType(Integer)))
    '        _PreviewTable.Columns("RECP23").Expression = "Convert([RECP2.3], 'System.Int32')"
    '        _PreviewTable.Columns("RECP22").Expression = "Convert([RECP2.2], 'System.Int32')"
    '        Dim dv As DataView
    '        dv = _PreviewTable.DefaultView
    '        dv.Sort = "RECP23 ASC, RECP22 ASC"
    '        Dim dt As DataTable
    '        dt = dv.ToTable()
    '        dt.Columns.Remove("RECP22")
    '        dt.Columns.Remove("RECP23")
    '        _PreviewTable = dt
    '        dt.Dispose()
    '        ValidationErrors.Clear()
    '        GenerateHeader()
    '        GenerateTrailer()
    '        FormatOutput()
    '        ValidateDataLength()
    '        ValidateMandatoryFields()
    '        Validate()
    '        _totalRecordCount = _PreviewTable.Rows.Count
    '        _consolidatedRecordCount = _PreviewTable.Rows.Count

    '        sourceDataView.RowFilter = String.Empty

    '    Catch mex As MagicException
    '        Throw New MagicException(mex.Message.ToString)
    '    Catch ex As Exception
    '        Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
    '    End Try
    'End Sub

    'SRS/BTMU/2010-0027
    'The above function is commented which is the old logic

    ''' <summary>
    ''' Generates Consolidated Data
    ''' </summary>
    ''' <param name="GroupByFields">List of Comma Separated Bank Fields</param>
    ''' <param name="ReferenceFields">List of Comma Separated Reference Fields</param>
    ''' <param name="AppendText">Text to append with Reference Fields</param>
    ''' <remarks></remarks>
    Public Overrides Sub GenerateConsolidatedData(ByVal GroupByFields As List(Of String), ByVal ReferenceFields As List(Of String), ByVal AppendText As List(Of String))
        If GroupByFields.Count = 0 Then Exit Sub

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(GroupByFields, ReferenceFields)

        '''''''Dont like to disturb the Previous Argument Setting, so making it up here
        Dim _groupByFields(IIf(GroupByFields.Count = 0, 0, GroupByFields.Count - 1)) As String
        Dim _referenceFields(IIf(ReferenceFields.Count = 0, 0, ReferenceFields.Count - 1), 1) As String

        _referenceFields(0, 0) = ""
        _referenceFields(0, 1) = ""

        For idx As Integer = 0 To GroupByFields.Count - 1
            _groupByFields(idx) = GroupByFields(idx)
        Next

        For idx As Integer = 0 To ReferenceFields.Count - 1
            If idx < AppendText.Count Then
                _referenceFields(idx, 0) = AppendText(idx)
            Else
                _referenceFields(idx, 0) = ""
            End If
            _referenceFields(idx, 1) = ReferenceFields(idx)
        Next
        ''''''''' End of Previous Argument Setting '''''''''''''

        Dim recp12 As Integer ' Total Number of Type 2 records for each vendor
        Dim recp13 As Integer ' Sequence Number allocated for each vendor
        Dim recp22 As Integer ' No:of Type 2 records for each vendor
        Dim recp23 As Integer ' Vendor Number
        Dim sourceDataView As DataView

        Dim filterString As String

        Dim totalInvAmt As Decimal
        Dim grandAmt As Decimal

        Dim referenceField(1) As String
        Try
            ' Check if any of the rows have invalid numeric data
            Dim _rowIndex As Integer = 0
            For Each _row As DataRow In _PreviewTable.Rows
                _rowIndex += 1
                If _row("InvAmt") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex + 1, "InvAmt"))
                If Not Decimal.TryParse(_row("InvAmt").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex + 1, "InvAmt"))
                'If Convert.ToDecimal(_row("InvAmt").ToString) <= 0 Then Throw New MagicException(String.Format(MsgReader.GetString("E09030110"), "Record ", _rowIndex + 1, "InvAmt", "0"))
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _groupByFields.GetUpperBound(0)
                If _groupByFields(intI).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _groupByFields(intI).ToUpper Then
                            _groupByFields(intI) = temp
                        End If
                    Next
                End If
            Next

            For Each _field As String In _groupByFields
                If Not TblBankFields.ContainsKey(_field) Then
                    Throw New Exception("Group by field not defined in Master Template")
                End If
            Next

            '2. Fixed quick conversion field name
            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    For Each temp As String In TblBankFields.Keys
                        If temp.ToUpper = _referenceFields(intI, 1).ToUpper Then
                            _referenceFields(intI, 1) = temp
                        End If
                    Next
                End If
            Next

            For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                If _referenceFields(intI, 1).Trim.Length <> 0 Then
                    If Not TblBankFields.ContainsKey(_referenceFields(intI, 1).ToString) Then
                        Throw New Exception("Reference field not defined in Master Template")
                    End If
                End If
            Next

            recp13 = 0
            sourceDataView = _PreviewTable.DefaultView
            sourceDataView.AllowEdit = True
            grandAmt = 0
            'add new column called IsConsolidated
            _PreviewTable.Columns.Add(New DataColumn("IsConsolidated", GetType(Boolean)))
            _PreviewTable.Columns("IsConsolidated").ReadOnly = False

            'Fill value false to IsConsolidated column
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("IsConsolidated") = False
            Next

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If _previewRecord("IsConsolidated") Then Continue For
                filterString = "1=1"

                For Each _field As String In _groupByFields

                    If IsNothingOrEmptyString(_field) Then Continue For

                    'prepare filterstring with group by fields
                    filterString = String.Format("{0} AND {1} = '{2}'", _
                       filterString, HelperModule.EscapeSpecialCharsForColumnName(_field) _
                        , HelperModule.EscapeSingleQuote(_previewRecord(_field).ToString().ToUpper))
                Next

                filterString = filterString.Replace("1=1 AND ", "")
                filterString = filterString.Replace("1=1", "")

                If filterString = "" Then Continue For


                sourceDataView.RowFilter = String.Empty
                sourceDataView.RowFilter = filterString
                recp12 = sourceDataView.Count
                recp13 += 1
                recp22 = 0
                recp23 = recp13
                totalInvAmt = 0

                For Each _row As DataRowView In sourceDataView
                    recp22 += 1

                    _row.BeginEdit()
                    'prepare data
                    _row("RECP1.1") = "1"
                    _row("RECP1.2") = recp12.ToString.PadLeft(TblBankFields("RECP1.2").DataLength.Value, "0")
                    _row("RECP1.3") = recp13.ToString.PadLeft(TblBankFields("RECP1.3").DataLength.Value, "0")
                    _row("RECP2.1") = "2"
                    _row("RECP2.2") = recp22.ToString.PadLeft(TblBankFields("RECP2.2").DataLength.Value, "0")
                    _row("RECP2.3") = recp23.ToString.PadLeft(TblBankFields("RECP2.3").DataLength.Value, "0")

                    ' Create Reference Fields
                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            referenceField(intI) = _referenceFields(intI, 0).ToString
                        End If
                    Next

                    If _row("InvAmt").ToString <> "" Then
                        _row("InvAmt") = Convert.ToDecimal(_row("InvAmt").ToString)

                        'check payment option to reflect with Invoice amount sign
                        If IsNegativePaymentOption And PaymentOption = "+" Then
                            If _row("InvAmtSign").ToString = "+" Then
                                _row("InvAmtSign") = "-"
                            ElseIf _row("InvAmtSign").ToString = "-" Then
                                _row("InvAmtSign") = "+"
                            End If
                        End If

                        If Not (IsNegativePaymentOption) And PaymentOption = "-" Then
                            If _row("InvAmtSign").ToString = "+" Then
                                _row("InvAmtSign") = "-"
                            ElseIf _row("InvAmtSign").ToString = "-" Then
                                _row("InvAmtSign") = "+"
                            End If
                        End If
                    End If

                    'convert amount value to decimal and take absolute value
                    If _row("InvAmt") <> 0 Then
                        _row("InvAmt") = Math.Abs(Convert.ToDecimal(_row("InvAmt").ToString)).ToString("#############")
                    Else
                        _row("InvAmt") = 0
                    End If

                    'padding zeroes in front
                    _row("InvAmt") = _row("InvAmt").ToString.PadLeft(TblBankFields("InvAmt").DataLength.Value, "0")

                    'sum up the amount value
                    If _row("InvAmtSign").ToString = "-" Then
                        totalInvAmt = totalInvAmt - Convert.ToDecimal(_row("InvAmt").ToString)
                        grandAmt = grandAmt - Convert.ToDecimal(_row("InvAmt").ToString)
                    ElseIf _row("InvAmtSign").ToString = "+" Then
                        totalInvAmt = totalInvAmt + Convert.ToDecimal(_row("InvAmt").ToString)
                        grandAmt = grandAmt + Convert.ToDecimal(_row("InvAmt").ToString)
                    End If

                    For intI As Integer = 0 To _referenceFields.GetUpperBound(0)
                        If _referenceFields(intI, 1).Trim.Length <> 0 Then
                            referenceField(intI) = referenceField(intI) & _row(_referenceFields(intI, 1).ToString)
                            _row(_referenceFields(intI, 1).ToString) = referenceField(intI)
                        End If
                    Next

                    _row("IsConsolidated") = True
                    _row.EndEdit()

                Next

                'update amount after consolidation is done
                For Each _row As DataRowView In sourceDataView
                    _row.BeginEdit()
                    _row("TotInvAmt") = totalInvAmt
                    ' Update the total Inv Amount 
                    If totalInvAmt >= 0 Then
                        _row("TotInvAmtSign") = "+"
                    Else
                        _row("TotInvAmtSign") = "-"
                    End If
                    _row("TotInvAmt") = Math.Abs(totalInvAmt).ToString("#############").PadLeft(TblBankFields("TotInvAmt").DataLength.Value, "0")
                    _row.EndEdit()
                Next
                sourceDataView.RowFilter = String.Empty
            Next

            ' Update the Grand Amount for the Header.Since the first row is taken as header, need to
            ' update only on the first row

            ' RECP (H)
            _PreviewTable.DefaultView.RowFilter = String.Empty
            _PreviewTable.Rows(0)("GrandNo") = recp13.ToString.PadLeft(TblBankFields("GrandNo").DataLength.Value, "0")
            If grandAmt >= 0 Then
                _PreviewTable.Rows(0)("GrandAmtSign") = "+"
            Else
                _PreviewTable.Rows(0)("GrandAmtSign") = "-"
            End If
            _PreviewTable.Rows(0)("GrandAmt") = Math.Abs(grandAmt).ToString.PadLeft(TblBankFields("GrandAmt").DataLength.Value, "0")
            _PreviewTable.DefaultView.RowFilter = String.Empty

            'added two new columns by converting recp2.3 and recp2.2 to integer type
            _PreviewTable.Columns.Add(New DataColumn("RECP23", GetType(Integer)))
            _PreviewTable.Columns.Add(New DataColumn("RECP22", GetType(Integer)))
            _PreviewTable.Columns("RECP23").Expression = "Convert([RECP2.3], 'System.Int32')"
            _PreviewTable.Columns("RECP22").Expression = "Convert([RECP2.2], 'System.Int32')"

            Dim dv As DataView
            dv = _PreviewTable.DefaultView
            'sort by vendor seq no, payment record seq no.
            dv.Sort = "RECP23 ASC, RECP22 ASC"
            Dim dt As DataTable
            dt = dv.ToTable()

            'remove extra columns added manually
            dt.Columns.Remove("RECP22")
            dt.Columns.Remove("RECP23")
            dt.Columns.Remove("IsConsolidated")
            _PreviewTable = dt
            dt.Dispose()

            ValidationErrors.Clear()
            GenerateHeader()
            GenerateTrailer()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            _totalRecordCount = _PreviewTable.Rows.Count
            _consolidatedRecordCount = _PreviewTable.Rows.Count



        Catch mex As MagicException
            Throw New MagicException(mex.Message.ToString)
        Catch ex As Exception
            Throw New Exception("Error on Consolidating records : " & ex.Message.ToString)
        Finally
            If _PreviewTable.Columns.Contains("IsConsolidated") Then _PreviewTable.Columns.Remove("IsConsolidated")
        End Try
    End Sub
#End Region


    ''' <summary>
    ''' This Structure holds the Unique VenCode, VenName1, VenName2
    ''' to group the records
    ''' </summary>
    ''' <remarks></remarks>
    Private Structure Vendor
        Public venCode As String
        Public venName1 As String
        Public venName2 As String
    End Structure

    Private Function GetVendorList() As ArrayList

        Dim vendorList As New ArrayList
        Dim _vendor As Vendor
        Dim venName(1) As String

        For Each _row As DataRow In _PreviewTable.Rows

            _vendor.venCode = String.Empty
            _vendor.venName1 = String.Empty
            _vendor.venName2 = String.Empty
            _vendor.venCode = _row("VenCode").ToString

            ' Generate VenName2 from VenName1
            ' If length of VenName1 > 35, then the remaining portion of the vendor Name
            ' starting with the word that exceeds the length should be displayed in VenName2
            If _row("VenName1").ToString.Length > TblBankFields("VenName1").DataLength.Value Then
                venName = SplitVenName(_row("VenName1").ToString, TblBankFields("VenName1").DataLength.Value)
                _vendor.venName1 = venName(0)
                _vendor.venName2 = venName(1)
                _row("VenName1") = venName(0)
                _row("VenName2") = venName(1)
            Else

                _vendor.venName1 = _row("VenName1").ToString

                If _row("VenName2").ToString() <> String.Empty Then _vendor.venName2 = _row("VenName2").ToString

            End If

            If vendorList.IndexOf(_vendor) = -1 Then vendorList.Add(_vendor)

        Next

        Return vendorList

    End Function

    Private Function SplitVenName(ByVal VenName As String, ByVal StringLength As Integer) As Array
        Dim strSplit() As String
        Dim VenNames(1) As String
        Dim venName2StartPosition As Integer

        VenNames(0) = String.Empty
        VenNames(1) = String.Empty

        strSplit = VenName.Split(" ")
        For i As Integer = 0 To strSplit.Length - 1
            If strSplit(i).Length <= StringLength Then
                If VenNames(0) = String.Empty Then
                    VenNames(0) = strSplit(i)
                Else
                    If String.Concat(VenNames(0), " ", strSplit(i)).Length <= StringLength Then
                        VenNames(0) = String.Concat(VenNames(0), " ", strSplit(i))
                    Else
                        venName2StartPosition = i
                        Exit For
                    End If
                End If
            End If
        Next

        For i As Integer = venName2StartPosition To strSplit.Length - 1
            If VenNames(1) = String.Empty Then
                VenNames(1) = strSplit(i)
            Else
                VenNames(1) = String.Concat(VenNames(1), " ", strSplit(i))
            End If
        Next
        Return VenNames
    End Function

End Class