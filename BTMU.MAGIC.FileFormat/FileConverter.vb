Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate

Public Class FileConverter

#Region "Private Property"

    Private _sourcedata As DataView
    Private BankFields As New MapBankFieldCollection

    Private _totalrecords As Int32
    Private _totalerrors As Int32
    Private _consolidaterecords As Int32

    Private _header As String
    Private _trailer As String
    Private _validationerrors As String
    Private _converteddata As New DataTable("ConvertedData")

#End Region

#Region "Public Property"

    Public Property SourceData() As DataView
        Get
            Return _sourcedata
        End Get
        Set(ByVal value As DataView)
            _sourcedata = value
        End Set
    End Property

    Public Property ConvertedData() As DataTable
        Get
            Return _converteddata
        End Get
        Set(ByVal value As DataTable)
            _converteddata = value
        End Set
    End Property

    Public ReadOnly Property Header() As String
        Get
            Return _header
        End Get

    End Property

    Public ReadOnly Property Trailer() As String
        Get
            Return _trailer
        End Get
    End Property

    Public ReadOnly Property ValidationErrors() As String
        Get
            Return _validationerrors
        End Get
    End Property

    Public ReadOnly Property TotalErrors() As Int32
        Get
            Return _totalerrors
        End Get
    End Property

    Public ReadOnly Property TotalRecords() As Int32
        Get
            Return _totalrecords
        End Get
    End Property

    Public ReadOnly Property ConsolidatedRecords() As Int32
        Get
            Return _consolidaterecords
        End Get
    End Property


#End Region


    Sub New()

    End Sub

    'Convert Source Data
    Public Sub Convert(ByVal SourceData As DataView, ByVal DataModel As CommonTemplateTextDetail, ByVal objMasterTemplate As MasterTemplateList)

        'Duplicate the BankFields in DataModel, So as to Impact Not DataModel by any chance

        For Each BankField As MapBankField In DataModel.MapBankFields
            Dim dupBankField As New MapBankField()
            BankField.CopyTo(dupBankField)
            BankFields.Add(dupBankField)
        Next

        Dim targetDate As Date
        Dim targetNumber As Decimal
        Dim ignoreConvertedData As Boolean

        For Each sourcerecord As DataRowView In SourceData

            ignoreConvertedData = False

            '#1. Copy Mapped Source Values to BankFields and  Validate Date, Numeric and Currency values
            For Each BankField As MapBankField In BankFields

                For Each SourceField As MapSourceField In BankField.MappedSourceFields
                    'Copy Value
                    SourceField.SourceFieldValue = System.Convert.ToString(sourcerecord(SourceField.SourceFieldName))

                    'Validate and Format Source Value
                    Select Case BankField.DataType
                        Case "DateTime"
                            If Common.HelperModule.IsDateDataType(SourceField.SourceFieldValue, DataModel.DateSeparator, DataModel.DateType, targetDate) Then
                                SourceField.SourceFieldValue = targetDate.ToString(BankField.DateFormat.Replace("D", "d").Replace("Y", "y"))
                            Else
                                Throw New Exception(String.Format("'{0}' has invalid Date '{1}'", SourceField.SourceFieldName, SourceField.SourceFieldValue))
                            End If
                        Case "Number", "Currency"
                            If Common.HelperModule.IsDecimalDataType(SourceField.SourceFieldValue, DataModel.DecimalSeparator, DataModel.ThousandSeparator, targetNumber) Then
                                SourceField.SourceFieldValue = targetNumber.ToString()
                            Else
                                Throw New Exception(String.Format("'{0}' has invalid Numeric Value '{1}'", SourceField.SourceFieldName, SourceField.SourceFieldValue))
                            End If
                    End Select
                Next
            Next




            '#2. Apply Translation
            Try

                For Each translatedBankField As TranslatorSetting In DataModel.TranslatorSettings

                    For Each BankField As MapBankField In BankFields
                        'Has it been translated?
                        If BankField.BankField = translatedBankField.BankFieldName Then
                            If translatedBankField.CustomerValue <> String.Empty Then
                                If BankField.MappedValues = translatedBankField.CustomerValue Then
                                    'No Clear Input from Customer on how they've been practising the Translation
                                    ' of BankFields that have more than one Customer Fields Mapped! 
                                    'Just Assign to the first sourcefield and remove the remaining mapped sourcefields

                                    BankField.MappedSourceFields.Clear()
                                    Dim nsrcfield As New MapSourceField()
                                    nsrcfield.SourceFieldName = String.Empty
                                    nsrcfield.SourceFieldValue = translatedBankField.BankValue
                                    BankField.MappedSourceFields.Add(nsrcfield)

                                End If
                            Else
                                'Just Assign to the first sourcefield and remove the remaining mapped sourcefields

                                BankField.MappedSourceFields.Clear()
                                Dim nsrcfield As New MapSourceField()
                                nsrcfield.SourceFieldName = String.Empty
                                nsrcfield.SourceFieldValue = translatedBankField.BankValue
                                BankField.MappedSourceFields.Add(nsrcfield)

                            End If
                        End If
                    Next

                Next

            Catch ex As Exception
                Throw New Exception(String.Format("Error occurred while Translating Bank Fields!, Error Details : {0}", ex.Message))
            End Try



            'Note: No more Source fieldnames in the MappedSourceFields Collection of TRANSLATED BankFields !!!! 

            '#3. Apply Lookup

            Try
                '''' Not Implemented Yet ''''
            Catch ex As Exception
                Throw New Exception(String.Format("Error occurred while processing Lookup Settings!, Error Details : {0}", ex.Message))
            End Try






            '#4. Apply Calculation 
            Try

                For Each calculatedBankField As CalculatedField In DataModel.CalculatedFields

                    For Each BankField As MapBankField In BankFields
                        If calculatedBankField.BankFieldName = BankField.BankField Then

                            BankField.MappedSourceFields.Clear()
                            Dim nsrcfield As New MapSourceField()
                            nsrcfield.SourceFieldName = String.Empty

                            If calculatedBankField.Operator1 = "+" Then
                                nsrcfield.SourceFieldValue = System.Convert.ToString(System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand1)) _
                                                        + System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand2)))
                            Else
                                nsrcfield.SourceFieldValue = System.Convert.ToString(System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand1)) _
                                                        - System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand2)))
                            End If

                            If calculatedBankField.Operator2 = "+" Then
                                nsrcfield.SourceFieldValue = System.Convert.ToString(System.Convert.ToDecimal(nsrcfield.SourceFieldValue) _
                                                        + System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand3)))
                            ElseIf calculatedBankField.Operator2 = "-" Then
                                nsrcfield.SourceFieldValue = System.Convert.ToString(System.Convert.ToDecimal(nsrcfield.SourceFieldValue) _
                                                        - System.Convert.ToDecimal(sourcerecord(calculatedBankField.Operand3)))
                            End If

                            BankField.MappedSourceFields.Add(nsrcfield)

                        End If
                    Next

                Next

            Catch ex As Exception
                Throw New Exception(String.Format("Error occurred while processing Calculation Settings!, Error Details : {0}", ex.Message))
            End Try




            '#5. Apply String Manipulation
            Try

                For Each manipulatedBankField As StringManipulation In DataModel.StringManipulations

                    For Each BankField As MapBankField In BankFields
                        If manipulatedBankField.BankFieldName = BankField.BankField Then

                            Dim nsrcfield As New MapSourceField()
                            nsrcfield.SourceFieldName = String.Empty
                            nsrcfield.SourceFieldValue = BankField.MappedValues
                            BankField.MappedSourceFields.Clear()

                            If nsrcfield.SourceFieldValue <> String.Empty Then
                                Select Case manipulatedBankField.FunctionName

                                    Case "LEFT"
                                        nsrcfield.SourceFieldValue = nsrcfield.SourceFieldValue.Substring(0, manipulatedBankField.NumberOfCharacters.Value)
                                    Case "RIGHT"
                                        nsrcfield.SourceFieldValue = nsrcfield.SourceFieldValue.Substring(nsrcfield.SourceFieldValue.Length - (manipulatedBankField.NumberOfCharacters.Value - 1), manipulatedBankField.NumberOfCharacters.Value)
                                    Case "SUBSTRING"
                                        nsrcfield.SourceFieldValue = nsrcfield.SourceFieldValue.Substring(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)
                                    Case "REMOVE"
                                        nsrcfield.SourceFieldValue = nsrcfield.SourceFieldValue.Remove(manipulatedBankField.StartingPosition.Value - 1, manipulatedBankField.NumberOfCharacters.Value)

                                End Select

                            End If
                            BankField.MappedSourceFields.Add(nsrcfield)

                        End If
                    Next

                Next

            Catch ex As Exception
                Throw New Exception(String.Format("Error occurred while processing String Manipulation Settings!, Error Details : {0}", ex.Message))
            End Try


            '#6.
            'If Master Template is a Non-Fixed Width Master Template, 'Adjust the Width of 
            ' Numeric and Currency Values so as to fit the Decimal Length in FixedWidthMaster Template

            If Not objMasterTemplate.IsFixed Then

                For Each BankField As MapBankField In BankFields
                    Select Case BankField.DataType
                        Case "Number"
                            If BankField.MappedValues.Length < BankField.DataLength.Value Then

                                Select Case objMasterTemplate.OutputFormat
                                    Case "SOAPL"
                                        BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, " ")

                                    Case "VPS"
                                        If BankField.BankField.ToUpper() <> "InvAmt".ToUpper() Then
                                            BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, "0")
                                        End If

                                    Case "EPS"
                                        If BankField.BankField.ToUpper() <> "Amount".ToUpper() Then
                                            BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, " ")
                                        ElseIf BankField.BankField.ToUpper() = "APCA ID".ToUpper() AndAlso BankField.MappedSourceFields(0).SourceFieldValue.Length > 0 Then
                                            BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, "0")
                                        Else
                                            BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, "0")
                                        End If

                                    Case Else
                                        BankField.MappedSourceFields(0).SourceFieldValue = BankField.MappedSourceFields(0).SourceFieldValue.PadLeft(BankField.DataLength.Value, "0")

                                End Select

                            End If
                    End Select

                    Dim sNumber As String
                    Dim sDecimal As String
                    Dim iDecimalLength As Integer = 0

                    '#7.
                    'Convert to Currency Decimal Separator Format Setting
                    If BankField.DataType = "Currency" Then
                        If BankField.MappedValues.IndexOf(".") >= 0 Then

                            sNumber = BankField.MappedValues.Substring(0, BankField.MappedValues.IndexOf("."))

                            sDecimal = BankField.MappedValues.Substring(BankField.MappedValues.IndexOf(".") + 1)
                        Else
                            sNumber = BankField.MappedValues
                            sDecimal = ""
                        End If

                        'Get Decimal Length from Default Value Field Currency
                        Try
                            For Each defvalue As String In BankField.DefaultValue.Split(",")
                                If defvalue.Split("-").Length > 1 Then
                                    iDecimalLength = defvalue.Split("-")(1).Length
                                    Exit For
                                End If
                            Next
                        Catch ex As Exception
                        Finally
                            If iDecimalLength = 0 Then iDecimalLength = 2
                        End Try

                        If Len(sDecimal) < iDecimalLength Then
                            sDecimal.PadRight(iDecimalLength, "0")
                        ElseIf Len(sDecimal) > iDecimalLength Then
                            If MsgBox("Error, Decimal length is greater than decimal data length in master template." _
                                & Chr(13) & "Do you want to cut or repair customer data ?" _
                                & Chr(13) & Chr(13) & "Click 'Yes', if you want to cut customer data." _
                                & Chr(13) & "Click 'No', if you want to repair customer data.", _
                                vbQuestion & vbYesNo, "Generate Common Transaction Template") = vbYes Then
                                sDecimal = Left(sDecimal, iDecimalLength)
                            Else
                                Exit Sub
                            End If
                        End If

                        If BankField.IncludeDecimal = "Yes" Then

                            sNumber = sNumber & sDecimal
                            If BankField.DataLength.HasValue Then
                                sNumber = sNumber.PadLeft(BankField.DataLength.Value, "0")
                            End If
                            BankField.MappedSourceFields(0).SourceFieldValue = sNumber

                        ElseIf BankField.IncludeDecimal = "No" Then

                            If BankField.DataLength.HasValue Then
                                sNumber = sNumber.PadLeft(BankField.DataLength.Value, "0")
                            End If
                            BankField.MappedSourceFields(0).SourceFieldValue = sNumber & BankField.DecimalSeparator & sDecimal

                        End If

                    End If

                Next

            End If

            '#8.
            'Start- getting Master Values''''''''''''''''''''''''''''''''''''''''''
            Dim ListIndicator() As String, ListTransactionCode() As String
            Dim ListBankCharge() As String, ListServiceType() As String, ListWHTDMethod() As String
            Dim ListTaxForm() As String, ListPayerofIncome() As String, ListTypeofIncome() As String
            Dim bRecordW1Exist As Boolean, bRecordW2Exist As Boolean, bRecordW3Exist As Boolean, bRecordIExist As Boolean
            Dim RECORD_TYPE_P As String, FILE_FORMAT_TYPE As String, RECORD_TYPE_I As String, RECORD_TYPE_W As String, BRANCH_CODE As String, BANK_CODE As String


            'Added for EPS format only
            'Indicator
            For Each BankField As MapBankField In BankFields
                If BankField.BankField = "Indicator" Then
                    ListIndicator = BankField.DefaultValue.Split(",")
                ElseIf BankField.BankField = "Transaction Code" Then
                    ListTransactionCode = BankField.DefaultValue.Split(",")
                End If
            Next

            'Added for iFTS2 format only
            If objMasterTemplate.OutputFormat = "iFTS-2" Then
                For Each BankField As MapBankField In BankFields
                    Select Case BankField.BankField
                        Case "W1 Record Type-W"
                            bRecordW1Exist = IIf(BankField.MappedValues = String.Empty, False, True)
                        Case "W2 Record Type-W"
                            bRecordW2Exist = IIf(BankField.MappedValues = String.Empty, False, True)
                        Case "W3 Record Type-W"
                            bRecordW3Exist = IIf(BankField.MappedValues = String.Empty, False, True)
                        Case "Record Type-I"
                            bRecordIExist = IIf(BankField.MappedValues = String.Empty, False, True)
                    End Select
                Next
            End If

            For Each BankField As MapBankField In BankFields
                Select Case BankField.BankField
                    Case "File Format Type"  'File format Type
                        FILE_FORMAT_TYPE = BankField.DefaultValue
                    Case "Record Type-P" 'Record type P
                        RECORD_TYPE_P = BankField.DefaultValue
                    Case "Bank Code"  'Bank Code 'permitted values : 4, 04, 004
                        BANK_CODE = BankField.DefaultValue
                    Case "Branch Code"   'Branch Code 'permitted values : 1, 01, 001, 0001
                        BRANCH_CODE = BankField.DefaultValue
                    Case "W1 Record Type-W" 'Record type W
                        RECORD_TYPE_W = BankField.DefaultValue
                    Case "Record Type-I"  'Record type I
                        RECORD_TYPE_I = BankField.DefaultValue
                    Case "Bank Charge" 'Bank Charge  
                        ListBankCharge = BankField.DefaultValue.Split(",")
                    Case "Service Type" ' Service Type   
                        ListServiceType = BankField.DefaultValue.Split(",")
                    Case "WHT Delivery Method" 'WHT Delivery Method
                        ListWHTDMethod = BankField.DefaultValue.Split(",")
                    Case "W1 Tax Form" ' W1 Tax Form   
                        ListTaxForm = BankField.DefaultValue.Split(",")
                    Case "W1 The Payer of Income" ' W1 The Payer of Income 
                        ListPayerofIncome = BankField.DefaultValue.Split(",")
                    Case "W1 Type of Income (1)" ' W1 Type of Income (1)
                        ListTypeofIncome = BankField.DefaultValue.Split(",")
                End Select
            Next
            'End - getting master values''''''''''''''''''''''''''''''''''''''''''

            '#9.
            Dim bFieldSectorSelectionExist As Boolean = False
            Dim bFieldTemp1Exist As Boolean = False
            Dim bFieldTemp2Exist As Boolean = False
            Dim bFieldTemp3Exist As Boolean = False
            Dim bFieldCountryExist As Boolean = False
            Dim bFieldLocalCurrencyExist As Boolean = False
            Dim ListTemp1() As String, ListTemp2() As String, ListTemp3() As String
            Dim ListCountry() As String, ListLocalCurrency() As String

            For Each BankField As MapBankField In BankFields
                Select Case BankField.BankField
                    Case "Sector Selection"
                        bFieldSectorSelectionExist = True
                    Case "Temp1"
                        bFieldTemp1Exist = True
                    Case "Temp2"
                        bFieldTemp2Exist = True
                    Case "Temp3"
                        bFieldTemp3Exist = True
                    Case "Local Currency"
                        bFieldLocalCurrencyExist = True
                    Case "Local Country"
                        bFieldCountryExist = True
                End Select
            Next

            If ((bFieldTemp1Exist Or bFieldTemp2Exist) And bFieldSectorSelectionExist) Then

                Dim sTemp1 As String, sTemp2 As String, sTemp3 As String, sCountry As String, sLocalCurrency As String

                If bFieldTemp1Exist Then 'Get "Temp1" Data Sample
                    ListTemp1 = BankFields.Item("Temp1").BankSampleValue.Split("|")
                Else
                    sTemp1 = ""
                End If

                If bFieldTemp2Exist Then 'Get "Temp2" Data Sample
                    ListTemp2 = BankFields.Item("Temp2").BankSampleValue.Split("|")
                Else
                    sTemp2 = ""
                End If

                If bFieldTemp3Exist Then 'Get "Temp3" Data Sample
                    ListTemp3 = BankFields.Item("Temp3").BankSampleValue.Split("|")
                Else
                    sTemp3 = ""
                End If

                If bFieldCountryExist Then 'Get "Country" Data Sample
                    ListCountry = BankFields.Item("Local Country").BankSampleValue.Split("|")
                Else
                    sCountry = ""
                End If

                If bFieldLocalCurrencyExist Then  'Get "Currency" Data Sample
                    ListLocalCurrency = BankFields.Item("Local Currency").BankSampleValue.Split("|")
                Else
                    sLocalCurrency = ""
                End If

                Dim currencyRead As String, _tempValue As String
                Dim s As Boolean, C As Boolean, tem1 As Boolean, tem2 As Boolean

                s = False : C = False : tem1 = False : tem2 = False
                'Fill Field "Sector Selection"

                _tempValue = BankFields.Item("Temp1").MappedValues.ToUpper()
                For Each tempvalue As String In ListTemp1
                    If _tempValue = tempvalue Then
                        tem1 = True
                        Exit For
                    End If
                Next

                _tempValue = BankFields.Item("Temp2").MappedValues.ToUpper()
                For Each tempvalue As String In ListTemp2
                    If _tempValue = tempvalue Then
                        tem2 = True
                        Exit For
                    End If
                Next

                Select Case ListCountry(0).ToUpper()

                    Case "INDONESIA"

                        If tem1 Or tem2 Then
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = "Book Transfer"
                        Else
                            _tempValue = BankFields.Item("Currency").MappedValues.ToUpper()
                            For Each currencyRead In ListLocalCurrency
                                If _tempValue = currencyRead.ToUpper() Then
                                    s = True
                                    Exit For
                                End If
                            Next
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = IIf(s = True, "Domestic", "International")
                        End If

                    Case "SINGAPORE", "MALAYSIA", "THAILAND", "AUSTRALIA"

                        If tem1 Or tem2 Then
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = "Book Transfer"
                        Else

                            _tempValue = BankFields.Item("Currency").MappedValues.ToUpper()
                            For Each currencyRead In ListLocalCurrency
                                If _tempValue = currencyRead.ToUpper() Then
                                    s = True
                                    Exit For
                                End If
                            Next

                            _tempValue = BankFields.Item("Local Country").MappedValues.ToUpper()
                            For Each currencyRead In ListCountry
                                If _tempValue = currencyRead.ToUpper() Then
                                    C = True
                                    Exit For
                                End If
                            Next

                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = IIf(s And C, "Domestic", "International")
                        End If

                    Case "VIETNAM"

                        If tem1 Or tem2 Then
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = "Book Transfer"
                        Else

                            _tempValue = BankFields.Item("Local Country").MappedValues.ToUpper()
                            For Each currencyRead In ListCountry
                                If _tempValue = currencyRead.ToUpper() Then
                                    s = True
                                    Exit For
                                End If
                            Next

                            _tempValue = BankFields.Item("Temp3").MappedValues.ToUpper()
                            For Each currencyRead In ListTemp3
                                If _tempValue = currencyRead.ToUpper() Then
                                    s = True
                                    Exit For
                                End If
                            Next
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = IIf(s And C, "Domestic", "International")
                        End If

                    Case "PHILIPPINES"

                        If tem1 Or tem2 Then
                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = "Book Transfer"
                        Else

                            _tempValue = BankFields.Item("Currency").MappedValues.ToUpper()
                            For Each currencyRead In ListLocalCurrency
                                If _tempValue = currencyRead.ToUpper() Then
                                    s = True
                                    Exit For
                                End If
                            Next

                            _tempValue = BankFields.Item("Local Country").MappedValues.ToUpper()
                            For Each currencyRead In ListCountry
                                If _tempValue = currencyRead.ToUpper() Then
                                    C = True
                                    Exit For
                                End If
                            Next

                            BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = IIf(s And C, "Domestic", "International")
                        End If
                End Select
            End If

            'Fixed width Master Template
            If objMasterTemplate.IsFixed Then

                Dim bFieldSequenceNoExist As Boolean = False
                Dim bFieldFTBeneficiaryAccountNoExist As Boolean = False
                Dim bFieldLLGBeneficiaryAccountNoExist As Boolean = False
                bFieldSectorSelectionExist = False

                For Each BankField As MapBankField In BankFields
                    Select Case BankField.BankField
                        Case "Sequence No"
                            bFieldSequenceNoExist = True
                        Case "Sector Selection"
                            bFieldSectorSelectionExist = True
                        Case "FT Beneficiary Account No"
                            bFieldFTBeneficiaryAccountNoExist = True
                        Case "LLG Beneficiary Account No"
                            bFieldLLGBeneficiaryAccountNoExist = True
                    End Select
                Next

                If bFieldSectorSelectionExist Then
                    If BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue = "International" Then
                        MsgBox("There is [Sector Selection] = 'International'. " & _
                                "So the row which contains [Sector Selection] = 'International' will not be previewed or converted.", vbExclamation, "Generate Common Transaction Template")
                    End If
                End If

                Dim iNoDomestic As Integer, iNoBookTransfer As Integer
                Dim sSequenceNoDomestic As String, sSequenceNoBookTransfer As String
                iNoDomestic = 1 : sSequenceNoDomestic = "1" : iNoBookTransfer = 1 : sSequenceNoBookTransfer = "1"

                If bFieldSectorSelectionExist = True Then
                    'For "Sector Selection" = "Domestic" make up "Sequence No" = "A01XXXXXX"
                    'For "Sector Selection" = "Book Transfer" make up "Sequence No" = "A02XXXXXX"
                    Select Case BankFields.Item("Sector Selection").MappedSourceFields(0).SourceFieldValue

                        Case "Domestic"
                            If bFieldFTBeneficiaryAccountNoExist Then
                                BankFields.Item("FT Beneficiary Account No").MappedSourceFields(0).SourceFieldValue = ""
                            End If

                            If bFieldSequenceNoExist Then
                                sSequenceNoDomestic = sSequenceNoDomestic.PadLeft(6, "0")
                                BankFields.Item("Sequence No").MappedSourceFields(0).SourceFieldValue = "A01" & sSequenceNoDomestic
                                iNoDomestic = iNoDomestic + 1
                                sSequenceNoDomestic = CStr(iNoDomestic)
                            End If

                        Case "Book Transfer"
                            If bFieldLLGBeneficiaryAccountNoExist = True Then
                                BankFields.Item("LLG Beneficiary Account No").MappedSourceFields(0).SourceFieldValue = ""
                            End If

                            If bFieldSequenceNoExist = True Then
                                'Fill Field "Sequence No"
                                sSequenceNoBookTransfer = sSequenceNoBookTransfer.PadLeft(6, "0")

                                BankFields.Item("Sequence No").MappedSourceFields(0).SourceFieldValue = "A02" & sSequenceNoBookTransfer
                                iNoBookTransfer = iNoBookTransfer + 1
                                sSequenceNoBookTransfer = CStr(iNoBookTransfer)
                            End If
                        Case Else
                            ignoreConvertedData = True ' ignore this row of converted data
                    End Select
                End If
            End If


            ''''''''''''''''Need to fix the commented functions ''''''''''''''''''

            Select Case objMasterTemplate.OutputFormat
                Case "GCMS"
                    ' Call GCMSGeneration("TextG")
                Case "BULKVN"
                    ' Call BulkVNGeneration("TextG")
                Case "FTS"
                    ' Call FTSGeneration("TextG")
                Case "SOAPPL"
                    ' Call SOAPPLGeneration("TextG")
                Case "EPS"
                    'Call EPSGeneration("TextG")
                Case "iFTS-2" 'iFTS2 - Thailand
                    ' Call iFTS2Generation("TextG")
                Case "iFTS"
                    'Call iFTSGeneration("TextG")
                Case "VPS"
                    'Call VPSGeneration("TextG")
                Case "CWS"
                    'Call CWSGeneration("TextG")
                Case "iRTMSCI"
                    'Call ChequeGeneration("TextG")
                Case "iRTMSGC", "iRTMSGD"
                    'Call GiroGeneration("TextG")
                Case "OiRTMSRM"
                    'Call OptionalRemittanceGeneration("TextG")
                Case "OiRTMSCI"
                    'Call OptionalChequeGeneration("TextG")
                Case "OiRTMSGC", "OiRTMSGD"
                    'Call OptionalGiroGeneration("TextG")
                Case "iRTMSRM"
                    'For Each BankField As MapBankField In BankFields
                    '    Select Case BankField.BankField
                    '        Case "Beneficiary Bank Name"
                    '            ListTemp1 = BankField.BankSampleValue.Split("|")
                    '        Case "Beneficiary Bank Country"
                    '            ListTemp2 = BankField.BankSampleValue.Split("|")
                    '        Case "Beneficiary Bank Branch Name"
                    '            ListTemp3 = BankField.BankSampleValue.Split("|")
                    '        Case "Beneficiary Country"
                    '            ListCountry = BankField.BankSampleValue.Split("|")
                    '        Case "Currency"
                    '            ListLocalCurrency = BankField.BankSampleValue.Split("|")
                    '    End Select
                    'Next
                    'Call RemittanceGeneration("TextG")
            End Select


            ''''''''''''''''TO BE CONTINUED............................................

            If ignoreConvertedData Then
                'Do not add the converted data to converted data table
            Else
                'Add the Converted data to Converted Data Table
            End If

        Next 'Next data row from Source File for Conversion




        '''''''''''' Not implemented Yet '''''
        _header = "No header"
        _trailer = "No trailer"
        _validationerrors = "No Error"
        ''''''''''''''''''''''''''''''''''''''

        'Convert Source Data  
        Dim gcmsfile As New GCMSMoneyTransferFileFormat()

        Dim _errors As FileFormatErrorCollection = gcmsfile.ValidateData()

        _converteddata = gcmsfile.ConvertedData

        For Each _error As FileFormatError In _errors

            _validationerrors = _validationerrors _
                        & String.Format("Record No: {0} Column No: {1} Field: '{2}' {3}" _
                                , _error.RecordNo, _error.ColumnOrdinalNo _
                                , _error.ColumnName, _error.Description) _
                        & vbCrLf

        Next

    End Sub

    'Consolidate Converted Data
    Public Sub Consolidate()

    End Sub



End Class