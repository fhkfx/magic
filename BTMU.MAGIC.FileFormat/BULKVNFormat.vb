Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.CommonTemplate
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the BULKVN Format
''' </summary>
''' <remarks></remarks>
Public Class BULKVNFormat
    Inherits BaseFileFormat

    'Amount field is summed up while grouping records
    Private Shared _amountFieldName As String = "Amount"

    Private Shared _editableFields() As String = {"BulkNo", "No", "Value Date", "Bene", "BeneBank" _
                                        , "BeneCity_Province", "BeneAC", "CCY", "Amount", "Remark"}

    Private Shared _autoTrimmableFields() As String = {"Bene", "BeneBank", "BeneCity_Province", "CCY"}

    'In this format, group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Customer Name", "Settlement Account", "BulkNo", "No", "Value Date", "Bene", "BeneBank" _
                                        , "BeneCity_Province", "BeneAC", "CCY", "Amount", "Remark"}

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "BULKVN"
        End Get
    End Property

#End Region

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Public Shared Function OutputFormatString() As String
        Return "BULKVN"
    End Function

#Region "Overriden Methods"

    Protected Overrides Sub GenerateHeader()

        _Header = String.Empty

        _Header = String.Format("Customer Name:,""{0}"", ,Settlement Account:,""{1}""", _
                    _PreviewTable.Rows(0)("Customer Name").ToString, _PreviewTable.Rows(0)("Settlement Account").ToString)
        _Header += vbCrLf & "BulkNo,No,Value Date,Bene,BeneBank,BeneCity_Province,BeneAC,CCY,Amount,Remark"

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim _rowIndex As Integer = 1

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            _previewRecord("No") = _rowIndex 'No - Sequence Number starts from 1

            If Decimal.TryParse(_previewRecord("Amount").ToString(), Nothing) Then
                _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString).ToString("##,##0.00")
            End If

            _rowIndex += 1

        Next

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Usually this routines is for Summing up the amount value before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                _rowIndex += 1

                'Value Date - dd/MM/yyyy
                If _previewRecord("Value Date") <> "" AndAlso _previewRecord("Value Date").ToString.Length > 5 Then
                    _previewRecord("Value Date") = String.Format("{0}/{1}/{2}", _
                                                  _previewRecord("Value Date").ToString.Substring(0, 2) _
                                                , _previewRecord("Value Date").ToString.Substring(2, 2) _
                                                , _previewRecord("Value Date").ToString.Substring(4))
                End If

                'Amount - ##,##0.00
                If _previewRecord("Amount") <> "" Then

                    If Decimal.TryParse(_previewRecord("Amount").ToString(), Nothing) Then
                        If _DataModel.IsAmountInCents Then _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If

                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex, " Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex, " Amount"))
        End Try

    End Sub

    'Performs Data Type and Custom Validations (other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            '#1. Value Date 
            Dim _ValueDate As Date
            If _previewRecord("Value Date") <> "" Then
                If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), "/", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then

                    If _ValueDate < Date.Today Then
                        AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09030070"), "Record", RowNumber + 1, "Value Date"), RowNumber + 1, True)
                    End If

                    '#2. Value date shall not be Saturday / Sunday
                    If _ValueDate.DayOfWeek = DayOfWeek.Saturday OrElse _ValueDate.DayOfWeek = DayOfWeek.Sunday Then
                        AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09020030"), "Record", RowNumber + 1, "Value Date"), RowNumber + 1, True)
                    End If
                Else
                    AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Value Date"), RowNumber + 1, True)
                End If
            End If
            '#3. Amount
            If _previewRecord("Amount") <> "" Then
                If Decimal.TryParse(_previewRecord("Amount"), dAmount) Then
                    If Not IsConsolidated AndAlso dAmount <= 0 Then
                        'AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09050080"), "Record", RowNumber + 1, "Amount"), RowNumber + 1)
                    End If
                Else
                    AddValidationError("Amount", _PreviewTable.Columns("Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Amount"), RowNumber + 1, True)
                End If
            End If


            '4. Validate the Character values
            ' The allowed characters for any non-numeric fields are a-z, A-Z, 0-9,
            ' forward slash(/), hyphen(-), question mark (?), opening bracket( ( ), closing bracket ( ) ), comma(,) 
            ' , period(.), single quote('), plus sign(+), colon(:) and space( ).
            ' Validation - Do not allow unicode characters
            Dim pattern As String = "[^a-zA-Z0-9,':/\-" & Regex.Escape("?().+") & "\s]"
            Dim regAllow As New Regex(pattern, RegexOptions.Compiled)

            For Each charField As String In New String() {"Customer Name", "Settlement Account", "BulkNo", "No", "Bene", "BeneBank", "BeneCity_Province", "BeneAC", "CCY", "Remark"}

                If _previewRecord(charField).ToString().Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(charField).ToString().Trim()) Then

                    Dim validationError As New FileFormatError()
                    validationError.ColumnName = charField
                    validationError.ColumnOrdinalNo = _PreviewTable.Columns(charField).Ordinal + 1

                    If TblBankFields(charField).Header AndAlso RowNumber = 0 Then
                        validationError.Description = String.Format(MsgReader.GetString("E09020100"), "Header", "", charField)
                        validationError.RecordNo = 0
                    Else
                        validationError.Description = String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, charField)
                        validationError.RecordNo = RowNumber + 1
                    End If

                    _ValidationErrors.Add(validationError)
                    _ErrorAtMandatoryField = True

                End If

            Next

            RowNumber = RowNumber + 1

        Next

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Amount"))
            If Not Decimal.TryParse(_row("Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Amount")

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            GenerateHeader()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()

        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try


    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sDeli As String = ","
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1

        Try
            ' BULKVN uses Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New MagicException("Invalid master Template")
            End If


            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                sEnclosureCharacter = ""
            Else
                sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            If _Header <> "" Then sText = sText & _Header & vbNewLine

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                For Each _column As String In TblBankFields.Keys

                    'Ignore Header Columns
                    If TblBankFields(_column).Header Then Continue For

                    If _column.Equals("Amount", StringComparison.InvariantCultureIgnoreCase) Then
                        If _previewRecord(_column).ToString.StartsWith("-") Then
                            _previewRecord(_column) = "-" & Math.Abs(Convert.ToDecimal(_previewRecord(_column).ToString)).ToString("#,##0.00")
                        Else
                            _previewRecord(_column) = Math.Abs(Convert.ToDecimal(_previewRecord(_column).ToString)).ToString("#,##0.00")
                        End If

                    End If

                    'No delimiter beyond last Column
                    If _column.Equals("Remark", StringComparison.InvariantCultureIgnoreCase) Then sDeli = "" Else sDeli = ","

                    If sEnclosureCharacter = "" Then

                        Select Case _column.ToUpper()
                            Case "BULKNO", "NO"
                                sLine = sLine & _previewRecord(_column).ToString & sDeli
                            Case Else 'These fields have to be enclosed with enclosure character
                                ' Case "VALUE DATE", "BENE", "BENEBANK", "BENECITY_PROVINCE", "BENEAC", "CCY", "AMOUNT", "REMARK"
                                If _previewRecord(_column).ToString.StartsWith("""") AndAlso _previewRecord(_column).ToString.EndsWith("""") Then
                                    sLine = sLine & _previewRecord(_column) & sDeli
                                Else
                                    sLine = sLine & """" & _previewRecord(_column).ToString & """" & sDeli
                                End If
                        End Select

                    Else 'All fields have to be enclosed with enclosure character
                        sLine = sLine & sEnclosureCharacter & _previewRecord(_column).ToString & sEnclosureCharacter & sDeli
                    End If

                Next

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Return SaveTextToFile(sText, _outputFile)


        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    'Can be implemented by FileFormat that validates "Amount" data after consolidation/edit/update 
    'Done on conversion
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If
                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If
            End If

            RowNumber += 1

        Next

    End Sub


#End Region

End Class
 