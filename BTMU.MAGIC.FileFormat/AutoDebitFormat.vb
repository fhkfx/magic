'Funds Transfer Instruction Import Format (BTMU-Hong Kong)

Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the RTMS-AutoDebit Format
''' </summary>
''' <remarks></remarks>
Public Class AutoDebitFormat
    Inherits HKRTMSFormat
    'Change request during Development time
    'to take out the field: Cheque Number from the output/spec

    Private Shared _editableFields() As String = {"Group ID", "Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Instruction ID", "Instruction Remarks", "Identifier", "Counter Party Name", "Bank Number" _
                    , "Branch Number", "Account Number", "Transaction Amount", "Our Reference", "Advice Template" _
                    , "Counter Party Address", "Transaction Remarks", "AutoCheque Advice Information", "Delivery Option"}

    Private Shared _autoTrimmableFields() As String = {"Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Instruction ID", "Instruction Remarks", "Identifier" _
                    , "Our Reference", "Advice Template", "Counter Party Address", "Transaction Remarks" _
                    , "AutoCheque Advice Information", "Delivery Option"}


    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Group ID", "Payment Date", "Currency", "Principal Account Number" _
                    , "Principal Reference", "Instruction ID", "Instruction Remarks", "Identifier", "Counter Party Name", "Bank Number" _
                    , "Branch Number", "Account Number", "Transaction Amount", "Our Reference", "Advice Template" _
                    , "Counter Party Address", "Transaction Remarks", "AutoCheque Advice Information", "Delivery Option", "Consolidate Field"}


    Public Shared Function OutputFormatString() As String
        Return "AutoDebit"
    End Function

#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "AutoDebit"
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    Protected Overrides Sub GenerateOutputFormat()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Convert to all Capital letters
                For Each col As String In TblBankFields.Keys
                    If _previewRecord(col).ToString.Trim().Length > 0 Then
                        _previewRecord(col) = _previewRecord(col).ToString().ToUpper

                    End If
                Next

                '#8. Identifier (set running number.)
                If _previewRecord("Identifier").ToString().Trim = String.Empty Then
                    seqNo += 1
                    _previewRecord("Identifier") = "A" + seqNo.ToString("000000")
                End If

                '#10. Bank Number (Pad leading zeroes)
                If _previewRecord("Bank Number").ToString.Trim <> String.Empty And _previewRecord("Bank Number").ToString.Length < 3 And _
                    IsNumeric(_previewRecord("Bank Number").ToString) Then
                    _previewRecord("Bank Number") = Val(_previewRecord("Bank Number").ToString()).ToString("000")
                End If

                '#11. Branch Number (Pad leading zeroes)
                If _previewRecord("Branch Number").ToString.Trim <> String.Empty And _previewRecord("Branch Number").ToString.Length < 3 And _
                    IsNumeric(_previewRecord("Branch Number").ToString) Then
                    _previewRecord("Branch Number") = Val(_previewRecord("Branch Number").ToString()).ToString("000")
                End If


                '#13. Transaction Amount formatting 
                If _previewRecord("Transaction Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()) / 100
                    End If

                    _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()).ToString("########0.00")

                End If

                '#16. Address (always set blank)
                If _previewRecord("Counter Party Address").ToString.Trim <> String.Empty Then
                    _previewRecord("Counter Party Address") = String.Empty
                End If

                '#17. Transaction Remarks (Auto Shifting) - check position 70th
                Dim tempstr As String = String.Empty
                Dim pos As Int16 = 0
                Dim strbld As New System.Text.StringBuilder

                If _previewRecord("Transaction Remarks").ToString <> String.Empty And _previewRecord("Transaction Remarks").ToString().Length > 70 Then
                    tempstr = _previewRecord("Transaction Remarks").ToString.Substring(0, 70)
                    If Not tempstr(69).Equals(" ") Then
                        pos = tempstr.LastIndexOf(" ")
                        If pos > 0 Then
                            'get first half
                            strbld = New System.Text.StringBuilder(_previewRecord("Transaction Remarks").ToString().Substring(0, pos))
                            'second half
                            tempstr = _previewRecord("Transaction Remarks").ToString().Substring(pos + 1)

                            'fill trailing spaces
                            For i As Integer = 1 To 70 - pos
                                strbld.Append(" ")
                            Next

                            strbld.Append(tempstr)
                            _previewRecord("Transaction Remarks") = strbld.ToString()
                        End If

                    End If
                End If

                '#18. AutoCheque Advice Information (always set blank)
                If _previewRecord("AutoCheque Advice Information").ToString.Trim <> String.Empty Then
                    _previewRecord("AutoCheque Advice Information") = String.Empty
                End If

                '#19. Delivery Option (always set blank)
                If _previewRecord("Delivery Option").ToString.Trim <> String.Empty Then
                    _previewRecord("Delivery Option") = String.Empty
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Convert to all Capital letters
                For Each col As String In TblBankFields.Keys
                    If _previewRecord(col).ToString.Trim().Length > 0 Then
                        _previewRecord(col) = _previewRecord(col).ToString().ToUpper

                    End If
                Next

                '#8. Identifier (set running number.)
                'If _previewRecord("Identifier").ToString().Trim = String.Empty Then
                '    seqNo += 1
                '    _previewRecord("Identifier") = "A" + seqNo.ToString("000000")
                'End If

                '#10. Bank Number (Pad leading zeroes)
                If _previewRecord("Bank Number").ToString.Trim <> String.Empty And _
                    IsNumeric(_previewRecord("Bank Number").ToString) Then
                    _previewRecord("Bank Number") = Val(_previewRecord("Bank Number").ToString()).ToString("000")
                End If

                '#11. Branch Number (Pad leading zeroes)
                If _previewRecord("Branch Number").ToString.Trim <> String.Empty And _
                    IsNumeric(_previewRecord("Branch Number").ToString) Then
                    _previewRecord("Branch Number") = Val(_previewRecord("Branch Number").ToString()).ToString("000")
                End If


                '#13. Transaction Amount
                If _previewRecord("Transaction Amount").ToString <> String.Empty Then
                    _previewRecord("Transaction Amount") = Convert.ToDecimal(_previewRecord("Transaction Amount").ToString()).ToString("########0.00")
                End If

                '#16. Address (always set blank)
                If _previewRecord("Counter Party Address").ToString.Trim <> String.Empty Then
                    _previewRecord("Counter Party Address") = String.Empty
                End If

                '#17. Transaction Remarks (Auto Shifting) - check position 70th
                Dim tempstr As String = String.Empty
                Dim pos As Int16 = 0
                Dim strbld As New System.Text.StringBuilder

                If _previewRecord("Transaction Remarks").ToString <> String.Empty And _previewRecord("Transaction Remarks").ToString().Length > 70 Then
                    tempstr = _previewRecord("Transaction Remarks").ToString.Substring(0, 70)
                    If Not tempstr(69).Equals(" ") Then
                        pos = tempstr.LastIndexOf(" ")
                        If pos > 0 Then
                            'get first half
                            strbld = New System.Text.StringBuilder(_previewRecord("Transaction Remarks").ToString().Substring(0, pos))
                            'second half
                            tempstr = _previewRecord("Transaction Remarks").ToString().Substring(pos + 1)

                            'fill trailing spaces
                            For i As Integer = 1 To 70 - pos
                                strbld.Append(" ")
                            Next

                            strbld.Append(tempstr)
                            _previewRecord("Transaction Remarks") = strbld.ToString()
                        End If

                    End If
                End If

                '#18. AutoCheque Advice Information (always set blank)
                If _previewRecord("AutoCheque Advice Information").ToString.Trim <> String.Empty Then
                    _previewRecord("AutoCheque Advice Information") = String.Empty
                End If

                '#19. Delivery Option (always set blank)
                If _previewRecord("Delivery Option").ToString.Trim <> String.Empty Then
                    _previewRecord("Delivery Option") = String.Empty
                End If

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Transaction Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub CustomRTMSValidate()

        Dim rowNo As Int32 = 0
        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim formatError As FileFormatError

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            ' Field #10 - Bank Number
            If _previewRecord("Bank Number").ToString.Trim <> String.Empty _
                                And regNonNumeric.IsMatch(_previewRecord("Bank Number").ToString) Then
                formatError = New FileFormatError
                With formatError
                    .ColumnName = "Bank Number"
                    .ColumnOrdinalNo = _PreviewTable.Columns("Bank Number").Ordinal + 1
                    .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Bank Number")
                    .RecordNo = rowNo + 1
                End With
                _ValidationErrors.Add(formatError)
                _ErrorAtMandatoryField = True
            End If

            ' Field #11 - Branch Number
            If _previewRecord("Branch Number").ToString.Trim <> String.Empty _
                                And regNonNumeric.IsMatch(_previewRecord("Branch Number").ToString) Then
                formatError = New FileFormatError
                With formatError
                    .ColumnName = "Branch Number"
                    .ColumnOrdinalNo = _PreviewTable.Columns("Branch Number").Ordinal + 1
                    .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Branch Number")
                    .RecordNo = rowNo + 1
                End With
                _ValidationErrors.Add(formatError)
                _ErrorAtMandatoryField = True
            End If

            'if bank number <> '047', then account number max length is 9.
            If _previewRecord("Bank Number").ToString.Trim <> String.Empty Then
                If Not _previewRecord("Bank Number").ToString().Equals("047") AndAlso _
                _previewRecord("Account Number").ToString().Trim.Length > 9 Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Account Number"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Account Number").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "Account Number", "9-digits when 'Bank Number' is not '047'")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                Else
                    If _previewRecord("Account Number").ToString().Trim.Length > 11 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Account Number").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09020050"), "Record", rowNo + 1, "Account Number", "11")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If
            End If

            rowNo += 1

        Next

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


#End Region


End Class
