Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Specific Features of iRTMS GD Format
''' </summary>
''' <remarks></remarks>
Public Class iRTMSGDFormat
    Inherits iRTMSFormat
    'In this format, group by fields and reference fields are same
    Private Shared _editableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                                 "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                                 "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                 "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                                 "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                                 "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                                 "Beneficiary Bank Country", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                 "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                 "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                 "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                                 "Debtor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                                 "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                                 "Account Currency"}

    Private Shared _validGroupByFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                                 "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                                 "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                 "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                                 "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                                 "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                                 "Beneficiary Bank Country", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                 "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                 "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                 "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                                 "Debtor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                                 "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                                 "Account Currency", "Consolidate Field"}

    Private Shared _autoTrimmableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", _
                                                   "Currency", "Beneficiary Name 1", "Beneficiary Name 2", "Beneficiary Attention", _
                                                   "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                   "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Bank Name", _
                                                   "Beneficiary Bank Branch Name", "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", _
                                                   "Beneficiary Bank Address 3", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                   "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                   "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                   "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                                   "Debtor Reference", "Charge Code", "Contract No", "Delivery Mode", "Special Flag", _
                                                   "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice"}
#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iRTMSGD"
        End Get
    End Property

#End Region

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property
    Public Shared Function OutputFormatString() As String
        Return "iRTMSGD"
    End Function

#Region "Overriden Methods"

    Protected Overrides Sub CustomiRTMSValidate()

        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            '3. Validation - Debtor Reference : Is mandatory when 'Transaction Code' is 'DDR'.
            If Not IsNothingOrEmptyString(_previewRecord("Transaction Code TR")) Then

                If _previewRecord("Transaction Code TR").ToString.Trim.ToUpper = "DDR" Then

                    If IsNothingOrEmptyString(_previewRecord("Debtor Reference")) Then

                        AddValidationError("Debtor Reference", _PreviewTable.Columns("Debtor Reference").Ordinal + 1 _
                        , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Debtor Reference", "'Transaction Code TR' is 'DDR'") _
                        , RowNumber + 1, True)

                    End If

                End If

            End If

            RowNumber += 1

        Next

    End Sub

#End Region

End Class