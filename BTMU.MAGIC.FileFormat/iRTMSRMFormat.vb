Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Specific Features of iRTMS RM Format
''' </summary>
''' <remarks></remarks>
Public Class iRTMSRMFormat
    Inherits iRTMSFormat
    'In this format,  group by fields and reference fields are same
    Private Shared _editableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                             "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                             "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                             "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                             "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                             "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                             "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                             "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                             "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                             "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                             "Customer Reference", "Charge Code", "Contract No", "Delivery Mode", "Pre-Advice Flag", _
                                             "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                             "Account Currency"}

    Private Shared _validGroupByFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", "Value Date", _
                                            "Account No. with BTMU", "Currency", "Amount", "Beneficiary Name 1", "Beneficiary Name 2", _
                                            "Beneficiary Attention", "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                            "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Account No.", "Beneficiary Bank Name", _
                                            "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                            "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", "Beneficiary Bank Address 3", _
                                            "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                            "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                            "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                            "Intermediary Bank BIC", "Charge Account No.", "Message to Beneficiary", "Message to Bank", _
                                            "Customer Reference", "Charge Code", "Contract No", "Delivery Mode", "Pre-Advice Flag", _
                                            "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice", _
                                            "Account Currency", "Consolidate Field"}

    Private Shared _autoTrimmableFields() As String = {"Serial No. TR", "Payment Method TR", "Transaction Code TR", "Record Type TR", _
                                                   "Currency", "Beneficiary Name 1", "Beneficiary Name 2", "Beneficiary Attention", _
                                                   "Beneficiary Contact", "Beneficiary Address 1", "Beneficiary Address 2", _
                                                   "Beneficiary Address 3", "Beneficiary Country", "Beneficiary Bank Name", _
                                                   "Beneficiary Bank Code", "Beneficiary Bank Branch Name", "Beneficiary Bank Branch Code", _
                                                   "Beneficiary Bank Address 1", "Beneficiary Bank Address 2", _
                                                   "Beneficiary Bank Address 3", "Beneficiary Bank Country", "Beneficiary Bank BIC", _
                                                   "Intermediary Bank Name", "Intermediary Bank Branch Name", "Intermediary Bank Address 1", _
                                                   "Intermediary Bank Address 2", "Intermediary Bank Address 3", "Intermediary Bank Country", _
                                                   "Intermediary Bank BIC", "Message to Beneficiary", "Message to Bank", _
                                                   "Customer Reference", "Charge Code", "Contract No", "Delivery Mode", "Pre-Advice Flag", _
                                                   "Serial No. AR", "Payment Method AR", "Transaction Code AR", "Record Type AR", "Transaction Advice"}
#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "iRTMSRM"
        End Get
    End Property

#End Region

    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property

    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validGroupByFields
        End Get
    End Property
    Public Shared Function OutputFormatString() As String
        Return "iRTMSRM"
    End Function

#Region "Overriden Methods"

    Protected Overrides Sub CustomiRTMSValidate()

        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Not IsNothingOrEmptyString(_previewRecord("Transaction Code TR")) Then

                Select Case _previewRecord("Transaction Code TR").ToString.Trim.ToUpper

                    Case "DD"
                        '3. Validation - Beneficiary Country : Is mandatory when 'Transaction Code' is 'DD'.
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Country")) Then
                            AddValidationError("Beneficiary Country", _PreviewTable.Columns("Beneficiary Country").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Country", "'Transaction Code TR' is 'DD'"), RowNumber + 1, True)
                        End If
                    Case "BT"
                        '4. Validation - Beneficiary Account No. : Is mandatory when 'Transaction Code' is 'BT' or 'DT' or 'FR'.
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Account No.")) Then
                            AddValidationError("Beneficiary Account No.", _PreviewTable.Columns("Beneficiary Account No.").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Account No.", "'Transaction Code TR' is 'BT' or 'DT' or 'FR'"), RowNumber + 1, True)
                        End If

                        '5. Validation - Beneficiary Bank Name : Is mandatory when 'Transaction Code' is 'BT' or 'DT' or 'FR' or empty.
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Name")) Then
                            AddValidationError("Beneficiary Bank Name", _PreviewTable.Columns("Beneficiary Bank Name").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Bank Name", "'Transaction Code TR' is 'BT', 'DT', 'FR' or empty"), RowNumber + 1, True)
                        End If


                    Case "DT", "FR"
                        '4.1 Validation - Beneficiary Account No. : Is mandatory when 'Transaction Code' is 'BT' or 'DT' or 'FR'.
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Account No.")) Then
                            AddValidationError("Beneficiary Account No.", _PreviewTable.Columns("Beneficiary Account No.").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Account No.", "'Transaction Code TR' is 'BT' or 'DT' or 'FR'"), RowNumber + 1, True)
                        End If

                        '5. Validation - Beneficiary Bank Name : Is mandatory when 'Transaction Code' is 'BT' or 'DT' or 'FR' or empty.
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Name")) Then
                            AddValidationError("Beneficiary Bank Name", _PreviewTable.Columns("Beneficiary Bank Name").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Bank Name", "'Transaction Code TR' is 'BT', 'DT', 'FR' or empty"), RowNumber + 1, True)
                        End If

                    Case ""
                        If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Name")) Then AddValidationError("Beneficiary Bank Name", _PreviewTable.Columns("Beneficiary Bank Name").Ordinal + 1, String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Bank Name", "'Transaction Code TR' is 'DT' or 'FR' or empty"), RowNumber + 1, True)

                End Select

            End If

            RowNumber += 1

        Next

        ' Validation - Beneficiary Bank Name : If 'Transaction Code' is 'BT' then the value is default BTMU, value read from source file.

    End Sub

    Protected Overrides Sub GenerateOutputFormat()

        Dim _rowIndex As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                _rowIndex += 1

                If Not IsNothingOrEmptyString(_previewRecord("Amount")) Then
                    If Decimal.TryParse(_previewRecord("Amount").ToString(), Nothing) Then
                        If _DataModel.IsAmountInCents Then _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If

                    'modified @ 08/03/2011, enhancement 2011
                    '#1. 2 decimal places for all Currency codes except TWD, JPY, and KRW.
                    Select Case _previewRecord("Currency").ToString().ToUpper()
                        Case "TWD", "JPY", "KRW"
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0.00")

                    End Select
                End If

                'modified @ 16/03/2011, enhancement 2011
                '#2. Replace with space to Non SWIFT Characters @
                '1.	    Field 9	    Beneficiary Name 1
                '2.	    Field 10	Beneficiary Name 2
                '3.	    Field 13	Beneficiary Address 1
                '4.	    Field 14	Beneficiary Address 2
                '5.	    Field 15	Beneficiary Address 3
                '       Field 16    Beneficiary Country
                '6.	    Field 18	Beneficiary Bank Name
                '7.	    Field 20	Beneficiary Branch Name
                '8.     Field 22	Beneficiary Bank Address 1
                '9.	    Field 23	Beneficiary Bank Address 2
                '10.	Field 24	Beneficiary Bank Address 3
                '       Field 25    Beneficiary Bank Country
                '11.	Field 27	Intermediary Bank Name
                '12.	Field 28	Intermediary Bank Branch Name
                '13.	Field 29	Intermediary Bank Address 1
                '14.	Field 30	Intermediary Bank Address 2
                '15.	Field 31	Intermediary Bank Address 3
                '       Field 32    Intermediary Bank Country
                '16.	Field 35	Message to Beneficiary

              
                _previewRecord(8) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(8).ToString())
                _previewRecord(9) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(9).ToString())
                _previewRecord(12) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(12).ToString())
                _previewRecord(13) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(13).ToString())
                _previewRecord(14) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(14).ToString())
                _previewRecord(15) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(15).ToString()) 'Bene country
                _previewRecord(17) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(17).ToString())
                _previewRecord(19) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(19).ToString())
                _previewRecord(21) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(21).ToString())
                _previewRecord(22) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(22).ToString())
                _previewRecord(23) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(23).ToString())
                _previewRecord(24) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(24).ToString()) 'Bene bank country
                _previewRecord(26) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(26).ToString())
                _previewRecord(27) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(27).ToString())
                _previewRecord(28) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(28).ToString())
                _previewRecord(29) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(29).ToString())
                _previewRecord(30) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(30).ToString())
                _previewRecord(31) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(31).ToString()) 'Intermediary bank country
                _previewRecord(34) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(34).ToString())


                'modified @ 08/03/2011, enhancement 2011
                '#2. Replace with space for Colon �:� or Hyphen �-� character in position 1st, 36th, 71st and 106th
                Dim replaceValue As New System.Text.StringBuilder(_previewRecord(34).ToString())
                For Each index As Int32 In ReplaceCharLocs

                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then replaceValue(index) = " "
                    End If
                Next
                _previewRecord(34) = replaceValue.ToString()
                replaceValue = Nothing

               
            Next

            GenerateTransactionCode()

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex, " Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex, " Amount"))
        End Try

    End Sub

    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                _rowIndex += 1

                If Not IsNothingOrEmptyString(_previewRecord("Amount")) Then
                    If Decimal.TryParse(_previewRecord("Amount").ToString(), Nothing) Then
                        If _DataModel.IsAmountInCents Then _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()) / 100
                    End If

                    'modified @ 08/03/2011, enhancement 2011
                    '#1. 2 decimal places for all Currency codes except TWD, JPY, and KRW.
                    Select Case _previewRecord("Currency").ToString().ToUpper()
                        Case "TWD", "JPY", "KRW"
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0")

                        Case Else
                            _previewRecord("Amount") = Convert.ToDecimal(_previewRecord("Amount").ToString()).ToString("###########0.00")

                    End Select
                End If

                'modified @ 16/03/2011, enhancement 2011
                '#2. Replace with space to Non SWIFT Characters @
                '1.	    Field 9	    Beneficiary Name 1
                '2.	    Field 10	Beneficiary Name 2
                '3.	    Field 13	Beneficiary Address 1
                '4.	    Field 14	Beneficiary Address 2
                '5.	    Field 15	Beneficiary Address 3
                '       Field 16    Beneficiary Country
                '6.	    Field 18	Beneficiary Bank Name
                '7.	    Field 20	Beneficiary Branch Name
                '8.     Field 22	Beneficiary Bank Address 1
                '9.	    Field 23	Beneficiary Bank Address 2
                '10.	Field 24	Beneficiary Bank Address 3
                '       Field 25    Beneficiary Bank Country
                '11.	Field 27	Intermediary Bank Name
                '12.	Field 28	Intermediary Bank Branch Name
                '13.	Field 29	Intermediary Bank Address 1
                '14.	Field 30	Intermediary Bank Address 2
                '15.	Field 31	Intermediary Bank Address 3
                '       Field 32    Intermediary Bank Country
                '16.	Field 35	Message to Beneficiary


                _previewRecord(8) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(8).ToString())
                _previewRecord(9) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(9).ToString())
                _previewRecord(12) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(12).ToString())
                _previewRecord(13) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(13).ToString())
                _previewRecord(14) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(14).ToString())
                _previewRecord(15) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(15).ToString()) 'Bene country
                _previewRecord(17) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(17).ToString())
                _previewRecord(19) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(19).ToString())
                _previewRecord(21) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(21).ToString())
                _previewRecord(22) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(22).ToString())
                _previewRecord(23) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(23).ToString())
                _previewRecord(24) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(24).ToString()) 'Bene bank country
                _previewRecord(26) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(26).ToString())
                _previewRecord(27) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(27).ToString())
                _previewRecord(28) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(28).ToString())
                _previewRecord(29) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(29).ToString())
                _previewRecord(30) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(30).ToString())
                _previewRecord(31) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(31).ToString()) 'Intermediary bank country
                _previewRecord(34) = RemoveIRTMSNonSWIFTCharacters(_previewRecord(34).ToString())


                'modified @ 08/03/2011, enhancement 2011
                '#2. Replace with space for Colon �:� or Hyphen �-� character in position 1st, 36th, 71st and 106th
                Dim replaceValue As New System.Text.StringBuilder(_previewRecord(34).ToString())
                For Each index As Int32 In ReplaceCharLocs

                    If index < replaceValue.Length Then
                        If replaceValue(index) = "-" Or replaceValue(index) = ":" Then replaceValue(index) = " "
                    End If
                Next
                _previewRecord(34) = replaceValue.ToString()
                replaceValue = Nothing


            Next

            GenerateTransactionCode()

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex, " Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030190"), "Record ", _rowIndex, " Amount"))
        End Try

    End Sub
#End Region

#Region "Helper Methods"

    ''' <summary>
    ''' This function is used only by iRTMSRM Format.
    ''' This is used to generate the value for "Transaction Code TR" and "Transaction Code AR",
    ''' if "Transaction Code TR" is empty.
    ''' The value for the "Transaction Code" is determined by the bank fields
    ''' 1. "Beneficiary Bank Name", 2. "Beneficiary Bank Country", 3. "Beneficiary Bank Branch Name",
    ''' 4. "Beneficiary Country", 5. "Currency"
    ''' </summary>
    Private Sub GenerateTransactionCode()

        Dim beneBankList() As String
        Dim beneBankCountryList() As String
        Dim beneBranchList() As String
        Dim beneCountryList() As String
        Dim currencyList() As String
        Dim matchBeneBank As Boolean
        Dim matchBeneBankCountry As Boolean
        Dim matchBeneBranch As Boolean
        Dim matchBeneCountry As Boolean
        Dim matchCurrency As String

        'Step 1 : Initialise the lists
        beneBankList = "".Split("|")
        beneBankCountryList = "".Split("|")
        beneBranchList = "".Split("|")
        beneCountryList = "".Split("|")
        currencyList = "".Split("|")

        ' Step 2 : Get the Data Sample Values from the Master Template for
        '          "Beneficiary Bank Name", "Beneficiary Bank Country", 
        '          "Beneficiary Bank Branch Name", "Beneficiary Country",
        '          "Currency".
        ' Get Beneficiary Bank Name Data Sample Values
        If Not IsNothingOrEmptyString(TblBankFields("Beneficiary Bank Name").BankSampleValue) Then
            beneBankList = TblBankFields("Beneficiary Bank Name").BankSampleValue.Split("|")
        End If
        ' Get Beneficiary Bank Country Data Sample Values
        If Not IsNothingOrEmptyString(TblBankFields("Beneficiary Bank Country").BankSampleValue) Then
            beneBankCountryList = TblBankFields("Beneficiary Bank Country").BankSampleValue.Split("|")
        End If
        ' Get Beneficiary Bank Branch Name Data Sample Values
        If Not IsNothingOrEmptyString(TblBankFields("Beneficiary Bank Branch Name").BankSampleValue) Then
            beneBranchList = TblBankFields("Beneficiary Bank Branch Name").BankSampleValue.Split("|")
        End If
        ' Get Beneficiary Country Data Sample Values
        If Not IsNothingOrEmptyString(TblBankFields("Beneficiary Country").BankSampleValue) Then
            beneCountryList = TblBankFields("Beneficiary Country").BankSampleValue.Split("|")
        End If
        ' Get Currency Data Sample Values
        If Not IsNothingOrEmptyString(TblBankFields("Currency").BankSampleValue) Then
            currencyList = TblBankFields("Currency").BankSampleValue.Split("|")
        End If

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If IsNothingOrEmptyString(_previewRecord("Transaction Code TR")) Then _previewRecord("Transaction Code TR") = ""
            If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Name")) Then _previewRecord("Beneficiary Bank Name") = ""
            If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Country")) Then _previewRecord("Beneficiary Bank Country") = ""
            If IsNothingOrEmptyString(_previewRecord("Beneficiary Bank Branch Name")) Then _previewRecord("Beneficiary Bank Branch Name") = ""
            If IsNothingOrEmptyString(_previewRecord("Beneficiary Country")) Then _previewRecord("Beneficiary Country") = ""
            If IsNothingOrEmptyString(_previewRecord("Currency")) Then _previewRecord("Currency") = ""

            If IsNothingOrEmptyString(_previewRecord("Transaction Code TR")) Then

                matchBeneBank = (BinarySearchCI(beneBankList, _previewRecord("Beneficiary Bank Name").ToString()) >= 0)
                matchBeneBankCountry = (BinarySearchCI(beneBankCountryList, _previewRecord("Beneficiary Bank Country").ToString()) >= 0)
                matchBeneBranch = (BinarySearchCI(beneBranchList, _previewRecord("Beneficiary Bank Branch Name").ToString()) >= 0)
                matchBeneCountry = (BinarySearchCI(beneCountryList, _previewRecord("Beneficiary Country").ToString()) >= 0)
                matchCurrency = (BinarySearchCI(currencyList, _previewRecord("Currency").ToString()) >= 0)

                If matchBeneBank And (matchBeneBankCountry Or (_previewRecord("Beneficiary Bank Country").ToString = "" And matchBeneCountry)) Then
                    _previewRecord("Transaction Code TR") = "BT"
                ElseIf matchBeneBank And matchBeneBranch Then
                    _previewRecord("Transaction Code TR") = "BT"
                ElseIf matchCurrency And (matchBeneBankCountry Or (_previewRecord("Beneficiary Bank Country").ToString = "" And matchBeneCountry)) Then
                    _previewRecord("Transaction Code TR") = "DT"
                Else
                    _previewRecord("Transaction Code TR") = "FR"
                End If

            End If

            _previewRecord("Transaction Code AR") = _previewRecord("Transaction Code TR").ToString

        Next

    End Sub

#End Region

End Class