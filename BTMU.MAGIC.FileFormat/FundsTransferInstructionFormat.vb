'Funds Transfer Instruction Import Format (BTMU-Hong Kong)
'***********************************************************************
' Change Requests after UAT (27/10/2011)
' 1. Remove 'Set always blank' validation from 'Template ID' field
' 2. Include ':' in the permitted character lists for all fields
' 3. Add a conditional logic for 'Funds Transfer Type' field
'***********************************************************************

Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the RTMS-FundsTransferInstruction Format
''' </summary>
''' <remarks></remarks>
Public Class FundsTransferInstructionFormat
    Inherits BaseFileFormat

    Private Shared _amountFieldName As String = "Settlement Amount"

    ' editable fields
    Private Shared _editableFields() As String = {"Template ID", "Value Date", "Transaction Reference" _
                    , "Settlement Currency", "Settlement Amount", "Beneficiary", "Beneficiary Address" _
                    , "Beneficiary Account", "Beneficiary Account GL", "Beneficiary Bank Name", "Beneficiary Branch" _
                    , "Beneficiary Bank Address", "Province", "City", "Settlement Account", "Charge Account" _
                    , "Funds Transfer Type", "Exchange Method", "Contract No", "Other Banks' Charge" _
                    , "Priority", "Orderer", "Message to Bank", "Message to Beneficiary", "Remark/ Remittance Usage" _
                    , "Security Group"}

    Private Shared _autoTrimmableFields() As String = {"Template ID", "Value Date", "Beneficiary Account GL" _
                    , "Beneficiary Bank Address", "Province", "City", "Charge Account", "Exchange Method" _
                    , "Other Banks' Charge", "Priority", "Orderer", "Message to Bank" _
                    , "Message to Beneficiary", "Remark/ Remittance Usage", "Security Group"}

    'In this format group by fields and reference fields are same
    Private Shared _validgroupbyFields() As String = {"Template ID", "Value Date", "Transaction Reference" _
                    , "Settlement Currency", "Settlement Amount", "Beneficiary", "Beneficiary Address" _
                    , "Beneficiary Account", "Beneficiary Account GL", "Beneficiary Bank Name", "Beneficiary Branch" _
                    , "Beneficiary Bank Address", "Province", "City", "Settlement Account", "Charge Account" _
                    , "Funds Transfer Type", "Exchange Method", "Contract No", "Other Banks' Charge" _
                    , "Priority", "Orderer", "Message to Bank", "Message to Beneficiary", "Remark/ Remittance Usage" _
                    , "Security Group", "Consolidate Field"}


    Public Shared Function OutputFormatString() As String
        Return "FTI"
    End Function

#Region "Overriden Properties"

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidGroupByFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property
    Protected Overrides ReadOnly Property ValidReferenceFields() As String()
        Get
            Return _validgroupbyFields
        End Get
    End Property

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "FTI"
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As System.Collections.Generic.List(Of String), ByVal _referenceFields As System.Collections.Generic.List(Of String), ByVal _appendText As System.Collections.Generic.List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("Settlement Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Settlement Amount"))
            If Not Decimal.TryParse(_row("Settlement Amount").ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Settlement Amount"))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, "Settlement Amount")

            _consolidatedRecordCount = _consolidatedData.Rows.Count

            For Each row As DataRow In _consolidatedData.Rows
                Dim sentences As String() = row.Item("Message to Bank").ToString.Split(New Char() {","c})
                Dim wordList As New List(Of String)

                For Each sentence As String In sentences
                    Dim sentenceTemp As String = sentence.Replace(vbCrLf, " ").Replace(vbLf, " ") & ","
                    Dim words As String() = sentenceTemp.Split(" ")
                    For Each word As String In words
                        wordList.Add(word)
                    Next
                Next

                Dim lines As New List(Of String)
                Dim line As String = String.Empty
                Dim isFirst As Boolean = True
                For Each word As String In wordList
                    If ((line.Length + word.Length) <= 35 - 1) Then
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            line = line & " " & word
                        End If
                    Else
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            lines.Add(line)
                            line = word
                        End If
                    End If
                Next
                lines.Add(line)

                If lines.Count > 2 Then
                    For i As Integer = lines.Count - 1 To 2 Step -1
                        lines.RemoveAt(i)
                    Next
                End If
                Dim output As String = String.Join(vbLf, lines.ToArray())
                If output.Length > 2 Then
                    output = output.Substring(0, output.Length - 1)
                End If
                row.Item("Message to Bank") = output
            Next

            For Each row As DataRow In _consolidatedData.Rows
                Dim sentences As String() = row.Item("Message to Beneficiary").ToString.Split(New Char() {","c})
                Dim wordList As New List(Of String)

                For Each sentence As String In sentences
                    Dim sentenceTemp As String = sentence.Replace(vbCrLf, " ").Replace(vbLf, " ") & ","
                    Dim words As String() = sentenceTemp.Split(" ")
                    For Each word As String In words
                        wordList.Add(word)
                    Next
                Next

                Dim lines As New List(Of String)
                Dim line As String = String.Empty
                Dim isFirst As Boolean = True
                For Each word As String In wordList
                    If ((line.Length + word.Length) <= 35 - 1) Then
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            line = line & " " & word
                        End If
                    Else
                        If isFirst Then
                            line = word
                            isFirst = False
                        Else
                            lines.Add(line)
                            line = word
                        End If
                    End If
                Next
                lines.Add(line)

                If lines.Count > 4 Then
                    For i As Integer = lines.Count - 1 To 3 Step -1
                        lines.RemoveAt(i)
                    Next
                End If
                Dim output As String = String.Join(vbLf, lines.ToArray())
                If output.Length > 2 Then
                    output = output.Substring(0, output.Length - 1)
                End If
                row.Item("Message to Beneficiary") = output
            Next

            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean
        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim enclosureCharacter As String
        Dim headertext As String = String.Empty
        'Dim crc32 As New CRC32()
        Dim hash As String = String.Empty
        Dim isFileGenerated, isControlFileGenerated As Boolean

        'iniitalize the values
        isFileGenerated = isControlFileGenerated = False

        Try

            'FTI format is Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
            Else
                Throw New Exception("Invalid master Template")
            End If

            'Prepare header (only field names)
            For Each col As DataColumn In _PreviewTable.Columns
                If Not col.ColumnName.ToUpper.Equals("CONSOLIDATE FIELD") Then
                    headertext &= """" & col.ColumnName() & ""","
                End If
            Next
            'remove the last comma
            headertext = headertext.Remove(headertext.LastIndexOf(","), 1)

            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                enclosureCharacter = ""
            Else
                enclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                line = ""

                For Each _column As String In TblBankFields.Keys

                    If TblBankFields(_column).Detail.ToUpper <> "NO" Then

                        If _column.ToUpper = "Settlement Amount".ToUpper Then

                            Dim amount As Decimal
                            '#########During SIT, Issue found!######################
                            '#ABS() function is used for amount (taken out already)#
                            '#######################################################
                            amount = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString)
                            Select Case _previewRecord("Settlement Currency").ToString.ToUpper
                                Case "TND", "KWD"
                                    If amount.ToString.StartsWith("-") Then
                                        _previewRecord("Settlement Amount") = "-" & Math.Round(amount, 3).ToString("###########0.000")
                                    Else
                                        _previewRecord("Settlement Amount") = Math.Round(amount, 3).ToString("###########0.000")
                                    End If
                                Case "TWD", "JPY", "KRW", "VND"
                                    If amount.ToString.StartsWith("-") Then
                                        _previewRecord("Settlement Amount") = "-" & Math.Round(amount).ToString("###########0")
                                    Else
                                        _previewRecord("Settlement Amount") = Math.Round(amount).ToString("###########0")
                                    End If
                                Case Else
                                    If amount.ToString.StartsWith("-") Then
                                        _previewRecord("Settlement Amount") = "-" & Math.Round(amount, 2).ToString("###########0.00")
                                    Else
                                        _previewRecord("Settlement Amount") = Math.Round(amount, 2).ToString("###########0.00")
                                    End If

                            End Select

                        End If

                        'added for consolidate field checking
                        If (Not _column.Equals("Consolidate Field")) Then
                            If enclosureCharacter.Trim = String.Empty Then
                                If line = "" Then
                                    line &= _previewRecord(_column).ToString
                                Else
                                    line = line & "," & _previewRecord(_column).ToString
                                End If
                            Else
                                If line = "" Then
                                    line = line & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                Else
                                    line = line & "," & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                End If
                            End If
                        End If

                    End If

                Next

                If rowNo <> _PreviewTable.Rows.Count - 1 Then
                    text &= line & vbNewLine
                Else
                    text &= line
                End If

                rowNo += 1

            Next

            'Append header from top
            '06062018 append header only if output format does not equal to FTI II (FHK Maggie)
            If (Not objMasterTemplate.OutputFormat().Equals(FundsTransferInstructionFormatII.OutputFormatString)) Then
                text = headertext & vbNewLine & text
            End If
            isFileGenerated = SaveTextToFile(text, _outputFile)

            '******************************************
            'commented the code which uses the CRC32 class
            ' it can generate only hexa value
            ' ''prepare for control code file
            ''Using fs As IO.FileStream = System.IO.File.Open(_outputFile, System.IO.FileMode.Open)
            ''    For Each b As Byte In crc32.ComputeHash(fs)

            ''        'Request from BTMU-HK @ 07/10/2011
            ''        'hexa value (no need to convert to hexa)
            ''        'hash += b.ToString("X2").ToUpper()
            ''        hash += b.ToString()
            ''    Next
            ''    hash = Convert.ToUInt64(hash.ToString())
            ''End Using
            '***************************************************

            '*******************************
            'Start Decimal Control code
            Dim fhk_crc As New FHK_CRC32
            Dim crc_32 As ULong = &HFFFFFFFFL

            Using fs As IO.FileStream = System.IO.File.Open(_outputFile, System.IO.FileMode.Open)
                Dim sr As IO.StreamReader = New IO.StreamReader(fs)
                Do While sr.Peek() >= 0
                    crc_32 = fhk_crc.update_crc_32(crc_32, Convert.ToChar(sr.Read()))
                Loop
                sr.Close()
                sr = Nothing
                fs.Close()
            End Using
            crc_32 = crc_32 Xor &HFFFFFFFFL

            'change the file extension for Control Code File
            _outputFile = _outputFile.Remove(_outputFile.Length - 4, 4) & ".txt" ' remove the extension and cast ".txt"
            isControlFileGenerated = SaveTextToFile(crc_32.ToString("0000000000"), _outputFile)

            'IF both files are generated successfully
            Return isFileGenerated AndAlso isControlFileGenerated

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)

        End Try

    End Function

    Protected Overrides Sub GenerateOutputFormat()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Change Request 1 after UAT
                '**************************
                '#1. Set Template ID to blank always
                '_previewRecord("Template ID") = String.Empty

                '#2. Transaction Reference (set running number.)
                If _previewRecord("Transaction Reference").ToString().Trim = String.Empty Then
                    seqNo += 1
                    _previewRecord("Transaction Reference") = "A" + seqNo.ToString("000000")
                End If

                '#Change Request Before UAT by BTMU-HK (13/10/2011)
                'Convert currency codes to upper case
                _previewRecord("Settlement Currency") = _previewRecord("Settlement Currency").ToString().ToUpper()

                '#3. Settlement Amount formatting by currency codes
                If _previewRecord("Settlement Amount").ToString <> String.Empty Then

                    If Not _DataModel.IsAmountInDollars Then
                        _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()) / 100
                    End If

                    Select Case _previewRecord("Settlement Currency").ToString().ToUpper()

                        Case "TND", "KWD"
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("##########0.000")
                        Case "TWD", "JPY", "KRW", "VND"
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("##############0")
                        Case Else
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("###########0.00")
                    End Select

                End If

                '#4. Beneficiary Account (set to blank if Fund Transfer Type is Demand Draft)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Then
                    _previewRecord("Beneficiary Account") = String.Empty

                End If

                '#5. Beneficiary Bank Name (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Bank Name") = String.Empty

                End If

                '#6. Beneficiary Branch (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Branch") = String.Empty

                End If

                '#7. Beneficiary Bank Address (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Bank Address") = String.Empty

                End If

                '#8. Set Province to blank always
                _previewRecord("Province") = String.Empty

                '#9. Set City to blank always
                _previewRecord("City") = String.Empty

                '#10. Set Settlement Account value to Charge Account if it's blank
                If _previewRecord("Charge Account").ToString.Trim = String.Empty Then
                    _previewRecord("Charge Account") = _previewRecord("Settlement Account")
                End If

                '#11. Set 'Beneficiary's Account' to Other Banks' Charge if it's blank
                If _previewRecord("Other Banks' Charge").ToString.Trim = String.Empty Then
                    _previewRecord("Other Banks' Charge") = "Beneficiary's Account"
                End If

                '#12. Set Priority to blank always
                _previewRecord("Priority") = String.Empty

                '#13. Set Remark/ Remittance Usage to blank always
                _previewRecord("Remark/ Remittance Usage") = String.Empty

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Settlement Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Settlement Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    'Any changes made in preview shall be refreshed
    Protected Overrides Sub GenerateOutputFormat1()
        Dim _rowIndex As Integer = -1
        Dim seqNo As Long = 0
        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _rowIndex += 1

                'Change Request 1 after UAT
                '**************************
                '#1. Set Template ID to blank always
                '_previewRecord("Template ID") = String.Empty

                '#2. Transaction Reference (set running number.)
                'will do only once in preview
                'If Not _previewRecord("Transaction Reference").ToString().Trim = String.Empty Then
                '    seqNo += 1
                '    _previewRecord("Transaction Reference") = "A" + seqNo.ToString("000000")
                'End If


                '#Change Request Before UAT by BTMU-HK (13/10/2011)
                'Convert currency codes to upper case
                _previewRecord("Settlement Currency") = _previewRecord("Settlement Currency").ToString().ToUpper()

                '#3. Settlement Amount formatting by currency codes
                If _previewRecord("Settlement Amount").ToString <> String.Empty Then

                    '############ During SIT Phase, found this issue ##################
                    '# Once the data is converted into dollar, no need to do it again #
                    '# Fixed @ 30 Sep 2011 by Kay                                     #
                    '##################################################################
                    'If Not _DataModel.IsAmountInDollars Then
                    '    _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()) / 100
                    'End If

                    Select Case _previewRecord("Settlement Currency").ToString().ToUpper()

                        Case "TND", "KWD"
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("##########0.000")
                        Case "TWD", "JPY", "KRW", "VND"
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("##############0")
                        Case Else
                            _previewRecord("Settlement Amount") = Convert.ToDecimal(_previewRecord("Settlement Amount").ToString()).ToString("###########0.00")
                    End Select

                End If

                '#4. Beneficiary Account (set to blank if Fund Transfer Type is Demand Draft)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Then
                    _previewRecord("Beneficiary Account") = String.Empty

                End If

                '#5. Beneficiary Bank Name (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Bank Name") = String.Empty

                End If

                '#6. Beneficiary Branch (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Branch") = String.Empty

                End If

                '#7. Beneficiary Bank Address (set to blank if Fund Transfer Type is Demand Draft or Book Transfer)
                If _previewRecord("Funds Transfer Type").ToString <> String.Empty And _
                    (_previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer")) Then
                    _previewRecord("Beneficiary Bank Address") = String.Empty

                End If

                '#8. Set Province to blank always
                _previewRecord("Province") = String.Empty

                '#9. Set City to blank always
                _previewRecord("City") = String.Empty

                '#10. Set Settlement Account value to Charge Account if it's blank
                If _previewRecord("Charge Account").ToString.Trim = String.Empty Then
                    _previewRecord("Charge Account") = _previewRecord("Settlement Account")
                End If

                '#11. Set 'Beneficiary's Account' to Other Banks' Charge if it's blank
                If _previewRecord("Other Banks' Charge").ToString.Trim = String.Empty Then
                    _previewRecord("Other Banks' Charge") = "Beneficiary's Account"
                End If

                '#12. Set Priority to blank always
                _previewRecord("Priority") = String.Empty

                '#13. Set Remark/ Remittance Usage to blank always
                _previewRecord("Remark/ Remittance Usage") = String.Empty

            Next

        Catch NumberOverFlow As ArithmeticException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030180"), "Record ", _rowIndex + 1, "Settlement Amount"))
        Catch fex As FormatException
            Throw New MagicException(String.Format(MsgReader.GetString("E09030191"), "Record ", _rowIndex + 1, "Settlement Amount"))
        Catch ex As Exception
            Throw New Exception("Error on generating records : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub Validate()

        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim _previewTableCopy As New DataTable
        Dim prevTransRef As String = String.Empty

        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)

        rowNo = 0

        Try
            ' Validate the fields of each record and add file format errors to _validationerrors object
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'Field #2 - Value Date
                Dim _ValueDate As Date
                If _previewRecord("Value Date").ToString().Trim <> String.Empty Then
                    If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString().Trim, "", TblBankFields("Value Date").DateFormat, _ValueDate, True) Then
                        'Cannot be a past date
                        If _ValueDate < Date.Today Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Value Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Value Date' cannot be a past date.", rowNo + 1)
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        'Cannot fall on Saturday or Sunday. 
                        If Weekday(_ValueDate) = 1 Or Weekday(_ValueDate) = 7 Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Value Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                            validationError.Description = String.Format(MsgReader.GetString("E09020030"), "Record", rowNo + 1, "Value Date")
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If
                    Else
                        'Must be a valid Date value
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Value Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Value Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Value Date' should be a valid date.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If
                End If

                'Field #3 - Transaction Reference
                'If _previewRecord("Transaction Reference").ToString.Trim = String.Empty Then

                '#################CHANGE REQUEST @ 30/09/2011#################
                '## No need to check uniquess anymore                        #
                '#############################################################

                'suppose to run only for the first record
                'If prevTransRef = String.Empty Then prevTransRef = _previewRecord("Transaction Reference").ToString

                'check uniqueness
                'If _previewRecord("Transaction Reference").ToString = prevTransRef Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Transaction Reference"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Reference").Ordinal + 1
                '        .Description = String.Format("Record: {0} Field: 'Transaction Reference' must be unique.", rowNo + 1)
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'End If

                ' Field #4 - Settlement Currency : Must be one of default values in master template
                If _previewRecord("Settlement Currency").ToString.Trim <> String.Empty Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Settlement Currency").DefaultValue) Then
                        If TblBankFields("Settlement Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Settlement Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Settlement Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Settlement Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Settlement Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Settlement Currency' is not a valid Currency.", rowNo + 1)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If


                ' Field #5 - Settlement Amount : Check after consolidation
                Dim amount As Decimal
                If IsConsolidated Then
                    If _previewRecord("Settlement Amount").ToString.Trim <> String.Empty And _
                           Decimal.TryParse(_previewRecord("Settlement Amount").ToString, amount) Then
                        'If IsNegativePaymentOption Then
                        '    If Val(_previewRecord("Settlement Amount").ToString.Trim) >= 0 Then
                        '        formatError = New FileFormatError
                        '        With formatError
                        '            .ColumnName = "Settlement Amount"
                        '            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Amount").Ordinal + 1
                        '            .Description = String.Format("Record: {0}, Field: '{1}' should not be zero or positive. Please check the payment option selected or there may be invalid data in your source file.", rowNo + 1, "Settlement Amount")
                        '            .RecordNo = rowNo + 1
                        '        End With
                        '        _ValidationErrors.Add(formatError)
                        '        _ErrorAtMandatoryField = True
                        '    End If
                        'Else
                        '    If Val(_previewRecord("Settlement Amount").ToString.Trim) <= 0 Then
                        '        formatError = New FileFormatError
                        '        With formatError
                        '            .ColumnName = "Settlement Amount"
                        '            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Amount").Ordinal + 1
                        '            .Description = String.Format("Record: {0}, Field: '{1}' should not be zero or negative. Please check the payment option selected or there may be invalid data in your source file.", rowNo + 1, "Settlement Amount")
                        '            .RecordNo = rowNo + 1
                        '        End With
                        '        _ValidationErrors.Add(formatError)
                        '        _ErrorAtMandatoryField = True
                        '    End If
                        'End If
                    End If

                End If

                ' Field #7 - Beneficiary Address (Conditionally mandatory)
                If _previewRecord("Funds Transfer Type").ToString.Equals("Foreign Remittance") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Demand Draft") Then
                    If _previewRecord("Beneficiary Address").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary Address"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Address").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Beneficiary Address", "'Funds Transfer Type' is 'Foreign Remittance' or 'Demand Draft'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If


                ' Field #8 - Beneficiary Account (Conditionally mandatory)
                If _previewRecord("Funds Transfer Type").ToString.Equals("Foreign Remittance") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("CHATS") Then
                    If _previewRecord("Beneficiary Account").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary Account"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Account").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Beneficiary Account", "'Funds Transfer Type' is 'Foreign Remittance', 'CHATS' or 'Book Transfer'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #8 - Beneficiary Account (maximum length is different for some FT-type)
                If _previewRecord("Beneficiary Account").ToString.Trim <> String.Empty Then
                    If (_previewRecord("Funds Transfer Type").ToString.Equals("Book Transfer") Or _
                        _previewRecord("Funds Transfer Type").ToString.Equals("CHATS")) Then
                        If _previewRecord("Beneficiary Account").ToString.Length > 20 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Beneficiary Account"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Account").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09020040"), "Record", rowNo + 1, "Beneficiary Account", "20-characters when 'Funds Transfer Type' is 'Book Transfer' or 'CHATS'")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    Else
                        If _previewRecord("Beneficiary Account").ToString.Length > 34 Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Beneficiary Account"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Account").Ordinal + 1
                                .Description = String.Format(MsgReader.GetString("E09020050"), "Record", rowNo + 1, "Beneficiary Account", "34")
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If
                    End If
                End If

                ' Field #9 - Beneficiary Account GL (allow 0-9)
                If _previewRecord("Beneficiary Account GL").ToString.Trim <> String.Empty And regNonNumeric.IsMatch(_previewRecord("Beneficiary Account GL").ToString) Then
                    formatError = New FileFormatError
                    With formatError
                        .ColumnName = "Beneficiary Account GL"
                        .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Account GL").Ordinal + 1
                        .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Beneficiary Account GL")
                        .RecordNo = rowNo + 1
                    End With
                    _ValidationErrors.Add(formatError)
                    _ErrorAtMandatoryField = True
                End If

                ' Field #10 - Beneficiary Bank Name (Conditionally mandatory)
                If _previewRecord("Funds Transfer Type").ToString.Equals("Foreign Remittance") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("CHATS") Then
                    If _previewRecord("Beneficiary Bank Name").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary Bank Name"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Bank Name").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Beneficiary Bank Name", "'Funds Transfer Type' is 'Foreign Remittance' or 'CHATS'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #11 - Beneficiary Branch (Conditionally mandatory)
                If _previewRecord("Funds Transfer Type").ToString.Equals("Foreign Remittance") Or _
                    _previewRecord("Funds Transfer Type").ToString.Equals("CHATS") Then
                    If _previewRecord("Beneficiary Branch").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Beneficiary Branch"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Beneficiary Branch").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Beneficiary Branch", "'Funds Transfer Type' is 'Foreign Remittance' or 'CHATS'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #15 - Settlement Account (exactly 10 or 11 digits)
                If _previewRecord("Settlement Account").ToString.Trim <> String.Empty Then

                    'Check digits first
                    If regNonNumeric.IsMatch(_previewRecord("Settlement Account").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Settlement Account"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Settlement Account")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Check length 10 or 11
                    If _previewRecord("Settlement Account").ToString.Length <> 10 And _previewRecord("Settlement Account").ToString.Length <> 11 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Settlement Account"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Settlement Account").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050041"), "Record", rowNo + 1, "Settlement Account", "10 or 11")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    ' Field #16 - Charge Account (digits)
                    If _previewRecord("Charge Account").ToString.Trim <> String.Empty And regNonNumeric.IsMatch(_previewRecord("Charge Account").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Charge Account"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Charge Account").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Charge Account")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Field #17 - Fund Transfer Type 
                    If _previewRecord("Funds Transfer Type").ToString.Trim <> String.Empty Then
                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Funds Transfer Type").DefaultValue) Then
                            If TblBankFields("Funds Transfer Type").DefaultValue.Length > 0 Then
                                Dim defaultTypes() As String
                                defaultTypes = TblBankFields("Funds Transfer Type").DefaultValue.Split(",")
                                For Each val As String In defaultTypes
                                    'this value is case sensitive
                                    If _previewRecord("Funds Transfer Type").ToString.Equals(val) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    formatError = New FileFormatError
                                    With formatError
                                        .ColumnName = "Funds Transfer Type"
                                        .ColumnOrdinalNo = _PreviewTable.Columns("Funds Transfer Type").Ordinal + 1
                                        .Description = String.Format("Record: {0} Field: 'Funds Transfer Type' is not a valid 'Funds Transfer Type'.", rowNo + 1)
                                        .RecordNo = rowNo + 1
                                    End With
                                    _ValidationErrors.Add(formatError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                    'Field #18 - Exchange Method 
                    If _previewRecord("Exchange Method").ToString.Trim <> String.Empty Then
                        Dim isFound As Boolean = False
                        If Not IsNothingOrEmptyString(TblBankFields("Exchange Method").DefaultValue) Then
                            If TblBankFields("Exchange Method").DefaultValue.Length > 0 Then
                                Dim defaultMethods() As String
                                defaultMethods = TblBankFields("Exchange Method").DefaultValue.Split(",")
                                For Each val As String In defaultMethods
                                    'this value is case in-sensitive
                                    If _previewRecord("Exchange Method").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                        isFound = True
                                        Exit For
                                    End If
                                Next

                                If Not isFound Then
                                    formatError = New FileFormatError
                                    With formatError
                                        .ColumnName = "Exchange Method"
                                        .ColumnOrdinalNo = _PreviewTable.Columns("Exchange Method").Ordinal + 1
                                        .Description = String.Format("Record: {0} Field: 'Exchange Method' is not a valid 'Exchange Method'.", rowNo + 1)
                                        .RecordNo = rowNo + 1
                                    End With
                                    _ValidationErrors.Add(formatError)
                                    _ErrorAtMandatoryField = True
                                End If
                            End If
                        End If
                    End If

                End If

                ' Field #19 - Contract No (Conditionally mandatory)
                If _previewRecord("Exchange Method").ToString.Equals("Contract", StringComparison.InvariantCultureIgnoreCase) Then
                    If _previewRecord("Contract No").ToString.Trim = String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Contract No"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Contract No").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050010"), "Record", rowNo + 1, "Contract No", "'Exchange Method' is 'Contract'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #19 - Contract No (must be blank)
                If Not _previewRecord("Exchange Method").ToString.Equals("Contract", StringComparison.InvariantCultureIgnoreCase) Then
                    If _previewRecord("Contract No").ToString.Trim <> String.Empty Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Contract No"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Contract No").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050011"), "Record", rowNo + 1, "Contract No", "'Exchange Method' is not 'Contract'")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                'Field #20 - Other Banks' Charge 
                If _previewRecord("Other Banks' Charge").ToString.Trim <> String.Empty Then
                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Other Banks' Charge").DefaultValue) Then
                        If TblBankFields("Other Banks' Charge").DefaultValue.Length > 0 Then
                            Dim defaultVals() As String
                            defaultVals = TblBankFields("Other Banks' Charge").DefaultValue.Split(",")
                            For Each val As String In defaultVals
                                'this value is case sensitive
                                If _previewRecord("Other Banks' Charge").ToString.Equals(val) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                formatError = New FileFormatError
                                With formatError
                                    .ColumnName = "Other Banks' Charge"
                                    .ColumnOrdinalNo = _PreviewTable.Columns("Other Banks' Charge").Ordinal + 1
                                    .Description = String.Format("Record: {0} Field: 'Other Banks' Charge' is not a valid 'Other Banks' Charge'.", rowNo + 1)
                                    .RecordNo = rowNo + 1
                                End With
                                _ValidationErrors.Add(formatError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If
                End If

                ' Validation - Do not allow unpermitted characters
                Dim regAllow As New Regex("[^0-9A-Za-z/?(),.:'+\-\s]", RegexOptions.Compiled)

                For Each col As String In TblBankFields.Keys
                    Select Case col
                        Case "Template ID", "Transaction Reference", "Beneficiary", "Beneficiary Address", _
                             "Beneficiary Account", "Beneficiary Bank Name", "Beneficiary Branch", "Beneficiary Bank Address", _
                             "Contract No", "Other Banks' Charge", "Orderer", "Message to Bank", "Message to Beneficiary", "Security Group"

                            If _previewRecord(col).ToString.Trim().Length > 0 AndAlso regAllow.IsMatch(_previewRecord(col).ToString) Then

                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = col
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns(col).Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: '{1}' contains invalid characters.", rowNo + 1, col)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True

                            End If

                        Case Else
                            'Do Nothing
                    End Select
                Next

                rowNo += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Validation : " & ex.Message.ToString)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Determine the Funds Transfer Type value 
    ''' which is similar to Sector Selection logic generation
    ''' In order to have a consistent fashion, implement the logic inside GenerateSectorSelection()
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GenerateSectorSelection()

        Dim ListBTMUBkName() As String, ListBTMUBrnName() As String
        Dim ListBTMUAdd() As String, ListLocalCurrency() As String
        Dim matchBkName As Boolean, matchCurrency As Boolean, matchAdd As Boolean, matchBranch As Boolean
        'Dim expandedCountryName As String = ""

        Try
            If ((TblBankFields.ContainsKey("BTMU Bank Name") Or TblBankFields.ContainsKey("BTMU Branch Name")) _
                    And TblBankFields.ContainsKey("Funds Transfer Type")) Then

                ListBTMUBkName = "".Split("|")
                ListBTMUBrnName = "".Split("|")
                ListBTMUAdd = "".Split("|")
                ListLocalCurrency = "".Split("|")

                If TblBankFields.ContainsKey("BTMU Bank Name") Then 'Get "BTMU Bank Name" Data Sample
                    If Not TblBankFields("BTMU Bank Name").BankSampleValue Is Nothing Then
                        ListBTMUBkName = TblBankFields("BTMU Bank Name").BankSampleValue.Split("|")
                        System.Array.Sort(ListBTMUBkName)
                    End If
                End If

                If TblBankFields.ContainsKey("BTMU Branch Name") Then 'Get "BTMU Branch Name" Data Sample
                    If Not TblBankFields("BTMU Branch Name").BankSampleValue Is Nothing Then
                        ListBTMUBrnName = TblBankFields("BTMU Branch Name").BankSampleValue.Split("|")
                        System.Array.Sort(ListBTMUBrnName)
                    End If
                End If

                If TblBankFields.ContainsKey("BTMU Address") Then  'Get "BTMU Address" Data Sample
                    If Not TblBankFields("BTMU Address").BankSampleValue Is Nothing Then
                        ListBTMUAdd = TblBankFields("BTMU Address").BankSampleValue.Split("|")
                        'expandedCountryName = ListCountry(0).ToUpper()
                        System.Array.Sort(ListBTMUAdd)
                    End If
                End If

                If TblBankFields.ContainsKey("Local Currency") Then 'Get "Currency" Data Sample
                    If Not TblBankFields("Local Currency").BankSampleValue Is Nothing Then
                        ListLocalCurrency = TblBankFields("Local Currency").BankSampleValue.Split("|")
                        System.Array.Sort(ListLocalCurrency)
                    End If
                End If

                
                For Each _previewRecord As DataRow In _PreviewTable.Rows

                    If _previewRecord("Funds Transfer Type").ToString() = "" Then
                        matchBkName = (BinarySearchCI(ListBTMUBkName, _previewRecord("Beneficiary Bank Name").ToString()) >= 0)
                        matchBranch = (BinarySearchCI(ListBTMUBrnName, _previewRecord("Beneficiary Branch").ToString()) >= 0)
                        matchAdd = (BinarySearchCI(ListBTMUAdd, _previewRecord("Beneficiary Bank Address").ToString()) >= 0)
                        If matchBkName And (matchBranch Or matchAdd) Then
                            _previewRecord("Funds Transfer Type") = "Book Transfer"
                        Else
                            matchCurrency = (BinarySearchCI(ListLocalCurrency, _previewRecord("Settlement Currency").ToString()) >= 0)

                            _previewRecord("Funds Transfer Type") = IIf(matchCurrency And matchAdd, "CHATS", "Foreign Remittance")
                        End If
                    End If
                Next

            End If

        Catch ex As Exception
            Throw New Exception("Error on generating Funds Transfer Type : " & ex.Message.ToString)
        End Try

    End Sub

#End Region


End Class
