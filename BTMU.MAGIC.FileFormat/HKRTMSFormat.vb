Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the Common Features of HK (AutoDebit/AutoCheque) formats
''' ''' </summary>
''' <remarks></remarks>
''' 
Public MustInherit Class HKRTMSFormat
    Inherits BaseFileFormat

    Public Shared _amountFieldName As String = "Transaction Amount"

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return _amountFieldName
        End Get
    End Property

#End Region

#Region "Overriden Methods"

    'validates "Amount" data after consolidation/edit/update 
    Protected Overrides Sub ConversionValidation()

        Dim dAmount As Decimal = 0
        Dim RowNumber As Int32 = 0

        For Each _previewRecord As DataRow In _PreviewTable.Rows

            If Decimal.TryParse(_previewRecord(_amountFieldName), dAmount) Then

                If IsConsolidated Then

                    'If IsNegativePaymentOption Then
                    '    If dAmount >= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050081"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'Else
                    '    If dAmount <= 0 Then AddValidationError(_amountFieldName, _PreviewTable.Columns(_amountFieldName).Ordinal + 1, String.Format(MsgReader.GetString("E09050082"), "Record", RowNumber + 1, _amountFieldName), RowNumber + 1, True)
                    'End If

                Else

                    'If dAmount <= 0 Then _ErrorAtMandatoryField = True

                End If
                If _previewRecord(_amountFieldName).ToString.Length > TblBankFields(_amountFieldName).DataLength.Value Then
                    _ErrorAtMandatoryField = True
                End If

            End If

            RowNumber += 1

        Next

    End Sub

    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As System.Collections.Generic.List(Of String), ByVal _referenceFields As System.Collections.Generic.List(Of String), ByVal _appendText As System.Collections.Generic.List(Of String))

        If _groupByFields.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0

        '1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row(_amountFieldName) Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, _amountFieldName))
            If Not Decimal.TryParse(_row(_amountFieldName).ToString(), Nothing) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, _amountFieldName))
        Next

        '2. Quick Conversion Setup File might have incorrect Group By and Reference Fields
        ValidateGroupByAndReferenceFields(_groupByFields, _referenceFields)

        '3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        Try
            dsPreview.Tables.Add(_PreviewTable.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto3("output", dsPreview.Tables(0) _
                                                    , _groupByFields, _referenceFields, _appendText, _amountFieldName)

            _consolidatedRecordCount = _consolidatedData.Rows.Count
            _PreviewTable.Rows.Clear()

            Dim fmtTblView As DataView = _consolidatedData.DefaultView
            'fmtTblView.Sort = "No ASC"
            _PreviewTable = fmtTblView.ToTable()

            ValidationErrors.Clear()
            FormatOutput()
            ValidateDataLength()
            ValidateMandatoryFields()
            Validate()
            ConversionValidation()
        Catch ex As Exception
            Throw New Exception("Error while consolidating records: " & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _masterTemplate As Object) As Boolean

        Dim text As String = String.Empty
        Dim line As String = String.Empty
        Dim rowNo As Integer = 0
        Dim enclosureCharacter As String
        Dim headertext As String = String.Empty
        'Dim crc32 As New CRC32()
        Dim hash As String = String.Empty
        Dim isFileGenerated, isControlFileGenerated As Boolean

        'iniitalize the values
        isFileGenerated = isControlFileGenerated = False

        Try

            'FTI format is Non Fixed Type Master Template
            Dim objMasterTemplate As MasterTemplateNonFixed
            If _masterTemplate.GetType Is GetType(MasterTemplateNonFixed) Then
                objMasterTemplate = CType(_masterTemplate, MasterTemplateNonFixed)
            Else
                Throw New Exception("Invalid master Template")
            End If

            'Prepare header (only field names)
            For Each col As DataColumn In _PreviewTable.Columns
                If Not col.ColumnName.ToUpper.Equals("CONSOLIDATE FIELD") Then
                    headertext &= """" & col.ColumnName() & ""","
                End If
            Next
            'remove the last comma
            headertext = headertext.Remove(headertext.LastIndexOf(","), 1)

            If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
                enclosureCharacter = ""
            Else
                enclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
            End If

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                line = ""

                For Each _column As String In TblBankFields.Keys

                    If TblBankFields(_column).Detail.ToUpper <> "NO" Then

                        'Round up the amount value
                        'If _column.ToUpper = "Transaction Amount".ToUpper Then
                        '    Dim amount As Decimal
                        '    amount = Math.Abs(Convert.ToDecimal(_previewRecord("Transaction Amount").ToString))
                        '    _previewRecord("Transaction Amount") = Math.Round(amount, 2).ToString("###########0.00")
                        'End If

                        'added for consolidate field checking
                        If (Not _column.Equals("Consolidate Field")) Then
                            If enclosureCharacter.Trim = String.Empty Then
                                If line = "" Then
                                    line &= _previewRecord(_column).ToString
                                Else
                                    line = line & "," & _previewRecord(_column).ToString
                                End If
                            Else
                                If line = "" Then
                                    line = line & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                Else
                                    line = line & "," & enclosureCharacter & _previewRecord(_column).ToString() & enclosureCharacter
                                End If
                            End If
                        End If

                    End If

                Next

                If rowNo <> _PreviewTable.Rows.Count - 1 Then
                    text &= line & vbNewLine
                Else
                    text &= line
                End If

                rowNo += 1

            Next

            'Append header from top
            'Header is not required to be capitalized
            '04062018 append header only if output format does not equal to AutoCheque ACMS (FHK Maggie)
            If (Not objMasterTemplate.OutputFormat().Equals(AutoChequeFormatII.OutputFormatString)) Then
                text = headertext & vbNewLine & text
            End If
            isFileGenerated = SaveTextToFile(text, _outputFile)

            'hexa control code (commented bcoz it's not applicable)
            ''prepare for control code file
            'Using fs As IO.FileStream = System.IO.File.Open(_outputFile, System.IO.FileMode.Open)
            '    For Each b As Byte In crc32.ComputeHash(fs)
            '        'Request from BTMU-HK @ 07/10/2011
            '        'hexa value (no need to convert to hexa)
            '        'hash += b.ToString("X2").ToUpper()
            '        hash += b.ToString()
            '    Next
            'End Using

            'Start Decimal Control code
            Dim fhk_crc As New FHK_CRC32
            Dim crc_32 As ULong = &HFFFFFFFFL

            Using fs As IO.FileStream = System.IO.File.Open(_outputFile, System.IO.FileMode.Open)
                Dim sr As IO.StreamReader = New IO.StreamReader(fs)
                Do While sr.Peek() >= 0
                    crc_32 = fhk_crc.update_crc_32(crc_32, Convert.ToChar(sr.Read()))
                Loop
                sr.Close()
                sr = Nothing
                fs.Close()
            End Using
            crc_32 = crc_32 Xor &HFFFFFFFFL

            'change the file extension
            _outputFile = _outputFile.Remove(_outputFile.Length - 4, 4) & ".txt" ' remove the extension and cast ".txt"
            isControlFileGenerated = SaveTextToFile(crc_32.ToString("0000000000"), _outputFile)

            'IF both files are generated successfully
            Return isFileGenerated AndAlso isControlFileGenerated

        Catch ex As Exception
            Throw New Exception("Error on file generation : " & ex.Message.ToString)
        End Try

    End Function

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Payment Date") = ValueDate.ToString(TblBankFields("Payment Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Payment Date: {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub Validate()

        Dim formatError As FileFormatError
        Dim rowNo As Integer = 0
        Dim _previewTableCopy As New DataTable
        Dim prevID As String = String.Empty
        Dim tempId As String = String.Empty
        Dim tempDate As Date
        Dim tempCur As String = String.Empty
        Dim tempPAN As String = String.Empty
        Dim tempPRef As String = String.Empty
        Dim tempInstID As String = String.Empty
        Dim tempInstrRemark As String = String.Empty


        Dim regNonNumeric As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim regNonAlphanum As New Regex("[^0-9a-zA-Z]", RegexOptions.Compiled)
        Dim regUnpermittedChar As New Regex("[^0-9a-zA-Z/?(),.:'+\-\s]", RegexOptions.Compiled)
        rowNo = 0

        Try
            ' Validate the fields of each record and add file format errors to _validationerrors object
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                'Field #1 - Group ID 
                If _previewRecord("Group ID").ToString.Trim <> String.Empty Then
                    'For uniqueness checking
                    If rowNo = 0 Then tempId = _previewRecord("Group ID").ToString.Trim

                    'must be D### format.
                    If _previewRecord("Group ID").ToString.Length < 4 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                    'must start with 'D'
                    If _previewRecord("Group ID").ToString.Length = 4 And _
                        Not _previewRecord("Group ID").ToString.StartsWith("D", StringComparison.InvariantCultureIgnoreCase) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                        'End If

                        'must followed by 3 digits
                    ElseIf _previewRecord("Group ID").ToString.Length = 4 And _
                            regNonNumeric.IsMatch(_previewRecord("Group ID").ToString.Substring(1)) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must start with 'D' and followed by 3-digits.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Check for uniqueness in the whole file
                    If Not tempId.Equals(_previewRecord("Group ID").ToString.Trim) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Group ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Group ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Group ID' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                'Field #2 - Payment Date
                Dim _ValueDate As Date
                If _previewRecord("Payment Date").ToString().Trim <> String.Empty Then
                    If HelperModule.IsDateDataType(_previewRecord("Payment Date").ToString(), "", TblBankFields("Payment Date").DateFormat, _ValueDate, True) Then

                        'For uniqueness checking
                        If rowNo = 0 Or tempDate.ToString().Equals("01/01/0001 12:00:00 AM") Then tempDate = _ValueDate

                        'Cannot be a past date or today's date
                        If _ValueDate <= Date.Today Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Payment Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                            validationError.Description = String.Format("Record: {0} Field: 'Payment Date' cannot be a past date or today's date.", rowNo + 1)
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        'Cannot fall on Saturday or Sunday. 
                        If Weekday(_ValueDate) = 1 Or Weekday(_ValueDate) = 7 Then
                            Dim validationError As New FileFormatError()
                            validationError.ColumnName = "Payment Date"
                            validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                            validationError.Description = String.Format(MsgReader.GetString("E09020030"), "Record", rowNo + 1, "Payment Date")
                            validationError.RecordNo = rowNo + 1
                            _ValidationErrors.Add(validationError)
                            _ErrorAtMandatoryField = True
                        End If

                        'Check for uniqueness in the whole file
                        If Not tempDate.ToString().Equals("01/01/0001 12:00:00 AM") And tempDate <> _ValueDate Then
                            formatError = New FileFormatError
                            With formatError
                                .ColumnName = "Payment Date"
                                .ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                                .Description = String.Format("Record: {0} Field: 'Payment Date' must be same for all records", rowNo + 1)
                                .RecordNo = rowNo + 1
                            End With
                            _ValidationErrors.Add(formatError)
                            _ErrorAtMandatoryField = True
                        End If

                    Else
                        'Must be a valid Date value
                        Dim validationError As New FileFormatError()
                        validationError.ColumnName = "Payment Date"
                        validationError.ColumnOrdinalNo = _PreviewTable.Columns("Payment Date").Ordinal + 1
                        validationError.Description = String.Format("Record: {0} Field: 'Payment Date' should be a valid date.", rowNo + 1)
                        validationError.RecordNo = rowNo + 1
                        _ValidationErrors.Add(validationError)
                        _ErrorAtMandatoryField = True

                    End If
                End If

                ' Field #3 - Currency : Must be one of default values in master template
                If _previewRecord("Currency").ToString <> String.Empty Then
                    If rowNo = 0 Or tempCur = String.Empty Then tempCur = _previewRecord("Currency").ToString

                    Dim isFound As Boolean = False
                    If Not IsNothingOrEmptyString(TblBankFields("Currency").DefaultValue) Then
                        If TblBankFields("Currency").DefaultValue.Length > 0 Then
                            Dim defaultCurrency() As String
                            defaultCurrency = TblBankFields("Currency").DefaultValue.Split(",")
                            For Each val As String In defaultCurrency
                                If _previewRecord("Currency").ToString.Equals(val, StringComparison.InvariantCultureIgnoreCase) Then
                                    isFound = True
                                    Exit For
                                End If
                            Next

                            If Not isFound Then
                                Dim validationError As New FileFormatError()
                                validationError.ColumnName = "Currency"
                                validationError.ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                                validationError.Description = String.Format("Record: {0} Field: 'Currency' is not a valid Currency.", rowNo + 1)
                                validationError.RecordNo = rowNo + 1
                                _ValidationErrors.Add(validationError)
                                _ErrorAtMandatoryField = True
                            End If
                        End If
                    End If

                    'Check for uniqueness in the whole file
                    If _previewRecord("Currency").ToString.Trim <> String.Empty And _
                        Not tempCur.Equals(_previewRecord("Currency").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Currency"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Currency").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Currency' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #4 - Principal Account Number (must be numeric)
                If _previewRecord("Principal Account Number").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempPAN = String.Empty Then tempPAN = _previewRecord("Principal Account Number").ToString

                    If regNonNumeric.IsMatch(_previewRecord("Principal Account Number").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Account Number").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Principal Account Number")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'Check for uniqueness in the whole file
                    If _previewRecord("Principal Account Number").ToString.Trim <> String.Empty And _
                            Not tempPAN.Equals(_previewRecord("Principal Account Number").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Account Number").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Principal Account Number' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #5 - Principal Reference
                If _previewRecord("Principal Reference").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempPRef = String.Empty Then tempPRef = _previewRecord("Principal Reference").ToString

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'If regUnpermittedChar.IsMatch(_previewRecord("Principal Reference").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Principal Reference"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Principal Reference").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Principal Reference")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                    'end remarked by fhk on 12-aug-2015

                    'Check for uniqueness in the whole file
                    If _previewRecord("Principal Reference").ToString.Trim <> String.Empty And _
                            Not tempPRef.Equals(_previewRecord("Principal Reference").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Principal Reference"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Principal Reference").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Principal Reference' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #6 - Instruction ID
                If _previewRecord("Instruction ID").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempInstID = String.Empty Then tempInstID = _previewRecord("Instruction ID").ToString
                    'check length (CR during Dev time)
                    If _previewRecord("Instruction ID").ToString.Length <> 3 Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Instruction ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Instruction ID").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09050040"), "Record", rowNo + 1, "Instruction ID", "3")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'check alphanumeric
                    'If regNonAlphanum.IsMatch(_previewRecord("Instruction ID").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Instruction ID"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Instruction ID").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Instruction ID")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                    'end remarked by fhk on 12-aug-2015

                    'Check for uniqueness in the whole file
                    If _previewRecord("Instruction ID").ToString.Trim <> String.Empty And _
                            Not tempInstID.Equals(_previewRecord("Instruction ID").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Instruction ID"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Instruction ID").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Instruction ID' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                ' Field #7 - Instruction Remarks
                If _previewRecord("Instruction Remarks").ToString.Trim <> String.Empty Then
                    If rowNo = 0 Or tempInstrRemark = String.Empty Then tempInstrRemark = _previewRecord("Instruction Remarks").ToString()

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'If regUnpermittedChar.IsMatch(_previewRecord("Instruction Remarks").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Instruction Remarks"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Instruction Remarks").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Instruction Remarks")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                    'end remarked by fhk on 12-aug-2015

                    'Check for uniqueness in the whole file
                    If _previewRecord("Instruction Remarks").ToString.Trim <> String.Empty And _
                            Not tempInstrRemark.Equals(_previewRecord("Instruction Remarks").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Instruction Remarks"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Instruction Remarks").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Instruction Remarks' must be same for all records", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If
                End If

                'Field #8 - Identifier
                If _previewRecord("Identifier").ToString.Trim <> String.Empty Then

                    'remarked by fhk on 12-aug-2015, allow special characters
                    'check alphanumeric
                    'If regNonAlphanum.IsMatch(_previewRecord("Identifier").ToString) Then
                    '    formatError = New FileFormatError
                    '    With formatError
                    '        .ColumnName = "Identifier"
                    '        .ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                    '        .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Identifier")
                    '        .RecordNo = rowNo + 1
                    '    End With
                    '    _ValidationErrors.Add(formatError)
                    '    _ErrorAtMandatoryField = True
                    'End If
                    'end remarked by fhk on 12-aug-2015

                    Dim isFound As Boolean = False
                    Dim sub_rno As Integer = -1
                    'check uniqueness in each record
                    For Each _subRec As DataRow In _PreviewTable.Rows
                        sub_rno += 1
                        'skip for the same record
                        If sub_rno = rowNo Then Continue For

                        If _previewRecord("Identifier").ToString.Equals(_subRec("Identifier").ToString, StringComparison.InvariantCultureIgnoreCase) Then
                            isFound = True
                            Exit For
                        End If

                    Next

                    If isFound Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Identifier"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Identifier").Ordinal + 1
                            .Description = String.Format("Record: {0} Field: 'Identifier' must be unique.", rowNo + 1)
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #9 - Counter Party Name
                'CR @ 07/10/2011 > from alphanum checking to unpermitted char checking.
                'remarked by fhk on 12-aug-2015, allow special characters
                'If _previewRecord("Counter Party Name").ToString.Trim <> String.Empty _
                '                    And regUnpermittedChar.IsMatch(_previewRecord("Counter Party Name").ToString) Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Counter Party Name"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Counter Party Name").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Counter Party Name")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'end remarked by fhk on 12-aug-2015

                ' Field #12 - Account Number
                If _previewRecord("Account Number").ToString.Trim <> String.Empty Then
                    'must be numeric
                    If regNonNumeric.IsMatch(_previewRecord("Account Number").ToString) Then
                        formatError = New FileFormatError
                        With formatError
                            .ColumnName = "Account Number"
                            .ColumnOrdinalNo = _PreviewTable.Columns("Account Number").Ordinal + 1
                            .Description = String.Format(MsgReader.GetString("E09030020"), "Record", rowNo + 1, "Account Number")
                            .RecordNo = rowNo + 1
                        End With
                        _ValidationErrors.Add(formatError)
                        _ErrorAtMandatoryField = True
                    End If

                End If

                ' Field #13 - Transaction Amount
                'Dim amount As Decimal
                'Amount must be positive
                'If _previewRecord("Transaction Amount").ToString().Trim <> String.Empty And _
                '    Decimal.TryParse(_previewRecord("Transaction Amount").ToString, amount) Then
                '    If amount <= 0 Then
                '        formatError = New FileFormatError
                '        With formatError
                '            .ColumnName = "Transaction Amount"
                '            .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Amount").Ordinal + 1
                '            .Description = String.Format(MsgReader.GetString("E09050080"), "Record", rowNo + 1, "Transaction Amount")
                '            .RecordNo = rowNo + 1
                '        End With
                '        _ValidationErrors.Add(formatError)
                '        _ErrorAtMandatoryField = True
                '    End If
                'End If


                ' Field #14 - Our Reference
                'remarked by fhk on 12-aug-2015, allow special characters
                'If _previewRecord("Our Reference").ToString.Trim <> String.Empty And _
                '    regUnpermittedChar.IsMatch(_previewRecord("Our Reference").ToString) Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Our Reference"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Our Reference").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Our Reference")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'end remarked by fhk on 12-aug-2015

                ' Field #15 - Advice Template
                'remarked by fhk on 12-aug-2015, allow special characters
                'If _previewRecord("Advice Template").ToString.Trim <> String.Empty And _
                '    regNonAlphanum.IsMatch(_previewRecord("Advice Template").ToString) Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Advice Template"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Advice Template").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09060020"), "Record", rowNo + 1, "Advice Template")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'end remarked by fhk on 12-aug-2015

                ' Field #17 - Transaction Remarks
                'remarked by fhk on 12-aug-2015, allow special characters
                'If _previewRecord("Transaction Remarks").ToString.Trim <> String.Empty And _
                '    regUnpermittedChar.IsMatch(_previewRecord("Transaction Remarks").ToString) Then
                '    formatError = New FileFormatError
                '    With formatError
                '        .ColumnName = "Transaction Remarks"
                '        .ColumnOrdinalNo = _PreviewTable.Columns("Transaction Remarks").Ordinal + 1
                '        .Description = String.Format(MsgReader.GetString("E09020100"), "Record", rowNo + 1, "Transaction Remarks")
                '        .RecordNo = rowNo + 1
                '    End With
                '    _ValidationErrors.Add(formatError)
                '    _ErrorAtMandatoryField = True
                'End If
                'end remarked by fhk on 12-aug-2015
                rowNo += 1

            Next

        Catch ex As Exception
            Throw New Exception("Error on Validation : " & ex.Message.ToString)
        End Try

        CustomRTMSValidate()

    End Sub

#End Region

    Protected Overridable Sub CustomRTMSValidate()
        'AutoDebit/AutoCheque formats can perform additional validations by overriding this method
    End Sub
End Class
