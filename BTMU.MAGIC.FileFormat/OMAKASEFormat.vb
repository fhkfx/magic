Imports BTMU.MAGIC.Common
Imports BTMU.MAGIC.MasterTemplate
Imports BTMU.MAGIC.CommonTemplate
Imports System.Collections
Imports System.Text.RegularExpressions

''' <summary>
''' Encapsulates the OMAKASE output Format:
''' </summary>
Public Class OMAKASEFormat
    Inherits BaseFileFormat

    'Target Data Tables
    Private _tblPayment As DataTable
    Private _tblInvoice As DataTable

    'Source Data Tables
    Private _tblPaymentSrc As DataTable
    Private _tblInvoiceSrc As DataTable

#Region "Local Constants"

    Private Shared _editableFields() As String = { _
                                "File Type-P", "Seq. No.-P", "Transaction Type(Payment Type)", "Beneficiary Bank Branch IFSC Code" _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary A/C No.", "Beneficiary Name" _
                                , "Transaction Amount", "Remark", "Payee Name-1", "Payee Name-2", "Notice By", "Beneficiary Fax Area Code" _
                                , "Beneficiary Fax No.", "Beneficiary E-mail Address", "Bene Address 1", "Bene Address 2", "Bene Address 3" _
                                , "Customer Reference 1", "Customer Reference 2", "VAT Amount-P", "Discount Amount", "Invoice Amount-P" _
                                , "Commission Charge To", "Mailing Name", "Mailing Address1", "Mailing Address2", "Mailing Address3", "Reserve-P" _
 _
                                , "Record Type-I", "Seq. No.-I", "Invoice No.", "Invoice Date", "Invoice Description" _
                                , "Invoice Amount-I", "VAT Amount-I", "Payment ID", "Reserve-I" _
                                }

    Private Shared _autoTrimmableFields() As String = { _
                                "Debit Type", "Fax Area Code", "Fax No.", "E-mail Address", "Contact Person" _
                                , "Batch ID", "Reserve-H" _
 _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Remark", "Payee Name-2" _
                                , "Beneficiary Fax Area Code", "Beneficiary Fax No.", "Beneficiary E-mail Address" _
                                , "Bene Address 2", "Bene Address 3", "Customer Reference 2", "Mailing Name" _
                                , "Mailing Address1", "Mailing Address2", "Mailing Address3", "Reserve-P" _
 _
                                , "Invoice Description", "Reserve-I" _
                                }

    Private Shared _headerfields() As String = {"File Type-H", "Account No", "Value Date", "No. of Record", "Total Amount", "Debit Type", "Fax Area Code", "Fax No.", "E-mail Address", "Contact Person", "Customer ID", "Batch ID", "Reserve-H"}

    'Fields: Payment Record
    Private Shared _fieldsPaymentRecord() As String = {"File Type-P", "Seq. No.-P", "Transaction Type(Payment Type)", "Beneficiary Bank Branch IFSC Code" _
                                , "Beneficiary Bank Name", "Beneficiary Branch Name", "Beneficiary A/C No.", "Beneficiary Name" _
                                , "Transaction Amount", "Remark", "Payee Name-1", "Payee Name-2", "Notice By", "Beneficiary Fax Area Code" _
                                , "Beneficiary Fax No.", "Beneficiary E-mail Address", "Bene Address 1", "Bene Address 2", "Bene Address 3" _
                                , "Customer Reference 1", "Customer Reference 2", "VAT Amount-P", "Discount Amount", "Invoice Amount-P" _
                                , "Commission Charge To", "Mailing Name", "Mailing Address1", "Mailing Address2", "Mailing Address3", "Reserve-P" _
                                }
    'Fields: Invoice Record
    Private Shared _fieldsInvoiceRecord() As String = {"Record Type-I", "Seq. No.-I", "Invoice No.", "Invoice Date", "Invoice Description" _
                                , "Invoice Amount-I", "VAT Amount-I", "Payment ID", "Reserve-I"}

#End Region

#Region "Overriden Properties"

    Protected Overrides ReadOnly Property AutoTrimmableFields() As String()
        Get
            Return _autoTrimmableFields
        End Get
    End Property

    Public Shared ReadOnly Property EditableFields() As String()
        Get
            Return _editableFields
        End Get
    End Property

    'Has more than one amount fields  
    Protected Overrides ReadOnly Property AmountFieldName() As String
        Get
            Return ""
        End Get
    End Property

    Protected Overrides ReadOnly Property FileFormatName() As String
        Get
            Return "Mr.Omakase India"
        End Get
    End Property

#End Region

    Public Shared Function OutputFormatString() As String
        Return "Mr.Omakase India"
    End Function

    Public Sub New()
        SetupPTITables()
    End Sub

#Region "Overriden Methods"

    ''' <summary>
    ''' Copy Source Values to BankFieds per Mapping and Add the record to "PreviewData" Data Table 
    ''' </summary>
    Protected Overrides Sub ReadAllSourceData()

        Try

            Try

                Dim obj As CommonTemplateTextDetail = _DataModel
                _tblPaymentSrc = obj._tblPaymentSrc.Copy()
                _tblInvoiceSrc = obj._tblInvoiceSrc.Copy()

            Catch ex As InvalidCastException
                Dim obj As CommonTemplateExcelDetail = _DataModel
                _tblPaymentSrc = obj._tblPaymentSrc.Copy()
                _tblInvoiceSrc = obj._tblInvoiceSrc.Copy()
            End Try

            Dim tmpDb As New DataSet()
            tmpDb.Tables.Add(_tblPaymentSrc)
            tmpDb.Tables.Add(_tblInvoiceSrc)

            Dim RPaymentInvoice As New DataRelation("PaymentInvoice" _
                                                  , tmpDb.Tables(0).Columns("ID") _
                                                  , tmpDb.Tables(1).Columns("ID"))

            tmpDb.Relations.Add(RPaymentInvoice)

            'Copy Source Data to Preview Table based on Map Settings
            Dim newrecord As DataRow

            For Each row As DataRow In tmpDb.Tables(0).Rows

                '#1. Copy Source Data to Header Fields
                'In case Source Fields of Invoice data have been mapped to 
                ' Payment Bank Fields, Value from the very first Row will be copied  
                'For Each headerfld As String In _headerfields

                '    For Each srcfld As MapSourceField In TblBankFields(headerfld).MappedSourceFields

                '        If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                '            srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                '        ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                '            Dim invoicerecords() As DataRow = row.GetChildRows("PaymentInvoice")
                '            If invoicerecords.Length > 0 Then srcfld.SourceFieldValue = invoicerecords(0)(srcfld.SourceFieldName)
                '        End If
                '    Next

                'Next

                'Copy Source Data to Payment Bank Fields
                For Each BankField As MapBankField In TblBankFields.Values

                    If Array.IndexOf(_fieldsPaymentRecord, BankField.BankField) = -1 Then Continue For

                    For Each srcfld As MapSourceField In BankField.MappedSourceFields

                        If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                            srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                        ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                            Dim invoicerecords() As DataRow = row.GetChildRows("PaymentInvoice")
                            If invoicerecords.Length > 0 Then srcfld.SourceFieldValue = invoicerecords(0)(srcfld.SourceFieldName)
                        End If

                    Next

                Next

                newrecord = _PreviewTable.NewRow()

                'For Each headerfld As String In _headerfields
                '    newrecord(headerfld) = TblBankFields(headerfld).MappedValues
                'Next

                ' Add Payment Record
                For Each column As String In _fieldsPaymentRecord
                    newrecord(column) = TblBankFields(column).MappedValues
                Next
                _PreviewTable.Rows.Add(newrecord)


                '#3. Copy Source Data to Invoice Bank Fields
                'In case Source Fields of Tax Data have been mapped to Invoice Bank Fields,
                ' value of the first row will be taken
                For Each invoicerow As DataRow In row.GetChildRows("PaymentInvoice")

                    For Each BankField As MapBankField In TblBankFields.Values

                        If Array.IndexOf(_fieldsInvoiceRecord, BankField.BankField) = -1 Then Continue For

                        For Each srcfld As MapSourceField In BankField.MappedSourceFields

                            If _tblPaymentSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(row(srcfld.SourceFieldName))
                            ElseIf _tblInvoiceSrc.Columns.IndexOf(srcfld.SourceFieldName) <> -1 Then
                                srcfld.SourceFieldValue = Convert.ToString(invoicerow(srcfld.SourceFieldName))
                            End If

                        Next

                    Next

                    newrecord = _PreviewTable.NewRow()
                    ' Add Invoice Record
                    For Each column As String In _fieldsInvoiceRecord
                        newrecord(column) = TblBankFields(column).MappedValues
                    Next
                    _PreviewTable.Rows.Add(newrecord)

                Next

            Next

            ' Clean up
            tmpDb.Dispose()

        Catch ex As InvalidCastException
            Throw New MagicException("Unsupported Common Template for iFTS-2 MultiLine Format!")
        Catch ex As Exception
            Throw New MagicException("Error reading Source Data(ReadAllSourceData): " & ex.Message.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Translate Bank Fields
    ''' Translation defined on Payment Bank Field is applicable to Payment Records.
    ''' Similarly the Translation defined on WHTax and Invoice Bank Fields are applicable to respective records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyTranslation()

        Dim rowindex As Int32 = 0
        Dim pid As String = ""
        Dim iid As String = ""
        Try

            Dim obj As CommonTemplateTextDetail = _DataModel
            pid = obj.PIndicator
            iid = obj.IIndicator

        Catch ex As InvalidCastException
            Dim obj As CommonTemplateExcelDetail = _DataModel
            pid = obj.PIndicator
            iid = obj.IIndicator
        End Try

        Try

            For Each translatedBankField As TranslatorSetting In _DataModel.TranslatorSettings
                rowindex = 0
                For Each _previewRecord As DataRow In _PreviewTable.Rows


                    If Array.IndexOf(_fieldsPaymentRecord, translatedBankField.BankFieldName) <> -1 Then
                        If Not pid.Equals(_previewRecord("File Type-P").ToString, StringComparison.InvariantCultureIgnoreCase) _
                           AndAlso Not "P".Equals(_previewRecord("File Type-P").ToString, StringComparison.InvariantCultureIgnoreCase) Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord, translatedBankField.BankFieldName) <> -1 Then
                        If Not iid.Equals(_previewRecord("Record Type-I").ToString, StringComparison.InvariantCultureIgnoreCase) _
                           AndAlso Not "I".Equals(_previewRecord("Record Type-I").ToString, StringComparison.InvariantCultureIgnoreCase) Then Continue For
                    End If

                    'If rowindex > 0 AndAlso _previewRecord("File Type-H") = "H" Then Continue For

                    If translatedBankField.CustomerValue = String.Empty Then
                        _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                    Else
                        If translatedBankField.CustomerValue.Equals(_previewRecord(translatedBankField.BankFieldName).ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                            _previewRecord(translatedBankField.BankFieldName) = translatedBankField.BankValue
                        End If
                    End If

                    rowindex += 1
                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying Translation Settings(ApplyTranslation) : {0}", ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply Lookup
    ''' Applicable only for Payment Records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyLookup()

        Dim _rowindex As Int32 = 0


        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If _previewRecord("File Type-P") <> "P" Then Continue For

                For Each lookupBankField As LookupSetting In _DataModel.LookupSettings

                    If Array.IndexOf(_fieldsPaymentRecord, lookupBankField.BankFieldName) = -1 Then Continue For

                    _previewRecord(lookupBankField.BankFieldName) = GetLookupValue(lookupBankField.TableName, _tblPaymentSrc.Rows(_rowindex)(lookupBankField.SourceFieldName), lookupBankField.LookupValue.Split("-")(0), lookupBankField.LookupKey.Split("-")(0))
                Next

                _rowindex += 1

            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error applying Lookup Settings(ApplyLookup) at Recod: {0} Error: {1}", _rowindex, ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply Calculation 
    ''' Applicable only for Payment Records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyCalculation()

        Dim _calvalue As Decimal = 0, rowIndex As Int32 = 0

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                If _previewRecord("File Type-P") <> "P" Then Continue For

                For Each calculatedBankField As CalculatedField In _DataModel.CalculatedFields

                    If Array.IndexOf(_fieldsPaymentRecord, calculatedBankField.BankFieldName) = -1 Then Continue For

                    If calculatedBankField.Operator1 = "+" Then
                        _calvalue = System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand1)) _
                                    + System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand2))
                    Else
                        _calvalue = System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand1)) _
                                    - System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand2))
                    End If

                    If calculatedBankField.Operator2 = "+" Then
                        _calvalue = _calvalue + System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand3))
                    ElseIf calculatedBankField.Operator2 = "-" Then
                        _calvalue = _calvalue - System.Convert.ToDecimal(_tblPaymentSrc.Rows(rowIndex)(calculatedBankField.Operand3))
                    End If

                    _previewRecord(calculatedBankField.BankFieldName) = _calvalue.ToString()

                Next

                rowIndex = rowIndex + 1
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error applying Calculation Settings(ApplyCalculation) at Recod: {0} Error: {1}", rowIndex, ex.Message))
        End Try

    End Sub

    ''' <summary>
    ''' Apply String Manipulation
    ''' String Manipulation defined on Payment Bank Field is applicable to Payment Records.
    ''' Similarly the String Manipulation defined on WHTax and Invoice Bank Fields are applicable to respective records
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub ApplyStringManipulation()

        Try
            Dim _svalue As String

            For Each manipulatedBankField As StringManipulation In _DataModel.StringManipulations

                For Each _previewRecord As DataRow In _PreviewTable.Rows

                    If Array.IndexOf(_fieldsPaymentRecord, manipulatedBankField.BankFieldName) <> -1 Then
                        If _previewRecord("File Type-P") <> "P" Then Continue For
                    ElseIf Array.IndexOf(_fieldsInvoiceRecord, manipulatedBankField.BankFieldName) <> -1 Then
                        If _previewRecord("Record Type-I") <> "I" Then Continue For
                    End If

                    _svalue = _previewRecord(manipulatedBankField.BankFieldName)

                    If _svalue = String.Empty Then Continue For

                    Select Case manipulatedBankField.FunctionName

                        Case "LEFT"
                            _svalue = Left(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "RIGHT"
                            _svalue = Right(_svalue, manipulatedBankField.NumberOfCharacters.Value)
                        Case "SUBSTRING"
                            _svalue = Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value)
                        Case "REMOVE"
                            ''''''''''Modified by jacky on 2019-07-15'''''''''
                            'Purpose: Replace function should only do once
                            '_svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "")
                            _svalue = Replace(_svalue, Mid(_svalue, manipulatedBankField.StartingPosition.Value, manipulatedBankField.NumberOfCharacters.Value), "", , 1)
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Case "REMOVE LAST"
                            If _svalue.Length > manipulatedBankField.NumberOfCharacters.Value Then
                                _svalue = _svalue.Substring(0, _svalue.Length - manipulatedBankField.NumberOfCharacters.Value)
                            Else
                                _svalue = ""
                            End If

                    End Select

                    _previewRecord(manipulatedBankField.BankFieldName) = _svalue

                Next

            Next

        Catch ex As Exception
            Throw New Exception(String.Format("Error applying String Manipulation Settings(ApplyStringManipulation) : {0}", ex.Message))
        End Try

    End Sub

    Protected Overrides Sub GenerateHeader()

        _Header = String.Empty

        For Each row As DataRow In _PreviewTable.Rows

            For Each headerfld As String In _headerfields
                _Header &= row(headerfld).ToString()
                _Header &= IIf(_Header = String.Empty, "", "|")
            Next

            If _Header.EndsWith("|") Then _Header = _Header.Substring(0, _Header.Length - 1)

            Exit For

        Next

        _Header &= vbCrLf

    End Sub

    'If UseValueDate Option is specified in Quick/Manual Conversion
    Protected Overrides Sub ApplyValueDate()

        Try

            For Each _previewRecord As DataRow In _PreviewTable.Rows
                _previewRecord("Value Date") = ValueDate.ToString(TblBankFields("Value Date").DateFormat.Replace("D", "d").Replace("Y", "y"), HelperModule.enUSCulture)
                Exit For
            Next

        Catch ex As Exception
            Throw New MagicException(String.Format("Error on Applying Value Date: {0}", ex.Message))
        End Try

    End Sub

    'Formatting of Number/Currency/Date Fields
    'Padding with Zero for certain fields
    Protected Overrides Sub FormatOutput()

        Dim strField As String = String.Empty
        Dim rowindex As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim payeeNames As New List(Of String)

        For Each row As DataRow In _PreviewTable.Rows

            'Format Fields: Header '''''''''''''''''''''''''''''''''''''''''''''''''''''
            If rowindex = 0 Then
                If row("Contact Person") <> "" Then
                    row("Contact Person") = row("Contact Person").ToString().ToUpper()
                End If
                If row("Total Amount") <> "" AndAlso Decimal.TryParse(row("Total Amount").ToString(), Nothing) Then
                    'row("Total Amount") = Convert.ToDecimal(row("Total Amount")).ToString("#.00")
                    row("Total Amount") = FormatAmount(Convert.ToDecimal(row("Total Amount").ToString))
                End If
                If row("Debit Type") <> "" Then
                    If row("Debit Type").ToString() <> "1" AndAlso row("Debit Type").ToString() <> "0" Then
                        row("Debit Type") = ""
                    End If
                End If

                row("Batch ID") = ""
            End If



            'Format Fields: Payment Record '''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row("File Type-P").ToString() = "P" Then

                If row("Transaction Amount") <> "" AndAlso Decimal.TryParse(row("Transaction Amount").ToString(), Nothing) Then
                    row("Transaction Amount") = row("Transaction Amount").ToString().Replace("-", "").Replace(",", "")
                    'row("Transaction Amount") = Convert.ToDecimal(row("Transaction Amount")).ToString("#########0.00")
                    row("Transaction Amount") = FormatAmount(Convert.ToDecimal(row("Transaction Amount").ToString))
                End If

                If row("VAT Amount-P") <> "" AndAlso Decimal.TryParse(row("VAT Amount-P").ToString(), Nothing) Then
                    'row("VAT Amount-P") = Convert.ToDecimal(row("VAT Amount-P")).ToString("#########0.00")
                    row("VAT Amount-P") = FormatAmount(Convert.ToDecimal(row("VAT Amount-P").ToString))
                End If

                If row("Discount Amount") <> "" AndAlso Decimal.TryParse(row("Discount Amount").ToString(), Nothing) Then
                    'row("Discount Amount") = Convert.ToDecimal(row("Discount Amount")).ToString("#########0.00")
                    row("Discount Amount") = FormatAmount(Convert.ToDecimal(row("Discount Amount").ToString))
                End If

                If row("Invoice Amount-P") <> "" AndAlso Decimal.TryParse(row("Invoice Amount-P").ToString(), Nothing) Then
                    'row("Invoice Amount-P") = Convert.ToDecimal(row("Invoice Amount-P")).ToString("#########0.00")
                    row("Invoice Amount-P") = FormatAmount(Convert.ToDecimal(row("Invoice Amount-P").ToString))
                End If

                If "A".Equals(row("Transaction Type(Payment Type)").ToString(), StringComparison.InvariantCultureIgnoreCase) Then

                    If "BTMU".Equals(row("Beneficiary Bank Name").ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                        If "BTMU".Equals(row("Beneficiary Branch Name").ToString(), StringComparison.InvariantCultureIgnoreCase) Then
                            row("Transaction Type(Payment Type)") = "3"
                        Else
                            row("Transaction Type(Payment Type)") = "4"
                        End If
                    Else
                        If Decimal.TryParse(row("Transaction Amount").ToString(), dAmount) Then
                            If dAmount > 100000 Then
                                row("Transaction Type(Payment Type)") = "1"
                            Else
                                row("Transaction Type(Payment Type)") = "2"
                            End If
                        End If
                    End If

                End If

                Select Case row("Transaction Type(Payment Type)").ToString()
                    Case "5"
                        row("Beneficiary A/C No.") = ""
                        row("Beneficiary Name") = ""
                        row("Mailing Name") = ""
                        row("Commission Charge To") = "A"
                    Case "6", "7"
                        row("Beneficiary A/C No.") = ""
                    Case Else
                        row("Payee Name-1") = ""
                        row("Payee Name-2") = ""
                End Select

                Select Case row("Notice By").ToString
                    Case "01", "03"
                        'no change required
                    Case "02", "04"
                        If IsNothingOrEmptyString(row("Beneficiary Fax Area Code")) Then row("Beneficiary Fax Area Code") = _PreviewTable.Rows(0)("Fax Area Code").ToString()
                        If IsNothingOrEmptyString(row("Beneficiary Fax No.")) Then row("Beneficiary Fax No.") = _PreviewTable.Rows(0)("Fax No.").ToString()
                    Case Else
                        row("Notice By") = "01"
                End Select

                'Payee Name-1 exceeds 80 chars, the exceeding chars shall be copied to payee name-2
                If row("Payee Name-1").ToString().Length > 80 Then
                    payeeNames = SplitPayeeName(row("Payee Name-1").ToString(), 80)
                    row("Payee Name-1") = payeeNames(0)
                    row("Payee Name-2") = payeeNames(1)
                End If

                If row("Mailing Name") = "" Then
                    row("Mailing Name") = row("Beneficiary Name")
                End If
                If row("Mailing Address1") = "" Then
                    row("Mailing Address1") = row("Bene Address 1")
                End If
                If row("Mailing Address2") = "" Then
                    row("Mailing Address2") = row("Bene Address 2")
                End If
                If row("Mailing Address3") = "" Then
                    row("Mailing Address3") = row("Bene Address 3")
                End If

                Select Case row("Commission Charge To").ToString().Trim
                    Case "A", "B"
                        'its valid
                    Case ""
                        row("Commission Charge To") = "A"
                End Select

                'Format Fields: Invoice Record '''''''''''''''''''''''''''''''''''''''''''''''
            ElseIf row("Record Type-I").ToString() = "I" Then

                If row("Invoice Amount-I") <> "" AndAlso Decimal.TryParse(row("Invoice Amount-I").ToString(), Nothing) Then
                    row("Invoice Amount-I") = row("Invoice Amount-I").ToString().Replace("-", "").Replace(",", "")
                    'row("Invoice Amount-I") = Convert.ToDecimal(row("Invoice Amount-I")).ToString("#########0.00")
                    row("Invoice Amount-I") = FormatAmount(Convert.ToDecimal(row("Invoice Amount-I").ToString))
                End If

                If row("VAT Amount-I") <> "" AndAlso Decimal.TryParse(row("VAT Amount-I").ToString(), Nothing) Then
                    'row("VAT Amount-I") = Convert.ToDecimal(row("VAT Amount-I")).ToString("#########0.00")
                    row("VAT Amount-I") = FormatAmount(Convert.ToDecimal(row("VAT Amount-I").ToString))
                End If

            End If

            rowindex += 1
        Next

    End Sub
    Private Function SplitPayeeName(ByVal VenName As String, ByVal StringLength As Integer) As List(Of String)
        Dim strSplit() As String
        Dim PayeeNames As New List(Of String)
        Dim PayeeName2StartPosition As Integer

        PayeeNames.Add("")
        PayeeNames.Add("")


        strSplit = VenName.Split(" ")
        For i As Integer = 0 To strSplit.Length - 1
            If strSplit(i).Length <= StringLength Then
                If PayeeNames(0) = String.Empty Then
                    PayeeNames(0) = strSplit(i)
                Else
                    If String.Concat(PayeeNames(0), " ", strSplit(i)).Length <= StringLength Then
                        PayeeNames(0) = String.Concat(PayeeNames(0), " ", strSplit(i))
                    Else
                        PayeeName2StartPosition = i
                        Exit For
                    End If
                End If
            End If
        Next

        For i As Integer = PayeeName2StartPosition To strSplit.Length - 1
            If PayeeNames(1) = String.Empty Then
                PayeeNames(1) = strSplit(i)
            Else
                PayeeNames(1) = String.Concat(PayeeNames(1), " ", strSplit(i))
            End If
        Next
        Return PayeeNames
    End Function
    'Validates Mandatory Fields.
    Protected Overrides Sub ValidateMandatoryFields()

        Dim RowNumber As Int32 = 0
        Dim IsPRecord, IsIRecord As Boolean

        Try

            'Validate Mandatory Header Fields
            For Each headerfld As String In New String() {"File Type-H", "Account No", "Value Date", "No. of Record", "Total Amount", "Customer ID"}

                If IsNothingOrEmptyString(_PreviewTable.Rows(0)(headerfld).ToString()) Then
                    AddValidationError(headerfld, _PreviewTable.Columns(headerfld).Ordinal + 1 _
                                       , String.Format(MsgReader.GetString("E09030160"), "Header", "", headerfld) _
                                       , 0, True)
                End If

            Next

            For Each row As DataRow In _PreviewTable.Rows

                IdentifyRecordType(row, IsPRecord, IsIRecord)

                If IsPRecord Then

                    'Validate Mandatory Payment Fields 
                    For Each _column As String In New String() {"File Type-P", "Seq. No.-P", "Transaction Type(Payment Type)", "Transaction Amount", "Notice By", "Bene Address 1", "Customer Reference 1", "VAT Amount-P", "Discount Amount", "Invoice Amount-P", "Commission Charge To"}

                        If IsNothingOrEmptyString(row(_column).ToString()) Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                                , RowNumber + 1, True)
                        End If

                    Next

                    'Payment Field 4 and 7 are required when Field 3 is '1', '2', '3' or '4'

                    Select Case row("Transaction Type(Payment Type)").ToString()
                        Case "1", "2", "3", "4"
                            If IsNothingOrEmptyString(row("Beneficiary Bank Branch IFSC Code").ToString()) Then
                                AddValidationError("Beneficiary Bank Branch IFSC Code", _PreviewTable.Columns("Beneficiary Bank Branch IFSC Code").Ordinal + 1 _
                                                 , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Bank Branch IFSC Code", "'Transaction Type(Payment Type)' is '1', '2', '3' or '4'") _
                                                 , RowNumber + 1, True)
                            End If

                            If IsNothingOrEmptyString(row("Beneficiary A/C No.").ToString()) Then
                                AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary A/C No.", "'Transaction Type(Payment Type)' is '1', '2', '3' or '4'") _
                                                , RowNumber + 1, True)
                            End If
                        Case "5", "6", "7"
                            If IsNothingOrEmptyString(row("Payee Name-1").ToString()) Then
                                AddValidationError("Payee Name-1", _PreviewTable.Columns("Payee Name-1").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Payee Name-1", "'Transaction Type(Payment Type)' is '5', '6' or '7'") _
                                                 , RowNumber + 1, True)
                            End If
                    End Select

                    'Payment field 8 is required when field-3 is other than '5'
                    If IsNothingOrEmptyString(row("Beneficiary Name").ToString()) AndAlso row("Transaction Type(Payment Type)") <> "5" Then
                        AddValidationError("Beneficiary Name", _PreviewTable.Columns("Beneficiary Name").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary Name", "'Transaction Type(Payment Type)' is '1', '2', '3', '4', '6' or '7'") _
                                            , RowNumber + 1, True)
                    End If

                    'Payment field 26 is required when field-3 is other than '5'
                    If IsNothingOrEmptyString(row("Mailing Name").ToString()) AndAlso row("Transaction Type(Payment Type)") <> "5" Then
                        AddValidationError("Mailing Name", _PreviewTable.Columns("Mailing Name").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Mailing Name", "'Transaction Type(Payment Type)' is '1', '2', '3', '4', '6' or '7'") _
                                            , RowNumber + 1, True)
                    End If

                    Select Case row("Notice By").ToString
                        Case "02", "04"
                            If IsNothingOrEmptyString(row("Beneficiary Fax Area Code").ToString()) Then
                                AddValidationError("Beneficiary Fax Area Code", _PreviewTable.Columns("Beneficiary Fax Area Code").Ordinal + 1 _
                                                     , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, "Beneficiary Fax Area Code") _
                                                     , RowNumber + 1, True)
                            End If

                            If IsNothingOrEmptyString(row("Beneficiary Fax No.").ToString()) Then
                                AddValidationError("Beneficiary Fax No.", _PreviewTable.Columns("Beneficiary Fax No.").Ordinal + 1 _
                                                     , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, "Beneficiary Fax No.") _
                                                     , RowNumber + 1, True)
                            End If
                    End Select

                    Select Case row("Notice By").ToString
                        Case "03", "04"
                            If IsNothingOrEmptyString(row("Beneficiary E-mail Address").ToString()) Then
                                AddValidationError("Beneficiary E-mail Address", _PreviewTable.Columns("Beneficiary E-mail Address").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09050010"), "Record", RowNumber + 1, "Beneficiary E-mail Address", "the P-record Field: 'Notice By' is '03' or '04'") _
                                                     , RowNumber + 1, True)
                            End If

                    End Select

                ElseIf IsIRecord Then 'Validate Mandatory Invoice Fields

                    For Each _column As String In New String() {"Record Type-I", "Seq. No.-I", "Invoice No." _
                                                    , "Invoice Date", "Invoice Amount-I", "VAT Amount-I", "Payment ID"}

                        If IsNothingOrEmptyString(row(_column).ToString()) Then
                            AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030160"), "Record", RowNumber + 1, _column) _
                                                , RowNumber + 1, True)
                        End If

                    Next

                End If

                RowNumber += 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Mandatory Fields: " & ex.Message.ToString)
        End Try

    End Sub

    'Performs Data Type, Conditional Mandatory and Custom Validations 
    '(other than Data Length and  Mandatory field validations)
    Protected Overrides Sub Validate()

        Dim IsPRecord, IsIRecord As Boolean
        Dim found As Boolean = False
        Dim RowNumber As Int32 = 0
        Dim dAmount As Decimal = 0
        Dim strField As String = String.Empty
        Dim _ValueDate As Date
        Dim regAllowAlphaNum As New Regex("[^0-9A-Za-z]", RegexOptions.Compiled)
        Dim regAllowNum As New Regex("[^0-9]", RegexOptions.Compiled)
        Dim regEmail As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled)

        Try
            For Each _previewRecord As DataRow In _PreviewTable.Rows
                For Each col As String In TblBankFields.Keys
                    If IsNothingOrEmptyString(_previewRecord(col)) Then _previewRecord(col) = ""
                    If _previewRecord(col) Is DBNull.Value Then _previewRecord(col) = ""
                Next

                IdentifyRecordType(_previewRecord, IsPRecord, IsIRecord)

                If Not (IsPRecord Xor IsIRecord) Then
                    AddValidationError("", 1, String.Format(MsgReader.GetString("E09030221"), RowNumber + 1) _
                                        , RowNumber + 1, True)
                    Continue For
                End If

                If RowNumber = 0 Then

                    'Validate Header Records
                    '#1. File Type is incorrect. It must be 'H'.
                    If (_previewRecord("File Type-H") & "").ToString().ToUpper() <> "H" Then
                        AddValidationError("File Type-H", _PreviewTable.Columns("File Type-H").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Type-H", "'H'") _
                                            , 0, True)
                    End If

                    If _previewRecord("Account No") <> "" Then
                        If _previewRecord("Account No").ToString().Length <> 19 Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                        , String.Format(MsgReader.GetString("E09050040"), "Header", "", "Account No", 19) _
                                        , 0, True)
                        End If
                        If regAllowAlphaNum.IsMatch(_previewRecord("Account No").ToString()) Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060020"), "Header", "", "Account No") _
                                                    , 0, True)
                        End If
                        If Not _previewRecord("Account No").ToString().EndsWith("INR") Then
                            AddValidationError("Account No", _PreviewTable.Columns("Account No").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060011"), "Header", "", "Account No") _
                                                    , 0, True)
                        End If
                    End If

                    If _previewRecord("Value Date") <> "" Then
                        If HelperModule.IsDateDataType(_previewRecord("Value Date").ToString(), TblBankFields("Value Date").DateFormat, True) Then
                            _ValueDate = DateTime.ParseExact(_previewRecord("Value Date").ToString(), TblBankFields("Value Date").DateFormat.Replace("YY", "yy").Replace("DD", "dd"), Nothing)
                            '_value date must be between current date and current date + 14 days
                            If _ValueDate < Date.Today OrElse _ValueDate > Date.Today.AddDays(14) Then
                                AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09030071"), "Header", "", "Value Date"), 0, True)
                            End If
                        Else
                            AddValidationError("Value Date", _PreviewTable.Columns("Value Date").Ordinal + 1, String.Format(MsgReader.GetString("E09040020"), "Header", "", "Value Date"), 0, True)
                        End If
                    End If

                    If _previewRecord("Fax Area Code") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Fax Area Code").ToString()) Then
                            AddValidationError("Fax Area Code", _PreviewTable.Columns("Fax Area Code").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Fax Area Code") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("Fax No.") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Fax No.").ToString()) Then
                            AddValidationError("Fax No.", _PreviewTable.Columns("Fax No.").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Fax No.") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("E-mail Address") <> "" Then 'valid internet email address
                        If Not regEmail.IsMatch(_previewRecord("E-mail Address").ToString()) Then
                            AddValidationError("E-mail Address", _PreviewTable.Columns("E-mail Address").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030022"), "Header", "", "E-mail Address") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("Contact Person") <> "" Then 'alphanumeric
                        If regAllowAlphaNum.IsMatch(_previewRecord("Contact Person").ToString().Replace(" ", "")) Then
                            AddValidationError("Contact Person", _PreviewTable.Columns("Contact Person").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09060020"), "Header", "", "Contact Person") _
                                                    , 0, True)
                        End If
                    End If
                    If _previewRecord("Customer ID") <> "" Then 'numbers only
                        If regAllowNum.IsMatch(_previewRecord("Customer ID").ToString()) Then
                            AddValidationError("Customer ID", _PreviewTable.Columns("Customer ID").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Header", "", "Customer ID") _
                                                    , 0, True)
                        End If
                    End If

                    '#2. First Record must be P Record
                    If Not IsPRecord Then
                        AddValidationError("File Type-P", _PreviewTable.Columns("File Type-P").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Type-P", "'P'") _
                                            , RowNumber + 1, True)
                    End If
                End If


                If IsPRecord Then 'Validate Fields: Payment Record

                    '#3. 'File Type-P' is incorrect. It must be 'P'.
                    If _previewRecord("File Type-P").ToString() <> "" AndAlso _previewRecord("File Type-P").ToString().ToUpper() <> "P" Then
                        AddValidationError("File Type-P", _PreviewTable.Columns("File Type-P").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "File Type-P", "'P'") _
                                            , RowNumber + 1, True)
                    End If

                    '#4. Transaction Amount Must be numeric
                    If _previewRecord("Transaction Amount") <> "" Then 'numbers only
                        If Not Decimal.TryParse(_previewRecord("Transaction Amount").ToString(), dAmount) Then
                            AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1, String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Transaction Amount"), RowNumber + 1, True)

                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Transaction Amount", _PreviewTable.Columns("Transaction Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Transaction Amount") _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        End If
                    End If

                    '#5. VAT Amount-P Must be numeric
                    If _previewRecord("VAT Amount-P") <> "" Then 'numbers only
                        If Not Decimal.TryParse(_previewRecord("VAT Amount-P").ToString(), dAmount) Then
                            AddValidationError("VAT Amount-P", _PreviewTable.Columns("VAT Amount-P").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "VAT Amount-P") _
                                                , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("VAT Amount-P", _PreviewTable.Columns("VAT Amount-P").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "VAT Amount-P") _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        End If
                    End If

                    '#6. Discount Amount Must be numeric
                    If _previewRecord("Discount Amount") <> "" Then 'numbers only
                        If Not Decimal.TryParse(_previewRecord("Discount Amount").ToString(), dAmount) Then
                            AddValidationError("Discount Amount", _PreviewTable.Columns("Discount Amount").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Discount Amount") _
                                                , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Discount Amount", _PreviewTable.Columns("Discount Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Discount Amount") _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        End If
                    End If

                    '#7. Invoice Amount-P Must be numeric
                    If _previewRecord("Invoice Amount-P") <> "" Then 'numbers only
                        If Not Decimal.TryParse(_previewRecord("Invoice Amount-P").ToString(), dAmount) Then
                            AddValidationError("Invoice Amount-P", _PreviewTable.Columns("Invoice Amount-P").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Invoice Amount-P") _
                                                    , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Invoice Amount-P", _PreviewTable.Columns("Invoice Amount-P").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Invoice Amount-P") _
                                                        , RowNumber + 1, True)
                                End If
                            End If

                        End If
                    End If

                    '#8. "Transaction Type(Payment Type)" must be one of   1, 2, 3, 4, 5, 6, 7
                    If _previewRecord("Transaction Type(Payment Type)") <> "" Then
                        Select Case _previewRecord("Transaction Type(Payment Type)")
                            Case "1", "2", "3", "4", "5", "6", "7"
                                'valid data
                            Case Else
                                AddValidationError("Transaction Type(Payment Type)", _PreviewTable.Columns("Transaction Type(Payment Type)").Ordinal + 1 _
                                                                                  , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Transaction Type(Payment Type)") _
                                                                                  , RowNumber + 1, True)
                        End Select
                    End If

                    If _previewRecord("Transaction Type(Payment Type)") <> "" Then
                        If _previewRecord("Transaction Type(Payment Type)") = "3" Or _previewRecord("Transaction Type(Payment Type)") = "4" Then
                            If _previewRecord("Beneficiary Bank Branch IFSC Code") = "" OrElse (Not _previewRecord("Beneficiary Bank Branch IFSC Code").ToString().StartsWith("BOTM")) Then
                                AddValidationError("Beneficiary Bank Branch IFSC Code", _PreviewTable.Columns("Beneficiary Bank Branch IFSC Code").Ordinal + 1 _
                                                                                                                  , String.Format(MsgReader.GetString("E09050061"), "Record", RowNumber + 1, "Beneficiary Bank Branch IFSC Code", "Transaction Type(Payment Type) is '3' or '4'") _
                                                                                                                  , RowNumber + 1, True)
                            End If
                        End If
                    End If
                    '#9. Beneficiary A/C No. must be alphanumeric
                    If _previewRecord("Beneficiary A/C No.") <> "" Then
                        If regAllowAlphaNum.IsMatch(_previewRecord("Beneficiary A/C No.").ToString()) Then
                            AddValidationError("Beneficiary A/C No.", _PreviewTable.Columns("Beneficiary A/C No.").Ordinal + 1 _
                                                , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Beneficiary A/C No.") _
                                                , RowNumber + 1, True)
                        End If
                    End If

                    '#10. Beneficiary Fax Area Code and fax no. must be numeric
                    'Select Case _previewRecord("Notice By").ToString
                    'Case "02", "04"
                    If regAllowNum.IsMatch(_previewRecord("Beneficiary Fax Area Code").ToString()) Then
                        AddValidationError("Beneficiary Fax Area Code", _PreviewTable.Columns("Beneficiary Fax Area Code").Ordinal + 1 _
                                         , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Beneficiary Fax Area Code") _
                                         , RowNumber + 1, True)
                    End If

                    If regAllowNum.IsMatch(_previewRecord("Beneficiary Fax No.").ToString()) Then
                        AddValidationError("Beneficiary Fax No.", _PreviewTable.Columns("Beneficiary Fax No.").Ordinal + 1 _
                                         , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Beneficiary Fax No.") _
                                         , RowNumber + 1, True)
                    End If

                    'End Select

                    If _previewRecord("Beneficiary E-mail Address") <> "" Then
                        If _previewRecord("Beneficiary E-mail Address").ToString().Split(",").Length > 2 Then
                            AddValidationError("Beneficiary E-mail Address", _PreviewTable.Columns("Beneficiary E-mail Address").Ordinal + 1 _
                                             , String.Format(MsgReader.GetString("E09030021"), "Record", RowNumber + 1, "Beneficiary E-mail Address") _
                                              , RowNumber + 1, True)
                        End If
                    End If


                    If _previewRecord("Beneficiary E-mail Address") <> "" Then 'valid internet email address
                        If Not regEmail.IsMatch(_previewRecord("Beneficiary E-mail Address").ToString()) Then
                            AddValidationError("Beneficiary E-mail Address", _PreviewTable.Columns("Beneficiary E-mail Address").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030022"), "Record", RowNumber + 1, "Beneficiary E-mail Address") _
                                                    , RowNumber + 1, True)
                        End If
                    End If

                    '#11. "Commission Charge To" must be A or B
                    If _previewRecord("Commission Charge To") <> "" Then
                        Select Case _previewRecord("Commission Charge To")
                            Case "A", "B"
                                'valid data
                            Case Else
                                AddValidationError("Commission Charge To)", _PreviewTable.Columns("Commission Charge To").Ordinal + 1 _
                                                                                  , String.Format(MsgReader.GetString("E09030100"), "Record", RowNumber + 1, "Commission Charge To") _
                                                                                  , RowNumber + 1, True)
                        End Select
                    End If

                ElseIf IsIRecord Then 'Validate Fields: Invoice Record

                    '#22. 'Record Type-I' is incorrect. It must be 'I'.
                    If _previewRecord("Record Type-I").ToString() <> "" AndAlso _previewRecord("Record Type-I").ToString().ToUpper() <> "I" Then
                        AddValidationError("Record Type-I", _PreviewTable.Columns("Record Type-I").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030050"), "Record", RowNumber + 1, "Record Type-I", "'I'") _
                                            , RowNumber + 1, True)
                    End If

                    '#23. Invoice No. - must be alphanumeric
                    If _previewRecord("Invoice No.") <> "" Then
                        If regAllowAlphaNum.IsMatch(_previewRecord("Invoice No.").ToString()) Then
                            AddValidationError("Invoice No.", _PreviewTable.Columns("Invoice No.").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09060020"), "Record", RowNumber + 1, "Invoice No.") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#24. Invoice Date - must be valid date and can be DDMMYYYY format
                    If _previewRecord("Invoice Date") <> "" Then
                        If (Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), TblBankFields("Invoice Date").DateFormat, True)) _
                          AndAlso (Not HelperModule.IsDateDataType(_previewRecord("Invoice Date").ToString(), "", "yMMdd", _ValueDate, True)) Then
                            AddValidationError("Invoice Date", _PreviewTable.Columns("Invoice Date").Ordinal + 1 _
                                               , String.Format(MsgReader.GetString("E09040020"), "Record", RowNumber + 1, "Invoice Date") _
                                               , RowNumber + 1, True)
                        End If
                    End If

                    '#25. Invoice Amount - must be numeric
                    If _previewRecord("Invoice Amount-I") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("Invoice Amount-I"), dAmount) Then
                            AddValidationError(strField, _PreviewTable.Columns("Invoice Amount-I").Ordinal + 1 _
                                                    , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "Invoice Amount-I") _
                                               , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Invoice Amount-I", _PreviewTable.Columns("Invoice Amount-I").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "Invoice Amount-I") _
                                                        , RowNumber + 1, True)
                                End If
                            End If
                        End If
                    End If

                    '#26. VAT - must be numeric
                    If _previewRecord("VAT Amount-I") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("VAT Amount-I"), dAmount) Then
                            AddValidationError(strField, _PreviewTable.Columns("VAT Amount-I").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Record", RowNumber + 1, "VAT Amount-I") _
                                            , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("VAT Amount-I", _PreviewTable.Columns("VAT Amount-I").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Record", RowNumber + 1, "VAT Amount-I") _
                                                        , RowNumber + 1, True)
                                End If
                            End If

                        End If
                    End If
                End If

                '#27. Check for | character in the following fields
                'Header : E-mail Address, Reserve-H,
                For Each fld As String In New String() {"E-mail Address", "Reserve-H"}
                    If Not IsNothingOrEmptyString(_previewRecord(fld)) Then
                        If _previewRecord(fld).ToString.Contains("|") Then
                            AddValidationError(fld, _PreviewTable.Columns(fld).Ordinal + 1 _
                                                                        , String.Format(MsgReader.GetString("E09020100"), "Header", 0, fld) _
                                                                        , 0, True)
                        End If
                    End If
                Next

                'Payment : Beneficiary Bank Branch IFSC Code, Beneficiary Bank Name, Beneficiary Branch Name, Beneficiary Name,
                '          Remark, Payee Name-1, Payee Name-2, Beneficiary E-mail address, Bene Address 1, Bene Address 2, Bene Address 3,
                '          Customer Reference 1, Customer Reference 2, Mailing Name, Mailing Address1,Mailing Address2,Mailing Address3,
                '          Reserve-P,
                For Each fld As String In New String() {"Beneficiary Bank Branch IFSC Code", "Beneficiary Bank Name", _
                                                        "Beneficiary Branch Name", "Beneficiary Name", "Remark", _
                                                        "Payee Name-1", "Payee Name-2", "Beneficiary E-mail address", _
                                                        "Bene Address 1", "Bene Address 2", "Bene Address 3", _
                                                        "Customer Reference 1", "Customer Reference 2", "Mailing Name", _
                                                        "Mailing Address1", "Mailing Address2", "Mailing Address3", "Reserve-P"}
                    If Not IsNothingOrEmptyString(_previewRecord(fld)) Then
                        If _previewRecord(fld).ToString.Contains("|") Then
                            AddValidationError(fld, _PreviewTable.Columns(fld).Ordinal + 1 _
                                                                        , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, fld) _
                                                                        , RowNumber + 1, True)
                        End If
                    End If
                Next
                'Invoice : Invoice No, Invoice Description, Reserve-I
                For Each fld As String In New String() {"Invoice No.", "Invoice Description", "Reserve-I"}
                    If Not IsNothingOrEmptyString(_previewRecord(fld)) Then
                        If _previewRecord(fld).ToString.Contains("|") Then
                            AddValidationError(fld, _PreviewTable.Columns(fld).Ordinal + 1 _
                                                                        , String.Format(MsgReader.GetString("E09020100"), "Record", RowNumber + 1, fld) _
                                                                        , RowNumber + 1, True)
                        End If
                    End If
                Next

                '#28. Do not allow non ascii characters in any fields
                For Each _column As String In TblBankFields.Keys

                    If ContainsNonASCIIChar(_previewRecord(_column)) Then

                        AddValidationError(_column, _PreviewTable.Columns(_column).Ordinal + 1 _
                        , String.Format(MsgReader.GetString("E09020100"), IIf(TblBankFields(_column).Header, "Header", "Record"), RowNumber + 1, _column) _
                        , RowNumber + 1)

                    End If
                Next

                'Total Amount should not contain more than 2 decimal places in the header
                If RowNumber = 0 Then
                    If _previewRecord("Total Amount") <> "" Then
                        If Not Decimal.TryParse(_previewRecord("Total Amount"), dAmount) Then
                            AddValidationError(strField, _PreviewTable.Columns("Total Amount").Ordinal + 1 _
                                            , String.Format(MsgReader.GetString("E09030020"), "Header", RowNumber + 1, "Total Amount") _
                                            , RowNumber + 1, True)
                        Else
                            If dAmount.ToString().IndexOf(".") > 0 Then 'Should not have more than 2 decimal places
                                If dAmount.ToString().Substring(dAmount.ToString().IndexOf(".") + 1).Length > 2 Then
                                    AddValidationError("Total Amount", _PreviewTable.Columns("Total Amount").Ordinal + 1 _
                                                        , String.Format(MsgReader.GetString("E09050070"), "Header", RowNumber + 1, "Total Amount") _
                                                        , RowNumber + 1, True)
                                End If
                            End If

                        End If
                    End If
                End If

                RowNumber = RowNumber + 1

            Next

        Catch ex As Exception
            Throw New MagicException("Error on validating Records: " & ex.Message.ToString)
        End Try

    End Sub

    'Usually this routines is for Summing up the amount field before producing output for preview
    'Exceptionally certain File Formats do not sum up amount field
    Protected Overrides Sub GenerateOutputFormat()

        If _PreviewTable.Rows.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0
        Dim _out As Decimal

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("File Type-P").ToString = "P" AndAlso _row("Transaction Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Transaction Amount"))
            If _row("File Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Transaction Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Transaction Amount"))
            If _row("File Type-P").ToString = "P" AndAlso _row("Discount Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Discount Amount"))
            If _row("File Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Discount Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Discount Amount"))
            If _row("Record Type-I").ToString = "I" AndAlso _row("Invoice Amount-I") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Invoice Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso (Not Decimal.TryParse(_row("Invoice Amount-I").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Invoice Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso _row("VAT Amount-I") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "VAT Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso (Not Decimal.TryParse(_row("VAT Amount-I").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "VAT Amount-I"))

            ' Take the absolute value of Transaction Amount and Invoice Amount-I
            If Not IsNothingOrEmptyString(_row("Transaction Amount")) Then
                If Decimal.TryParse(_row("Transaction Amount"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Transaction Amount") = (Math.Abs(_out) / 100).ToString
                    Else
                        _row("Transaction Amount") = Math.Abs(_out).ToString
                    End If
                End If
            End If

            If Not IsNothingOrEmptyString(_row("Discount Amount")) Then
                If Decimal.TryParse(_row("Discount Amount"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Discount Amount") = (_out / 100).ToString
                    Else
                        _row("Discount Amount") = (_out).ToString
                    End If
                End If
            End If

            If Not IsNothingOrEmptyString(_row("Invoice Amount-I")) Then
                If Decimal.TryParse(_row("Invoice Amount-I"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("Invoice Amount-I") = (Math.Abs(_out) / 100).ToString
                    Else
                        _row("Invoice Amount-I") = Math.Abs(_out).ToString
                    End If

                End If
            End If

            If Not IsNothingOrEmptyString(_row("VAT Amount-I")) Then
                If Decimal.TryParse(_row("VAT Amount-I"), _out) Then
                    If _DataModel.IsAmountInCents Then
                        _row("VAT Amount-I") = (_out / 100).ToString
                    Else
                        _row("VAT Amount-I") = (_out).ToString
                    End If
                End If
            End If

        Next

        '#3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        '#4. Preserve Header Fields
        Dim headervalues As New List(Of String)
        If _PreviewTable.Rows.Count > 0 Then
            For Each headerfld As String In _headerfields
                headervalues.Add(_PreviewTable.Rows(0)(headerfld).ToString())
            Next
        End If

        Try
            'Fork: Preview Table => Payment and Invoice Tables
            ForkPaymentRecords()

            dsPreview.Tables.Add(_tblInvoice.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0) _
                , "ID, sum{VAT Amount-I}=VAT Amount-I, SUM{Invoice Amount-I}=Invoice Amount-I", "", "ID", False, "", Nothing)

            'update the Invoice and Vat Amount fields of Payment Table
            Dim tmpDs As New DataSet()
            tmpDs.Tables.Add(_tblPayment.Copy)
            tmpDs.Tables.Add(_consolidatedData.Copy())
            tmpDs.Relations.Add(New DataRelation("PaymentGrpInvoice", tmpDs.Tables(0).Columns("ID"), tmpDs.Tables(1).Columns("ID")))

            For Each row As DataRow In tmpDs.Tables(0).Rows
                Dim chldRow() As DataRow = row.GetChildRows("PaymentGrpInvoice")
                If chldRow.Length > 0 Then
                    row("VAT Amount-P") = chldRow(0)("VAT Amount-I")
                    row("Invoice Amount-P") = chldRow(0)("Invoice Amount-I")
                End If
            Next
            _tblPayment = tmpDs.Tables(0).Copy()

            tmpDs.Tables(1).ParentRelations.Remove("PaymentGrpInvoice")
            tmpDs.Tables(1).Constraints.Remove("PaymentGrpInvoice")
            tmpDs.AcceptChanges()
            tmpDs.Dispose()

            _tblPayment.Constraints.RemoveAt(0)
            _tblPayment.Columns.Remove("ID")
            _tblPayment.Columns.Add("ID", GetType(String))

            dsPreview = New DataSet("PreviewDataSet")
            dsPreview.Tables.Add(_tblPayment.Copy())

            For Each row As DataRow In _tblPayment.Rows
                row("ID") = "1" ' just to enable grouping of all records
            Next
            dhelper.ds = dsPreview

            _consolidatedData = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0) _
               , "count{ID}=totalprecords, sum{Transaction Amount}=Transaction Amount", "", "ID", False, "", Nothing)

            Dim _cnt As Int32 = 1
            For Each row As DataRow In _tblPayment.Rows
                row("ID") = _cnt.ToString() ' restore
                _cnt += 1
            Next
            'update the header fields
            Dim tmpValue As Decimal = 0

            If headervalues.Count > 4 AndAlso _consolidatedData.Rows.Count > 0 Then
                headervalues(3) = _consolidatedData.Rows(0)("TOTALPRECORDS").ToString()
                If Decimal.TryParse(_consolidatedData.Rows(0)("Transaction Amount").ToString, tmpValue) Then headervalues(4) = FormatAmount(tmpValue.ToString())
            End If

            'tmpDs.Tables.Remove("TPayment")
            dsPreview.Dispose()

            'Update Sequence No.
            'Format : 0000#
            _cnt = 1
            For Each row As DataRow In _tblPayment.Rows
                row("Seq. No.-P") = _cnt.ToString("0000#")
                _cnt += 1
            Next

            'Join: Payment and Invoice Tables => Preview Table           
            JoinPaymentRecords()

            'Restore the Header Fields
            If _PreviewTable.Rows.Count > 0 Then
                _cnt = 0
                For Each headerfld As String In _headerfields
                    _PreviewTable.Rows(0)(headerfld) = headervalues(_cnt)
                    _cnt += 1
                Next
            End If
            'This is to format the calculated Amount fields
            FormatOutput()

        Catch ex As ArgumentException
            Throw New MagicException("Record Type is not recognizable ! Make sure the File Type-P and Record Type-I are Mapped to the correct ('P'/'I') values. If the field values is anything other than the default value, it must be translated.")
        Catch ex As Exception
            Throw New MagicException("Error while generating output records: " & ex.Message.ToString)
        End Try

    End Sub
    Protected Overrides Sub GenerateOutputFormat1()

        If _PreviewTable.Rows.Count = 0 Then Exit Sub

        Dim _rowIndex As Integer = 0
        Dim _out As Decimal

        '#1. Is Data valid for Consolidation?
        For Each _row As DataRow In _PreviewTable.Rows
            _rowIndex += 1
            If _row("File Type-P").ToString = "P" AndAlso _row("Transaction Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Transaction Amount"))
            If _row("File Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Transaction Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Transaction Amount"))
            If _row("File Type-P").ToString = "P" AndAlso _row("Discount Amount") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Discount Amount"))
            If _row("File Type-P").ToString = "P" AndAlso (Not Decimal.TryParse(_row("Discount Amount").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Discount Amount"))
            If _row("Record Type-I").ToString = "I" AndAlso _row("Invoice Amount-I") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "Invoice Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso (Not Decimal.TryParse(_row("Invoice Amount-I").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "Invoice Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso _row("VAT Amount-I") Is Nothing Then Throw New MagicException(String.Format(MsgReader.GetString("E09030200"), "Record ", _rowIndex, "VAT Amount-I"))
            If _row("Record Type-I").ToString = "I" AndAlso (Not Decimal.TryParse(_row("VAT Amount-I").ToString(), Nothing)) Then Throw New MagicException(String.Format(MsgReader.GetString("E09030020"), "Record ", _rowIndex, "VAT Amount-I"))

            ' Take the absolute value of Transaction Amount and Invoice Amount-I
            If Not IsNothingOrEmptyString(_row("Transaction Amount")) Then
                If Decimal.TryParse(_row("Transaction Amount"), _out) Then
                    _row("Transaction Amount") = Math.Abs(_out).ToString
                End If
            End If
            If Not IsNothingOrEmptyString(_row("Invoice Amount-I")) Then
                If Decimal.TryParse(_row("Invoice Amount-I"), _out) Then
                    _row("Invoice Amount-I") = Math.Abs(_out).ToString
                End If
            End If
        Next

        '#3. Consolidate Records and apply validations on
        Dim dhelper As New DataSetHelper()
        Dim dsPreview As New DataSet("PreviewDataSet")

        '#4. Preserve Header Fields
        Dim headervalues As New List(Of String)
        If _PreviewTable.Rows.Count > 0 Then
            For Each headerfld As String In _headerfields
                headervalues.Add(_PreviewTable.Rows(0)(headerfld).ToString())
            Next
        End If

        Try
            'Fork: Preview Table => Payment and Invoice Tables
            ForkPaymentRecords()

            dsPreview.Tables.Add(_tblInvoice.Copy())
            dhelper.ds = dsPreview

            Dim _consolidatedData As DataTable = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0) _
                , "ID, sum{VAT Amount-I}=VAT Amount-I, SUM{Invoice Amount-I}=Invoice Amount-I", "", "ID", False, "", Nothing)

            'update the Invoice and Vat Amount fields of Payment Table
            Dim tmpDs As New DataSet()
            tmpDs.Tables.Add(_tblPayment.Copy)
            tmpDs.Tables.Add(_consolidatedData.Copy())
            tmpDs.Relations.Add(New DataRelation("PaymentGrpInvoice", tmpDs.Tables(0).Columns("ID"), tmpDs.Tables(1).Columns("ID")))

            For Each row As DataRow In tmpDs.Tables(0).Rows
                Dim chldRow() As DataRow = row.GetChildRows("PaymentGrpInvoice")
                If chldRow.Length > 0 Then
                    row("VAT Amount-P") = chldRow(0)("VAT Amount-I")
                    row("Invoice Amount-P") = chldRow(0)("Invoice Amount-I")
                End If
            Next
            _tblPayment = tmpDs.Tables(0).Copy()

            tmpDs.Tables(1).ParentRelations.Remove("PaymentGrpInvoice")
            tmpDs.Tables(1).Constraints.Remove("PaymentGrpInvoice")
            tmpDs.AcceptChanges()
            tmpDs.Dispose()

            _tblPayment.Constraints.RemoveAt(0)
            _tblPayment.Columns.Remove("ID")
            _tblPayment.Columns.Add("ID", GetType(String))

            dsPreview = New DataSet("PreviewDataSet")
            dsPreview.Tables.Add(_tblPayment.Copy())

            For Each row As DataRow In _tblPayment.Rows
                row("ID") = "1" ' just to enable grouping of all records
            Next
            dhelper.ds = dsPreview

            _consolidatedData = dhelper.SelectGroupByInto2("output", dsPreview.Tables(0) _
               , "count{ID}=totalprecords, sum{Transaction Amount}=Transaction Amount", "", "ID", False, "", Nothing)

            Dim _cnt As Int32 = 1
            For Each row As DataRow In _tblPayment.Rows
                row("ID") = _cnt.ToString() ' restore
                _cnt += 1
            Next
            'update the header fields
            Dim tmpValue As Decimal = 0
            If headervalues.Count > 4 AndAlso _consolidatedData.Rows.Count > 0 Then
                headervalues(3) = _consolidatedData.Rows(0)("TOTALPRECORDS").ToString()
                If Decimal.TryParse(_consolidatedData.Rows(0)("Transaction Amount").ToString, tmpValue) Then headervalues(4) = FormatAmount(tmpValue.ToString)
                ' headervalues(4) = _consolidatedData.Rows(0)("Transaction Amount").ToString("#.00")
            End If

            'tmpDs.Tables.Remove("TPayment")
            dsPreview.Dispose()

            'Update Sequence No.
            'Format : 0000#
            _cnt = 1
            For Each row As DataRow In _tblPayment.Rows
                row("Seq. No.-P") = _cnt.ToString("0000#")
                _cnt += 1
            Next

            'Join: Payment and Invoice Tables => Preview Table           
            JoinPaymentRecords()

            'Restore the Header Fields
            If _PreviewTable.Rows.Count > 0 Then
                _cnt = 0
                For Each headerfld As String In _headerfields
                    _PreviewTable.Rows(0)(headerfld) = headervalues(_cnt)
                    _cnt += 1
                Next
            End If

            'This is to format the calculated Amount fields
            FormatOutput()

        Catch ex As ArgumentException
            Throw New MagicException("Record Type is not recognizable ! Make sure the File Type-P and Record Type-I are Mapped/Translated to the correct ('P'/'I') values.")
        Catch ex As Exception
            Throw New MagicException("Error while generating output records: " & ex.Message.ToString)
        End Try

    End Sub
    'Consolidation is not applicable to this file format
    Public Overrides Sub GenerateConsolidatedData(ByVal _groupByFields As List(Of String), ByVal _referenceFields As List(Of String), ByVal _appendText As List(Of String))
        Throw New MagicException(String.Format("File Format '{0}' does not support Data Consolidation", FileFormatName))
    End Sub

    'The output file consists of
    '1)	All Header Fields in one line separated by | 
    '3)	Payment Record (mandatory)
    '4)	Multiple WHT Records (optional)
    '5)	Multiple Invoice Record (optional)
    '�	iFTS-2 output file format will be a text file with all fields enclosed within double quotes and separated by comma. 
    '�	Empty data fields will be separated by comma without double quotes
    Public Overrides Function GenerateFile(ByVal _userName As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _objMaster As Object) As Boolean

        Dim sTaxAmts As String = String.Empty
        Dim sText As String = String.Empty
        Dim sLine As String = String.Empty
        Dim sEnclosureCharacter As String
        Dim rowNo As Integer = 0
        Dim rowNoLast As Integer = _PreviewTable.Rows.Count - 1
        Dim omakaseOutput2FileName As String = String.Empty

        Dim objMasterTemplate As MasterTemplateNonFixed ' Non Fixed Type Master Template
        If _objMaster.GetType Is GetType(MasterTemplateNonFixed) Then
            objMasterTemplate = CType(_objMaster, MasterTemplateNonFixed)
        Else
            Throw New MagicException("Invalid master Template")
        End If

        If IsNothingOrEmptyString(objMasterTemplate.EnclosureCharacter) Then
            sEnclosureCharacter = ""
        Else
            sEnclosureCharacter = objMasterTemplate.EnclosureCharacter.Trim
        End If

        Try

            '1. File Format Type
            GenerateHeader()
            sText = _Header

            For Each _previewRecord As DataRow In _PreviewTable.Rows

                sLine = ""

                '2. Payment Record
                If _previewRecord("File Type-P").ToString() = "P" Then

                    If _previewRecord("Transaction Amount").ToString() <> "" Then
                        If _previewRecord("Transaction Amount").ToString.StartsWith("-") Then
                            _previewRecord("Transaction Amount") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Transaction Amount"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("Transaction Amount") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Transaction Amount"))), 2).ToString("############0.00")
                        End If
                    End If

                    For Each _column As String In _fieldsPaymentRecord

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", "|") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", "|") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                    '3. Invoice Records
                ElseIf _previewRecord("Record Type-I").ToString() = "I" Then

                    If _previewRecord("Invoice Amount-I").ToString() <> "" Then
                        If _previewRecord("Invoice Amount-I").ToString.StartsWith("-") Then
                            _previewRecord("Invoice Amount-I") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Invoice Amount-I"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("Invoice Amount-I") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("Invoice Amount-I"))), 2).ToString("############0.00")
                        End If
                    End If

                    If _previewRecord("VAT Amount-I").ToString() <> "" Then
                        If _previewRecord("VAT Amount-I").ToString.StartsWith("-") Then
                            _previewRecord("VAT Amount-I") = "-" & Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("VAT Amount-I"))), 2).ToString("############0.00")
                        Else
                            _previewRecord("VAT Amount-I") = Math.Round(Math.Abs(Convert.ToDecimal(_previewRecord("VAT Amount-I"))), 2).ToString("############0.00")
                        End If
                    End If

                    For Each _column As String In _fieldsInvoiceRecord

                        If _previewRecord(_column).ToString() = "" Then
                            sLine = sLine & IIf(sLine = "", "", "|") & _previewRecord(_column).ToString()
                        Else
                            sLine = sLine & IIf(sLine = "", "", "|") & sEnclosureCharacter & _previewRecord(_column).ToString() & sEnclosureCharacter
                        End If

                    Next

                End If

                sText = sText & sLine & IIf(rowNo = rowNoLast, "", vbNewLine)

                rowNo += 1

            Next

            Dim encoutputfile As String = _outputFile
            'Save in Plain Format as well 
            If _outputFile.Length > objMasterTemplate.CustomerOutputExtension.Length Then
                _outputFile = _outputFile.Substring(0, _outputFile.Length - (objMasterTemplate.CustomerOutputExtension.Length + 1)) & "FYI." & objMasterTemplate.CustomerOutputExtension
            Else
                _outputFile = "FYI." & objMasterTemplate.CustomerOutputExtension
            End If

            SaveTextToFile(sText, _outputFile)

            'Encrypt with GnuPg 1.4.7 
            'Save the encrypted Copy
            If objMasterTemplate.EncryptCustomerOutputFile Then
                ' EncryptOmakase(GnuPg147, GnuPubKey, argInputFile, argOutputFile)
                EncryptOmakase(GnuPG147, _outputFile, encoutputfile)
            End If

            Return True

        Catch fex As FormatException
            Throw New MagicException(String.Format("Record: {0} Amount Field has invalid numeric value", rowNo))
        Catch ex As Exception
            Throw New Exception("Error on file generation: " & ex.Message.ToString)
        End Try

    End Function

    Private Function FormatAmount(ByVal amount As Decimal) As String

        Dim formattedAmount As String = String.Empty

        'Amount fields should not be formatted if there are more than 2 decimal places.
        'Total Amount
        'Transaction Amount
        'VAT Amount-P
        'Discount Amount
        'Invoice Amount-P
        'Invoice Amount-I
        'VAT Amount-I

        If amount.ToString().IndexOf(".") > 0 Then 'Should not format if  more than 2 decimal places
            If amount.ToString().Substring(amount.ToString().IndexOf(".") + 1).Length <= 2 Then
                formattedAmount = (amount).ToString("#0.00")
            Else
                formattedAmount = amount
            End If
        Else
            formattedAmount = (amount).ToString("#0.00")
        End If

        Return formattedAmount

    End Function
#End Region

#Region "Helper Methods"

    'Permitted Special Characters in iFTS-2 File Format despite Eng/Thai alphabets
    ' @	#	%	&	*	(	)	_	-	=	+	;	:	"	'	<
    '>	,	.	?	/	~	`	!	$	^	[	]	{	}	|	\
    Private Function IsValidiFTS2Text(ByVal textValue As String) As Boolean

        If IsNothingOrEmptyString(textValue) Then Return True

        textValue = textValue.Trim()

        Dim thaiEncoding As System.Text.Encoding = System.Text.Encoding.GetEncoding(874)
        '33 - 126 English chars: A-Za-z0-9 and special characters 
        '161 - 251 Thai Charas: Alphabets and Numbers
        For Each b As Byte In thaiEncoding.GetBytes(textValue)
            If (b > 32 AndAlso b < 127) OrElse (b > 160 AndAlso b < 252) Then
                'its english/thai alphabets/numbers/special chars
            Else
                ' non english/thai character
                Return False
            End If

        Next

        Return True

    End Function

    'Initialize Payment/Tax/Invoice Tables
    Private Sub SetupPTITables()

        _tblPayment = New DataTable("TPayment")
        _tblPayment.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsPaymentRecord
            _tblPayment.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

        _tblInvoice = New DataTable("TInvoice")
        _tblInvoice.Columns.Add(New DataColumn("ID", GetType(String)))
        For Each _column As String In _fieldsInvoiceRecord
            _tblInvoice.Columns.Add(New DataColumn(_column, GetType(String)))
        Next

    End Sub

    'Fork: Preview Table => Payment, Tax and Invoice Tables
    'PreviewTable is expected to have Payment, WHTax and Invoice Records on different rows as given below:
    ' P x x x x 
    '           I x x x
    Sub ForkPaymentRecords()

        '#1. Clear Payment Tables
        _tblPayment.Rows.Clear()
        _tblInvoice.Rows.Clear()

        '#2. Populate Payment Tables
        Dim id As Integer = 0
        Dim _NewRecord As DataRow

        For Each row As DataRow In _PreviewTable.Rows

            If row("File Type-P").ToString() = "P" Then

                id += 1
                _NewRecord = _tblPayment.NewRow()
                _NewRecord("ID") = id.ToString()
                For Each _column As DataColumn In _tblPayment.Columns
                    If _column.ColumnName = "ID" Then Continue For
                    _NewRecord(_column.ColumnName) = row(_column.ColumnName)
                Next
                _tblPayment.Rows.Add(_NewRecord)

            ElseIf row("Record Type-I").ToString() = "I" Then

                _NewRecord = _tblInvoice.NewRow()
                _NewRecord("ID") = id.ToString()
                For Each _column As DataColumn In _tblInvoice.Columns
                    If _column.ColumnName = "ID" Then Continue For
                    _NewRecord(_column.ColumnName) = row(_column.ColumnName)
                Next
                _tblInvoice.Rows.Add(_NewRecord)

            End If

        Next

    End Sub

    'Join: Payment, Tax and Invoice Tables => Preview Table
    Sub JoinPaymentRecords()

        '#1. Clear Preview Table
        _PreviewTable.Rows.Clear()

        '#2. Add Payment Tables to Temporary Buffer and Setup relationship among them
        Dim tmpDb As New DataSet()
        tmpDb.Tables.Add(_tblPayment.Copy())
        tmpDb.Tables.Add(_tblInvoice.Copy())

        Dim RPaymentInvoice As New DataRelation("PaymentInvoice" _
          , tmpDb.Tables("TPayment").Columns("ID") _
          , tmpDb.Tables("TInvoice").Columns("ID"))

        tmpDb.Relations.Add(RPaymentInvoice)

        '#3. Populate Preview Table
        Dim newrecord As DataRow
        Dim _cnt As Int32 = 0
        For Each row As DataRow In tmpDb.Tables("TPayment").Rows

            '#3.a. Add Payment Record
            newrecord = _PreviewTable.NewRow()
            For Each column As DataColumn In tmpDb.Tables("TPayment").Columns
                If column.ColumnName = "ID" Then Continue For
                newrecord(column.ColumnName) = row(column.ColumnName)
            Next
            _PreviewTable.Rows.Add(newrecord)

            _cnt = 0
            '#3.c. Add Invoice Record
            For Each invoicerow As DataRow In row.GetChildRows("PaymentInvoice")

                _cnt += 1
                newrecord = _PreviewTable.NewRow()

                For Each column As DataColumn In tmpDb.Tables("TInvoice").Columns

                    If column.ColumnName = "ID" Then Continue For
                    If "Seq. No.-I".Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase) Then
                        newrecord(column.ColumnName) = _cnt.ToString("0000#")
                    ElseIf "Payment ID".Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase) Then
                        newrecord(column.ColumnName) = String.Format("P{0}", row("Seq. No.-P"))
                    Else
                        newrecord(column.ColumnName) = invoicerow(column.ColumnName)
                    End If

                Next
                _PreviewTable.Rows.Add(newrecord)

            Next

        Next

        '#4.Clean up
        tmpDb.Dispose()

    End Sub

    'Identifies the given Row as Payment/WHTax/Invoice Record
    Sub IdentifyRecordType(ByVal row As DataRow, ByRef IsPRecord As Boolean _
                            , ByRef IsIRecord As Boolean)
        IsPRecord = False
        IsIRecord = False

        For Each _pField As String In _fieldsPaymentRecord

            If IsNothingOrEmptyString(row(_pField)) Then Continue For
            IsPRecord = True
            Exit For

        Next

        For Each _iField As String In _fieldsInvoiceRecord

            If IsNothingOrEmptyString(row(_iField)) Then Continue For
            IsIRecord = True
            Exit For

        Next

    End Sub

    'gpg --output c:\OMAKASE20100105133416.txt -er OmakaseIndia  c:\OMAKASE20100105133416FYI.txt
    Private Sub EncryptOmakase(ByVal GnuPg147 As String, ByVal argInputFile As String, _
                                ByVal argOutputFile As String _
                                )
        Dim homedirectory As String = GnuPg147.Replace("\gpg.exe", "")
        Dim argSign As String = " --output """ & argOutputFile & """ -er OmakaseIndia  """ & argInputFile & """"

        argSign = "--homedir """ & homedirectory & """" & argSign
        Dim proc As Process


        proc = Process.Start(GnuPg147, argSign)
        proc.WaitForExit()
        If proc.ExitCode <> 0 Then Throw New Exception("Failed to encrypt output file!")
        proc.Close()
        proc.Dispose()


    End Sub
#End Region

End Class