Imports System.IO
Imports System.ComponentModel
Imports System.Xml.Serialization
<Serializable()> _
Public Class Column
    '''Declare column properties and write/read XML
#Region "Class Variables Declaration"
    Private _curColName As String
    Private _curColNo As String
    Private _curColDataType As String
    Private _curDataFormat As String
    Private _curMultiLineEnd As String
    Private _curRepeat As String
    Private _curIDNo As String
    Private _curColLineNo As String
    Private _curStart As String
    Private _curColLength As String
    Private _curDelimColNo As String
    Private _curDelimiter As String
    Private _curTextQualifier As String
#End Region

#Region " Public Property "
    Public Property ColName() As String
        Get
            Return _curColName
        End Get
        Set(ByVal value As String)
            _curColName = value
        End Set
    End Property
    Public Property ColNo() As String
        Get
            Return _curColNo
        End Get
        Set(ByVal value As String)
            _curColNo = value
        End Set
    End Property
    Public Property ColDataType() As String
        Get
            Return _curColDataType
        End Get
        Set(ByVal value As String)
            _curColDataType = value
        End Set
    End Property
    Public Property DataFormat() As String
        Get
            Return _curDataFormat
        End Get
        Set(ByVal value As String)
            _curDataFormat = value
        End Set
    End Property
    Public Property MultiLineEnd() As String
        Get
            Return _curMultiLineEnd
        End Get
        Set(ByVal value As String)
            _curMultiLineEnd = value
        End Set
    End Property
    Public Property Repeat() As String
        Get
            Return _curRepeat
        End Get
        Set(ByVal value As String)
            _curRepeat = value
        End Set
    End Property
    Public Property IDNo() As String
        Get
            Return _curIDNo
        End Get
        Set(ByVal value As String)
            _curIDNo = value
        End Set
    End Property
    Public Property ColLineNo() As String
        Get
            Return _curColLineNo
        End Get
        Set(ByVal value As String)
            _curColLineNo = value
        End Set
    End Property
    Public Property Start() As String
        Get
            Return _curStart
        End Get
        Set(ByVal value As String)
            _curStart = value
        End Set
    End Property
    Public Property ColLength() As String
        Get
            Return _curColLength
        End Get
        Set(ByVal value As String)
            _curColLength = value
        End Set
    End Property
    Public Property DelimColNo() As String
        Get
            Return _curDelimColNo
        End Get
        Set(ByVal value As String)
            _curDelimColNo = value
        End Set
    End Property
    Public Property Delimiter() As String
        Get
            Return _curDelimiter
        End Get
        Set(ByVal value As String)
            _curDelimiter = value
        End Set
    End Property
    Public Property TextQualifier() As String
        Get
            Return _curTextQualifier
        End Get
        Set(ByVal value As String)
            _curTextQualifier = value
        End Set
    End Property
#End Region

#Region " Serialization - Read And Write XML"
    Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
        Dim returnValue As String
        reader.ReadStartElement(elementName)
        If (reader.HasValue) Then
            returnValue = reader.ReadContentAsString()
            reader.ReadEndElement()
            Return returnValue
        Else
            Return Nothing
        End If
    End Function
    Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
        writer.WriteStartElement(elementName)
        writer.WriteString(value)
        writer.WriteEndElement()
    End Sub
    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            reader.ReadStartElement()
            ColName = ReadXMLElement(reader, "ColName")
            ColNo = ReadXMLElement(reader, "ColNo")
            ColDataType = ReadXMLElement(reader, "ColDataType")
            DataFormat = ReadXMLElement(reader, "DataFormat")

            MultiLineEnd = ReadXMLElement(reader, "MultiLineEnd")
            Repeat = ReadXMLElement(reader, "Repeat")
            IDNo = ReadXMLElement(reader, "IDNo")
            ColLineNo = ReadXMLElement(reader, "ColLineNo")

            Start = ReadXMLElement(reader, "Start")
            ColLength = ReadXMLElement(reader, "ColLength")
            DelimColNo = ReadXMLElement(reader, "DelimColNo")
            Delimiter = ReadXMLElement(reader, "Delimiter")

            TextQualifier = ReadXMLElement(reader, "TextQualifier")

            reader.ReadEndElement()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        WriteXmlElement(writer, "ColName", ColName)
        WriteXmlElement(writer, "ColNo", ColNo)
        WriteXmlElement(writer, "ColDataType", ColDataType)
        WriteXmlElement(writer, "DataFormat", DataFormat)

        WriteXmlElement(writer, "MultiLineEnd", MultiLineEnd)
        WriteXmlElement(writer, "Repeat", Repeat)
        WriteXmlElement(writer, "IDNo", IDNo)
        WriteXmlElement(writer, "ColLineNo", ColLineNo)

        WriteXmlElement(writer, "Start", Start)
        WriteXmlElement(writer, "ColLength", ColLength)
        WriteXmlElement(writer, "DelimColNo", DelimColNo)
        WriteXmlElement(writer, "Delimiter", Delimiter)

        WriteXmlElement(writer, "TextQualifier", TextQualifier)
    End Sub
#End Region
End Class

'''Declare group of columns as collection
Partial Public Class ColumnCollection
    Inherits System.ComponentModel.BindingList(Of Column)
    Implements System.Xml.Serialization.IXmlSerializable

    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function

    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As Column
        reader.ReadStartElement()
        While reader.LocalName = "Column"
            child = New Column
            child.ReadXml(reader)
            Me.Add(child)
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub

    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As Column
        For Each child In Me
            writer.WriteStartElement("Column")
            child.WriteXml(writer)
            writer.WriteEndElement()
        Next
    End Sub
End Class
