Imports System.IO
Imports System.ComponentModel
Imports System.Xml.Serialization
<Serializable()> _
Public Class Identifier
    '''Declare identifier properties and write/read XML
#Region "Class Variables Declaration"
    Private _curIDNo As String
    Private _curIDString As String
    Private _curIDType As String
    Private _curLineOffset As String
#End Region

#Region "Public Property"
    Public Property IDNo() As String
        Get
            Return _curIDNo
        End Get
        Set(ByVal value As String)
            _curIDNo = value
        End Set
    End Property
    Public Property IDString() As String
        Get
            Return IIf(_curIDString Is Nothing, "",  _curIDString)
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                _curIDString = ""
            Else
                _curIDString = IIf(value.Trim = "", "", value)
            End If
        End Set
    End Property

    Public Property IDType() As String
        Get
            Return IIf(_curIDType Is Nothing, "D", _curIDType)
        End Get
        Set(ByVal value As String)
            _curIDType = value
        End Set
    End Property

    Public Property LineOffset() As String
        Get
            Return IIf(_curLineOffset Is Nothing, "0", _curLineOffset)
        End Get
        Set(ByVal value As String)
            _curLineOffset = value
        End Set
    End Property
#End Region

#Region "Serialization - Read And Write XML"
    Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
        Dim returnValue As String
        reader.ReadStartElement(elementName)
        If (reader.HasValue) Then
            returnValue = reader.ReadContentAsString()
            reader.ReadEndElement()
            Return returnValue
        Else
            Return Nothing
        End If
    End Function
    Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
        writer.WriteStartElement(elementName)
        writer.WriteString(value)
        writer.WriteEndElement()
    End Sub
    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            reader.ReadStartElement()
            IDNo = ReadXMLElement(reader, "IDNo")
            IDString = ReadXMLElement(reader, "IDString")
            IDType = ReadXMLElement(reader, "IDType")
            LineOffset = ReadXMLElement(reader, "LineOffset")
            reader.ReadEndElement()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        WriteXmlElement(writer, "IDNo", IDNo)
        WriteXmlElement(writer, "IDString", IDString)
        WriteXmlElement(writer, "IDType", IDType)
        WriteXmlElement(writer, "LineOffset", LineOffset)
    End Sub
#End Region
End Class

'''Declare group of identifiers as collection
Partial Public Class IdentifierCollection
    Inherits System.ComponentModel.BindingList(Of Identifier)
    Implements System.Xml.Serialization.IXmlSerializable

    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function

    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        Dim child As Identifier
        reader.ReadStartElement()
        While reader.LocalName = "Identifier"
            child = New Identifier
            child.ReadXml(reader)
            Me.Add(child)
        End While
        If Me.Count > 0 Then reader.ReadEndElement()
    End Sub

    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        Dim child As Identifier
        For Each child In Me
            writer.WriteStartElement("Identifier")
            child.WriteXml(writer)
            writer.WriteEndElement()
        Next
    End Sub
End Class
