Imports System.IO
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Text
Imports System.Text.RegularExpressions
Imports Excel = Microsoft.Office.Interop.excel


<Serializable()> _
Public Class UnstructuredFileConverter
    '''Declare unstructured file converter passed in parameters for output file and write/read XML
#Region "Class Variables Declaration"
    Private _curCreatedDate As String
    Private _curDefinitionFile As String
    Private _curDefinitionDesc As String
    Private _curSourceFileType As String

    Private _curSourceFile As String
    Private _curDelimiter As String
    Private _curTextQualifier As String
    Private _curImportFirstLine As String

    Private _IdentifierCollection As New IdentifierCollection
    Private _ColumnCollection As New ColumnCollection
#End Region

#Region "Public Property"
    Public Property CreatedDate() As String
        Get
            Return _curCreatedDate
        End Get
        Set(ByVal value As String)
            _curCreatedDate = value
        End Set
    End Property
    Public Property DefinitionFile() As String
        Get
            Return _curDefinitionFile
        End Get
        Set(ByVal value As String)
            _curDefinitionFile = value
        End Set
    End Property
    Public Property DefinitionDesc() As String
        Get
            Return _curDefinitionDesc
        End Get
        Set(ByVal value As String)
            _curDefinitionDesc = value
        End Set
    End Property
    Public Property SourceFileType() As String
        Get
            Return _curSourceFileType
        End Get
        Set(ByVal value As String)
            _curSourceFileType = value
        End Set
    End Property

    Public Property SourceFile() As String
        Get
            Return _curSourceFile
        End Get
        Set(ByVal value As String)
            _curSourceFile = value
        End Set
    End Property
    Public Property Delimiter() As String
        Get
            Return _curDelimiter
        End Get
        Set(ByVal value As String)
            _curDelimiter = value
        End Set
    End Property
    Public Property TextQualifier() As String
        Get
            Return _curTextQualifier
        End Get
        Set(ByVal value As String)
            _curTextQualifier = value
        End Set
    End Property
    Public Property ImportFirstLine() As String
        Get
            Return _curImportFirstLine
        End Get
        Set(ByVal value As String)
            _curImportFirstLine = value
        End Set
    End Property

    Public Property IdentifierCollection() As IdentifierCollection
        Get
            Return _IdentifierCollection
        End Get
        Set(ByVal value As IdentifierCollection)
            _IdentifierCollection = value
        End Set
    End Property
    Public Property ColumnCollection() As ColumnCollection
        Get
            Return _ColumnCollection
        End Get
        Set(ByVal value As ColumnCollection)
            _ColumnCollection = value
        End Set
    End Property
#End Region

#Region "Serialization - Read And Write XML"
    Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
        Dim returnValue As String
        reader.ReadStartElement(elementName)
        If (reader.HasValue) Then
            returnValue = reader.ReadContentAsString()
            reader.ReadEndElement()
            Return returnValue
        Else
            Return Nothing
        End If
    End Function
    Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
        writer.WriteStartElement(elementName)
        writer.WriteString(value)
        writer.WriteEndElement()
    End Sub
    Protected Sub ReadXml(ByVal reader As System.Xml.XmlReader)
        Try
            reader.ReadStartElement()
            CreatedDate = ReadXMLElement(reader, "CreatedDate")
            DefinitionFile = ReadXMLElement(reader, "DefinitionFile")
            DefinitionDesc = ReadXMLElement(reader, "DefinitionDesc")
            SourceFileType = ReadXMLElement(reader, "SourceFileType")

            SourceFile = ReadXMLElement(reader, "SourceFile")
            Delimiter = ReadXMLElement(reader, "Delimiter")
            TextQualifier = ReadXMLElement(reader, "TextQualifier")
            ImportFirstLine = ReadXMLElement(reader, "ImportFirstLine")

            IdentifierCollection.ReadXml(reader)
            ColumnCollection.ReadXml(reader)
            reader.ReadEndElement()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub WriteXml(ByVal writer As System.Xml.XmlWriter)
        WriteXmlElement(writer, "CreatedDate", CreatedDate)
        WriteXmlElement(writer, "DefinitionFile", DefinitionFile)
        WriteXmlElement(writer, "DefinitionDesc", DefinitionDesc)
        WriteXmlElement(writer, "SourceFileType", SourceFileType)

        WriteXmlElement(writer, "SourceFile", SourceFile)
        WriteXmlElement(writer, "Delimiter", Delimiter)
        WriteXmlElement(writer, "TextQualifier", TextQualifier)
        WriteXmlElement(writer, "ImportFirstLine", ImportFirstLine)
        IdentifierCollection.WriteXml(writer)
        ColumnCollection.WriteXml(writer)
    End Sub
#End Region

    Public Sub ApplyEdit()

    End Sub
    Public Sub BeginEdit()

    End Sub

    '''Create preview screen and output file
#Region "Preview And File Converter"
    Public Function Preview(ByVal dfilename As String, ByVal sourcefilename As String, ByRef errlog As String) As DataTable
        Dim xmlDataTable As New DataTable
        errlog = ""
        xmlDataTable = FileConverter(dfilename, sourcefilename, errlog)
        Return xmlDataTable
    End Function

    Public Function UnstructuredFileConverter(ByVal user As String, ByVal dfilename As String, ByVal sourcefilename As String, ByVal outputfilepath As String, ByVal fileformat As String, ByVal delimitertext As String, ByVal txtqualifier As String) As Boolean
        Dim xmlDataTable As New DataTable
        Dim errlog As String
        Dim createdfile As Boolean = False
        errlog = ""
        xmlDataTable = FileConverter(dfilename, sourcefilename, errlog)
        If errlog = "" Then
            'Create DataFile
            OutputfileCreation(dfilename, xmlDataTable, outputfilepath, fileformat, delimitertext, txtqualifier)
            createdfile = True
            'Write Activity Log
            BTMU.MAGIC.Common.HelperModule.Log_ConvertSourceFile(user, sourcefilename, outputfilepath, xmlDataTable.Rows.Count.ToString)
        End If
        Return createdfile
    End Function

    Private Sub OutputfileCreation(ByVal dfilename As String, ByVal convertedDataTable As DataTable, ByVal filepath As String, ByVal fileformat As String, ByVal delimitertext As String, ByVal txtqualifier As String)
        Dim oFile As FileStream
        Dim oStream As StreamWriter
        Dim exportstr As String = ""
        Dim excelDataFile As String = "" 'For Excel File
        Dim strBuilder As New StringBuilder() 'For Excel File
        Dim xmlHeaderDataTable As New DataTable
        Dim xmlIdDataTable As New DataTable
        Dim xmlColDataTable As New DataTable
        Dim duplicateColName As Boolean = False
        'added by fhk on 15-sep-2015, convert text file to UTF-8 with BOM format
        Dim utf8WithBom As New System.Text.UTF8Encoding(True)
        'end by fhk on 15-sep-2015
        'Read Defination File
        ReadDefinationFile(dfilename, xmlIdDataTable, xmlHeaderDataTable, xmlColDataTable, duplicateColName)
        Select Case fileformat
            Case "D" 'Delimited File
                oFile = New FileStream(filepath, FileMode.Create, FileAccess.Write)
                'edited by fhk on 15-sep-2015, convert text file to UTF-8 with BOM format
                'oStream = New StreamWriter(oFile)
                oStream = New StreamWriter(oFile, utf8WithBom)
                'end by fhk on 15-sep-2015
                exportstr = ""
                For Each colHeader As DataRow In xmlHeaderDataTable.Rows
                    Select Case txtqualifier
                        Case ("{" + Chr(34) + "}") 'Double Code
                            exportstr = exportstr + Chr(34) + colHeader("ColName").ToString.Replace(Chr(34), Chr(34) + Chr(34)) + Chr(34)
                        Case ("{" + Chr(39) + "}") 'Single Code
                            exportstr = exportstr + Chr(39) + colHeader("ColName").ToString.Replace(Chr(39), Chr(39) + Chr(39)) + Chr(39)
                        Case "{none}"
                            exportstr = exportstr + colHeader("ColName").ToString
                        Case Else
                            exportstr = exportstr + txtqualifier + colHeader("ColName").ToString.Replace(txtqualifier, txtqualifier + txtqualifier) + txtqualifier
                    End Select

                    Select Case delimitertext
                        Case "{tab}"
                            exportstr = exportstr + Chr(9)
                        Case "{space}"
                            exportstr = exportstr + " "
                        Case "{new line}"
                            exportstr = exportstr + Chr(10)
                        Case "{,}"
                            exportstr = exportstr + ","
                        Case "{none}"
                            exportstr = exportstr
                        Case Else
                            exportstr = exportstr + delimitertext
                    End Select
                Next
                oStream.WriteLine(exportstr)
                For r As Integer = 0 To convertedDataTable.Rows.Count - 1
                    exportstr = ""
                    For c As Integer = 1 To convertedDataTable.Columns.Count - 1
                        Select Case txtqualifier
                            Case ("{" + Chr(34) + "}") 'Double Code
                                exportstr = exportstr + Chr(34) + convertedDataTable.Rows(r).Item(c).ToString.Replace(Chr(34), Chr(34) + Chr(34)) + Chr(34)
                            Case ("{" + Chr(39) + "}") 'Single Code
                                exportstr = exportstr + Chr(39) + convertedDataTable.Rows(r).Item(c).ToString.Replace(Chr(39), Chr(39) + Chr(39)) + Chr(39)
                            Case "{none}"
                                exportstr = exportstr + convertedDataTable.Rows(r).Item(c).ToString
                            Case Else
                                exportstr = exportstr + txtqualifier + convertedDataTable.Rows(r).Item(c).ToString.Replace(txtqualifier, txtqualifier + txtqualifier) + txtqualifier
                        End Select

                        Select Case delimitertext
                            Case "{tab}"
                                exportstr = exportstr + Chr(9)
                            Case "{space}"
                                exportstr = exportstr + " "
                            Case "{new line}"
                                exportstr = exportstr + Chr(10)
                            Case "{,}"
                                exportstr = exportstr + ","
                            Case "{none}"
                                exportstr = exportstr
                            Case Else
                                exportstr = exportstr + delimitertext
                        End Select

                    Next
                    oStream.WriteLine(exportstr)
                Next
                oStream.Flush()
                oStream.Close()
            Case "E"
                strBuilder.Append("<html>")
                strBuilder.Append("<head>")
                strBuilder.Append("<title>")
                strBuilder.Append("Export To Excel")
                strBuilder.Append("</title>")
                strBuilder.Append("</head>")
                strBuilder.Append("<body>")
                strBuilder.Append("<table cellpadding='5' cellspacing='0'>")
                strBuilder.Append("<tr align='left' valign='top'>")
                For Each colHeader As DataRow In xmlHeaderDataTable.Rows
                    exportstr = ""
                    Select Case txtqualifier
                        Case ("{" + Chr(34) + "}") 'Double Code
                            exportstr = exportstr + Chr(34) + colHeader("ColName").ToString + Chr(34)
                        Case ("{" + Chr(39) + "}") 'Single Code
                            exportstr = exportstr + Chr(39) + colHeader("ColName").ToString + Chr(39)
                        Case "{none}"
                            exportstr = exportstr + colHeader("ColName").ToString
                        Case Else
                            exportstr = exportstr + txtqualifier + colHeader("ColName").ToString + txtqualifier
                    End Select

                    Select Case delimitertext
                        Case "{tab}"
                            exportstr = exportstr + Chr(9)
                        Case "{space}"
                            exportstr = exportstr + " "
                        Case "{new line}"
                            exportstr = exportstr + Chr(10)
                        Case "{,}"
                            exportstr = exportstr + ","
                        Case "{none}"
                            exportstr = exportstr
                        Case Else
                            exportstr = exportstr + delimitertext
                    End Select
                    strBuilder.Append("<td align='left' valign='top'>")
                    strBuilder.Append(exportstr)
                    strBuilder.Append("</td>")
                Next
                strBuilder.Append("</tr>")

                For r As Integer = 0 To convertedDataTable.Rows.Count - 1
                    exportstr = ""
                    strBuilder.Append("<tr align='left' valign='top'>")
                    For c As Integer = 1 To convertedDataTable.Columns.Count - 1
                        exportstr = ""
                        Select Case txtqualifier
                            Case ("{" + Chr(34) + "}") 'Double Code
                                exportstr = exportstr + Chr(34) + convertedDataTable.Rows(r).Item(c).ToString + Chr(34)
                            Case ("{" + Chr(39) + "}") 'Single Code
                                exportstr = exportstr + Chr(39) + convertedDataTable.Rows(r).Item(c).ToString + Chr(39)
                            Case "{none}"
                                exportstr = exportstr + convertedDataTable.Rows(r).Item(c).ToString
                            Case Else
                                exportstr = exportstr + txtqualifier + convertedDataTable.Rows(r).Item(c).ToString + txtqualifier
                        End Select

                        Select Case delimitertext
                            Case "{tab}"
                                exportstr = exportstr + Chr(9)
                            Case "{space}"
                                exportstr = exportstr + " "
                            Case "{new line}"
                                exportstr = exportstr + Chr(10)
                            Case "{,}"
                                exportstr = exportstr + ","
                            Case "{none}"
                                exportstr = exportstr
                            Case Else
                                exportstr = exportstr + delimitertext
                        End Select
                        strBuilder.Append("<td align='left' valign='top'>")
                        strBuilder.Append(exportstr)
                        strBuilder.Append("</td>")
                    Next
                    strBuilder.Append("</tr>")
                Next
                'Close tags. 
                strBuilder.Append("</table>")
                strBuilder.Append("</body>")
                strBuilder.Append("</html>")
                'Get the string 
                excelDataFile = strBuilder.ToString()
                oFile = New FileStream(filepath, FileMode.Create, FileAccess.Write)
                'edited by fhk on 15-sep-2015, convert text file to UTF-8 with BOM format
                'oStream = New StreamWriter(oFile)
                oStream = New StreamWriter(oFile, utf8WithBom)
                'end by fhk on 15-sep-2015
                oStream.WriteLine(excelDataFile)
                oStream.Flush()
                oStream.Close()
        End Select

    End Sub

    Private Function FileConverter(ByVal dfilename As String, ByVal sourcefilename As String, ByRef errlog As String) As DataTable
        Dim textdatafile As New RichTextBox
        Dim oRead As System.IO.StreamReader
        oRead = File.OpenText(sourcefilename)
        textdatafile.Text = oRead.ReadToEnd()
        textdatafile.SelectAll()
        textdatafile.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        'Variable declaration to Read XML Defination File 
        Dim xmlHeaderDataTable As New DataTable
        Dim xmlIdDataTable As New DataTable
        Dim xmlColDataTable As New DataTable
        Dim xmlDataTable As New DataTable
        Dim sourceFileType As String = ""
        Dim excelErrlog As String = ""
        Dim excelDataTable As DataTable
        Dim duplicateColName As Boolean = False
        'Read Defination File
        ReadDefinationFile(dfilename, xmlIdDataTable, xmlHeaderDataTable, xmlColDataTable, duplicateColName)
        xmlDataTable = New DataTable
        'Add Header Columns
        If xmlHeaderDataTable.Rows.Count = 0 Then 'No column is not specified
            errlog = "No column is not defined." + Chr(13) + Chr(10)
            Return xmlDataTable
            Exit Function
        End If
        xmlDataTable.Columns.Add("Line No")
        For Each colHeader As DataRow In xmlHeaderDataTable.Rows
            xmlDataTable.Columns.Add(colHeader("ColName"))
        Next

        sourceFileType = Me.SourceFileType
        Select Case sourceFileType
            Case "F" 'Fixed With
                ReadSourceFile(sourcefilename, textdatafile)
                FixedWidthFileConverter(textdatafile, xmlIdDataTable, xmlColDataTable, xmlDataTable, errlog, duplicateColName)
            Case "D" 'Delimited File
                ReadSourceFile(sourcefilename, textdatafile)
                excelDataTable = Nothing
                DelimitedAndExcelFileConverter(textdatafile, excelDataTable, xmlIdDataTable, xmlColDataTable, xmlDataTable, errlog)
            Case "E" 'Excel     
                'excelDataTable = ReadWorkSheet(sourcefilename, excelErrlog)
                excelDataTable = ReadWorkSheet2(sourcefilename, excelErrlog)
                errlog = errlog + excelErrlog
                textdatafile = Nothing
                DelimitedAndExcelFileConverter(textdatafile, excelDataTable, xmlIdDataTable, xmlColDataTable, xmlDataTable, errlog)
        End Select
        Return xmlDataTable
    End Function
    'Private Function ReadWorkSheet(ByVal sourcefilename As String, ByRef errlog As String) As DataTable
    '    Dim connectionString As String
    '    Dim conn As OleDbConnection
    '    Dim command As OleDbCommand
    '    Dim dataAdapter As OleDbDataAdapter
    '    Dim excelDataTable As New DataTable
    '    Dim sheetName As String = ""
    '    excelDataTable = New DataTable
    '    connectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", sourcefilename)
    '    conn = New OleDbConnection(connectionString)
    '    Try
    '        conn.Open()
    '    Catch ex As Exception
    '        errlog = errlog + "Invalid source file." + Chr(13) + Chr(10)
    '        excelDataTable = Nothing
    '        Return excelDataTable
    '        Exit Function
    '    End Try

    '    Try
    '        sheetName = Me.Delimiter 'Excel Sheet Name
    '        Try
    '            command = New OleDbCommand(String.Format("Select * From [{0}]", sheetName & "$"), conn)
    '            dataAdapter = New OleDbDataAdapter(command)
    '            dataAdapter.Fill(excelDataTable)
    '        Catch ex As Exception
    '            command = New OleDbCommand(String.Format("Select * From ['{0}']", sheetName & "$"), conn)
    '            dataAdapter = New OleDbDataAdapter(command)
    '            dataAdapter.Fill(excelDataTable)
    '        End Try
    '    Catch ex As Exception
    '        errlog = errlog + ex.Message.ToString + Chr(13) + Chr(10)
    '        MessageBox.Show(ex.Message.ToString)
    '    Finally
    '        If conn.State = ConnectionState.Open Then
    '            conn.Close()
    '        End If
    '        conn = Nothing
    '    End Try
    '    Return excelDataTable

    'End Function

    Private Function ReadWorkSheet2(ByVal sourcefilename As String, ByRef errlog As String) As DataTable
        Dim excelDataTable As New DataTable
        Dim sheetName As String = ""

        Dim originalCultureInfo As Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Dim enCulture As New Globalization.CultureInfo("en-US")
        System.Threading.Thread.CurrentThread.CurrentCulture = enCulture

        Try

            Try
                sheetName = Me.Delimiter 'Excel Sheet Name
            Catch ex As Exception
                errlog = errlog + ex.Message.ToString + Chr(13) + Chr(10)
                MessageBox.Show(ex.Message.ToString)
            Finally
            End Try




            Dim XlApp As New Excel.Application

            Dim XlWorkBook As Excel.Workbook

            Dim XlSheet As Excel.Sheets

            Dim XlWorkSheet As Excel.Worksheet


            'CurrentFilePath stores the selected Excel Workbook file path
            Try
                XlWorkBook = XlApp.Workbooks.Open(sourcefilename)
            Catch ex As Exception
                errlog = errlog + "Invalid source file." + Chr(13) + Chr(10)
                excelDataTable = Nothing
                Return excelDataTable
                Exit Function
            End Try

            XlSheet = XlWorkBook.Worksheets

            Dim xlSheetIndex As Integer
            Dim xlIntRows As Integer
            Dim xlIntCols As Integer

            XlWorkBook = XlApp.Workbooks.Open(sourcefilename)

            XlSheet = XlWorkBook.Worksheets

            XlWorkSheet = CType(XlSheet(sheetName), Excel.Worksheet)

            xlSheetIndex = xlSheetIndex + 1

            xlIntRows = XlWorkSheet.UsedRange.Rows.Count

            xlIntCols = XlWorkSheet.UsedRange.Columns.Count


            'Fetch values from the selected excel sheet

            Dim XlObj As Excel.Range

            Dim XlArray() As String

            ReDim XlArray(xlIntCols - 1)

            Dim TempCnt1 As Integer

            Dim TempCnt2 As Integer

            For TempCnt1 = 0 To xlIntCols - 1
                excelDataTable.Columns.Add("Field" & (TempCnt1 + 1).ToString)
            Next

            For TempCnt1 = 1 To xlIntRows

                For TempCnt2 = 1 To xlIntCols

                    XlObj = CType(XlWorkSheet.Cells(TempCnt1, TempCnt2), Excel.Range)

                    XlArray(TempCnt2 - 1) = XlObj.Value

                Next

                excelDataTable.Rows.Add(XlArray)

            Next

            XlWorkBook.Close()
            XlApp.Quit()
            Return excelDataTable

        Finally

            System.Threading.Thread.CurrentThread.CurrentCulture = originalCultureInfo
        End Try
      
    End Function
    Private Function ReadWorkSheet3(ByVal sourcefilename As String, ByRef errlog As String) As DataTable
        Dim excelDataTable As New DataTable
        Dim sheetName As String = ""

        Try
            sheetName = Me.Delimiter 'Excel Sheet Name
        Catch ex As Exception
            errlog = errlog + ex.Message.ToString + Chr(13) + Chr(10)
            MessageBox.Show(ex.Message.ToString)
        Finally
        End Try

        Dim result As Boolean = False
        Dim ExcelConncetion As OleDbConnection
        Dim ExcelConncetionString As String
        ExcelConncetionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", sourcefilename)
        ExcelConncetion = New OleDbConnection(ExcelConncetionString)
        Try
            'excelDataTable = ExcelReaderInterop.ExcelOpenSpreadsheets(sourcefilename, sheetName)
        Catch ex As Exception
            result = False
        Finally
            If ExcelConncetion.State = ConnectionState.Open Then
                ExcelConncetion.Close()
            End If
        End Try
        Return excelDataTable

    End Function
    Private Sub DelimitedAndExcelFileConverter(ByVal textdatafile As RichTextBox, ByVal excelDataTable As DataTable, ByVal xmlIdDataTable As DataTable, ByVal xmlColDataTable As DataTable, ByRef xmlDataTable As DataTable, ByRef errlog As String)
        'Conversion Column Detail
        'Defination File
        Dim def_Delimiter As String = ""
        Dim def_TextQualifier As String = ""
        Dim def_ImportFirstLine As String = ""
        Dim def_SourceFileType As String = ""
        'Colums Fields
        Dim colName As String = ""
        Dim colNo As String = ""
        Dim colDataType As String = ""
        Dim dataFormat As String = ""
        Dim multiLineEnd As String = ""
        Dim repeat As String = ""
        Dim idNo As String = ""
        Dim colLineNo As String = ""
        Dim start As Integer = 0
        Dim colLength As Integer = 0
        Dim delimColNo As String = ""
        Dim delimiter As String = ""
        Dim textQualifier As String = ""
        'Identifier
        Dim hdridNo As String = ""
        Dim tmphdridNo As String = ""
        Dim idString As String = ""
        Dim tmpidString As String = ""
        Dim idType As String = ""
        Dim tmpidType As String = ""
        Dim lineOffset As String = ""
        Dim tmplineOffset As String = ""
        'Conversion Variables
        Dim dataValue As String = ""
        Dim tmpdataValue As String = ""
        Dim xmlDataRow As DataRow
        Dim tmpErrorLog As String = ""
        Dim delimiterstr As String()
        Dim delimiterstr_ex As String()
        Dim textdata As String = ""
        Dim tmpxmlDataTable As DataTable
        Dim startReadLineNo As Integer = 0
        Dim rowAdded As Boolean = False 'For Record Adding
        Dim idhdrCount As Integer = 0
        Dim iddtlCount As Integer = 0
        Dim datafound As Boolean = False
        Dim checkcountId As Integer = 0
        'Read Text File To Tmp Table
        Dim tmpDataTable As New DataTable 'Source File DataTable
        Dim tmpDataRow As DataRow 'Source File DataRow
        Dim tmpstring As String = ""
        Dim tmpstring_ex As String = ""
        Dim rowNo As Integer = 0
        Dim idstringFound As Boolean = False

        'Read Defination File         
        def_ImportFirstLine = Me.ImportFirstLine
        def_SourceFileType = Me.SourceFileType

        'Insert into TmpDataTable From Source File
        tmpDataTable.Columns.Add("TextData")
        Select Case def_SourceFileType
            Case "D"
                For Each txtLine As String In textdatafile.Lines
                    tmpDataRow = tmpDataTable.NewRow
                    tmpDataRow(0) = txtLine.ToString
                    tmpDataTable.Rows.Add(tmpDataRow)
                Next
            Case "E"
                For r As Integer = 0 To excelDataTable.Rows.Count - 1
                    tmpDataRow = tmpDataTable.NewRow
                    tmpstring = ""
                    For c As Integer = 0 To excelDataTable.Columns.Count - 1
                        tmpstring = tmpstring + excelDataTable.Rows(r).Item(c).ToString + Chr(9)
                    Next
                    tmpDataRow(0) = tmpstring
                    tmpDataTable.Rows.Add(tmpDataRow)
                Next
        End Select



        'For Record Conversion   
        rowNo = -1
        tmpxmlDataTable = xmlDataTable 'For ID Checking
        startReadLineNo = 0
        If def_ImportFirstLine = "N" Then
            startReadLineNo = 1
            rowNo = 0
        End If

        For x As Integer = 0 To xmlIdDataTable.Rows.Count - 1
            idType = xmlIdDataTable.Rows(x).Item("IDType").ToString
            If idType = "D" Then
                iddtlCount = iddtlCount + 1
            Else
                idhdrCount = idhdrCount + 1
            End If
        Next

        rowAdded = False
        For t As Integer = startReadLineNo To tmpDataTable.Rows.Count - 1
            rowNo = rowNo + 1
            If rowNo = tmpDataTable.Rows.Count Then
                Exit For
            End If
            idstringFound = False
            tmpstring = tmpDataTable.Rows(rowNo).Item(0).ToString

            If def_SourceFileType = "E" Then
                def_Delimiter = GetDelimiterCode("{tab}") 'Assign
                def_TextQualifier = GetTextQualifierCode("{none}") 'Assign
            Else
                def_Delimiter = GetDelimiterCode(Me.Delimiter)
                def_TextQualifier = GetTextQualifierCode(Me.TextQualifier)
            End If
            delimiterstr = SplitString(tmpstring, def_Delimiter, def_TextQualifier, True)
            If tmpErrorLog <> "" Then
                errlog = errlog + "Source File Line No " + (rowNo + 1).ToString + tmpErrorLog
            End If

            If delimiterstr.Length = 0 Then
                idstringFound = False
            Else
                If xmlIdDataTable.Rows.Count >= 1 Then
                    'Header And Record
                    checkcountId = -1
                    For x As Integer = 0 To xmlIdDataTable.Rows.Count - 1
                        checkcountId = checkcountId + 1
                        hdridNo = xmlIdDataTable.Rows(x).Item("IDNo").ToString
                        idString = xmlIdDataTable.Rows(x).Item("IDString").ToString
                        idType = xmlIdDataTable.Rows(x).Item("IDType").ToString
                        lineOffset = xmlIdDataTable.Rows(x).Item("LineOffset").ToString
                        If lineOffset = 0 Then
                            idstringFound = True
                            Exit For
                        Else
                            Try
                                If delimiterstr.Length > (lineOffset - 1) Then
                                    dataValue = delimiterstr(lineOffset - 1) 'Search Chars
                                Else
                                    dataValue = ""
                                End If

                            Catch ex As Exception
                                errlog = errlog + "Source File Line No " + (rowNo + 1).ToString + ":" + ex.Message.ToString + Chr(13) + Chr(10)
                            End Try
                            idstringFound = CheckSourceFileWithIdentifierType(idString, dataValue)
                            If idstringFound = True Then
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If

            If idstringFound = True Then
                If rowAdded = False Then
                    If idType = "D" And xmlIdDataTable.Rows.Count >= 1 Then
                        xmlDataRow = xmlDataTable.NewRow
                        rowAdded = True
                    ElseIf idType = "H" And xmlIdDataTable.Rows.Count >= 1 Then
                        xmlDataRow = xmlDataTable.NewRow
                        rowAdded = True
                    End If
                End If

                'Insert data to datatable
                datafound = False
                For Each xmlcol As DataRow In xmlColDataTable.Rows
                    colName = xmlcol("ColName")
                    colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                    colDataType = xmlcol("ColDataType")
                    dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                    multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                    repeat = xmlcol("Repeat")
                    idNo = xmlcol("IDNo")
                    If xmlcol("ColLineNo") Is DBNull.Value Then
                        colLineNo = 0 '""
                    Else
                        colLineNo = xmlcol("ColLineNo")
                    End If
                    If xmlcol("Start") Is DBNull.Value Then
                        start = 0
                    Else
                        start = CType(xmlcol("Start").ToString, Integer)
                    End If
                    If xmlcol("ColLength") Is DBNull.Value Then
                        colLength = 0
                    Else
                        colLength = xmlcol("ColLength")
                    End If
                    delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                    delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                    textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))
                    If hdridNo = idNo Then
                        '        Try
                        '            dataValue = delimiterstr(colNo - 1)
                        '        Catch ex As Exception
                        '            dataValue = ""
                        '        End Try
                       
                        'Updated By Sandar 28 August 09
                        If colLineNo > 0 Then
                            tmpstring_ex = tmpDataTable.Rows(rowNo + colLineNo).Item(0).ToString
                            delimiterstr_ex = SplitString(tmpstring_ex, def_Delimiter, def_TextQualifier, True)
                            Try
                                dataValue = delimiterstr_ex(colNo - 1)
                            Catch ex As Exception
                                dataValue = ""
                            End Try
                        ElseIf colLineNo = 0 Then
                            Try
                                dataValue = delimiterstr(colNo - 1)
                            Catch ex As Exception
                                dataValue = ""
                            End Try
                        End If

                        If dataValue = Nothing Then
                            If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                            End If
                        End If

                        If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                            'Copy last imported data
                            If idType <> "T" Then 'Updated on 08 Sept 09
                                dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                            End If

                        End If

                        Select Case colDataType
                            Case "T"
                                If dataValue = Nothing Then
                                    dataValue = ""
                                Else
                                    If dataFormat = "Trim" Then
                                        dataValue = dataValue.ToString.Trim
                                    Else
                                        dataValue = dataValue.ToString
                                    End If
                                End If

                            Case "N"
                                If IsNumeric(dataValue) = True Then
                                    If dataFormat = "," Then
                                        dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                    Else
                                        dataValue = CType(dataValue.ToString.Trim, Double)
                                    End If

                                Else
                                    errlog = errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                    dataValue = dataValue.ToString
                                End If
                        End Select
                        datafound = True
                        xmlDataRow(colName) = dataValue
                        If xmlDataTable.Rows.Count = 0 Then
                            xmlDataRow("Line No") = 1
                        Else
                            If rowAdded = True Then
                                xmlDataRow("Line No") = xmlDataTable.Rows.Count + 1
                            Else
                                xmlDataRow("Line No") = xmlDataTable.Rows.Count
                            End If
                        End If
                    Else
                        'Same Line  and more than one header
                        'Check Header
                        '------------------------------start -----------------------------
                        If xmlIdDataTable.Rows.Count >= 1 And checkcountId < xmlIdDataTable.Rows.Count - 1 Then
                            'Header And Record
                            For x As Integer = checkcountId + 1 To xmlIdDataTable.Rows.Count - 1
                                tmphdridNo = xmlIdDataTable.Rows(x).Item("IDNo").ToString
                                tmpidString = xmlIdDataTable.Rows(x).Item("IDString").ToString
                                tmpidType = xmlIdDataTable.Rows(x).Item("IDType").ToString
                                tmplineOffset = xmlIdDataTable.Rows(x).Item("LineOffset").ToString
                                If tmplineOffset = 0 And tmphdridNo <> hdridNo And tmpidType = "H" Then
                                    idstringFound = True
                                    Exit For
                                ElseIf tmphdridNo <> hdridNo And tmpidType = "H" Then
                                    Try
                                        If delimiterstr.Length > (tmplineOffset - 1) Then
                                            tmpdataValue = delimiterstr(tmplineOffset - 1) 'Search Chars
                                        Else
                                            tmpdataValue = ""
                                        End If

                                    Catch ex As Exception
                                        errlog = errlog + "Source File Line No " + (rowNo + 1).ToString + ":" + ex.Message.ToString + Chr(13) + Chr(10)
                                    End Try
                                    idstringFound = CheckSourceFileWithIdentifierType(tmpidString, tmpdataValue)
                                    If idstringFound = True Then
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                        If idstringFound = True Then
                            datafound = False
                            For Each xmlcol2 As DataRow In xmlColDataTable.Rows
                                colName = xmlcol2("ColName")
                                colNo = IIf(xmlcol2("ColNo") Is DBNull.Value, "", xmlcol2("ColNo"))
                                colDataType = xmlcol2("ColDataType")
                                dataFormat = IIf(xmlcol2("DataFormat") Is DBNull.Value, "", xmlcol2("DataFormat"))
                                multiLineEnd = IIf(xmlcol2("MultiLineEnd") Is DBNull.Value, "", xmlcol2("MultiLineEnd"))
                                repeat = xmlcol2("Repeat")
                                idNo = xmlcol2("IDNo")
                                If xmlcol2("ColLineNo") Is DBNull.Value Then
                                    colLineNo = 0 '""
                                Else
                                    colLineNo = xmlcol2("ColLineNo")
                                End If
                                If xmlcol2("Start") Is DBNull.Value Then
                                    start = 0
                                Else
                                    start = CType(xmlcol2("Start").ToString, Integer)
                                End If
                                If xmlcol2("ColLength") Is DBNull.Value Then
                                    colLength = 0
                                Else
                                    colLength = xmlcol2("ColLength")
                                End If
                                delimColNo = IIf(xmlcol2("DelimColNo") Is DBNull.Value, "", xmlcol2("DelimColNo"))
                                delimiter = IIf(xmlcol2("Delimiter") Is DBNull.Value, "", xmlcol2("Delimiter"))
                                textQualifier = IIf(xmlcol2("TextQualifier") Is DBNull.Value, "", xmlcol2("TextQualifier"))
                                If tmphdridNo = idNo Then
                                    'Updated By Sandar 28 August 09
                                    If colLineNo > 0 Then
                                        tmpstring_ex = tmpDataTable.Rows(rowNo + colLineNo).Item(0).ToString
                                        delimiterstr_ex = SplitString(tmpstring_ex, def_Delimiter, def_TextQualifier, True)
                                        Try
                                            dataValue = delimiterstr_ex(colNo - 1)
                                        Catch ex As Exception
                                            dataValue = ""
                                        End Try
                                    ElseIf colLineNo = 0 Then
                                        Try
                                            dataValue = delimiterstr(colNo - 1)
                                        Catch ex As Exception
                                            dataValue = ""
                                        End Try
                                    End If

                                    If dataValue = Nothing Then
                                        If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                            dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                        End If
                                    End If

                                    If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                        'Copy last imported data
                                        dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                    End If

                                    Select Case colDataType
                                        Case "T"
                                            If dataValue = Nothing Then
                                                dataValue = ""
                                            Else
                                                If dataFormat = "Trim" Then
                                                    dataValue = dataValue.ToString.Trim
                                                Else
                                                    dataValue = dataValue.ToString
                                                End If
                                            End If

                                        Case "N"
                                            If IsNumeric(dataValue) = True Then
                                                If dataFormat = "," Then
                                                    dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                                Else
                                                    dataValue = CType(dataValue.ToString.Trim, Double)
                                                End If

                                            Else
                                                errlog = errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                                dataValue = dataValue.ToString
                                            End If
                                    End Select
                                    datafound = True
                                    xmlDataRow(colName) = dataValue
                                End If
                            Next
                        End If
                        '------------------END-------------------------------------
                    End If 'Check ID
                Next

                If datafound = True Then 'one or more data found in source file

                    If rowAdded = True And idType = "D" Then
                        xmlDataTable.Rows.Add(xmlDataRow)
                        rowAdded = False
                    Else
                        If rowAdded = True And idType = "H" Then
                            If iddtlCount = 0 And idhdrCount > 0 Then
                                xmlDataTable.Rows.Add(xmlDataRow)
                                rowAdded = False
                            Else
                                rowAdded = True
                            End If
                        End If
                    End If

                End If

            End If 'Check ID String  Condition
        Next 'tmp Data Table
        'Check Header Data For Repeat
        If idhdrCount > 0 Then
            For r As Integer = 1 To xmlDataTable.Rows.Count - 1
                For c As Integer = 0 To xmlDataTable.Columns.Count - 1
                    repeat = 0
                    For Each xmlcol As DataRow In xmlColDataTable.Rows
                        colName = xmlcol("ColName")
                        idNo = xmlcol("IDNo")
                        datafound = False
                        For x As Integer = 0 To xmlIdDataTable.Rows.Count - 1
                            idType = xmlIdDataTable.Rows(x).Item("IDType").ToString
                            hdridNo = xmlIdDataTable.Rows(x).Item("IDNo").ToString
                            If idType = "H" And hdridNo = idNo And xmlDataTable.Columns(c).ColumnName.ToString.Trim = colName Then
                                repeat = xmlcol("Repeat")
                                If repeat >= 1 Then
                                    Select Case repeat
                                        Case 1
                                            If xmlDataTable.Rows(r).Item(c).ToString = "" Then
                                                xmlDataTable.Rows(r).Item(c) = xmlDataTable.Rows(r - 1).Item(c).ToString()
                                            End If
                                        Case 2
                                            xmlDataTable.Rows(r).Item(c) = xmlDataTable.Rows(r - 1).Item(c).ToString()
                                    End Select
                                End If
                            ElseIf idType = "T" And hdridNo = idNo And xmlDataTable.Columns(c).ColumnName.ToString.Trim = colName Then
                                repeat = xmlcol("Repeat")
                                If repeat >= 1 Then
                                    Select Case repeat
                                        Case 1, 2
                                            If xmlDataTable.Rows(r).Item(c).ToString <> "" Then
                                                For t2 As Integer = r - 1 To 0 Step -1
                                                    If xmlDataTable.Rows(t2).Item(c).ToString = "" Then
                                                        xmlDataTable.Rows(t2).Item(c) = xmlDataTable.Rows(r).Item(c).ToString()
                                                    Else
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                            
                                    End Select
                                End If
                            End If

                        Next
                    Next 'xmlcol
                Next 'column
            Next 'row          

        End If
        'Conversion End
    End Sub
    Private Sub GetDuplicateData( _
    ByVal columnno As Integer, _
    ByVal hdridno As String, _
    ByVal xmlColDataTable As DataTable, _
    ByVal tmpstring As String, _
    ByVal xmlDataTable As DataTable, _
    ByVal idString As String, _
    ByVal xmlIdDataTable As DataTable, _
    ByVal startLineNo As Integer, _
    ByVal endLineNo As Integer, _
    ByVal textdataString() As String, _
    ByRef xmlDataRow As DataRow, _
    ByRef Errlog As String)

        Dim datavalue As String = ""
        Dim colcount As Integer = -1
        'Colums Fields
        Dim colName As String = ""
        Dim colNo As String = ""
        Dim colDataType As String = ""
        Dim dataFormat As String = ""
        Dim multiLineEnd As String = ""
        Dim repeat As String = ""
        Dim idNo As String = ""
        Dim colLineNo As String = ""
        Dim start As Integer = 0
        Dim colLength As Integer = 0
        Dim delimColNo As String = ""
        Dim delimiter As String = ""
        Dim textQualifier As String = ""

        Dim def_Delimiter As String = ""
        Dim def_TextQualifier As String = ""
        Dim delimiterStr As String()
        For Each xmlcol As DataRow In xmlColDataTable.Rows
            colcount = colcount + 1
            idNo = xmlcol("IDNo")
            'If colcount > columnno And hdridno = idNo Then
            If colcount <> columnno And hdridno = idNo Then 'Updated By Sandar 27 August 09
                colName = xmlcol("ColName")
                colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                colDataType = xmlcol("ColDataType")
                dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                repeat = xmlcol("Repeat")
                idNo = xmlcol("IDNo")
                colLineNo = xmlcol("ColLineNo")
                start = CType(xmlcol("Start").ToString, Integer)
                colLength = xmlcol("ColLength")
                delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))

                Try
                    If start + colLength > tmpstring.Length Then
                        datavalue = tmpstring.Substring(start, tmpstring.Length - start)
                    Else
                        datavalue = textdataString(startLineNo).Substring(start, colLength)
                    End If
                    If datavalue.ToString.Trim = "" Then
                        If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                            datavalue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                        End If
                    End If

                    If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                        'Copy last imported data
                        datavalue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                    End If

                Catch ex As Exception
                    datavalue = ""
                End Try
                'Check Delimiter
                If delimiter <> "" Then
                    If datavalue <> "" Then 'Added By Sandar 31 Aug 09
                        def_Delimiter = GetDelimiterCode(delimiter)
                        def_TextQualifier = GetTextQualifierCode(textQualifier)
                        delimiterStr = SplitString(datavalue, def_Delimiter, def_TextQualifier, True)
                        Try 'Added By Sandar 31 Aug 09
                            datavalue = delimiterStr(delimColNo - 1)
                        Catch ex As Exception
                            datavalue = ""
                        End Try

                    End If
                End If

                Select Case colDataType
                    Case "T"
                        If dataFormat = "Trim" Or dataFormat = "Line" Then
                            datavalue = datavalue.ToString.Trim
                        Else
                            datavalue = datavalue.ToString
                        End If

                    Case "N"
                        If IsNumeric(datavalue) = True Then
                            If dataFormat = "," Then
                                datavalue = Format(CType(datavalue.ToString.Trim, Double), "###,##0.00")
                            Else
                                datavalue = CType(datavalue.ToString.Trim, Double)
                            End If
                        Else
                            Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                            datavalue = datavalue.ToString
                        End If
                    Case "M"
                        datavalue = CheckMultilineEnd(idString, xmlIdDataTable, textdataString, xmlDataTable, colName, startLineNo, endLineNo, start, colLength, dataFormat, multiLineEnd, repeat, datavalue)
                        'If dataFormat = "Line" Then
                        '    datavalue = datavalue.ToString.Trim
                        'Else
                        '    datavalue = datavalue.ToString
                        'End If
                End Select
                xmlDataRow(colName) = datavalue
            End If 'count
        Next 'XML Column Table        
    End Sub
    Private Sub FixedWidthFileConverter(ByVal textdatafile As RichTextBox, ByVal xmlIdDataTable As DataTable, ByVal xmlColDataTable As DataTable, ByRef xmlDataTable As DataTable, ByRef Errlog As String, ByVal duplicateColName As Boolean)
        'Conversion Column Detail
        'Colums Fields
        Dim colName As String = ""
        Dim colNo As String = ""
        Dim colDataType As String = ""
        Dim dataFormat As String = ""
        Dim multiLineEnd As String = ""
        Dim repeat As String = ""
        Dim idNo As String = ""
        Dim colLineNo As String = ""
        Dim start As Integer = 0
        Dim colLength As Integer = 0
        Dim delimColNo As String = ""
        Dim delimiter As String = ""
        Dim textQualifier As String = ""
        'Identifier
        Dim hdridNo As String = ""
        Dim idString As String = ""
        Dim idType As String = ""
        Dim lineOffset As String = ""
        'Conversion Variables
        Dim dataValue As String = ""
        Dim xmlDataRow As DataRow
        Dim tmpErrorLog As String = ""
        Dim tmpidString As String = ""
        Dim prevIdNo As String = ""
        Dim tmpstring As String = ""
        Dim rowNo As Integer = 0
        Dim colLineNoAdd As Boolean = False
        Dim idstringFound As Boolean = False
        Dim startLineNo As Integer = 0
        Dim endLineNo As Integer = 0
        Dim idFound As Boolean = False
        Dim def_Delimiter As String = ""
        Dim def_TextQualifier As String = ""
        Dim delimiterStr As String()
        Dim linestr As String = ""
        Dim linenostr As String()
        Dim tmpid As String = ""
        Dim tmpcolid As String = ""
        Dim colidFound As Boolean = False
        Dim textdataString() As String
        Dim txtstr As String = ""
        Dim colcount As Integer = 0
        'Read Text File To Tmp Table
        Dim tmpDataTable As New DataTable 'Source File DataTable
        Dim tmpDataRow As DataRow 'Source File DataRow
        'For Header Data Table
        Dim tmpHdrDataTable As New DataTable
        Dim tmphdrRepeat As String = ""
        Dim chkRecordExist As Boolean = False
        'Insert into TmpDataTable From Source File
        tmpDataTable.Columns.Add("TextData")

        For Each txtLine As String In textdatafile.Lines
            tmpDataRow = tmpDataTable.NewRow
            tmpDataRow(0) = txtLine.ToString
            tmpDataTable.Rows.Add(tmpDataRow)
        Next
        textdataString = textdatafile.Lines

        'Read Header Data Start----------------------------------------
        ReadHeaderDataAll(tmpHdrDataTable, textdataString, tmpDataTable, xmlIdDataTable, xmlColDataTable, chkRecordExist)
        'Read Header Data End------------------------------------------

        'Conversion Start
        'sample 3 Address Format
        If chkRecordExist = False Then 'For Header Only Address Format
            For Each xmlId As DataRow In xmlIdDataTable.Rows
                hdridNo = xmlId("IDNo")
                idString = xmlId("IDString")
                idType = xmlId("IDType")
                lineOffset = xmlId("LineOffset")
                If idType = "D" Then
                    rowNo = -1
                    colLineNoAdd = False
                    For t As Integer = 0 To tmpDataTable.Rows.Count - 1
                        rowNo = rowNo + 1
                        If rowNo = tmpDataTable.Rows.Count Then
                            Exit For
                        End If
                        idstringFound = False
                        tmpstring = tmpDataTable.Rows(rowNo).Item(0).ToString
                        'Compare Identifier Type and Source File based on Defination File
                        idstringFound = CheckSourceFileWithIdentifierType(idString, tmpDataTable.Rows(rowNo).Item(0).ToString)
                        If rowNo = tmpDataTable.Rows.Count - 1 And idstringFound = False Then
                            idstringFound = True
                            endLineNo = rowNo
                        Else
                            endLineNo = rowNo - 1
                        End If

                        If idstringFound = True Then
                            xmlDataRow = xmlDataTable.NewRow
                            tmpstring = textdataString(startLineNo).ToString
                            prevIdNo = ""
                            While startLineNo < endLineNo
                                colcount = -1
                                For Each xmlcol As DataRow In xmlColDataTable.Rows
                                    colcount = colcount + 1
                                    If startLineNo > endLineNo Then
                                        Exit For
                                    End If
                                    colName = xmlcol("ColName")
                                    colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                                    colDataType = xmlcol("ColDataType")
                                    dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                                    multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                                    repeat = xmlcol("Repeat")
                                    idNo = xmlcol("IDNo")
                                    colLineNo = xmlcol("ColLineNo")
                                    start = CType(xmlcol("Start").ToString, Integer)
                                    colLength = xmlcol("ColLength")
                                    delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                                    delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                                    textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))
                                    'Compare Indentfier
                                    tmpidString = ""
                                    For Each xmlIdData As DataRow In xmlIdDataTable.Rows
                                        If idNo = xmlIdData("IDNo") Then
                                            tmpidString = xmlIdData("IDString")
                                        End If
                                    Next

                                    tmpstring = textdataString(startLineNo).ToString
                                    idstringFound = CheckSourceFileWithIdentifierType(tmpidString, tmpstring)
                                    'Column Line No > 0 for Same ID
                                    If prevIdNo = idNo And idstringFound = False And colLineNo > 0 Then
                                        idstringFound = True
                                    End If

                                    If idstringFound = True Then '2
                                        prevIdNo = idNo
                                        Try
                                            If start + colLength > tmpstring.Length Then
                                                dataValue = tmpstring.Substring(start, tmpstring.Length - start)
                                            Else
                                                dataValue = textdataString(startLineNo).Substring(start, colLength)
                                            End If
                                            If dataValue.ToString.Trim = "" Then
                                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                                    dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                End If
                                            End If

                                            If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                                'Copy last imported data
                                                dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                            End If

                                        Catch ex As Exception
                                            dataValue = ""
                                        End Try
                                        'Check Delimiter
                                        If delimiter <> "" Then
                                            If dataValue <> "" Then 'Added By Sandar 31 Aug 09
                                                def_Delimiter = GetDelimiterCode(delimiter)
                                                def_TextQualifier = GetTextQualifierCode(textQualifier)
                                                delimiterStr = SplitString(dataValue, def_Delimiter, def_TextQualifier, True)
                                                Try 'Added By Sandar 31 Aug 09
                                                    dataValue = delimiterStr(delimColNo - 1)
                                                Catch ex As Exception
                                                    dataValue = ""
                                                End Try

                                                'dataValue = CheckDelimiter(delimColNo, delimiter, textQualifier, dataValue, tmpErrorLog)
                                                If tmpErrorLog <> "" Then
                                                    Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + tmpErrorLog
                                                End If
                                            End If
                                        End If

                                        Select Case colDataType
                                            Case "T"
                                                If dataFormat = "Trim" Or dataFormat = "Line" Then
                                                    dataValue = dataValue.ToString.Trim
                                                Else
                                                    dataValue = dataValue.ToString
                                                End If

                                            Case "N"
                                                If IsNumeric(dataValue) = True Then
                                                    If dataFormat = "," Then
                                                        dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                                    Else
                                                        dataValue = CType(dataValue.ToString.Trim, Double)
                                                    End If

                                                Else
                                                    Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                                    dataValue = dataValue.ToString
                                                End If
                                            Case "M"
                                                dataValue = CheckMultilineEnd(idString, xmlIdDataTable, textdataString, xmlDataTable, colName, startLineNo, endLineNo, start, colLength, dataFormat, multiLineEnd, repeat, dataValue)
                                                'If dataFormat = "Line" Then
                                                '    dataValue = dataValue.ToString.Trim
                                                'Else
                                                '    dataValue = dataValue.ToString
                                                'End If

                                        End Select
                                        xmlDataRow(colName) = dataValue
                                        GetDuplicateData(colcount, idNo, xmlColDataTable, tmpstring, xmlDataTable, idString, xmlIdDataTable, startLineNo, endLineNo, textdataString, xmlDataRow, Errlog)
                                        startLineNo = startLineNo + 1
                                    End If ' 2 ID String Found 

                                    If idstringFound = False Then
                                        'Text File'fileds are not ordered. 
                                        idstringFound = False
                                        tmpidString = ""
                                        For Each xmlIdData As DataRow In xmlIdDataTable.Rows
                                            tmpidString = xmlIdData("IDString")
                                            idstringFound = CheckSourceFileWithIdentifierType(tmpidString, tmpstring)
                                            If idstringFound = True Then
                                                tmpid = xmlIdData("IDNo")
                                                Exit For
                                            End If
                                        Next
                                        If idstringFound = False Then
                                            startLineNo = startLineNo + 1
                                        Else
                                            colidFound = False
                                            For Each xmlcolumn As DataRow In xmlColDataTable.Rows
                                                If tmpid = xmlcolumn("IDNo").ToString Then
                                                    colidFound = True
                                                End If
                                            Next
                                            If colidFound = False Then
                                                'Skip Line
                                                startLineNo = startLineNo + 1
                                            End If
                                        End If
                                    End If

                                Next 'XML Column Table
                            End While 'StartLineNo < EndLineNo
                            If xmlDataTable.Rows.Count = 0 Then
                                xmlDataRow("Line No") = 1
                            Else
                                xmlDataRow("Line No") = xmlDataTable.Rows.Count + 1
                            End If
                            xmlDataTable.Rows.Add(xmlDataRow)
                            startLineNo = rowNo + 1
                        End If 'ID String Found
                    Next 'tmp Data Table
                End If
            Next
            'For Repeat Case For Header
            For r As Integer = 1 To xmlDataTable.Rows.Count - 1
                For c As Integer = 0 To xmlDataTable.Columns.Count - 1
                    repeat = 0
                    For Each xmlcol As DataRow In xmlColDataTable.Rows
                        colName = xmlcol("ColName")
                        If xmlDataTable.Columns(c).ColumnName.ToString.Trim = colName Then
                            repeat = xmlcol("Repeat")
                            If repeat >= 1 Then
                                Exit For
                            End If
                        End If
                    Next
                    If repeat >= 1 Then
                        Select Case repeat
                            Case 1
                                If xmlDataTable.Rows(r).Item(c).ToString = "" Then
                                    xmlDataTable.Rows(r).Item(c) = xmlDataTable.Rows(r - 1).Item(c).ToString()
                                End If
                            Case 2
                                xmlDataTable.Rows(r).Item(c) = xmlDataTable.Rows(r - 1).Item(c).ToString()
                        End Select
                    End If
                Next
            Next
            Exit Sub
        End If

        'For Record and Header Convesion sample 8(duplicate column name)
        If duplicateColName = True Then
            For Each xmlId As DataRow In xmlIdDataTable.Rows
                hdridNo = xmlId("IDNo")
                idString = xmlId("IDString")
                idType = xmlId("IDType")
                lineOffset = xmlId("LineOffset")
                Select Case idType
                    Case "D"
                        rowNo = -1
                        colLineNoAdd = False
                        For t As Integer = 0 To xmlDataTable.Columns.Count - 1
                            rowNo = rowNo + 1
                            If rowNo = xmlDataTable.Columns.Count Then
                                Exit For
                            End If
                            idstringFound = False
                            tmpstring = tmpDataTable.Rows(rowNo).Item(0).ToString
                            'Compare Identifier Type and Source File based on Defination File
                            idstringFound = CheckSourceFileWithIdentifierType(idString, tmpDataTable.Rows(rowNo).Item(0).ToString)
                            If idstringFound = True Then
                                xmlDataRow = xmlDataTable.NewRow
                                idFound = False
                                For Each xmlcol As DataRow In xmlColDataTable.Rows
                                    colName = xmlcol("ColName")
                                    colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                                    colDataType = xmlcol("ColDataType")
                                    dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                                    multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                                    repeat = xmlcol("Repeat")
                                    idNo = xmlcol("IDNo")
                                    colLineNo = xmlcol("ColLineNo")
                                    start = CType(xmlcol("Start").ToString, Integer)
                                    colLength = xmlcol("ColLength")
                                    delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                                    delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                                    textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))
                                    If hdridNo = idNo Then
                                        If colLineNo = 0 Then
                                            Try
                                                tmpstring = textdataString(rowNo).ToString
                                                If start + colLength > tmpstring.Length Then
                                                    dataValue = tmpstring.Substring(start, tmpstring.Length - start)
                                                Else
                                                    dataValue = textdataString(rowNo).Substring(start, colLength)
                                                End If
                                                If dataValue.ToString.Trim = "" Then
                                                    If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                                        dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                    End If
                                                End If

                                                If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                                    'Copy last imported data
                                                    dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                End If

                                            Catch ex As Exception
                                                dataValue = ""
                                            End Try
                                        Else
                                            'Column Line No 1 0r greater
                                            colLineNoAdd = True
                                            Try
                                                txtstr = textdataString(rowNo + colLineNo).ToString
                                                If start > txtstr.Length Then
                                                    dataValue = ""
                                                ElseIf start + colLength > txtstr.Length Then
                                                    dataValue = textdataString(rowNo + colLineNo).Substring(start, txtstr.Length - start)
                                                Else
                                                    dataValue = textdataString(rowNo + colLineNo).Substring(start, colLength)
                                                End If

                                                If dataValue.ToString.Trim = "" Then
                                                    If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                                        dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                    End If
                                                End If
                                                If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                                    'Copy last imported data
                                                    dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                End If
                                            Catch ex As Exception
                                                dataValue = ""
                                            End Try
                                        End If

                                        'Check Delimiter
                                        If delimiter <> "" Then
                                            If dataValue <> "" Then 'Added By Sandar 31 August 09
                                                def_Delimiter = GetDelimiterCode(delimiter)
                                                def_TextQualifier = GetTextQualifierCode(textQualifier)
                                                delimiterStr = SplitString(dataValue, def_Delimiter, def_TextQualifier, True)
                                                Try 'Added By Sandar 31 Aug 09
                                                    dataValue = delimiterStr(delimColNo - 1)
                                                Catch ex As Exception
                                                    dataValue = ""
                                                End Try
                                                '
                                                'dataValue = CheckDelimiter(delimColNo, delimiter, textQualifier, dataValue, tmpErrorLog)
                                                If tmpErrorLog <> "" Then
                                                    Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + tmpErrorLog
                                                End If
                                            End If
                                        End If
                                        Select Case colDataType
                                            Case "T"
                                                If dataFormat = "Trim" Then
                                                    dataValue = dataValue.ToString.Trim
                                                Else
                                                    dataValue = dataValue.ToString
                                                End If
                                            Case "N"
                                                If IsNumeric(dataValue) = True Then
                                                    If dataFormat = "," Then
                                                        dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                                    Else
                                                        dataValue = CType(dataValue.ToString.Trim, Double)
                                                    End If

                                                Else
                                                    Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                                    dataValue = dataValue.ToString
                                                End If
                                            Case "M"
                                                dataValue = CheckMultilineEnd(idString, xmlIdDataTable, textdataString, xmlDataTable, colName, startLineNo, endLineNo, start, colLength, dataFormat, multiLineEnd, repeat, dataValue)
                                                'If dataFormat = "Line" Then
                                                '    dataValue = dataValue.ToString.Trim
                                                'Else
                                                '    dataValue = dataValue.ToString
                                                'End If
                                        End Select
                                        If idFound = False Then
                                            idFound = True
                                        End If
                                        xmlDataRow(colName) = dataValue
                                    End If
                                Next
                                If idFound = True Then
                                    If xmlDataTable.Rows.Count = 0 Then
                                        xmlDataRow("Line No") = 1
                                    Else
                                        xmlDataRow("Line No") = xmlDataTable.Rows.Count + 1
                                    End If
                                    xmlDataTable.Rows.Add(xmlDataRow)
                                    Exit For
                                End If
                            End If 'Check ID String  Condition
                        Next 'tmp Data Table
                End Select 'ID Type
            Next 'Id DataTable
            Exit Sub
        End If

        '  'For Record and Header Convesion sample1,2,5(No Duplicate Col Name)
        For Each xmlId As DataRow In xmlIdDataTable.Rows
            hdridNo = xmlId("IDNo")
            idString = xmlId("IDString")
            idType = xmlId("IDType")
            lineOffset = xmlId("LineOffset")
            Select Case idType
                Case "D"
                    rowNo = -1
                    colLineNoAdd = False
                    For t As Integer = 0 To tmpDataTable.Rows.Count - 1

                        If Mid(idString.Trim, 1, 5) = "<LINE" Then
                            linestr = idString.Trim
                            linestr = Mid(linestr, 6, linestr.Length - 6)
                            linenostr = Split(linestr, ",")
                            If t = 0 Then
                                Try
                                    rowNo = CType(linenostr(0).ToString, Integer) - 1
                                Catch ex As Exception
                                    rowNo = 0
                                End Try
                            Else
                                Try
                                    rowNo = CType(linenostr(0).ToString, Integer) + (CType(linenostr(1).ToString, Integer) * t)
                                    rowNo = rowNo - 1
                                Catch ex As Exception
                                    rowNo = 0
                                End Try
                            End If
                            idstringFound = True
                        Else
                            'Normal case
                            rowNo = rowNo + 1
                            idstringFound = False
                        End If

                        If rowNo >= tmpDataTable.Rows.Count Then
                            Exit For
                        End If

                        tmpstring = tmpDataTable.Rows(rowNo).Item(0).ToString
                        'Compare Identifier Type and Source File based on Defination File
                        If idstringFound = False Then
                            idstringFound = CheckSourceFileWithIdentifierType(idString, tmpDataTable.Rows(rowNo).Item(0).ToString)
                        End If

                        If idstringFound = True Then
                            xmlDataRow = xmlDataTable.NewRow
                            For Each xmlcol As DataRow In xmlColDataTable.Rows
                                colName = xmlcol("ColName")
                                colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                                colDataType = xmlcol("ColDataType")
                                dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                                multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                                repeat = xmlcol("Repeat")
                                idNo = xmlcol("IDNo")
                                colLineNo = xmlcol("ColLineNo")
                                start = CType(xmlcol("Start").ToString, Integer)
                                colLength = xmlcol("ColLength")
                                delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                                delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                                textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))
                                If hdridNo = idNo Then
                                    If colLineNo = 0 Then
                                        Try
                                            tmpstring = textdataString(rowNo).ToString
                                            If start + colLength > tmpstring.Length Then
                                                dataValue = tmpstring.Substring(start, tmpstring.Length - start)
                                            Else
                                                dataValue = textdataString(rowNo).Substring(start, colLength)
                                            End If
                                            If dataValue.ToString.Trim = "" Then
                                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                                    dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                End If
                                            End If

                                            If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                                'Copy last imported data
                                                dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                            End If

                                        Catch ex As Exception
                                            dataValue = ""
                                        End Try
                                    Else
                                        'Column Line No 1 0r greater
                                        colLineNoAdd = True
                                        Try
                                            txtstr = textdataString(rowNo + colLineNo).ToString
                                            If start > txtstr.Length Then
                                                dataValue = ""
                                            ElseIf start + colLength > txtstr.Length Then
                                                dataValue = textdataString(rowNo + colLineNo).Substring(start, txtstr.Length - start)
                                            Else
                                                dataValue = textdataString(rowNo + colLineNo).Substring(start, colLength)
                                            End If

                                            If dataValue.ToString.Trim = "" Then
                                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                                    dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                                End If
                                            End If
                                            If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                                'Copy last imported data
                                                dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                            End If
                                        Catch ex As Exception
                                            dataValue = ""
                                        End Try
                                    End If

                                    'Check Delimiter
                                    If delimiter <> "" Then
                                        If dataValue <> "" Then 'Added By Sandar 31 Aug 09
                                            def_Delimiter = GetDelimiterCode(delimiter)
                                            def_TextQualifier = GetTextQualifierCode(textQualifier)
                                            delimiterStr = SplitString(dataValue, def_Delimiter, def_TextQualifier, True)
                                            Try
                                                dataValue = delimiterStr(delimColNo - 1)
                                            Catch ex As Exception
                                                dataValue = ""
                                            End Try

                                            'dataValue = CheckDelimiter(delimColNo, delimiter, textQualifier, dataValue, tmpErrorLog)
                                            If tmpErrorLog <> "" Then
                                                Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + tmpErrorLog
                                            End If
                                        End If
                                    End If
                                    Select Case colDataType
                                        Case "T"
                                            If dataFormat = "Trim" Then
                                                dataValue = dataValue.ToString.Trim
                                            Else
                                                dataValue = dataValue.ToString
                                            End If
                                        Case "N"
                                            If IsNumeric(dataValue) = True Then
                                                If dataFormat = "," Then
                                                    dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                                Else
                                                    dataValue = CType(dataValue.ToString.Trim, Double)
                                                End If

                                            Else
                                                Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                                dataValue = dataValue.ToString
                                            End If

                                        Case "M"
                                            startLineNo = rowNo 'Added By Sandar 31 Aug 09
                                            endLineNo = tmpDataTable.Rows.Count - 1 'Added By Sandar 31 Aug 09
                                            dataValue = CheckMultilineEnd(idString, xmlIdDataTable, textdataString, xmlDataTable, colName, startLineNo, endLineNo, start, colLength, dataFormat, multiLineEnd, repeat, dataValue)
                                            'If dataFormat = "Line" Then
                                            '    dataValue = dataValue.ToString.Trim
                                            'Else
                                            '    dataValue = dataValue.ToString
                                            'End If
                                    End Select
                                    xmlDataRow(colName) = dataValue
                                Else
                                    'Read Header Data based on Line No
                                    If chkRecordExist = True Then
                                        ReadHeaderAndTrialerDataById(xmlIdDataTable, tmpHdrDataTable, idNo, colName, dataValue, rowNo, tmphdrRepeat)
                                        'Check Delimiter
                                        If delimiter <> "" Then
                                            If dataValue <> "" Then 'Added By Sandar 31 Aug 09
                                                def_Delimiter = GetDelimiterCode(delimiter)
                                                def_TextQualifier = GetTextQualifierCode(textQualifier)
                                                delimiterStr = SplitString(dataValue, def_Delimiter, def_TextQualifier, True)
                                                Try 'Added By Sandar 31 Aug 09
                                                    dataValue = delimiterStr(delimColNo - 1)
                                                Catch ex As Exception
                                                    dataValue = ""
                                                End Try

                                                'dataValue = CheckDelimiter(delimColNo, delimiter, textQualifier, dataValue, tmpErrorLog)
                                            End If
                                        End If

                                        Select Case colDataType
                                            Case "T"
                                                If dataFormat = "Trim" Then
                                                    dataValue = dataValue.ToString.Trim
                                                Else
                                                    dataValue = dataValue.ToString
                                                End If
                                            Case "N"
                                                If IsNumeric(dataValue) = True Then
                                                    If dataFormat = "," Then
                                                        dataValue = Format(CType(dataValue.ToString.Trim, Double), "###,##0.00")
                                                    Else
                                                        dataValue = CType(dataValue.ToString.Trim, Double)
                                                    End If

                                                Else
                                                    Errlog = Errlog + "Line " + (xmlDataTable.Rows.Count + 1).ToString + " Column " + "'" + colName + "' should be numeric." + Chr(13) + Chr(10)
                                                    dataValue = dataValue.ToString
                                                End If
                                            
                                        End Select

                                        If tmphdrRepeat = 1 Then
                                            xmlDataRow(colName) = dataValue
                                        ElseIf repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                            'Copy last imported data
                                            dataValue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                            xmlDataRow(colName) = dataValue
                                        ElseIf repeat = 2 And xmlDataTable.Rows.Count = 0 Then
                                            xmlDataRow(colName) = dataValue
                                        End If

                                    End If
                                End If
                            Next

                            If xmlDataTable.Rows.Count = 0 Then
                                xmlDataRow("Line No") = 1
                            Else
                                xmlDataRow("Line No") = xmlDataTable.Rows.Count + 1
                            End If
                            xmlDataTable.Rows.Add(xmlDataRow)
                            If colLineNoAdd = True Then
                                rowNo = rowNo + 1 'Sample 1,2,5
                            End If
                        End If 'Check ID String  Condition

                    Next 'tmp Data Table
            End Select 'ID Type
        Next 'Id DataTable
        'Conversion End
    End Sub
    Private Function CheckMultilineEnd(ByVal xmlCuridString As String, _
            ByVal xmlIdDataTable As DataTable, _
            ByVal textdataString() As String, _
            ByVal xmlDataTable As DataTable, _
            ByVal colName As String, _
            ByRef startlineno As Integer, _
            ByVal endLineNo As Integer, _
            ByVal start As Integer, _
            ByVal collength As Integer, _
            ByVal dataFormat As String, _
            ByVal multilineEnd As String, _
            ByVal repeat As String, _
            ByVal tmpdatavalue As String) As String

        Dim datavalue As String = ""
        Dim tmpstring As String = ""
        Dim idString As String = ""
        Dim hdridNo As String = ""
        Dim anyIdentifierFound As Boolean = False
        Dim idNameStr As String()
        Dim startlineno_idfound As Integer = 0
        Dim startReadPosition As Integer = 0
        datavalue = tmpdatavalue
        If dataFormat = "Clip" Or dataFormat = "Line" Then '1
            If datavalue.Trim <> "" And dataFormat = "Clip" Then 'Added on 08 Sept 09
                datavalue = datavalue + " "
            End If
            If multilineEnd = "Any Identifier" Then '2
                For m As Integer = startlineno + 1 To endLineNo
                    tmpstring = textdataString(m).ToString
                    idString = ""
                    anyIdentifierFound = False
                    For Each xmlId As DataRow In xmlIdDataTable.Rows
                        idString = xmlId("IDString")
                        If xmlCuridString <> idString Then
                            anyIdentifierFound = CheckSourceFileWithIdentifierType(idString, tmpstring)
                            If anyIdentifierFound = True Then
                                Exit For
                            End If
                        End If

                    Next
                    If anyIdentifierFound = False Then
                        Try
                            tmpstring = textdataString(m).ToString
                            If dataFormat = "Line" Then
                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(0, tmpstring.Length)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(0, start + collength)
                                End If
                            Else

                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(start, tmpstring.Length - start)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(start, collength)
                                End If
                                'datavalue = datavalue.Trim  'Added By Sandar 31 Aug 09  
                                datavalue = datavalue + " "   'Add one space 08 Sept 09
                            End If

                            If datavalue.ToString.Trim = "" Then
                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                    datavalue = datavalue + xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()

                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If

                            Else
                                If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                    'Copy last imported data
                                    datavalue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()

                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If
                            End If
                        Catch ex As Exception
                            datavalue = ""
                        End Try
                    Else
                        startlineno = m - 1
                        Exit For
                    End If
                Next
            ElseIf multilineEnd = "Blank Line" Then
                For m As Integer = startlineno + 1 To endLineNo
                    tmpstring = textdataString(m).ToString
                    idString = ""
                    anyIdentifierFound = False
                    anyIdentifierFound = CheckSourceFileWithIdentifierType("<EMPTY LINE>", tmpstring)
                    If anyIdentifierFound = False Then
                        Try
                            tmpstring = textdataString(m).ToString
                            If dataFormat = "Line" Then
                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(0, tmpstring.Length)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(0, start + collength)
                                End If
                            Else
                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(start, tmpstring.Length - start)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(start, collength)
                                End If
                                'datavalue = datavalue.Trim 'Added By Sandar 31 Aug 09  
                                datavalue = datavalue + " "   'Add one space 08 Sept 09

                            End If

                            If datavalue.ToString.Trim = "" Then
                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                    datavalue = datavalue + xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If
                            Else
                                If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                    'Copy last imported data
                                    datavalue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()

                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If
                            End If
                        Catch ex As Exception
                            datavalue = ""
                        End Try
                    Else
                        startlineno = m - 1
                        Exit For
                    End If
                Next
            Else
                'Identifier ID1,ID2,ID3
                idNameStr = Split(Mid(multilineEnd, 12, multilineEnd.Length - 10), ",")
                If idNameStr.Length = 0 Then
                    idNameStr(0) = Mid(multilineEnd, 12, multilineEnd.Length - 10)
                End If
                For m As Integer = startlineno + 1 To endLineNo
                    tmpstring = textdataString(m).ToString
                    idString = ""
                    anyIdentifierFound = False
                    For Each xmlId As DataRow In xmlIdDataTable.Rows
                        idString = xmlId("IDString")
                        hdridNo = xmlId("IDNo")
                        If xmlCuridString <> idString Then
                            anyIdentifierFound = CheckSourceFileWithIdentifierType(idString, tmpstring)
                            If anyIdentifierFound = True Then
                                If startlineno_idfound = 0 Then
                                    startlineno_idfound = m - 1
                                End If
                                anyIdentifierFound = False
                                For d As Integer = 0 To idNameStr.Length - 1
                                    If idNameStr(d).ToString.Trim = hdridNo Then
                                        anyIdentifierFound = True
                                        Exit For
                                    End If
                                Next
                                If anyIdentifierFound = True Then
                                    Exit For
                                End If
                            End If
                        End If
                    Next

                    If anyIdentifierFound = False Then
                        Try
                            tmpstring = textdataString(m).ToString
                            If dataFormat = "Line" Then
                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(0, tmpstring.Length)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(0, start + collength)
                                End If
                            Else
                                If start + collength > tmpstring.Length Then
                                    datavalue = datavalue + tmpstring.Substring(start, tmpstring.Length - start)
                                Else
                                    datavalue = datavalue + textdataString(m).Substring(start, collength)
                                End If
                                'datavalue = datavalue.Trim  'Added By Sandar 31 Aug 09

                                datavalue = datavalue + " "   'Add one space 08 Sept 09
                            End If
                            If datavalue.ToString.Trim = "" Then
                                If repeat = 1 And xmlDataTable.Rows.Count > 0 Then
                                    datavalue = datavalue + xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If

                            Else
                                If repeat = 2 And xmlDataTable.Rows.Count > 0 Then
                                    'Copy last imported data
                                    datavalue = xmlDataTable.Rows(xmlDataTable.Rows.Count - 1).Item(colName).ToString()
                                    If dataFormat = "Clip" Then
                                        datavalue = datavalue + " "   'Add one space 08 Sept 09
                                    End If
                                End If
                            End If
                        Catch ex As Exception
                            datavalue = ""
                        End Try
                    Else
                        ' startlineno = m - 1
                        Exit For
                    End If
                Next
                startlineno = startlineno_idfound
            End If '2
        End If '1

        Return datavalue
    End Function

    Private Function GetDelimiterCode(ByVal delimitertext As String) As String
        Dim delimiterCode As String = ""
        Select Case delimitertext
            Case "{space}"
                delimiterCode = " "
            Case "{tab}"
                delimiterCode = vbTab
            Case "{new line}"
                delimiterCode = vbNewLine
            Case "{,}"
                delimiterCode = ","
            Case "{}"
                delimiterCode = "`"
            Case Else
                If delimitertext.IndexOf("{") > -1 And delimitertext.IndexOf("}") > -1 Then
                    delimiterCode = delimitertext.Replace("{", "").Replace("}", "")
                Else
                    delimiterCode = ""
                End If
        End Select
        Return delimiterCode
    End Function

    Private Function GetTextQualifierCode(ByVal txtQualifier As String) As String
        Dim textQualifierCode As String = ""
        Select Case txtQualifier
            Case "{""}"
                textQualifierCode = """"
            Case "{'}"
                textQualifierCode = "'"
            Case "{none}"
                textQualifierCode = ""
            Case Else
                If txtQualifier.IndexOf("{") <> -1 And txtQualifier.IndexOf("}") <> -1 Then
                    textQualifierCode = txtQualifier.Replace("{", "").Replace("}", "")
                Else
                    textQualifierCode = ""
                End If
        End Select
        Return textQualifierCode
    End Function

    Private Sub ReadHeaderAndTrialerDataById(ByVal xmlIDDataTable As DataTable, ByVal tmpHdrDataTable As DataTable, ByVal idNo As String, ByVal colName As String, ByRef hdrdatavalue As String, ByVal rowNo As Integer, ByRef repeat As String)
        hdrdatavalue = ""
        Dim startRow As Integer = 0
        Dim idFound As Integer = 0
        Dim hdrvalue As String = ""
        Dim hdrIdNo As String = ""
        Dim hdrColName As String = ""
        Dim idType As String = ""
        Dim lastRowNo As Integer = 0
        Dim ExitLoop As Boolean = False
        Dim rowcount As Integer = 0
        Dim FoundAtRowNo As Integer = 0
        repeat = 0
        lastRowNo = CType(tmpHdrDataTable.Rows(tmpHdrDataTable.Rows.Count - 1).Item("RowNo").ToString, Integer)
        rowcount = tmpHdrDataTable.Rows.Count - 1
        For k As Integer = 0 To tmpHdrDataTable.Rows.Count - 1
            hdrIdNo = tmpHdrDataTable.Rows(k).Item("IDNo")
            hdrColName = tmpHdrDataTable.Rows(k).Item("ColName")
            If hdrIdNo = idNo And hdrColName = colName Then
                idType = ""
                For d As Integer = 0 To xmlIDDataTable.Rows.Count - 1
                    If idNo = xmlIDDataTable.Rows(d).Item("IDNo").ToString Then
                        idType = xmlIDDataTable.Rows(d).Item("IDType")
                        Exit For
                    End If
                Next
                idFound = idFound + 1
                FoundAtRowNo = k
                startRow = CType(tmpHdrDataTable.Rows(k).Item("RowNo").ToString, Integer)
                If idType <> "T" Then
                    If (rowNo < startRow) Then
                        'Sub Head Found
                        If k > 0 Then
                            ExitLoop = False
                            For kk As Integer = 1 To tmpHdrDataTable.Rows.Count - 1 'tmpHdrDataTable.Columns.Count
                                If k - kk < 0 Then
                                    Exit For
                                End If
                                If hdrColName = tmpHdrDataTable.Rows(k - kk).Item("ColName").ToString Then
                                    hdrvalue = ""
                                    hdrdatavalue = tmpHdrDataTable.Rows(k - kk).Item("DataValue").ToString
                                    repeat = tmpHdrDataTable.Rows(k - kk).Item("Repeat").ToString
                                    If tmpHdrDataTable.Rows(k - kk).Item("Read") = "N" Then
                                        If repeat = 0 Then repeat = 1
                                        tmpHdrDataTable.Rows(k - kk).Item("Read") = "Y"
                                    ElseIf tmpHdrDataTable.Rows(k - kk).Item("Read") = "Y" And repeat = 0 Then
                                        hdrdatavalue = ""
                                    End If
                                    ExitLoop = True
                                    Exit For
                                End If
                            Next
                            If ExitLoop = True Then
                                Exit For
                            End If

                        End If

                    ElseIf rowNo > lastRowNo Then
                        'Last Record
                        ExitLoop = False
                        For kk As Integer = 0 To tmpHdrDataTable.Rows.Count - 1 'tmpHdrDataTable.Columns.Count

                            If hdrColName = tmpHdrDataTable.Rows(rowcount - kk).Item("ColName").ToString Then
                                hdrvalue = ""
                                hdrdatavalue = tmpHdrDataTable.Rows(rowcount - kk).Item("DataValue").ToString
                                repeat = tmpHdrDataTable.Rows(rowcount - kk).Item("Repeat").ToString
                                If tmpHdrDataTable.Rows(rowcount - kk).Item("Read") = "N" Then
                                    If repeat = 0 Then repeat = 1
                                    tmpHdrDataTable.Rows(rowcount - kk).Item("Read") = "Y"
                                ElseIf tmpHdrDataTable.Rows(rowcount - kk).Item("Read") = "Y" And repeat = 0 Then
                                    hdrdatavalue = ""
                                End If
                                ExitLoop = True
                                Exit For
                            End If
                        Next
                        If ExitLoop = True Then
                            Exit For
                        End If
                    End If
                Else
                    'For Trailer
                    If (rowNo < startRow) Then
                        Exit For
                    End If
                End If

            End If
        Next

        If idFound = 1 And hdrdatavalue = "" Then
            'Main Header
            'Updated By Sandar 27 August 09
            hdrvalue = tmpHdrDataTable.Rows(FoundAtRowNo).Item("DataValue").ToString
            repeat = tmpHdrDataTable.Rows(FoundAtRowNo).Item("Repeat").ToString
            If tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "N" Then
                If repeat = 0 Then repeat = 1
                tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y"
                hdrdatavalue = hdrvalue
            ElseIf tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y" And repeat = 0 Then
                hdrdatavalue = ""
            ElseIf tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y" And repeat = 1 Then
                hdrdatavalue = hdrvalue
            End If
            'repeat = 1
        ElseIf idFound > 1 And hdrdatavalue = "" Then
            'Last Sub Header
            hdrdatavalue = hdrvalue
            If idType <> "T" Then
                repeat = tmpHdrDataTable.Rows(tmpHdrDataTable.Rows.Count - 1).Item("Repeat").ToString
            Else
                'Trailer
                'Added on 08 Sept 09
                hdrvalue = tmpHdrDataTable.Rows(FoundAtRowNo).Item("DataValue").ToString
                repeat = tmpHdrDataTable.Rows(FoundAtRowNo).Item("Repeat").ToString
                If tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "N" Then
                    If repeat = 0 Then repeat = 1
                    tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y"
                    hdrdatavalue = hdrvalue
                ElseIf tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y" And repeat = 0 Then
                    hdrdatavalue = ""
                ElseIf tmpHdrDataTable.Rows(FoundAtRowNo).Item("Read") = "Y" And repeat = 1 Then
                    hdrdatavalue = hdrvalue
                End If
                'repeat = 1
            End If
        End If

    End Sub

    Private Sub ReadHeaderDataAll(ByRef tmpHdrDataTable As DataTable, ByVal textdatastring() As String, ByVal tmpDataTable As DataTable, ByVal xmlIdDataTable As DataTable, ByVal xmlColDataTable As DataTable, ByRef chkRecordExist As Boolean)
        'Colums Fields
        Dim colName As String = ""
        Dim colNo As String = ""
        Dim colDataType As String = ""
        Dim dataFormat As String = ""
        Dim multiLineEnd As String = ""
        Dim repeat As String = ""
        Dim idNo As String = ""
        Dim colLineNo As String = ""
        Dim start As Integer = 0
        Dim colLength As Integer = 0
        Dim delimColNo As String = ""
        Dim delimiter As String = ""
        Dim textQualifier As String = ""
        'Identifier
        Dim hdridNo As String = ""
        Dim idString As String = ""
        Dim idType As String = ""
        Dim lineOffset As String = ""
        'Data Variables for Conversion
        Dim dataValue As String = ""
        Dim tmpstring As String = ""
        Dim rowNo As Integer = 0
        Dim idstringFound As Boolean = False
        Dim tmpHdrDataRow As DataRow
        Dim endLineNo As Integer = 0
        Dim startLineNo As Integer = 0
        For Each xmlId As DataRow In xmlIdDataTable.Rows
            hdridNo = xmlId("IDNo")
            idType = xmlId("IDType")
            If idType = "D" Then
                For Each xmlcol As DataRow In xmlColDataTable.Rows
                    idNo = xmlcol("IDNo")
                    If hdridNo = idNo Then
                        chkRecordExist = True
                    End If
                Next
            End If
        Next
        If chkRecordExist = False Then Exit Sub
        tmpHdrDataTable.Columns.Add("IDNo")
        tmpHdrDataTable.Columns.Add("DataValue")
        tmpHdrDataTable.Columns.Add("RowNo")
        tmpHdrDataTable.Columns.Add("ColName")
        tmpHdrDataTable.Columns.Add("Repeat")
        tmpHdrDataTable.Columns.Add("Read")
        For Each xmlId As DataRow In xmlIdDataTable.Rows
            hdridNo = xmlId("IDNo")
            idString = xmlId("IDString")
            idType = xmlId("IDType")
            lineOffset = xmlId("LineOffset")
            If idType = "H" Or idType = "T" Then 'Header or Trailer
                rowNo = -1
                For t As Integer = 0 To tmpDataTable.Rows.Count - 1
                    rowNo = rowNo + 1
                    If rowNo = tmpDataTable.Rows.Count Then
                        Exit For
                    End If
                    idstringFound = False
                    tmpstring = tmpDataTable.Rows(rowNo).Item(0).ToString
                    'Compare Identifier Type and Source File based on Defination File
                    idstringFound = CheckSourceFileWithIdentifierType(idString, tmpDataTable.Rows(rowNo).Item(0).ToString)
                    If idstringFound = True Then
                        For Each xmlcol As DataRow In xmlColDataTable.Rows
                            colName = xmlcol("ColName")
                            colNo = IIf(xmlcol("ColNo") Is DBNull.Value, "", xmlcol("ColNo"))
                            colDataType = xmlcol("ColDataType")
                            dataFormat = IIf(xmlcol("DataFormat") Is DBNull.Value, "", xmlcol("DataFormat"))
                            multiLineEnd = IIf(xmlcol("MultiLineEnd") Is DBNull.Value, "", xmlcol("MultiLineEnd"))
                            repeat = xmlcol("Repeat")
                            idNo = xmlcol("IDNo")
                            colLineNo = xmlcol("ColLineNo")
                            start = CType(xmlcol("Start").ToString, Integer) '- 1
                            colLength = xmlcol("ColLength")
                            delimColNo = IIf(xmlcol("DelimColNo") Is DBNull.Value, "", xmlcol("DelimColNo"))
                            delimiter = IIf(xmlcol("Delimiter") Is DBNull.Value, "", xmlcol("Delimiter"))
                            textQualifier = IIf(xmlcol("TextQualifier") Is DBNull.Value, "", xmlcol("TextQualifier"))
                            If hdridNo = idNo Then
                                If colLineNo = 0 Then
                                    Try
                                        tmpstring = textdatastring(rowNo).ToString
                                        If start + colLength > tmpstring.Length Then
                                            dataValue = tmpstring.Substring(start, tmpstring.Length - start)
                                        Else
                                            dataValue = textdatastring(rowNo).Substring(start, colLength)
                                        End If
                                    Catch ex As Exception
                                        dataValue = ""
                                    End Try
                                Else
                                    'Column Line No 1 0r greater
                                    Try
                                        tmpstring = textdatastring(rowNo + colLineNo).ToString
                                        If start + colLength > tmpstring.Length Then
                                            dataValue = tmpstring.Substring(start, tmpstring.Length - start)
                                        Else
                                            dataValue = textdatastring(rowNo + colLineNo).Substring(start, colLength)
                                        End If
                                    Catch ex As Exception
                                        dataValue = ""
                                    End Try
                                End If
                                Select Case colDataType
                                    Case "T"
                                        dataValue = dataValue.ToString.Trim
                                    Case "N"
                                        If IsNumeric(dataValue) = True Then
                                            dataValue = CType(dataValue, Double)
                                        End If
                                    Case "M"
                                        endLineNo = tmpDataTable.Rows.Count - 1
                                        startLineNo = rowNo
                                        dataValue = ""
                                        dataValue = CheckMultilineEnd(idString, xmlIdDataTable, textdatastring, tmpDataTable, colName, startLineNo, endLineNo, start, colLength, dataFormat, multiLineEnd, repeat, dataValue)
                                        
                                End Select
                                tmpHdrDataRow = tmpHdrDataTable.NewRow
                                tmpHdrDataRow("IDNo") = idNo
                                tmpHdrDataRow("DataValue") = dataValue
                                tmpHdrDataRow("RowNo") = rowNo
                                tmpHdrDataRow("ColName") = colName
                                tmpHdrDataRow("Repeat") = repeat
                                tmpHdrDataRow("Read") = "N"
                                tmpHdrDataTable.Rows.Add(tmpHdrDataRow)
                            End If
                        Next

                    End If 'Check ID String  Condition
                Next 'tmp Data Table
            End If
        Next
    End Sub

    Private Function CheckSourceFileWithIdentifierType(ByVal idTypeStr As String, ByVal txtdata As String) As Boolean
        Dim idTypeFound As Boolean = False
        Dim idTypeStrLength As Integer = 0
        Dim idTypeCount As Integer = -1
        Dim charCode As Integer = 0
        Dim idTypeLegends As String = ""

        If idTypeStr.Trim = "<EMPTY LINE>" Then
            If txtdata.ToString.Trim = "" Then
                idTypeFound = True
            Else
                idTypeFound = False
            End If
            Return idTypeFound
            Exit Function
        End If

        'Old Coding
        '**********
        'If txtdata = Nothing Then 'No Data in Column
        '    idTypeFound = False
        '    Exit Function
        'End If

        '*************************************
        'Modified by Kay, according to an issue
        'info escalated by Juntak > Meera > Pieter > Kay
        '*************************************
        If String.IsNullOrEmpty(txtdata) Then 'No Data in Column
            idTypeFound = False
            Exit Function
        End If
        If idTypeStr.Trim().Length > txtdata.Length Then
            txtdata = txtdata.PadRight(idTypeStr.Trim().Length - txtdata.Length, " ")
        End If

        '**********************
        'End of modification
        '**********************

        idTypeStrLength = idTypeStr.Length - 1
        For t As Integer = 0 To txtdata.Length - 1
            If idTypeStr.Trim = "<NEW PAGE>" Then
                idTypeLegends = idTypeStr.Trim
            Else
                idTypeCount = idTypeCount + 1
                If idTypeCount > idTypeStr.Length - 1 Then
                    If idTypeFound = True Then
                        idTypeLegends = " "
                    Else
                        Exit For
                    End If
                Else
                    idTypeLegends = idTypeStr(idTypeCount)
                End If
            End If

            Select Case idTypeLegends
                Case "_" 'Matches space
                    If txtdata(t) = " " Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case "?" 'Matches non space
                    If txtdata(t) <> " " Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case " " 'Any Character 
                    If txtdata(t).ToString.Length <> 0 Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case "@" 'A-Z , a-Z
                    charCode = Asc(txtdata(t))
                    If (charCode >= 65 And charCode <= 90) Or (charCode >= 97 And charCode <= 122) Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case "#" 'Any Character 
                    If IsNumeric(txtdata(t).ToString) = True Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case "<NEW PAGE>"
                    If txtdata(t).ToString = Chr(12) Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
                Case "<Line n,m>"
                Case Else  'Other
                    If txtdata(t).ToString = idTypeLegends Then
                        idTypeFound = True
                    Else
                        idTypeFound = False
                        Exit For
                    End If
            End Select



        Next

        Return idTypeFound
    End Function

    Private Sub ReadDefinationFile(ByVal filename As String, ByRef idDataTable As DataTable, ByRef hdrDataTable As DataTable, ByRef colDataTable As DataTable, ByRef duplicateColName As Boolean)
        LoadFromFile(filename)
        Dim idCollection As IdentifierCollection = Me.IdentifierCollection
        idDataTable = New DataTable
        idDataTable.Columns.Add("IDNo")
        idDataTable.Columns.Add("IDString")
        idDataTable.Columns.Add("IDType")
        idDataTable.Columns.Add("LineOffset")
        Dim idDataRow As DataRow
        For Each id As Identifier In idCollection
            idDataRow = idDataTable.NewRow
            idDataRow(0) = id.IDNo
            idDataRow(1) = id.IDString
            idDataRow(2) = id.IDType
            idDataRow(3) = id.LineOffset
            idDataTable.Rows.Add(idDataRow)
        Next

        'Read Column Header and Col Data Value
        Dim ColCollection As ColumnCollection = Me.ColumnCollection
        Dim samecolname As Boolean = False
        duplicateColName = False
        hdrDataTable = New DataTable
        hdrDataTable.Columns.Add("ColName")
        Dim hdrDataRow As DataRow
        For Each col As Column In ColCollection
            'Read Column Header
            hdrDataRow = hdrDataTable.NewRow
            hdrDataRow(0) = col.ColName
            If hdrDataTable.Rows.Count = 0 Then
                hdrDataTable.Rows.Add(hdrDataRow)
            Else
                'Check same column name
                'if same column name, skip column name
                samecolname = False
                For k As Integer = 0 To hdrDataTable.Rows.Count - 1
                    If hdrDataTable.Rows(k).Item("ColName") = col.ColName Then
                        samecolname = True
                        If duplicateColName = False Then
                            duplicateColName = True
                        End If
                        Exit For
                    End If
                Next
                If samecolname = False Then
                    hdrDataTable.Rows.Add(hdrDataRow)
                End If
            End If
        Next

        colDataTable = New DataTable
        Dim colDataRow As DataRow
        colDataTable.Columns.Add("ColName") '0
        colDataTable.Columns.Add("ColNo") '1
        colDataTable.Columns.Add("ColDataType") '2
        colDataTable.Columns.Add("DataFormat") '3
        colDataTable.Columns.Add("MultiLineEnd") '4
        colDataTable.Columns.Add("Repeat") '5
        colDataTable.Columns.Add("IDNo") '6
        colDataTable.Columns.Add("ColLineNo") '7
        colDataTable.Columns.Add("Start") '8
        colDataTable.Columns.Add("ColLength") '9
        colDataTable.Columns.Add("DelimColNo") '10
        colDataTable.Columns.Add("Delimiter") '11
        colDataTable.Columns.Add("TextQualifier") '12

        For Each col As Column In ColCollection
            'Read Column Detail Value
            colDataRow = colDataTable.NewRow
            colDataRow(0) = col.ColName
            colDataRow(1) = col.ColNo
            colDataRow(2) = col.ColDataType
            colDataRow(3) = col.DataFormat
            colDataRow(4) = col.MultiLineEnd
            colDataRow(5) = col.Repeat
            colDataRow(6) = col.IDNo
            colDataRow(7) = col.ColLineNo
            colDataRow(8) = col.Start
            colDataRow(9) = col.ColLength
            colDataRow(10) = col.DelimColNo
            colDataRow(11) = col.Delimiter
            colDataRow(12) = col.TextQualifier
            colDataTable.Rows.Add(colDataRow)
        Next
    End Sub
    Private Sub ReadSourceFile(ByVal filename As String, ByRef textfile As RichTextBox)
        Dim rhTextBox As New RichTextBox
        Dim tmpstr As String = ""
        'Dim oRead As System.IO.StreamReader
        'Updated By Sandasr 27 Sept 09 For encoding
        Dim encoding As System.Text.Encoding = BTMU.MAGIC.Common.HelperModule.GetFileEncoding(filename)
        Dim oRead As IO.StreamReader = New IO.StreamReader(filename, encoding)
        '
        oRead = File.OpenText(filename)
        tmpstr = oRead.ReadToEnd()
        rhTextBox.Text = tmpstr.Replace(vbCr + vbCrLf, vbCr)
        textfile = rhTextBox


    End Sub

#End Region

    '''Save and Read definition file (encrypt and decrypt file)
#Region "Save And Loading - Serialize And Deserialize"
    'Public Sub SaveToFile(ByVal filename As String)
    '    Dim serializer As New XmlSerializer(GetType(UnstructuredFileConverter))
    '    Dim stream As FileStream = File.Open(filename, FileMode.Create)
    '    serializer.Serialize(stream, Me)
    '    stream.Close()
    'End Sub
    'Public Sub LoadFromFile(ByVal filename As String)
    '    Dim objE As UnstructuredFileConverter
    '    Dim serializer As New XmlSerializer(GetType(UnstructuredFileConverter))
    '    Dim stream As FileStream = File.OpenRead(filename)
    '    objE = DirectCast(serializer.Deserialize(stream), UnstructuredFileConverter)
    '    stream.Close()
    '    Clone(objE)
    'End Sub

    Public Sub SaveToFile(ByVal filename As String)
        Dim rawFileName As String = filename + ".tmp"
        Dim serializer As New XmlSerializer(GetType(UnstructuredFileConverter))
        Dim stream As FileStream = File.Open(rawFileName, FileMode.Create)
        serializer.Serialize(stream, Me)
        stream.Close()

        'Encrypt to temp file 
        Dim rawFile As System.IO.File
        Dim rawReader As System.IO.StreamReader
        rawReader = rawFile.OpenText(rawFileName)
        Dim encryptText As String = BTMU.MAGIC.Common.HelperModule.AESEncrypt(rawReader.ReadToEnd)
        rawReader.Close()
        File.WriteAllText(filename, encryptText)
        File.Delete(rawFileName)
    End Sub

    'Commented by Regina on 14 Sep 2009 - Replace with Fujitsu code to use memorystream instead
    'Public Sub LoadFromFile(ByVal filename As String)
    '    Dim decryptFilePath As String = filename + ".tmpRead"

    '    Dim rawFile As System.IO.File
    '    Dim rawReader As System.IO.StreamReader
    '    rawReader = rawFile.OpenText(filename)
    '    Dim decryptText As String = BTMU.MAGIC.Common.HelperModule.AESDecrypt(rawReader.ReadToEnd)
    '    rawReader.Close()
    '    File.WriteAllText(decryptFilePath, decryptText)


    '    Dim objE As UnstructuredFileConverter
    '    Dim serializer As New XmlSerializer(GetType(UnstructuredFileConverter))
    '    Dim stream As FileStream = File.OpenRead(decryptFilePath)
    '    objE = DirectCast(serializer.Deserialize(stream), UnstructuredFileConverter)
    '    stream.Close()
    '    Clone(objE)
    '    File.Delete(decryptFilePath)
    'End Sub

    Public Sub LoadFromFile(ByVal filename As String)
        Dim decryptFilePath As String = filename + ".tmpRead"

        Dim rawFile As System.IO.File
        Dim rawReader As System.IO.StreamReader
        rawReader = rawFile.OpenText(filename)
        Dim decryptText As String = BTMU.MAGIC.Common.HelperModule.AESDecrypt(rawReader.ReadToEnd)
        rawReader.Close()

        Dim stream As New MemoryStream(System.Text.Encoding.UTF8.GetBytes(decryptText))
        stream.Position = 0

        Dim objE As UnstructuredFileConverter
        Dim serializer As New XmlSerializer(GetType(UnstructuredFileConverter))
        'Dim stream As FileStream = File.OpenRead(decryptFilePath)
        objE = DirectCast(serializer.Deserialize(stream), UnstructuredFileConverter)
        'stream.Close()
        Clone(objE)
        'File.Delete(decryptFilePath)
    End Sub

    Protected Sub Clone(ByVal obj As Object)
        Dim objE As UnstructuredFileConverter = DirectCast(obj, UnstructuredFileConverter)
        CreatedDate = objE.CreatedDate
        DefinitionFile = objE.DefinitionFile
        DefinitionDesc = objE.DefinitionDesc
        SourceFileType = objE.SourceFileType

        SourceFile = objE.SourceFile
        Delimiter = objE.Delimiter
        TextQualifier = objE.TextQualifier
        ImportFirstLine = objE.ImportFirstLine

        IdentifierCollection = objE.IdentifierCollection
        ColumnCollection = objE.ColumnCollection
    End Sub
#End Region

    '''Split function for delimiter and text qualifier
#Region "Split Function"
    Public Function IsNothingOrEmptyString(ByVal text As String) As Boolean
        If text Is Nothing Then
            Return True
        ElseIf text.Trim().Length = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    ' <summary>

    ' To split the string based on delimiter and text qualifier

    ' </summary>

    ' <param name="expression">Input Text.</param>

    ' <param name="delimiter">Delimiter, only one character is allowed, Else system throw error.</param>

    ' <param name="qualifier">Qualifier, only one character is allowed, Else system throw error.</param>

    ' <param name="ignoreCase">Whether the split need to ignore the case</param>

    ' <returns></returns>

    ' <remarks></remarks>

    Public Function SplitString(ByVal expression As String, ByVal delimiter As String, ByVal qualifier As String, ByVal ignoreCase As Boolean) As String()



        Dim csvArray As String()

        Dim loopArray As Int32

        Dim statement As String

        Dim options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline



        If IsNothingOrEmptyString(qualifier) Then

            statement = String.Format("{0}", _
                                        Regex.Escape(delimiter))

        Else

            statement = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", _
                                        Regex.Escape(delimiter), Regex.Escape(qualifier))

        End If





        If ignoreCase Then options = options Or RegexOptions.IgnoreCase

        Dim _Expression As Regex = New Regex(statement, options)



        csvArray = _Expression.Split(expression)

        ' Strip off qualifier (if the entry was qualifier)

        For loopArray = 0 To UBound(csvArray)

            csvArray(loopArray) = Trim(csvArray(loopArray))

            If csvArray(loopArray) <> "" Then

                If qualifier IsNot Nothing Then

                    If Left(csvArray(loopArray), 1) = qualifier And Right(csvArray(loopArray), 1) = qualifier Then

                        csvArray(loopArray) = Trim(Mid(csvArray(loopArray), 2, Len(csvArray(loopArray)) - 2))

                        'If qualifier is double quote and there are two double quote then remove the one of double quote

                        If qualifier = """" Then

                            csvArray(loopArray) = Replace(csvArray(loopArray), """""", """")

                        ElseIf qualifier = "'" Then 'If qualifier is single quote and there are two single quote then remove the one of single quote

                            csvArray(loopArray) = Replace(csvArray(loopArray), "''", "'")

                        End If

                    End If

                End If

            End If

        Next

        Return csvArray

    End Function

#End Region
End Class

