'*******************************************************************************
'Reference Link:
'http://www.lammertbies.nl/comm/info/crc-calculation.html
'Retrieved from above link on Aug 2011
'*******************************************************************************

Imports System
Imports System.Security.Cryptography

Public Class FHK_CRC32
    'Imports System.Security.Cryptography

    '    Public Class Crc32
    'Inherits HashAlgorithm
    Public Const P_32 As ULong = &HEDB88320UL

    Private crc_tab32(256) As ULong
    Private crc_tab32_init As Boolean = False
    'Private Shared defaultTable As ULong()

    Public Sub New()
        'init_crc32_tab()

    End Sub

    Public Function update_crc_32(ByVal crc As ULong, ByVal c As Char) As ULong
        Dim tmp, long_c As ULong
        long_c = &HFFL And CType(AscW(c), ULong)

        If Not crc_tab32_init Then
            init_crc32_tab()
        End If

        tmp = crc Xor long_c
        crc = (crc >> 8) Xor crc_tab32(tmp And &HFF)

        Return crc

    End Function

    Private Sub init_crc32_tab()
        Dim i, j As Integer
        Dim crc As ULong
        i = 0
        For i = 0 To 255
            crc = CType(i, ULong)

            For j = 0 To 7
                If crc And &H1L Then
                    crc = (crc >> 1) Xor P_32
                Else
                    crc = crc >> 1
                End If
            Next
            crc_tab32(i) = crc
        Next

        crc_tab32_init = True

    End Sub

End Class
