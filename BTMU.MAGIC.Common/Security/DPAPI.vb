Imports System
Imports System.Security.Cryptography

Public Class DPAPI
    Private Shared s_aditionalEntropy As Byte() = {9, 8, 7, 6, 5}


    Public Shared Function Protect(ByVal data() As Byte) As Byte()
        Try
            ' Encrypt the data using DataProtectionScope.CurrentUser. The result can be decrypted
            '  only by the same current user.
            Return ProtectedData.Protect(data, s_aditionalEntropy, DataProtectionScope.LocalMachine)
        Catch e As CryptographicException
            Throw New Exception("Data was not encrypted. An error occurred." & Environment.NewLine & e.Message)
            Return Nothing
        End Try

    End Function


    Public Shared Function Unprotect(ByVal data() As Byte) As Byte()
        Try
            'Decrypt the data using DataProtectionScope.CurrentUser.
            Return ProtectedData.Unprotect(data, s_aditionalEntropy, DataProtectionScope.LocalMachine)
        Catch e As CryptographicException
            Throw New Exception("Data was not decrypted. An error occurred." & Environment.NewLine & e.Message)
            Return Nothing
        End Try

    End Function

End Class
