Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.IO
Imports System.Security.Cryptography
Imports System.Management
Imports System.Data
Imports System.Data.SqlServerCe
Imports System.Xml

Public Module HelperModule
    Public enUSCulture As New Globalization.CultureInfo("en-US")

    Public Function BinarySearchCI(ByVal uarray As System.Array, ByVal uvalue As String) As Integer

        For Each tmpvalue As String In uarray
            If uvalue.Equals(tmpvalue, StringComparison.InvariantCultureIgnoreCase) Then Return 1
        Next

        Return -1

    End Function

   
    Public Function EncrypteClientFileUsingGnuPGWrapper(ByVal sInputString As String, ByVal shomeDirectory As String) As String
        Try

            Dim sOutput As String = ""
            Dim gpg As Cryptography.GnuPG.GnuPGWrapper = New Cryptography.GnuPG.GnuPGWrapper

            'set GnuPG properties 
            gpg.homedirectory = shomeDirectory
            gpg.passphrase = "enjoybankingwithbtm"
            gpg.originator = "EPS_Client" ' "btmaeps@btma.com.au" ' no originator needed for decryption only
            gpg.recipient = "EPS_Centre" '"btmaeps@btma.com.au" ' no recipient needed for decryption only

            gpg.command = Cryptography.GnuPG.Commands.SignAndEncrypt

            'Execute the GnuPG command 
            gpg.ExecuteCommand(sInputString, sOutput)

            If gpg.exitcode <> 0 Then
                Throw New ApplicationException("Error found during the payment transmission encryption")
            End If

            Return sOutput

        Catch ex As Exception
            Throw

        End Try
    End Function


    Public Function XMLDecode(ByVal xmlstring As String) As String

        If IsNothingOrEmptyString(xmlstring) Then Return xmlstring

        Dim xmlreader As New XmlTextReader(New StringReader(String.Format("<value>{0}<value>", xmlstring)))
        xmlreader.Read()
        Dim result As String = xmlreader.ReadString()
        xmlreader.Close()

        Return result

    End Function

    Public Sub ArchiveLogFile()
        Dim logFolder As String = GetLogFolder()
        Dim archiveFolder As String = GetLogFolder() & GetArchieveLogFolder()

        Dim files As String() = Directory.GetFiles(logFolder, "*.log")
        Dim lst As New List(Of String)(files)
        Dim lastYearLog As String = logFolder & "\" & DateTime.Now.AddYears(-1).ToString("yyyyMMdd", enUSCulture) & ".log"

        lst.Sort()

        For Each file As String In lst
            If file < lastYearLog Then
                Dim flInfo As FileInfo = New FileInfo(file)
                System.IO.File.Move(file, archiveFolder & "\" & flInfo.Name)
            End If
        Next

    End Sub
    Public Sub DeleteArchiveLogFile()
        Dim archiveFolder As String = GetLogFolder() & GetArchieveLogFolder()

        Dim files As String() = Directory.GetFiles(archiveFolder, "*.log")
        Dim lst As New List(Of String)(files)
        Dim lastTwoYearsLog As String = archiveFolder & "\" & DateTime.Now.AddYears(-2).ToString("yyyyMMdd", enUSCulture) & ".log"

        lst.Sort()

        For Each file As String In lst
            If file < lastTwoYearsLog Then
                System.IO.File.Delete(file)
            End If
        Next

    End Sub

    Private Function GetCurrentFileName() As String
        Return DateTime.Now.ToString("yyyyMMdd", enUSCulture) & ".log"
    End Function

    '1. Empty Customer Field names are replaced by Field<running number>. E.g: Field1, Field2, ...
    '2. Duplicate Customer Field names are replaced by Field_<running number>. E.g: Field1, Field1_1,....
    Public Sub FormatMapSourceFields(ByRef SourceFields As List(Of String))

        '1.

        For Counter As Int32 = 0 To SourceFields.Count - 1
            If IsNothingOrEmptyString(SourceFields.Item(Counter)) Then
                SourceFields.Item(Counter) = String.Format("Field {0}", Counter + 1)
            End If
        Next

        '2.
        Dim runningNum As Int32 = 0
        For I As Int32 = 0 To SourceFields.Count - 2
            runningNum = 1
            For J As Int32 = I + 1 To SourceFields.Count - 1
                If SourceFields.Item(I) = SourceFields.Item(J) Then
                    SourceFields.Item(J) = String.Format("{0}_{1}", SourceFields.Item(J), runningNum)
                    runningNum += 1
                End If
            Next
        Next

    End Sub

    Public Function EscapeSingleQuote(ByVal StringValue As String) As String

        If IsNothingOrEmptyString(StringValue) Then Return StringValue
        Return StringValue.Replace("'", "''")

    End Function

    Public Function EscapeSpecialCharsForColumnName(ByVal ColumnName As String) As String

        If IsNothingOrEmptyString(ColumnName) Then Return ColumnName

        Return String.Format("[{0}]", ColumnName.Replace("]", "\]"))

    End Function


    Public Function GetFileEncoding(ByVal FileName As String) As System.Text.Encoding
        Dim Result As System.Text.Encoding = Nothing

        Dim FI As New FileInfo(FileName)

        Dim FS As FileStream = Nothing

        Try
            FS = FI.OpenRead()

            'edited by fhk on 28-aug-2015, detect correct Encoding by rearrange the encoding sequence
            'Dim UnicodeEncodings As System.Text.Encoding() = {System.Text.Encoding.GetEncoding(874), System.Text.Encoding.BigEndianUnicode, System.Text.Encoding.Unicode, System.Text.Encoding.UTF8, System.Text.Encoding.GetEncoding("Shift-JIS")}
            Dim UnicodeEncodings As System.Text.Encoding() = {System.Text.Encoding.UTF8, System.Text.Encoding.BigEndianUnicode, System.Text.Encoding.Unicode, System.Text.Encoding.GetEncoding(874), System.Text.Encoding.GetEncoding("Shift-JIS")}
            'end by fhk on 28-aug-2015

            Dim i As Integer = 0

            While Result Is Nothing AndAlso i < UnicodeEncodings.Length
                FS.Position = 0

                Dim Preamble As Byte() = UnicodeEncodings(i).GetPreamble()

                Dim PreamblesAreEqual As Boolean = True

                Dim j As Integer = 0
                While PreamblesAreEqual AndAlso j < Preamble.Length
                    PreamblesAreEqual = Preamble(j) = FS.ReadByte()
                    j += 1
                End While

                If PreamblesAreEqual Then
                    Result = UnicodeEncodings(i)
                End If
                i += 1
            End While
        Catch generatedExceptionName As System.IO.IOException
        Finally
            If FS IsNot Nothing Then
                FS.Close()
            End If
        End Try

        If Result Is Nothing Then
            Result = System.Text.Encoding.UTF8
        End If

        Return Result
    End Function





    ''' <summary>
    ''' Executes the given Query and returns a scalar value
    ''' </summary>
    ''' <param name="strQuery">Query to Execute and return a scalar value</param>
    ''' <returns>returns the value for 1st column at 1st row, if there is a resultset</returns>
    ''' <remarks></remarks>
    Public Function QueryTable(ByVal strQuery As String, ByVal strConnectionString As String) As String

        Dim dtable As New DataTable

        Dim result As New List(Of String)

        Dim dbCEConnection As New SqlCeConnection(strConnectionString)
        dbCEConnection.Open()

        Try

            Dim dbCECommand As SqlCeCommand = dbCEConnection.CreateCommand()
            dbCECommand.CommandText = strQuery
            dtable.Load(dbCECommand.ExecuteReader(CommandBehavior.SingleRow))

        Catch ex As Exception
            Throw ex
        Finally
            dbCEConnection.Close()
        End Try

        If dtable.Rows.Count > 0 Then Return dtable.Rows(0)(0)

        Return ""

    End Function

    Public Function IsNothingOrEmptyString(ByVal text As String) As Boolean
        If text Is Nothing Then
            Return True
        ElseIf text.Trim().Length = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsNothingOrEmptyString(ByVal text As Object) As Boolean
        If text Is Nothing Then Return True

        If text Is DBNull.Value Then Return True
        Return IsNothingOrEmptyString(text.ToString())
    End Function


    '''' <summary>
    '''' To split the string based on delimiter and text qualifier
    '''' </summary>
    '''' <param name="expression">Input Text.</param>
    '''' <param name="delimiter">Delimiter.</param>
    '''' <param name="qualifier">Qualifier, only one character is allowed, Else system throw error.</param>
    '''' <returns></returns>
    '''' <remarks></remarks>

    'Public Function Split(ByVal expression As String, ByVal delimiter As String, ByVal qualifier As String, ByVal ignoreCase As Boolean) As String()
    '    Dim csvArray As String() = Nothing

    '    If expression.Trim.Length > 0 Then
    '        Dim loopArray As Int32
    '        Dim statement As String
    '        Dim options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline

    '        If IsNothingOrEmptyString(qualifier) Then
    '            statement = String.Format("{0}", _
    '                                        Regex.Escape(delimiter))
    '        Else
    '            statement = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", _
    '                                        Regex.Escape(delimiter), Regex.Escape(qualifier))
    '        End If

    '        If ignoreCase Then options = options Or RegexOptions.IgnoreCase
    '        Dim _Expression As Regex = New Regex(statement, options)

    '        csvArray = _Expression.Split(expression)
    '        ' Strip off qualifier (if the entry was qualifier)
    '        For loopArray = 0 To UBound(csvArray)
    '            csvArray(loopArray) = Trim(csvArray(loopArray))
    '            If csvArray(loopArray) <> "" Then
    '                If qualifier IsNot Nothing Then
    '                    If Left(csvArray(loopArray), 1) = qualifier And Right(csvArray(loopArray), 1) = qualifier Then
    '                        csvArray(loopArray) = Trim(Mid(csvArray(loopArray), 2, Len(csvArray(loopArray)) - 2))
    '                        'If qualifier is double quote and there are two double quote then remove the one of double quote
    '                        If qualifier = """" Then
    '                            csvArray(loopArray) = Replace(csvArray(loopArray), """""", """")
    '                        ElseIf qualifier = "'" Then 'If qualifier is single quote and there are two single quote then remove the one of single quote
    '                            csvArray(loopArray) = Replace(csvArray(loopArray), "''", "'")
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        Next
    '    End If

    '    Return csvArray

    'End Function

    Private _cacheRegexList As New SortedDictionary(Of String, Regex)
    ''' <summary>
    ''' To split the string based on delimiter and text qualifier
    ''' </summary>
    ''' <param name="expression">Input Text.</param>
    ''' <param name="delimiter">Delimiter.</param>
    ''' <param name="qualifier">Qualifier, only one character is allowed, Else system throw error.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Split(ByVal expression As String, ByVal delimiter As String, ByVal qualifier As String, ByVal ignoreCase As Boolean) As String()
        Dim csvArray As String() = Nothing

        If expression.Trim.Length > 0 Then
            Dim loopArray As Int32
            Dim statement As String
            Dim options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline

            If IsNothingOrEmptyString(qualifier) Then
                statement = String.Format("{0}", _
                                            Regex.Escape(delimiter))
            Else
                statement = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", _
                                            Regex.Escape(delimiter), Regex.Escape(qualifier))
            End If

            If ignoreCase Then options = options Or RegexOptions.IgnoreCase Or RegexOptions.Compiled

            If Not _cacheRegexList.ContainsKey(statement & ignoreCase.ToString()) Then
                _cacheRegexList.Add(statement & ignoreCase.ToString(), New Regex(statement, options))
            End If

            Dim _Expression As Regex = _cacheRegexList(statement & ignoreCase.ToString())

            csvArray = _Expression.Split(expression)
            ' Strip off qualifier (if the entry was qualifier)
            For loopArray = 0 To UBound(csvArray)
                csvArray(loopArray) = (csvArray(loopArray))
                If csvArray(loopArray) <> "" Then
                    If qualifier IsNot Nothing Then
                        If Left(csvArray(loopArray), 1) = qualifier And Right(csvArray(loopArray), 1) = qualifier Then
                            csvArray(loopArray) = (Mid(csvArray(loopArray), 2, Len(csvArray(loopArray)) - 2))
                            'If qualifier is double quote and there are two double quote then remove the one of double quote
                            If qualifier = """" Then
                                csvArray(loopArray) = Replace(csvArray(loopArray), """""", """")
                            ElseIf qualifier = "'" Then 'If qualifier is single quote and there are two single quote then remove the one of single quote
                                csvArray(loopArray) = Replace(csvArray(loopArray), "''", "'")
                            End If
                        End If
                    End If
                End If
            Next
        End If

        Return csvArray

    End Function

    Public Function IsDateDataType(ByVal value As String, ByVal dateFormat As String, ByVal isLeadingZero As Boolean) As Boolean
        Dim provider As CultureInfo = CultureInfo.InvariantCulture
        Try
            'modified by fujitsu 2013/03/21
            'dateFormat = dateFormat.Replace("YYYY", "yyyy").Replace("YY", "yy").Replace("DD", "dd")
            dateFormat = dateFormat.Replace("YYYY", "yyyy").Replace("YY", "yy").Replace("D", "d")
            If isLeadingZero Then
                dateFormat = dateFormat.Replace("dd", "d").Replace("MM", "M")
            End If
            Dim temp As DateTime = DateTime.ParseExact(value, dateFormat, provider)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function IsDateDataType(ByVal value As String, ByVal dateSeparator As String, ByVal dateFormat As String, ByVal isLeadingZero As Boolean) As Boolean
        Dim dummy As Date
        Return IsDateDataType(value, dateSeparator, dateFormat, dummy, isLeadingZero)
    End Function

    Public Function IsDateDataType(ByVal value As String, ByVal dateSeparator As String, ByVal dateFormat As String, ByRef output As Date, ByVal isLeadingZero As Boolean) As Boolean

        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        Try
            If isLeadingZero Then
                Select Case dateFormat
                    Case "DDMMYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "ddMMyy"
                        Else
                            dateFormat = "dd" & dateSeparator & "MM" & dateSeparator & "yy"
                        End If
                    Case "DDMMMYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "ddMMMyy"
                        Else
                            dateFormat = "dd" & dateSeparator & "MMM" & dateSeparator & "yy"
                        End If

                    Case "DDMMYYYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "ddMMyyyy"
                        Else
                            dateFormat = "dd" & dateSeparator & "MM" & dateSeparator & "yyyy"
                        End If
                    Case "DDMMMYYYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "ddMMMyyyy"
                        Else
                            dateFormat = "dd" & dateSeparator & "MMM" & dateSeparator & "yyyy"
                        End If

                        'Added for (HK-RTMS formats)
                    Case "DD-MM-YYYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "dd-MM-yyyy"
                        Else
                            dateFormat = "dd" & "-" & "MM" & "-" & "yyyy"
                        End If

                    Case "MMDDYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "MMddyy"
                        Else
                            dateFormat = "MM" & dateSeparator & "dd" & dateSeparator & "yy"
                        End If
                    Case "MMMDDYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "MMMddyy"
                        Else
                            dateFormat = "MMM" & dateSeparator & "dd" & dateSeparator & "yy"
                        End If
                    Case "MMDDYYYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "MMddyyyy"
                        Else
                            dateFormat = "MM" & dateSeparator & "dd" & dateSeparator & "yyyy"
                        End If
                    Case "MMMDDYYYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "MMMddyyyy"
                        Else
                            dateFormat = "MMM" & dateSeparator & "dd" & dateSeparator & "yyyy"
                        End If
                    Case "YYMMDD"
                        If dateSeparator = "No Space" Then
                            dateFormat = "yyMMdd"
                        Else
                            dateFormat = "yy" & dateSeparator & "MM" & dateSeparator & "dd"
                        End If
                    Case "YYYYDDMM"
                        If dateSeparator = "No Space" Then
                            dateFormat = "yyyyddMM"
                        Else
                            dateFormat = "yyyy" & dateSeparator & "dd" & dateSeparator & "MM"
                        End If
                    Case "YYYYMMDD"
                        If dateSeparator = "No Space" Then
                            dateFormat = "yyyyMMdd"
                        Else
                            dateFormat = "yyyy" & dateSeparator & "MM" & dateSeparator & "dd"
                        End If

                        'CARROT formats(Added by FHK 2013-03-05)
                    Case "YYYY/MM/DD"
                        If dateSeparator = "No Space" Then
                            dateFormat = "yyyy/MM/dd"
                        Else
                            dateFormat = "yyyy" & "/" & "MM" & "/" & "dd"
                        End If
                    Case "DD/MM/YYYY"
                        dateFormat = "dd/MM/yyyy"
                    Case "D/M/YYYY"
                        dateFormat = "d/M/yyyy"
                    Case "MM/DD/YYYY"
                        dateFormat = "MM/dd/yyyy"
                    Case "YYYY/M/D"
                        dateFormat = "yyyy/M/d"
                    Case "DDMMYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "ddMMyy"
                        Else
                            dateFormat = "dd" & dateSeparator & "MM" & dateSeparator & "yy"
                        End If
                    Case "MMDDYY"
                        If dateSeparator = "No Space" Then
                            dateFormat = "MMddyy"
                        Else
                            dateFormat = "MM" & dateSeparator & "dd" & dateSeparator & "yy"
                        End If
                    Case "YYMMDD"
                        If dateSeparator = "No Space" Then
                            dateFormat = "yyMMdd"
                        Else
                            dateFormat = "yy" & dateSeparator & "MM" & dateSeparator & "dd"
                        End If
                End Select

            Else
                Select Case dateFormat
                    Case "DDMMYY"
                        dateFormat = "d" & dateSeparator & "M" & dateSeparator & "y"
                    Case "DDMMMYY"
                        dateFormat = "d" & dateSeparator & "MMM" & dateSeparator & "y"
                    Case "DDMMYYYY"
                        dateFormat = "d" & dateSeparator & "M" & dateSeparator & "yyyy"

                        'Added for (HK-RTMS formats)
                    Case "DD-MM-YYYY"
                        dateFormat = "d" & "-" & "M" & "-" & "yyyy"

                    Case "DDMMMYYYY"
                        dateFormat = "d" & dateSeparator & "MMM" & dateSeparator & "yyyy"
                    Case "MMDDYY"
                        dateFormat = "M" & dateSeparator & "d" & dateSeparator & "y"
                    Case "MMMDDYY"
                        dateFormat = "MMM" & dateSeparator & "d" & dateSeparator & "y"
                    Case "MMDDYYYY"
                        dateFormat = "M" & dateSeparator & "d" & dateSeparator & "yyyy"
                    Case "MMMDDYYYY"
                        dateFormat = "MMM" & dateSeparator & "d" & dateSeparator & "yyyy"
                    Case "YYMMDD"
                        dateFormat = "y" & dateSeparator & "M" & dateSeparator & "d"
                    Case "YYYYDDMM"
                        dateFormat = "yyyy" & dateSeparator & "d" & dateSeparator & "M"
                    Case "YYYYMMDD"
                        dateFormat = "yyyy" & dateSeparator & "M" & dateSeparator & "d"

                        'CARROT formats(Added by FHK 2013-03-05)
                    Case "YYYY/MM/DD"
                        dateFormat = "yyyy" & dateSeparator & "M" & dateSeparator & "d"

                    Case "DDMMYY"
                        dateFormat = "d" & dateSeparator & "M" & dateSeparator & "y"
                    Case "MMDDYY"
                        dateFormat = "M" & dateSeparator & "d" & dateSeparator & "y"
                    Case "YYMMDD"
                        dateFormat = "y" & dateSeparator & "M" & dateSeparator & "d"
                End Select
            End If

            value = value.Replace("""", "").Replace("'", "")
            'If value.Length + 1 = dateFormat.Length Then value = "0" & value
            Dim temp As Date = DateTime.ParseExact(value, dateFormat, provider)
            output = temp
            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function


    Public Function IsDecimalDataType(ByVal value As String, ByVal decimalSeparator As String, ByVal thousandSeparator As String, ByRef output As Decimal) As Boolean

        'chop off the enclosures
        If Not IsNothingOrEmptyString(value) Then
            If value.StartsWith("""") Or value.StartsWith("'") Then
                value = value.Remove(0, 1)
            End If
            If value.EndsWith("""") Or value.EndsWith("'") Then
                value = value.Remove(value.Length - 1, 1)
            End If
        End If


        If value.Contains(".") AndAlso (decimalSeparator = "No Separator" Or decimalSeparator = "") Then Return False

        Dim provider As New NumberFormatInfo()
        'Decimal Separator Cannot be empty!
        provider.NumberDecimalSeparator = IIf(decimalSeparator = "No Separator", ".", decimalSeparator)
        provider.NumberGroupSeparator = IIf(thousandSeparator = "No Separator", String.Empty, thousandSeparator)

        Try

            'Return Decimal.TryParse(value, provider, output)
            Return Decimal.TryParse(value, NumberStyles.Number, provider, output)
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function AESEncrypt(ByVal plainText As String) As String
        Dim cipherText As String
        Dim passPhrase As String
        Dim saltValue As String
        Dim hashAlgorithm As String
        Dim passwordIterations As Integer
        Dim initVector As String
        Dim keySize As Integer

        passPhrase = "Pas5pr@se"        ' can be any string
        saltValue = "s@1tValue"        ' can be any string
        hashAlgorithm = "SHA1"             ' can be "MD5"
        passwordIterations = 2                  ' can be any number
        initVector = "@1B2c3D4e5F6g7H8" ' must be 16 bytes
        keySize = 256                ' can be 192 or 128


        cipherText = BTMU.MAGIC.Common.AESRijndael.Encrypt(plainText, _
                                               passPhrase, _
                                               saltValue, _
                                               hashAlgorithm, _
                                               passwordIterations, _
                                               initVector, _
                                               keySize)
        Return cipherText
    End Function



    Public Function AESDecrypt(ByVal cipherText As String) As String
        Dim plainText As String
        Dim passPhrase As String
        Dim saltValue As String
        Dim hashAlgorithm As String
        Dim passwordIterations As Integer
        Dim initVector As String
        Dim keySize As Integer

        passPhrase = "Pas5pr@se"        ' can be any string
        saltValue = "s@1tValue"        ' can be any string
        hashAlgorithm = "SHA1"             ' can be "MD5"
        passwordIterations = 2                  ' can be any number
        initVector = "@1B2c3D4e5F6g7H8" ' must be 16 bytes
        keySize = 256                ' can be 192 or 128


        plainText = BTMU.MAGIC.Common.AESRijndael.Decrypt(cipherText, _
                                                       passPhrase, _
                                                       saltValue, _
                                                       hashAlgorithm, _
                                                       passwordIterations, _
                                                       initVector, _
                                                       keySize)
        Return plainText
    End Function

    ' <summary>
    ' Generates a hash for the given plain text value and returns a
    ' base64-encoded result. Before the hash is computed, a random salt
    ' is generated and appended to the plain text. This salt is stored at
    ' the end of the hash value, so it can be used later for hash
    ' verification.
    ' </summary>
    ' <param name="plainText">
    ' Plaintext value to be hashed. The function does not check whether
    ' this parameter is null.
    ' </param>
    ' < name="hashAlgorithm">
    ' Name of the hash algorithm. Allowed values are: "MD5", "SHA1",
    ' "SHA256", "SHA384", and "SHA512" (if any other value is specified
    ' MD5 hashing algorithm will be used). This value is case-insensitive.
    ' </param>
    ' < name="saltBytes">
    ' Salt bytes. This parameter can be null, in which case a random salt
    ' value will be generated.
    ' </param>
    ' <returns>
    ' Hash value formatted as a base64-encoded string.
    ' </returns>
    Public Function ComputeHash(ByVal plainText As String, _
                                       ByVal hashAlgorithm As String, _
                                       ByVal saltBytes() As Byte) _
                           As String

        ' If salt is not specified, generate it on the fly.
        If (saltBytes Is Nothing) Then

            ' Define min and max salt sizes.
            Dim minSaltSize As Integer
            Dim maxSaltSize As Integer

            minSaltSize = 4
            maxSaltSize = 8

            ' Generate a random number for the size of the salt.
            Dim random As Random
            random = New Random()

            Dim saltSize As Integer
            saltSize = random.Next(minSaltSize, maxSaltSize)

            ' Allocate a byte array, which will hold the salt.
            saltBytes = New Byte(saltSize - 1) {}

            ' Initialize a random number generator.
            Dim rng As RNGCryptoServiceProvider
            rng = New RNGCryptoServiceProvider()

            ' Fill the salt with cryptographically strong byte values.
            rng.GetNonZeroBytes(saltBytes)
        End If

        ' Convert plain text into a byte array.
        Dim plainTextBytes As Byte()
        plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText)

        ' Allocate array, which will hold plain text and salt.
        Dim plainTextWithSaltBytes() As Byte = _
            New Byte(plainTextBytes.Length + saltBytes.Length - 1) {}

        ' Copy plain text bytes into resulting array.
        Dim I As Integer
        For I = 0 To plainTextBytes.Length - 1
            plainTextWithSaltBytes(I) = plainTextBytes(I)
        Next I

        ' Append salt bytes to the resulting array.
        For I = 0 To saltBytes.Length - 1
            plainTextWithSaltBytes(plainTextBytes.Length + I) = saltBytes(I)
        Next I

        ' Because we support multiple hashing algorithms, we must define
        ' hash object as a common (abstract) base class. We will specify the
        ' actual hashing algorithm class later during object creation.
        Dim hash As HashAlgorithm

        ' Make sure hashing algorithm name is specified.
        If (hashAlgorithm Is Nothing) Then
            hashAlgorithm = ""
        End If

        ' Initialize appropriate hashing algorithm class.
        Select Case hashAlgorithm.ToUpper()

            Case "SHA1"
                hash = New SHA1Managed()

            Case "SHA256"
                hash = New SHA256Managed()

            Case "SHA384"
                hash = New SHA384Managed()

            Case "SHA512"
                hash = New SHA512Managed()

            Case Else
                hash = New MD5CryptoServiceProvider()

        End Select

        ' Compute hash value of our plain text with appended salt.
        Dim hashBytes As Byte()
        hashBytes = hash.ComputeHash(plainTextWithSaltBytes)

        ' Create array which will hold hash and original salt bytes.
        Dim hashWithSaltBytes() As Byte = _
                                   New Byte(hashBytes.Length + _
                                            saltBytes.Length - 1) {}

        ' Copy hash bytes into resulting array.
        For I = 0 To hashBytes.Length - 1
            hashWithSaltBytes(I) = hashBytes(I)
        Next I

        ' Append salt bytes to the result.
        For I = 0 To saltBytes.Length - 1
            hashWithSaltBytes(hashBytes.Length + I) = saltBytes(I)
        Next I

        ' Convert result into a base64-encoded string.
        Dim hashValue As String
        hashValue = Convert.ToBase64String(hashWithSaltBytes)

        ' Return the result.
        ComputeHash = hashValue
    End Function

    ' <summary>
    ' Generates a hash for the given plain text value and returns a
    ' hexa string result. 
    ' </summary>
    ' <param name="plainText">
    ' Plaintext value to be hashed. The function does not check whether
    ' this parameter is null.
    ' </param>
    ' < name="hashAlgorithm">
    ' Name of the hash algorithm. Allowed values are: "MD5", "SHA1",
    ' "SHA256", "SHA384", and "SHA512" (if any other value is specified
    ' MD5 hashing algorithm will be used). This value is case-insensitive.
    ' </param>
    ' <returns>
    ' Hash value formatted as a hexadecimal string.
    ' </returns>

    Public Function ComputeHashAsHexString(ByVal plainText As String, _
                                       ByVal hashAlgorithm As String) As String

        ' Convert plain text into a byte array.
        Dim plainTextBytes As Byte()

        plainTextBytes = System.Text.Encoding.ASCII.GetBytes(plainText)
        ' Because we support multiple hashing algorithms, we must define
        ' hash object as a common (abstract) base class. We will specify the
        ' actual hashing algorithm class later during object creation.
        Dim hash As HashAlgorithm

        ' Make sure hashing algorithm name is specified.
        If (hashAlgorithm Is Nothing) Then
            hashAlgorithm = ""
        End If

        ' Initialize appropriate hashing algorithm class.
        Select Case hashAlgorithm.ToUpper()
            Case "SHA1"
                hash = New SHA1Managed()
            Case "SHA256"
                hash = New SHA256Managed()
            Case "SHA384"
                hash = New SHA384Managed()
            Case "SHA512"
                hash = New SHA512Managed()
            Case Else
                hash = New MD5CryptoServiceProvider()
        End Select

        ' Compute hash value of our plain text with appended salt.
        Dim hashBytes As Byte()

        hashBytes = hash.ComputeHash(plainTextBytes)

        ' Convert result into a hexadecimal string
        Dim hashValue As New System.Text.StringBuilder

        For Each hashbyte As Byte In hashBytes
            hashValue.Append(hashbyte.ToString("X2"))
        Next

        ' Return the result.
        ComputeHashAsHexString = hashValue.ToString.ToLower

    End Function
    ''' <summary>
    ''' This function returns the hash value for a given byte array
    ''' </summary>
    ''' <param name="plainTextBytes">Byte array for which the hash is to be generated</param>
    ''' <param name="hashAlgorithm">Name of the hash algorithm. Allowed values are: "MD5", "SHA1",
    ''' SHA256", "SHA384", and "SHA512" (if any other value is specified
    ''' MD5 hashing algorithm will be used). This value is case-insensitive.</param>
    ''' <returns>Hash value formatted as Hexadecimal string</returns>
    ''' <remarks></remarks>
    Public Function ComputeHashAsHexString(ByVal plainTextBytes As Byte(), _
                                           ByVal hashAlgorithm As String) As String
        ' Because we support multiple hashing algorithms, we must define
        ' hash object as a common (abstract) base class. We will specify the
        ' actual hashing algorithm class later during object creation.
        Dim hash As HashAlgorithm

        ' Make sure hashing algorithm name is specified.
        If (hashAlgorithm Is Nothing) Then
            hashAlgorithm = ""
        End If

        ' Initialize appropriate hashing algorithm class.
        Select Case hashAlgorithm.ToUpper()
            Case "SHA1"
                hash = New SHA1Managed()
            Case "SHA256"
                hash = New SHA256Managed()
            Case "SHA384"
                hash = New SHA384Managed()
            Case "SHA512"
                hash = New SHA512Managed()
            Case Else
                hash = New MD5CryptoServiceProvider()
        End Select

        ' Compute hash value of byte array.
        Dim hashBytes As Byte()

        hashBytes = hash.ComputeHash(plainTextBytes)

        ' Convert result into a hexadecimal string
        Dim hashValue As New System.Text.StringBuilder

        For Each hashbyte As Byte In hashBytes
            hashValue.Append(hashbyte.ToString("X2"))
        Next

        ' Return the result.
        ComputeHashAsHexString = hashValue.ToString.ToLower

    End Function
    ' <summary>
    ' Compares a hash of the specified plain text value to a given hash
    ' value. Plain text is hashed with the same salt value as the original
    ' hash.
    ' </summary>
    ' <param name="plainText">
    ' Plain text to be verified against the specified hash. The function
    ' does not check whether this parameter is null.
    ' </param>
    ' < name="hashAlgorithm">
    ' Name of the hash algorithm. Allowed values are: "MD5", "SHA1",
    ' "SHA256", "SHA384", and "SHA512" (if any other value is specified
    ' MD5 hashing algorithm will be used). This value is case-insensitive.
    ' </param>
    ' < name="hashValue">
    ' Base64-encoded hash value produced by ComputeHash function. This value
    ' includes the original salt appended to it.
    ' </param>
    ' <returns>
    ' If computed hash mathes the specified hash the function the return
    ' value is true; otherwise, the function returns false.
    ' </returns>
    Public Function VerifyHash(ByVal plainText As String, _
                                      ByVal hashAlgorithm As String, _
                                      ByVal hashValue As String) _
                           As Boolean

        ' Convert base64-encoded hash value into a byte array.
        Dim hashWithSaltBytes As Byte()
        hashWithSaltBytes = Convert.FromBase64String(hashValue)

        ' We must know size of hash (without salt).
        Dim hashSizeInBits As Integer
        Dim hashSizeInBytes As Integer

        ' Make sure that hashing algorithm name is specified.
        If (hashAlgorithm Is Nothing) Then
            hashAlgorithm = ""
        End If

        ' Size of hash is based on the specified algorithm.
        Select Case hashAlgorithm.ToUpper()

            Case "SHA1"
                hashSizeInBits = 160

            Case "SHA256"
                hashSizeInBits = 256

            Case "SHA384"
                hashSizeInBits = 384

            Case "SHA512"
                hashSizeInBits = 512

            Case Else ' Must be MD5
                hashSizeInBits = 128

        End Select

        ' Convert size of hash from bits to bytes.
        hashSizeInBytes = hashSizeInBits / 8

        ' Make sure that the specified hash value is long enough.
        If (hashWithSaltBytes.Length < hashSizeInBytes) Then
            VerifyHash = False
        End If

        ' Allocate array to hold original salt bytes retrieved from hash.
        Dim saltBytes() As Byte = New Byte(hashWithSaltBytes.Length - _
                                           hashSizeInBytes - 1) {}

        ' Copy salt from the end of the hash to the new array.
        Dim I As Integer
        For I = 0 To saltBytes.Length - 1
            saltBytes(I) = hashWithSaltBytes(hashSizeInBytes + I)
        Next I

        ' Compute a new hash string.
        Dim expectedHashString As String
        expectedHashString = ComputeHash(plainText, hashAlgorithm, saltBytes)

        ' If the computed hash matches the specified hash,
        ' the plain text value must be correct.
        VerifyHash = (hashValue = expectedHashString)
    End Function

    Public Function GetCurrencyDecimalPoint(ByVal strCurrency As String) As Short

        Select Case strCurrency
            Case "JPY", "KRW", "TWD", "WON"
                Return 0
            Case "KWD", "TND"
                Return 3
            Case Else
                Return 2
        End Select

    End Function

    Private Function GetLogFolder() As String
        Return AppDomain.CurrentDomain.BaseDirectory & "\Log"
    End Function

    'Private Function GetCurrentFileName() As String
    '    Return DateTime.Now.ToString("yyyyMMdd") & ".log"
    'End Function

    Private Function GetArchieveLogFolder() As String
        Return "\Archive"
    End Function

    Private Function GetActivityName() As String
        Return "Activity Name		: "
    End Function

    Private Function GetSourceFile() As String
        Return "Source File Name	: "
    End Function

    Private Function GetOutputFile() As String
        Return "Output File Name	: "
    End Function

    Private Function GetActivityDate() As String
        Return "Activity Date		: " & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", enUSCulture)
    End Function

    Public Function GetUserName(ByVal _user As String) As String
        If UCase(_user) = "BKMASTER" Then
            Return "Administrator"
        ElseIf UCase(_user) = "SUPER" Then
            Return "Common Admin"
        ElseIf UCase(_user) = "COMMON" Then
            Return "Common User"
        Else
            Return ""
        End If
    End Function

    Public Sub WriteActivityLog(ByVal _message As String, ByVal _userid As String)
        Dim logfile As FileStream
        Dim data As Byte()
        'BEGIN CHECK EXISTING DIRECTORY
        Dim di As DirectoryInfo = New DirectoryInfo(GetLogFolder)
        If Not di.Exists Then
            di.Create()
        End If
        di = New DirectoryInfo(GetLogFolder() & "\" & GetArchieveLogFolder())
        If Not di.Exists Then
            di.Create()
        End If

        If File.Exists(GetLogFolder() & "\" & GetCurrentFileName()) Then
            logfile = File.Open(GetLogFolder() & "\" & GetCurrentFileName(), FileMode.Append)
        Else
            logfile = File.Create(GetLogFolder() & "\" & GetCurrentFileName())
        End If
        _message = _userid & "|" & _message
        data = System.Text.ASCIIEncoding.ASCII.GetBytes(AESEncrypt(_message) & vbCrLf)

        logfile.Write(data, 0, data.Length)
        logfile.Close()
    End Sub

    Public Sub WriteConvertLog(ByVal _fileName As String, ByVal dateCreated As String, ByVal dateModified As String, ByVal logFileName As String)
        Dim logfile As FileStream
        Dim data As Byte()
        Dim _message As String

        If File.Exists(logFileName) Then
            logfile = File.Open(logFileName, FileMode.Append)
        Else
            logfile = File.Create(logFileName)
        End If
        _message = _fileName & ";" & dateCreated & ";" & dateModified & ";"
        data = System.Text.ASCIIEncoding.ASCII.GetBytes(AESEncrypt(_message) & vbCrLf)

        logfile.Write(data, 0, data.Length)
        logfile.Close()
    End Sub

    Public Function ReadActivityLog(ByVal _fileName As String, ByVal _isArchive As Boolean, ByVal _userid As String) As String
        Dim srFileReader As System.IO.StreamReader
        Dim sInputLine As String = ""
        Dim sOutputLine As String = ""
        Dim sOutputLineTemp As String = ""
        Dim valueArr() As String
        'BEGIN CHECK EXISTING DIRECTORY
        Dim di As DirectoryInfo = New DirectoryInfo(GetLogFolder)
        If Not di.Exists Then
            di.Create()
        End If
        di = New DirectoryInfo(GetLogFolder() & "\" & GetArchieveLogFolder())
        If Not di.Exists Then
            di.Create()
        End If


        Try
            If _isArchive Then
                srFileReader = File.OpenText(GetLogFolder() & "\" & GetArchieveLogFolder() & "\" & _fileName)
            Else
                srFileReader = File.OpenText(GetLogFolder() & "\" & _fileName)
            End If

            sInputLine = srFileReader.ReadLine()
            Do Until sInputLine Is Nothing
                sOutputLineTemp = AESDecrypt(sInputLine) & vbCrLf
                valueArr = sOutputLineTemp.Split("|")
                If UCase(_userid) = "COMMON" AndAlso UCase(valueArr(0)) = "COMMON" Then
                    sOutputLine &= valueArr(1) & vbCrLf
                ElseIf UCase(_userid) = "SUPER" AndAlso (UCase(valueArr(0)) = "SUPER" Or UCase(valueArr(0)) = "COMMON") Then
                    sOutputLine &= valueArr(1) & vbCrLf
                ElseIf UCase(_userid) = "BKMASTER" Then
                    sOutputLine &= valueArr(1) & vbCrLf
                End If
                sInputLine = srFileReader.ReadLine()
            Loop
            srFileReader.Close()
            Return sOutputLine
        Catch ex As FileNotFoundException
            Return "No activity log found."
        End Try

    End Function

    'Activity Log Management
    Public Function ReadConvertLog(ByVal filePath As String, ByVal _fileName As String) As DataTable
        Dim srFileReader As System.IO.StreamReader
        Dim sInputLine As String = ""
        Dim sOutputLine As String = ""
        Dim sOutputLineTemp As String = ""
        Dim convertDataTable As New DataTable
        Dim convertDataRow As DataRow
        Dim valueArr() As String
        convertDataTable.Columns.Add("FileName", GetType(String))
        'convertDataTable.Columns.Add("FilePath", GetType(String))
        convertDataTable.Columns.Add("DateCreated", GetType(String))
        convertDataTable.Columns.Add("DateModified", GetType(String))
        Try
            srFileReader = File.OpenText(_fileName)
            sInputLine = srFileReader.ReadLine()
            Do Until sInputLine Is Nothing
                sOutputLineTemp = AESDecrypt(sInputLine) & vbCrLf
                valueArr = sOutputLineTemp.Split(";")
                convertDataRow = convertDataTable.NewRow
                convertDataRow(0) = valueArr(0)
                convertDataRow(1) = valueArr(1)
                convertDataRow(2) = valueArr(2)
                'convertDataRow(3) = valueArr(3)
                convertDataTable.Rows.Add(convertDataRow)
                sInputLine = srFileReader.ReadLine()
            Loop
            srFileReader.Close()
        Catch ex As FileNotFoundException

        End Try
        Return convertDataTable
    End Function


    'Create Master Template
    Public Sub Log_CreateMasterTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Create Master Template by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Modify Master Template
    Public Sub Log_ModifyMasterTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Modify Master Template by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Create Common Template
    Public Sub Log_CreateCommonTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Create Common Template by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Modify Common Template
    Public Sub Log_ModifyCommonTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Modify Common Template by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Create Swift Template
    Public Sub Log_EnableCommonTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Common Template Enabled by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Modify Swift Template
    Public Sub Log_DisableCommonTemplate(ByVal _user As String, ByVal _templateFile As String)
        Dim msg As String
        msg = GetActivityName() & "Common Template Disabled by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _templateFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Disable/Enable Common Template
    'File Conversion
    'Change Password
    Public Sub Log_ChangePassword(ByVal _user As String)
        Dim msg As String
        msg = GetActivityName() & "Change Password by " & GetUserName(_user) & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub

    'Reset Password (only for Super User)
    Public Sub Log_ResetPassword(ByVal _user As String)
        Dim msg As String
        msg = GetActivityName() & "Reset Password by " & GetUserName(_user) & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub

    'Login/Logout
    Public Sub Log_Login(ByVal _user As String)
        Dim msg As String
        msg = GetActivityName() & "Login by " & GetUserName(_user) & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub

    Public Sub Log_Logout(ByVal _user As String)
        Dim msg As String
        msg = GetActivityName() & "Logout by " & GetUserName(_user) & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub

    'Create GCMS Money Transfer Data Entry Transaction
    Public Sub Log_CreateMoneyTransfer(ByVal _user As String, ByVal _savMode As String, ByVal _newFile As String)
        Dim msg As String

        msg = GetActivityName() & "(" & _savMode & ") Create Money Transfer Instruction by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _newFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub
    'Modify GCMS Money Transfer Data Entry Transaction
    Public Sub Log_EditMoneyTransfer(ByVal _user As String, ByVal _savMode As String, ByVal _editFile As String)
        Dim msg As String

        msg = GetActivityName() & "(" & _savMode & ") Edit Money Transfer Instruction by " & GetUserName(_user) & vbCrLf
        msg &= GetOutputFile() & _editFile & vbCrLf
        msg &= GetActivityDate()

        WriteActivityLog(msg, _user)
    End Sub

    'Create Definition File
    'Modify Definition File
    'Create Definition File
    Public Sub Log_CreateDefinitionFile(ByVal _user As String, ByVal _sourceFile As String, ByVal _definitionFile As String)
        Dim msg As String
        msg = GetActivityName() & "Create Definition File by " & GetUserName(_user) & vbCrLf
        msg &= GetSourceFile() & _sourceFile & vbCrLf
        msg &= GetOutputFile() & _definitionFile & vbCrLf
        msg &= GetActivityDate()
        WriteActivityLog(msg, _user)
    End Sub

    'Modify Definition File

    Private Function GetTotalConvertedTransaction() As String
        Return "Total Converted Transaction	: "
    End Function

    'Output File Creation
    Public Sub Log_ConvertSourceFile(ByVal _user As String, ByVal _sourceFile As String, ByVal _outputFile As String, ByVal _totalRecord As String, Optional ByVal _currencyAmt As SortedList(Of String, Decimal) = Nothing)
        Dim msg As String
        msg = GetActivityName() & "Convert Source File into Output File by " & GetUserName(_user) & vbCrLf
        msg &= GetSourceFile() & _sourceFile & vbCrLf
        msg &= GetOutputFile() & _outputFile & vbCrLf
        msg &= GetActivityDate() & vbCrLf
        msg &= GetTotalConvertedTransaction() & _totalRecord & " transactions" & vbCrLf
        If Not _currencyAmt Is Nothing Then
            For Each _currency As String In _currencyAmt.Keys
                msg &= "Total Amount (" & _currency & ")" & vbTab & vbTab & " : " & _currencyAmt(_currency).ToString & vbCrLf
            Next
        End If
        WriteActivityLog(msg, _user)
    End Sub

    ''' <summary>
    ''' DP API Windows Protection. Protect.
    ''' </summary>
    ''' <param name="plainText">To encrypted Text</param>
    ''' <returns>Encrypted string in base64</returns>
    ''' <remarks></remarks>
    Private Function DPAPIProtect(ByVal plainText As String) As String
        Dim dataInByte As Byte() = System.Text.Encoding.UTF8.GetBytes(plainText)
        Dim encryptedByte As Byte() = DPAPI.Protect(dataInByte)
        Dim encryptedString As String = Convert.ToBase64String(encryptedByte)
        Return encryptedString
    End Function

    ''' <summary>
    ''' DP API Windows Protection. Unprotect.
    ''' </summary>
    ''' <param name="encryptedTextBase64">Encrypted string in base64</param>
    ''' <returns>Plain Text</returns>
    ''' <remarks></remarks>
    Private Function DPAPIUnProtect(ByVal encryptedTextBase64 As String) As String
        Dim encryptedByte As Byte() = Convert.FromBase64String(encryptedTextBase64)
        Dim dataInByte As Byte() = DPAPI.Unprotect(encryptedByte)
        Return System.Text.Encoding.UTF8.GetString(dataInByte)
    End Function

    ''' <summary>
    ''' Generate License Data for Copy Protection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GenerateLicenseData() As String
        Dim stb As New System.Text.StringBuilder
        Dim objMOS As ManagementObjectSearcher
        Dim objMOC As Management.ManagementObjectCollection
        Dim objMO As Management.ManagementObject
        objMOS = New ManagementObjectSearcher("Select * From Win32_Processor")
        objMOC = objMOS.Get()

        For Each objMO In objMOC
            stb.AppendLine(String.Format("CPU ID = {0}", objMO("ProcessorID")))
            objMO.Dispose()
            objMO = Nothing
        Next

        objMOS = New ManagementObjectSearcher("Select * From Win32_BaseBoard")
        objMOC = objMOS.Get()

        For Each objMO In objMOC
            stb.AppendLine(String.Format("MBSerialNumber = {0}", objMO("SerialNumber")))
        Next

        objMOS = New ManagementObjectSearcher("Select * From Win32_OperatingSystem")
        objMOC = objMOS.Get()

        For Each objMO In objMOC
            stb.AppendLine(String.Format("SerialNumber = {0}", objMO("SerialNumber")))
        Next

        objMOS.Dispose()
        objMOS = Nothing

        Return stb.ToString
    End Function

    ''' <summary>
    ''' Generate License File
    ''' </summary>
    ''' <param name="file">License File Name</param>
    ''' <remarks></remarks>
    Public Sub GenerateLicense(ByVal file As String)
        Dim licenseData As String = GenerateLicenseData()
        System.IO.File.WriteAllText(file, DPAPIProtect(licenseData))
    End Sub

    ''' <summary>
    ''' Verify Licence File
    ''' </summary>
    ''' <param name="file">License File</param>
    ''' <returns>True if license is valid.</returns>
    ''' <remarks></remarks>
    Public Function VerifyLicense(ByVal file As String) As Boolean
        Return True

        If System.IO.File.Exists(file) Then
            Dim fileData As String = System.IO.File.ReadAllText(file)
            Dim licenseData As String = GenerateLicenseData()

            Try
                Dim fileDataInPlain As String = HelperModule.DPAPIUnProtect(fileData)
                If fileDataInPlain = licenseData Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw New Exception("System do not able to continue. License file is tempered")

            End Try

        Else
            Throw New Exception("System do not able to continue. License file is missing")
        End If
    End Function

    Public Function GetDateFormatPattern(ByVal dtseparator As String, ByVal dttype As String) As String

        Dim dtpattern As String = ""
        dttype = dttype.Replace("DD", "dd").Replace("YYYY", "yyyy").Replace("YY", "yy")
        If dtseparator = "No Space" Then
            Return dttype
        ElseIf dtseparator = "<Space>" Then
            dtseparator = " "
        End If

        Select Case dttype
            Case "ddMMyy"
                Return String.Format("dd{0}MM{0}yy", dtseparator)
            Case "ddMMMyy"
                Return String.Format("dd{0}MMM{0}yy", dtseparator)
            Case "ddMMyyyy"
                Return String.Format("dd{0}MM{0}yyyy", dtseparator)
            Case "ddMMMyyyy"
                Return String.Format("dd{0}MMM{0}yyyy", dtseparator)
            Case "MMddyy"
                Return String.Format("MM{0}dd{0}yy", dtseparator)
            Case "MMMddyy"
                Return String.Format("MMM{0}dd{0}yy", dtseparator)
            Case "MMddyyyy"
                Return String.Format("MM{0}dd{0}yyyy", dtseparator)
            Case "MMMddyyyy"
                Return String.Format("MMM{0}dd{0}yyyy", dtseparator)
            Case "yyMMdd"
                Return String.Format("yy{0}MM{0}dd", dtseparator)
            Case "yyyyddMM"
                Return String.Format("yyyy{0}dd{0}MM", dtseparator)
            Case "yyyyMMdd"
                Return String.Format("yyyy{0}MM{0}dd", dtseparator)
            Case "ddMMyy"
                Return String.Format("dd{0}MM{0}yy", dtseparator)
            Case "MMddyy"
                Return String.Format("MM{0}dd{0}yy", dtseparator)
            Case "yyMMdd"
                Return String.Format("yy{0}MM{0}dd", dtseparator)
        End Select

        Return dtpattern



    End Function

    Public Function GetFilePasswordAdmin() As String
        Return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "default\pAdm.dat")
    End Function

    Public Function GetFilePasswordSuperUser() As String
        Return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "default\pSuper.dat")
    End Function

    Public Function GetFilePasswordCommonUser() As String
        Return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "default\pCommon.dat")
    End Function

    Public Function GetFilePasswordCommon2User() As String
        Return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "default\pCommon2.dat")
    End Function

    Public Function GetHelpFile() As String
        Return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Help\BTMU-MAGIC-HK-Help.chm")
    End Function

    Public Function GetMasterTemplateFileExtension() As String
        Return ".mmt"
    End Function

    Public Function GetCommonTemplateFileExtension() As String
        Return ".mct"
    End Function

    'Private DefaultFont As System.Drawing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25)
    'Public Sub SetDefaultFont(ByVal parentCtl As System.Windows.Forms.Control)
    '    For Each ctl As System.Windows.Forms.Control In parentCtl.Controls
    '        If ctl.Controls.Count > 0 Then
    '            SetDefaultFont(ctl)
    '        ElseIf ctl.GetType() Is GetType(System.Windows.Forms.Label) Then
    '            If ctl.Font.SystemFontName = "DefaultFont" Then
    '                ctl.Font = DefaultFont
    '            End If
    '        End If
    '    Next
    'End Sub

    Public Function TrimmedString(ByVal Data As String) As String
        If Data Is Nothing Then
            Return Data
        Else
            Return Data.Trim
        End If
    End Function
    'Public Sub ArchiveLogFile()
    '    Dim logFolder As String = GetLogFolder()
    '    Dim archiveFolder As String = GetLogFolder() & GetArchieveLogFolder()

    '    Dim files As String() = Directory.GetFiles(logFolder, "*.log")
    '    Dim lst As New List(Of String)(files)
    '    Dim lastYearLog As String = logFolder & "\" & DateTime.Now.AddYears(-1).ToString("yyyyMMdd") & ".log"

    '    lst.Sort()

    '    For Each file As String In lst
    '        If file < lastYearLog Then
    '            Dim flInfo As FileInfo = New FileInfo(file)
    '            System.IO.File.Move(file, archiveFolder & "\" & flInfo.Name)
    '        End If
    '    Next

    'End Sub
    'Public Sub DeleteArchiveLogFile()
    '    Dim archiveFolder As String = GetLogFolder() & GetArchieveLogFolder()

    '    Dim files As String() = Directory.GetFiles(archiveFolder, "*.log")
    '    Dim lst As New List(Of String)(files)
    '    Dim lastTwoYearsLog As String = archiveFolder & "\" & DateTime.Now.AddYears(-2).ToString("yyyyMMdd") & ".log"

    '    lst.Sort()

    '    For Each file As String In lst
    '        If file < lastTwoYearsLog Then
    '            System.IO.File.Delete(file)
    '        End If
    '    Next

    'End Sub
    'Modify Definition File
    Public Sub Log_ModifyDefinitionFile(ByVal _user As String, ByVal _sourceFile As String, ByVal _definitionFile As String)
        Dim msg As String
        msg = GetActivityName() & "Modify Definition File by " & GetUserName(_user) & vbCrLf 'Regina 26 Aug 09
        msg &= GetSourceFile() & _sourceFile & vbCrLf
        msg &= GetOutputFile() & _definitionFile & vbCrLf
        msg &= GetActivityDate()
        WriteActivityLog(msg, _user)
    End Sub

    Public Function IsFileFormatUsingRecordTypes(ByVal format As String) As Boolean
        Dim formatsUsingRecordTypes() As String = New String() {"iFTS-2 MultiLine", "Mr.Omakase India"}
        For Each fileformat As String In formatsUsingRecordTypes
            If fileformat.Equals(format, StringComparison.InvariantCultureIgnoreCase) Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Function IsIFTS2SingleLine(ByVal format As String) As Boolean
        If format.Equals("iFTS-2 SingleLine", StringComparison.InvariantCultureIgnoreCase) Then
            Return True
        End If
        Return False
    End Function

    Public Function ContainsNonASCIIChar(ByVal fieldValue As String) As Boolean

        If IsNothingOrEmptyString(fieldValue) Then Return False

        For Each eachChar As Char In fieldValue
            If Convert.ToInt32(eachChar) > 255 Then Return True
        Next

        Return False

    End Function
End Module
