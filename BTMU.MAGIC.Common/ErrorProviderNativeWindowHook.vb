Imports System.Reflection
Imports System.ComponentModel
Imports System.Windows.Forms

''' <SUMMARY>
''' A NativeWindow, which we use to trap the WndProc messages
''' and patch up the ErrorProvider/ToolTip bug.
''' </SUMMARY>
Public Class ErrorProviderNativeWindowHook
    Inherits NativeWindow

    Private _int407Count As Integer = 0
    Friend _blnTrigerRefresh As Boolean = False


    ''' <SUMMARY>
    ''' On the 0x407 message, we need to reset the 
    ''' error provider; however, I can't do it directly in the WndProc; 
    ''' otherwise, we could get a cross threading type exception, since
    ''' this WndProc is called on a separate thread.  The Timer will make
    ''' sure the work gets done on the Main GUI thread.          
    ''' </SUMMARY>
    ''' <param name="m"></param>
    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H407 Then
            _int407Count = _int407Count + 1
            If _int407Count > 3 Then  ' if this occurs we need to release...
                Me.ReleaseHandle()
                _blnTrigerRefresh = True
            End If
        Else
            _int407Count = 0
        End If
        MyBase.WndProc(m)
    End Sub


End Class
