Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary>
''' Fixed Dissappearing Tooltip
''' Source: http://www.codeproject.com/KB/dotnet/ErrorProvider_ToolTipFix.aspx
''' </summary>
''' <remarks></remarks>
Public Class ErrorProviderFixed
    Inherits ErrorProvider

    Dim _toolTipFix As ErrorProviderFixManager = Nothing

    Public Sub New()
        MyBase.New()
        _toolTipFix = New ErrorProviderFixManager(Me)
        BlinkStyle = ErrorBlinkStyle.NeverBlink
    End Sub

    Public Sub New(ByVal parentControl As ContainerControl)
        MyBase.New(parentControl)
        _toolTipFix = New ErrorProviderFixManager(Me)
    End Sub

    Public Sub New(ByVal container As IContainer)
        MyBase.New(container)
        _toolTipFix = New ErrorProviderFixManager(Me)
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        _toolTipFix.Dispose()
        MyBase.Dispose(disposing)
    End Sub
End Class


