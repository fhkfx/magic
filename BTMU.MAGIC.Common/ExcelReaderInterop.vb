Imports System.Globalization
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop.Excel
Imports Excel = Microsoft.Office.Interop.Excel

Public Class ExcelReaderInterop

    '''Added by Regina on 18 Aug 2009 - help to speed up loading of Excel
    ''' <summary>
    ''' Open the file path received in Excel. Then, open the workbook
    ''' within the file. Send the workbook to the next function, the internal scan
    ''' function. Will throw an exception if a file cannot be found or opened.
    ''' </summary>
    Public Shared Function ExcelOpenSpreadsheets(ByVal fileName As String, ByVal worksheetName As String, Optional ByVal FromCommonTemplate As Boolean = False) As System.Data.DataTable

        Dim excelApp As Application = Nothing
        Dim wkBook As Workbook = Nothing

        Dim originalCultureInfo As CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Dim enCulture As New CultureInfo("en-US")
        System.Threading.Thread.CurrentThread.CurrentCulture = enCulture

        Try

            excelApp = New Application()

            wkBook = excelApp.Workbooks.Open(fileName, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing, _
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing)
            Dim dt As System.Data.DataTable

            If FromCommonTemplate Then
                dt = ExcelScanIntenalForCommonTemplate(wkBook, worksheetName)
            Else
                dt = ExcelScanIntenal(wkBook, worksheetName)
            End If

            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            If wkBook IsNot Nothing Then
                wkBook.Close(False, fileName, Nothing)
                Marshal.ReleaseComObject(wkBook)
            End If

            If excelApp IsNot Nothing Then
                Marshal.ReleaseComObject(excelApp)
                excelApp = Nothing
                GC.Collect()
                GC.WaitForPendingFinalizers()
            End If

            System.Threading.Thread.CurrentThread.CurrentCulture = originalCultureInfo
        End Try
    End Function

    Private Shared Function ExcelScanIntenal(ByVal wkBook As Workbook, ByVal worksheetName As String) As System.Data.DataTable

        Dim numSheets As Integer = wkBook.Sheets.Count
        Dim sheetNum As Integer
        For sheetNum = 1 To numSheets


            Dim sheet As Worksheet = CType(wkBook.Sheets(sheetNum), Worksheet)
            If sheet.Name.Trim.Equals(worksheetName.Trim, StringComparison.InvariantCultureIgnoreCase) Then

                Dim excelRange As Range = sheet.UsedRange

                Dim valueArray As Object(,) = CType(excelRange.Value(Excel.XlRangeValueDataType.xlRangeValueDefault), Object(,))

                Dim dt As System.Data.DataTable = New System.Data.DataTable()
                Dim colIndex As Integer
                Dim rowIndex As Integer

                If valueArray IsNot Nothing Then
                    For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                        dt.Columns.Add("Column" & colIndex, GetType(String))
                    Next


                    For rowIndex = 0 To valueArray.GetUpperBound(0) - 1
                        Dim dr As System.Data.DataRow = dt.NewRow()
                        For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                            Dim temp As Object = valueArray(rowIndex + 1, colIndex + 1)
                            If temp IsNot Nothing Then

                                If temp.GetType() IsNot GetType(DateTime) Then
                                    dr(colIndex) = temp
                                Else
                                    dr(colIndex) = CType(temp, String)
                                End If
                                'If temp.GetType() Is GetType(String) Then
                                '    dr(colIndex) = temp.ToString()
                                'Else
                                '    dr(colIndex) = excelRange.Range(excelRange(rowIndex + 1, colIndex + 1), excelRange(rowIndex + 1, colIndex + 1)).Text
                                'End If

                            End If

                        Next
                        dt.Rows.Add(dr)
                    Next
                    ' Remove the empty rows at the end
                    Dim rowCount As Integer = dt.Rows.Count - 1
                    Dim intI As Integer = 0
                    Dim isValidRow As Boolean = False
                    While rowCount >= 0
                        For intI = 0 To dt.Columns.Count - 1
                            If Not IsNothingOrEmptyString(dt.Rows(rowCount)(intI).ToString) Then
                                isValidRow = True
                                Exit For
                            Else
                                isValidRow = False
                            End If
                        Next
                        If Not isValidRow Then
                            dt.Rows.RemoveAt(rowCount)
                        Else
                            Exit While
                        End If
                        rowCount -= 1
                    End While

                    Return dt
                Else
                    Return dt
                End If
            End If


        Next sheetNum

        Return Nothing
    End Function
    Private Shared Function ExcelScanIntenalForCommonTemplate(ByVal wkBook As Workbook, ByVal worksheetName As String) As System.Data.DataTable

        Dim numSheets As Integer = wkBook.Sheets.Count
        Dim sheetNum As Integer
        For sheetNum = 1 To numSheets


            Dim sheet As Worksheet = CType(wkBook.Sheets(sheetNum), Worksheet)
            If sheet.Name.Trim.Equals(worksheetName.Trim, StringComparison.InvariantCultureIgnoreCase) Then

                Dim excelRange As Range = sheet.UsedRange
                If sheet.UsedRange.Rows.Count > 1 Then
                    excelRange = sheet.Range("A1", GetColumnString(sheet.UsedRange.Rows().Columns.Count) & (sheet.UsedRange.Rows.Count + sheet.UsedRange.Row - 1).ToString)
                End If

                Dim valueArray As Object(,) = CType(excelRange.Value(Excel.XlRangeValueDataType.xlRangeValueDefault), Object(,))

                Dim dt As System.Data.DataTable = New System.Data.DataTable()
                Dim colIndex As Integer
                Dim rowIndex As Integer

                If valueArray IsNot Nothing Then
                    For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                        dt.Columns.Add("Column" & colIndex, GetType(String))
                    Next


                    For rowIndex = 0 To valueArray.GetUpperBound(0) - 1
                        Dim dr As System.Data.DataRow = dt.NewRow()
                        For colIndex = 0 To valueArray.GetUpperBound(1) - 1
                            Dim temp As Object = valueArray(rowIndex + 1, colIndex + 1)
                            If temp IsNot Nothing Then

                                If temp.GetType() IsNot GetType(DateTime) Then
                                    dr(colIndex) = temp
                                Else
                                    dr(colIndex) = CType(temp, String)
                                End If
                                'If temp.GetType() Is GetType(String) Then
                                '    dr(colIndex) = temp.ToString()
                                'Else
                                '    dr(colIndex) = excelRange.Range(excelRange(rowIndex + 1, colIndex + 1), excelRange(rowIndex + 1, colIndex + 1)).Text
                                'End If

                            End If

                        Next
                        dt.Rows.Add(dr)
                    Next
                    ' Remove the empty rows at the end
                    Dim rowCount As Integer = dt.Rows.Count - 1
                    Dim intI As Integer = 0
                    Dim isValidRow As Boolean = False

                    Dim emptyCol As Integer

                    While rowCount >= 0
                        'initialize empty column
                        emptyCol = 0

                        For intI = 0 To dt.Columns.Count - 1
                            'Error raised by BTMU-BKK branch
                            'When they convert a source file without having data in the first few columns
                            'but the rest have data, MAGIC says 'No source records found!'
                            'Fixed @ 5th Oct 2010
                            'By Kay
                            If Not IsNothingOrEmptyString(dt.Rows(rowCount)(intI).ToString) Then
                                isValidRow = True
                                Exit For
                            Else
                                emptyCol += 1
                            End If
                        Next

                        'Check emptyCol count and total no. of columns count are equal
                        If emptyCol = dt.Columns.Count AndAlso Not isValidRow Then
                            dt.Rows.RemoveAt(rowCount)
                        Else
                            Exit While
                        End If
                        rowCount -= 1
                    End While

                    Return dt
                Else
                    Return dt
                End If
            End If


        Next sheetNum

        Return Nothing
    End Function
    ''' <summary>
    ''' This function is to get Name of Column for the given Column Index
    ''' </summary>
    ''' <param name="iColumnIndex">Oridinal Number of Column</param>
    ''' <returns>Returns Name of Column at the given Column Index</returns>
    ''' <remarks></remarks>
    Public Shared Function GetColumnString(ByVal iColumnIndex As Integer) As String
        Dim _columnString As String = String.Empty
        Dim leftString As String
        Dim leftInteger As Integer
        Dim rightString As String
        Dim rightInteger As Integer

        Select Case iColumnIndex
            Case Is <= 26
                _columnString = Chr(iColumnIndex + 64)
            Case Is > 26
                leftInteger = Int(iColumnIndex / 26)
                If iColumnIndex Mod 26 = 0 Then
                    leftInteger = leftInteger - 1
                End If
                leftString = Chr(leftInteger + 64)

                rightInteger = iColumnIndex Mod 26
                If rightInteger = 0 Then
                    rightString = Chr(rightInteger + 64 + 26)
                Else
                    rightString = Chr(rightInteger + 64)
                End If

                _columnString = leftString & rightString
            Case Else
                _columnString = ""
        End Select
        Return _columnString
    End Function

    ''' <summary>
    ''' This function is to check whether the file is opened
    ''' </summary>
    ''' <param name="FileName">Name of file</param>
    ''' <returns>Return false if the file can be open, else throw exception</returns>
    ''' <remarks></remarks>
    Public Shared Function IsFileOpen(ByVal FileName As String)
        Dim stream As System.IO.FileStream = Nothing
        Try
            stream = System.IO.File.Open(FileName, IO.FileMode.Open, IO.FileAccess.ReadWrite)
            Return False
        Catch ex As Exception
            Throw ex
        Finally
            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Function

    ''' <summary>
    ''' This function is used to get the connectionstring based on the excel version
    ''' </summary>
    ''' <param name="excelFile"></param>
    ''' <param name="ExcelVersion"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetConnectionString(ByVal excelFile As String, ByVal ExcelVersion As String) As String
        Dim excelConnectionString As String = String.Empty
        Select Case ExcelVersion
            Case "2007"
                excelConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended properties=""Excel 12.0 Xml;HDR=No""", excelFile)
            Case Else
                excelConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)
        End Select
        Return excelConnectionString
    End Function

    ''' <summary>
    ''' This function is used to check whether the file is a valid excel file
    ''' </summary>
    ''' <param name="excelFile"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function IsValidExcelFile(ByVal excelFile As String) As Boolean

        Dim excelConnection As System.Data.OleDb.OleDbConnection

        'Test Connection
        Try


            excelConnection = New System.Data.OleDb.OleDbConnection(IIf(excelFile.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase), _
              String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended properties=""Excel 12.0 Xml;HDR=No""", excelFile), _
              String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", excelFile)))
            excelConnection.Open()
            excelConnection.Close()
            Return True

        Catch ex As Exception

            Return False

        End Try


    End Function

    ' ''Public Shared Function ReadWorkSheet2(ByVal sourcefilename As String, ByRef errlog As String, ByVal sheetname As String) As DataTable
    ' ''    Dim excelDataTable As New System.Data.DataTable
    ' ''    'Dim sheetName As String = ""

    ' ''    Dim originalCultureInfo As Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
    ' ''    Dim enCulture As New Globalization.CultureInfo("en-US")
    ' ''    System.Threading.Thread.CurrentThread.CurrentCulture = enCulture

    ' ''    Try

    ' ''        Try
    ' ''            'sheetName = Me.Delimiter 'Excel Sheet Name
    ' ''        Catch ex As Exception
    ' ''            errlog = errlog + ex.Message.ToString + Chr(13) + Chr(10)
    ' ''            Throw New Exception(ex.Message.ToString)
    ' ''            'MessageBox.Show(ex.Message.ToString)
    ' ''        Finally
    ' ''        End Try




    ' ''        Dim XlApp As New Excel.Application

    ' ''        Dim XlWorkBook As Excel.Workbook

    ' ''        Dim XlSheet As Excel.Sheets

    ' ''        Dim XlWorkSheet As Excel.Worksheet


    ' ''        'CurrentFilePath stores the selected Excel Workbook file path
    ' ''        Try
    ' ''            XlWorkBook = XlApp.Workbooks.Open(sourcefilename)
    ' ''        Catch ex As Exception
    ' ''            errlog = errlog + "Invalid source file." + Chr(13) + Chr(10)
    ' ''            excelDataTable = Nothing
    ' ''            Return excelDataTable
    ' ''            Exit Function
    ' ''        End Try

    ' ''        XlSheet = XlWorkBook.Worksheets

    ' ''        Dim xlSheetIndex As Integer
    ' ''        Dim xlIntRows As Integer
    ' ''        Dim xlIntCols As Integer

    ' ''        XlWorkBook = XlApp.Workbooks.Open(sourcefilename)

    ' ''        XlSheet = XlWorkBook.Worksheets

    ' ''        XlWorkSheet = CType(XlSheet(sheetName), Excel.Worksheet)

    ' ''        xlSheetIndex = xlSheetIndex + 1

    ' ''        xlIntRows = XlWorkSheet.UsedRange.Rows.Count

    ' ''        xlIntCols = XlWorkSheet.UsedRange.Columns.Count


    ' ''        'Fetch values from the selected excel sheet

    ' ''        Dim XlObj As Excel.Range

    ' ''        Dim XlArray() As String

    ' ''        ReDim XlArray(xlIntCols - 1)

    ' ''        Dim TempCnt1 As Integer

    ' ''        Dim TempCnt2 As Integer

    ' ''        For TempCnt1 = 0 To xlIntCols - 1
    ' ''            excelDataTable.Columns.Add("Field" & (TempCnt1 + 1).ToString)
    ' ''        Next

    ' ''        For TempCnt1 = 1 To xlIntRows

    ' ''            For TempCnt2 = 1 To xlIntCols

    ' ''                XlObj = CType(XlWorkSheet.Cells(TempCnt1, TempCnt2), Excel.Range)

    ' ''                XlArray(TempCnt2 - 1) = XlObj.Value

    ' ''            Next

    ' ''            excelDataTable.Rows.Add(XlArray)

    ' ''        Next

    ' ''        XlWorkBook.Close()
    ' ''        XlApp.Quit()
    ' ''        Return excelDataTable

    ' ''    Finally

    ' ''        System.Threading.Thread.CurrentThread.CurrentCulture = originalCultureInfo
    ' ''    End Try

    ' ''End Function
    'Added by Hein 20090910
    'Public Shared Function CheckExcelFormat(ByVal fileString As String) As Boolean
    '    'Dim sheetNameDataTable As New DataTable
    '    'Dim result As Boolean = False
    '    'Dim ExcelConncetionString As String = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1"";", fileString)
    '    'Dim ExcelConncetion As New System.Data.OleDb.OleDbConnection(ExcelConncetionString)
    '    ''Test Connection
    '    'Try
    '    '    ExcelConncetion.Open()

    '    '    Return True
    '    'Catch ex As Exception
    '    '    result = False
    '    'Finally
    '    '    If ExcelConncetion.State = ConnectionState.Open Then
    '    '        ExcelConncetion.Close()
    '    '    End If
    '    'End Try
    '    'Return result
    '    Dim result As Boolean = False
    '    Dim ExcelConnectionString As String = String.Empty
    '    Dim ExcelConnection As New System.Data.OleDb.OleDbConnection


    '    Try
    '        ExcelConnectionString = ExcelReaderInterop.GetConnectionString(fileString, "2003")
    '        ExcelConnection = New System.Data.OleDb.OleDbConnection(ExcelConnectionString)
    '        ExcelConnection.Open()
    '        Return True
    '    Catch ex As Exception
    '        Try
    '            ExcelConnectionString = ExcelReaderInterop.GetConnectionString(fileString, "2007")
    '            ExcelConnection = New System.Data.OleDb.OleDbConnection(ExcelConnectionString)
    '            ExcelConnection.Open()
    '            Return True
    '        Catch ex1 As Exception
    '            result = False
    '        End Try
    '    Finally
    '        If ExcelConnection.State = ConnectionState.Open Then
    '            ExcelConnection.Close()
    '        End If
    '    End Try
    '    Return result
    'End Function
End Class
