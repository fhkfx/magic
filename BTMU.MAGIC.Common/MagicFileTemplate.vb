Public Class MagicFileTemplate
    Private _listValue As String
    Private _detailValue As String

    Public Property ListValue() As String
        Get
            Return _listValue
        End Get
        Set(ByVal value As String)
            _listValue = value
        End Set
    End Property

    Public Property DetailValue() As String
        Get
            Return _detailValue
        End Get
        Set(ByVal value As String)
            _detailValue = value
        End Set
    End Property


End Class
