Imports System.Text.RegularExpressions
Imports System.Reflection

Public Module CommonRules

#Region " StringRequired "

    ''' <summary>
    ''' Rule ensuring a string value contains one or more characters.
    ''' </summary>
    ''' <param name="target">Object containing the data to validate</param>
    ''' <param name="e">Arguments parameter specifying the name of the string property to validate</param>
    ''' <returns><see langword="false" /> if there is error</returns>
    ''' <remarks>
    ''' This implementation uses late binding, and will only work
    ''' against string property values.
    ''' </remarks>
    Public Function StringRequired(ByVal target As Object, ByVal e As ValidationArgs) As Boolean

        Dim value As String = CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Len(value) = 0 Then
            If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(My.Resources.E00000050, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(My.Resources.E00000050, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            Else
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(e.CustomErrorDescription, ValidationArgs.GetFriendlyPropertyName(e))
                End If

            End If
            Return False

        Else
            Return True
        End If

    End Function

#End Region

#Region " StringMaxLength "

    ''' <summary>
    ''' Rule ensuring a string value doesn't exceed a specified length.
    ''' </summary>
    ''' <param name="target">Object containing the data to validate</param>
    ''' <param name="e">Arguments parameter specifying the name of the string
    ''' property to validate</param>
    ''' <returns><see langword="false" /> if there is error</returns>
    ''' <remarks>
    ''' This implementation uses late binding, and will only work
    ''' against string property values.
    ''' </remarks>
    Public Function StringMaxLength(ByVal target As Object, _
      ByVal e As ValidationArgs) As Boolean

        Dim max As Integer = DirectCast(e, MaxLengthValidationArgs).MaxLength
        Dim value As String = _
          CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Len(value) > max Then

            If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(e.CustomErrorDescription, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            Else
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(My.Resources.E00000010, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(My.Resources.E00000010, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            End If


            Return False

        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Custom <see cref="ValidationArgs" /> object required by the
    ''' <see cref="StringMaxLength" /> rule method.
    ''' </summary>
    Public Class MaxLengthValidationArgs
        Inherits ValidationArgs

        Private _maxLength As Integer
        ''' <summary>
        ''' Get the max length for the string.
        ''' </summary>
        Public ReadOnly Property MaxLength() As Integer
            Get
                Return _maxLength
            End Get
        End Property

        ''' <summary>
        ''' Create a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="maxLength">Max length of characters allowed.</param>
        Public Sub New(ByVal propertyName As String, ByVal maxLength As Integer)
            MyBase.New(propertyName)
            _maxLength = maxLength
        End Sub

    End Class

#End Region

#Region " StringMinLength "

    ''' <summary>
    ''' Rule ensuring a string value doesn't exceed
    ''' a specified length.
    ''' </summary>
    ''' <param name="target">Object containing the data to validate</param>
    ''' <param name="e">Arguments parameter specifying the name of the string
    ''' property to validate</param>
    ''' <returns><see langword="false" /> if the rule is broken</returns>
    ''' <remarks>
    ''' This implementation uses late binding, and will only work
    ''' against string property values.
    ''' </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")> _
    Public Function StringMinLength(ByVal target As Object, _
      ByVal e As ValidationArgs) As Boolean

        Dim min As Integer = DirectCast(e, MinLengthValidationArgs).MinLength
        Dim value As String = _
          CStr(CallByName(target, e.PropertyName, CallType.Get))
        If Len(value) < min Then

            If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(e.CustomErrorDescription, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            Else
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(My.Resources.E00000020, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(My.Resources.E00000020, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            End If



            Return False

        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Custom <see cref="ValidationArgs" /> object required by the
    ''' <see cref="StringMinLength" /> rule method.
    ''' </summary>
    Public Class MinLengthValidationArgs
        Inherits ValidationArgs

        Private _minLength As Integer

        ''' <summary>
        ''' Get the Min length for the string.
        ''' </summary>
        Public ReadOnly Property MinLength() As Integer
            Get
                Return _minLength
            End Get
        End Property

        ''' <summary>
        ''' Create a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="minLength">Min length of characters allowed.</param>
        Public Sub New(ByVal propertyName As String, ByVal minLength As Integer)
            MyBase.New(propertyName)
            _minLength = minLength
        End Sub

    End Class

#End Region

#Region " MaxValue "

    ''' <summary>
    ''' Rule ensuring that a numeric value
    ''' doesn't exceed a specified maximum.
    ''' </summary>
    ''' <typeparam name="T">Type of the property to validate.</typeparam>
    ''' <param name="target">Object containing value to validate.</param>
    ''' <param name="e">Arguments variable specifying the
    ''' name of the property to validate, along with the max
    ''' allowed value.</param>
    Public Function MaxValue(Of T As IComparable)(ByVal target As Object, ByVal e As MaxValueValidationArgs(Of T)) As Boolean
        Dim pi As PropertyInfo = target.GetType.GetProperty(e.PropertyName)
        Dim value As T = DirectCast(pi.GetValue(target, Nothing), T)
        Dim result As Integer = value.CompareTo(e.MaxValue)
        If result >= 1 Then
            If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(e.CustomErrorDescription, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            Else
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(My.Resources.E00000030, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(My.Resources.E00000030, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            End If


            Return False

        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Custom <see cref="ValidationArgs" /> object required by the
    ''' <see cref="MaxValue" /> rule method.
    ''' </summary>
    ''' <typeparam name="T">Type of the property to validate.</typeparam>
    Public Class MaxValueValidationArgs(Of T)
        Inherits ValidationArgs

        Private _maxValue As T
        Private _valueType As String
        ''' <summary>
        ''' Get the max value for the property.
        ''' </summary>
        Public ReadOnly Property MaxValue() As T
            Get
                Return _maxValue
            End Get
        End Property

        ''' <summary>
        ''' Create a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property.</param>
        ''' <param name="maxValue">Maximum allowed value for the property.</param>
        Public Sub New(ByVal propertyName As String, ByVal maxValue As T)
            MyBase.New(propertyName)
            _maxValue = maxValue
            _valueType = GetType(T).FullName
        End Sub

    End Class

#End Region

#Region " MinValue "

    ''' <summary>
    ''' Rule ensuring that a numeric value
    ''' doesn't exceed a specified minimum.
    ''' </summary>
    ''' <typeparam name="T">Type of the property to validate.</typeparam>
    ''' <param name="target">Object containing value to validate.</param>
    ''' <param name="e">Arguments variable specifying the
    ''' name of the property to validate, along with the min
    ''' allowed value.</param>
    Public Function MinValue(Of T As IComparable)(ByVal target As Object, ByVal e As MinValueValidationArgs(Of T)) As Boolean

        Dim pi As PropertyInfo = target.GetType.GetProperty(e.PropertyName)
        Dim value As T = DirectCast(pi.GetValue(target, Nothing), T)

        Dim result As Integer = value.CompareTo(e.MinValue)
        If result <= -1 Then
            If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(e.CustomErrorDescription, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            Else
                If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                    e.Description = _
                      String.Format(My.Resources.E00000040, ValidationArgs.GetPropertyName(e))
                Else
                    e.Description = _
                        String.Format(My.Resources.E00000040, ValidationArgs.GetFriendlyPropertyName(e))
                End If
            End If

            Return False

        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Custom <see cref="ValidationArgs" /> object required by the
    ''' <see cref="MinValue" /> rule method.
    ''' </summary>
    ''' <typeparam name="T">Type of the property to validate.</typeparam>
    Public Class MinValueValidationArgs(Of T)
        Inherits ValidationArgs

        Private _minValue As T
        Private _valueType As String

        ''' <summary>
        ''' Get the min value for the property.
        ''' </summary>
        Public ReadOnly Property MinValue() As T
            Get
                Return _minValue
            End Get
        End Property

        ''' <summary>
        ''' Create a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property.</param>
        ''' <param name="minValue">Minimum allowed value for the property.</param>
        Public Sub New(ByVal propertyName As String, ByVal minValue As T)
            MyBase.New(propertyName)
            _minValue = minValue
            _valueType = GetType(T).FullName
        End Sub

    End Class

#End Region

#Region " RegEx "

    ''' <summary>
    ''' Rule that checks to make sure a value
    ''' matches a given regex pattern.
    ''' </summary>
    ''' <param name="target">Object containing the data to validate</param>
    ''' <param name="e">RegExRuleArgs parameter specifying the name of the 
    ''' property to validate and the regex pattern.</param>
    ''' <returns>False if the rule is broken</returns>
    ''' <remarks>
    ''' This implementation uses late binding.
    ''' </remarks>
    Public Function RegExMatch(ByVal target As Object, _
      ByVal e As ValidationArgs) As Boolean

        Dim ex As RegExValidationArgs = DirectCast(e, RegExValidationArgs)
        Dim rx As Regex = ex.RegEx

        Dim tempString As String = CallByName(target, e.PropertyName, CallType.Get)

        If IsNothingOrEmptyString(tempString) Then
            Return True
        Else
            Dim isMatch As Boolean = rx.IsMatch(tempString)

            If ex.ReverseMatch Then
                isMatch = Not isMatch
            End If
            If Not isMatch Then
                If IsNothingOrEmptyString(e.CustomErrorDescription) Then
                    If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                        e.Description = _
                            String.Format(My.Resources.E00000060, e.FriendlyPropertyName)
                    Else
                        e.Description = _
                            String.Format(My.Resources.E00000060, ValidationArgs.GetPropertyName(e))
                    End If
                Else
                    If IsNothingOrEmptyString(e.FriendlyPropertyName) Then
                        e.Description = _
                            String.Format(e.CustomErrorDescription, e.FriendlyPropertyName)
                    Else
                        e.Description = _
                            String.Format(e.CustomErrorDescription, ValidationArgs.GetPropertyName(e))
                    End If
                End If


                Return False
            Else
                Return True
            End If
        End If
    End Function

    ''' <summary>
    ''' List of built-in regex patterns.
    ''' </summary>
    Public Enum RegExPatterns
        ''' <summary>
        ''' US Social Security number pattern.
        ''' </summary>
        SSN
        ''' <summary>
        ''' Email address pattern.
        ''' </summary>
        Email
    End Enum

    ''' <summary>
    ''' Custom <see cref="ValidationArgs" /> object required by the
    ''' <see cref="RegExMatch" /> rule method.
    ''' </summary>
    Public Class RegExValidationArgs
        Inherits ValidationArgs

        Private _regEx As Regex
        Private _reverseMatch As Boolean

        ''' <summary>
        ''' The <see cref="RegEx"/> object used to validate
        ''' the property.
        ''' </summary>
        Public ReadOnly Property RegEx() As Regex
            Get
                Return _regEx
            End Get
        End Property

        ''' <summary>
        ''' Retrive Reverse Match Property
        ''' </summary>
        Public ReadOnly Property ReverseMatch() As Boolean
            Get
                Return _reverseMatch
            End Get
        End Property

        ''' <summary>
        ''' Creates a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="pattern">Built-in regex pattern to use.</param>
        Public Sub New(ByVal propertyName As String, ByVal pattern As RegExPatterns)
            MyBase.New(propertyName)
            _regEx = New Regex(GetPattern(pattern))
        End Sub

        ''' <summary>
        ''' Creates a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="pattern">Custom regex pattern to use.</param>
        ''' <param name="friendlyPropertyName">friendlyPropertyName.</param>
        ''' <param name="customErrorDescription">customErrorDescription.</param>
        Public Sub New(ByVal propertyName As String, ByVal pattern As String, ByVal friendlyPropertyName As String, ByVal customErrorDescription As String, Optional ByVal reverseMatch As Boolean = False)
            MyBase.New(propertyName, friendlyPropertyName, customErrorDescription)
            _regEx = New Regex(pattern)
            _reverseMatch = reverseMatch

        End Sub

        ''' <summary>
        ''' Creates a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="pattern">Custom regex pattern to use.</param>
        Public Sub New(ByVal propertyName As String, ByVal pattern As String)
            MyBase.New(propertyName)
            _regEx = New Regex(pattern)
        End Sub

        ''' <summary>
        ''' Creates a new object.
        ''' </summary>
        ''' <param name="propertyName">Name of the property to validate.</param>
        ''' <param name="regEx"><see cref="RegEx"/> object to use.</param>
        Public Sub New(ByVal propertyName As String, ByVal regEx As Regex)
            MyBase.New(propertyName)
            _regEx = regEx
        End Sub

        ''' <summary>
        ''' Returns a string representation of the object.
        ''' </summary>
        Public Overrides Function ToString() As String
            Return MyBase.ToString & "!" & _regEx.ToString
        End Function

        ''' <summary>
        ''' Returns the specified built-in regex pattern.
        ''' </summary>
        Public Shared Function GetPattern(ByVal pattern As RegExPatterns) As String
            Select Case pattern
                Case RegExPatterns.SSN
                    Return "^\d{3}-\d{2}-\d{4}$"

                Case RegExPatterns.Email
                    Return "\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b"

                Case Else
                    Return ""
            End Select
        End Function

    End Class

#End Region



End Module
