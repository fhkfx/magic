Friend Class ValidationMethod
    Implements IValidationMethod
    Private _handler As ValidationHandler
    Private _validationName As String = ""
    Private _args As ValidationArgs

    ''' <summary>
    ''' Returns the name of the method that implementing the validation
    ''' </summary>
    Public Overrides Function ToString() As String
        Return _validationName
    End Function

    ''' <summary>
    ''' Gets the name of the validation.
    ''' </summary>
    ''' <remarks>
    ''' The validation's name must be unique and is used
    ''' to identify a error info in the collection error info of a property.
    ''' </remarks>
    Public ReadOnly Property ValidationName() As String Implements IValidationMethod.ValidationName
        Get
            Return _validationName
        End Get
    End Property

    ''' <summary>
    ''' Returns the argument of the validation
    ''' </summary>
    Public ReadOnly Property ValidationArgs() As ValidationArgs Implements IValidationMethod.ValidationArgs
        Get
            Return _args
        End Get
    End Property

    ''' <summary>
    ''' Creates and initializes the validation method.
    ''' </summary>
    ''' <param name="handler">The address of the method implementing the validation.</param>
    ''' <param name="args">A ValidationArgs object.</param>
    Public Sub New(ByVal handler As ValidationHandler, ByVal args As ValidationArgs)

        _handler = handler
        _args = args
        _validationName = String.Format("rule://{0}/{1}", _
            _handler.Method.Name, _args.ToString)
    End Sub

    ''' <summary>
    ''' Invokes the method to validate the data.
    ''' </summary>
    ''' <returns>
    ''' <see langword="true" /> if the data is valid, 
    ''' <see langword="false" /> if the data is invalid.
    ''' </returns>
    Public Function Invoke(ByVal target As Object) As Boolean Implements IValidationMethod.Invoke
        Return _handler.Invoke(target, _args)
    End Function
End Class
