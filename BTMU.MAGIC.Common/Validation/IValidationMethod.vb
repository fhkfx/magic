Friend Interface IValidationMethod
    ''' <summary>
    ''' Gets the validation name.
    ''' </summary>
    ''' <remarks>
    ''' The validation name must be unique and is used
    ''' to identify a error info in the error info 
    ''' collection.
    ''' </remarks>
    ReadOnly Property ValidationName() As String
    ''' <summary>
    ''' Returns the validation argument.
    ''' </summary>
    ReadOnly Property ValidationArgs() As ValidationArgs
    ''' <summary>
    ''' Invokes to validate the data.
    ''' </summary>
    ''' <returns>
    ''' <see langword="true" /> if the data is valid, 
    ''' <see langword="false" /> if the data is invalid.
    ''' </returns>
    Function Invoke(ByVal target As Object) As Boolean
End Interface
