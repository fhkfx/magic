Imports System.Text

Public Class ValidationEngine
    Private _errorInfoCollection As Dictionary(Of String, Dictionary(Of String, ErrorInfo))
    Private _validationMethodCollection As Dictionary(Of String, Dictionary(Of String, IValidationMethod))
    Private _target As Object
    Private _enableValidationEngine As Boolean = True

    ''' <summary>
    ''' Create Validation Engine
    ''' </summary>
    Public Sub New(ByVal target As Object)
        _target = target
        _errorInfoCollection = New Dictionary(Of String, Dictionary(Of String, ErrorInfo))
        _validationMethodCollection = New Dictionary(Of String, Dictionary(Of String, IValidationMethod))
    End Sub

    ''' <summary>
    ''' Disable Validation Engine
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EnableValidationEngine() As Boolean
        Get
            Return _enableValidationEngine
        End Get
        Set(ByVal value As Boolean)
            _enableValidationEngine = value
        End Set
    End Property
    ''' <summary>
    ''' Add new validation rule
    ''' </summary>
    Public Sub AddValidationRule(ByVal handler As ValidationHandler, ByVal args As ValidationArgs)
        Dim lstPropertyValidationCollection As Dictionary(Of String, IValidationMethod)
        Dim valMethod As ValidationMethod
        If Not _validationMethodCollection.ContainsKey(args.PropertyName) Then
            _validationMethodCollection.Add(args.PropertyName, New Dictionary(Of String, IValidationMethod))
        End If

        lstPropertyValidationCollection = _validationMethodCollection(args.PropertyName)

        valMethod = New ValidationMethod(handler, args)
        If lstPropertyValidationCollection.ContainsKey(valMethod.ValidationName) Then
            Throw New Exception(String.Format("Validation Name: {0} has already existed"))
        Else
            lstPropertyValidationCollection.Add(valMethod.ValidationName, valMethod)
        End If
    End Sub

    ''' <summary>
    ''' Validate the object against it direct property
    ''' </summary>
    Public Sub Validate()
        Dim propertyName As String
        For Each propertyName In _validationMethodCollection.Keys
            Validate(propertyName)
        Next
    End Sub

    ''' <summary>
    ''' Validate the object for certain property name
    ''' </summary>
    Public Sub Validate(ByVal propertyName As String)
        Dim lstPropertyValidationCollection As Dictionary(Of String, IValidationMethod)
        Dim validationMethod As IValidationMethod

        If _enableValidationEngine Then
            If _validationMethodCollection.ContainsKey(propertyName) Then
                lstPropertyValidationCollection = _validationMethodCollection(propertyName)
                For Each validationMethod In lstPropertyValidationCollection.Values
                    If Not validationMethod.Invoke(_target) Then
                        'Error Occur    
                        'Check whether the error info has exist
                        If _errorInfoCollection.ContainsKey(propertyName) AndAlso _
                            _errorInfoCollection(propertyName).ContainsKey(validationMethod.ValidationName) Then
                            'The error occurred before, so do nothing
                        Else
                            'add the error info
                            If Not _errorInfoCollection.ContainsKey(propertyName) Then
                                _errorInfoCollection.Add(propertyName, New Dictionary(Of String, ErrorInfo))
                            End If
                            _errorInfoCollection(propertyName).Add(validationMethod.ValidationName, New ErrorInfo(validationMethod))
                        End If
                    Else
                        If _errorInfoCollection.ContainsKey(propertyName) AndAlso _
                            _errorInfoCollection(propertyName).ContainsKey(validationMethod.ValidationName) Then
                            'The error occurred before, so do delete the error
                            _errorInfoCollection(propertyName).Remove(validationMethod.ValidationName)
                        Else
                            'do nothing
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Retrieve error information for certain object property.
    ''' </summary>
    Public Function RetrieveError() As String
        Dim propertyName As String
        Dim errMessages As New StringBuilder
        Dim errMessageTemp As String

        For Each propertyName In _errorInfoCollection.Keys
            errMessageTemp = RetrieveError(propertyName)
            If errMessageTemp.Trim.Length > 0 Then
                errMessages.Append(errMessageTemp)
            End If
        Next

        Return errMessages.ToString
    End Function

    ''' <summary>
    ''' Retrieve error information for certain property of object.
    ''' </summary>
    ''' <param name="propertyName">The name of the property where system retrieve error from.</param>
    Public Function RetrieveError(ByVal propertyName As String) As String
        Dim errInfo As ErrorInfo
        Dim errMessages As New StringBuilder
        If _errorInfoCollection.ContainsKey(propertyName) AndAlso _
            _errorInfoCollection(propertyName).Count > 0 Then

            For Each errInfo In _errorInfoCollection(propertyName).Values
                errMessages.AppendLine(errInfo.Description)
            Next

            Return errMessages.ToString
        Else

            Return String.Empty

        End If

    End Function

    ''' <summary>
    ''' Check whether there is any error in object.
    ''' </summary>
    Public Function IsValid() As Boolean
        Dim errList As Dictionary(Of String, ErrorInfo)
        If _errorInfoCollection.Count = 0 Then
            Return True
        End If

        For Each errList In _errorInfoCollection.Values
            If errList.Count > 0 Then
                Return False
            End If
        Next

        Return True

    End Function

End Class
