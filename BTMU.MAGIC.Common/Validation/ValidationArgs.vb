Public Class ValidationArgs

    Private _propertyName As String
    Private _friendlyPropertyName As String
    Private _description As String
    Private _customErrorDescription As String

    ''' <summary>
    ''' The name of the property to be validated.
    ''' </summary>
    Public ReadOnly Property PropertyName() As String
        Get
            Return _propertyName
        End Get
    End Property

    ''' <summary>
    ''' The friendly name of the property to be validated.
    ''' </summary>
    Public ReadOnly Property FriendlyPropertyName() As String
        Get
            Return _friendlyPropertyName
        End Get
    End Property

    ''' <summary>
    ''' The Custom Error Description.
    ''' </summary>
    Public ReadOnly Property CustomErrorDescription() As String
        Get
            Return _customErrorDescription
        End Get
    End Property

    ''' <summary>
    ''' Description of the Error.
    ''' </summary>
    ''' <value>A human-readable description of error.</value>
    ''' <remarks>
    ''' Setting this property only has an effect if
    ''' the rule method returns <see langword="false" />.
    ''' </remarks>
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property

    ''' <summary>
    ''' Creates an instance of ValidationArgs.
    ''' </summary>
    ''' <param name="propertyName">The name of the property to be validated.</param>
    Public Sub New(ByVal propertyName As String)
        _propertyName = propertyName
    End Sub

    ''' <summary>
    ''' Creates an instance of ValidationArgs.
    ''' </summary>
    ''' <param name="propertyName">The name of the property to be validated.</param>
    ''' <param name="friendlyPropertyName">friendlyPropertyName.</param>
    Public Sub New(ByVal propertyName As String, ByVal friendlyPropertyName As String)
        _propertyName = propertyName
        _friendlyPropertyName = friendlyPropertyName
    End Sub

    ''' <summary>
    ''' Creates an instance of ValidationArgs.
    ''' </summary>
    ''' <param name="propertyName">The name of the property to be validated.</param>
    ''' <param name="friendlyPropertyName">friendlyPropertyName.</param>
    ''' <param name="customErrorDescription">customErrorDescription.</param>
    Public Sub New(ByVal propertyName As String, ByVal friendlyPropertyName As String, ByVal customErrorDescription As String)
        _propertyName = propertyName
        _friendlyPropertyName = friendlyPropertyName
        _customErrorDescription = customErrorDescription
    End Sub

    ''' <summary>
    ''' Returns a string representation of the object.
    ''' </summary>
    Public Overrides Function ToString() As String
        Return _propertyName
    End Function

    ''' <summary>
    ''' Gets the property name from the ValidationArgs
    ''' object.
    ''' </summary>
    ''' <param name="e">Object from which to extract the name.</param>
    ''' <returns>
    ''' The name of property.
    ''' </returns>
    Public Shared Function GetPropertyName(ByVal e As ValidationArgs) As String
        Return e.PropertyName
    End Function

    ''' <summary>
    ''' Gets the friendly property name from the ValidationArgs
    ''' object.
    ''' </summary>
    ''' <param name="e">Object from which to extract the name.</param>
    ''' <returns>
    ''' The friednly name of property.
    ''' </returns>
    Public Shared Function GetFriendlyPropertyName(ByVal e As ValidationArgs) As String
        Return e.FriendlyPropertyName
    End Function

End Class
