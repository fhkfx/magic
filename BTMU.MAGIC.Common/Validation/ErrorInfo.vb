Public Class ErrorInfo
    Private _validationName As String
    Private _description As String
    Private _property As String

    ''' <summary>
    ''' Creates and initializes the ErrorInfo.
    ''' </summary>
    ''' <param name="validationMethod">Attach Validation Method to Error Object.</param>
    Friend Sub New(ByVal validationMethod As IValidationMethod)
        _validationName = validationMethod.ValidationName
        _description = validationMethod.ValidationArgs.Description
        _property = validationMethod.ValidationArgs.PropertyName
    End Sub


    ''' <summary>
    ''' Provides access to the name of the validation.
    ''' </summary>
    ''' <value>The name of the rule.</value>
    Public ReadOnly Property ValidationName() As String
        Get
            Return _validationName
        End Get
    End Property

    ''' <summary>
    ''' Provides access to the description of the error.
    ''' </summary>
    ''' <value>The description of the error.</value>
    Public ReadOnly Property Description() As String
        Get
            Return _description
        End Get
    End Property

    ''' <summary>
    ''' Provides access to the property affected by the validation.
    ''' </summary>
    ''' <value>The property affected by the validation.</value>
    Public ReadOnly Property [Property]() As String
        Get
            Return _property
        End Get
    End Property
End Class
