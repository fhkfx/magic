''' <summary>
''' Delegate that defines the method signature for all validation handler methods.
''' </summary>
''' <param name="target">
''' Object that containing the data to be validated.
''' </param>
''' <param name="e">
''' Parameter used to pass information to and from the validation method.
''' </param>
''' <returns>
''' <see langword="true" /> if the validation was satisfied.
''' </returns>
''' <remarks>
''' <para>
''' When implementing a validation handler, you must conform to the method signature
''' defined by this delegate. 
''' </para><para>
''' The method implementing the validation must return 
''' <see langword="true"/> if the data is valid and
''' return <see langword="false"/> if the data is invalid.
''' </para>
''' </remarks>
Public Delegate Function ValidationHandler(ByVal target As Object, ByVal e As validationArgs) As Boolean

''' <summary>
''' Delegate that defines the method signature for all validation handler methods.
''' </summary>
''' <typeparam name="T">Type of the target object.</typeparam>
''' <typeparam name="R">Type of the arguments parameter.</typeparam>
''' <param name="target">
''' Object that containing the data to be validated.
''' </param>
''' <param name="e">
''' Parameter used to pass information to and from the rule method.
''' </param>
''' <returns>
''' <see langword="true" /> if the rule was satisfied.
''' </returns>
''' <remarks>
''' <para>
''' When implementing a validation handler, you must conform to the method signature
''' defined by this delegate. 
''' </para><para>
''' The method implementing the validation must return 
''' <see langword="true"/> if the data is valid and
''' return <see langword="false"/> if the data is invalid.
''' </para>
''' </remarks>
Public Delegate Function ValidationHandler(Of T, R As ValidationArgs) _
    (ByVal target As T, ByVal e As R) As Boolean