Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Data
Imports System.Reflection
Imports System.Reflection.Emit
Imports System.Runtime.Remoting
Imports System.Text
Imports System.Text.RegularExpressions

Friend Class DefaultFilterEngine(Of T)
    Implements IFilterEngine(Of T)

    Private _filterString As String
    Private _predicates As List(Of Predicate(Of Object)) = New List(Of Predicate(Of Object))

#Region " IFilterEngine Members "
    Public Function Evaluate(ByVal item As Object) As Boolean Implements IFilterEngine(Of T).Evaluate
        Dim predicate As Predicate(Of Object)
        For Each predicate In _predicates
            If Not predicate(item) Then Return False
        Next
        Return True
    End Function

    Public Property FilterString() As String Implements IFilterEngine(Of T).FilterString
        Get
            Return _filterString
        End Get
        Set(ByVal value As String)
            If _filterString <> value Then
                _filterString = value
                BuildPredicates()
            End If
        End Set
    End Property
#End Region

    Public Sub New()

    End Sub

    Public Sub New(ByVal filterString As String)
        filterString = filterString
    End Sub

    Private Sub BuildPredicates()
        _predicates.Clear()

        If Not String.IsNullOrEmpty(_filterString) Then
            Dim part As String
            Dim regex As Regex = New Regex(" AND ", RegexOptions.IgnoreCase)
            Dim parts() As String = regex.Split(_filterString)
            For Each part In parts
                Dim predicate As Predicate(Of Object) = GetPredicate(part)
                _predicates.Add(predicate)
            Next
        End If
    End Sub

    Private Function GetPredicate(ByVal expression As String) As Predicate(Of Object)
        Dim predicate As Predicate(Of Object) = Nothing
        Dim filterParts() As String = expression.Split("=")
        If filterParts.Length <> 2 Or filterParts(0) = String.Empty Or filterParts(1) = String.Empty Then
            Throw New ArgumentException("Filter string must be of the form 'properyName=value'.", "Filter")
        End If

        'Trim whitespace from property name and value.
        Dim propertyName As String = filterParts(0).Trim()
        Dim propertyValue As String = filterParts(1).Trim()

        ' Trim square brackets
        If (Not String.IsNullOrEmpty(propertyName)) Then

            If (propertyName(0) = "[") Then
                If (propertyName(propertyName.Length - 1) <> "]") Then
                    Throw New ArgumentException("Unbalanced brackets in property name.", "Filter")
                End If
                propertyName = propertyName.Trim(New Char() {"[", "]"})
            End If
        End If

        ' Get a property descriptor for the filter property
        Dim [property] As PropertyDescriptor = TypeDescriptor.GetProperties(GetType(T))(propertyName)
        If ([property] Is Nothing) Then
            Throw New ArgumentException("The property '" + propertyName + "' is not a property of the list item type.", "Filter")
        End If

        If [property].PropertyType Is GetType(String) Then
            ' check for regular expression
            Dim exp As String = GetRegularExpression(propertyValue)
            If (Not String.IsNullOrEmpty(exp)) Then
                propertyValue = exp
                Dim pred As StringComparerPredicate = New StringComparerPredicate([property], propertyValue, True)
                predicate = New Predicate(Of Object)(AddressOf pred.Matches)
                Return predicate
            End If
        End If

        ' Trim quoted values.
        If (propertyValue(0) = "'") Then
            If (propertyValue(propertyValue.Length - 1) <> "'") Then
                Throw New ArgumentException("Unbalanced quotes in property value.", "Filter")
            End If
            propertyValue = propertyValue.Trim(New Char() {"'"})
        ElseIf (propertyValue(0) = """") Then
            If (propertyValue(propertyValue.Length - 1) <> """") Then
                Throw New ArgumentException("Unbalanced quotes in property value.", "Filter")
            End If
            propertyValue = propertyValue.Trim(New Char() {""""})
        End If


        If (String.Compare(propertyValue, "null", True) = 0) Then
            propertyValue = nothing
        End If
        Dim propertyType As Type = [property].PropertyType
        If propertyType Is GetType(SByte) Then
            Dim pred As SByteComparerPredicate = New SByteComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Byte)) Then
            Dim pred As ByteComparerPredicate = New ByteComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Int16)) Then
            Dim pred As Int16ComparerPredicate = New Int16ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(UInt16)) Then
            Dim pred As UInt16ComparerPredicate = New UInt16ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Int32)) Then
            Dim pred As Int32ComparerPredicate = New Int32ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(UInt32)) Then
            Dim pred As UInt32ComparerPredicate = New UInt32ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Int64)) Then
            Dim pred As Int64ComparerPredicate = New Int64ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(UInt64)) Then
            Dim pred As UInt64ComparerPredicate = New UInt64ComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Char)) Then
            Dim pred As CharComparerPredicate = New CharComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Single)) Then
            Dim pred As SingleComparerPredicate = New SingleComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Double)) Then
            Dim pred As DoubleComparerPredicate = New DoubleComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Boolean)) Then
            Dim pred As BooleanComparerPredicate = New BooleanComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf (propertyType Is GetType(Decimal)) Then
            Dim pred As DecimalComparerPredicate = New DecimalComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        ElseIf ([property].PropertyType Is GetType(String)) Then
            Dim pred As StringComparerPredicate = New StringComparerPredicate([property], propertyValue, False)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        Else
            Dim pred As PropertyComparerPredicate = New PropertyComparerPredicate([property], propertyValue)
            predicate = New Predicate(Of Object)(AddressOf pred.Matches)
        End If

        Return predicate
    End Function

    Private Function GetRegularExpression(ByVal input As String) As String
        Dim exp As String = String.Empty
        Dim regex As Regex = New Regex("regex\s*\(\s*\""([^""]*)\""\s*\)", RegexOptions.IgnoreCase)

        ' Check for match
        Dim isMatch As Boolean = regex.IsMatch(input)
        If isMatch Then
            ' Get match
            Dim match As Match = regex.Match(input)
            If Not match Is Nothing AndAlso match.Groups.Count > 1 Then
                Dim group As Group = match.Groups(1)
                exp = group.Value
            End If
        End If
        Return exp
    End Function

End Class

Friend Class PropertyComparerPredicate

    Private _value As Object
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")

        _property = [property]
        If value Is Nothing Then
            _value = Nothing
        Else
            _value = [property].Converter.ConvertFromString(value)
        End If
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")

        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")

        Dim testValue As Object = _property.GetValue(item)

        Dim result As Boolean = False
        If _value Is Nothing Then
            result = (testValue Is Nothing)
        Else
            result = (DirectCast(_value, IComparable).CompareTo(testValue) = 0)
        End If
        Return result
    End Function
End Class

Friend Class SByteComparerPredicate

    Private _value As SByte
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")


        If [property].PropertyType Is GetType(SByte) Then Throw New ArgumentException("property")

        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")

        _property = [property]
        _value = SByte.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Dim testValue As SByte = DirectCast(_property.GetValue(item), SByte)
        Return _value.CompareTo(testValue) = 0
    End Function
End Class

Friend Class ByteComparerPredicate
    Private _value As Byte
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Byte) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Byte.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class Int16ComparerPredicate
    Private _value As Int16
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Int16) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Int16.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class UInt16ComparerPredicate
    Private _value As UInt16
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(UInt16) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = UInt16.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class Int32ComparerPredicate
    Private _value As Int32
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Int32) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Int32.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class UInt32ComparerPredicate
    Private _value As UInt32
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(UInt32) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = UInt32.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class Int64ComparerPredicate
    Private _value As Int64
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Int64) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Int64.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class UInt64ComparerPredicate
    Private _value As UInt64
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(UInt64) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = UInt64.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class CharComparerPredicate
    Private _value As Char
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Char) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Char.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class SingleComparerPredicate
    Private _value As Single
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Single) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Single.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class DoubleComparerPredicate
    Private _value As Double
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Double) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Double.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class BooleanComparerPredicate
    Private _value As Boolean
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Boolean) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Boolean.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class DecimalComparerPredicate
    Private _value As Decimal
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(Decimal) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = Decimal.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class DateTimeComparerPredicate
    Private _value As DateTime
    Private _property As PropertyDescriptor

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String)
        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        If Not [property].PropertyType Is GetType(DateTime) Then Throw New ArgumentException("property")
        If String.IsNullOrEmpty(value) Then Throw New ArgumentNullException("value")
        _property = [property]
        _value = DateTime.Parse(value)
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")
        Return _value.CompareTo(_property.GetValue(item)) = 0
    End Function
End Class

Friend Class StringComparerPredicate
    Private _value As String
    Private _property As PropertyDescriptor
    Private _regEx As Regex

    Public Sub New(ByVal [property] As PropertyDescriptor, ByVal value As String, ByVal isRegex As Boolean)

        If [property] Is Nothing Then Throw New ArgumentNullException("property")
        _property = [property]
        _value = value

        If isRegex Then
            _regEx = New Regex(_value, RegexOptions.IgnoreCase)
        Else
            If _value.IndexOfAny(New Char() {"%", "*"}) > -1 Then
                Dim expr As StringBuilder = New StringBuilder()
                Dim c As Char
                expr.Append("^")
                For Each c In _value
                    If (c = "*" Or c = "%") Then
                        expr.Append(".*")
                    Else
                        expr.Append(Regex.Escape(c))
                    End If
                    _regEx = New Regex(expr.ToString(), RegexOptions.IgnoreCase)
                Next
            End If
        End If
    End Sub

    Public Function Matches(ByVal item As Object) As Boolean
        If item Is Nothing Then Throw New ArgumentNullException("item")
        If Not _property.ComponentType Is item.GetType() Then Throw New ArgumentException("item")

        Dim testValue As String = DirectCast(_property.GetValue(item), String)
        Dim result As Boolean = False
        If String.IsNullOrEmpty(_value) Then
            result = String.IsNullOrEmpty(testValue)
        Else
            If Not String.IsNullOrEmpty(testValue) Then
                If _regEx Is Nothing Then
                    result = (_value.CompareTo(testValue) = 0)
                Else
                    Dim m As Match = _regEx.Match(testValue)
                    result = m.Success
                End If
            End If
            End If
        Return result
    End Function
End Class
