Imports System.Xml.XPath
Imports System.Xml.Serialization

<Serializable()> _
Public Class QuickConfigurationItem
    '  Implements System.Xml.Serialization.IXmlSerializable  'Test

    Private _sourceFormat As String
    Private _templateName As String
    Private _commonTemplateName As String
    Private _sourceWorkSheet As String
    Private _sourceFileName As String
    Private _saveLocation As String

    Private _definitionFile As String

    Private _valueDateCheck As Boolean
    Private _valueDate As Integer
    Private _outputFileName As String
    Private _trimData As Boolean

    Private _eliminateChar As String
    Private _transactionEndsRow As Integer

    Private _removeRowsFromEnd As Integer

    Private _consolidateData As Boolean
    Private _consolidateGroupFields As String
    Private _consolidateRefAppend As String
    Private _consolidateRefField As String
    Private _consolidationPaymentMode As String
    Private _consolidationAmount As Boolean

    Public Sub New()
        _sourceFormat = " "
        _templateName = " "
        _commonTemplateName = " "
        _sourceWorkSheet = " "

        _sourceFileName = " "
        _saveLocation = " "

        _definitionFile = " "

        _valueDateCheck = False
        _valueDate = 0
        _outputFileName = " "
        _trimData = False

        _eliminateChar = " "
        _transactionEndsRow = 0

        _removeRowsFromEnd = 0

        _consolidateData = False

        _consolidateGroupFields = " "
        _consolidateRefAppend = " "
        _consolidateRefField = " "
        _consolidationPaymentMode = "+"
        _consolidationAmount = False
    End Sub

    Public Sub SetEmptyString()  'Fix the  deserialization bug of .NET
        If IsNothingOrEmptyString(_sourceFormat) Then _sourceFormat = " "
        If IsNothingOrEmptyString(_templateName) Then _templateName = " "
        If IsNothingOrEmptyString(_commonTemplateName) Then _commonTemplateName = " "
        If IsNothingOrEmptyString(_sourceWorkSheet) Then _sourceWorkSheet = " "

        If IsNothingOrEmptyString(_sourceFileName) Then _sourceFileName = " "
        If IsNothingOrEmptyString(_saveLocation) Then _saveLocation = " "

        If IsNothingOrEmptyString(_definitionFile) Then _definitionFile = " "

        If IsNothingOrEmptyString(_outputFileName) Then _outputFileName = " "

        If IsNothingOrEmptyString(_eliminateChar) Then _eliminateChar = " "

        If IsNothingOrEmptyString(_consolidateGroupFields) Then _consolidateGroupFields = " "
        If IsNothingOrEmptyString(_consolidateRefAppend) Then _consolidateRefAppend = " "
        If IsNothingOrEmptyString(_consolidateRefField) Then _consolidateRefField = " "

        If IsNothingOrEmptyString(_consolidationPaymentMode) Then _consolidationPaymentMode = "+"
    End Sub

#Region " Serialization "
    '    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
    '        Return Nothing
    '    End Function

    '    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
    '        Try
    '            _sourceFormat = ReadXMLElement(reader, "SourceFormat")
    '            _templateName = ReadXMLElement(reader, "TemplateName")
    '            _commonTemplateName = ReadXMLElement(reader, "CommonTemplateName")
    '            _sourceWorkSheet = ReadXMLElement(reader, "SourceWorkSheet")

    '            _sourceFileName = ReadXMLElement(reader, "SourceFileName")
    '            _saveLocation = ReadXMLElement(reader, "SaveLocation")

    '            _valueDateCheck = ReadXMLElement(reader, "ValueDateCheck")
    '            _valueDate = ReadXMLElement(reader, "ValueDate")
    '            _outputFileName = ReadXMLElement(reader, "OutputFileName")
    '            _trimData = ReadXMLElement(reader, "TrimData")

    '            _eliminateChar = ReadXMLElement(reader, "EliminateChar")
    '            _transactionEndsRow = ReadXMLElement(reader, "TransactionEndsRow")

    '            _removeRowsFromEnd = ReadXMLElement(reader, "RemoveRowsFromEnd")

    '            _consolidateData = ReadXMLElement(reader, "ConsolidateData")

    '            _consolidateGroupFields = ReadXMLElement(reader, "ConsolidateGroupFields")
    '            _consolidateRefAppend = ReadXMLElement(reader, "ConsolidateRefAppend")
    '            _consolidateRefField = ReadXMLElement(reader, "ConsolidateRefField")
    '            _consolidationPaymentMode = ReadXMLElement(reader, "ConsolidationPaymentMode")
    '            _consolidationAmount = ReadXMLElement(reader, "ConsolidationAmount")
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub
    '    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml


    '        WriteXmlElement(writer, "SourceFormat", _sourceFormat)
    '        WriteXmlElement(writer, "TemplateName", _templateName)
    '        WriteXmlElement(writer, "CommonTemplateName", _commonTemplateName)
    '        WriteXmlElement(writer, "SourceWorkSheet", _sourceWorkSheet)

    '        WriteXmlElement(writer, "SourceFileName", _sourceFileName)
    '        WriteXmlElement(writer, "SaveLocation", _saveLocation)

    '        WriteXmlElement(writer, "ValueDateCheck", _valueDateCheck)
    '        WriteXmlElement(writer, "ValueDate", _valueDate)
    '        WriteXmlElement(writer, "OutputFileName", _outputFileName)
    '        WriteXmlElement(writer, "TrimData", _trimData)

    '        WriteXmlElement(writer, "EliminateChar", _eliminateChar)
    '        WriteXmlElement(writer, "TransactionEndsRow", _transactionEndsRow)

    '        WriteXmlElement(writer, "RemoveRowsFromEnd", _removeRowsFromEnd)
    '        WriteXmlElement(writer, "ConsolidateData", _consolidateData)

    '        WriteXmlElement(writer, "ConsolidateGroupFields", _consolidateGroupFields)
    '        WriteXmlElement(writer, "ConsolidateRefAppend", _consolidateRefAppend)
    '        WriteXmlElement(writer, "ConsolidateRefField", _consolidateRefField)
    '        WriteXmlElement(writer, "ConsolidationPaymentMode", _consolidationPaymentMode)
    '        WriteXmlElement(writer, "ConsolidationAmount", _consolidationAmount)
    '    End Sub

    '    Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
    '        writer.WriteStartElement(elementName)
    '        writer.WriteString(value)
    '        writer.WriteEndElement()
    '    End Sub

    '    Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
    '        Dim returnValue As String
    '        reader.ReadStartElement(elementName)
    '        If (reader.HasValue) Then
    '            returnValue = reader.ReadContentAsString()
    '            reader.ReadEndElement()
    '            Return returnValue
    '        Else

    '            Return Nothing
    '        End If

    '    End Function
#End Region  'Test

    Public Property TemplateName() As String
        Get
            Return _templateName
        End Get
        Set(ByVal value As String)
            _templateName = value
        End Set
    End Property

    Public Property CommonTemplateName() As String
        Get
            Return _commonTemplateName
        End Get
        Set(ByVal value As String)
            _commonTemplateName = value
        End Set
    End Property

    Public Property SourceFormat() As String
        Get
            Return _sourceFormat
        End Get
        Set(ByVal value As String)
            _sourceFormat = value
        End Set
    End Property

    Public Property SourceWorkSheet() As String
        Get
            Return _sourceWorkSheet
        End Get
        Set(ByVal value As String)
            _sourceWorkSheet = value
        End Set
    End Property

    Public Property SourceFileName() As String
        Get
            Return _sourceFileName
        End Get
        Set(ByVal value As String)
            _sourceFileName = value
        End Set
    End Property

    Public Property SaveLocation() As String
        Get
            Return _saveLocation
        End Get
        Set(ByVal value As String)
            _saveLocation = value
        End Set
    End Property

    Public Property DefinitionFile() As String
        Get
            Return _definitionFile
        End Get
        Set(ByVal value As String)
            _definitionFile = value
        End Set
    End Property

    Public Property ValueDateCheck() As Boolean
        Get
            Return _valueDateCheck
        End Get
        Set(ByVal value As Boolean)
            _valueDateCheck = value
        End Set
    End Property

    Public Property ValueDate() As Integer
        Get
            Return _valueDate
        End Get
        Set(ByVal value As Integer)
            _valueDate = value
        End Set
    End Property

    Public Property OutputFileName() As String
        Get
            Return _outputFileName
        End Get
        Set(ByVal value As String)
            _outputFileName = value
        End Set
    End Property

    Public Property TrimData() As Boolean
        Get
            Return _trimData
        End Get
        Set(ByVal value As Boolean)
            _trimData = value
        End Set
    End Property

    Public Property EliminateChar() As String
        Get
            Return _eliminateChar
        End Get
        Set(ByVal value As String)
            _eliminateChar = value
        End Set
    End Property

    Public Property TransactionEndsRow() As Integer
        Get
            Return _transactionEndsRow
        End Get
        Set(ByVal value As Integer)
            _transactionEndsRow = value
        End Set
    End Property


    Public Property RemoveRowsFromEnd() As Integer
        Get
            Return _removeRowsFromEnd
        End Get
        Set(ByVal value As Integer)
            _removeRowsFromEnd = value
        End Set
    End Property

    Public Property ConsolidateData() As Boolean
        Get
            Return _consolidateData
        End Get
        Set(ByVal value As Boolean)
            _consolidateData = value
        End Set
    End Property


    Public Property ConsolidateGroupFields() As String
        Get
            Return _consolidateGroupFields
        End Get
        Set(ByVal value As String)
            _consolidateGroupFields = value
        End Set
    End Property

    Public Property ConsolidateRefAppend() As String
        Get
            Return _consolidateRefAppend
        End Get
        Set(ByVal value As String)
            _consolidateRefAppend = value
        End Set
    End Property

    Public Property ConsolidateRefField() As String
        Get
            Return _consolidateRefField
        End Get
        Set(ByVal value As String)
            _consolidateRefField = value
        End Set
    End Property

    Public Property ConsolidationPaymentMode() As String
        Get
            Return _consolidationPaymentMode
        End Get
        Set(ByVal value As String)
            _consolidationPaymentMode = value
        End Set
    End Property

    Public Property ConsolidationAmount() As Boolean
        Get
            Return _consolidationAmount
        End Get
        Set(ByVal value As Boolean)
            _consolidationAmount = value
        End Set
    End Property

End Class
