Imports System.Xml.XPath
Imports System.Xml.Serialization

<Serializable()> _
Public Class ConfigurationMaster
    '    Implements System.Xml.Serialization.IXmlSerializable 'Test
    Private Shared _msgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    Private _loginMode As Boolean
    Private _setup1 As New QuickConfigurationItem
    Private _setup2 As New QuickConfigurationItem
    Private _setup3 As New QuickConfigurationItem
    Private _setup4 As New QuickConfigurationItem
    Private _setup5 As New QuickConfigurationItem
    Private _setup6 As New QuickConfigurationItem

    Public Sub New()
        _loginMode = False
    End Sub

#Region " Serialization "
    'Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
    '    Return Nothing
    'End Function

    'Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
    '    Try
    '        reader.ReadStartElement()

    '        _loginMode = ReadXMLElement(reader, "LoginMode")

    '        reader.ReadStartElement("Setup1")
    '        DirectCast(_setup1, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadStartElement("Setup2")
    '        DirectCast(_setup2, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadStartElement("Setup3")
    '        DirectCast(_setup3, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadStartElement("Setup4")
    '        DirectCast(_setup4, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadStartElement("Setup5")
    '        DirectCast(_setup5, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadStartElement("Setup6")
    '        DirectCast(_setup6, IXmlSerializable).ReadXml(reader)
    '        reader.ReadEndElement()

    '        reader.ReadEndElement()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
    'Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
    '    WriteXmlElement(writer, "LoginMode", _loginMode)

    '    writer.WriteStartElement("Setup1")
    '    DirectCast(_setup1, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()

    '    writer.WriteStartElement("Setup2")
    '    DirectCast(_setup2, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()

    '    writer.WriteStartElement("Setup3")
    '    DirectCast(_setup3, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()

    '    writer.WriteStartElement("Setup4")
    '    DirectCast(_setup4, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()

    '    writer.WriteStartElement("Setup5")
    '    DirectCast(_setup5, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()

    '    writer.WriteStartElement("Setup6")
    '    DirectCast(_setup6, IXmlSerializable).WriteXml(writer)
    '    writer.WriteEndElement()
    'End Sub

    'Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
    '    writer.WriteStartElement(elementName)
    '    writer.WriteString(value)
    '    writer.WriteEndElement()
    'End Sub

    'Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
    '    Dim returnValue As String
    '    reader.ReadStartElement(elementName)
    '    If (reader.HasValue) Then
    '        returnValue = reader.ReadContentAsString()
    '        reader.ReadEndElement()
    '        Return returnValue
    '    Else
    '        Return Nothing
    '    End If

    'End Function
#End Region  'Test


    Public Property LoginMode() As Boolean
        Get
            Return _loginMode
        End Get
        Set(ByVal value As Boolean)
            _loginMode = value
        End Set
    End Property

    Public Property Setup1() As QuickConfigurationItem
        Get
            Return _setup1
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup1 = value
        End Set
    End Property

    Public Property Setup2() As QuickConfigurationItem
        Get
            Return _setup2
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup2 = value
        End Set
    End Property

    Public Property Setup3() As QuickConfigurationItem
        Get
            Return _setup3
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup3 = value
        End Set
    End Property

    Public Property Setup4() As QuickConfigurationItem
        Get
            Return _setup4
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup4 = value
        End Set
    End Property

    Public Property Setup5() As QuickConfigurationItem
        Get
            Return _setup5
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup5 = value
        End Set
    End Property

    Public Property Setup6() As QuickConfigurationItem
        Get
            Return _setup6
        End Get
        Set(ByVal value As QuickConfigurationItem)
            _setup6 = value
        End Set
    End Property

    Public Shared Function ReadConfigurationFromFile(ByVal fileName As String) As ConfigurationMaster
        Try
            If System.IO.File.Exists(fileName) Then
                Dim confMaster As ConfigurationMaster
                Dim serializer As New XmlSerializer(GetType(BTMU.MAGIC.Common.ConfigurationMaster))
                Dim xmlData As String
                Dim cipherText As String = System.IO.File.ReadAllText(fileName)

                If cipherText.Trim.Length > 0 Then
                    Try
                        xmlData = BTMU.MAGIC.Common.HelperModule.AESDecrypt(cipherText)
                    Catch ex As Exception
                        Throw New Exception( _
                            String.Format(_msgReader.GetString("E00000005"), fileName))
                        Return Nothing
                    End Try
                    Dim stream As New System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(xmlData))
                    confMaster = serializer.Deserialize(stream)
                    confMaster.Setup1.SetEmptyString()
                    confMaster.Setup2.SetEmptyString()
                    confMaster.Setup3.SetEmptyString()
                    confMaster.Setup4.SetEmptyString()
                    confMaster.Setup5.SetEmptyString()
                    confMaster.Setup6.SetEmptyString()
                    stream.Close()

                    Return confMaster
                Else
                    Throw New Exception( _
                    String.Format(_msgReader.GetString("E00000004"), fileName))
                End If
            Else
                Throw New Exception(String.Format(_msgReader.GetString("E00000003"), _
                    fileName))
                Exit Function
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
