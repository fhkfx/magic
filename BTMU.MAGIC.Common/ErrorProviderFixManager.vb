Imports System.Reflection
Imports System.ComponentModel
Imports System.Windows.Forms


Public Class ErrorProviderFixManager
    Implements IDisposable


    Private _theErrorProvider As ErrorProvider = Nothing
    Private _tmrCheckHandelsProc As Timer = Nothing
    Private _hashOfNativeWindows As New Hashtable

    Public Sub Dispose() Implements IDisposable.Dispose
        _tmrCheckHandelsProc.Stop()
        _tmrCheckHandelsProc.Dispose()
        _tmrCheckHandelsProc = Nothing
    End Sub

    ''' <SUMMARY>
    ''' constructor, which will started a timer that will
    ''' keep the errorProviders tooltip window up-to-date and enabled.
    ''' </SUMMARY>
    ''' <param name="ep"></param>
    Public Sub New(ByVal ep As ErrorProvider)
        _theErrorProvider = ep
        _tmrCheckHandelsProc = New Timer()
        _tmrCheckHandelsProc.Enabled = True
        _tmrCheckHandelsProc.Interval = 1000
        AddHandler _tmrCheckHandelsProc.Tick, AddressOf tmr_CheckHandels
    End Sub

    ''' <SUMMARY>
    ''' Resets the error provider, error messages.  I've noticed that when
    ''' you click on an error provider, while its tooltip is displayed,
    ''' the tooltip doesn't return.  It will return if the text is reset, or
    ''' if the user is able to hover over another error provider message for
    ''' that errorProvider instance.
    ''' </SUMMARY>
    Private Sub RefreshProviderErrors()
        Dim hashRes As Hashtable = CType(GetFieldValue(_theErrorProvider, "items"), Hashtable)
        Dim ctl As Control
        For Each ctl In hashRes.Keys

            If hashRes(ctl) Is Nothing Then
                Exit For
            End If
            If Not GetFieldValue(hashRes(ctl), "toolTipShown") Then

                If _theErrorProvider.GetError(ctl) IsNot Nothing And _
                    _theErrorProvider.GetError(ctl).Length > 0 Then

                    Dim str As String = _theErrorProvider.GetError(ctl)
                    Dim prev As ErrorBlinkStyle = _theErrorProvider.BlinkStyle
                    _theErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink
                    _theErrorProvider.SetError(ctl, String.Empty)
                    _theErrorProvider.SetError(ctl, str.Replace("&&", "&").Replace("&", "&&"))
                    _theErrorProvider.BlinkStyle = prev
                End If

            End If
        Next
    End Sub

    ''' <SUMMARY>
    ''' This method checks to see if the error provider's tooltip window has
    ''' changed and if it updates this Native window with the new handle.
    ''' 
    ''' If there is some sort of change, it also calls the RefreshProviderErrors, 
    ''' which fixes the tooltip problem...
    ''' </SUMMARY>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Sub tmr_CheckHandels(ByVal sender As Object, ByVal e As EventArgs)

        If _theErrorProvider.ContainerControl Is Nothing Then
            Return
        End If

        Dim hook As ErrorProviderNativeWindowHook
        If _theErrorProvider.ContainerControl.Visible Then
            Dim hashRes As Hashtable = CType(GetFieldValue(_theErrorProvider, "windows"), Hashtable)
            If hashRes.Count > 0 Then
                Dim obj As Object
                For Each obj In hashRes.Keys
                    hook = New ErrorProviderNativeWindowHook
                    If _hashOfNativeWindows.Contains(obj) Then
                        hook = CType(_hashOfNativeWindows(obj), ErrorProviderNativeWindowHook)
                    Else

                        hook = New ErrorProviderNativeWindowHook()
                        _hashOfNativeWindows(obj) = hook
                    End If

                    Dim ntvWindow As NativeWindow = CType(GetFieldValue(hashRes(obj), "tipWindow"), NativeWindow)
                    If ntvWindow IsNot Nothing And hook.Handle = IntPtr.Zero Then
                        hook.AssignHandle(ntvWindow.Handle)
                    End If
                Next
            End If

            For Each hook In _hashOfNativeWindows.Values
                If hook._blnTrigerRefresh Then
                    hook._blnTrigerRefresh = False
                    RefreshProviderErrors()
                End If
            Next
        End If
    End Sub

    ''' <SUMMARY>
    ''' A helper method, which allows us to get the value of private fields.
    ''' </SUMMARY>
    ''' <param name="instance">the object instance</param>
    ''' <param name="name">the name of the field, which we want to get</param>
    ''' <RETURNS>the value of the private field</RETURNS>
    Private Function GetFieldValue(ByVal instance As Object, ByVal name As String) As Object

        If instance Is Nothing Then Return Nothing

        Dim fInfo As FieldInfo = Nothing
        Dim type As Type = instance.GetType()
        While fInfo Is Nothing And type IsNot Nothing
            fInfo = type.GetField(name, System.Reflection.BindingFlags.FlattenHierarchy Or System.Reflection.BindingFlags.NonPublic Or BindingFlags.Instance)
            type = type.BaseType
        End While

        Return fInfo.GetValue(instance)
    End Function
End Class