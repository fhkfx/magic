''' <summary>
''' Encapsulates the logic to perform Group BY on DataTables
''' </summary>
''' <remarks></remarks>
Public Class DataSetHelper

    Private m_FieldInfo As ArrayList, m_FieldList As String
    Private GroupByFieldInfo As ArrayList, GroupByFieldList As String

    Private Class FieldInfo
        Public RelationName As String
        Public FieldName As String      ' source table field name
        Public FieldAlias As String     ' destination table field name
        Public Aggregate As String
    End Class

    Public ds As DataSet

    Public Sub New(ByVal _DataSet As DataSet)
        ds = _DataSet
    End Sub

    Public Sub New()
        ds = Nothing
    End Sub
    '
    ' Parses FieldList into FieldInfo objects and then adds them to the m_FieldInfo private member
    '
    ' FieldList syntax: [relationname.]fieldname=[alias],...
    '
    Private Sub ParseFieldList(ByVal FieldList As String, Optional ByVal AllowRelation As Boolean = False)

        If m_FieldList = FieldList Then Exit Sub
        m_FieldInfo = New ArrayList()
        m_FieldList = FieldList
        Dim Field As FieldInfo, FieldParts() As String, Fields() As String = FieldList.Split(",")
        Dim I As Integer
        For I = 0 To Fields.Length - 1
            Field = New FieldInfo()
            '
            ' Parse FieldAlias
            '
            FieldParts = Fields(I).Trim().Split("=")
            Select Case FieldParts.Length
                Case 1
                    ' To be set at the end of the loop
                Case 2
                    Field.FieldAlias = FieldParts(1)
                Case Else
                    Throw New ArgumentException("Too many spaces in field definition: '" & Fields(I) & "'.")
            End Select

            'Table aliasing is not supported in this version 
            Field.FieldName = FieldParts(0)
            '
            ' Parse FieldName and RelationName
            '
            ' No plans to have relation specifiers as part of field specification as of now. 
            'FieldParts = FieldParts(0).Split(".")
            'Select Case FieldParts.Length
            '    Case 1
            '        Field.FieldName = FieldParts(0)
            '    Case 2
            '        If Not AllowRelation Then _
            '            Throw New ArgumentException("Relation specifiers not allowed in field list: '" & Fields(I) & "'.")
            '        Field.RelationName = FieldParts(0).Trim()
            '        Field.FieldName = FieldParts(1).Trim()
            '    Case Else
            '        Throw New ArgumentException("Invalid field definition: '" & Fields(I) & "'.")
            'End Select
            If Field.FieldAlias = "" Then Field.FieldAlias = Field.FieldName
            m_FieldInfo.Add(Field)
        Next
    End Sub

    '
    ' Parses FieldList into FieldInfo objects and then adds them to the GroupByFieldInfo private member
    '
    ' FieldList syntax: fieldname=[alias]|operatorname(fieldname)=[alias],...
    '
    ' Supported Operators: count,sum,max,min,first,last
    '
    Private Sub ParseGroupByFieldList(ByVal FieldList As String)

        If GroupByFieldList = FieldList Then Exit Sub
        'Const OperatorList As String = ",count,sum,max,min,first,last,"
        GroupByFieldInfo = New ArrayList()
        Dim Field As FieldInfo, FieldParts() As String, Fields() As String = FieldList.Split(",")
        Dim I As Integer
        For I = 0 To Fields.Length - 1
            Field = New FieldInfo()
            '
            ' Parse FieldAlias
            '
            FieldParts = Fields(I).Trim().Split("=")
            Select Case FieldParts.Length
                Case 1
                    ' To be set at the end of the loop
                Case 2
                    Field.FieldAlias = FieldParts(1)
                Case Else
                    Throw New ArgumentException("Too many => in field definition: '" & Fields(I) & "'.")
            End Select
            '
            ' Parse FieldName and Aggregate
            '
            FieldParts = FieldParts(0).Split("{")
            Select Case FieldParts.Length
                Case 1
                    Field.FieldName = FieldParts(0)
                Case 2
                    Field.Aggregate = FieldParts(0).Trim().ToLower() ' You will do a case-sensitive comparison later.
                    Field.FieldName = FieldParts(1).Trim(" "c, "}"c)
                Case Else
                    Throw New ArgumentException("Invalid field definition: '" & Fields(I) & "'.")
            End Select
            If Field.FieldAlias = "" Then
                If Field.Aggregate = "" Then
                    Field.FieldAlias = Field.FieldName
                Else
                    Field.FieldAlias = Field.Aggregate & "Of" & Field.FieldName
                End If
            End If
            GroupByFieldInfo.Add(Field)
        Next
        GroupByFieldList = FieldList
    End Sub


    Public Function CreateGroupByTable(ByVal TableName As String, _
                                   ByVal SourceTable As DataTable, _
                                   ByVal FieldList As String) As DataTable
        '
        ' Creates a table based on aggregates of fields of another table
        '
        ' RowFilter affects rows before the GroupBy operation. No HAVING-type support
        ' although this can be emulated by later filtering of the resultant table.
        '
        ' FieldList syntax: fieldname[ alias]|aggregatefunction(fieldname)[ alias], ...
        '
        If FieldList = "" Then
            Throw New ArgumentException("You must specify at least one field in the field list.")
            ' Return CreateTable(TableName, SourceTable)
        Else
            Dim dt As New DataTable(TableName)
            ParseGroupByFieldList(FieldList)
            Dim Field As FieldInfo, dc As DataColumn
            For Each Field In GroupByFieldInfo
                dc = SourceTable.Columns(Field.FieldName)
                If Field.Aggregate = "" Then
                    dt.Columns.Add(Field.FieldAlias, dc.DataType, dc.Expression)
                Else
                    dt.Columns.Add(Field.FieldAlias, dc.DataType)
                End If
            Next
            If Not ds Is Nothing Then
                ds.Tables.Clear()
                ds.Tables.Add(dt)
            End If

            Return dt
        End If
    End Function

    '
    ' Copies the selected rows and columns from SourceTable and inserts them into DestTable
    ' FieldList has same format as CreateGroupByTable
    '
    Public Sub InsertGroupByInto(ByVal DestTable As DataTable, _
                             ByVal SourceTable As DataTable, _
                             ByVal FieldList As String, _
                             Optional ByVal RowFilter As String = "", _
                             Optional ByVal GroupBy As String = "", _
                             Optional ByVal Rollup As Boolean = False)

        ParseGroupByFieldList(FieldList)  ' parse field list
        ParseFieldList(GroupBy)           ' parse field names to Group By into an arraylist
        Dim Field As FieldInfo
        Dim Rows() As DataRow = SourceTable.Select(RowFilter, GroupBy)
        Dim SourceRow, LastSourceRow As DataRow, SameRow As Boolean, I As Integer, J As Integer, K As Integer
        Dim DestRows(m_FieldInfo.Count) As DataRow, RowCount(m_FieldInfo.Count) As Integer
        LastSourceRow = Nothing
        '
        ' Initialize Grand total row
        '
        DestRows(0) = DestTable.NewRow()
        '
        ' Process source table rows
        '

        For Each SourceRow In Rows
            '
            ' Determine whether we've hit a control break
            '
            SameRow = False
            If Not (LastSourceRow Is Nothing) Then
                SameRow = True
                For I = 0 To m_FieldInfo.Count - 1 ' fields to Group By
                    Field = m_FieldInfo(I)
                    If ColumnEqual(LastSourceRow(Field.FieldName), SourceRow(Field.FieldName)) = False Then
                        SameRow = False
                        Exit For
                    End If
                Next I
                '
                ' Add previous totals to the destination table
                '
                If Not SameRow Then
                    For J = m_FieldInfo.Count To I + 1 Step -1
                        '
                        ' Make NULL the key values for levels that have been rolled up
                        '
                        For K = m_FieldInfo.Count - 1 To J Step -1
                            Field = LocateFieldInfoByName(GroupByFieldInfo, m_FieldInfo(K).FieldName)
                            If Not (Field Is Nothing) Then   ' Group By field does not have to be in field list
                                DestRows(J)(Field.FieldAlias) = DBNull.Value
                            End If
                        Next K
                        '
                        ' Make NULL any non-aggregate, non-group-by fields in anything other than
                        ' the lowest level.
                        '
                        If J <> m_FieldInfo.Count Then
                            For Each Field In GroupByFieldInfo
                                If Field.Aggregate <> "" Then Exit For
                                If LocateFieldInfoByName(m_FieldInfo, Field.FieldName) Is Nothing Then
                                    DestRows(J)(Field.FieldAlias) = DBNull.Value
                                End If
                            Next
                        End If
                        '
                        ' Add row
                        '
                        DestTable.Rows.Add(DestRows(J))
                        If Rollup = False Then Exit For ' only add most child row if not doing a roll-up
                    Next J
                End If
            End If

            '
            ' create new destination rows
            ' Value of I comes from previous If block
            '
            If Not SameRow Then
                For J = m_FieldInfo.Count To I + 1 Step -1
                    DestRows(J) = DestTable.NewRow()
                    RowCount(J) = 0
                    'Did it on purpose to avoid new rows from being created for each row of sub total 
                    'If Rollup = False Then Exit For 
                Next J
            End If
            For J = 0 To m_FieldInfo.Count
                RowCount(J) += 1
                For Each Field In GroupByFieldInfo
                    Select Case Field.Aggregate  ' this test is case-sensitive - made lower-case by Build_GroupByFiledInfo
                        Case ""    ' implicit Last
                            DestRows(J)(Field.FieldAlias) = SourceRow(Field.FieldName)
                        Case "last"
                            DestRows(J)(Field.FieldAlias) = SourceRow(Field.FieldName)
                        Case "first"
                            If RowCount(J) = 1 Then DestRows(J)(Field.FieldAlias) = SourceRow(Field.FieldName)
                        Case "count"
                            DestRows(J)(Field.FieldAlias) = RowCount(J)
                        Case "sum"
                            DestRows(J)(Field.FieldAlias) = Add(DestRows(J)(Field.FieldAlias), SourceRow(Field.FieldName))
                        Case "concat"
                            If DestRows(J)(Field.FieldAlias).ToString() = String.Empty Then
                                DestRows(J)(Field.FieldAlias) = SourceRow(Field.FieldName).ToString()
                            Else
                                DestRows(J)(Field.FieldAlias) = DestRows(J)(Field.FieldAlias).ToString() & "," & SourceRow(Field.FieldName).ToString()
                            End If

                    End Select
                Next
            Next J

            LastSourceRow = SourceRow

        Next

        If Rows.Length > 0 Then

            '
            ' Make NULL the key values for levels that have been rolled up
            '
            For J = m_FieldInfo.Count To 0 Step -1
                For K = m_FieldInfo.Count - 1 To J Step -1
                    Field = LocateFieldInfoByName(GroupByFieldInfo, m_FieldInfo(K).FieldName)
                    If Not (Field Is Nothing) Then  ' Group By field does not have to be in field list
                        DestRows(J)(Field.FieldAlias) = DBNull.Value
                    End If
                Next K

                '
                ' Make NULL any non-aggregate, non-group-by fields in anything other than
                ' the lowest level.
                '
                If J <> m_FieldInfo.Count Then
                    For Each Field In GroupByFieldInfo
                        If Field.Aggregate <> "" Then Exit For
                        If LocateFieldInfoByName(m_FieldInfo, Field.FieldName) Is Nothing Then
                            DestRows(J)(Field.FieldAlias) = DBNull.Value
                        End If
                    Next
                End If

                '
                ' Add row
                '
                DestTable.Rows.Add(DestRows(J))

                If Rollup = False Then Exit For

            Next J

        End If

    End Sub

    '
    ' Looks up a FieldInfo record based on FieldName
    '
    Private Function LocateFieldInfoByName(ByVal FieldList As ArrayList, ByVal Name As String) As FieldInfo

        Dim Field As FieldInfo

        For Each Field In FieldList
            If Field.FieldName = Name Then Return Field
        Next

        Return Nothing

    End Function

    '
    ' Compares two values to determine if they are equal. Also compares DBNULL.Value.
    '
    Private Function ColumnEqual(ByVal A As Object, ByVal B As Object) As Boolean
        If IsNothingOrEmptyString(A) AndAlso IsNothingOrEmptyString(B) Then Return True
        If A Is DBNull.Value And B Is DBNull.Value Then Return True ' Both are DBNull.Value.
        If A Is DBNull.Value Or B Is DBNull.Value Then Return False ' Only one is DbNull.Value.

        Return A.ToString().Equals(B.ToString(), StringComparison.InvariantCultureIgnoreCase)                                                ' Value type standard comparison

    End Function

    '
    ' Adds two values. If one is DBNull, returns the other.
    '
    Private Function Add(ByVal A As Object, ByVal B As Object) As Object

        If A Is DBNull.Value OrElse A = "" Then Return B
        If B Is DBNull.Value OrElse B = "" Then Return A

        Return Convert.ToDecimal(A) + Convert.ToDecimal(B)

    End Function

    ''' <summary>
    ''' Selects data from one DataTable to another and performs various aggregate functions along the way
    ''' </summary>
    ''' <param name="TableName">Output Table Name</param>
    ''' <param name="SourceTable">Source Table</param>
    ''' <param name="FieldList">List of Table Fields</param>
    ''' <param name="RowFilter">Row Filter Text</param>
    ''' <param name="GroupBy">Listing of Group by Clauses</param>
    ''' <param name="Rollup">Not Used</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectGroupByInto(ByVal TableName As String, _
                                  ByVal SourceTable As DataTable, _
                                  ByVal FieldList As String, _
                                  Optional ByVal RowFilter As String = "", _
                                  Optional ByVal GroupBy As String = "", _
                                  Optional ByVal Rollup As Boolean = False) As DataTable

        Dim dt As DataTable = CreateGroupByTable(TableName, SourceTable, FieldList)

        InsertGroupByInto(dt, SourceTable, FieldList, RowFilter, GroupBy, Rollup)

        Return dt

    End Function


    ''' <summary>
    ''' Selects data from one DataTable to another and performs various aggregate functions along the way
    ''' </summary>
    ''' <param name="TableName">Output Table Name</param>
    ''' <param name="SourceTable">Source Table</param>
    ''' <param name="FieldList">List of Table Fields</param>
    ''' <param name="RowFilter">Row Filter Text</param>
    ''' <param name="GroupBy">Listing of Group by Clauses</param>
    ''' <param name="Rollup">Indicates Rollup operation </param>
    ''' <param name="RollupField">Column Name to rollup</param>
    ''' <param name="RollupValue">Rollup Value</param>
    ''' <returns>Data Table</returns>
    ''' <remarks></remarks>
    Public Function SelectGroupByInto2(ByVal TableName As String, _
                                  ByVal SourceTable As DataTable, _
                                  ByVal FieldList As String, _
                                  Optional ByVal RowFilter As String = "", _
                                  Optional ByVal GroupBy As String = "", _
                                  Optional ByVal Rollup As Boolean = False, _
                                  Optional ByVal RollupField As String = "", _
                                  Optional ByRef RollupValue As Decimal = 0) As DataTable

        Dim dt As DataTable = CreateGroupByTable(TableName, SourceTable, FieldList)

        InsertGroupByInto(dt, SourceTable, FieldList, RowFilter, GroupBy, False)

        Dim dv As DataView = dt.DefaultView
        Try
            dv.Sort = GroupBy
        Catch ex As Exception
        End Try
        dt = dv.ToTable(dt.TableName)


        'If there is no Rollup argument, Column "S.No." will not be added to table and Rollup value will not be calculated
        If RollupField <> String.Empty Then

            'Auto number column "S.No." is added when rollupfield argument is given
            Dim newdt As New DataTable(dt.TableName)
            Dim AutoNumberColumn As New DataColumn()
            AutoNumberColumn.ColumnName = "S.No."
            AutoNumberColumn.DataType = GetType(Integer)
            AutoNumberColumn.AutoIncrement = True
            AutoNumberColumn.AutoIncrementSeed = 1
            AutoNumberColumn.AutoIncrementStep = 1
            newdt.Columns.Add(AutoNumberColumn)
            newdt.Merge(dt)

            For Each row As DataRow In newdt.Rows
                RollupValue += Convert.ToDecimal(row(RollupField))
            Next
            dt = newdt

        End If

        Return dt

    End Function

    ''' <summary>
    ''' Selects data from one DataTable to another and performs various aggregate functions along the way
    ''' </summary>
    ''' <param name="TableName">Output Table Name</param>
    ''' <param name="SourceTable">Source Table</param>
    ''' <param name="GroupByFields">List of Fields to Group By the Source Table Records</param>
    ''' <param name="ReferenceFields">List of Fields to Concatenate values of Records in a Group</param>
    ''' <returns>Data Table</returns>
    ''' <remarks></remarks>
    Public Function SelectGroupByInto3(ByVal TableName As String _
                                        , ByVal SourceTable As DataTable _
                                        , ByRef GroupByFields As List(Of String) _
                                        , ByRef ReferenceFields As List(Of String) _
                                        , ByRef AppendToReferenceFields As List(Of String) _
                                        , ByVal AmountField As String _
                                        ) As DataTable

        Dim FieldList As String = ""
        Dim RowFilter As String = "" 'As of now, no intention to pass RowFilter
        Dim GroupBy As String = ""

        '1. Prepare Select Field List
        Dim column As String = ""
        For Each dcolumn As DataColumn In SourceTable.Columns
            column = dcolumn.ColumnName
            If IsReferenceField(column, ReferenceFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf IsGroupByField(column, GroupByFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & column
            ElseIf column.Equals("AutoCheque Advice Information", StringComparison.InvariantCultureIgnoreCase) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf column.Equals("Message to Bank", StringComparison.InvariantCultureIgnoreCase) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf column.Equals("Message to Beneficiary", StringComparison.InvariantCultureIgnoreCase) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf column.Equals(AmountField, StringComparison.InvariantCultureIgnoreCase) Then 'IsAmountField(column, AmountField) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "sum{" & column & "}=" & column
            Else
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "first{" & column & "}=" & column
            End If
        Next

        '2. Prepare Group By Field List
        For Each _field As String In GroupByFields
            GroupBy = GroupBy & IIf(GroupBy = "", "", ", ") & _field
        Next

        '3. Group Records
        Dim dt As DataTable = CreateGroupByTable(TableName, SourceTable, FieldList)
        InsertGroupByInto(dt, SourceTable, FieldList, RowFilter, GroupBy, False)

        '4. Prepend Reference Prefix to Reference Field Columns
        For idx As Integer = 0 To ReferenceFields.Count - 1

            If idx >= AppendToReferenceFields.Count Then Continue For
            If AppendToReferenceFields(idx) = String.Empty Then Continue For

            For Each row As DataRow In dt.Rows
                row(ReferenceFields(idx)) = AppendToReferenceFields(idx) & row(ReferenceFields(idx)).ToString()
            Next

        Next

        Return dt

    End Function
    ''' <summary>
    ''' This function is used only in iRTMS format when the option "Sum up Remittance Amount" is unchecked.
    ''' The "Amount" value for each consolidated record is the amount of the last transaction record in each group.
    ''' </summary>
    ''' <param name="TableName">Output Table Name</param>
    ''' <param name="SourceTable">Source Table Name</param>
    ''' <param name="GroupByFields">List of fields to Group By the Source Table records</param>
    ''' <param name="ReferenceFields">List of fields to concatenate values in each group</param>
    ''' <param name="AppendToReferenceFields">List of values to append to the concatenated values in each group</param>
    ''' <param name="AmountField">The Amount Field Name used in the format to consolidate</param>
    ''' <returns>Data Table</returns>
    ''' <remarks></remarks>
    Public Function SelectGroupByInto4(ByVal TableName As String _
                                        , ByVal SourceTable As DataTable _
                                        , ByRef GroupByFields As List(Of String) _
                                        , ByRef ReferenceFields As List(Of String) _
                                        , ByRef AppendToReferenceFields As List(Of String) _
                                        , ByVal AmountField As String _
                                        ) As DataTable

        Dim FieldList As String = ""
        Dim RowFilter As String = "" 'As of now, no intention to pass RowFilter
        Dim GroupBy As String = ""

        '1. Prepare Select Field List
        Dim column As String = ""
        For Each dcolumn As DataColumn In SourceTable.Columns
            column = dcolumn.ColumnName
            If IsReferenceField(column, ReferenceFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf IsGroupByField(column, GroupByFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & column
                'change request by btmu-casey @ 27 Apr 2011 (magic version 3.14)
                'commented the below checking to take the amount value from first record.
                'ElseIf column.Equals(AmountField, StringComparison.InvariantCultureIgnoreCase) Then 'IsAmountField(column, AmountField) Then
                '    FieldList = FieldList & IIf(FieldList = "", "", ", ") & "last{" & column & "}=" & column
            Else
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "first{" & column & "}=" & column
            End If
        Next

        '2. Prepare Group By Field List
        For Each _field As String In GroupByFields
            GroupBy = GroupBy & IIf(GroupBy = "", "", ", ") & _field
        Next

        '3. Group Records
        Dim dt As DataTable = CreateGroupByTable(TableName, SourceTable, FieldList)
        InsertGroupByInto(dt, SourceTable, FieldList, RowFilter, GroupBy, False)

        '4. Prepend Reference Prefix to Reference Field Columns
        For idx As Integer = 0 To ReferenceFields.Count - 1

            If idx >= AppendToReferenceFields.Count Then Continue For
            If AppendToReferenceFields(idx) = String.Empty Then Continue For

            For Each row As DataRow In dt.Rows
                row(ReferenceFields(idx)) = AppendToReferenceFields(idx) & row(ReferenceFields(idx)).ToString()
            Next

        Next

        Return dt

    End Function


    ''' <summary>
    ''' Selects data from one DataTable to another and performs various aggregate functions along the way
    ''' </summary>
    ''' <param name="TableName">Output Table Name</param>
    ''' <param name="SourceTable">Source Table</param>
    ''' <param name="GroupByFields">List of Fields to Group By the Source Table Records</param>
    ''' <param name="ReferenceFields">List of Fields to Concatenate values of Records in a Group</param>
    ''' <returns>Data Table</returns>
    ''' <remarks>This function is used by iFTS-2 Multiline Format</remarks>
    Public Function SelectGroupByInto5(ByVal TableName As String _
                                        , ByVal SourceTable As DataTable _
                                        , ByRef GroupByFields As List(Of String) _
                                        , ByRef ReferenceFields As List(Of String) _
                                        , ByRef AppendToReferenceFields As List(Of String) _
                                        , ByVal AmountField As String _
                                        , ByVal isCheckedAmountSumUp As Boolean) As DataTable

        Dim FieldList As String = ""
        Dim RowFilter As String = "" 'As of now, no intention to pass RowFilter
        Dim GroupBy As String = ""

        '0. Prepare Consolidate field for second time onwards consoidate_clicks
        If SourceTable.Columns.Count = 14 Then
            SourceTable.Columns.Add("Consolidate Field")
        End If

        '1. Prepare Select Field List
        Dim column As String = ""
        For Each dcolumn As DataColumn In SourceTable.Columns
            column = dcolumn.ColumnName
            If IsReferenceField(column, ReferenceFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{" & column & "}=" & column
            ElseIf IsGroupByField(column, GroupByFields) Then
                FieldList = FieldList & IIf(FieldList = "", "", ", ") & column
                ' Added a check for isCheckedAmountSumUp or not due to change request by btmu-casey @ 27 Apr 2011
                ' Since Magic version 3.14, if isCheckedAmountSumUp is false, amount value from first record will be taken
            ElseIf column.Equals(AmountField, StringComparison.InvariantCultureIgnoreCase) Then 'IsAmountField(column, AmountField) Then
                If isCheckedAmountSumUp Then
                    FieldList = FieldList & IIf(FieldList = "", "", ", ") & "sum{" & column & "}=" & column
                Else
                    FieldList = FieldList & IIf(FieldList = "", "", ", ") & "first{" & column & "}=" & column
                End If

                Else
                    FieldList = FieldList & IIf(FieldList = "", "", ", ") & "first{" & column & "}=" & column
                End If
        Next

        'For iFTS-2 Format: To relate back all the WHTax and Invoice Records of Grouped Payment Records
        FieldList = FieldList & IIf(FieldList = "", "", ", ") & "concat{ID}=IDs"

        '2. Prepare Group By Field List
        For Each _field As String In GroupByFields
            GroupBy = GroupBy & IIf(GroupBy = "", "", ", ") & _field
        Next


        '3. Group Records
        Dim dt As DataTable = CreateGroupByTable(TableName, SourceTable, FieldList)
        InsertGroupByInto(dt, SourceTable, FieldList, RowFilter, GroupBy, False)

        '4. Prepend Reference Prefix to Reference Field Columns
        For idx As Integer = 0 To ReferenceFields.Count - 1

            If idx >= AppendToReferenceFields.Count Then Continue For
            If AppendToReferenceFields(idx) = String.Empty Then Continue For

            For Each row As DataRow In dt.Rows
                row(ReferenceFields(idx)) = AppendToReferenceFields(idx) & row(ReferenceFields(idx)).ToString()
            Next

        Next

        Return dt

    End Function



    Private Function IsGroupByField(ByVal afield As String, ByRef GroupByFields As List(Of String)) As Boolean

        For Each gfield As String In GroupByFields

            If afield.Equals(gfield, StringComparison.InvariantCultureIgnoreCase) Then Return True

        Next

        Return False

    End Function

    Private Function IsReferenceField(ByVal rfield As String, ByRef ReferenceFields As List(Of String)) As Boolean


        For Each gfield As String In ReferenceFields

            If rfield.Equals(gfield, StringComparison.InvariantCultureIgnoreCase) Then Return True

        Next

        Return False

    End Function

End Class
