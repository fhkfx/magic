Imports System.IO
Imports System.Xml
Imports System.ComponentModel
Imports System.Configuration.Install

Public Class GenerateLicenseFileInstaller
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Common.Resources", GetType(BTMUExceptionManager).Assembly)
    Private Const _fileName As String = "magic.lic"
    Private Shared _path As String

    '##############################################################################################
    'Please set these values before creating the Installer.
#Const IncludeGnuPG = True ' Set to True for Australia, else set as False
#Const IncludeGnuPGForIndia = True ' Set to True for India, else set as False

    'Private IncludeGnuPG As Boolean
    'Private IncludeGnuPGForIndia As Boolean
    '##############################################################################################

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent

    End Sub

    Public Overrides Sub Install(ByVal stateSaver As System.Collections.IDictionary)
        MyBase.Install(stateSaver)
        stateSaver.Add("TargetDir", Context.Parameters("DP_TargetDir").ToString())
        'Used for GnuPG Encrytpion methods for Australia and India
        'stateSaver.Add("AUGnuPG", Context.Parameters("CHECKBOXA1").ToString())
        'stateSaver.Add("INGnuPG", Context.Parameters("CHECKBOXA2").ToString())

        Dim path As String = stateSaver("TargetDir")
        Dim fileName As String = path + _fileName
        _path = path

        GenerateLicense(fileName)

    End Sub

    Protected Overrides Sub OnAfterInstall(ByVal savedState As System.Collections.IDictionary)

        MyBase.OnAfterInstall(savedState)

        Dim path As String = Context.Parameters("DP_TargetDir").ToString()

        UpdateConfigFile(path)
        CreateNecessaryFolder(path)
        CreateNecessaryFile(path)

        CopyHelpFile(Context.Parameters("InstallPath").ToString(), path)

        '#If IncludeGnuPGForIndia Then
        '        ImportGnu147PubKey(path)
        '#End If

    End Sub

    Public Overrides Sub Uninstall(ByVal savedState As System.Collections.IDictionary)
        MyBase.Uninstall(savedState)
        Dim path As String = Context.Parameters("DP_TargetDir").ToString()


        Dim fileName As String = path + _fileName
        If File.Exists(fileName) Then
            Try
                File.Delete(fileName)
            Catch ex As IOException
                File.SetAttributes(fileName, FileAttributes.Normal)
                File.Delete(fileName)
            End Try
        End If

        Dim helpFile As String = System.IO.Path.Combine(path, "Help\BTMU-MAGIC-HK-Help.chm")

        If File.Exists(helpFile) Then
            Try
                File.Delete(helpFile)
            Catch ex As Exception
                File.SetAttributes(helpFile, FileAttributes.Normal)
                File.Delete(helpFile)
            End Try

        End If



    End Sub

    Private Sub CopyHelpFile(ByVal path As String, ByVal tPath As String)

        If Not Directory.Exists(System.IO.Path.Combine(tPath, "Help")) Then
            Directory.CreateDirectory(System.IO.Path.Combine(tPath, "Help"))
        End If

        If File.Exists(System.IO.Path.Combine(tPath, "Help\BTMU-MAGIC-HK-Help.chm")) Then
            File.SetAttributes(System.IO.Path.Combine(tPath, "Help\BTMU-MAGIC-HK-Help.chm"), FileAttributes.Normal)
            File.Delete(System.IO.Path.Combine(tPath, "Help\BTMU-MAGIC-HK-Help.chm"))
        End If

        File.Copy(System.IO.Path.Combine(path, "Help\BTMU-MAGIC-HK-Help.chm"), System.IO.Path.Combine(tPath, "Help\BTMU-MAGIC-HK-Help.chm"), True)

        


    End Sub

    Private Sub CreateNecessaryFolder(ByVal path As String)

        If Not Directory.Exists(path + "Common") Then
            Directory.CreateDirectory(path + "Common")
        End If

        If Not Directory.Exists(path + "Common\Draft") Then
            Directory.CreateDirectory(path + "Common\Draft")
        End If

        If Not Directory.Exists(path + "Common Excel") Then
            Directory.CreateDirectory(path + "Common Excel")
        End If

        If Not Directory.Exists(path + "Common Excel\Draft") Then
            Directory.CreateDirectory(path + "Common Excel\Draft")
        End If

        If Not Directory.Exists(path + "Default") Then
            Directory.CreateDirectory(path + "Default")
        End If

        If Not Directory.Exists(path + "GCMS-MT") Then
            Directory.CreateDirectory(path + "GCMS-MT")
        End If

        If Not Directory.Exists(path + "GCMS-MT\Archive") Then
            Directory.CreateDirectory(path + "GCMS-MT\Archive")
        End If

        If Not Directory.Exists(path + "GCMS-MT\Master") Then
            Directory.CreateDirectory(path + "GCMS-MT\Master")
        End If

        If Not Directory.Exists(path + "GCMS-MT\Save") Then
            Directory.CreateDirectory(path + "GCMS-MT\Save")
        End If

        If Not Directory.Exists(path + "GCMS-MT\Upload") Then
            Directory.CreateDirectory(path + "GCMS-MT\Upload")
        End If

        If Not Directory.Exists(path + "Master") Then
            Directory.CreateDirectory(path + "Master")
        End If

        If Not Directory.Exists(path + "Master\Draft") Then
            Directory.CreateDirectory(path + "Master\Draft")
        End If

        If Not Directory.Exists(path + "Sample Source") Then
            Directory.CreateDirectory(path + "Sample Source")
        End If

        If Not Directory.Exists(path + "Intermediary") Then
            Directory.CreateDirectory(path + "Intermediary")
        End If
        If Not Directory.Exists(path + "SWIFT") Then
            Directory.CreateDirectory(path + "SWIFT")
        End If

        If Not Directory.Exists(path + "SWIFT\Draft") Then
            Directory.CreateDirectory(path + "SWIFT\Draft")
        End If

        If Not Directory.Exists(path + "Definition") Then
            Directory.CreateDirectory(path + "Definition")
        End If

        If Not Directory.Exists(path + "Converted Common") Then
            Directory.CreateDirectory(path + "Converted Common")
        End If

        If Not Directory.Exists(path + "Converted SWIFT") Then
            Directory.CreateDirectory(path + "Converted SWIFT")
        End If

        If Not Directory.Exists(path + "MagicDB") Then
            Directory.CreateDirectory(path + "MagicDB")
        End If

        'If IncludeGnuPG Then
        '        'if CHECKBOXA1.checked then
        '        If Not Directory.Exists(path + "GnuPGHome") Then
        '            Directory.CreateDirectory(path + "GnuPGHome")
        '        End If
        'End If
        'for AU
        'If Not Directory.Exists(path + "GnuPGHome") Then
        '    Directory.CreateDirectory(path + "GnuPGHome")
        'End If
        '#If IncludeGnuPGForIndia Then
        '        If Not Directory.Exists(path + "GnuPG147") Then
        '            Directory.CreateDirectory(path + "GnuPG147")
        '        End If
        '#End If
        ''for IN
        'If Not Directory.Exists(path + "GnuPG147") Then
        '    Directory.CreateDirectory(path + "GnuPG147")
        'End If

    End Sub

    Private Sub UpdateConfigFile(ByVal path As String)
        Dim configFile As String = path & "BTMU-MAGIC-HK.exe.config"
        Dim configDoc As New XmlDocument()
        configDoc.Load(configFile)

        UpdateSetting(path, "CommonTemplateExcelDraftFolder", configDoc, "Common Excel\Draft")
        UpdateSetting(path, "CommonTemplateExcelFolder", configDoc, "Common Excel")

        UpdateSetting(path, "CommonTemplateTextDraftFolder", configDoc, "Common\Draft")
        UpdateSetting(path, "CommonTemplateTextFolder", configDoc, "Common")

        UpdateSetting(path, "SwiftDraftFolder", configDoc, "Swift\Draft")
        UpdateSetting(path, "SwiftFolder", configDoc, "Swift")

        UpdateSetting(path, "MasterTemplateViewDraftFolder", configDoc, "Master\Draft")
        UpdateSetting(path, "MasterTemplateViewFolder", configDoc, "Master")

        UpdateSetting(path, "ConvertCommonFolder", configDoc, "Converted Common")
        UpdateSetting(path, "ConvertSWIFTFolder", configDoc, "Converted Swift")

        UpdateSetting(path, "SampleSourceFolder", configDoc, "Sample Source")

        UpdateSetting(path, "IntermediateFile", configDoc, "Intermediary\MDTOutput.txt")

        UpdateSetting(path, "DefinitionFileFolder", configDoc, "Definition")
        UpdateSetting(path, "MagicDB", configDoc, "MagicDB")

        '#If IncludeGnuPG Then
        '        UpdateSetting(path, "GnuPGHome", configDoc, "GnuPGHome")
        '#End If

        '#If IncludeGnuPGForIndia Then
        'UpdateSetting(path, "Gnu147", configDoc, "GnuPG147\gpg.exe")
        'UpdateSetting(path, "Gnu147PubKey", configDoc, "GnuPG147\OmakaseIndia_pub.asc")
        '#End If

        UpdateSetting(path, "GnuPGHome", configDoc, "GnuPGHome")
        UpdateSetting(path, "Gnu147", configDoc, "GnuPG147\gpg.exe")
        UpdateSetting(path, "Gnu147PubKey", configDoc, "GnuPG147\OmakaseIndia_pub.asc")

        configDoc.Save(configFile)
    End Sub


    Private Sub CreateNecessaryFile(ByVal path As String)

        Dim offlineINI As String = path + "Default\Offline.ini"
        Dim pAdm As String = path + "Default\pAdm.dat"
        Dim pCommon As String = path + "Default\pCommon.dat"
        Dim pSuper As String = path + "Default\pSuper.dat"
        'Dim pCommon2 As String = path + "Default\pCommon2.dat"
        Dim magicXML As String = path + "magic.xml"


        If Not File.Exists(offlineINI) Then
            File.WriteAllText(offlineINI, MsgReader.GetString("OfflineIni"))
        End If

        If Not File.Exists(pAdm) Then
            File.WriteAllText(pAdm, MsgReader.GetString("DefaultPassword"))
            'Changed for BTMU's request to use different password '3631'
            'File.WriteAllText(pAdm, MsgReader.GetString("altAdminPassword"))
        End If

        If Not File.Exists(pCommon) Then
            File.WriteAllText(pCommon, MsgReader.GetString("DefaultPassword"))
        End If

        'added for common2
        'If Not File.Exists(pCommon2) Then
        '    File.WriteAllText(pCommon2, MsgReader.GetString("DefaultPassword"))
        'End If

        If Not File.Exists(pSuper) Then
            File.WriteAllText(pSuper, MsgReader.GetString("DefaultPassword"))
        End If

        If Not File.Exists(magicXML) Then
            File.WriteAllText(magicXML, MsgReader.GetString("magicXML"))
        End If
    End Sub

    Private Sub UpdateSetting(ByVal AppPath As String, ByRef propertyName As String, ByRef configDoc As XmlDocument, ByVal newFolder As String)
        Dim result As XmlNode = configDoc.SelectSingleNode("configuration/applicationSettings/BTMU.Magic.UI.My.MySettings/setting[@name='" & propertyName & "']")
        Dim value As XmlNode = result.SelectSingleNode("value")
        value.InnerText = Path.Combine(AppPath, newFolder)
    End Sub


    Private Sub GenerateLicenseFileInstaller_Committed(ByVal sender As Object, ByVal e As System.Configuration.Install.InstallEventArgs) Handles Me.Committed

        Dim filename As String = System.IO.Path.Combine(Context.Parameters("InstallPath").ToString(), "..\SQL Server Compact Edition\SQLServerCE31-EN.msi")
        Dim installerfile As New System.IO.FileInfo(filename)
        If installerfile.Exists Then
            Dim process As New System.Diagnostics.Process()
            process.Start(installerfile.FullName)
        Else
            MsgBox("Microsoft SQL Server Compact Edition is missing!, Install it manually", MsgBoxStyle.Exclamation, "SQLCE Not Found!")
        End If


    End Sub


    Private Sub ImportGnu147PubKey(ByVal path As String)

        Dim configFile As String = path & "BTMU-MAGIC-HK.exe.config"
        Dim configDoc As New XmlDocument()

        configDoc.Load(configFile)

        'code to import public key, if exists
        Dim result As XmlNode = configDoc.SelectSingleNode("configuration/applicationSettings/BTMU.Magic.UI.My.MySettings/setting[@name='Gnu147PubKey']")

        Dim value As XmlNode = result.SelectSingleNode("value")

        Dim pubkey As String = value.InnerText

        result = configDoc.SelectSingleNode("configuration/applicationSettings/BTMU.Magic.UI.My.MySettings/setting[@name='Gnu147']")

        value = result.SelectSingleNode("value")

        Dim gnu As String = value.InnerText

        If File.Exists(pubkey) AndAlso File.Exists(gnu) Then
            Dim proc As Process
            proc = Process.Start(gnu, "--import """ & pubkey & """")
            proc.WaitForExit() ' Waiting for exit. 
            If proc.ExitCode <> 0 Then Throw New Exception("Failed to import public key file!")
            proc.Close()
            proc.Dispose()

            ''Edit key file and adjust trust level
            'proc = Process.Start(gnu, "--edit-key OmakaseIndia", )

            'proc.WaitForExit() ' Waiting for exit. 
            'If proc.ExitCode <> 0 Then Throw New Exception("Failed to adjust the Trust Level of public key file!")
            'proc.Close()
            'proc.Dispose()
        End If

    End Sub
     
End Class
