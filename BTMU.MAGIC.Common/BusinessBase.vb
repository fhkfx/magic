Imports System.IO
Imports System.Reflection
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.Xml
Imports System.Xml.Serialization

''' <summary>
''' This class is the base of all bindable object.
''' </summary>
<Serializable()> _
Public MustInherit Class BusinessBase
    Inherits BaseClass
    Implements System.ComponentModel.INotifyPropertyChanged
    Implements System.ComponentModel.IDataErrorInfo
    Implements System.ComponentModel.IEditableObject
    Implements System.Xml.Serialization.IXmlSerializable


    Private _bindingEdit As Boolean
    Private _sequence As Int32
    Private _myCollection As Object

    ''' <summary>
    ''' Creates an instance of the BusinessBase.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub New()
    End Sub

    Public ReadOnly Property Sequence() As Int32
        Get
            Return _sequence
        End Get
    End Property

    Public Sub setSequence(ByVal seq As Int32)
        _sequence = seq
    End Sub

    Public Sub EnableValidationEngine()
        _validationEngine.EnableValidationEngine = True
    End Sub

    Public Sub DisableValidationEngine()
        _validationEngine.EnableValidationEngine = False
    End Sub

    Public ReadOnly Property MyCollection() As Object
        Get
            Return _myCollection
        End Get
    End Property

    Friend Sub SetCollection(ByVal myCollection As Object)
        _myCollection = myCollection
    End Sub


#Region " INotifyPropertyChanged "

    <NonSerialized()> _
    Private _NonSerializableHandlers As PropertyChangedEventHandler
    Private _SerializableHandlers As PropertyChangedEventHandler

    ''' <summary>
    ''' Implements a serialization-safe PropertyChanged event.
    ''' </summary>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler _
              Implements INotifyPropertyChanged.PropertyChanged
        AddHandler(ByVal value As PropertyChangedEventHandler)
            If value.Method.IsPublic AndAlso _
              (value.Method.DeclaringType.IsSerializable OrElse _
              value.Method.IsStatic) Then
                _SerializableHandlers = _
                  DirectCast(System.Delegate.Combine( _
                    _SerializableHandlers, value), PropertyChangedEventHandler)
            Else
                _NonSerializableHandlers = _
                  DirectCast(System.Delegate.Combine( _
                    _NonSerializableHandlers, value), PropertyChangedEventHandler)
            End If
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            If value.Method.IsPublic AndAlso _
              (value.Method.DeclaringType.IsSerializable OrElse _
              value.Method.IsStatic) Then
                _SerializableHandlers = DirectCast( _
                  System.Delegate.Remove( _
                    _SerializableHandlers, value), PropertyChangedEventHandler)
            Else
                _NonSerializableHandlers = DirectCast( _
                  System.Delegate.Remove( _
                    _NonSerializableHandlers, value), PropertyChangedEventHandler)
            End If
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            If _NonSerializableHandlers IsNot Nothing Then
                _NonSerializableHandlers.Invoke(sender, e)
            End If
            If _SerializableHandlers IsNot Nothing Then
                _SerializableHandlers.Invoke(sender, e)
            End If
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Call this method to raise the PropertyChanged event
    ''' for a specific property.
    ''' </summary>
    ''' <param name="propertyName">Name of the property that
    ''' has changed.</param>
    ''' <remarks>
    ''' This method may be called by properties in the business
    ''' class to indicate the change in a specific property.
    ''' </remarks>
    <EditorBrowsable(EditorBrowsableState.Advanced)> _
    Protected Overridable Sub OnPropertyChanged(ByVal propertyName As String)
        RaiseEvent PropertyChanged( _
          Me, New PropertyChangedEventArgs(propertyName))
        ValidationEngine.Validate(propertyName)

        _isDirty = True
    End Sub
#End Region

#Region " Initialize "

    ''' <summary>
    ''' Override this method to set up event handlers so user
    ''' code in a partial class can respond to events raised by
    ''' generated code.
    ''' </summary>
    Protected Overridable Sub Initialize()
        ' allows a generated class to set up events to be
        ' handled by a partial class containing user code
    End Sub

#End Region

#Region " IsNew, IsDeleted, IsDirty "

    ' keep track of whether we are new, deleted or dirty
    Private _isNew As Boolean = True
    Private _isDeleted As Boolean
    Protected _isDirty As Boolean = True

    ''' <summary>
    ''' Returns <see langword="true" /> if this is a new object, 
    ''' <see langword="false" /> if it is a pre-existing object.
    ''' </summary>
    ''' <remarks>
    ''' To indicate whether the object is new. 
    ''' </remarks>
    ''' <returns>A value indicating if this object is new.</returns>
    <Browsable(False)> _
    Public ReadOnly Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
    End Property

    ''' <summary>
    ''' Returns <see langword="true" /> if this object is marked for deletion.
    ''' </summary>
    ''' <remarks>
    ''' To indicate whether the object is deleted. 
    ''' </remarks>
    ''' <returns>A value indicating if this object is marked for deletion.</returns>
    <Browsable(False)> _
    Public ReadOnly Property IsDeleted() As Boolean
        Get
            Return _isDeleted
        End Get
    End Property

    ''' <summary>
    ''' Returns <see langword="true" /> if this object's data has been changed.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' To indicate whether the object is dirty. Newly created object is always dirty.
    ''' </para>
    ''' </remarks>
    ''' <returns>A value indicating if this object's data has been changed.</returns>
    <Browsable(False)> _
    Public Overridable ReadOnly Property IsDirty() As Boolean
        Get
            Return _isDirty
        End Get
    End Property

    ''' <summary>
    ''' Marks the object as being a new object. This also marks the object
    ''' as being dirty and ensures that it is not marked for deletion.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' Newly created objects are marked new by default. 
    ''' </para><para>
    ''' If you override this method, make sure to call the base
    ''' implementation after executing your new code.
    ''' </para>
    ''' </remarks>
    Protected Overridable Sub MarkNew()
        _isNew = True
        _isDeleted = False
        MarkDirty()
    End Sub

    ''' <summary>
    ''' Marks the object as being an old (not new) object. This also
    ''' marks the object as being unchanged (not dirty).
    ''' </summary>
    Protected Overridable Sub MarkOld()
        _isNew = False
        MarkClean()
    End Sub

    ''' <summary>
    ''' Marks an object for deletion. This also marks the object
    ''' as being dirty.
    ''' </summary>
    ''' <remarks>
    ''' You should call this method in your business logic in the
    ''' case that you want to have the object deleted when it is
    ''' saved to the database.
    ''' </remarks>
    Protected Sub MarkDeleted()
        _isDeleted = True
        MarkDirty()
    End Sub

    ''' <summary>
    ''' Marks an object as being dirty, or changed.
    ''' </summary>
    <EditorBrowsable(EditorBrowsableState.Advanced)> _
    Protected Sub MarkDirty()
        _isDirty = True
    End Sub

    ''' <summary>
    ''' Performs processing required when the current
    ''' property has changed.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' This method calls CheckRules(propertyName), MarkDirty and
    ''' OnPropertyChanged(propertyName). MarkDirty is called such
    ''' that no event is raised for IsDirty, so only the specific
    ''' property changed event for the current property is raised.
    ''' </para>
    ''' </remarks>
    Protected Sub PropertyHasChanged()

        Dim propertyName As String = _
          New System.Diagnostics.StackTrace(). _
          GetFrame(1).GetMethod.Name.Substring(4)
        PropertyHasChanged(propertyName)

    End Sub

    ''' <summary>
    ''' Performs processing required when a property
    ''' has changed.
    ''' </summary>
    ''' <param name="propertyName">Name of the property that
    ''' has changed.</param>
    ''' <remarks>
    ''' This method calls Validate(propertyName), MarkDirty and
    ''' OnPropertyChanged(propertyName). MarkDirty is called such
    ''' that no event is raised for IsDirty, so only the specific
    ''' property changed event for the current property is raised.
    ''' </remarks>
    Protected Overridable Sub PropertyHasChanged(ByVal propertyName As String)

        _validationEngine.Validate(propertyName)
        MarkDirty()
        OnPropertyChanged(propertyName)

    End Sub

    ''' <summary>
    ''' Forces the object's IsDirty flag to <see langword="false" />.
    ''' </summary>
    ''' <remarks>
    ''' This method is normally called automatically and is
    ''' not intended to be called manually.
    ''' </remarks>
    <EditorBrowsable(EditorBrowsableState.Advanced)> _
    Protected Sub MarkClean()

        _isDirty = False

    End Sub

    ''' <summary>
    ''' Returns <see langword="true" /> if this object is both dirty and valid.
    ''' </summary>
    ''' <remarks>
    ''' An object is considered dirty (changed) if 
    ''' <see cref="P:Csla.BusinessBase.IsDirty" /> returns <see langword="true" />. It is
    ''' considered valid if IsValid
    ''' returns <see langword="true" />. The IsSavable property is
    ''' a combination of these two properties. 
    ''' </remarks>
    ''' <returns>A value indicating if this object is both dirty and valid.</returns>
    <Browsable(False)> _
    Public Overridable ReadOnly Property IsSavable() As Boolean
        Get
            Return IsDirty AndAlso IsValid
        End Get
    End Property

#End Region

#Region " Parent/Child link "

    <NonSerialized()> _
    Private mParent As IParent

    ''' <summary>
    ''' Provide access to the parent reference for use
    ''' in child object code.
    ''' </summary>
    ''' <remarks>
    ''' This value will be Nothing for root objects.
    ''' </remarks>
    <EditorBrowsable(EditorBrowsableState.Advanced)> _
    Protected ReadOnly Property Parent() As IParent
        Get
            Return mParent
        End Get
    End Property

    ''' <summary>
    ''' Used by BusinessListBase as a child object is 
    ''' created to tell the child object about its
    ''' parent.
    ''' </summary>
    ''' <param name="parent">A reference to the parent collection object.</param>
    Friend Sub SetParent(ByVal parent As IParent)

        mParent = parent

    End Sub

#End Region

#Region " IEditableObject "



    ''' <summary>
    ''' Allow data binding to start a nested edit on the object.
    ''' </summary>
    ''' <remarks>
    ''' Data binding may call this method many times. Only the first
    ''' call should be honored, so we have extra code to detect this
    ''' and do nothing for subsquent calls.
    ''' </remarks>
    Private Sub IEditableObject_BeginEdit() _
      Implements System.ComponentModel.IEditableObject.BeginEdit

        If Not _bindingEdit Then
            _bindingEdit = True
            BeginEdit()
        End If

    End Sub

    ''' <summary>
    ''' Allow data binding to cancel the current edit.
    ''' </summary>
    ''' <remarks>
    ''' Data binding may call this method many times. Only the first
    ''' call to either IEditableObject.CancelEdit or 
    ''' IEditableObject.EndEdit
    ''' should be honored. We include extra code to detect this and do
    ''' nothing for subsequent calls.
    ''' </remarks>
    Private Sub IEditableObject_CancelEdit() _
      Implements System.ComponentModel.IEditableObject.CancelEdit

        If Not _bindingEdit Then
            CancelEdit()
            If IsNew Then
                ' we're new and no EndEdit or ApplyEdit has ever been
                ' called on us, and now we've been canceled back to 
                ' where we were added so we should have ourselves  
                ' removed from the parent collection
                If Not Parent Is Nothing Then
                    Parent.RemoveChild(Me)
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Allow data binding to apply the current edit.
    ''' </summary>
    ''' <remarks>
    ''' Data binding may call this method many times. Only the first
    ''' call to either IEditableObject.EndEdit or 
    ''' IEditableObject.CancelEdit
    ''' should be honored. We include extra code to detect this and do
    ''' nothing for subsequent calls.
    ''' </remarks>
    Private Sub IEditableObject_EndEdit() _
      Implements System.ComponentModel.IEditableObject.EndEdit

        If _bindingEdit Then
            ApplyEdit()
        End If

    End Sub

#End Region

#Region " Begin/Cancel/ApplyEdit "

    ''' <summary>
    ''' Override the method to implement begin edit method.
    ''' </summary>
    Public MustOverride Sub BeginEdit()



    ''' <summary>
    ''' Override the method to implement begin cancel edit method.
    ''' </summary>
    Public MustOverride Sub CancelEdit()

    ''' <summary>
    ''' Commits the current edit process.
    ''' </summary>
    Public MustOverride Sub ApplyEdit()

    ''' <summary>
    ''' Notifies the parent object (if any) that this
    ''' child object's edits have been accepted.
    ''' </summary>
    Protected Overridable Sub AcceptChangesComplete()

        If Parent IsNot Nothing Then
            Parent.ApplyEditChild(Me)
        End If

    End Sub

#End Region

#Region " IsChild "

    Private _isChild As Boolean

    ''' <summary>
    ''' Returns <see langword="true" /> if this is a child (non-root) object.
    ''' </summary>
    Protected Friend ReadOnly Property IsChild() As Boolean
        Get
            Return _isChild
        End Get
    End Property

    ''' <summary>
    ''' Marks the object as being a child object.
    ''' </summary>
    Protected Sub MarkAsChild()
        _isChild = True
    End Sub

#End Region

#Region " Delete "

    ''' <summary>
    ''' Marks the object for deletion. The object will be deleted as part of the
    ''' next save operation.
    ''' </summary>
    Public Overridable Sub Delete()
        If Me.IsChild Then
            Throw New NotSupportedException(My.Resources.E00000001)
        End If

        MarkDeleted()

    End Sub

    ''' <summary>
    ''' Called by a parent object to mark the child
    ''' for deferred deletion.
    ''' </summary>
    Friend Sub DeleteChild()
        If Not Me.IsChild Then
            Throw New NotSupportedException(My.Resources.E00000002)
        End If

        _bindingEdit = False
        MarkDeleted()

    End Sub

#End Region

#Region " ValidationRules, IsValid "

    <NonSerialized()> _
    Private _validationEngine As ValidationEngine

    ''' <summary>
    ''' Provides access to the validation engine functionality.
    ''' </summary>
    Protected ReadOnly Property ValidationEngine() As ValidationEngine
        Get
            If _validationEngine Is Nothing Then
                _validationEngine = New ValidationEngine(Me)
            End If
            Return _validationEngine
        End Get
    End Property

    ''' <summary>
    ''' Override this method in your business class to
    ''' be notified when you need to add
    ''' business rules.
    ''' </summary>
    Protected Overridable Sub AddBusinessRules()

    End Sub

    ''' <summary>
    ''' Returns <see langword="true" /> if the object is currently valid, <see langword="false" /> if the
    ''' object has broken rules or is otherwise invalid.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' By default this property relies on the underling ValidationRules
    ''' object to track whether any business rules are currently broken for this object.
    ''' </para><para>
    ''' You can override this property to provide more sophisticated
    ''' implementations of the behavior. For instance, you should always override
    ''' this method if your object has child objects, since the validity of this object
    ''' is affected by the validity of all child objects.
    ''' </para>
    ''' </remarks>
    ''' <returns>A value indicating if the object is currently valid.</returns>
    <Browsable(False)> _
    Public Overridable ReadOnly Property IsValid() As Boolean
        Get
            Return ValidationEngine.IsValid
        End Get
    End Property

    ''' <summary>
    ''' Validate all the property
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub Validate()
        ValidationEngine.Validate()
    End Sub

    ''' <summary>
    ''' Validate all the property
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub Validate(ByVal propertyName As String)
        ValidationEngine.Validate(propertyName)
    End Sub
#End Region

#Region " IDataErrorInfo "

    Private ReadOnly Property [Error]() As String _
      Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            If Not IsValid Then
                Return _validationEngine.RetrieveError()

            Else
                Return String.Empty
            End If
        End Get
    End Property

    Private ReadOnly Property Item(ByVal columnName As String) As String _
      Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            Return _validationEngine.RetrieveError(columnName)
        End Get
    End Property

#End Region

    Private Shared Function IsNonSerializedField(ByVal field As FieldInfo) As Boolean
        Return Attribute.IsDefined(field, GetType(NonSerializedAttribute))
    End Function

    Private Function GetFieldName(ByVal field As FieldInfo) As String
        Return field.DeclaringType.Name & "!" & field.Name
    End Function

    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function

    'Protected Overridable Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
    '    reader.ReadStartElement()
    '    _bindingEdit = Boolean.Parse(ReadXMLElement(reader, "_bindingEdit"))
    '    _isChild = Boolean.Parse(ReadXMLElement(reader, "_isChild"))
    '    _isDeleted = Boolean.Parse(ReadXMLElement(reader, "_isDeleted"))
    '    _isDirty = Boolean.Parse(ReadXMLElement(reader, "_isDirty"))
    '    _isNew = Boolean.Parse(ReadXMLElement(reader, "_isNew"))
    '    '_sequence = Boolean.Parse(ReadXMLElement(reader, "_sequence"))
    'End Sub

    'Protected Overridable Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
    '    WriteXmlElement(writer, "_bindingEdit", _bindingEdit)
    '    WriteXmlElement(writer, "_isChild", _isChild)
    '    WriteXmlElement(writer, "_isDeleted", _isDeleted)
    '    WriteXmlElement(writer, "_isDirty", _isDirty)
    '    WriteXmlElement(writer, "_isNew", _isNew)
    '    'WriteXmlElement(writer, "_sequence", _sequence)
    'End Sub

    'Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
    '    Try
    '        If value IsNot Nothing AndAlso value.GetType() Is GetType(String) AndAlso value.ToString().Trim().Length = 0 Then
    '            value = Nothing
    '        End If
    '        writer.WriteStartElement(elementName)
    '        writer.WriteString(value)
    '        writer.WriteEndElement()
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    'Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
    '    Dim returnValue As String
    '        reader.ReadStartElement(elementName)
    '        If (reader.HasValue) Then
    '            returnValue = reader.ReadContentAsString()
    '            reader.ReadEndElement()
    '            Return returnValue
    '        Else
    '            Return Nothing
    '        End If

    'End Function


    Protected Overridable Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        'Dim xmlContent As String = reader.ReadOuterXml()
        'Dim doc As XmlDocument = New XmlDocument()
        'doc.PreserveWhitespace = True
        'doc.LoadXml(xmlContent)

        While reader.NodeType = XmlNodeType.Whitespace
            reader.Skip()
        End While
        reader.ReadStartElement()

        _bindingEdit = Boolean.Parse(ReadXMLElement(reader, "_bindingEdit"))
        _isChild = Boolean.Parse(ReadXMLElement(reader, "_isChild"))
        _isDeleted = Boolean.Parse(ReadXMLElement(reader, "_isDeleted"))
        _isDirty = Boolean.Parse(ReadXMLElement(reader, "_isDirty"))
        _isNew = Boolean.Parse(ReadXMLElement(reader, "_isNew"))
        '_sequence = Boolean.Parse(ReadXMLElement(reader, "_sequence"))
    End Sub



    Protected Overridable Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        WriteXmlElement(writer, "_bindingEdit", _bindingEdit)
        WriteXmlElement(writer, "_isChild", _isChild)
        WriteXmlElement(writer, "_isDeleted", _isDeleted)
        WriteXmlElement(writer, "_isDirty", _isDirty)
        WriteXmlElement(writer, "_isNew", _isNew)
        'WriteXmlElement(writer, "_sequence", _sequence)
    End Sub

    Protected Sub WriteXmlElement(ByVal writer As System.Xml.XmlWriter, ByVal elementName As Object, ByVal value As Object)
        Try
            If value IsNot Nothing AndAlso value.GetType() Is GetType(String) AndAlso value.ToString().Length = 0 Then
                value = Nothing
            End If
            writer.WriteStartElement(elementName)
            writer.WriteString(value)
            writer.WriteEndElement()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Protected Function ReadXMLElement(ByVal reader As System.Xml.XmlReader, ByVal elementName As Object) As String
        Dim returnValue As String

        While True
            If reader.NodeType = XmlNodeType.Whitespace Then
                reader.Skip()
            ElseIf reader.LocalName = elementName Then
                returnValue = reader.ReadInnerXml
                returnValue = returnValue.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&")

                Return IIf(returnValue.Length = 0, Nothing, returnValue)
            Else ' Added by swamy on Nov 6 2009 to avoid infinite looping while looking for newly added members which were not previously saved to serialized file 
                Return Nothing
            End If

        End While

        Return Nothing

    End Function


    Protected Overridable Sub Clone(ByVal obj As BusinessBase)
        _bindingEdit = obj._bindingEdit
        _isChild = obj._isChild
        _isDeleted = obj._isDeleted
        _isDirty = obj._isDirty
        _isNew = obj._isNew
    End Sub

End Class
