Public Interface IFilterEngine(Of T)
    Property FilterString() As String
    Function Evaluate(ByVal item As Object) As Boolean
End Interface
