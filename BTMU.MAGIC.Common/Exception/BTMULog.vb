Imports System.IO


Public Class BTMULog
    Public Shared enUSCulture As New Globalization.CultureInfo("en-US")

    Private Shared Function GetLogFolder() As String
        Return AppDomain.CurrentDomain.BaseDirectory & "/ExceptionLog"
    End Function

    Private Shared Function GetCurrentFileName() As String
        Return "CURRENT.log"
    End Function

    Private Shared Function GetFilenameForClosingLog() As String
        Return String.Format("{0}.log", DateTime.Now.ToString("yyyyMMdd", enUSCulture))
    End Function

    Private Shared Function GetLogFileLimit() As Long
        'Return 5 * 1024 * 1024 '5 MB
        Return 1 * 1024 '1 KB
    End Function
    Private Shared Function GetLogFieldSeparator() As String
        Return "|"
    End Function
    Private Shared Function IsLogFull(ByVal logfile As FileInfo) As Boolean
        Return logfile.Length >= GetLogFileLimit()
    End Function

    Private Shared Function IsLogFull(ByVal logfilesize As Long) As Boolean
        Return logfilesize >= GetLogFileLimit()
    End Function

    Public Shared Sub WriteToLog(ByVal UserName As String _
                        , ByVal ComputerName As String _
                        , ByVal OSVersion As String _
                        , ByVal ErrorMessage As String)

        Dim logfile As FileStream
        If Not Directory.Exists(GetLogFolder()) Then Directory.CreateDirectory(GetLogFolder())

        If File.Exists(GetLogFolder() & "/" & GetCurrentFileName()) Then
            logfile = File.Open(GetLogFolder() & "/" & GetCurrentFileName(), FileMode.Append)
            If IsLogFull(logfile.Length) Then
                Dim oldfile As New FileInfo(GetLogFolder() & "/" & GetCurrentFileName())
                logfile.Close()
                If File.Exists(GetLogFolder() & "/" & GetFilenameForClosingLog()) Then
                    oldfile.CopyTo(GetLogFolder() & "/" & String.Format("{0}_{1}.log", DateTime.Now.ToString("yyyyMMdd", enUSCulture), (New Random).NextDouble))
                Else
                    oldfile.CopyTo(GetLogFolder() & "/" & GetFilenameForClosingLog())
                End If


                logfile = File.Create(GetLogFolder() & "/" & GetCurrentFileName())
            End If
        Else
            logfile = File.Create(GetLogFolder() & "/" & GetCurrentFileName())
        End If

        Dim data As Byte()
        Dim timestamp As String = DateTime.Now.ToString("yyyymmdd HH:mm:ss", enUSCulture)
        data = System.Text.ASCIIEncoding.ASCII.GetBytes(timestamp _
                                & GetLogFieldSeparator() & UserName & GetLogFieldSeparator() _
                                & ComputerName & GetLogFieldSeparator() & OSVersion & vbCrLf _
                                & ErrorMessage & " END " & timestamp & vbCrLf)
        logfile.Write(data, 0, data.Length)
        logfile.Close()
    End Sub

    Public Shared Sub WriteToLog(ByVal ErrorMessage As String)
        WriteToLog(Environment.UserName, Environment.MachineName, Environment.OSVersion.VersionString, ErrorMessage)
    End Sub

End Class
