<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ErrorWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button
        Me.lblBriefErrorMessage = New System.Windows.Forms.Label
        Me.picBxErrorImage = New System.Windows.Forms.PictureBox
        Me.txtDetailedErrorMessage = New System.Windows.Forms.TextBox
        Me.btnDetails = New System.Windows.Forms.Button
        CType(Me.picBxErrorImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(469, 83)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(89, 27)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblBriefErrorMessage
        '
        Me.lblBriefErrorMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBriefErrorMessage.Location = New System.Drawing.Point(67, 21)
        Me.lblBriefErrorMessage.Name = "lblBriefErrorMessage"
        Me.lblBriefErrorMessage.Size = New System.Drawing.Size(491, 59)
        Me.lblBriefErrorMessage.TabIndex = 1
        Me.lblBriefErrorMessage.Text = "error has occured "
        '
        'picBxErrorImage
        '
        Me.picBxErrorImage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picBxErrorImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picBxErrorImage.Location = New System.Drawing.Point(22, 21)
        Me.picBxErrorImage.Name = "picBxErrorImage"
        Me.picBxErrorImage.Size = New System.Drawing.Size(41, 40)
        Me.picBxErrorImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picBxErrorImage.TabIndex = 2
        Me.picBxErrorImage.TabStop = False
        '
        'txtDetailedErrorMessage
        '
        Me.txtDetailedErrorMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDetailedErrorMessage.Location = New System.Drawing.Point(12, 119)
        Me.txtDetailedErrorMessage.Multiline = True
        Me.txtDetailedErrorMessage.Name = "txtDetailedErrorMessage"
        Me.txtDetailedErrorMessage.ReadOnly = True
        Me.txtDetailedErrorMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtDetailedErrorMessage.Size = New System.Drawing.Size(546, 127)
        Me.txtDetailedErrorMessage.TabIndex = 3
        '
        'btnDetails
        '
        Me.btnDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDetails.Location = New System.Drawing.Point(12, 83)
        Me.btnDetails.Name = "btnDetails"
        Me.btnDetails.Size = New System.Drawing.Size(89, 27)
        Me.btnDetails.TabIndex = 4
        Me.btnDetails.Text = "&Details >>"
        Me.btnDetails.UseVisualStyleBackColor = True
        '
        'ErrorWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(570, 258)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnDetails)
        Me.Controls.Add(Me.txtDetailedErrorMessage)
        Me.Controls.Add(Me.picBxErrorImage)
        Me.Controls.Add(Me.lblBriefErrorMessage)
        Me.Controls.Add(Me.btnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(576, 290)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(576, 290)
        Me.Name = "ErrorWindow"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Error"
        Me.TopMost = True
        CType(Me.picBxErrorImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblBriefErrorMessage As System.Windows.Forms.Label
    Friend WithEvents picBxErrorImage As System.Windows.Forms.PictureBox
    Friend WithEvents txtDetailedErrorMessage As System.Windows.Forms.TextBox
    Friend WithEvents btnDetails As System.Windows.Forms.Button
End Class
