Public Class ErrorWindow

    Public Property BriefErrorMessage() As String
        Get
            Return lblBriefErrorMessage.Text
        End Get
        Set(ByVal value As String)
            lblBriefErrorMessage.Text = value
        End Set
    End Property

    Public Property DetailedErrorMessage() As String
        Get
            Return txtDetailedErrorMessage.Text
        End Get
        Set(ByVal value As String)
            txtDetailedErrorMessage.Text = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click
        If btnDetails.Text = "&Details <<" Then
            btnDetails.Text = "&Details >>"
            txtDetailedErrorMessage.Visible = False
            Me.MaximumSize = New System.Drawing.Size(576, 150)
            Me.MinimumSize = New System.Drawing.Size(576, 150)
        Else
            btnDetails.Text = "&Details <<"
            txtDetailedErrorMessage.Visible = True
            Me.MaximumSize = New System.Drawing.Size(576, 290)
            Me.MinimumSize = New System.Drawing.Size(576, 290)
            Me.Refresh()
        End If


    End Sub

    Private Sub ErrorWindow_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnDetails.Text = "&Details >>"
        txtDetailedErrorMessage.Visible = False

        Me.MaximumSize = New System.Drawing.Size(576, 150)
        Me.MinimumSize = New System.Drawing.Size(576, 150)
    End Sub

    Private Sub picBxErrorImage_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picBxErrorImage.Paint
        e.Graphics.DrawIcon(System.Drawing.SystemIcons.Warning, 0, 0)
    End Sub
End Class