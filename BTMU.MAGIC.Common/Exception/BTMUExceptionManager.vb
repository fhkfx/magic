Public Class BTMUExceptionManager
    Private Shared MsgReader As New Global.System.Resources.ResourceManager("BTMU.MAGIC.Exception.Resources", GetType(BTMUExceptionManager).Assembly)
    Public Shared EnableExceptionLog As Boolean

    Public Shared Sub LogAndShowError(ByVal errorMsgBrief As String, ByVal errorMsgDetails As String)
        Dim err As New ErrorWindow()

        err.DetailedErrorMessage = errorMsgDetails
        err.BriefErrorMessage = errorMsgBrief
        err.ShowDialog()
        If EnableExceptionLog Then LogMessage(errorMsgDetails)
    End Sub
    
    'record exception incident
    Private Shared Sub LogMessage(ByVal ErrMsg As String)
        BTMULog.WriteToLog(ErrMsg)
    End Sub
    ' alert the user of exception occurence
    Private Shared Sub AlertMessage(ByVal s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub
    'Log Exception Message 
    Public Shared Sub HandleException(ByVal ex As System.Exception)
        HandleException(ex, True)
    End Sub
    'Log exception message and alert user
    Public Shared Sub HandleException(ByVal ex As System.Exception, ByVal Alert As Boolean)
        Dim ErrMsg As String = ex.Source & " { " & ex.Message & " } "
        Dim e As System.Exception
        e = ex.InnerException

        While Not (e Is Nothing)
            ErrMsg = e.Source & " { " & e.Message & " } " & vbNewLine & vbTab & ErrMsg
            e = ex.InnerException
        End While

        LogMessage(ErrMsg)

    End Sub

    'Returns Error/Information/Warning Message from Resource file
    Public Shared Function GetMessage(ByVal MsgId As String) As String

        Return MsgReader.GetString(MsgId)

    End Function

End Class
