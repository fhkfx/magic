Public Class BusinessBaseCollection(Of T As {New, BusinessBase})
    Inherits System.ComponentModel.BindingList(Of T)


    Protected Overrides Sub InsertItem(ByVal index As Integer, ByVal item As T)

        item.setSequence(index + 1)
        item.SetCollection(Me)
        MyBase.InsertItem(index, item)


    End Sub

    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Dim itm As BusinessBase
        Dim idx As Integer = 0
        MyBase.RemoveItem(index)

        For Each itm In MyBase.Items
            itm.setSequence(idx + 1)
            idx = idx + 1
        Next

    End Sub

End Class
