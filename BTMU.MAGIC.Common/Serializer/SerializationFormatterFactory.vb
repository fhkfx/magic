Module SerializationFormatterFactory
    ''' <summary>
    ''' Creates a serialization formatter object.
    ''' </summary>
    Public Function GetFormatter() As ISerializationFormatter
        Return New BinaryFormatterWrapper()
    End Function

End Module
