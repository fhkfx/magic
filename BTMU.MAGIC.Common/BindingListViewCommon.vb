Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Reflection
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports System.Windows.Forms.Design

Public Class BindingListViewCommon(Of T As {New, CommonListItem})
    Implements IList(Of T)
    Implements IBindingList
    Implements IEnumerable(Of T)
    Implements ICancelAddNew
    Implements IBindingListView

#Region " Nested Type "

#Region " ListItem class"


    Private Class ListItem
        ' the reference to the item is kept to manage the values cache
        Private _item As T
        ' position in the original list
        Private _baseIndex As Integer

        ' contains the values on which the sort is performed
        ' these values are cached so it is available for the next sort.
        Private _itemValues As HybridDictionary = New HybridDictionary()

        Public ReadOnly Property Item() As T
            Get
                Return _item
            End Get
        End Property

        Public Property BaseIndex() As Integer
            Get
                Return _baseIndex
            End Get
            Set(ByVal value As Integer)
                _baseIndex = value
            End Set
        End Property

        Public Sub New(ByVal item As T, ByVal baseIndex As Integer)
            _item = item
            _baseIndex = baseIndex
        End Sub


        ' if at construction time we already know the sort properties,
        ' we can cache at this moment.
        Public Sub New(ByVal item As T, ByVal baseIndex As Integer, ByVal properties As PropertyDescriptorCollection)
            _item = item
            _baseIndex = baseIndex
            CachePropertyValues(properties)
        End Sub

        Public Function GetPropertyValue(ByVal propertyDesc As PropertyDescriptor) As Object
            Dim value As Object = _itemValues(propertyDesc)
            If value = Nothing Then
                ' retrieve the value
                value = propertyDesc.GetValue(_item)
                ' cache it for next times
                _itemValues(propertyDesc) = value
            End If
            Return value
        End Function

        Public Sub CachePropertyValues(ByVal properties As PropertyDescriptorCollection)
            Dim propertyDesc As PropertyDescriptor
            For Each propertyDesc In properties
                _itemValues(propertyDesc) = propertyDesc.GetValue(_item)
            Next
        End Sub

    End Class


#End Region


#Region " SortComparer "
    ' this is a modified version of the Brian Noyes' SorterComparer 
    Private Class SortComparer
        Implements IComparer(Of ListItem)

        ' For complex sort
        Private _sortCollection As ListSortDescriptionCollection = Nothing

        ' For simple sort
        Private _propDesc As PropertyDescriptor = Nothing
        Private _direction As ListSortDirection = ListSortDirection.Ascending

        Public Sub New()

        End Sub

        ' The simple case
        Public Sub SortComparer(ByVal propDesc As PropertyDescriptor, ByVal direction As ListSortDirection)
            _propDesc = propDesc
            _direction = direction
        End Sub

        ' The complex case
        Public Sub SortComparer(ByVal sortCollection As ListSortDescriptionCollection)
            _sortCollection = sortCollection
        End Sub

        Public Sub SetSortCriteria()
            _propDesc = Nothing
            _direction = ListSortDirection.Ascending
            _sortCollection = Nothing
        End Sub


        Public Sub SetSortCriteria(ByVal propDesc As PropertyDescriptor, ByVal direction As ListSortDirection)
            _propDesc = propDesc
            _direction = direction
            _sortCollection = Nothing
        End Sub

        Public Sub SetSortCriteria(ByVal sortCollection As ListSortDescriptionCollection)
            _propDesc = Nothing
            _direction = ListSortDirection.Ascending
            _sortCollection = sortCollection
        End Sub




#Region " IComparer(Of ListItem) Members "

        Public Function Compare(ByVal x As BindingListViewCommon(Of T).ListItem, ByVal y As BindingListViewCommon(Of T).ListItem) As Integer Implements System.Collections.Generic.IComparer(Of BindingListViewCommon(Of T).ListItem).Compare
            If Not _propDesc Is Nothing Then 'Simple sort 
                Dim xValue As Object = x.GetPropertyValue(_propDesc)
                Dim yValue As Object = y.GetPropertyValue(_propDesc)
                Return CompareValues(xValue, yValue, _direction)
            ElseIf Not _sortCollection Is Nothing AndAlso _sortCollection.Count > 0 Then
                Return MultiPropertyComparer(x, y)
            Else
                ' sorting by original position
                Return x.BaseIndex.CompareTo(y.BaseIndex)
            End If
        End Function
#End Region

        Private Function CompareValues(ByVal xValue As Object, ByVal yValue As Object, ByVal direction As ListSortDirection) As Integer

            Dim retValue As Integer = 0
            Dim done As Boolean = False

            Dim xComp As IComparable = DirectCast(xValue, IComparable)
            If Not xComp Is Nothing Then
                ' Can ask the x value
                retValue = xComp.CompareTo(yValue)
                done = True
            Else
                Dim yComp As IComparable = DirectCast(yValue, IComparable)
                If Not yComp Is Nothing Then
                    'Can ask the y value
                    retValue = yComp.CompareTo(xValue)
                    done = True
                End If
            End If

            If Not done Then
                'not comparable, compare String representations
                retValue = xValue.ToString().CompareTo(yValue.ToString())
            End If

            If direction = ListSortDirection.Descending Then
                retValue = retValue * -1
            End If
            Return retValue
        End Function

        '/ this an iterative version of the Brian's RecursiveCompareInternal
        Private Function MultiPropertyComparer(ByVal x As ListItem, ByVal y As ListItem) As Integer
            Dim retValue As Integer = 0
            Dim index As Integer = 0

            While (index < _sortCollection.Count AndAlso retValue = 0)

                Dim listSortDesc As ListSortDescription = _sortCollection(index)
                Dim xValue As Object = x.GetPropertyValue(listSortDesc.PropertyDescriptor)
                Dim yValue As Object = y.GetPropertyValue(listSortDesc.PropertyDescriptor)
                retValue = CompareValues(xValue, yValue, listSortDesc.SortDirection)
                index = index + 1
            End While

            Return retValue
        End Function

    End Class
#End Region

#Region " ViewEnumerator "
    Private Class ViewEnumerator
        Implements IEnumerator(Of T)

        Private _list As IList(Of T)
        Private _viewIndex As List(Of ListItem)
        Private _index As Integer

        Public Sub New(ByVal List As IList(Of T), ByVal viewIndex As List(Of ListItem))
            _list = List
            _viewIndex = viewIndex
            Reset()
        End Sub

        Public ReadOnly Property Current() As T Implements System.Collections.Generic.IEnumerator(Of T).Current
            Get
                Return _list(_viewIndex(_index).BaseIndex)
            End Get
        End Property

        Public ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                Return _list(_viewIndex(_index).BaseIndex)
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            If _index < _viewIndex.Count - 1 Then
                _index = _index + 1
                Return True
            Else
                Return False
            End If
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _index = -1
        End Sub

#Region " IDisposable Support "

        Private _disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not _disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            _disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

#End Region

    End Class
#End Region

#End Region

    Private _list As IList(Of T)
    Private _supportsBinding As Boolean
    Private _bindingList As IBindingList

    Private _sorted As Boolean = False
    Private _sortDirection As ListSortDirection = ListSortDirection.Ascending
    Private _sortProperty As PropertyDescriptor = Nothing
    Private _sortDescriptions As ListSortDescriptionCollection = Nothing

    Private _filtered As Boolean = False
    Private _filterString As String = String.Empty
    Private _filterEngine As IFilterEngine(Of T) = Nothing

    ' view on data
    Private _viewIndex As List(Of ListItem) = New List(Of ListItem)()

    ' used to sort the view
    Private _sortComparer As SortComparer = New SortComparer()

    Public Property Filter() As String
        Get
            Return _filterString
        End Get
        Set(ByVal value As String)
            If _filterString <> value Then
                _filterString = value
                FilterEngine = Nothing
                UpdateView()
            End If
        End Set
    End Property

    ' this can be used to sort a view programmatically, 
    ' how we do with DataView.Sort
    Public Property Sort() As String
        Get
            ' Obtaining a string representation of the sort criteria
            Dim sortString As String = String.Empty
            If Not _sortProperty Is Nothing Then
                sortString = GetSortString(_sortProperty, _sortDirection)
            ElseIf Not _sortDescriptions Is Nothing AndAlso _sortDescriptions.Count > 0 Then
                Dim sb As StringBuilder = New StringBuilder()
                Dim desc As ListSortDescription
                For Each desc In _sortDescriptions
                    If sb.Length > 0 Then
                        sb.Append(", ")
                    End If
                    sb.Append(GetSortString(desc.PropertyDescriptor, desc.SortDirection))
                Next
                sortString = sb.ToString()
            End If
            Return sortString
        End Get
        Set(ByVal value As String)
            ' Parsing the string to obtain the sort criteria
            If (String.IsNullOrEmpty(value)) Then
                UndoSort()
                Return
            End If
            Dim sortParts() As String = value.Split("'")
            If (sortParts.Length = 1) Then
                Dim desc As ListSortDescription = GetSortDescription(sortParts(0).Trim())
                ApplySort(desc.PropertyDescriptor, desc.SortDirection)
            Else
                Dim descs As New List(Of ListSortDescription)
                Dim i As Integer
                For i = 0 To sortParts.Length - 1
                    descs.Add(GetSortDescription(sortParts(i).Trim()))
                Next
                Dim sorts As ListSortDescriptionCollection = New ListSortDescriptionCollection(descs.ToArray())
                ApplySort(sorts)
            End If
        End Set
    End Property

    Private Function GetSortString(ByVal prop As PropertyDescriptor, ByVal sortDirection As ListSortDirection) As String
        Dim name As String = prop.Name
        Dim direction As String = IIf(sortDirection = ListSortDirection.Ascending, "ASC", "DESC")
        Return String.Format("{0} {1}", name, direction)
    End Function

    Private Function GetSortDescription(ByVal input As String) As ListSortDescription

        Dim propertyName As String
        Dim direction As ListSortDirection = ListSortDirection.Ascending

        Dim options As RegexOptions = RegexOptions.IgnoreCase Or RegexOptions.ExplicitCapture
        Dim regex As Regex = New Regex("^((\[(?<Property>.+)\])|(?<Property>\S+))([ ]+(?<Direction>ASC|DESC))?$", options)

        ' Get match
        Dim match As Match = regex.Match(input)
        If Not match Is Nothing AndAlso match.Success Then

            propertyName = match.Groups("Property").Value
            Dim group As Group = match.Groups("Direction")
            If (Not group Is Nothing AndAlso group.Success) Then
                Dim dir As String = group.Value.ToUpper()
                direction = IIf(dir = "ASC", ListSortDirection.Ascending, ListSortDirection.Descending)
            End If
        Else
            Throw New ArgumentException("Invalid sort expression")
        End If

        Dim prop As PropertyDescriptor = GetPropertyDescriptor(propertyName)
        If prop Is Nothing Then
            Throw New ArgumentException("The property '" + propertyName + "' is not a property of the list item type.")
        End If
        Return New ListSortDescription(prop, direction)
    End Function

    ' <summary>
    ' Creates a new view based on the provided IList object.
    ' </summary>
    ' <param name="listArg">The IList (collection) containing the data.</param>
    Public Sub New(ByVal listArg As IList(Of T))
        Me.List = listArg
    End Sub

    ' <summary>
    ' Creates a new view based on the provided IList object.
    ' </summary>
    '<param name="listArg">The IList (collection) containing the data.</param>
    '<param name="filterEngine">
    'Filter engine instance object.
    ' </param>
    Public Sub New(ByVal listArg As IList(Of T), ByVal filterEngine As IFilterEngine(Of T))
        filterEngine = filterEngine
        Me.List = listArg
    End Sub

    Public Property List() As IList(Of T)
        Get
            Return _list
        End Get
        Set(ByVal value As IList(Of T))
            If Not _bindingList Is Nothing Then
                RemoveHandler _bindingList.ListChanged, AddressOf SourceChanged
                _bindingList = Nothing
                _supportsBinding = False
            End If
            _list = value
            If TypeOf (_list) Is IBindingList Then
                _supportsBinding = True
                _bindingList = DirectCast(_list, IBindingList)
                AddHandler _bindingList.ListChanged, AddressOf SourceChanged
            End If
            UpdateView()
        End Set
    End Property

    Public Property FilterEngine() As IFilterEngine(Of T)
        Get
            If _filterEngine Is Nothing Then
                _filterEngine = New DefaultFilterEngine(Of T)(_filterString)
            End If
            Return _filterEngine
        End Get
        Set(ByVal value As IFilterEngine(Of T))
            _filterEngine = value
            If _filterEngine Is Nothing Then
                _filterEngine = New DefaultFilterEngine(Of T)()
                _filterEngine.FilterString = _filterString
            End If
        End Set
    End Property

    ' <summary>
    ' Returns True if the view is currently filtered.
    ' </summary>
    Public ReadOnly Property IsFiltered() As Boolean
        Get
            Return _filtered
        End Get
    End Property

    Private Function GetSortProperties() As PropertyDescriptorCollection
        Dim list As List(Of PropertyDescriptor) = New List(Of PropertyDescriptor)()
        Dim sortDesc As ListSortDescription
        If Not _sortProperty Is Nothing Then
            list.Add(_sortProperty)
        ElseIf Not _sortDescriptions Is Nothing Then
            For Each sortDesc In _sortDescriptions
                list.Add(sortDesc.PropertyDescriptor)
            Next
        End If

        Dim sortProperties As PropertyDescriptorCollection = New PropertyDescriptorCollection(list.ToArray())
        Return sortProperties
    End Function

    Private Sub SourceChanged(ByVal sender As Object, ByVal e As ListChangedEventArgs)
        Dim origPos As Integer = -1
        Dim viewPos As Integer = -1
        Dim newItem As T
        Dim item As ListItem
        Dim sortProperties As PropertyDescriptorCollection = GetSortProperties()

        Select Case e.ListChangedType
            ' when an item is added, we do not reapply the sort or the filter
            ' and the item is simply added to the last postition
            Case ListChangedType.ItemAdded
                origPos = e.NewIndex
                ' add new item to view
                newItem = _list(origPos)
                _viewIndex.Add(New ListItem(newItem, origPos, sortProperties))
                viewPos = _viewIndex.Count - 1
                ' raise event
                OnListChanged(New ListChangedEventArgs(e.ListChangedType, viewPos))

            Case ListChangedType.ItemChanged
                origPos = e.OldIndex
                ' update view item
                viewPos = GetViewPosition(origPos)
                If (viewPos <> -1) Then
                    newItem = _list(origPos)
                    _viewIndex(viewPos) = New ListItem(newItem, origPos, sortProperties)
                Else
                    ' refresh cached values for sort
                    _viewIndex(viewPos).CachePropertyValues(sortProperties)
                End If
                ' raise event if appropriate
                If (viewPos <> -1) Then
                    OnListChanged(New ListChangedEventArgs(e.ListChangedType, viewPos))
                End If
            Case ListChangedType.ItemDeleted
                origPos = e.NewIndex
                ' delete corresponding item from index if (any)
                viewPos = GetViewPosition(origPos)
                If (viewPos <> -1) Then
                    _viewIndex.RemoveAt(viewPos)
                End If
                ' adjust index xref values
                For Each item In _viewIndex
                    If item.BaseIndex > e.NewIndex Then
                        item.BaseIndex = item.BaseIndex - 1
                    End If
                Next item

                ' raise event if appropriate
                If viewPos > -1 Then
                    OnListChanged(New ListChangedEventArgs(e.ListChangedType, viewPos))
                End If
            Case Else
                UpdateView()
        End Select
    End Sub


    Private Function GetOriginalPosition(ByVal viewPosition As Integer)
        Return _viewIndex(viewPosition).BaseIndex
    End Function

    Private Function GetViewPosition(ByVal originaPosition As Integer) As Integer
        Dim result As Integer = -1
        Dim i As Integer
        For i = 0 To _viewIndex.Count - 1
            If _viewIndex(i).BaseIndex = originaPosition Then
                result = i
                Exit For
            End If
        Next
        Return result
    End Function

#Region " Filtering / Sorting view "

    Private Sub UpdateView()
        Dim index As Integer = 0
        Dim sortProperties As PropertyDescriptorCollection = GetSortProperties()
        Dim filterEngine As IFilterEngine(Of T)
        Dim item As T
        _viewIndex.Clear()
        filterEngine = Me.FilterEngine


        If String.IsNullOrEmpty(_filterString) Then

            For Each item In _list
                _viewIndex.Add(New ListItem(item, index, sortProperties))
                index = index + 1
            Next
            _filtered = False
        Else

            For Each item In _list
                If filterEngine.Evaluate(item) Then
                    _viewIndex.Add(New ListItem(item, index, sortProperties))
                End If
                index = index + 1
            Next
            _filtered = True
        End If

        If _sorted Then
            SortView(False)
        End If
        OnListChanged(New ListChangedEventArgs(ListChangedType.Reset, 0))
    End Sub

    Private Sub SortView(ByVal raiseListChanged As Boolean)
        If Not _sortProperty Is Nothing Then
            _sortComparer.SetSortCriteria(_sortProperty, _sortDirection)
            _sorted = True
        ElseIf Not _sortDescriptions Is Nothing AndAlso _sortDescriptions.Count > 0 Then
            _sortComparer.SetSortCriteria(_sortDescriptions)
            _sorted = True
        Else
            _sortComparer.SetSortCriteria()
            _sorted = False
        End If
        _viewIndex.Sort(_sortComparer)

        If raiseListChanged Then
            OnListChanged(New ListChangedEventArgs(ListChangedType.Reset, 0))
        End If
    End Sub

    Private Sub UndoSort()
        _sortProperty = Nothing
        _sortDirection = ListSortDirection.Ascending
        _sortDescriptions = Nothing
        SortView(True)
    End Sub

    Private Function GetPropertyDescriptor(ByVal propertyName As String) As PropertyDescriptor
        Dim desc As PropertyDescriptor = Nothing
        If Not String.IsNullOrEmpty(propertyName) Then
            Dim prop As PropertyDescriptor
            Dim itemType As Type = GetType(T)
            For Each prop In TypeDescriptor.GetProperties(itemType)
                If (prop.Name = propertyName) Then
                    desc = prop
                    Exit For
                End If
            Next

        End If
        Return desc
    End Function

#End Region

#Region " IList(Of T) Members "

    Public Sub Add(ByVal item As T) Implements System.Collections.Generic.ICollection(Of T).Add
        _list.Add(item)
    End Sub

    Public Sub Clear() Implements System.Collections.Generic.ICollection(Of T).Clear
        _list.Clear()
    End Sub

    Public Function Contains(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Contains
        Return _list.Contains(item)
    End Function

    Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of T).CopyTo
        _list.CopyTo(array, arrayIndex)
    End Sub

    Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of T).Count
        Get
            Return _viewIndex.Count
        End Get
    End Property

    Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
        Get
            Return _list.IsReadOnly
        End Get
    End Property

    Public Function Remove(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Remove
        Return _list.Remove(item)
    End Function

    Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
        Return New ViewEnumerator(_list, _viewIndex)
    End Function

    Public Function IndexOf(ByVal item As T) As Integer Implements System.Collections.Generic.IList(Of T).IndexOf
        Return GetViewPosition(_list.IndexOf(item))
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal item As T) Implements System.Collections.Generic.IList(Of T).Insert
        _list.Insert(index, item)
    End Sub

    Default Public Property Item(ByVal index As Integer) As T Implements System.Collections.Generic.IList(Of T).Item
        Get
            Dim listItem As ListItem = _viewIndex(index)
            Return listItem.Item
        End Get
        Set(ByVal value As T)
            _list(GetOriginalPosition(index)) = value
        End Set
    End Property

    Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of T).RemoveAt
        _list.RemoveAt(GetOriginalPosition(index))
    End Sub

    Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return GetEnumerator()
    End Function
#End Region

#Region " IBindingList "
    Public Sub AddIndex(ByVal [property] As System.ComponentModel.PropertyDescriptor) Implements System.ComponentModel.IBindingList.AddIndex
        If _supportsBinding Then
            _bindingList.AddIndex([property])
        End If
    End Sub

    Public Function AddNew() As Object Implements System.ComponentModel.IBindingList.AddNew
        Dim result As T
        If _supportsBinding Then
            result = DirectCast(_bindingList.AddNew(), T)
        Else
            result = New T()
        End If
        Return result
    End Function

    Public ReadOnly Property AllowEdit() As Boolean Implements System.ComponentModel.IBindingList.AllowEdit
        Get
            If _supportsBinding Then
                Return _bindingList.AllowEdit
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements System.ComponentModel.IBindingList.AllowNew
        Get
            If _supportsBinding Then
                Return _bindingList.AllowNew
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property AllowRemove() As Boolean Implements System.ComponentModel.IBindingList.AllowRemove
        Get
            If _supportsBinding Then
                Return _bindingList.AllowRemove
            Else
                Return False
            End If
        End Get
    End Property

    Public Sub ApplySort(ByVal [property] As System.ComponentModel.PropertyDescriptor, ByVal direction As System.ComponentModel.ListSortDirection) Implements System.ComponentModel.IBindingList.ApplySort
        _sortProperty = [property]
        _sortDirection = direction
        _sortDescriptions = Nothing
        SortView(True)
    End Sub

    Public Function Find(ByVal [property] As String, ByVal key As Object) As Integer
        Dim findProperty As PropertyDescriptor = GetPropertyDescriptor([property])
        If Not findProperty Is Nothing Then
            Return Find1(findProperty, key)
        End If
        Return -1
    End Function

    Public Function Find1(ByVal [property] As System.ComponentModel.PropertyDescriptor, ByVal key As Object) As Integer Implements System.ComponentModel.IBindingList.Find
        ' Simple iteration
        Dim i As Integer
        For i = 0 To i < Count - 1
            Dim item As T
            item = Me(i)
            If [property].GetValue(item).Equals(key) Then
                Return i
            End If
        Next i
        Return -1 ' Not found
    End Function
    Public ReadOnly Property IsSorted() As Boolean Implements System.ComponentModel.IBindingList.IsSorted
        Get
            Return _sorted
        End Get
    End Property

    Public Event ListChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs) Implements System.ComponentModel.IBindingList.ListChanged

    Protected Overridable Sub OnListChanged(ByVal e As ListChangedEventArgs)
        'If Not ListChanged Is Nothing Then
        '    ListChanged(Me, e)
        'End If
        RaiseEvent ListChanged(Me, e)
    End Sub



    Public Sub RemoveIndex(ByVal [property] As System.ComponentModel.PropertyDescriptor) Implements System.ComponentModel.IBindingList.RemoveIndex
        If _supportsBinding Then
            _bindingList.RemoveIndex([property])
        End If
    End Sub

    Public Sub RemoveSort() Implements System.ComponentModel.IBindingList.RemoveSort
        UndoSort()
    End Sub

    Public ReadOnly Property SortDirection() As System.ComponentModel.ListSortDirection Implements System.ComponentModel.IBindingList.SortDirection
        Get
            Return _sortDirection
        End Get
    End Property

    Public ReadOnly Property SortProperty() As System.ComponentModel.PropertyDescriptor Implements System.ComponentModel.IBindingList.SortProperty
        Get
            Return _sortProperty
        End Get
    End Property

    Public ReadOnly Property SupportsChangeNotification() As Boolean Implements System.ComponentModel.IBindingList.SupportsChangeNotification
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property SupportsSearching() As Boolean Implements System.ComponentModel.IBindingList.SupportsSearching
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property SupportsSorting() As Boolean Implements System.ComponentModel.IBindingList.SupportsSorting
        Get
            Return True
        End Get
    End Property



    Private Sub CopyTo1(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
        CopyTo(array, index)
    End Sub

    Public ReadOnly Property Count1() As Integer Implements System.Collections.ICollection.Count
        Get
            Return Count()
        End Get
    End Property

    Public ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
        Get
            Return Nothing
        End Get
    End Property

    Public Function Add1(ByVal value As Object) As Integer Implements System.Collections.IList.Add
        Add(value)
    End Function

    Public Sub Clear1() Implements System.Collections.IList.Clear
        Clear()
    End Sub

    Public Function Contains1(ByVal value As Object) As Boolean Implements System.Collections.IList.Contains
        Contains1(value)
    End Function

    Public Function IndexOf1(ByVal value As Object) As Integer Implements System.Collections.IList.IndexOf
        IndexOf(value)
    End Function

    Public Sub Insert1(ByVal index As Integer, ByVal value As Object) Implements System.Collections.IList.Insert
        Insert(index, value)
    End Sub

    Public ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IList.IsFixedSize
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property IsReadOnly1() As Boolean Implements System.Collections.IList.IsReadOnly
        Get
            Return IsReadOnly()
        End Get
    End Property

    Public Overloads Property Item1(ByVal index As Integer) As Object Implements System.Collections.IList.Item
        Get
            Return Item(index)
        End Get
        Set(ByVal value As Object)
            Item(index) = value
        End Set
    End Property

    Public Sub Remove1(ByVal value As Object) Implements System.Collections.IList.Remove
        Remove(value)
    End Sub

    Public Sub RemoveAt1(ByVal index As Integer) Implements System.Collections.IList.RemoveAt
        RemoveAt(index)
    End Sub

#End Region

#Region " ICancelAddNew "
    Public Sub CancelNew(ByVal itemIndex As Integer) Implements System.ComponentModel.ICancelAddNew.CancelNew
        Dim can As ICancelAddNew = DirectCast(_list, ICancelAddNew)
        If Not can Is Nothing Then
            can.CancelNew(itemIndex)
        Else
            _list.RemoveAt(itemIndex)
        End If
    End Sub

    Public Sub EndNew(ByVal itemIndex As Integer) Implements System.ComponentModel.ICancelAddNew.EndNew
        Dim can As ICancelAddNew = DirectCast(_list, ICancelAddNew)
        If Not can Is Nothing Then
            can.EndNew(itemIndex)
        End If
    End Sub
#End Region

#Region " IBindingListView "
    Public Sub ApplySort(ByVal sorts As System.ComponentModel.ListSortDescriptionCollection) Implements System.ComponentModel.IBindingListView.ApplySort
        _sortDescriptions = sorts
        _sortDirection = ListSortDirection.Ascending
        _sortProperty = Nothing
        SortView(True)
    End Sub

    Public Property Filter1() As String Implements System.ComponentModel.IBindingListView.Filter
        Get
            Return Filter
        End Get
        Set(ByVal value As String)
            Filter = value
        End Set
    End Property

    Public Sub RemoveFilter() Implements System.ComponentModel.IBindingListView.RemoveFilter
        Filter = String.Empty
    End Sub

    Public ReadOnly Property SortDescriptions() As System.ComponentModel.ListSortDescriptionCollection Implements System.ComponentModel.IBindingListView.SortDescriptions
        Get
            Return _sortDescriptions
        End Get
    End Property

    Public ReadOnly Property SupportsAdvancedSorting() As Boolean Implements System.ComponentModel.IBindingListView.SupportsAdvancedSorting
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property SupportsFiltering() As Boolean Implements System.ComponentModel.IBindingListView.SupportsFiltering
        Get
            Return True
        End Get
    End Property

#End Region
End Class
